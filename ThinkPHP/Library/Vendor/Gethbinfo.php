<?php

/**
 * Created by PhpStorm
 * User : Me
 * Date : 11:44
 */

class Gethbinfo
{
    private $url = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/gethbinfo'; //请求URL

    private $nonce_str; // 随机字符串，不长于32位
    private $mch_billno; // 商户订单号
    private $mch_id; // 商户号
    private $weixin_appid; // 公众账号appid
    private $bill_type; // 订单类型 MCHT:通过商户订单号获取红包信息。

    private $api_password; // 商户支付密钥
    /**
     * 公钥
     */
    private $public_key;
    /**
     * 私钥
     */
    private $private_key;
    /**
     * ca证书
     */
    private $rootca;

    public function __construct()
    {
        //初始化红包设置信息
        $this->weixin_appid = C('WX_APPID');
        $this->mch_id       = C('WX_MCHID');
        $this->nonce_str    = $this->create_nonce_str(32);
        $this->api_password = C('WX_KEY');

        $this->public_key   = C('PUBLIC_KEY');
        $this->private_key  = C('PRIVATE_KEY');
        $this->rootca       = C('ROOTCA');
    }

    //发送红包请求
    public function gethbinfo($mch_billno, $bill_type = 'MCHT')
    {
        $sign = $this->create_sign($mch_billno, $bill_type); // 签名
        $send_array = array(
            'nonce_str'    => $this->nonce_str,
            'sign'         => $sign,
            'mch_billno'   => $mch_billno,
            'mch_id'       => $this->mch_id,
            'appid'      => $this->weixin_appid,
            'bill_type'    => $bill_type
        );

        $send_xml = $this->toXml($send_array, '');

        \Think\Log::write($send_xml, 'GET_HB_INFO_XML');

        $data = $this->curl_post_ssl($this->url, $send_xml);
        $time = date('Y-m-d H:i:s');
        $str  = $time . " ";

        if ($data) {
            $data = $this->xmlToArray($data);
            \Think\Log::write(json_encode($data), 'REDBAG_RESULT');
            return $data;
        } else {
            \Think\Log::write('未获得微信返回数据', 'REDBAG_RESULT');
            return FALSE;
        }
    }

    /*
    请确保您的libcurl版本是否支持双向认证，版本高于7.20.1
    */
    private function curl_post_ssl($url, $vars, $second = 60, $aHeader = array())
    {
        $ch = curl_init();
        //超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '10.206.30.98');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //以下两种方式需选择一种
        //第一种方法，cert 与 key 分别属于两个.pem文件
        //默认格式为PEM，可以注释
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');
        curl_setopt($ch, CURLOPT_SSLCERT, $this->public_key);
        //默认格式为PEM，可以注释
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, 'PEM');
        curl_setopt($ch, CURLOPT_SSLKEY, $this->private_key);
        //ca证书
        curl_setopt($ch, CURLOPT_CAINFO, $this->rootca);
        //第二种方式，两个文件合成一个.pem文件
        //curl_setopt($ch,CURLOPT_SSLCERT,getcwd().'/all.pem');
        if (count($aHeader) >= 1) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeader);
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
        $data = curl_exec($ch);
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            echo "call faild, errorCode:$error\n\n\n\n";
            curl_close($ch);
            return false;
        }
    }

    //生成签名
    private function create_sign($mch_billno, $bill_type)
    {
        $string_array = array(
            'nonce_str' => $this->nonce_str,
            'mch_billno' => $mch_billno,
            'mch_id' => $this->mch_id,
            'appid' => $this->weixin_appid,
            'bill_type' => $bill_type
        );
        //对参数按照key=value的格式，并按照参数名ASCII字典序排序
        ksort($string_array);
        $stringA = '';
        foreach ($string_array as $key => $value) {
            if (!empty($value)) {
                $stringA .= "$key=$value";
                if ($key != 'nonce_str') {
                    $stringA .= '&';
                }
            }
        }

        //转成UTF-8
        $stringA = $this->gbkToUtf8($stringA);
        $stringSignTemp = "$stringA&key=$this->api_password";
        $sign = MD5($stringSignTemp);
        $sign = strtoupper($sign);

        return $sign;
    }

    //生成随机字符串
    private function create_nonce_str($length)
    {
        $str = null;
        $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        $max = strlen($strPol) - 1;
        for ($i = 0; $i < $length; $i++) {
            $str .= $strPol[rand(0, $max)]; //rand($min,$max)生成介于min和max两个数之间的一个随机整数
        }
        return $str;
    }

    /**
     *自动判断把gbk或gb2312编码的字符串转为utf8
     *能自动判断输入字符串的编码类，如果本身是utf-8就不用转换，否则就转换为utf-8的字符串
     *支持的字符编码类型是：utf-8,gbk,gb2312
     *@$str:string 字符串
     */
    private function gbkToUtf8($str)
    {
        $charset = mb_detect_encoding($str, array('ASCII', 'UTF-8', 'GBK', 'GB2312'));
        $charset = strtolower($charset);
        if ("utf-8" != $charset) {
            $str = iconv('UTF-8', $charset, $str);
        }
        return $str;
    }

    /*xml转数组*/
    private function xmlToArray($postStr)
    {
        $msg = array();
        $msg = (array) simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        return $msg;
    }

    /*数组转xml*/
    private function toXml($data)
    {
        $xml = '<xml>';
        foreach ($data as $key => $value) {
            $xml .= "<" . $key . "><![CDATA[" . $value . "]]></" . $key . ">";
        }
        $xml .= '</xml>';
        return $xml;
    }
}
