<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/8
 * Time: 14:06
 */
namespace Home\Controller;
use Think\Controller;

class TestController extends Controller{

    public function payIncome()
    {

        $pay = D('Paylog');
        $redpacketLog = D('RedpacketLogs');
        $tasks = D('tasks');

        $b_time = I('b_time');
        $e_time = I('e_time');
        if(!$b_time || !$e_time) {
            echo 'error';exit;
        }

        $map['addtime'] = array('between', array($b_time, $e_time));
        $map['state'] = 3;
        $map['from_id'] = ['not in' , [10000, 998]]; //去掉官方渠道的 充值数据
        // $map['from_id'] = ['NEQ' , 10000]; //去掉官方渠道的 充值数据
        $paylog = $pay->where($map)->field('paytype, money, addtime')->order('addtime desc')->select();


        $repMap['paytype'] = array('in', [1,3,4]);
        $repMap['create_time'] = array('between', array($b_time, $e_time));
        $repMap['status'] = 1;
        // $repMap['from_id'] = ['NEQ' , 10000];
        $redpacketList = $redpacketLog->where($repMap)->field('amount, paytype, create_time, all_consume')->select();


        //计算扣除红包
        $redLog = D('BuckleRedLog')->where(['time'=>['between', array($b_time, $e_time)]])->field('from_unixtime(time,"%Y-%m-%d") as date_at, sum(num) as red_num')->group('date_at')->select();

        $redLogData = [];
        foreach ($redLog as $val) {
            $redLogData[$val['date_at']] = $val['red_num'] /100;
        }

        $ret = [];

        foreach ($paylog as $k => $v) {
            $date = date('Y-m-d', $v['addtime']);
            $ret[$date]['money_'.$v['paytype']] += $v['money']; //10微信 2支付宝充值 ,13米花充值
            $ret[$date]['total'] += intval($v['money']);  //充值总额

            if(!isset($ret[$date]['all_consume'])) {
                $t_list = $tasks->where(['date_at'=> $date, 'from_id' => ['NEQ',10000]])->field('sum(all_consume) as all_consume')->find();
            }
            $ret[$date]['all_consume'] = $t_list['all_consume'];
            $ret[$date]['hb_consume'] = $redLogData[$date]; //红包扣除
        }

        foreach ($redpacketList as $k => $v) {
            $date = date('Y-m-d', $v['create_time']);
            $ret[$date]['amount_'.$v['paytype']] += $v['amount'] ? $v['amount'] : 0; //1微信 3支付宝充值兑换
        }
        krsort($ret);

        echo json_encode($ret);
    }
}