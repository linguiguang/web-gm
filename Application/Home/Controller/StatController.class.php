<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/8
 * Time: 14:06
 */
namespace Home\Controller;
use Think\Controller;

class StatController extends Controller{


    /**新 渠道任务统计 **/

    public function channelstat() {

        writeLog('channelstat::'. json_encode(I('')), 'info');

        //$flist = F('NIUNIUSTAT');
        $fromkeyStat = D('TaskStat');
        $taskConfig = D('TaskConfig');
        $taskRecord = D('TaskRecord');
        $fromKey = D('DFromkey');

        $from_id = I('from_id');
        $uid = I('uid');
        $char_id = I('char_id');
        $recharge_amount = I('recharge_amount'); //充值金额
        $task_time = I('task_time'); //期数
        // $screenings = I('type'); //场次 1红包场，2休闲场

        if(I('gold_amount') > 0) {
            $gold_amount = I('gold_amount'); //休闲场
            $screenings = 2;
        } else if(I('gold_hb') > 0) {
            $gold_amount = I('gold_hb'); //红包
            $screenings = 1;
        } else {
            $screenings = 0;
        }

        $map['from_id'] = $newMap['from_id'] = $from_id;
        $map['task_time'] = $task_time;
        $key = 'FROM_'.$from_id."_TASK_".$task_time;
        $map['screenings'] = ['in' ,[0,$screenings]]; //包含充值的查询 //场次

        $flist = $taskConfig->where($map)->select();  //查询渠道配置内容
        unset($map['screenings']);

        $map['uid'] = $recordMap['uid'] = $uid;
        $map['char_id'] = $recordMap['char_id'] = $char_id;

        $info = $fromkeyStat->where($map)->getField('task_config_id', true); //查询task_stat表的数据
        // $taskRecordInfo = $taskRecord->where($map)->find(); //查询充值金币消耗等数据

        //TODO 修改计算当日月收益的总消耗
        //查询当日的数据
        $redpacketLog = D('tasks');

        $from_key = $fromKey->where(['from_id' => $from_id, 'num' => $task_time])->find();

        //查询用户数据
        $user_from_reg = D('User')->where(['adddate' => ['BETWEEN', [$from_key['start_time'], $from_key['end_time']]], 'id' => $uid])->find();

        // var_dump($flist);exit;
        //遍历$flist 配置的内容
        if(!empty($flist)) {
            // $all_consume = 0;
            $level = 0;  //关卡
            foreach ($flist as $val ) {

                //关卡不跳过
                if($gold_amount >= $val['value'] && $val['screenings'] > 0 && $val['level'] > 0) {
                    $level = $val['level'];
                    $task_config_id = $val['id'];

                }


                //不是在本期注册的用户不进新手金币奖励
                if(!$user_from_reg && $val['type'] == 1 && $val['condition'] == 2) {
                    continue;
                }

                if($user_from_reg && $val['type'] == 2 && $val['condition'] == 3) {
                    //是新用户就跳过 老用户充值奖励设置
                    continue;
                }

                //是老用户，跳过新注册用户充值奖励设置
                if(!$user_from_reg && $val['type'] == 2 && $val['condition'] == 2) {
                    //是新用户就跳过 老用户充值奖励设置
                    continue;
                }

                $all_consume = 0;
                if($val['type'] == 1 && $gold_amount >= $val['value']) {  //金币的

                    //检查是否已有这条数据
                    if(!in_array($val['id'], $info)) {
                        //插入数据
                        $taskStatData = [
                            'task_config_id' => $val['id'],
                            'uid' => $uid,
                            'char_id' => $char_id,
                            'from_id' => $from_id,
                            'task_time' => $task_time,
                            'addtime' => time(),
                        ];
                        $fromkeyStat->add($taskStatData);
                        //累计消耗
                        $all_consume = $val['reward'];
                    }
                }


                if($val['type'] == 2 && $recharge_amount >= $val['value']) {  //充值的
                    //检查是否已有这条数据
                    if(!in_array($val['id'], $info)) {
                        //插入数据
                        $taskStatData = [
                            'task_config_id' => $val['id'],
                            'uid' => $uid,
                            'char_id' => $char_id,
                            'from_id' => $from_id,
                            'task_time' => $task_time,
                            'addtime' => time(),
                        ];
                        $res = $fromkeyStat->add($taskStatData);
                        //累计消耗
                        $all_consume = $val['reward'];
                    }
                }
                $redMap = [];
                $redMap['date_at'] = date("Y-m-d");
                $redMap['task_time'] = $task_time;
                $redMap['from_id'] = $from_id;
                $redMap['screenings'] = $val['screenings'];//场次区分
                $redpacketInfo = $redpacketLog->where($redMap)->find();

                if($all_consume > 0 ) {
                    $all_consume = bcadd($redpacketInfo['all_consume'], $all_consume ,1);
                     //记录总消耗
                    if(empty($redpacketInfo)) {
                        $res = $redpacketLog->add([
                            'date_at' =>date("Y-m-d"),
                            'task_time' => $task_time,
                            'from_id' =>$from_id,
                            'screenings' => $val['screenings'], //场次区分
                            'all_consume' => $all_consume,
                        ]);


                    } else {
                        if( $all_consume > $redpacketInfo['all_consume']) {
                            $res = $redpacketLog->where(['id' => $redpacketInfo['id']])->save(['all_consume' => $all_consume]);
                        }
                    }
                }


                // var_dump($gold_amount, $val['value'], $val['screenings'], $val['level']);exit;
                //关卡判断

            }

            //这里开始记录用户关卡
            if( $level > 0 ) {
                $levelModel = D('TaskLevel');
                $levelInfo = $levelModel->where(['uid' => $uid, 'screenings' => $screenings, 'from_id' => $from_id, 'task_time' => $task_time])->find();
                if(empty($levelInfo)) {
                    //新增
                    $levelData = [
                        'uid' => $uid,
                        'level' => $level,
                        'task_config_id' => $task_config_id,
                        'value' => $gold_amount,
                        'addtime' => time(),
                        'updatetime' => time(),
                        'screenings' => $screenings,
                        'from_id' => $from_id,
                        'task_time' => $task_time,
                    ];
                    $levelModel->add($levelData);
                } else if(!empty($levelInfo) && $levelInfo['value'] < $gold_amount ) {
                    //更新
                    $levelModel->where(['id' => $levelInfo['id']])->save([
                            'level' => $level,
                            'task_config_id' => $task_config_id,
                            'updatetime' => time(),
                            'value' => $gold_amount,
                        ]);
                }
            }

            echo "ok";

        }
    }

    // public function channelstat() {

    //     //$flist = F('NIUNIUSTAT');
    //     $fromkeyStat = D('TaskStat');
    //     $taskConfig = D('TaskConfig');
    //     $taskRecord = D('TaskRecord');
    //     $fromKey = D('DFromkey');

    //     $from_id = I('from_id');
    //     $uid = I('uid');
    //     $char_id = I('char_id');
    //     $recharge_amount = I('recharge_amount'); //充值金额
    //     $task_time = I('task_time'); //期数
    //     // $screenings = I('type'); //场次 1红包场，2休闲场

    //     writeLog('channelstat::'. json_encode(I('')), 'info');

    //     if(I('gold_amount') > 0) {
    //         $gold_amount = I('gold_amount'); //休闲场
    //         $screenings = 2;
    //     } else if(I('gold_hb') > 0) {
    //         $gold_amount = I('gold_hb'); //红包
    //         $screenings = 1;
    //     } else {
    //         $screenings = 0;
    //     }

    //     $map['from_id'] = $newMap['from_id'] = $from_id;
    //     $map['task_time'] = $task_time;
    //     $key = 'FROM_'.$from_id."_TASK_".$task_time;
    //     $map['screenings'] = ['in' ,[0,$screenings]]; //包含充值的查询 //场次

    //     $flist = $taskConfig->where($map)->select();  //查询渠道配置内容
    //     unset($map['screenings']);

    //     $map['uid'] = $recordMap['uid'] = $uid;
    //     $map['char_id'] = $recordMap['char_id'] = $char_id;

    //     $info = $fromkeyStat->where($map)->getField('task_config_id', true); //查询task_stat表的数据

    //     $taskRecordInfo = $taskRecord->where($map)->find(); //查询充值金币消耗等数据

    //     //TODO 修改计算当日月收益的总消耗
    //     //查询当日的数据
    //     $redpacketLog = D('tasks');

    //     $redMap['date_at'] = date("Y-m-d");
    //     $redMap['task_time'] = $task_time;
    //     $redMap['from_id'] = $from_id;
    //     // $redMap['screenings'] = $screenings;//场次区分
    //     $redpacketInfo = $redpacketLog->where($redMap)->find();

    //     $from_key = $fromKey->where(['from_id' => $from_id, 'num' => $task_time])->find();

    //     //查询用户数据
    //     $user_from_reg = D('User')->where(['adddate' => ['BETWEEN', [$from_key['start_time'], $from_key['end_time']]], 'id' => $uid])->find();

    //     //遍历$flist 配置的内容
    //     if(!empty($flist)) {
    //         $all_consume = 0;
    //         $level = 0;  //关卡
    //         foreach ($flist as $val ) {

    //             //不是在本期注册的用户不进新手金币奖励
    //             if(!$user_from_reg && $val['type'] == 1 && $val['condition'] == 2) {
    //                 continue;
    //             }

    //             if($user_from_reg && $val['type'] == 2 && $val['condition'] == 3) {
    //                 //是新用户就跳过 老用户充值奖励设置
    //                 continue;
    //             }

    //             //是老用户，跳过新注册用户充值奖励设置
    //             if(!$user_from_reg && $val['type'] == 2 && $val['condition'] == 2) {
    //                 //是新用户就跳过 老用户充值奖励设置
    //                 continue;
    //             }


    //             if($val['type'] == 1 && $gold_amount >= $val['value']) {  //金币的

    //                 //检查是否已有这条数据
    //                 if(!in_array($val['id'], $info)) {
    //                     //插入数据
    //                     $taskStatData = [
    //                         'task_config_id' => $val['id'],
    //                         'uid' => $uid,
    //                         'char_id' => $char_id,
    //                         'from_id' => $from_id,
    //                         'task_time' => $task_time,
    //                         'addtime' => time(),
    //                     ];
    //                     $fromkeyStat->add($taskStatData);
    //                     //累计消耗
    //                     $all_consume = $all_consume + $val['reward'];
    //                 }
    //             }

    //             if($val['type'] == 2 && $recharge_amount >= $val['value']) {  //充值的
    //                 //检查是否已有这条数据
    //                 if(!in_array($val['id'], $info)) {
    //                     //插入数据
    //                     $taskStatData = [
    //                         'task_config_id' => $val['id'],
    //                         'uid' => $uid,
    //                         'char_id' => $char_id,
    //                         'from_id' => $from_id,
    //                         'task_time' => $task_time,
    //                         'addtime' => time(),
    //                     ];
    //                     $fromkeyStat->add($taskStatData);
    //                     //累计消耗
    //                     $all_consume = $all_consume + $val['reward'];
    //                 }
    //             }

    //             //关卡判断
    //             if($gold_amount >= $val['value'] && $val['screenings'] > 0 && $val['level'] > 0) {
    //                 $level = $val['level'];
    //                 $task_config_id = $val['id'];
    //             }
    //         }

    //         //处理总消耗
    //         if(!empty($taskRecordInfo) ) {
    //             $consume = $all_consume - $taskRecordInfo['consume']; //计算差值消耗
    //             //加到当日消耗数据
    //             $redpacketData['all_consume'] = $redpacketInfo['all_consume'] + $all_consume;
    //             $taskRecord->where(['id' => $taskRecordInfo['id']])->save([
    //                 'recharge_amount' => $recharge_amount,
    //                 'gold_amount' => $gold_amount,
    //                 'consume' => $redpacketData['all_consume'],
    //             ]);

    //         } else {
    //             //直接加入今日消耗
    //             $redpacketData['all_consume'] = $redpacketInfo['all_consume'] + $all_consume;
    //             //插入记录数据
    //             $taskRecord->add([
    //                 'uid' => $uid,
    //                 'char_id' => $char_id,
    //                 'recharge_amount' => $recharge_amount,
    //                 'gold_amount' => $gold_amount,
    //                 'consume' => $all_consume,
    //                 'from_id' =>$from_id,
    //                 'task_time' => $task_time,
    //                 'screenings' => $screenings, //场次区分
    //                 'addtime' => time(),
    //             ]);
    //         }

    //         //记录总消耗
    //         if(empty($redpacketInfo)) {
    //             $res = $redpacketLog->add([
    //                 'date_at' =>date("Y-m-d"),
    //                 'task_time' => $task_time,
    //                 'from_id' =>$from_id,
    //                 // 'screenings' => $screenings, //场次区分
    //                 'all_consume' => $redpacketData['all_consume'],
    //             ]);

    //         } else {
    //             if(isset($redpacketData['all_consume'])) {
    //                 $res = $redpacketLog->where(['id' => $redpacketInfo['id']])->save($redpacketData);
    //             }

    //         }
    //     }

    //     //这里开始记录用户关卡
    //     if( $level > 0 ) {
    //         $levelModel = D('TaskLevel');
    //         $levelInfo = $levelModel->where(['uid' => $uid, 'screenings' => $screenings, 'from_id' => $from_id, 'task_time' => $task_time])->find();
    //         if(empty($levelInfo)) {
    //             //新增
    //             $levelData = [
    //                 'uid' => $uid,
    //                 'level' => $level,
    //                 'task_config_id' => $task_config_id,
    //                 'value' => $gold_amount,
    //                 'addtime' => time(),
    //                 'updatetime' => time(),
    //                 'screenings' => $screenings,
    //                 'from_id' => $from_id,
    //                 'task_time' => $task_time,
    //             ];
    //             $levelModel->add($levelData);
    //         } else if(!empty($levelInfo) && $levelInfo['value'] < $gold_amount ) {
    //             //更新
    //             $levelModel->where(['id' => $levelInfo['id']])->save([
    //                     'level' => $level,
    //                     'task_config_id' => $task_config_id,
    //                     'updatetime' => time(),
    //                     'value' => $gold_amount,
    //                 ]);
    //         }
    //     }

    //     echo "ok";
    // }


    //获取游戏数据保存mysql
    public function getGmOprate()
    {
        $uid = I('uid');
        $json_data = I('json_data');
        $level = I('user_level');

        $model = M('gameStat');
        // writeLog('getGmOprate::'. json_encode(I('')), 'info');
        if($uid) {
            $data_arr = json_decode($json_data, true);
            if(count($data_arr) == 0) {
                echo 'json_data error';exit;
            }
            foreach ($data_arr as $k => $val) {
                //循环插入数据或者更新数据
                //先查询是否有本条数据
                $find = $model->where(['uid'=> $uid, 'game_type' => $val['game_type']])->find();
                $val['user_level'] = $level;
                if($find) {
                    //更新操作
                    $model->where(['id' => $find['id']])->save($val);
                } else {
                    $val['uid'] = $uid;
                    $model->add($val);
                }
            }
            echo "ok";exit;
        } else {
            echo 'params error';exit;
        }
    }

    public function test() {

        writeLog('channelstat::'. json_encode(I('')), 'info');

        //$flist = F('NIUNIUSTAT');
        $fromkeyStat = D('TaskStat');
        $taskConfig = D('TaskConfig');
        $taskRecord = D('TaskRecord');
        $fromKey = D('DFromkey');

        $from_id = I('from_id');
        $uid = I('uid');
        $char_id = I('char_id');
        $recharge_amount = I('recharge_amount'); //充值金额
        $task_time = I('task_time'); //期数
        // $screenings = I('type'); //场次 1红包场，2休闲场

        if(I('gold_amount') > 0) {
            $gold_amount = I('gold_amount'); //休闲场
            $screenings = 2;
        } else if(I('gold_hb') > 0) {
            $gold_amount = I('gold_hb'); //红包
            $screenings = 1;
        } else {
            $screenings = 0;
        }

        $map['from_id'] = $newMap['from_id'] = $from_id;
        $map['task_time'] = $task_time;
        $key = 'FROM_'.$from_id."_TASK_".$task_time;
        $map['screenings'] = ['in' ,[0,$screenings]]; //包含充值的查询 //场次

        $flist = $taskConfig->where($map)->select();  //查询渠道配置内容
        unset($map['screenings']);

        $map['uid'] = $recordMap['uid'] = $uid;
        $map['char_id'] = $recordMap['char_id'] = $char_id;

        $info = $fromkeyStat->where($map)->getField('task_config_id', true); //查询task_stat表的数据
        // $taskRecordInfo = $taskRecord->where($map)->find(); //查询充值金币消耗等数据

        //TODO 修改计算当日月收益的总消耗
        //查询当日的数据
        $redpacketLog = D('tasks');

        $from_key = $fromKey->where(['from_id' => $from_id, 'num' => $task_time])->find();

        //查询用户数据
        $user_from_reg = D('User')->where(['adddate' => ['BETWEEN', [$from_key['start_time'], $from_key['end_time']]], 'id' => $uid])->find();

        // var_dump($flist);exit;
        //遍历$flist 配置的内容
        if(!empty($flist)) {
            // $all_consume = 0;
            $level = 0;  //关卡
            foreach ($flist as $val ) {

                //关卡不跳过
                if($gold_amount >= $val['value'] && $val['screenings'] > 0 && $val['level'] > 0) {
                    $level = $val['level'];
                    $task_config_id = $val['id'];

                }


                //不是在本期注册的用户不进新手金币奖励
                if(!$user_from_reg && $val['type'] == 1 && $val['condition'] == 2) {
                    continue;
                }

                if($user_from_reg && $val['type'] == 2 && $val['condition'] == 3) {
                    //是新用户就跳过 老用户充值奖励设置
                    continue;
                }

                //是老用户，跳过新注册用户充值奖励设置
                if(!$user_from_reg && $val['type'] == 2 && $val['condition'] == 2) {
                    //是新用户就跳过 老用户充值奖励设置
                    continue;
                }

                $all_consume = 0;
                if($val['type'] == 1 && $gold_amount >= $val['value']) {  //金币的

                    //检查是否已有这条数据
                    if(!in_array($val['id'], $info)) {
                        //插入数据
                        $taskStatData = [
                            'task_config_id' => $val['id'],
                            'uid' => $uid,
                            'char_id' => $char_id,
                            'from_id' => $from_id,
                            'task_time' => $task_time,
                            'addtime' => time(),
                        ];
                        $fromkeyStat->add($taskStatData);
                        //累计消耗
                        $all_consume = $val['reward'];
                    }
                }


                if($val['type'] == 2 && $recharge_amount >= $val['value']) {  //充值的
                    //检查是否已有这条数据
                    if(!in_array($val['id'], $info)) {
                        //插入数据
                        $taskStatData = [
                            'task_config_id' => $val['id'],
                            'uid' => $uid,
                            'char_id' => $char_id,
                            'from_id' => $from_id,
                            'task_time' => $task_time,
                            'addtime' => time(),
                        ];
                        $res = $fromkeyStat->add($taskStatData);
                        //累计消耗
                        $all_consume = $val['reward'];
                    }
                }
                $redMap = [];
                $redMap['date_at'] = date("Y-m-d");
                $redMap['task_time'] = $task_time;
                $redMap['from_id'] = $from_id;
                $redMap['screenings'] = $val['screenings'];//场次区分
                $redpacketInfo = $redpacketLog->where($redMap)->find();

                if($all_consume > 0 ) {
                    $all_consume = bcadd($redpacketInfo['all_consume'], $all_consume ,1);
                     //记录总消耗
                    if(empty($redpacketInfo)) {
                        $res = $redpacketLog->add([
                            'date_at' =>date("Y-m-d"),
                            'task_time' => $task_time,
                            'from_id' =>$from_id,
                            'screenings' => $val['screenings'], //场次区分
                            'all_consume' => $all_consume,
                        ]);


                    } else {
                        if( $all_consume > $redpacketInfo['all_consume']) {
                            $res = $redpacketLog->where(['id' => $redpacketInfo['id']])->save(['all_consume' => $all_consume]);
                        }
                    }
                }


                // var_dump($gold_amount, $val['value'], $val['screenings'], $val['level']);exit;
                //关卡判断

            }

            //这里开始记录用户关卡
            if( $level > 0 ) {
                $levelModel = D('TaskLevel');
                $levelInfo = $levelModel->where(['uid' => $uid, 'screenings' => $screenings, 'from_id' => $from_id, 'task_time' => $task_time])->find();
                if(empty($levelInfo)) {
                    //新增
                    $levelData = [
                        'uid' => $uid,
                        'level' => $level,
                        'task_config_id' => $task_config_id,
                        'value' => $gold_amount,
                        'addtime' => time(),
                        'updatetime' => time(),
                        'screenings' => $screenings,
                        'from_id' => $from_id,
                        'task_time' => $task_time,
                    ];
                    $levelModel->add($levelData);
                } else if(!empty($levelInfo) && $levelInfo['value'] < $gold_amount ) {
                    //更新
                    $levelModel->where(['id' => $levelInfo['id']])->save([
                            'level' => $level,
                            'task_config_id' => $task_config_id,
                            'updatetime' => time(),
                            'value' => $gold_amount,
                        ]);
                }
            }

            echo "ok";

        }
    }



}