<?php
class Withdraw
{
    public $mchid = '1645994839';
    public $appid = 'wx26a222fe8ebdd3f3';
    public $serial_no = '6457F76E451EC4A1246059BFAF4180149CEAD17E'; // 证书序列号，不知道怎么获取的看上面的图


    public function transfer($openid, $trade_no, $money, $desc='备注说明')
    {
        $money = (int)($money * 100);

        $post_data = [
            "appid" => $this->appid,//appid
            "out_batch_no" => $trade_no,//商家批次单号
            "batch_name" => $desc,//批次名称
            "batch_remark" => $desc,//批次备注
            "total_amount" => $money,// 转账金额单位为“分”
            "total_num" => 1, // 转账总笔数
            //此处可以多笔提现  组合二维数组放到transfer_detail_list即可   我这里单笔操作，写死了
            "transfer_detail_list" => [
                [
                    'out_detail_no' => $trade_no,
                    'transfer_amount' => $money,
                    'transfer_remark' => $desc,
                    'openid' => $openid,
                ]
            ]
        ];
        $url = 'https://api.mch.weixin.qq.com/v3/transfer/batches';
        //JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE  防止中文被转义
        $result = $this->wx_post($url, json_encode($post_data, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));

        $result = json_decode($result, true);
        if (isset($result['prepay_id'])) {
            return $result['prepay_id'];
        }
        return $result;
    }

    /**post请求
     * @param $url
     * @param $param
     * @return bool|string
     */
    private function wx_post($url, $param)
    {
        $authorization = $this->getV3Sign($url, "POST", $param);
        $curl = curl_init();
        $headers = [
            'Authorization:' . $authorization,
            'Accept:application/json',
            'Content-Type:application/json;charset=utf-8',
            'User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
        ];
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
        curl_setopt($curl, CURLOPT_POST, true);
        $res = curl_exec($curl);
        curl_close($curl);
        return $res;
    }

    /**
     * 微信提现V3签名
     * @param $url
     * @param $http_method
     * @param $body
     * @return string
     */
    private function getV3Sign($url, $http_method, $body)
    {
        $nonce = strtoupper($this->createNonceStr(32));
        $timestamp = time();
        $url_parts = parse_url($url);
        $canonical_url = ($url_parts['path'] . (!empty($url_parts['query']) ? $url_parts['query'] : ""));
        // $cert_dir = ROOT_PATH.DS."config".DS."payment_cert".DS."wechatpay".DS;
        $cert_dir = dirname(__FILE__).'/pem/';
        $sslKeyPath = $cert_dir."apiclient_wx_key.pem";
        //拼接参数
        $message = $http_method . "\n" .
            $canonical_url . "\n" .
            $timestamp . "\n" .
            $nonce . "\n" .
            $body . "\n";
        $private_key = $this->getPrivateKey($sslKeyPath);
        openssl_sign($message, $raw_sign, $private_key, 'sha256WithRSAEncryption');
        $sign   = base64_encode($raw_sign);
        $token = sprintf('WECHATPAY2-SHA256-RSA2048 mchid="%s",nonce_str="%s",timestamp="%s",serial_no="%s",signature="%s"', $this->mchid, $nonce, $timestamp, $this->serial_no, $sign);
        return $token;
    }

    /**
     * 生成随机32位字符串
     * @param $length
     * @return string
     */
    public function createNonceStr($length = 16) { //生成随机16个字符的字符串
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    /**
     * 获取私钥
     * @param $filepath
     * @return false|resource
     */
    private function getPrivateKey($filepath = '')
    {
        if (empty($filepath)) {
            //私钥位置
            // $cert_dir = ROOT_PATH.DS."config".DS."payment_cert".DS."wechatpay".DS;
            $cert_dir = dirname(__FILE__).'/pem/';
            $filepath = $cert_dir."apiclient_wx_key.pem";
        }

        return openssl_get_privatekey('-----BEGIN PRIVATE KEY-----
        MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDBxkL0GceopGW7
        TAWJEViPhCPyAEl8+hBqyzck4O7yjUHfVYDlCJb/ngostGlHykiNRZg5Ap89OSvF
        OqqW06UILgKQSrJx7gNs28g1c+73LakWxlJHolH288yPaS7kjfuJ5l7OuzY6XDHo
        6yAqsfBho7jWyFrEmH7NzsD4Xf/xzLyY1el7PECkbk0lQbSo4GpSN5LEfgXfrwUf
        nMa61dCoVKfhKeX4FmmqFsFQbdnIxFRhW6jfYTMPaVBjYsG+hnlCQbbGgqgTKF7x
        MGEohRJnLTTuAzbTM26yVd3YPVfR6W00bkOZ/2V/cVaJX9gZEdt1hrlF5WUNGzqV
        rZdCOp29AgMBAAECggEBAKZbV8/I8ihOOyh5M6dhRO6xO0rgBDdPWBjXLhhzI7EI
        pKzf8zCuFzSMsZ9/o4MGnxLp1K3dGmitVG6W1y11thE3ruvBtUEqXbZvWYrKZTW/
        9ZKf4LOvHz8sP0o/uwps+q6BIJPLFds0m4Sjl5IQAZdRv7Zle8ecRADcp76StNEY
        vUWhP3kUEiKZAabuG5izleaBhsZidhQMnioqW7I0fkiGbzoAMEXDC8N9Mmm6cmRF
        goscHza8+gt7dhGR+j/wH7jkBjBPmJ7ytjQfJ20nFoARlonx7wvq30Dlood95rRS
        URleBqs3fxcrpxXMWzeyTi8YEqtWWoZL5q3tqyvBBQECgYEA6lR6mqpVhRQWOln/
        2fO7T88b6Eg3T2spiqNbT6CkO8rDDNL6Pu+7xArsy5XjdNsilUb/Kb7PmZoMOPqN
        VdNu6M+fnXgi47pgC2mHwzjeGxjSoycMZnHw7W+TIxiwPWmLr1nSHKUCOCbPPWB0
        lDd7nSoBLnbIQ237AN/x6/yOi8kCgYEA07GsDk4Qbqg1QX7IRHRbYc9U+L7Yi+kZ
        CIXmOHlOOdfVIOVGgFACoA8pbGWiU5uWSTN7t5ICCPL0U1+f0tksKwiCufSMpMt2
        ldG+jMWwobvhZpdHN3lZBaE7fRBIt1E2E9mYQdnOgIahTjAS+QZTWZ/6KMTO2FPb
        ssR0iexmlFUCgYBoIzT9aTdqVdvvALIwZR3YQP1xBC74b3Jdt21L/OMf42M9XsFx
        mM7EN+q2Kx2TgBNlbdDV94OBCrHl3ItVGgqPEj4oGAzBUcF/MN3xx2MFfcmWUjMz
        j+13R3+HOGCSWkaGng8/RaGv3Kd66lPEzsdzuAggYw9q0ASdheiYbHvY6QKBgQC+
        xOcLQRgX0GelrQ3meoD7RK8LNljIxHe1sYzFKbPv9mtvKI5/BRtIiEFJGNHOzHAD
        aWy9Acvpqp+5HnxVszXVVecQkqvHuj8URh6Ul3ZUWY6HOB6KhEVQCHsmee09nebA
        vp8yAIjSVAC7CCGg3i1wn/T8llbcpb1ffSCURkAuvQKBgQCEd2E9HpzkT7hH2aVE
        fsSW0l6Eeb3Kl8clTPuU1hMOmfIw0p9A7fcjzj6GwxKvxrSK9d180NzAHqlvAbZI
        J4QCHvoxWwiPOGXXHDfUKayI8dz0JQEi5l5yRygXNbwKo7GyKJe9GiElXcEtYMqV
        KH24DeGUm/rKmHvjYjcK5vI9qQ==
        -----END PRIVATE KEY-----
');
    }
}