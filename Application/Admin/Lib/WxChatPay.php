<?php
namespace pay;
use \think\Db;
 
/**
 * 微信支付类
 */
class Weinxinpay {
	//是否沙盒环境
	private $is_sandbox = false;
	//沙盒地址
	private $sandurl = 'https://api.mch.weixin.qq.com/sandboxnew/pay';
	//正式地址
	private $apiurl  = 'https://api.mch.weixin.qq.com/pay';
	private $mchid;//微信支付分配的商户账号PartnerID 通过微信支付商户资料审核后邮件发送
    private $appid;//微信支付分配的公众账号ID（企业号corpid即为此appid）
	private $appkey;//微信支付申请对应的公众号的APP Key
    private $apikey;//微信支付分配的商户密钥
	private $wapname;//wap网站名
	private $wapurl; //wap网站域名（网址）  
	//网关地址
	private $gateway;
	//构造方法
	public function __construct($account=null){
	    if($account!=null){
			$this->mchid = $account['mchid'];
			$this->appid = $account['appid'];
			$this->apikey = $account['apikey'];
			if(isset($account['appkey'])){
				$this->appkey = $account['appkey'];
			}
			if(isset($account['wapname'])){
				$this->wapname = $account['wapname'];
			}
			if(isset($account['wapurl'])){
				$this->wapurl = $account['wapurl'];
			}
	    }
		//如果是沙箱测试
    	if($this->is_sandbox){
			$this->gateway = $this->sandurl;
		}else{
			$this->gateway = $this->apiurl;
		}
	}
	/**
	 *
	 * 1.发起原生支付
	 * $params 传输的数据
	 */
	public function nativePay($params){
		//商品名称
		$subject=isset($params['subject']) && !empty($params['subject']) ? $params['subject'] : "购买商品";
		$config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->appid,
            'key' => $this->apikey,
        );
        $unified = array(
            'appid' => $config['appid'],
            'attach' => 'pay', //商家数据包，原样返回，如果填写中文，请注意转换为utf-8
            'body' => $subject,//商品名称（商品简单描述）
            'mch_id' => $config['mch_id'],
            'nonce_str' => self::createNonceStr(),//随机字符串，长度要求在32位以内
            'notify_url' => $params['notify_url'],//异步通知地址
            'out_trade_no'=> $params['pay_id'],//唯一标识，订单编号（必须）
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
            'total_fee' => floatval($params['money']) * 100,//订单金额，单位 转为分
            'trade_type' => 'NATIVE',
        );
        $unified['sign'] = self::getSign($unified, $config['key']);
        $responseXml = self::curl_post($this->gateway.'/unifiedorder', self::arrayToXml($unified));
		//禁止引用外部xml实体
		libxml_disable_entity_loader(true);        
        $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($unifiedOrder === false) {
            die('parse xml error');
        }
        if ($unifiedOrder->return_code != 'SUCCESS') {
            die($unifiedOrder->return_msg);
        }
        if ($unifiedOrder->result_code != 'SUCCESS') {
            die($unifiedOrder->err_code);
        }
        $codeUrl = (array)($unifiedOrder->code_url);
        if(!$codeUrl[0]) exit('get code_url error');
		$timestamp=time();//付款时间
        $arr = array(
            "appId" => $config['appid'],
            "timeStamp" => "$timestamp",//这里是字符串的时间戳，不是int，所以需加引号
            "nonceStr" => self::createNonceStr(),//随机字符串，长度要求在32位以内
            "package" => "prepay_id=" . $unifiedOrder->prepay_id,
            "signType" => 'MD5',
            "code_url" => $codeUrl[0],
        );
        $arr['paySign'] = self::getSign($arr, $config['key']);
        return $arr;
	}
	/**
	 *
	 * 2.发起公众号支付
	 * $openId
	 * $params 传输的数据
	 */
	public function jsapiPay($openId,$params){	
		//商品名称
		$subject=isset($params['subject']) && !empty($params['subject']) ? $params['subject'] : "购买商品";		
		$config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->appid,
            'key' => $this->apikey,
        );
        //$orderName = iconv('GBK','UTF-8',$orderName);
        $unified = array(
            'appid' => $config['appid'],
            'attach' => 'pay', //商家数据包，原样返回，如果填写中文，请注意转换为utf-8
            'body' => $subject,//商品名称（商品简单描述）
            'mch_id' => $config['mch_id'],
            'nonce_str' => self::createNonceStr(),//随机字符串，长度要求在32位以内
            'notify_url' => $params['notify_url'],//异步通知地址
            'openid' => $openid, //rade_type=JSAPI，此参数必传
            'out_trade_no' => $params['pay_id'],//唯一标识，订单编号（必须）
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
            'total_fee' => floatval($params['money']) * 100,//订单金额，单位 转为分
            'trade_type' => 'JSAPI',
        );
        $unified['sign'] = self::getSign($unified, $config['key']);
        $responseXml = self::curl_post($this->gateway.'/unifiedorder', self::arrayToXml($unified));
		//禁止引用外部xml实体
		libxml_disable_entity_loader(true);	    
        $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($unifiedOrder === false) {
            die('parse xml error');
        }
        if ($unifiedOrder->return_code != 'SUCCESS') {
            die($unifiedOrder->return_msg);
        }
        if ($unifiedOrder->result_code != 'SUCCESS') {
            die($unifiedOrder->err_code);
        }
		$timestamp=time();//付款时间
        $arr = array(
            "appId" => $config['appid'],
            "timeStamp" => "$timestamp",//这里是字符串的时间戳，不是int，所以需加引号
            "nonceStr" => self::createNonceStr(),//随机字符串，长度要求在32位以内
            "package" => "prepay_id=" . $unifiedOrder->prepay_id,
            "signType" => 'MD5',
        );
        $arr['paySign'] = self::getSign($arr, $config['key']);
        return $arr;
	}
	/**
	 *
	 * 3.发起H5支付
	 * $params 传输的数据
	 */
	public function h5Pay($params){	
		//商品名称
		$subject=isset($params['subject']) && !empty($params['subject']) ? $params['subject'] : "购买商品";
		$config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->appid,
            'key' => $this->apikey,
        );
        $scene_info = array(
            'h5_info' =>array(
                'type'=>'Wap',
                'wap_url'=>$this->wapurl,
                'wap_name'=>$this->wapname,
            )
        );
        $unified = array(
            'appid' => $config['appid'],
            'attach' => 'pay',//商家数据包，原样返回，如果填写中文，请注意转换为utf-8
            'body' => $subject,//商品名称（商品简单描述）
            'mch_id' => $config['mch_id'],
            'nonce_str' => self::createNonceStr(),//随机字符串，长度要求在32位以内
            'notify_url' => $params['notify_url'],//异步通知地址
            'out_trade_no' => $params['pay_id'],//唯一标识，订单编号（必须）
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
            'total_fee' => floatval($params['money']) * 100,//订单金额，单位 转为分
            'trade_type' => 'MWEB',
            'scene_info'=>json_encode($scene_info)
        );
        $unified['sign'] = self::getSign($unified, $config['key']);
        $responseXml = self::curl_post($this->gateway.'/unifiedorder', self::arrayToXml($unified));
        $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($unifiedOrder->return_code != 'SUCCESS') {
            die($unifiedOrder->return_msg);
        }
        if($unifiedOrder->mweb_url){
            return $unifiedOrder->mweb_url.'&redirect_url='.urlencode($params['return_url']);
        }
        exit('error');
	}
	/**
	 * 异步通知
	 * @param $data 通知的数据
	 */
	public function notify()
    {
        $config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->appid,
            'key' => $this->apikey,
        );
        $postStr = file_get_contents('php://input');
		//禁止引用外部xml实体
		libxml_disable_entity_loader(true);        
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($postObj === false) {
            die('parse xml error');
        }
        if ($postObj->return_code != 'SUCCESS') {
            die($postObj->return_msg);
        }
        if ($postObj->result_code != 'SUCCESS') {
            die($postObj->err_code);
        }
        $arr = (array)$postObj;
        unset($arr['sign']);
        if (self::getSign($arr, $config['key']) == $postObj->sign) {
            echo '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
            return $arr;
        }
    }
	/**
     * 通过跳转获取用户的openid，跳转流程如下：
     * 1、设置自己需要调回的url及其其他参数，跳转到微信服务器https://open.weixin.qq.com/connect/oauth2/authorize
     * 2、微信服务处理完成之后会跳转回用户redirect_uri地址，此时会带上一些参数，如：code
     * @return 用户的openid
     */
    public function getOpenid()
    {
        //通过code获得openid
        if (!isset($_GET['code'])){
            //触发返回code码
            $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https://' : 'http://';
			$uri = $_SERVER['PHP_SELF'].$_SERVER['QUERY_STRING'];
			if($_SERVER['REQUEST_URI']) $uri = $_SERVER['REQUEST_URI'];
            $baseUrl = urlencode($scheme.$_SERVER['HTTP_HOST'].$uri);
            $url = $this->createOauthUrlForCode($baseUrl);
            Header("Location: $url");
            exit();
        } else {
            //获取code码，以获取openid
            $code = $_GET['code'];
            $openid = $this->getOpenidFromMp($code);
            return $openid;
        }
    }
	/**
     * 通过code从工作平台获取openid机器access_token
     * @param string $code 微信跳转回来带上的code
     * @return openid
     */
    public function getOpenidFromMp($code)
    {
        $url = $this->createOauthUrlForOpenid($code);
        $res = self::curl_get($url);
        //取出openid
        $data = json_decode($res,true);
        $this->data = $data;
        $openid = $data['openid'];
        return $openid;
    }
    /**
     * 构造获取open和access_toke的url地址
     * @param string $code，微信跳转带回的code
     * @return 请求的url
     */
    private function createOauthUrlForOpenid($code)
    {
        $urlObj["appid"] = $this->appid;
        $urlObj["secret"] = $this->appkey;
        $urlObj["code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/sns/oauth2/access_token?".$bizString;
    }
	/**
     * 构造获取code的url连接
     * @param string $redirectUrl 微信服务器回跳的url，需要url编码
     * @return 返回构造好的url
     */
    private function createOauthUrlForCode($redirectUrl)
    {
        $urlObj["appid"] = $this->appid;
        $urlObj["redirect_uri"] = "$redirectUrl";
        $urlObj["response_type"] = "code";
        $urlObj["scope"] = "snsapi_base";
        $urlObj["state"] = "STATE"."#wechat_redirect";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://open.weixin.qq.com/connect/oauth2/authorize?".$bizString;
    }
	protected static function createNonceStr($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }
	/**
     * 拼接签名字符串
     * @param array $urlObj
     * @return 返回已经拼接好的字符串
     */
    private function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v)
        {
            if($k != "sign") $buff .= $k . "=" . $v . "&";
        }
        $buff = trim($buff, "&");
        return $buff;
    }
	/**
     * 获取签名
     */
    protected static function getSign($params, $key)
    {
        ksort($params, SORT_STRING);
        $unSignParaString = self::formatQueryParaMap($params, false);
        $signStr = strtoupper(md5($unSignParaString . "&key=" . $key));
        return $signStr;
    }
    protected static function formatQueryParaMap($paraMap, $urlEncode = false)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if (null != $v && "null" != $v) {
                if ($urlEncode) {
                    $v = urlencode($v);
                }
                $buff .= $k . "=" . $v . "&";
            }
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
	protected static function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
        }
        $xml .= "</xml>";
        return $xml;
    }
	/**
	 * file_get_contents发送post请求
	 * @param url       请求地址
	 * @param postData  要传递的post数据
	 */
	protected function file_post($url, $post_data) {
		$postdata = http_build_query($post_data);
		$options = array('http' => array('method' => 'POST', 'header' => 'Content-type:application/x-www-form-urlencoded', 'content' => $postdata, 'timeout' => 300
		// 超时时间（单位:s）
		));
		$context = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		//去空格
		$result = trim($result);
		//转换字符编码
		$result = mb_convert_encoding($result, 'utf-8', 'UTF-8,GBK,GB2312,BIG5');
		//解决返回的json字符串中返回了BOM头的不可见字符（某些编辑器默认会加上BOM头）
		$result = trim($result,chr(239).chr(187).chr(191));
		return $result;
	}
	public static function curl_get($url = '', $options = array())
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
	/**
	 * curl发送post请求
	 * @param url       请求地址
	 * @param postData  要传递的post数据
	 */	
	protected function curl_post($url = '', $postData = '', $options = array())
    {
        if (is_array($postData)) {
            $postData = http_build_query($postData);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置cURL允许执行的最长秒数
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
	/**
     * 建立请求，以表单HTML形式构造（默认）
	 * @param $url 请求地址
     * @param $params 请求参数数组
     * @return 提交表单HTML文本
     */
    protected function buildRequestForm($url,$params) {
 
        $sHtml = "正在跳转至支付页面...<form id='alipaysubmit' name='alipaysubmit' action='".$url."?charset=".$this->charset."' method='POST'>";
		foreach($params as $key=>$val){
            if (false === $this->checkEmpty($val)) {
                $val = str_replace("'","&apos;",$val);
                $sHtml.= "<input type='hidden' name='".$key."' value='".$val."'/>";
            }	
		}
        //submit按钮控件请不要含有name属性
        $sHtml = $sHtml."<input type='submit' value='ok' style='display:none;''></form>";
        $sHtml = $sHtml."<script>document.forms['alipaysubmit'].submit();</script>";
        return $sHtml;
    }
}
?>
