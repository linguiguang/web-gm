<?php



/**
 * 签名效验
 * Class SignValidateService
 * @package app\common\server
 */
class RsaService
{

    protected $PublicKey;    
    protected $PrivateKey;
    protected $charset="utf-8";
    protected $error = null;

    /**
     * 初始化配置
     */
    public function __construct($PublicKey = '', $PrivateKey = '')
    {
        $this->charset = 'utf-8';
        $this->PublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3pNGYc01AvA7qiXXqDebkVMGjNE9RfXJlj5ae1QXNYz/3KAORqqYtGCvuzvSV/P5e/6k3PcDzI7zKV68DsqGRqu0f80Y3Vpr54OUXMNrRBZ1QvZ0fDHcI6OCb0hYRjl7Gj/fvDQANNytgkZgYfKPGck9AuZxZOdskomwBn4ksrbTsV17lppyGX4xornvJX1EaKBx0e0Rk/4YE2ROjH3Yr2NCRQUOTjIpTwfUZGRjJ+iuJEuwThGIghT2tMZYvv+7LGAY97lmGzVn0SmwHsNxoIKbmc8orR4WIA+8TrEFftBeiAoR1hrweGUXhy+1Ru/t52aCbinkjvJRdAkz2ozonQIDAQAB";
        $this->PrivateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDek0ZhzTUC8DuqJdeoN5uRUwaM0T1F9cmWPlp7VBc1jP/coA5Gqpi0YK+7O9JX8/l7/qTc9wPMjvMpXrwOyoZGq7R/zRjdWmvng5Rcw2tEFnVC9nR8Mdwjo4JvSFhGOXsaP9+8NAA03K2CRmBh8o8ZyT0C5nFk52ySibAGfiSyttOxXXuWmnIZfjGiue8lfURooHHR7RGT/hgTZE6MfdivY0JFBQ5OMilPB9RkZGMn6K4kS7BOEYiCFPa0xli+/7ssYBj3uWYbNWfRKbAew3GggpuZzyitHhYgD7xOsQV+0F6IChHWGvB4ZReHL7VG7+3nZoJuKeSO8lF0CTPajOidAgMBAAECggEAZofVjkwntXjRI2pXVAh5w5JJnCjvXwEAohOLPvDX2DVh0FJzAR6pRb6cdVXzaczcCsCdVaT8vQjE/zPC0dV90aRbe9wRLjbvKjE08R6clTr7PaOmuWeGj2xNM9JRx9fG26q8+EfEKJXX4AFnePdx8GEBmApsBh2NqFuBGRyW7zPpEAN2Lwr0frJZcn9BAv2SjHX3Bc++y+rqTcj1N9x3kSPFjTSYoyWNpnpgOyh0q0ytHmbXn1J5cfKWX4tKt+hK5c4ewbSFSC5/tlbhXYPcGL4DeER6tVoNcC1/ZCuZ68Z/Z0YDdmLi+qIlPSMxEFYxxM8kR+lrbAGzwgk3oZ9pgQKBgQD/6oWIY86XPmQB6B2eFfHo/Xn9yVJTBfXznjdEmCqUZfL4npMNtyICnjuqcsvh7BZ4Y1Ck0v667lPQtHkgELycjfZ2e17ygE9xi30KmzTu5DHS7R1WZUpDOGRLgymc26O8ixXsDrZ2bg4zKlpjS7C+xfvOosMOsdcu/Op9wwnB8QKBgQDepfSB+WErCs0wsD4YXSUdF87c8grJ/PtRv/1OmTkJHo3kh+IsE2ghBqUic6IsTcUDGrZgaUygqlxkCXq84on8StzZ5KRVrtgfyASq51FO2iVdy4KQjES/tIM73jWqEk4IUnvzeW2FeZl9/FTaJryUeHYBQWghRPjPo3XMxrWlbQKBgDLeiCqDRFZ19WUXoYOB8zMP54bV1MJiBfCl+3BsGtKirThUNQDd7Sm+EDgiV3qR9il5/Fa3BiFcLFbo+Max7PvlkB8fCNGFRcVZJ8LK9SujOnAl59X7jlZsSCtmaAgg9JwOvY3NouFJoRL0kUD5k6loK1R4r/Mk0JQre+UBttfRAoGASCss3TBzcZs3tY97ITSYAh0kfXOrUZvB7ta3idGarj0MK0ZsZnMH7qSTxF4M9CVlNcNj2E5pey6dcgOsmCsqJPAzNCGrruZUwcB42tjN/ywwrn133djJtAYT/8TgSjdSudnauanjeT9YJIE1E904hTuirHeCBD6yE8PdPDowyOUCgYBwEk8dK5uLBfLL4z5hM1TGhtOgeLN5GxlN0GfMUamKVH97CbE815bAmLpw+KUS2hHSqO+DvMWnpY2VLEjC5uLkTY6ALH5zEuNYqxS1OsNXhcneUup/9GlGKdInj9pI18VBmkLl8J6IAb5Qv5j09X02ZiPf/VJaCHoie+JgnEsT9g==";
    }

    /**
     * @notes 获取错误信息
     * @return mixed
     * @author Tab
     * @date 2021/8/19 14:42
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     *  验证签名
     **/
    public function rsaCheck($params)
    {
        $sign = $params['sign'] ? $params['sign']:'';
        $signType = $params['sign_type'] ? $params['sign_type']:'RSA2';
        unset($params['sign']);
        return $this->verify($this->getSignContent($params), $sign, $signType);
    }

    public function generateSign($params, $signType = "RSA2")
    {
        return $this->sign($this->getSignContent($params), $signType);
    }

    protected function sign($data, $signType = "RSA2")
    {
        $priKey = $this->PrivateKey;
        $res = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($priKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";
           
        ($res) or die('您使用的私钥格式错误，请检查RSA私钥配置');
        try {
            if ("RSA2" == $signType) {
                openssl_sign($data, $sign, $res, version_compare(PHP_VERSION, '5.4.0', '<') ? SHA256 : OPENSSL_ALGO_SHA256); //OPENSSL_ALGO_SHA256是php5.4.8以上版本才支持
            } else {
                openssl_sign($data, $sign, $res);
            }
        } catch (Exception $e) {
            $this->error = '加签失败:' . $e->getMessage();
            return false;
        }
        $sign = base64_encode($sign);
        return $sign;
    }

    function verify($data, $sign, $signType = 'RSA2')
    {
        $pubKey = $this->PublicKey;
        $res = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($pubKey, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
        try {
            //调用openssl内置方法验签，返回bool值
            if ("RSA2" == $signType) {
                $result = (bool)openssl_verify($data, base64_decode($sign), $res, version_compare(PHP_VERSION, '5.4.0', '<') ? SHA256 : OPENSSL_ALGO_SHA256);
            } else {
                $result = (bool)openssl_verify($data, base64_decode($sign), $res);
            }
        } catch (Exception $e) {
            $this->error = '签名效验失败:' . $e->getMessage();
            return false;
        }
        return $result;

    }

    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value)
    {
        if (is_array($value))
            return true;
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;
        return false;
    }

    public function getSignContent($params)
    {
        ksort($params);
        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {
                // 转换成目标字符集
                $v = $this->characet($v, $this->charset);
                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }
        unset ($k, $v);
        return $stringToBeSigned;
    }

    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    function characet($data, $targetCharset)
    {
        if (!empty($data)) {
            $fileType = $this->charset;
            if (strcasecmp($fileType, $targetCharset) != 0) {
                $data = mb_convert_encoding($data, $targetCharset, $fileType);
            }
        }
        return $data;
    }
    
    public function curlPost($url = '', $postData = '', $options = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); //设置cURL允许执行的最长秒数
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        
        curl_close($ch);
        return $data;
    }

    
  //test
  public function testDo() 
  {
      $endpoint = "https://gateway.jxpays.com/pay.order/create";

      $data = array(
          "mchid"=>"880888119740251",
          "method"=>    "pay.order/create",
          "sign_type"=> "RSA2",
          "timestamp"=> time(),
          "version"=>   "1.0",
          "charset"=>   "utf-8"
      );      
      // $bizData = array("aa"=>1,"bb"=>2);
      $bizCnt = array(
          "trade_type"=>"alipayApp",
          "out_trade_no"=>"23206546644",
          "total_amount"=>"0.3",
          "subject"=>"rr4",
          "notify_url"=>"http://r455",
          "client_ip"=>"112.62.14.67"
      );
      $bizStr = json_encode($bizCnt);
      $data["biz_content"] = $bizStr;
      $sign = $this->generateSign($data);
      $data["sign"] = $sign;
      $pdata = $this->curlPost($endpoint, $data, []);

      return $pdata;
  }
    
    
}