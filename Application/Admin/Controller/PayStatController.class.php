<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/12
 * Time: 17:08
 */
namespace Admin\Controller;

use Think\Controller;

class PayStatController extends Controller{

    /**
     * 更新充值到userinfo表
     */
    public function pay(){

        $stime = microtime(true); #获取程序开始执行的时间

        //================================更新充值到userinfo表
        $Userinfo = D('userinfo');
        //总充值数据
        $paylogs = D('Paylog')->where(array('state' => 3))->field('sum(money) as pays,uid')->group('uid')->select();

        $ten_money_uid = array();
        foreach ($paylogs as $v) {
            //更新充值数据到userinfo表
            $Userinfo->where(array('uid' => intval($v['uid'])))->save(array('pays' => intval($v['pays'])));

            //累计充值超过10元角色ID
            if (intval($v['pays']) > 10) {
                $ten_money_uid[] = intval($v['uid']);
            }
        }

        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    /**
     * 统计各个渠道充值金额和充值超过10元,159,799用户数
     */
    public function pay_from_stat(){
        $stime = microtime(true); #获取程序开始执行的时间

        $d_time = NOW_TIME;

        $b_time = strtotime(date('Y-m-d 00:00:00', $d_time));
        $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));

        //重新跑一次昨天数据
        $yes_btime = strtotime(date('Y-m-d 01:00:00', $d_time));
        $yes_etime = strtotime(date('Y-m-d 02:00:00', $d_time));
        if($d_time > $yes_btime && $d_time < $yes_etime){
            $b_time = strtotime(date('Y-m-d 00:00:00', $b_time - 600));
            $e_time = $b_time + 86399;
        }

        $map['state'] = 3;
//        $map['addtime'] = array('between', array($b_time, $e_time));
        $paylogs = D('Paylog')->where($map)->field('money,uid,from_id,addtime')->select();

        $data = array();

        $Paystat = D('Paymoneystat');
        $dailywingold = D('Dailywingold');

        for($i=0; $i<=($e_time - 86399 - $b_time)/86400; $i++){
            $en_time = $b_time + $i*86400 + 86399;

            $dt = date('Y-m-d', $en_time);

            echo $dt."\r\n";

            foreach ($paylogs as $v){

                if($v['addtime'] > $en_time) continue;

                $data[$dt][$v['from_id']][$v['uid']] += intval($v['money']);

            }

        }

        foreach ($data as $k=>$v){
            $time = strtotime($k);

            foreach ($v as $key=>$val){

                $total_money = $ten_man = $_159_man = $_799_man = 0;

                foreach ($val as $m=>$n){

                    if(intval($n) > 10){//充值超过10元玩家
                        $ten_man += 1;
                    }

                    if(intval($n) > 159){//充值超过159元玩家
                        $_159_man += 1;
                    }

                    if(intval($n) > 799){//充值超过799元玩家
                        $_799_man += 1;
                    }

                    $total_money += intval($n);
                }

                $from_id = intval($key);
                $ret['date'] = $time;
                $ret['from_id'] = $from_id;
                $ret['money'] = $total_money;
                $ret['ten_man'] = $ten_man;
                $ret['159_man'] = $_159_man;
                $ret['799_man'] = $_799_man;

                $fap['from_id'] = $from_id;
                $fap['date'] = $time;

                if($info = $Paystat->where($fap)->find()){
                    $Paystat->where(array('_id'=>$info['_id']))->save($ret);
                }else{
                    $Paystat->add($ret);
                }

                //金币累计赢取记录
                /*$rep['time'] = $time;
                $rep['from_id'] = $from_id;
                if($rechar = $dailywingold->where($rep)->find()){
                    $re_data = array(
                        'ten_man' => $ten_man,
                        '159_man' => $_159_man,
                        '799_man' => $_799_man
                    );
                    $dailywingold->where(array('_id'=>$rechar['_id']))->save($re_data);
                }*/

            }

        }

        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

}