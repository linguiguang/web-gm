<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/5
 * Time: 13:42
 */
namespace Admin\Controller;
use Think\Controller;

class EverydayshareusersController extends Controller{

    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;
        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = D('Share')->where($map)->select();
        $data = array();
        foreach ($list as $v){
            $data[$v['uid']]['type'.$v['type']] += 1;
        }

        $everydayShareUsers = D('Everydayshareusers');
        foreach ($data as $char_id=>$type){
            $ret['time'] = $b_time;
            $ret['char_id'] = $char_id;
            $ret['type1'] = intval($type['type1']);
            $ret['type2'] = intval($type['type2']);

            $eap['time'] = $b_time;
            $eap['char_id'] = $char_id;
            if($info = $everydayShareUsers->where($eap)->find()){
                $everydayShareUsers->where(array('_id'=>$info['_id']))->save($ret);
            }else{
                $everydayShareUsers->add($ret);
            }

        }

        $etime = microtime(true);
        $totals = round($etime-$stime, 2);
        echo "Run {$totals} S";
    }

}