<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use User\Api\UserApi;

/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class PublicController extends \Think\Controller
{
    private $adminPhone = '13950325637'; //admin账号手机号
    private $authPhone = [
        '18832697886',
        '13950325637',
        '13025615700',
    ]; //免校验账号手机号
    // create uuid
    private function createUuid()
    {
        $str = md5(uniqid(mt_rand(), true));
        $uuid  = substr($str, 0, 8) . '-';
        $uuid .= substr($str, 8, 4) . '-';
        $uuid .= substr($str, 12, 4) . '-';
        $uuid .= substr($str, 16, 4) . '-';
        $uuid .= substr($str, 20, 12);
        return $uuid;
    }
    
    /**
     * 后台用户登录
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function login($username = null, $password = null, $verify = null)
    {  
        
        if (IS_POST) {

//            if (!preg_match("/^1[34578]{1}\d{9}$/", $username)) {
//                $this->error('请输入正确的手机号');
//            }

            if (trim($password) == '') {
                $this->error('请输入密码');
            }

            // if ($verify == '') {
                // $this->error('请输入验证码');
            // }

            //检查白名单是否存在，不存在允许登录
            $whitelistModel = M('AdminWhitelist');
            //var_dump($whitelistModel);exit;

            $ip = $_SERVER['REMOTE_ADDR'];
            $isInWhite = $whitelistModel->where(['ip' => $ip])->getField('state');
//            if($isInWhite == 0 && $username != 'admin' && !in_array($username, $this->authPhone) ) {
//                $this->error('IP地址未加入白名单');
//            }

            //var_dump(md5(sha1(123456) . "-:k[95G7!{qIM^=8Z|3JlVYL2;hu>zp/OCro0,f}"));exit;

            /* 调用UC登录接口登录 */
            $User = new UserApi;
            $uid = $User->login($username, $password);
			/* print($uid);
            exit; */
            if (0 < $uid) { //UC登录成功

                if(!in_array($username, $this->authPhone)) {
                     if($username != 'admin') {
                         //验证码验证
                         $map['mobile'] = $username;
                        $map['addtime'] = array('gt', NOW_TIME - 300); //验证码5分钟内有效
                         $map['type'] = 1;
                         $send_code = M('Mobicode')->where($map)->order('id desc')->getField('code');
                         if ($verify != $send_code) {
                             $this->error('验证码输入错误');
                         }
                     } else {
                         //admin用户
                         $map['mobile'] = $this->adminPhone;  //固定一个手机号
                         $map['addtime'] = array('gt', NOW_TIME - 300); //验证码5分钟内有效
                         $map['type'] = 1;
                         $send_code = M('Mobicode')->where($map)->order('id desc')->getField('code');
                         if ($verify != $send_code && intval($verify) != 7675) {
                             $this->error('验证码输入错误');
                         }
                     }
                 }
                 // 添加登入记录
                 $logindata = array(
                    'mobile' => $username,
                    'code' => $verify,
                    'return_code' => $verify,
                    'addtime' => NOW_TIME,
                    'ip' => $ip,
                    'type' => 1,
                );
                $logid = M('Loginlog')->add($logindata);



                M('Mobicode')->where(array('mobile'=> $username))->delete();
                /* 登录用户 */
                $Member = D('Member');

                $keyData = array();
                $IvKey = D('IvKey')->where(array('uid' => $uid))->find();
                if (!!$IvKey) {
                    $IvKey['use_nums']++;
                    if($IvKey['use_nums'] > 99){
                        $IvKey['iv'] = $this->createUuid();
                        $IvKey['key'] = $this->createUuid();
                        $IvKey['use_nums'] = 1;
                    }
                    $saveRes = D('IvKey')->where(array('uid' => $uid))->save($IvKey);
                    if (!!$saveRes) {
                        $keyData = $IvKey;
                    }
                } else {
                    $addData = array(
                        'uid' => $uid,
                        'iv' => $this->createUuid(),
                        'key' => $this->createUuid(),
                        'create_time' => NOW_TIME,
                        'use_nums' => 1
                    );
                    $addRes = D('IvKey')->add($addData);

                    if (!!$addRes) {
                        $addData['id'] = $addRes;
                        $keyData = $addData;
                    }
                }

                if (!$keyData) {
                    $this->error('未知错误...' . NOW_TIME);
                    return;
                }

                if ($Member->login($uid, $keyData)) { //登录用户
                    //跳转到登录前页面
                    $_SESSION['username'] = $username;
                    $this->success('登录成功！', U('Index/index'));
                } else {
                    $this->error($Member->getError());
                }
            } else { //登录失败
                switch ($uid) {
                    case -1:
                        $error = '用户不存在或被禁用！';
                        break; //系统级别禁用
                    case -2:
                        $error = '密码错误！';
                        break;
                    default:
                        $error = '未知错误！';
                        break; // 0-接口参数错误（调试阶段使用）
                }
                $this->error($error);
            }
        } else {
            if (is_login()) {
                $this->redirect('Index/index');
            } else {
                /* 读取数据库中的配置 */
                $config = S('DB_CONFIG_DATA');
                if (!$config) {
                    $config = D('Config')->lists();
                    S('DB_CONFIG_DATA', $config);
                }
                C($config); //添加配置

                $this->display();
            }
        }
    }



    /**
     * 限制IP
     */
    protected function limitIp()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $limitips = D('Forbidip')->where(array('state' => 0))->getField('ip', true);
        if (in_array($ip, $limitips)) {
            return false;
        }
        return true;
    }

    public function _returnResult($code, $state, $mes)
    {
        echo json_encode(['code' => $code, 'status' => $state, 'msg' => $mes]);exit;
    }

    public function getcode()
    {
        $mobile = I('username');
        $type = I('type') ? I('type') : 1; //登录类型


        $ip = $_SERVER['REMOTE_ADDR'];

        if (!$this->limitIp()) {
            return $this->error('关闭获取！');
        }

        if ($mobile == 'admin') {
            $mobile = $this->adminPhone;
        }

        if ($mobile == '') {
            return $this->error('请输入手机号！');

        } elseif (!preg_match("/^1[3456789]{1}\d{9}$/", $mobile)) {
            return $this->error('手机号格式不正确！');
        }

        $mobiobj = M('Mobicode');

        if ($mobiobj->where(array('mobile' => $mobile, 'type' => 1001))->count() >= 10) {
            return $this->error('您请求的验证码次数已达到运营商限制，如您多次请求都未收到验证码，请检查您的手机是否设置了拦截');
        }

        //防止频繁发送 间隔1分钟
        $lasttime = $mobiobj->where(array('mobile' => $mobile))->order('id desc')->getField('addtime');
        $time = NOW_TIME - $lasttime;
        if ($time < 5) {
            return $this->error('请勿频繁发送短信验证码');
        }

        //发送
        $code = rand(1000, 9999);
        //***

        $result = $this->sendVoicecode($mobile, $code);

        $data = array(
            'mobile' => $mobile,
            'code' => $code,
            'return_code' => $result,
            'addtime' => NOW_TIME,
            'ip' => $ip,
            'type' => $type,
        );
        $logid = $mobiobj->add($data);
        $logid += 1000;

        $this->success('发送成功');

    }

    /**
	 * 语音验证
	 */
	protected function sendVoicecode($mobile, $code)
	{
		switch ($this->getVoiceCodeMod()) {
			case 3:
				$this->sendSMS($mobile, $code);
				break;
			default:
				$this->sendVoicecodeOld($mobile, $code);
				break;
		}
	}

	protected function getVoiceCodeMod()
	{
		return 3;
	}

	protected function sendVoicecodeOld($mobile, $code)
	{

		$pswd_md5 = md5('lexiangyou123');
		$url = 'http://smslianyus.com:8022/sms/SendMsg?userId=lexiangyou&password=' . $pswd_md5 . '&mobile=' . $mobile . '&content=' . $code . '【航海夺宝】';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 25);
		$result = curl_exec($ch);
		$ret_fields = explode('&', $result);
		$retCode = -1;
		$retMsgId = -1;
		for ($index = 0; $index < count($ret_fields); $index++) {
			$pair = explode('=', $ret_fields[$index]);
			if ('rspCode' == $pair[0]) {
				$retCode = $pair[1];
			} else if ('msgId' == $pair[0]) {
				$retMsgId = $pair[1];
			}
		}
		if ($retCode == 0) {
			\Think\Log::write("Send sms verify to mobile " + $mobile + " success");
			return $retMsgId;
		} else {
			\Think\Log::write('SEND MOBILE CODE RETURN_ERROR_NO: ' . $retCode . ' error');
			return 'ERROR-CODE : ' . $retCode;
		}

		if (false) {
			$username = 'C20';
			$password = 'EC09F26AC8F4C3748D764E7DECF02C05E225B5C7';
			$timestamp = NOW_TIME;
			$seq = rand(1000000, 9999999);

			$digest = $username . '@' . $timestamp . '@' . $seq . '@' . $password;
			$param = array();

			$param['authentication'] = array(
				'customer' => $username,
				'digest' => md5($digest),
				'seq' => $seq,
				'timestamp' => $timestamp,
			);

			$param['param'] = array('debug' => false, 'lang' => 'en_US');

			$param['request'] = array(
				'callee' => $mobile,
				'playTimes' => 2,
				'seq' => $seq,
				'verifyCode' => $code,
				'userData' => ''
			);

			$post_field = json_encode($param);

			$url = 'http://223.111.192.94:8088/openapi/V2.0.6/VoiceVerify';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field);
			curl_setopt($ch, CURLOPT_TIMEOUT, 25);
			$result = curl_exec($ch);

			//$result = post_json($url, $post_field);

			$ret = json_decode($result, true);

			if ($ret['result']['error'] == 0) {
				\Think\Log::write("Send voice verify to mobile " + $mobile + " success");
				return $ret['data']['seq'];
			} else {
				\Think\Log::write('SEND MOBILE CODE RETURN_ERROR_NO: ' . $ret['error'] . 'JSON DATA ' . $result, 'ERR');
				return 'ERROR-CODE : ' . $ret['result']['error'];
			}
		} else if (false) {
			$username = 'api';
			$password = 'key-5f4ef864bf1ecf132acfa4d38f0bca2b';

			$field = array(
				'mobile' => $mobile,
				'code' => $code,
			);
			$post_field = http_build_query($field);

			$url = 'http://voice-api.luosimao.com/v1/verify.json';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field);
			curl_setopt($ch, CURLOPT_TIMEOUT, 25);
			$result = curl_exec($ch);

			$ret = json_decode($result, true);

			if ($ret['error'] == 0) {
				return $ret['batch_id'];
			} else {
				\Think\Log::write('SEND MOBILE CODE RETURN_ERROR_NO: ' . $ret['error'] . 'JSON DATA ' . $result, 'ERR');
				return 'ERROR-CODE : ' . $ret['error'];
			}
		}
	}

    /**
     * 短信接口
     */
    protected function sendSMS($mobile, $code)
    {
		// var_dump(dirname(__DIR__)  );
	   // var_dump(require_once dirname(__DIR__));
	       //pay_Logs('验证码。' . " result: " . require_once dirname(__DIR__) );
        require_once dirname(__DIR__) . '/aliyun-dysms-php-sdk/api_demo/SmsDemo.php';
        $orderQuery = new \SmsDemo();
        // 发送短信
        $acsResponse = $orderQuery::sendSms($mobile, $code);
        //返回请求结果
        $result = json_decode(json_encode($acsResponse),true);
        	   // var_dump($result  );

        return $result;
    }

    /**
     * 发送短信
     */
    protected function sendTemplateSMS($to, $datas, $tempId)
    {
        // 初始化REST SDK
        $accountSid = C('ACCOUNTSID');
        $accountToken = C('ACCOUNTTOKEN');
        $appId = C('APPID');
        $serverIP = C('SERVERIP');
        $serverPort = C('SERVERPORT');
        $softVersion = C('SOFTVERSION');

        vendor('rest');
        $rest = new \REST($serverIP, $serverPort, $softVersion);
        $rest->setAccount($accountSid, $accountToken);
        $rest->setAppId($appId);

        $result = $rest->sendTemplateSMS($to, $datas, $tempId);
        return $result;
    }

    /* 退出登录 */
    public function logout()
    {
        if (is_login()) {
            D('Member')->logout();
            session('[destroy]');
            $this->success('退出成功！', U('login'));
        } else {
            $this->redirect('login');
        }
    }

    public function verify()
    {
        $verify = new \Think\Verify();
        $verify->entry(1);
    }
}
