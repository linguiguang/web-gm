<?php

namespace Admin\Controller;

use Think\Controller;

class CheckdataController extends Controller
{
    //
    public function lotterylog()
    {
        $count = D('Lotterylog')->count();
        $arr = array();
        $uids = array();

        // 获取数据的条件
        $wherelog['time'] = array('between', array(1582300800, 1582387199));

        $logs = D('Lotterylog')->where($wherelog)->select();
        // 根据时间组装数据 && 根据uid组装数据
        foreach ($logs as $v) {
            $time = date('Ymd', $v['time']);
            $arr[$time][$v['uid']]['uid'] = $v['uid'];
            $arr[$time][$v['uid']]['nickname'] = $v['nickname'];
            $arr[$time][$v['uid']]['num'] += 1;
            $arr[$time][$v['uid']]['getRedbag'] += $v['getRedbag'];
            $arr[$time][$v['uid']]['getGold'] += $v['getGold'];
            $uids[$v['uid']]['getRedbag'] += $v['getRedbag'];
            $uids[$v['uid']]['getGold'] += $v['getGold'];
        }

        $arrNeed = array();
        // 挑选次数大于10的数据
        foreach ($arr as $k => $ele) {
            foreach ($ele as $key => $val) {
                if ($val['num'] >= 10) {
                    $arrNeed[$k][$key] = $val;
                }
            }
        }

        // print_r($arrNeed);
        echo "抽奖总人数：" . count($uids) . "\n";
        $low4 = array();
        $high4 = array();
        $getRedbag = 0;
        $getGold = 0;

        // 根据vip等级4区分数据
        foreach ($uids as  $k1 => $v1) {
            $where['uid'] = intval($k1);
            $userinfo = D('userinfo')->where($where)->field('vip,uid,nickname')->find();
            if ($userinfo['vip'] < 4) {
                $getRedbag += $v1['getRedbag'];
                $getGold += $v1['getGold'];

                $userinfo['getRedbag'] = $v1['getRedbag'];
                $userinfo['getGold'] = $v1['getGold'];
                $low4[] = $userinfo;
            } else {
                $userinfo['getRedbag'] = $v1['getRedbag'];
                $userinfo['getGold'] = $v1['getGold'];
                $high4[] = $userinfo;
            }
        };

        // 输出日志
        echo "VIP小于4：" . count($low4) . "\n";
        echo "红包: " . $getRedbag . "\n";
        echo "金币: " . $getGold . "\n";
        print_r($high4);
    }

    public function fruitlog()
    {
        // 获取数据的条件
        $_500 = 1;
        $wherelog['c_time'] = array('between', array(1582300800, 1582387199));
        $wherelog['remark'] = array('LIKE', ',' . $_500 . ',');
        $logs = D('fruitlog')->where($wherelog)->select();

        $sget_test_type_1 = 0;
        $sget_test_type_2 = 0;
        foreach ($logs as $v) {
            //
            if (intval($v['test_type']) == 1) {
                $sget_test_type_1 += $v['get'];
            } else if (intval($v['test_type']) == 2) {
                $sget_test_type_2 += $v['get'];
            }
        }

        echo "流水test_type_1: " . $sget_test_type_1 . "\n";
        echo "流水test_type_2: " . $sget_test_type_2 . "\n";
    }

    public function fruitlogAll()
    {
        // 获取数据的条件
        $arr = array(
            '1' => 500, '2' => 1000, '3' => 2500,
            '4' => 5000, '5' => 10000, '6' => 25000,
            '7' => 50000, '8' => 100000, '9' => 250000, '10' => 500000
        );

        $btime = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
        $etime = $btime + 86400 - 1;
        $needData = array();

        for ($i = 1; $i <= 10; $i++) {
            $tempF = function () use (&$i, $arr, &$needData, $btime, $etime) {
                $wherelog['c_time'] = array('between', array($btime, $etime));
                $wherelog['remark'] = array('LIKE', ',' . $i . ',');
                $logs = D('fruitlog')->where($wherelog)->select();
                $sget_test_type_1 = 0;
                $sget_test_type_2 = 0;
                foreach ($logs as $v) {
                    //
                    if (intval($v['test_type']) == 1) {
                        $sget_test_type_1 += $v['get'];
                    } else if (intval($v['test_type']) == 2) {
                        $sget_test_type_2 += $v['get'];
                    }
                }

                $time = date('Y-m-d', $btime);
                $needData[$time]['试玩'][$arr[$i]] =  $sget_test_type_1;
                $needData[$time]['娱乐'][$arr[$i]] =  $sget_test_type_2;
            };
            $tempF();
        }

        foreach ($needData as $k => $v) {
            foreach ($v as $k2 => $v2) {
                if ($k2 == '试玩') {
                    $v2['test_type'] = 1;
                }
                if ($k2 == '娱乐') {
                    $v2['test_type'] = 2;
                }

                $v2['c_time'] = $k;
                $v2['time'] = strtotime($k);
                $check['c_time'] = $k;
                $check['test_type'] = $v2['test_type'];
                $data = D('Fruitlogdiagram')->where($check)->find();
                if (!!$data) {
                    D('Fruitlogdiagram')->where(array('_id' => $data['_id']))->save($v2);
                } else {
                    D('Fruitlogdiagram')->add($v2);
                }
            }
        }
    }

    public function superFruitlogAll()
    {
        // 获取数据的条件
        $arr = array(
            '1' => 500, '2' => 1000, '3' => 2500,
            '4' => 5000, '5' => 10000, '6' => 25000,
            '7' => 50000, '8' => 100000, '9' => 250000, '10' => 500000
        );

        $btime = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
        $etime = $btime + 86400 - 1;
        $needData = array();

        for ($i = 1; $i <= 10; $i++) {
            $tempF = function () use (&$i, $arr, &$needData, $btime, $etime) {
                $wherelog['c_time'] = array('between', array($btime, $etime));
                $wherelog['remark'] = array('LIKE', ',' . $i . ',');
                $logs = D('superfruitlog')->where($wherelog)->select();
                $sget_test_type_1 = 0;
                $sget_test_type_2 = 0;
                foreach ($logs as $v) {
                    //
                    if (intval($v['test_type']) == 1) {
                        $sget_test_type_1 += $v['get'];
                    } else if (intval($v['test_type']) == 2) {
                        $sget_test_type_2 += $v['get'];
                    }
                }

                $time = date('Y-m-d', $btime);
                $needData[$time]['试玩'][$arr[$i]] =  $sget_test_type_1;
                $needData[$time]['娱乐'][$arr[$i]] =  $sget_test_type_2;
            };
            $tempF();
        }

        foreach ($needData as $k => $v) {
            foreach ($v as $k2 => $v2) {
                if ($k2 == '试玩') {
                    $v2['test_type'] = 61;
                }
                if ($k2 == '娱乐') {
                    $v2['test_type'] = 62;
                }

                $v2['c_time'] = $k;
                $v2['time'] = strtotime($k);
                $check['c_time'] = $k;
                $check['test_type'] = $v2['test_type'];
                $data = D('Fruitlogdiagram')->where($check)->find();
                if (!!$data) {
                    D('Fruitlogdiagram')->where(array('_id' => $data['_id']))->save($v2);
                } else {
                    D('Fruitlogdiagram')->add($v2);
                }
            }
        }
    }

    public function fruitlogAll_bk()
    {
        // 获取数据的条件
        $arr = array(
            '1' => 500, '2' => 1000, '3' => 2500,
            '4' => 5000, '5' => 10000, '6' => 25000,
            '7' => 50000, '8' => 100000, '9' => 250000, '10' => 500000
        );

        for ($j = 1581091200 + 86400 * 17; $j < 1581091200 + 86400 * 18; $j = $j + 86400) {
            $needData = array();
            $b_time = $j;
            $e_time = $j + 86400 - 1;
            for ($i = 1; $i <= 10; $i++) {
                $tempF = function () use (&$i, $arr, &$needData, $b_time,  $e_time) {
                    $wherelog['c_time'] = array('between', array($b_time,  $e_time));
                    $wherelog['remark'] = array('LIKE', ',' . $i . ',');
                    $logs = D('fruitlog')->where($wherelog)->select();

                    $sget_test_type_1 = 0;
                    $sget_test_type_2 = 0;
                    foreach ($logs as $v) {
                        //
                        if (intval($v['test_type']) == 1) {
                            $sget_test_type_1 += $v['get'];
                        } else if (intval($v['test_type']) == 2) {
                            $sget_test_type_2 += $v['get'];
                        }
                    }

                    $time = date('Y-m-d', $b_time);

                    $needData[$time]['试玩'][$arr[$i]] =  $sget_test_type_1;
                    $needData[$time]['娱乐'][$arr[$i]] =  $sget_test_type_2;
                };
                $tempF();
            }

            print_r($needData);
            foreach ($needData as $k => $v) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == '试玩') {
                        $v2['test_type'] = 1;
                    }
                    if ($k2 == '娱乐') {
                        $v2['test_type'] = 2;
                    }

                    $v2['c_time'] = $k;
                    $v2['time'] = strtotime($k);

                    $check['c_time'] = $k;
                    $check['test_type'] = $v2['test_type'];
                    $data = D('Fruitlogdiagram')->where($check)->find();
                    if (!!$data) {
                        D('Fruitlogdiagram')->where(array('_id' => $data['_id']))->save($v2);
                    } else {
                        D('Fruitlogdiagram')->add($v2);
                    }
                }
            }
        }
    }

    public function onlineuser2right()
    {
        $where['islogin'] = 1;
        $where['lastlogin'] = array(
            'lt', NOW_TIME - 86400 * 2
        );
        $userinfo = D('userinfo')->where($where)->select();

        foreach ($userinfo as $k => $info) {
            $savedata['islogin'] = 0;
            D('userinfo')->where(array('_id' => $info['_id']))->save($savedata);
        }
    }

    // 检查红包情况
    public function redbaglog(){
        $where['c_time'] = array('$gte', 1582992000);
        $logs = D('Redbaglog')->where($where)->select();

        print_r($logs);
    }
}
