<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/17
 * Time: 11:03
 */

namespace Admin\Controller;

use Think\Controller;

class OnlineStatController extends Controller
{

    /**
     * 在线用户
     */
    public function online()
    {
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        /*$bb_time = strtotime('2018-03-01');
        $ee_time = strtotime('2018-03-29');
        for($i=0;$i<(($ee_time-$bb_time)/86400);$i++) {
            $b_time = $bb_time + $i*86400;
            $e_time = $b_time + 86399;
            echo date('Y-m-d', $e_time).PHP_EOL;*/

        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = D('Online')->where($map)->select();

        $arr = array();
        foreach ($list as $v) {
            $time = date('Y-m-d H:00:00', $v['c_time']);
            $arr[$time]['totals'] += $v['totals'];
            $arr[$time]['xs'] += $v['xs'];
            $arr[$time]['cj'] += $v['cj'];
            $arr[$time]['zj'] += $v['zj'];
            $arr[$time]['gj'] += $v['gj'];
            $arr[$time]['gb'] += $v['gb'];
            $arr[$time]['fh'] += $v['fh'];
            $arr[$time]['brnn'] += $v['brnn'];
            $arr[$time]['fruit'] += $v['fruit'];
            $arr[$time]['car'] += $v['car'];
            $arr[$time]['red'] += $v['red'];
            // $arr[$time]['airlaba'] += $v['airlaba'];
            $arr[$time]['fruit_testtype1'] += $v['fruit_testtype1'];
            $arr[$time]['fruit_testtype2'] += $v['fruit_testtype2'];
            $arr[$time]['superfruit_testtype1'] += $v['superfruit_testtype1'];
            $arr[$time]['superfruit_testtype2'] += $v['superfruit_testtype2'];
            $arr[$time]['airlaba_testtype1'] += $v['airlaba_testtype1'];
            $arr[$time]['airlaba_testtype2'] += $v['airlaba_testtype2'];
            $arr[$time]['airsuperlaba_testtype1'] += $v['airsuperlaba_testtype1'];
            $arr[$time]['airsuperlaba_testtype2'] += $v['airsuperlaba_testtype2'];
            $arr[$time]['num'] += 1; //增加次数
        }

        $onlinestat = D('Onlinestat');
        foreach ($arr as $k => $v) {
            $num = $v['num']; //每小时发送次数
            $c_time = strtotime($k);
            $data['c_time'] = $c_time;
            $data['totals'] = (int) floor($v['totals'] / $num);
            $data['xs'] = (int) floor($v['xs'] / $num);
            $data['cj'] = (int) floor($v['cj'] / $num);
            $data['zj'] = (int) floor($v['zj'] / $num);
            $data['gj'] = (int) floor($v['gj'] / $num);
            $data['gb'] = (int) floor($v['gb'] / $num);
            $data['fh'] = (int) floor($v['fh'] / $num);
            $data['brnn'] = (int) floor($v['brnn'] / $num);
            $data['fruit'] = (int) floor($v['fruit'] / $num);
            $data['car'] = (int) floor($v['car'] / $num);
            $data['red'] = (int) floor($v['red'] / $num);
            // $data['airlaba'] = (int) floor($v['airlaba'] / $num);
            $data['fruit_testtype1'] = (int) floor($v['fruit_testtype1'] / $num);
            $data['fruit_testtype2'] = (int) floor($v['fruit_testtype2'] / $num);
            $data['superfruit_testtype1'] = (int) floor($v['superfruit_testtype1'] / $num);
            $data['superfruit_testtype2'] = (int) floor($v['superfruit_testtype2'] / $num);
            $data['airlaba_testtype1'] = (int) floor($v['airlaba_testtype1'] / $num);
            $data['airlaba_testtype2'] = (int) floor($v['airlaba_testtype2'] / $num);
            $data['airsuperlaba_testtype1'] = (int) floor($v['airsuperlaba_testtype1'] / $num);
            $data['airsuperlaba_testtype2'] = (int) floor($v['airsuperlaba_testtype2'] / $num);
            $data['room'] = $data['xs'] + $data['cj'] + $data['zj'] + $data['gj'] +
                $data['gb'] + $data['fh'] + $data['brnn'] + $data['car'] + $data['red'] + $data['fruit']+ 
                $data['fruit_testtype1'] + $data['fruit_testtype2'] + $data['superfruit_testtype1'] + $data['superfruit_testtype2'] +
                $data['airlaba_testtype1'] + $data['airlaba_testtype2'] + $data['airsuperlaba_testtype1'] + $data['airsuperlaba_testtype2']; //房间在线人数
            $data['use'] = 0;

            $wap['c_time'] = $c_time;
            $wap['use'] = 0;
            if ($info = $onlinestat->where($wap)->find()) {
                $onlinestat->where(array('_id' => $info['_id']))->save($data);
            } else {
                $onlinestat->add($data);
            }
        }

        //总在线
        $Arealogin = D('Arealogin');
        $aap['login_time'] = array('between', array($b_time, $e_time));
        $aap['type'] = 1;
        $list = $Arealogin->where($aap)->select();

        $data = array();
        foreach ($list as $v) {
            $data['total'][$v['uid']] = $v['uid'];
            $data[$v['room']][$v['uid']] = $v['uid'];
        }

        $onlinestat = D('Onlinestat');
        foreach ($data as $room => $uidList) {
            $ret['c_time'] = $b_time;
            $ret[$room] = count($uidList);
        }

        $ret['use'] = 1; //使用的数据，标识
        if ($info = $onlinestat->where(array('c_time' => $b_time, 'use' => 1))->find()) {
            $onlinestat->where(array('_id' => $info['_id']))->save($ret);
        } else {
            $onlinestat->add($ret);
        }
        //        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    /**
     * 活跃用户
     */
    public function actives()
    {
        $stime = microtime(true);

        $Arealogin = D('Arealogin');
        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['login_time'] = array('between', array($b_time, $e_time));
        $map['type'] = 1;
        //        $map['uid'] = array('egt', 150000);//排除AI
        $list = $Arealogin->where($map)->select();

        $userlist = D('Users')->where(array('id' => array('egt', 150000)))->getField('id,from_id', true);
        

        $users = array();
        foreach ($list as $v) {
            // $date = date("Y-m-d", strtotime("-1 day"));
            $date = date('Y-m-d', $v['login_time']);
            $users[$date][$userlist[$v['uid']]][] = $v['uid'];
        }
        //echo json_encode($users);exit;

        $activestat = D('Activestat');
        foreach ($users as $k => $v) {

            foreach ($v as $key => $val) {
                $c_time = strtotime($k);
                $data['c_time'] = $c_time;
                $data['from_id'] = intval($key);
                $data['user_nums'] = (int) count(array_unique($val));
                // var_dump($data);exit;

                $wap['c_time'] = $c_time;
                $wap['from_id'] = intval($key);
                $info = $activestat->where($wap)->find();
                if ($info) {
                    $activestat->where(array('_id' => $info['_id']))->save($data);
                } else {
                    $activestat->add($data);
                }
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }
}
