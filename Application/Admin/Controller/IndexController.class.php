<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use User\Api\UserApi as UserApi;

/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class IndexController extends AdminController {

    /**
     * 后台首页
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function index(){
        $this->meta_title = '管理首页';

        $b_time = strtotime(date('Y-m-d').'-1 month');

        for($i=$b_time;$i<=NOW_TIME;$i+=86400){
            $date = date('y/m/d', $i);
            $data[$date] = $date;
        }


        $stime = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
        $etime = $stime + 86400;
        for($i=$stime;$i<$etime;$i+=3600){
            $time = date('H:00', $i) ;
            $list[$time] = $time;
        }
     //   // 期数变更    任务计划，每日晚上12：25执行
     //   D('DTaskStat')->channelstat();
      //  // 渠道统计     任务计划，每日晚上12：25执行
    //    D('DTaskStat')->do_task_stat();
        // 金币统计   任务计划，每日晚上12：05执行
      //  D('DGoldstatday')->dgold(); 
        // for ($i=$stime+3600;$i<=$etime;$i+=3600){
        //     $time_h = date('H:i', $i);
        //     $time_arr[$time_h] = $time_h;
        // }
        $payTime = $this->pays(1);
        foreach ($payTime as $k => $item) {
            $time_arr[] = $k.":00";
        }
        $this->assign('adminname',$_SESSION['username']);
        $this->assign('time_h', $time_arr);
        $this->assign('data', $data);
        $this->assign('list', $list);
        $this->display();
    }

    public function getData() {

        $b_time = strtotime(date('Y-m-d').'-1 month');

        $pay = D('DPays');
        //$userinfo = D('Userinfo');
        $user = D('DAccounts');
        //$activestat = D('Activestat'); 
        $online = D('DUserRoomIoLogs');
        //$list=D('DUserDayStats')->get_this_day();
        $userStats=D('DUserDayStats');

        $data = array();
//      $data['chars'] = $this->_chars($b_time, $userinfo);
        $data['users'] = $this->_users($b_time, $user);
        $data['money'] = $this->_paylog($b_time, $pay);
        $data['stats'] = $this->_stats($b_time, $userStats);
       // $data['active'] = $this->actives($b_time, $online);
        //$data['maxactive'] = $this->maxactive($b_time, $online);
       // print_r($data['users']);
        return $this->ajaxReturn($data);
    }

    public function getList() {
     
        //在线人数曲线图
        $list = array();
        for($s=0; $s<3; $s++){
            $stime = date('Y-m-d', strtotime('-'.$s. ' day')); 
            $ontime = $this->_online($stime);
            foreach ($ontime as $k => $item) {
                $list[$stime][] = $item;
            }             
        }
        return $this->ajaxReturn($list);

    }
    
    //每小时在线人数
    public function _online($paydate){

        $b_time = strtotime($paydate." 00:00:00");
        $e_time = strtotime($paydate." 23:59:59");

        $map['paydate'] = $paydate;       
        $list = D('DOnlineStat')->where($map)->field('hour, play_num')->order('hour')->select();
            
        $ret = $return = array();
        foreach ($list as $v){
            $hour = $v['hour'];
            $ret[$paydate][$hour] = intval($v['play_num']);
        }
       
        for($i=$b_time;$i<=$e_time;$i+=3600){
            $date[] = date('H', $i);
        }

        foreach ($date as $v){
            if(!isset($ret[$paydate][$v])) {
                $ret[$paydate][$v] = 0;
            }
        }
        ksort($ret[$paydate]);

        foreach ($ret[$paydate] as $v){
            $return[$paydate][] = $v;
        }
      
        return $return[$paydate];
    }
    //在线人数
    private function _onlineo($i, $obj){
        $b_time = $i;
       
        $map['paydate'] = date("Y-m-d",$b_time);

        $online = $obj;
        $nums = $online->where($map)->field('hour,play_num')->order('hour desc')->select();
      /*   print($online->getLastSql());
        print($nums); */
        $onuser = 0;
        $num = 0;
        foreach ($nums as $v){
            $onuser +=$v['play_num'];
            $num ++;
        }
        return $onuser; //intval($onuser/$num);
    }

    //充值
    private function _paylog($b_time, $obj){
        $d_time = $b_time;
        $b_time = date('Y-m-d 00:00:00', $b_time);
        $e_time = date('Y-m-d 23:59:59', NOW_TIME);

        $map['created_at'] = array('between', array($b_time, $e_time));
        $map['status'] = 1;
        $map['from_id'] = ['NEQ' , 1000];//去除官方渠道
        $map['pay_type'] = ['NEQ' , 9];//去除模拟
        $paylogs = $obj->where($map)->field('created_at,price,account')->order('created_at asc')->select();
        //print($obj->getLastSql());
        for($i=$d_time;$i<=NOW_TIME;$i+=86400){
            $date[] = date('Y-m-d', $i);
        }

        $moneyarr = $return = array();
        foreach ($paylogs as $v){
            $dt = date('Y-m-d', strtotime($v['created_at']));
            $moneyarr[$dt]['money'] += intval($v['price']);
            $moneyarr[$dt]['men'][$v['account']] = $v['account'];
        }

        foreach ($date as $v){
            if(!is_array($moneyarr[$v])) {
                $return['money'][] = 0;
                $return['men'][] = 0;
                continue;
            }
            $return['money'][] = intval($moneyarr[$v]['money']);
            $return['men'][] = count($moneyarr[$v]['men']);
        }

        return $return;
    }

    //用户统计
    private function _stats($b_time, $obj){
        $d_time = $b_time;

        $list=D('DUserDayStats')->get_user_new();
 
        for($i=$d_time;$i<=NOW_TIME;$i+=86400){
            $date[] = date('Y-m-d', $i);
        }
        
        $moneyarr = $return = array();
        foreach ($list as $v){
            $return['new_user'][] = intval($v['new_user']);
            $return['you_user'][] = intval($v['you_user']);
            $return['chong_user'][] = intval($v['chong_user']);
        }
 

        return $return;
    }

    //每小时充值 可以
    public function paysold($is_value=0){
        $b_time = date('Y-m-d')." 00:00:00";
        $e_time = date('Y-m-d')." 23:59:59";

        $map['paydate'] = date('Y-m-d');       
        $list = D('DPayhourStat')->where($map)->field('hour, gold')->order('hour')->select();
      
        $ret = array();
        foreach ($list as $v){
            $hour = $v['hour'];
            $ret[$hour] += intval($v['gold']);
        }
        if($is_value) {
            return $ret;
        }
        return $this->ajaxReturn(array_values($ret));
    }
    public function pays($is_value=0){
        $b_time = date('Y-m-d')." 00:00:00";
        $e_time = date('Y-m-d')." 23:59:59";

        $map['created_at'] = array('between', array($b_time, $e_time));
        $map['status'] = 1;
        $map['from_id'] = ['NEQ' , 1000];//去除官方渠道

        $list = D('DPays')->where($map)->field('HOUR(created_at) AS hour,price AS gold')->order('hour asc')->select();
      
        $ret = array();
        foreach ($list as $v){
            $hour = $v['hour'];
            $ret[$hour] += intval($v['gold']);
        }
        if($is_value) {
            return $ret;
        }
        return $this->ajaxReturn(array_values($ret));
    }

    //角色
    private function _chars($b_time, $obj){
        $b_time = strtotime(date('Y-m-d 00:00:00', $b_time));
        $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));

        $map['uid'] = array('egt', 150000);
        $map['c_time'] = array('between', array($b_time, $e_time));

        $chars = $obj->where($map)->field('c_time,uid')->order('c_time asc')->select();

        $total = $return = array();

        foreach ($chars as $v){
            $dt = date('Y-m-d', $v['c_time']);
            $total[$dt] += 1;
        }

        foreach ($total as $v){
            $return[] = $v;
        }

        return $return;
    }

    //用户
    private function _users($b_time, $obj){
        $d_time = $b_time;
        $b_time = date('Y-m-d 00:00:00', $b_time);
        $e_time = date('Y-m-d 23:59:59', NOW_TIME);

        $map['id'] = array('egt', 10000);
        $map['created_at'] = array('between', array($b_time, $e_time));

        $users = $obj->where($map)->field('created_at,id')->order('created_at asc')->select();
        
        $total = $return = array();

        for($i=$d_time;$i<=NOW_TIME;$i+=86400){
            $date[] = date('Y-m-d', $i);
        }


        foreach ($users as $v){
            $dt = date('Y-m-d', strtotime($v['created_at']));
            $total[$dt] += 1;
         
        }


        foreach ($date as $v){
            if(!isset($total[$v])) {
                $total[$v] = 0;
            }
        }
        ksort($total);


        foreach ($total as $v){
            $return[] = $v;
        }

        return $return;
    }

    //活跃
    public function actives($b_time, $obj){
        $b_time = strtotime(date('Y-m-d 00:00:00', $b_time));
        $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));

        $map['in_time'] = array('between', array($b_time, $e_time));

        $list = $obj->where($map)->field('in_time,out_time')->order('in_time asc')->select();

        $num = $return = array();
        foreach ($list as $v){
            $dt = date('Y-m-d', $v['in_time']);
            $num[$dt] += 1;
        }

        foreach ($num as $k=>$v){
            $return[] = $v;
        }

        return $return;
    }

    //当天最高同时在线
    private function maxactive($b_time, $obj){
        $d_time = $b_time;
        $b_time = strtotime(date('Y-m-d 00:00:00', $b_time));
        $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));

        $map['in_time'] = array('between', array($b_time, $e_time));
        
        $list = $obj->where($map)->field('uid,in_time,out_time')->order('in_time asc')->select();
       // print($obj->getLastSql());
        $num = $return = array();
      
        foreach($list as $v){ 
            $dt =  date('Y-m-d-H', $v['in_time']);
            $num[$dt] += 1;
        }

        foreach ($num as $k=>$v){
            $return[$k] = max($v);
        }

        for($i=$d_time;$i<=NOW_TIME;$i+=86400){
            $date[] = date('H', $i);
        }
       // print_r($date);
        foreach ($date as $v){
            if(!isset($return[$v])) {
                $return[$v] = 0;
            }

        }
        ksort($return);

        foreach ($return as $k=>$v){
            $item[] = $v;
        }

        return $item;
    }

}
