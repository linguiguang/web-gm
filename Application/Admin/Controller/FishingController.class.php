<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 17:12
 */
namespace Admin\Controller;
use Think\Model\MongoModel;
use User\Api\UserApi;
/**
 * 捕鱼后台统计
 */
class FishingController extends AdminController{


    /**
     * 日志记录来源
     */
    protected function fromdata(){
        $data = array(
            1=>'未定义',
            2=>'邮件附件',
            3=>'物品使用',
            4=>'特级礼包',
            5=>'签到',
            6=>'研究',
            7=>'战斗',
            8=>'月卡',
            9=>'特惠礼包',
            10=>'日常任务奖励',
            11=>'周常任务奖励',
            12=>'周常活跃奖励',
            13=>'周常活跃奖励',
            14=>'主线任务奖励',
            15=>'成就奖励',
            16=>'战斗任务奖励',
            17=>'每日在线',
            18=>'每日在线时间段',
            19=>'新手在线',
            20=>'商店充值',
            21=>'转盘奖励',
            22=>'起航礼包',
            23=>'活动',
            24=>'救济',
            25=>'实物兑换',
            26=>'买血',
            27=>'红包任务',
            28=>'买头像框',
            29=>'买头像',
            30=>'使用技能物品',
            31=>'使用技能',
            32=>'改名'
        );
        return $data;
    }

    /**
     * 新增用户
     */
    public function users(){
       //M('Users', 'stat_', 'DB_CONFIG2');
        if(isset($_GET['b_time'])){
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + (6 * 86400);
        }else {
            $b_time = NOW_TIME - (6 * 86400);
            $e_time = NOW_TIME;
        }

        if(!isset($_GET['from_id'])){
            $from_id = -1;
        }else{
            $from_id = intval(I('from_id'));
        }
        
        $data = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $data[$date]['users'] = $this->_users($i, $from_id);
            $data[$date]['active'] = $this->_active($i, $from_id);
        }

        if(I('op') == 'excel'){
            $expTitle = '用户';
            $xlsCell = array(//这里是写字段名称的
                array('a', '用户ID'),
            );
            $xlsData = array();
//            foreach ($info as $k => $v) {
                array_push($xlsData, array(
                    'a' => " " . 6,
                ));
//            }
            exportExcel($expTitle, $xlsCell, $xlsData);
        }

        $this->assign('from_id', $from_id);
        $this->assign('data', $data);
        $this->assign('from', D('DChannels')->select());
        $this->display();
    }

    private function _users($i, $from_id){
        $b_time = strtotime(date('Y-m-d 00:00:00', $i));
        $e_time = strtotime(date('Y-m-d 23:59:59', $i));

        $user = D('Users');
        $map['adddate'] = array('between', array($b_time, $e_time));
        $map['state'] = 1;

        if($from_id != -1){
            $map['from_id'] = $from_id;
        }

        $users = $user->where($map)->count();
        return $users;
    }

    private function _active($i, $from_id){
        $b_time = strtotime(date('Y-m-d 00:00:00', $i));

        $activestat = D('Activestat');
        $map['c_time'] = intval($b_time);

        if($from_id != -1){
            $map['from_id'] = $from_id;
        }

        $actives = $activestat->where($map)->select();

        $users = 0;
        foreach ($actives as $v){
            $users += $v['user_nums'];
        }
        return $users;
    }


    /**
     * 付费率
     */
    public function payrate($p=1){
        $limit = 20;
        $payrate = D('Payrate');

        $from_id = intval(I('from_id'));
        $map['from_id'] = $from_id;

        if(isset($_GET['b_time'])){
            $b_time = strtotime(I('b_time'));
            $map['date'][] = array('egt', $b_time);
        }

        if(isset($_GET['e_time'])){
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['date'][] = array('elt', $e_time);
        }

        $totals = $payrate->where($map)->count();
        $list   = $payrate->where($map)->page($p, $limit)->order('id desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('from_id', $from_id);
        $this->assign('from', D('DChannels')->select());
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 用户等级
     */
    public function level(){

        $st_time =I('b_time');
        $end_time = I('e_time');

        if(trim($st_time) != '' && trim($end_time) != ''){
            $map['c_time'] = array('between', array(strtotime($st_time.' 00:00:00'), strtotime($end_time.' 23:59:59')));
        }else if(trim($st_time) != ''){
            $map['c_time'] = array('egt', strtotime($st_time.' 00:00:00'));
        }else if(trim($end_time) != ''){
            $map['c_time'] = array('elt', strtotime($end_time.' 23:59:59'));
        }

        $userinfo = D('Userinfo');
        $users = $userinfo->where($map)->select();

        $vip = array();
        $totals = 0;
        foreach ($users as $k=>$v){

            if($v['vip'] != 0){
                $vip[$v['vip']] += 1;
                $totals += 1;
            }

        }


        $this->assign('rate1', empty($totals)? 0 : $vip[1] / $totals);
        $this->assign('rate2', empty($totals)? 0 : $vip[2] / $totals);
        $this->assign('rate3', empty($totals)? 0 : $vip[3] / $totals);
        $this->assign('rate4', empty($totals)? 0 : $vip[4] / $totals);
        $this->assign('rate5', empty($totals)? 0 : $vip[5] / $totals);
        $this->assign('rate6', empty($totals)? 0 : $vip[6] / $totals);
        $this->assign('rate7', empty($totals)? 0 : $vip[7] / $totals);

        $this->display();
    }

    public function _checkrange($no, $begin, $end){
        if($no >= $begin && $no <= $end){
            return true;
        }
        return false;
    }


    /**
     * 用户在线时长
     */
    public function onlinetime(){
        $oltimestat = D('Oltimestat');

        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $list[$date] = $this->_avgtime($i, $oltimestat);
        }

        $this->assign('_list', $list);
        $this->display();
    }

    private function _avgtime($time, $obj){
        $date = strtotime(date('Y-m-d 00:00:00', $time));

        $map['c_time'] = $date;
        $map['type'] = 'all';
        $map['room'] = 'totalroom';
        $info = $obj->where($map)->find();
        $game_times = $info['game_times'];
        $dating_times = $info['dating_times'];

        $actives = D('Activestat')->where(array('c_time'=>$date))->select();

        $nums = 0;
        foreach ($actives as $v){
            $nums += $v['user_nums'];
        }

        if($nums == 0){
            $data['avggametime'] = 0;
            $data['avgdatingtime'] = 0;
            $data['nums'] = 0;
        }else{
            $data['avggametime'] = round($game_times/$nums);
            $data['avgdatingtime'] = round($dating_times/$nums);
            $data['nums'] = $nums;
        }

        return $data;
    }
    
    /**
     * 充值统计
     */
    public function pay(){
        $pay = D('Paylog');

        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $list[$date] = $this->_paycount($i, $pay);
        }
       
        $this->assign('_list', $list);
        $this->display();
    }

    private function _paycount($time, $obj){
        $b_time = $time;
        $e_time = $time + 86399;

        $pay = $obj;
        $map['addtime'] = array('between', array($b_time, $e_time));
        $map['state'] = 3;

        $paylog = $pay->where($map)->select();

        $money = $recharge_num = $users = $uique_users = 0;
        $userarr = array();
        foreach ($paylog as $k=>$v){
            $money += $v['money'];
            $recharge_num += 1;
            $userarr[] = $v['uid'];
        }

        $data['money'] = $money;
        $data['recharge_num'] = $recharge_num;
        $data['users'] = count($userarr);
        $data['uique_users'] = count(array_unique($userarr));

        return $data;
    }

    /**
     * 金币消耗
     */
    public function golduse(){
        $golduse = D('Golduse');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
//            $list[$date] = $this->_usegold($i, $golduse);
        }
        $this->assign('_list', $list);
        $this->display();
    }


    /**
     * 金币获得
     */
    public function goldget(){
        $goldget = D('Goldget');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
//            $list[$date] = $this->_getgold($i, $goldget);
        }
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 金币统计
     */
    public function goldstat(){
        $goldstat = D('Goldstat');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $show = intval(I('show'));

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $list[$date] = $this->_statgold($i, $goldstat, $show);
        }
        $this->assign('_list', $list);
        $this->display();
    }

    private function _statgold($time, $obj, $show){
        $b_time = $time;
        $e_time = $time + 86399;

        $goldstat = $obj;
        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = $goldstat->where($map)->select();

        $area1 = $area2 = $area3 = $area4 = $area5 = $other = 0;//总共金币

        if($show == 1){
            $field = 'win';
        }elseif($show == 2){
            $field = 'lose';
        }else{
            $field = 'stat';
        }

        foreach ($list as $v){
            if($v['area'] == 101){
                $area1 = $v[$field];
            }elseif($v['area'] == 102){
                $area2 = $v[$field];
            }elseif($v['area'] == 103){
                $area3 = $v[$field];
            }elseif($v['area'] == 104){
                $area4 = $v[$field];
            }elseif($v['area'] == 105){
                $area5 = $v[$field];
            }else{
                $other += $v[$field];
            }
        }

        $data = array(
            'area1' => $area1,
            'area2' => $area2,
            'area3' => $area3,
            'area4' => $area4,
            'area5' => $area5,
            'other' => $other,
        );
        return $data;
    }

    /**
     * 宝石统计
     */
    public function gemstat(){
        $gemstat = D('Gemstat');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $show = intval(I('show'));

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $list[$date] = $this->_statgem($i, $gemstat, $show);
        }
        $this->assign('_list', $list);
        $this->display();
    }

    private function _statgem($time, $gemstat, $show){
        $b_time = $time;
        $e_time = $time + 86399;

        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = $gemstat->where($map)->select();

        $email = $shop = $sign = $battle = $task = $jn = $other = 0;

        if($show == 1){
            $field = 'win';
        }elseif($show == 2){
            $field = 'lose';
        }else{
            $field = 'stat';
        }

        $taskarr = array(10, 11, 12, 13, 14, 15, 16);//任务id
        $battlearr = array(101, 102, 103, 104, 105);//战斗区域

        foreach ($list as $v){
            if($v['area'] == 20){
                $shop = $v[$field];
            }elseif($v['area'] == 2){
                $email = $v[$field];
            }elseif($v['area'] == 5){
                $sign = $v[$field];
            }elseif(in_array($v['area'], $battlearr)){//战斗
                $battle = $v[$field];
            }elseif(in_array($v['area'], $taskarr)){//任务
                $task = $v[$field];
            }elseif($v['area'] == 31){//使用技能
                $jn = $v[$field];
            }else{
                $other += $v[$field];
            }
        }

        $data = array(
            'shop' => $shop,
            'email' => $email,
            'sign' => $sign,
            'battle' => $battle,
            'task' => $task,
            'jn' => $jn,
            'other' => $other,
        );
        return $data;
    }

    /**
     * 元宝统计
     */
    public function wingstat(){
        $wingstat = D('Wingstat');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $show = intval(I('show'));

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $list[$date] = $this->_statwing($i, $wingstat, $show);
        }
        $this->assign('_list', $list);
        $this->display();
    }

    private function _statwing($time, $wingstat, $show){
        $b_time = $time;
        $e_time = $time + 86399;

        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = $wingstat->where($map)->select();

        $shop = $email = $battle = $bag = $task = $exchange = $other = 0;

        if($show == 1){
            $field = 'win';
        }elseif($show == 2){
            $field = 'lose';
        }else{
            $field = 'stat';
        }

        $battlearr = array(101, 102, 103, 104, 105);//战斗区域

        foreach ($list as $v){
            if($v['area'] == 20){
                $shop = $v[$field];
            }elseif($v['area'] == 2){
                $email = $v[$field];
            }elseif($v['area'] == 4){//特技礼包
                $bag = $v[$field];
            }elseif(in_array($v['area'], $battlearr)){//战斗
                $battle = $v[$field];
            }elseif($v['area'] == 14){//主线任务
                $task = $v[$field];
            }elseif($v['area'] == 25){//实物兑换
                $exchange = $v[$field];
            }else{
                $other += $v[$field];
            }
        }

        $data = array(
            'shop' => $shop,
            'email' => $email,
            'bag' => $bag,
            'battle' => $battle,
            'task' => $task,
            'exchange' => $exchange,
            'other' => $other,
        );
        return $data;
    }

    /**
     * 鱼市统计
     */
    public function fishmarket($p=1){
        $limit = 20;
        $fish = D('Fishmarket');

        $b_time = strtotime(I('b_time'));
        $e_time = (strtotime(I('e_time')) + 86399);

        if(isset($_GET['b_time']) && isset($_GET['e_time'])){
            $map['stime'] = array('between', array($b_time, $e_time));
        }elseif(!isset($_GET['b_time']) && isset($_GET['e_time'])){
            $map['stime'] = array('elt', $e_time);
        }elseif(isset($_GET['b_time']) && !isset($_GET['e_time'])){
            $map['stime'] = array('egt', $b_time);
        }

        $type = intval(I('type'));
        if($type == 1){
            $map['sale'] = 1;
        }elseif($type == 2){
            $map['sale'] = 0;
        }

        $salbuy = intval(I('salbuy'));
        if($salbuy == 1){
            $field = 'bchar_id';
        }else{
            $field = 'schar_id';
        }

        $nickname = trim(I('nickname'));
        if($nickname != ''){
            $char = D('Userinfo')->where(array('username'=>$nickname))->find();
            $char_id = intval($char['char_id']);
            $map[$field] = $char_id;
        }

        $totals = $fish->where($map)->count();
        $list   = $fish->where($map)->page($p, $limit)->order('stime desc')->select();

        $ids = array();
        foreach ($list as $k=>$v){
            $list[$k]['sui'] = $v['price'] * 0.05;
            $ids[] = $v['schar_id'];
            $ids[] = $v['bchar_id'];
        }
        
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('chars', D('Userinfo')->getUserInfo($ids, 'char_id'));
        $this->assign('fishes', D('Configfishes')->getConfig());
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 鱼类产出统计
     */
    public function output(){
        $output = D('Output');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $list[$date] = $this->_fish($i, $output);
        }
        $this->assign('_list', $list);
        $this->display();
    }

    private function _fish($time, $obj){
        $b_time = $time;
        $e_time = $time + 86399;
    }

    /**
     * 宝石获得统计
     */
    public function gemget(){
        $gemget = D('Gemget');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
//            $list[$date] = $this->_getgem($i, $gemget);
        }
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 宝石消耗
     */
    public function gemuse(){
        $gemuse = D('Gemuse');

        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
//            $list[$date] = $this->_usegem($i, $gemuse);
        }
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 元宝获得统计
     */
    public function ybget(){
        $ybget = D('Wingget');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
//            $list[$date] = $this->_getyb($i, $ybget);
        }
        $this->assign('_list', $list);
        $this->display();
    }


    /**
     * 元宝消耗统计
     */
    public function ybuse(){
        $ybuse = D('Winguse');
        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
//            $list[$date] = $this->_useyb($i, $ybuse);
        }
        $this->assign('_list', $list);

        $this->display();
    }


    /**
     * 商城兑换记录
     */
    public function exchange($p=1){
        $limit = 20;
        $exchange = D('Exchangelogs');

        if(isset($_GET['b_time'])){
            $b_time = strtotime(I('b_time'));
            $map['date'][] = array('egt', $b_time);
        }

        if(isset($_GET['e_time'])){
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['date'][] = array('elt', $e_time);
        }

        $totals = $exchange->where($map)->count();
        $list   = $exchange->where($map)->page($p, $limit)->order('id desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 平台统计
     */
    public function platform($p=1){
        $limit = 20;
        $plat = D('Plat');

        if(isset($_GET['b_time'])){
            $b_time = strtotime(I('b_time'));
            $map['date'][] = array('egt', $b_time);
        }

        if(isset($_GET['e_time'])){
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['date'][] = array('elt', $e_time);
        }

        $totals = $plat->where($map)->count();
        $list   = $plat->where($map)->page($p, $limit)->order('id desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 在线用户统计
     */
    public function onlineusers(){
        $online = D('Online');
		$info = array();
		for($i = 1; $i < 10; $i++) {
			$map['serverid'] = $i;
			$map['create_time'] = array('egt', NOW_TIME - 180);
			$data = $online->where($map)->order('create_time desc')->find();

			foreach($data as $k=>$val) {
				$info[$k] += $val;
			}
		}
        $this->assign('info', $info);
        $this->display();
    }

    /**
     * 实时在线用户显示
     */
    public function showonlinusers($area, $p=1){

        $map['islogin'] = 1;

        $area = intval($area);
        if($area != 0){
            $map['area'] = intval($area);
        }

        $limit = 20;
        $totals = D('Userinfo')->where($map)->count();
        $list = D('Userinfo')->where($map)->page($p, $limit)->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $ids = array();
        foreach ($list as $v){
            $ids[] = $v['uid'];
        }

        $this->assign('_list', $list);
        $this->assign('area', $area);
        $this->assign('user', D('Users')->getUserInfo($ids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 付费统计
     */
    public function paystat(){
        $paystat = D('Paystat');
        $info = $paystat->find();
        $this->assign('_list', $info);
        $this->display();
    }

    /**
     * 用户在线时长
     */
    public function timeusers(){
        $select = intval(I('select'));
        $room = intval(I('room'));
        $oltimestat = D('Oltimestat');

        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $list[$date] = $this->_onlinetime($i, $oltimestat, $select, $room);
        }

        $this->assign('_list', $list);
        $this->display();
    }

    private function _onlinetime($time, $obj, $type, $room){
        $date = strtotime(date('Y-m-d 00:00:00', $time));

        if($room == 1){
            $map['room'] = 'gameroom';
        }else{
            $map['room'] = 'totalroom';
        }

        if($type == 1){//单日注册用户
            $map['type'] = 'new';
        }else{
            $map['type'] = 'all';
        }

        $map['c_time'] = $date;
        $list = $obj->where($map)->find();

        return $list;
    }

    /**
     * 计算开始结束时间
     */
    protected function _calculatetime(){

        if(isset($_GET['type']) && isset($_GET['month'])){
            $type = intval(I('type'));
            if($type == 0){//按月
                $month = intval(I('month'));
                $monarr = monthday(NOW_TIME,$month);

                $b_time = strtotime($monarr[0]);
                $now_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));//现在时间
                $e_time = strtotime($monarr[1]);
                if($now_time < $e_time){
                    $e_time = $now_time;
                }

            }elseif($type == 1){//按天数
                $b_time = strtotime(I('b_time'));
                $e_time = strtotime(I('e_time'));

                if($b_time == '' || $e_time == ''){
                    $this->error('请选择时间');
                }
            }

        }else{//默认当月数据
            $b_time = strtotime(date("Y-m-d",mktime(0, 0, 0, date("m"),1,date("Y"))));
            $e_time = NOW_TIME;
            //默认打开为当月数据,用作缓存判断
            $first = 1;
        }

        $data['b_time'] = $b_time;
        $data['e_time'] = $e_time;
        $data['first'] = $first;

        return $data;
    }

    /**
     * 输赢统计
     */
    public function winlose($p=1){
        $userinfo = D('Userinfo');
        $limit = 20;

        if(!isset($_GET['from_id'])){
            $from_id = -1;
        }else{
            $from_id = intval(I('from_id'));
            $map['from_id'] = $from_id;
        }

        if(!isset($_GET['room'])){
            $room = -1;
        }else{
            $room = intval(I('room'));
            $map['area'] = $room;
            $map['islogin'] = 1;
        }

        $username = trim(I('username'));
        if($username != ''){
            $map['username'] = $username;
        }

        $totals = $userinfo->where($map)->count();
        $list   = $userinfo->where($map)->page($p, $limit)->order('c_time desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $ids = array();
        foreach ($list as $k=>$v){
            $ids[] = $v['uid'];
            $list[$k]['winlose'] = $v['win_gold'] - $v['lose_gold'];
        }

        $this->assign('from_id', $from_id);
        $this->assign('room', $room);
        $this->assign('from', D('DChannels')->select());
        $this->assign('user', D('Users')->getUserInfo($ids));
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 在线统计
     */
    public function onlinestat(){
        $b_time = I('b_time');
        $e_time = I('e_time');

        if(isset($_GET['b_time']) || isset($_GET['e_time'])){
            if($b_time == '' || $e_time == ''){
                $this->error('查询时间请填写完整');
            }else{
                $st_time = strtotime($b_time);
                $end_time = strtotime($e_time)+86399;
                $map['c_time'] = array('between', array($st_time, $end_time));
            }
        }else{
            $st_time = mktime(0, 0 , 0,date("m"),1,date("Y"));
            $end_time = mktime(23,59,59,date("m"),date("t"),date("Y"));
            $map['c_time'] = array('between', array($st_time, $end_time));
        }

        $game = intval(I('game'));
        $gamearr = $this->room($game);

        $list = D('Onlinestat')->where($map)->select();
        $datearr = array();
        foreach ($list as $v){
            $date = date('y/m/d', $v['c_time']);
            $datearr[$date]['totals'] += $v['totals'];
            $datearr[$date][$gamearr['field']] += $v[$gamearr['field']];
            $datearr[$date]['num'] += 1;
        }

        //日对比曲线图
        $data = $this->dailyonline();

        $this->assign('data', $data);
        $this->assign('st_time', date('y/m/d', $st_time));
        $this->assign('end_time', date('y/m/d', $end_time));
        $this->assign('gamearr', $gamearr);
        $this->assign('date', $datearr);
        $this->display();
    }

    //日常对比曲线图
    protected function dailyonline(){

        $s_time = I('s_time');
        $end_time = I('end_time');

        if(isset($_GET['s_time']) || isset($_GET['end_time'])){
            if($s_time == '' || $end_time == ''){
                $this->error('查询时间请填写完整');
            }else{
                $map['c_time'] = array('between', array(strtotime($s_time), strtotime($end_time) + 86399));
            }
        }else{
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
            $b_time = $e_time - 86400 - 86399;
            $map['c_time'] = array('between', array($b_time, $e_time));
        }

        $list = D('Onlinestat')->where($map)->select();
        
        $data = array();
        foreach ($list as $v){
            $time = date('Y-m-d', $v['c_time']);
            $data[$time][$v['c_time']] = $v['totals'];
        }

        return $data;
    }

    protected function room($game){
        switch ($game){
            case 0 :
                $field = 'room';
                $name = '全部房间';
                break;
            case 1 :
                $field = 'coast';
                $name = '黄金海岸';
                break;
            case 2 :
                $field = 'forest';
                $name = '梦幻森林';
                break;
            case 3 :
                $field = 'kingdom';
                $name = '冰雪国度';
                break;
            case 4 :
                $field = 'relics';
                $name = '沙漠遗迹';
                break;
            case 5 :
                $field = 'volcano';
                $name = '神秘火山';
                break;
        }
        $data = array(
            'field' => $field,
            'name' => $name
        );
        return $data;
    }

    /**
     * 水果狂欢玩家信息
     */
    public function fruitusers($p=1){

        $limit = 20;
        $fruituser = D('Fruituser');

        $query = '';

        if(isset($_GET['uid'])){
            $uid = intval(I('uid'));
            $map['uid'] = $uid;
            $query .= 'uid/'.$uid.'/';
        }
        
        $nickname = trim(I('nickname'));
        if($nickname != ''){
            $char = D('Userinfo')->where(array('username'=>$nickname))->find();
            $uid = intval($char['uid']);
            $map['uid'] = $uid;
            $query .= 'nickname/'.$nickname.'/';
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if($orderby != '' && $order != '' && $orderby != 'stat'){
            $orders = $orderby.' '.$order;
        }else{
            $orders = '';
        }

        $char_ids = $uids = $list = $arr = $arr_data = array();
        if($orderby == 'stat'){        //总盈利排序

            $users = $fruituser->where($map)->select();
            $totals = count($users);

            foreach ($users as $k=>$v){
                $arr[$v['uid']] = $v['get'] - $v['use'];
                $v['yinli'] = $v['get'] - $v['use'];
                $arr_data[$v['uid']] = $v;
            }

            if($order == 'asc'){
                asort($arr);
            }else{
                arsort($arr);
            }

            $ids = array();
            foreach ($arr as $k=>$v){
                $ids[] = $k;
            }

            for($i=($p-1)*20; $i<=($p*20)-1; $i++){

                $k = $ids[$i];
                if($k != ''){
                    $list[$k] = $arr_data[$k];
                    $char_ids[] = $arr_data[$k]['char_id'];
                    $uids[] = $k;
                }

            }

        }else{    //其它排序
            $totals = $fruituser->where($map)->count();
            $list   = $fruituser->where($map)->page($p, $limit)->order($orders)->select();

            foreach ($list as $k=>$v){
                $list[$k]['yinli'] = $v['get'] - $v['use'];
                $char_ids[] = $v['char_id'];
                $uids[] = $v['uid'];
            }
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('char', D('Userinfo')->getUserInfo($char_ids, 'char_id'));
        $this->assign('user', D('Users')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 水果狂欢大盘
     */
    public function fruitgrail($p=1){
        $limit = 20;
        $poolsat = D('Poolstat');

        if(isset($_GET['b_time'])){
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $poolsat->where($map)->count();
        $list   = $poolsat->where($map)->page($p, $limit)->select();

        $ids = array();
        foreach ($list as $k=>$v){
            $list[$k]['get'] = $v['get']*0.96;
            $list[$k]['yinli'] = $v['use'] - $v['get']*0.96;
            $ids[] = $v['char_id'];
        }

        $list_data = $poolsat->where($map)->select();
        $data = array();
        foreach ($list_data as $v){
            $data['win'] += $v['get']*0.96;
            $data['lose'] += $v['use'];
            $data['yinli'] += ($v['use'] - $v['get']*0.96);
            $data['caichi'] += $v['get']*0.02 - $v['pool'];
            $data['sui'] += $v['get']*0.02;
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('char', D('Userinfo')->getUserInfo($ids, 'char_id'));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }


}