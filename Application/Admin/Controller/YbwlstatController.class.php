<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/5
 * Time: 13:42
 */
namespace Admin\Controller;

use Think\Controller;

class YbwlstatController extends Controller
{

    public function stat()
    {
        $stime = microtime(true);

        $dtime = NOW_TIME;
        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $rechargeLevel = range(401, 405);
        $map['addtime'] = array('between', array($b_time, $e_time));
        $map['state'] = 3;
        $payList = D('Paylog')->where($map)->select();
        $rechargeGold = array();

        foreach ($payList as $v) {
            if (!in_array($v['saletype'], $rechargeLevel)) {
                continue;
            }

            $rechargeGold[$v['money']]['money'] += $v['money'];
            $rechargeGold[$v['money']]['gold'] += $v['money'] * 10000;
            $rechargeGold[$v['money']]['uid'][$v['uid']] = $v['uid'];
        }
        echo $no;

        $tap['c_time'] = array('between', array($b_time, $e_time));
        $taskLogs = D('Tasklogs')->where($tap)->select();
        foreach ($taskLogs as $v) {
            if (strlen($v['taskid']) != 5 || strpos($v['taskid'], '40') !== 0) {
                continue;
            }

            $level = substr($v['taskid'], 0, 3);
            $money = $this->rechargeMoney($level);
            $rechargeGold[$money]['gold'] += $v['num'];
        }

        $ybwlstat = D('Ybwlstat');
        foreach ($rechargeGold as $money => $val) {
            $ret['time'] = $b_time;
            $ret['key'] = intval($money);
            $ret['money'] = intval($val['money']);
            $ret['gold'] = intval($val['gold']);
            $ret['mens'] = count($val['uid']);

            $eap['time'] = $b_time;
            $eap['key'] = $money;
            if ($info = $ybwlstat->where($eap)->find()) {
                $ybwlstat->where(array('_id' => $info['_id']))->save($ret);
            } else {
                $ybwlstat->add($ret);
            }
        }

        $etime = microtime(true);
        $totals = round($etime - $stime, 2);
        echo "Run {$totals} S";
    }

    protected function rechargeMoney($level)
    {
        $config = array(
            401 => 6,
            402 => 38,
            403 => 98,
            404 => 328,
            405 => 648,
        );

        return intval($config[$level]);
    }

}
