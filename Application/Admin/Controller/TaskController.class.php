<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/15
 * Time: 15:18
 */

namespace Admin\Controller;

use Admin\Controller\AdminController;


/**
 * 渠道配置
 */
class TaskController extends AdminController
{



    /**
     * 渠道管理
     */
    public function fromkey($p = 1)
    {
        $fromobj = D('DChannels');

        $limit = 20;

        $b_time = I('b_time');
        $e_time = I('e_time');

        $from_id = trim(I('from_id'));
        if ($from_id != '') {
            $map['from_id'] = $from_id;
        }

        $num = trim(I('num'));
        if ($num != '') {
            $map['num'] = $num;
        }

        $from_name = trim(I('from_name'));
        if ($from_name != '') {
            $map['from_name'] = array('like', '%' . $from_name . '%');
        }


        $total = $fromobj->where($map)->count();
        $list = $fromobj->where($map)->page($p, $limit)->order('id desc')->select();

        // var_dump($fromobj->getLastSql());exit;

        $from_ids = array();
        // foreach ($list as $k => $val) {
        //     if (intval($val['parent_id']) > 0) {
        //         $list[$k]['parent_name'] = $fromobj->where(array('id' => $val['parent_id']))->getField('from_name');
        //     } else {
        //         $list[$k]['parent_name'] = '无';
        //     }

        //     $recharge = D('Paymoneystat')->fromRecharge(intval($val['from_id']), $b_time, $e_time);
        //     $user = D('Users')->fromUser(intval($val['from_id']), $b_time, $e_time);

        //     $from_ids[] = intval($val['from_id']);
        //     $list[$k]['rechargeman'] = $recharge['man'];
        //     $list[$k]['rechargenum'] = $recharge['money'];
        //     $list[$k]['tenman'] = $recharge['tenman'];
        //     $list[$k]['_159man'] = $recharge['_159man'];
        //     $list[$k]['_799man'] = $recharge['_799man'];
        //     $list[$k]['bind'] = $user['bind'];
        //     $list[$k]['nums'] = $user['num'];
        // }

        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        
        //D("DTaskStat")->channelstat();
       // D("DTaskStat")->do_task_stat();  
          
        $this->assign('_list', $list);
        $this->assign('fromlist', $fromlist);
        // $this->assign('allfromrecharges', D('Paylog')->allFromRecharges($from_ids, $b_time, $e_time));
        $this->assign('_page', $pageNav->show());
        $this->assign();

        $this->display();
    }

    public function addfKey()
    {
        if (IS_POST) {
            $fromobj = D('DChannels');
            $from_name = trim(I('post.from_name'));
            $from_id = trim(I('post.from_id'));
            $start_time = strtotime(trim(I('post.start_time')));
            $end_time = strtotime(trim(I('post.end_time'))); //+ 86399;
            $num = trim(I('post.num'));

            if ($from_name == '') {
                $this->error('渠道名不能为空');
            }

            if ($from_id == '') {
                $this->error("渠道id不能为空");
            }

            if ($start_time == '') {
                $this->error("开始时间不能为空");
            }

            if ($end_time == '') {
                $this->error("结束时间不能为空");
            }

            if ($num == '') {
                $this->error("期数不能为空");
            }

            // $namelength = mb_strlen($username, 'utf-8');
            // $pwdlength = mb_strlen($password, 'utf-8');
            // if ($namelength < 1 || $namelength > 50) {
            //     $this->error('用户名长度不合法');
            // }

            // if ($pwdlength < 6 || $pwdlength > 30) {
            //     $this->error('密码长度不合法');
            // }

            // if ($fromobj->where(array('username' => $username))->find()) {
            //     $this->error('此用户名被占用');
            // }

            //$from_id = intval(D('DChannels')->where(array('parent_id' => 0))->order('id desc,from_id desc')->getField('from_id')) + 1;


            if ($fromobj->where(array('from_id' => $from_id, 'from_time' => $num))->find()) {
                $this->error('此渠道id与期数已存在');
            }


            $from_list = D('DChannels')->where(['from_id' => $from_id])->order('id desc')->field('id,from_time,end_time,start_time')->select();
            //print_r($from_list);exit;
            foreach ($from_list as $val) {
                if( $num < $val['from_time']) {
                    //判断是否是之后的时间
                    if($end_time > $val['start_time'] ) {
                        $this->error('期数日期段已存在1');
                    }
                } else {
                    if($end_time < $val['start_time'] ) {
                        $this->error('期数日期段已存在2');
                    }
                }

                //判断是否有重合的时间
                if( is_time_cross($val['start_time'], $val['end_time'], $start_time, $end_time )) {
                    $this->error('期数日期段已存在');
                }
            }

            // if(isset($newFrom) && $newFrom['end_time'] > strtotime($start_time) && $newFrom['start_time'] > $end_time) {
            //     $this->error('期数日期段已存在');
            // }

            // $titleList =  D('ConfigStat')->getConfig();
            //$fromkeyStat = D('DChannelTasks');
            //$fromkeyStatData['from_id'] = $from_id;
            //$fromkeyStatData['from_time'] = $num; //期数
            // $fromkeyStatData['virtual'] = $num.'_'.$from_id;
            // foreach ($titleList['task'] as $item) {
            //     $fromkeyStatData[$item['task_num']] = 0;
            // }
            // foreach ($titleList['rec'] as $item) {
            //     $fromkeyStatData[$item['task_name'].'/'.$item['task_num']] = 0;
            // }
            // $res = $fromkeyStat->add($fromkeyStatData); //添加一条数据

            //$list = $fromkeyStat->select();
            //$password = md5(md5($password));

            
            $secret_key = md5(getRandomString(6));
            $attr=0;
            $skey = $fromobj->where(array('from_id' => $from_id))->order("from_time desc")->find();
            if ($skey)
            {
                $secret_key = $skey['secret_key']; 
                $attr= $skey['attr'];                
            }    

            $data['from_name'] = $from_name;
            $data['from_id'] = $from_id;
            $data['start_time'] = $start_time;
            $data['end_time'] = $end_time;
            $data['from_time'] = $num;
            $data['create_time'] = NOW_TIME;
            $data['secret_key'] = $secret_key;
            $data['attr'] = $attr;
            
            //print_r($data);  
            //exit;      
            //发送给游戏服务端
            /* $post_url = C('GM_URL') .'cgi-bin/gm_oprate.notice_task_add';
            $sendPost['from_id'] = $from_id;
            $sendPost['from_time'] = $num;
            $sendPost['st_time'] = $data['start_time'];
            $sendPost['ed_time'] = $data['end_time'];
            $field = http_build_query($sendPost);
            $ret = PostUrl($post_url, $field); */

            if ($fromobj->add($data)) {
                $this->success('添加成功', U('fromkey'));
            } else {
                echo($fromobj->getLastSql());exit;
                $this->error('添加失败！');
            }
        } else {
            $this->display();
        }
    }




    public function editfKey($id)
    {
        $id = intval($id);
        $map['id'] = $id;
        $keyinfo = D('DChannels')->where($map)->find();

        if (IS_POST) {
            $from_name = trim(I('post.from_name'));
            $from_id = trim(I('post.from_id'));
            $start_time = strtotime(trim(I('post.start_time')));
            $end_time = strtotime(trim(I('post.end_time')));
            $from_time = trim(I('post.from_time'));

            if ($from_name == '') {
                $this->error('渠道名不能为空');
            }

            if ($from_id == '') {
                $this->error("渠道id不能为空");
            }

            if ($start_time == '') {
                $this->error("开始时间不能为空");
            }

            if ($end_time == '') {
                $this->error("结束时间不能为空");
            }

            if ($from_time == '') {
                $this->error("期数不能为空");
            }

            if ( D('DChannels')->where(array('from_id' => $from_id, 'from_time' => $from_time, 'id' => ['NEQ',$id ]))->find()) {
                $this->error('此渠道id与期数已存在');
            }

            $from_list = D('DChannels')->where(['id' => ['NEQ',$id ], 'from_id' => $from_id ])->order('id desc')->field('id,from_time,end_time,start_time')->select();
            // var_dump($from_list);

            foreach ($from_list as $val) {
                if( $from_time < $val['from_time']) {
                    //判断是否是之后的时间
                    if($end_time > $val['start_time'] ) {
                        $this->error('期数日期段已存在1');
                    }
                } else {
                    if($end_time < $val['start_time'] ) {
                        $this->error('期数日期段已存在2');
                    }
                }
                //判断是否有重合的时间
                if( is_time_cross($val['start_time'], $val['end_time'], $start_time, $end_time )) {
                    $this->error('期数日期段已存在');
                }
            }

            // $newFrom = D('DChannels')->order('id desc')->find();

            // if(isset($newFrom) && $newFrom['end_time'] > strtotime($start_time) && $newFrom['start_time'] > $end_time) {
            //     $this->error('期数日期段已存在');
            // }

            $data['from_name'] = $from_name;
            $data['from_id'] = $from_id;
            $data['start_time'] = $start_time;
            $data['end_time'] = $end_time;
            $data['from_time'] = $from_time;

            //发送给游戏服务端
            /* $post_url = C('GM_URL') .'cgi-bin/gm_oprate.notice_task_add';
            $sendPost['from_id'] = $from_id;
            $sendPost['from_time'] = $num;
            $sendPost['st_time'] = $data['start_time'];
            $sendPost['ed_time'] = $data['end_time'];
            $field = http_build_query($sendPost);
            $ret = PostUrl($post_url, $field); */


            if (D('DChannels')->where(['id' => $id])->save($data)) {
                $this->success('修改成功', U('fromkey'));
            }
        } else {
            $this->assign('keyinfo', $keyinfo);
            $this->display();
        }
    }

    public function editpwd($id)
    {
        $id = intval($id);
        if ($id == 0) {
            $this->error('参数错误');
        }

        if (IS_POST) {
            $password = trim(I('post.password'));
            $repassword = trim(I('post.repassword'));

            if ($password == '' || $repassword == '') {
                $this->error('密码输入不能为空');
            }

            if ($password != $repassword) {
                $this->error('确认密码输入不一致');
            }

            $password = md5(md5($password));

            $data['id'] = $id;
            $data['password'] = $password;

            if (D('DChannels')->save($data)) {
                $this->success('密码修改成功', U('fromkey'));
            }
        } else {
            $this->display();
        }
    }

    public function delfKey($id)
    {
        $id = intval($id);
        $user = D('DChannels')->where(array('id' => $id))->find();

        // if (intval($user['parent_id']) == 0) {
        //     $this->error('删除失败，只能删除子渠道！');
        // }

        if (D('DChannels')->delete($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 渠道任务列表
     */
    public function qudaotasks()
    {
        $titleList =  D('ConfigStat')->getConfig();
        $fromkeyStat = D('DChannelTasks');
        $list = [];
        $from_id = I('from_id');
        $from_time = I('from_time');
        if($from_id) {
            $map['from_id'] = $from_id;
        }

        if($from_time) {
            $map['from_time'] = $from_time;
        }

        foreach ($titleList['task'] as $item) {
            $field[] = 'sum(t_'.$item['task_num'].") as t_".$item['task_num'];
            $selectField[] =  "t_".$item['task_num'];
        }

        foreach ($titleList['rec'] as $item) {
            $name = $item['task_name'].'_'.$item['task_num'];
            $field2[] = 'sum('.$name.") as ".$name;
            $selectField2[] = $name;
        }

        $field_str = implode(',', $field);
        $field_str2 = implode(',', $field2);

        $list = $fromkeyStat->where($map)->field("from_id,from_time,".$field_str.','. $field_str2)->group('from_id,from_time')->order('field(from_id, "10000") desc,from_time desc,from_id asc')->select();

        // var_dump($fromkeyStat->getLastSql());exit;

        $fromkey = D('DChannels');


        foreach ($list as $k => $v) {
            $fromList = $fromkey->where([ 'from_id' => $v['from_id'], 'num' => $v['from_time'] ])->find();


            $list[$k]['from_name'] = $fromList['from_name'];
            $list[$k]['virtual'] = $fromList['from_id'].'_'.$fromList['num'];
            $all_total = 0 ;

            //计算总消耗
            foreach ($selectField as $item) {
                $all_total = $v[$item] + $all_total;
            }
            //加上充值的
            foreach ($selectField2 as $item2) {
                $all_total = $v[$item2] + $all_total;
            }


            $list[$k]['all_total'] =  $all_total;
            unset($fromList);
            unset($all_total);

        }


        // var_dump($list);exit;
        // var_dump($titleList['task']);exit;
        $this->assign('task_title', $titleList['task']);
        $this->assign('rec_title', $titleList['rec']);
        $this->assign('_list', $list);
        $this->display();
    }

    //删除渠道任务统计
    public function qudaotasksdel()
    {
        $from_id = I('from_id');
        $from_time = I('from_time');
        if(!empty($from_id) && isset($from_time)) {
            $map['from_id'] = $from_id;
            $map['from_time'] = $from_time;
            $res = D('DChannelTasks')->where($map)->delete();
        }
        if ($res) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    //渠道对比
    public function compared()
    {
        $b_time = I('b_time');
        if($b_time) {
            $e_time = strtotime($b_time.' 23:59:59');
            $between = [strtotime($b_time), $e_time];
        }
        $from_time = I('from_time');

        $model = D('DChannels');
        if($from_time) {
            $where = " from_time = ".$from_time." AND ";
            $model->where(['from_time' => $from_time]);
            $lastTheerTask=[$from_time];
        } else {
            $lastTheerTask = $model->order('from_time desc')->group('from_time')->limit(2)->getField('from_time',true);
            $model->where(['from_time' => ['in', $lastTheerTask]]);
        }

        $user = D('DAccounts');
        // var_dump($model->getLastSql());exit;

        //1、查出所有渠道记录
        $list = $model->order('create_time desc')->select();
        // var_dump($model->getLastSql());exit;
        
        //2，激活人数统计
        if($between) {
            $activationMap['addtime'] = ['BETWEEN', $between];
        }
        $activationList = D('fromActivation')->where($activationMap)->field('count(*) as count,from_id,task_time,task_time_back')->group('from_id,task_time,task_time_back')->select();
        //tasklogger("cost","fromActivation:".D('fromActivation')->getLastSql());
	    $activation = array();
        foreach ($activationList as $val) {
            if ($val['task_time_back']==$val['task_time']) 
            {
                $tback = 1;
            }
            else
            {
                $tback = 2;
            }
            $activation[$val['from_id']. "_".$val['task_time']."_".$tback] += $val['count'];
        }
        // echo json_encode($activationList);exit;

        // $sql = 'from_id='.$val['from_id']." AND  from_time =".$val['num']." AND ".' DATE(date_at) >= "2021-08-10" ';
        //3、计算每个渠道的消耗
        if($between) {
            $consumeMap['d_task_stat.addtime'] = ['BETWEEN', $between];
        }
		$consumeMap['d_task_stat.task_config_id'] = ['gt', 0];
        $consumeList = D('DTaskStat')->join('game1.d_channel_tasks as f ON f.id = d_task_stat.task_config_id', 'left')->where($consumeMap)
            ->field('SUM(reward) as reward,f.from_id,f.from_time,f.task_type,f.room_type')
            ->group('f.from_id,f.from_time,f.task_type,f.room_type')
            ->select();
	
		//tasklogger("cost",D('DTaskStat')->getLastSql());
        $consumeData = [];
        foreach ($consumeList as $val) {
            $consumeData[$val['room_type']][$val['task_type'].'_'.$val['from_id'].'_'.$val['from_time']] += $val['reward'];
		
        }

        //付费计算
        //总付费
        $pay = D('DPays');
        $fromIds = implode(',', array_column($list, 'from_id'));
        $lastTheerTask = implode(',', $lastTheerTask);
        // // var_dump($fromIds);exit;
        $pay->join('game1.d_channels as f ON (f.from_id = d_pays.from_id and f.from_time = d_pays.from_time)', 'left');

        $paySql = "d_pays.status in(1,2,32) AND f.from_time IN({$lastTheerTask}) AND f.from_id IN({$fromIds}) AND UNIX_TIMESTAMP(d_pays.created_at) BETWEEN f.start_time AND f.end_time";

        if($between) {
            $paySql .= " AND UNIX_TIMESTAMP(d_pays.created_at) BETWEEN ".strtotime($b_time). " and ".$e_time;
        }
        $pay->where($paySql);

        $all_pay = $pay->field('SUM(price) as money, f.from_id, f.from_time')
            ->group('f.from_id, f.from_time')
            ->select();
       // tasklogger("compared","-------------".$pay->getLastSql()."---------------\n");
        $payData = [];
        foreach ($all_pay as $val) {
            $payData[$val['from_id'].'_'.$val['from_time']] = $val['money'];
			//tasklogger("compared","-------------".$val['money']."---------------\n");
        }
        //红包统计
        $redPackages = D('DRedPackages');
         
        $redSql = "( status=0 or status=888) AND from_time IN({$lastTheerTask}) AND from_id IN({$fromIds}) ";

        if($between) {
            $redSql .= " AND UNIX_TIMESTAMP(created_at) BETWEEN ".strtotime($b_time). " and ".$e_time;
        }
        $redPackages->where($redSql);

        $all_red_packages = $redPackages->field('ROUND(SUM(amount)/100,2) as money, from_id, from_time')
            ->group('from_id, from_time')
            ->select();
       // tasklogger("compared","-------------".$pay->getLastSql()."---------------\n");
        $redData = [];
        foreach ($all_red_packages as $val) {
            $redData[$val['from_id'].'_'.$val['from_time']] = $val['money'];
			//tasklogger("compared","-------------".$val['money']."---------------\n");
        }

        $rooms = D('DRooms')->rooms_new_all();  //房间
        $room = array();
		$room[] = 0;
        foreach ($rooms as $k => $v) {
            $room[] = intval($k);
        }
        
        foreach ($list as $k => $val) {

            $map['d_users.from_id'] = $val['from_id'];
            $map['d_users.reg_from_task'] = $val['from_time'];
            
            //新用户
            $list[$k]['new_user'] = $activation[$val['from_id']."_".$val['from_time']."_1"]?$activation[$val['from_id']."_".$val['from_time']."_1"]:0;
            //激活人数
            $list[$k]['activation'] = $activation[$val['from_id']."_".$val['from_time']."_2"]?$activation[$val['from_id']."_".$val['from_time']."_2"]:0;

            foreach ($rooms as $r => $vel) {
                $list[$k][$r]['consume'] += $consumeData[$r]['1_'.$val['from_id'].'_'.$val['from_time']];
		       // tasklogger("compared","------room:".$r."-------".$list[$k][$r]['consume']."----------".$consumeData[$r]['1_'.$val['from_id'].'_'.$val['from_time']]."----------\n");
               //if(3==$r){$list[$k][$r]['consume']=$redData[$val['from_id'].'_'. $val['from_time']]?$redData[$val['from_id'].'_'. $val['from_time']]:0;} 
               $list[$k]['consume'] += $list[$k][$r]['consume'];

            }
            
            $list[$k]['redconsume'] = $redData[$val['from_id'].'_'. $val['from_time']]?$redData[$val['from_id'].'_'. $val['from_time']]:0; 
            $list[$k]['payconsume'] = $consumeData[0]['2_'.$val['from_id'].'_'.$val['from_time']];  //充值奖励 
			$list[$k]['consume'] += $list[$k]['payconsume'];
            //$list[$k]['consume'] = $consumeData[$val['from_id'].'_'.$val['from_time']];  //$consume['all_consume'];

            $list[$k]['all_pay'] = $payData[$val['from_id'].'_'. $val['from_time']]?$payData[$val['from_id'].'_'. $val['from_time']]:0; //$all_pay['money'];  //
            //总付费为0 新增及激活人数为0
            if(0==$list[$k]['all_pay'] ){$list[$k]['new_user']=0;$list[$k]['activation'] =0;}
            // //渠道评分
            $list[$k]['score'] = $list[$k]['all_pay'] - $list[$k]['consume']-$list[$k]['redconsume'];
        }

        //今日新增
        $stime = strtotime(date("Y-m-d"));
        $etime =  $stime + 86399;
       // $newUserCount = $user->where( " BETWEEN {$stime} AND {$etime}" )->count();
       // $newUserCount = $newUserCount ? $newUserCount : 0;

        //在线人数
       // $onlieList = D('Online')->order('c_time desc')->find();
       // $onlie_user = $onlieList['totals'];

       
        $this->assign('rooms', $rooms);
       // $this->assign('onlie_user', $onlie_user);
        //$this->assign('new_user', $newUserCount);
        $this->assign('_list', $list);
        $this->assign('from_time', $from_time);
        $this->display();
    }

    public function user()
    {
        $userId = I('uid');
        $devid = I('devid');
        $act = I('act');
        $Userfromstat = D('DChannelUserTasks');
        $rooms = D('DRooms')->rooms_new_all();  //房间
        if($act == 'change_player_info') {
            $char_id = I('char_id');
            $userinfo = D('DUsers')->where(['id' => intval($char_id)])->find();
          
            //修改用户信息
            $data = [
                'id' => $char_id,
                'goldnum' => I('goldnum') ? I('goldnum') : 0,
                'rmb' => I('rmb') ? I('rmb') : 0,
                'vip' => I('vip') ? I('vip') : 0,
                'ticket' => I('ticket') ? I('ticket') : 0,
            ];
          
            //发送给游戏服务端
            $post_url = C('GM_URL') .'cgi-bin/gm_oprate:change_player_info';
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field);
            
            $userdata = [
                'account' => $userId,
                'id' => $char_id,
                'gold' => I('goldnum') ? I('goldnum') : 0,
                'recharge' => I('rmb') ? I('rmb') : 0,
                'level' => I('vip') ? I('vip') : 0,
                'ticket' => I('ticket') ? I('ticket') : 0,
                'b_gold' => $userinfo['gold'],
                'b_recharge' => $userinfo['rmb'],
                'b_level' => $userinfo['vip'],
                'b_ticket' => $userinfo['cash'],
                'c_time' => time(),
                'c_date' => date("Y-m-d H:i:s"),
                'operate' => $_SESSION['username']
            ];
            D('DUserdataLog')->add($userdata);
            $this->success('成功');exit;
        } else if($act == 'refresh_zhc'){
            //刷新招魂财
            $data['uid'] = $userId = I('uid');
            $payLog = D('DPays')->where(['uid' => $userId, 'status' => 1])->getField('saletype', true);
            $payLog = is_array($payLog) ? $payLog : [];
            $moniPay = D('DMonipay')->where(['uid' => $userId])->getField('saletype', true);
            $moniPay = is_array($moniPay) ? $moniPay : [];
            $pay_log = array_merge($payLog, $moniPay);

            $shop_list = implode(',', $pay_log);
            $data['shop_list'] = $shop_list; //'"'.$shop_list.'"';
            // var_dump($data);exit;
            $post_url = C('GM_URL') .'cgi-bin/gm_oprate:reflush_zhaocaihun';
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field);
            $this->success('成功');exit;
        } else if($act == 'getWinGold') {  //已更新2023-7-13
            //获取本期赢金
            $ufrom = D('DUsers')->where(['account' => $userId])->field('from_id,from_time,id')->find();
			$from_id = $ufrom['from_id'];
			$from_time = $ufrom['from_time'];
            $fromStartTime = D('DChannels')->where(['from_id' => $from_id])->order('start_time desc')->getField('start_time');
            $b_time = date("Y-m-d H:i:s",$fromStartTime);
            $uid = $ufrom['id'];
          
            $win_gold=[];
            foreach($rooms as $rk => $rv)
            {
                $win_gold[$rv]=$Userfromstat->getuserstage($uid,$from_id,$from_time,$rk,1)['value'];
            }
            $this->assign('win_gold', $win_gold);
          
        }

        $userSearch = F('UserSearch');
        if(empty($devid) && empty($userId) ) {
            $userId = $userSearch['uid'];
            $devid = $userSearch['devid'];
        }
        

        $room = array();
        foreach ($rooms as $k => $v) {
            $room[] = intval($k);
        }
    
        if($devid || $userId) {
            F('UserSearch', ['uid'=> $userId, 'devid' => $devid]);  //缓存
            $userModel = D('DAccounts');
            if($userId) {
                $map['id'] = $userId;
            }
            if($devid) {
                $map['devid'] = $devid;
            }
            $user_list = $userModel->where($map)->find();
            //print($userModel->getLastSql());
            $userinfo = D('DUsers')->where(['account' => intval($userId)])->find();
            $str = $userinfo['name'];
            if (strlen($str)>11) $str=substr($str,0,11) . '...';

            $user_list['name'] = $str;
            $user_list['last_logout'] = $userinfo['last_logout'];
            $user_list['char_id'] = $userinfo['id'];
            $user_list['from_id'] = $userinfo['from_id'];
            $user_list['from_time'] = $userinfo['from_time'];

            $from = D('DChannels')->where([
                'from_id' => $userinfo['from_id'],
                'from_time' => $userinfo['from_time'],
            ])->find();
           // print_r(D('DChannels')->getLastSql());
            $user_list['from_name'] = $from['from_name'];
            //$pay = D('DPays')->where(['account' => $user_list['id']])->field('devid')->find();
            $user_list['device'] = $user_list['devid'];       

           
            // var_dump($Userfromstat->where(['from_id' =>"10004"] )->delete());

            $statMap['uid'] = $user_list['char_id'];          
            $stat = $Userfromstat->where($statMap)->group('uid,from_id,from_time,created_at')
            ->field("uid,from_id,from_time,created_at")->select();         

            //关卡处理
            foreach ($stat as $key => $val) {
                $levstat[$val['from_id']."_".$val['from_time']]['created_at'] = $val['created_at'];
                $levstat[$val['from_id']."_".$val['from_time']]['from_id'] =$val['from_id'];
                $levstat[$val['from_id']."_".$val['from_time']]['from_time'] =$val['from_time'];
                $topLevel0 = $Userfromstat->getuserstage($val['uid'],$val['from_id'],$val['from_time'],0,2);
                $levstat[$val['from_id']."_".$val['from_time']]['pays'] = $topLevel0['value'] ? $topLevel0['value'] : 0;
               
                foreach ($rooms as $key => $value) {
                    $topLevel1 = $Userfromstat->getuserstage($val['uid'],$val['from_id'],$val['from_time'],$key,1);
                    $levstat[$val['from_id']."_".$val['from_time']]['getlist'][$key] = $topLevel1['value'] ? $topLevel1['value'] : 0;
                    $levstat[$val['from_id']."_".$val['from_time']]['levellist'][$key] = $topLevel1['level']? $topLevel1['level'] : 0;

                    # code...
                }

               // $levelMap1 = ['uid' => $val['uid'],'from_id' => $val['from_id'], 'from_time' => $val['from_time'], 'room_type' => 1];                
                //$topLevel1 = $Userfromstat->getuserstage($val['uid'],$val['from_id'],$val['from_time'],1,1);
                //$levstat[$val['from_id']."_".$val['from_time']]['get1'] = $topLevel1['value'] ? $topLevel1['value'] : 0;
                //$levstat[$val['from_id']."_".$val['from_time']]['level1'] = $topLevel1['level']? $topLevel1['level'] : 0;
              
                
                //$topLevel2 = $Userfromstat->getuserstage($val['uid'],$val['from_id'],$val['from_time'],2,1);
                //$levstat[$val['from_id']."_".$val['from_time']]['get2'] = $topLevel2['value']? $topLevel2['value'] : 0;
                //$levstat[$val['from_id']."_".$val['from_time']]['level2'] = $topLevel2['level']? $topLevel2['level'] : 0;
                             
                //$topLevel3 = $Userfromstat->getuserstage($val['uid'],$val['from_id'],$val['from_time'],3,1);           
                //$levstat[$val['from_id']."_".$val['from_time']]['get3'] = $topLevel3['value']? $topLevel3['value'] : 0;
                //$levstat[$val['from_id']."_".$val['from_time']]['level3'] = $topLevel3['level']? $topLevel3['level'] : 0;
              
				

            }

        }
       // $gameType = C('TASK_GAME_TYPE');
 
     /*    $gamestat = D('DChannelUserTasks')
        ->where(['uid' =>$userinfo['id'],'from_id'=>$userinfo['from_id'],'from_time'=>$userinfo['from_time']])
        ->select();  */

        $gamestat = D('DUserRooms')->where(['uid' =>$userinfo['id']])->select(); 
        /*
        foreach ($gamestat as $k=>$v) {
            $topLevel = D('DResultLogs')->getusertimes($userinfo['id'],$v['room_type']);
            $gamestat[$k]['total_times'] = $topLevel? $topLevel: 0;

        }
            */
        
        $level = $userinfo['vip_lev'];
        // var_dump($gameUserLevel);exit;
        //print(D('DUserRooms')->getLastSql());
        $amountList = D('DAmountLog')->where(['uid' => $userinfo['id']])->order('c_time desc')->limit(50)->select();
       
        $this->assign('amountList', $amountList);

        $userdataLog = D('DUserdataLog')->where(['uid' => $userinfo['id']])->order('c_time desc')->limit(50)->select();
       
        $this->assign('userdataLog', $userdataLog);
   
        $this->assign('level', $level);
        $this->assign('gamestat', $gamestat);
        $this->assign('rooms', $rooms);
        $this->assign('stat', $levstat);
        $this->assign('user_list', $user_list);
        $this->assign('userinfo', $userinfo);
        $this->assign('from', $from);
        $this->assign('userSearch', $userSearch);
        $this->display();
    }

    //用户金币修改
    public function usergoldedit()
    {
        $uid = I('uid');
        $from_id = I('from_id');
        $from_time = I('from_time');

        $userModel = D('users');
        if($uid) {
            $map['uid'] = $uid;
            $map['from_id'] = $from_id;
            $map['from_time'] = $from_time;
        }

        $Userfromstat = D('Userfromstat');
        $user_list = $Userfromstat->where($map)->find();


        if(IS_POST) {
            $pwd = I('password');
            if(empty($pwd) || $pwd != 'wy789.789') {
                $this->error('密码错误！');
            }

            $data = [
                'pays' => I('pays'),
                'get1' => I('get1'),
                'get3' => I('get3'),
                'get2' => I('get2'),
                'get4' => I('get4'),
            ];
            $saveMap['_id'] = I('_id');
            $res = $Userfromstat->where($saveMap)->save($data);

            //var_dump($userModel->getLastSql());exit;
            if($res) {
                $this->success('编辑成功', 'user.html?from_id=&uid='.$uid );
            } else {
                $this->error('编辑失败');
            }

        }


        $this->assign('user_list', $user_list);
        $this->display();
    }

    public function useredit()
    {
        $uid = I('id');
        $userModel = D('DAccounts');
        if($uid) {
            $map['u.account'] = $uid;
			$map1['id'] = $uid;
			$map2['account'] = $uid;
        }
        if(IS_POST) {
			
			$data1 = [               
                'devid' => I('devid')
            ];
            $res1 = D('DAccounts')->where($map1)->save($data1);
            if($res1){
                //提醒服务端重载
                $post_url = C('GMBox_URL') . 'sys/reload_account'; 
                $datas['account'] = intval($uid);
                $datas['timestamp'] = intval(time());
                $datas['sign'] = md5(C('SENDGAME_KEY').time()); 
                $fields = json_encode($datas);

                $retss = PostUrl($post_url, $fields,1);
                 
            }
			//print_r(D('DAccounts')->getLastSql());exit;
            $data2 = [
                'from_id' =>  I('from_id'),
                'from_time' => I('from_time')
            ];
            $res2 = D('DUsers')->where($map2)->save($data2);
			
			if(1) {
                $cmap['from_id'] = intval(I('from_id'));
                $cmap['reg'] = 1;
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key');

			     //发送给游戏服务端
                $post_url = C('GM_URL') .'sys/channel/register_dev_chn2';

                $post_data['channel_id'] =  intval(I('from_id'));
                $post_data['devid'] = I('devid');
                $post_data['hash'] = md5($post_data['devid'].$post_data['channel_id'].$secret_key);
                
                $field = http_build_query($post_data);
                $field = str_replace('%2C', ',', $field);

                $ret = PostUrl($post_url, $field,0);
                $arrs = json_decode($ret, true);

                if ($arrs['ret'] == 0) {
                    $this->success('编辑成功', 'user.html?from_id=&uid='.$uid );
                } else {
                    $this->error($arrs['msg']);
                }
			     
                 
            } 

            if($res1||$res2){$this->success('编辑成功', 'user.html?from_id=&uid='.$uid );}

        }

        $user_list = $userModel->join("game1.d_users as u on u.account=d_accounts.id")->where($map)->field('d_accounts.id,d_accounts.devid,u.from_id,u.from_time')->find();
        $this->assign('user_list', $user_list);
        $this->display();
    }

    //回退期数
    public function backTaskTime()
    {
        $user_id = I('uid');
        $userModel = D('DUsers');
        $user = $userModel->where(['account' => $user_id])->find();
        if(empty($user)) {
            $this->error('用户参数有误');
        }

        $map['account'] = $user_id;

        //修改用户渠道 期数
        $from_time = $user['from_time']>1 ? $user['from_time'] - 1 : 1;
        $res = $userModel->where($map)->save(['from_time' => $from_time]);
        $this->success('回退成功',U('task/user'));


    }

    //激活期数
    public function activationTaskTIme()
    {
        $user_id = I('uid');
        $userModel = D('DUsers');
    
        $user = $userModel->where(['account' => $user_id])->find();
    
        if(empty($user)) {
            
            $this->error('用户参数有误');
        }
        
        $channel_id = $user['from_id'];
        $fromMap['from_id'] = $channel_id;
        $fromMap['start_time'] = ['LT', time()];
        $fromMap['end_time'] = ['GT', time()];
        $fromInfo = D('DChannels')->where($fromMap)->find();
   

        if(!empty($fromInfo)) {

            $map['account'] = $user_id;
            //修改用户渠道 期数
            $res = D('DUsers')->where($map)->save(['from_time' => $fromInfo['from_time']]);
            $this->success('激活成功',U('task/user'));

        }
    }


    /**
     * 新渠道任务
     **/
    public function fromStat() {
        
        $from_id = I('from_id');
        $from_time = I('from_time');

        if(!empty($from_id) && !empty($from_time)) {

            $taskConfig = D('DChannelTasks');
            $statModel = D('DTaskStat');
            $fromModel = D('DChannels');
            $taskConsumeModel = D('DTaskCost'); //消耗表

            $map['from_id'] = $from_id;
            $map['from_time'] = $from_time;

            $flist = $taskConfig->where($map)->order('`task_type` desc, `room_type` desc ,value+0 asc')->select();  //查询渠道配置内容
            
            $list = [];
            $taskConsume=0;
            $leisure=[];
            $rooms = D('DRooms')->rooms_new_all();  //房间
            foreach ($flist as $item) {
                $count = $statModel
                    ->join('game1.d_channel_tasks as c ON c.id = d_task_stat.task_config_id')
                    ->where(['task_config_id' => $item['id']])->count(); // 查询 Stats

                $list[$item['id']] = ($count * $item['reward']);

                $typename=$rooms[$item['room_type']]?$rooms[$item['room_type']]:"";
                if(!empty($typename))
                {
                    $leisure[$typename]+= ($count * $item['reward']);
                }
                if(!empty($typename) || 0 == $item['room_type'])
                {
                    $taskConsume +=($count * $item['reward']);
                }
            }
            foreach($rooms as $rk => $rv)
            {
                $leisure[$rv]=$leisure[$rv]?$leisure[$rv]:0;
            }

            //var_dump($leisure);exit;


            $from = $fromModel->where(['from_id' => $from_id, 'from_time' => $from_time])->find();
            $from_name = $from['from_name'];
             

        }
        // echo json_encode($flist);exit;
        $rooms = D('DRooms')->rooms();  //房间
       
        $this->assign('all_consume', $taskConsume ? $taskConsume: 0);
        $this->assign('leisure', $leisure);
        $this->assign('from_name', $from_name);
        $this->assign('from_id', $from_id);
        $this->assign('rooms', $rooms);
        $this->assign('from_time', $from_time);
        $this->assign('task_title', $flist);
        $this->assign('_list', $list);
        $this->display();
    }

    //渠道配置
    public function taskConf()
    {

  
        $fromobj = D('DChannelTasks'); //渠道配置
        $p = I('p');
        $limit = 20;

        $from_id = trim(I('from_id'));
        if ($from_id != '') {
            $map['from_id'] = $from_id;
        }

        $num = trim(I('num'));
        if ($num != '') {
            $map['from_time'] = $num;
        }
        
        $from_name = trim(I('from_name'));
        if ($from_name != '') {
            $from_ids = D('DChannels')->where(['from_name' => array('like', '%' . $from_name . '%')])->getField('from_id', true);
          
            $map['from_id'] = ['in', implode(',', $from_ids)];
        }
        
        $total = $fromobj->where($map)->count();
        $list = $fromobj->where($map)->page($p, $limit)->order('from_id desc,from_time')->select();
        $fromKeyModel =  D('DChannels');
        foreach ($list as $k => $tiem) {
            $Fromkey = $fromKeyModel->where(['from_id' => $tiem['from_id'], 'from_time' => $tiem['from_time']])->field('from_name')->find();

            // var_dump($fromKeyModel->getLastSql());exit;
            $list[$k]['from_name'] = $Fromkey['from_name'];
        }

       

        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $from_ids = array();

        $this->assign('_list', $list);
//        $this->assign('fromlist', $fromlist);
     
        //$allfromrecharges=D('DPays')->allFromRecharges($from_ids, $b_time, $e_time);
       
      
       // $this->assign('allfromrecharges', $allfromrecharges);
        $this->assign('_page', $pageNav->show());
        $this->assign();

        $this->display();
    }


    public function addTaskConf()
    {
        if (IS_POST) {
            $fromobj = D('DChannelTasks');  //渠道配置
            $desc = trim(I('post.desc'));
            $from_id = trim(I('post.from_id'));
            $from_time = trim(I('post.from_time'));
            $value = trim(I('post.value'));
            $task_type = trim(I('post.task_type'));
            $reward = trim(I('post.reward'));
            $rw_cond = trim(I('post.rw_cond'));


            if ($desc == '') {
                $this->error('描述不能为空');
            }

            if ($from_id == '') {
                $this->error("渠道id不能为空");
            }

            if ($from_time == '') {
                $this->error("期数不能为空");
            }

            if ($value == '') {
                $this->error("数值不能为空");
            }

            if ($task_type == '') {
                $this->error("类型不能为空");
            }

            if ($reward == '') {
                $this->error("奖励不能为空");
            }

            if ($rw_cond == '') {
                $this->error("领取条件不能为空");
            }

            if($fromobj->where(['from_id' => $from_id, 'from_time' => $from_time,'task_type' => $task_type, 'value' =>$value])->count() > 0) {
                $this->error("该渠道与期数的数值已存在");
            }

            $data = [
                'from_time' => $from_time,
                'from_id' => $from_id,
                'value' => $value,
                'desc' => $desc,
                'reward' => $reward,
                'task_type' => $task_type,
                'rw_cond' =>$rw_cond,

            ];

            if ($fromobj->add($data)) {
                $this->success('添加成功', U('taskConf'));
            } else {
                echo($fromobj->getLastSql());exit;
                $this->error('添加失败！');
            }
        } else {
            $this->display();
        }
    }

    //编辑配置
    public function editTaskConf()
    {
        $fromobj = D('DChannelTasks');  //渠道配置
        if (IS_POST) {

            $desc = trim(I('post.desc'));
            $from_id = trim(I('post.from_id'));
            $from_time = trim(I('post.from_time'));
            $value = trim(I('post.value'));
            $task_type = trim(I('post.task_type'));
            $reward = trim(I('post.reward'));
            $rw_cond = trim(I('post.rw_cond'));

            if ($desc == '') {
                $this->error('描述不能为空');
            }

            if ($from_id == '') {
                $this->error("渠道id不能为空");
            }

            if ($from_time == '') {
                $this->error("期数不能为空");
            }

            if ($value == '') {
                $this->error("数值不能为空");
            }

            if ($task_type == '') {
                $this->error("类型不能为空");
            }

            if ($reward == '') {
                $this->error("奖励不能为空");
            }

            if ($rw_cond == '') {
                $this->error("领取条件不能为空");
            }

            $id = I('id');
            $map = [
                'from_id' => $from_id,
                'from_time' => $from_time,
                'task_type' => $task_type,
                'value' =>$value,
                'id' => ['neq' => $id]
            ];

            if($fromobj->where($map)->count() > 0) {
                $this->error("该渠道与期数的数值已存在");
            }

            $data = [
                'from_time' => $from_time,
                'from_id' => $from_id,
                'value' => $value,
                'desc' => $desc,
                'reward' => $reward,
                'task_type' => $task_type,
                'rw_cond' =>$rw_cond,
            ];

            if ($fromobj->where(['id' => $id])->save($data)) {
                $this->success('编辑成功', U('taskConf'));
            } else {
                
                $this->error('编辑失败！');
            }

        } else {

            $id = I('id');
            $info = $fromobj->where(['id' => $id])->find();
            $info['desc'] = str_replace('"', '', $info['desc']);
            // var_dump($info);exit;
            $this->assign('info', $info);
            $this->display();

        }
    }

    //删除配置
    public function delTaskConf()
    {

        $id = I('id');
        if($id) {
            $fromobj = D('DChannelTasks');
            if ($fromobj->delete($id)) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        }
    }

    //生成配置
    public function addConf()
    {
        header("Content-Type:text/html;charset=utf-8");

        if(!file_exists('config/channel_plan.xlsx')) {
            $this->error('config目录下的channel_plan.xlsx文件不存在！');
        }

        $config = D('taskConf');
        $taskConfModel = D('DChannelTasks');
        $list = $config->getConfig();
       // print_r($list);exit;
        foreach ($list as $item) {
            $data = [];
            $data['from_id'] = $item['from_id'];
            $data['from_time'] = $item['from_time'];
            $data['value'] = $item['value'];
            $data['reward'] = $item['reward'];
            $data['task_type'] = $item['task_type'];
            if(!empty($item['room_type'])) {
                $data['room_type'] = $item['room_type'];
            }
            if($taskConfModel->where($data)->find()){
                echo "配置 {$item['desc']} 已存在 <br>";
                continue;  //存在这个配置就跳过
            } else {
                $item['room_type'] = $item['room_type'] > 0 ? $item['room_type'] : 0;
                $res = $taskConfModel->add($item);
                if($res) {
                    echo "添加配置 {$item['desc']} 成功<br>";
                }
            }

        }

        $this->success('操作成功');


    }

    //跑老数据task表入新数据task_stat表
    public function taskConfRun() {


        header("Content-Type:text/html;charset=utf-8");
        $taskModel = D('DChannelUserTasks');
 
        $from_id = I('from_id');
        $from_time = I('from_time');
        $p = I('p');
        $p = empty($p) ? 1 :$p;
        $map['from_id'] = $from_id;
        $map['from_time'] = $from_time;

        $limit = 1000;                  //一次跑几条

        $count = $taskModel->where($map)->count();
        $now_limit = $p * $limit;
        // var_dump($count);exit;

        $list = $taskModel->where($map)->page($p, $limit)->order('from_time asc,id desc')->field('uid,recharge as recharge_amount,(klm_win+tly_win) as gold_amount,from_id,from_time,room_type')->select();

        foreach ($list as $k =>$v) {
            //走接口插入
            $this->channelstat($v);
        }
        if(empty($list)) {
            $this->success('已完成', U('fromkey'));
        } else {
            $this->success('成功!一共'.$count."条数据，当前运行第{$now_limit}条", U('taskConfRun', ['from_id' => $from_id, 'from_time' => $from_time, 'p' => $p + 1]));
        }

    }

    /**新 渠道任务统计 **/
    public function channelstat($data) {

        //$flist = F('NIUNIUSTAT');

        
        $taskConfig = D('DChannelTasks');
        $taskRecord = D('DTaskRecord');

        $from_id = $data['from_id'];
        $uid = D("DUsers")->get_uid('char_id',$data['uid']);
        $char_id = $data['uid'];
        $recharge_amount = $data['recharge_amount']; //充值金额
        $gold_amount = $data['gold_amount']; //赢金值
        $from_time = $data['from_time']; //期数
        $room_type = $data['room_type']; //场次

        //先查询task_config表里的数据

        $map['from_id'] = $newMap['from_id'] = $from_id;
        $map['from_time'] = $from_time;        
        $key = 'FROM_'.$from_id."_TASK_".$from_time;

        $flist = F($key);
        if(empty($flist)) {
            $flist = $taskConfig->where($map)->select();  //查询渠道配置内容
            F($key, $flist);
        }

        $fromkeyStat = D('DTaskStat');
        //$is_old = $fromkeyStat->where($map)->count(); //查询是否是该渠道的新用户 0是，1不是
        $map['account'] = $recordMap['account'] = $uid;
        $map['uid'] = $recordMap['uid'] = $char_id;
        $info = $fromkeyStat->where($map)->getField('task_config_id', true); //查询task_stat表的数据

        $taskRecordInfo = $taskRecord->where($recordMap)->find(); //查询充值金币消耗等数据

        //TODO 修改计算当日月收益的总消耗
        //查询当日的数据
        $redpacketLog = D('DTaskCost');

        $redMap['date_at'] = date("Y-m-d");
        $redMap['from_time'] = $from_time;
        $redMap['from_id'] = $from_id;
        $redpacketInfo = $redpacketLog->where($redMap)->find();

        //新充值查询
        //$is_old = $fromkeyStat->where(['from_id' =>$from_id])->order('from_time')->find(); //查询是否是新用户金币


        //新用户充值查询
        $task_recharge = $taskConfig->where(['from_id' =>$from_id, 'task_type' => 2, 'condition' => 2 ])->order('from_time asc')->field('id,from_time')->find(); //查询最新一期有没有充值，有充值数据就是本期的新用户
        if($task_recharge) {
            $is_recharge = $fromkeyStat->where(['task_config_id' => $task_recharge['id'], 'uid' => $uid])->find(); //查询是否是新用户充值
        }

        //新用户充值查询
        $task_glod = $taskConfig->where(['from_id' =>$from_id, 'task_type' => 1, 'condition' => 2 ])->order('from_time asc')->field('id')->find(); //查询最新一期有没有充值，有充值数据就是本期的新用户
        if($task_glod) {
            $is_gold = $fromkeyStat->where(['task_config_id' => $task_glod['id'], 'uid' => $uid])->find(); //查询是否是新用户充值
        }
        // var_dump($task_glod, $is_gold);exit;



        //遍历$flist 配置的内容
        if(!empty($flist)) {
            $all_consume = 0;
            foreach ($flist as $val ) {

                if(isset($is_gold) && $val['task_type'] == 1 && $val['condition'] == 2) {
                    continue;//已有新用户充值数据就不再进
                }

                if(isset($is_recharge) && $val['from_time'] != $task_recharge['from_time'] && $val['task_type'] == 2 && $val['condition'] == 2) {
                    continue;  //老用户充值不进新用户选项
                }

                if(!isset($is_recharge) && $val['task_type'] == 2 && $val['condition'] == 3 ) {
                    continue;  //没有充值数据的新用户
                }

                if(isset($is_recharge) && $task_recharge['from_time'] == $val['from_time'] && $val['task_type'] == 2 && $val['condition'] == 3) {
                    continue;  //同期的充值新用户
                }

                //查询本期是否有新用户充值数据
                if( $val['from_time'] == $task_recharge['from_time']  && $val['task_type'] == 2 && $val['condition'] == 3 ) {
                    continue;
                }
                
                if($val['task_type'] == 1 && $gold_amount >= $val['value']) {  //金币的

                    //检查是否已有这条数据
                    if(!in_array($val['id'], $info)) {
                        //插入数据
                        $taskStatData = [
                            'task_config_id' => $val['id'],
                            'account' => $uid,
                            'uid' => $char_id,
                            'from_id' => $from_id,
                            'from_time' => $from_time,
                            'addtime' => time(),
                        ];
                        $fromkeyStat->add($taskStatData);
                        //累计消耗
                        $all_consume = $all_consume + $val['reward'];
                    }
                }

                if($val['task_type'] == 2 && $recharge_amount >= $val['value']) {  //充值的

                    //检查是否已有这条数据
                    if(!in_array($val['id'], $info)) {
                        //插入数据
                        $taskStatData = [
                            'task_config_id' => $val['id'],
                            'account' => $uid,
                            'uid' => $char_id,
                            'from_id' => $from_id,
                            'from_time' => $from_time,
                            'addtime' => time(),
                        ];
                        $fromkeyStat->add($taskStatData);
                        //累计消耗
                        $all_consume = $all_consume + $val['reward'];
                    }
                }
            }


            //处理总消耗
            if(!empty($taskRecordInfo) ) {
                $consume = $all_consume - $taskRecordInfo['consume']; //计算差值消耗
                //加到当日消耗数据
                $redpacketData['all_consume'] = $redpacketInfo['all_consume'] + $all_consume;

            } else {
                //直接加入今日消耗
                $redpacketData['all_consume'] = $redpacketInfo['all_consume'] + $all_consume;
                //插入记录数据
                $taskRecord->add([
                    'account' => $uid,
                    'uid' => $char_id,
                    'recharge_amount' => $recharge_amount,
                    'gold_amount' => $gold_amount,
                    'consume' => $all_consume,
                    'from_id' =>$from_id,
                    'from_time' => $from_time,
                    'addtime' => time(),
                ]);
            }

            //记录总消耗
            if(empty($redpacketInfo)) {
                $res = $redpacketLog->add([
                    'date_at' =>date("Y-m-d"),
                    'from_time' => $from_time,
                    'from_id' =>$from_id,
                    'all_consume' => $redpacketData['all_consume'],
                ]);

            } else {
                if(isset($redpacketData['all_consume'])) {
                    $res = $redpacketLog->where(['id' => $redpacketInfo['id']])->save($redpacketData);
                }
            }
        }
    }

    //给服务端发送获取游戏数据请求
    public function sendgm_oprate($uid)
    {
        $uid = I('uid');
        //发送给游戏服务端
        $post_url = C('GM_URL') .'cgi-bin/gm_oprate:get_player_game_info';

        $arr['uid'] = $uid;
        $field = http_build_query($arr);
        $ret = PostUrl($post_url, $field);
        json_decode($ret, true);

        redirect('/Task/user');

    }


    //刷新用户数据
    public function sendGMRefresh(){
        $uid = I('uid');

        //发送给游戏服务端
        $post_url = C('GM_URL') .'cgi-bin/gm_oprate:reflush_user';
        $sendPost['uid'] = $uid;
        $field = http_build_query($sendPost);
        $ret = PostUrl($post_url, $field);
        $this->success('成功');
    }

    //修复用户数据
    public function sendGMFixed(){
        $account = I('uid');
        $uid = I('char_id');
       
        $recharge_money = I('recharge_money');
        $data1 = I('data1');
        $data2 = I('data2');
        $data3 = I('data3');

        //发送给游戏服务端
        $post_url = C('GM_URL') .'cgi-bin/gm_oprate:repair_user';
        $sendPost['recharge_money'] = $recharge_money;
        $sendPost['uid'] = $uid;
        $sendPost['data1'] = $data1?$data1:0;
        $sendPost['data2'] = $data2?$data2:0;
        $sendPost['data3'] = $data3?$data3:0;
        $field = http_build_query($sendPost);
        $ret = PostUrl($post_url, $field);
        //记录
       
        D('DAmountLog')->add(['uid' => $uid,'account' => $account,  'amount' =>$recharge_money, 'get_gold1' => $data1 , 'get_gold2' => $data2, 'get_gold3' => $data3, 'c_time' => time(), 'c_date' => date("Y-m-d H:i:s"), 'operate' => $_SESSION['username']]);
        //print(D('DAmountLog')->getLastSql());
        $this->success('成功');
    }


    //查看关卡人数
    public function levelLimit()
    {
        header("Content-Type:text/html;charset=utf-8");
        $config_id = I('config_id');
        $taskLevelInfoModel = D('TaskLevelInfo');
        $taskStatModel = D('TaskStat');
        $userModel = D('Users');

        $taskConfig = D("TaskConfig")->where(['id' => $config_id])->find();
        if($taskConfig && $taskConfig['limit'] > 0) {

            $limit = $taskConfig['limit'];
            $limitNow = $taskLevelInfoModel->where(['task_config_id' => $config_id])->count(); //从第几个开始计算

            if($limitNow >= $limit) {
                //数据已经存在了，直接查询出来
                $list = $taskLevelInfoModel->field('uid,name')->where(['task_config_id' => $config_id])->select();
            } else {
                //否则没有数据 就要生成数据
                $pageSize = 100;
                //查询出来符合的用户记录
                $statList = $taskStatModel
                    ->where(['task_config_id' => $config_id])
                    ->order('addtime asc')
                    ->limit($limitNow, $pageSize)
                    ->getField('uid', true);

                if(count($statList) > 0) {
                    foreach ($statList as $val) {
                        $name = $userModel->where(['id' => $val])->getField('wxnick');
                        $taskLevelInfoModel->add(['uid' => $val, 'name' => $name, 'task_config_id' => $config_id]);
                    }
                    redirect(U('Task/levelLimit', ['config_id' => $config_id]),1, '请稍等..正在生成数据，请勿刷新关闭!');
                }
                //没有数据了，直接查询出来
                $list = $taskLevelInfoModel->field('uid,name')->where(['task_config_id' => $config_id])->select();
            }
            echo "目前人数：".count($list). '人，当前关卡：【'. $taskConfig['desc']."】\n";
            foreach ($list as $value) {
                echo "<div>{$value['uid']} {$value['name']}</div>";
            }

            exit;
        }

        echo '暂未设置限制人数';exit;

    }

    //信息流
    public function index(){
        
		$b_time = I('b_time');
        if(empty($b_time)) {
            $b_time = date('Y-m-d', NOW_TIME-(5 * 24 * 60 * 60));
        }
        $e_time = I('e_time');
        if(empty($e_time)) {
            $e_time = date('Y-m-d', NOW_TIME+86400);
        }
        $from_id = I('from_id');
        if(empty($from_id)){
            $from_id = D('DChannels')->order('create_time desc')->getField('from_id'); 
        }
        if('9999'==$from_id)
        {
            $list=D('DUserDayStats')->get_xin_xi_liu_stat('',$b_time,$e_time);
        }
        else
        {
            $list=D('DUserDayStats')->get_xin_xi_liu_stat($from_id,$b_time,$e_time);
        }
        
        $statlist=D('DUserDayConsume')->select();

        //数据处理
        $newList = array(); //构建新返回数据 
        foreach ($list as $item) {
            //消耗和收益汇总
            $item['consume']=0;
            foreach ($statlist as $stat) {
                if($stat['from_id'] == $item['from_id'] && $stat['from_time'] == $item['from_time'] && $stat['reg_day'] == $item['reg_day'])
                {
                    $item['consume']= $stat['consume'];
                }
            }
            $pay_count=$item['pay1']+$item['pay2']+$item['pay3']+$item['pay4']+$item['pay5']+$item['pay6']+$item['pay7']+$item['pay8']+$item['pay9']+$item['pay10']+$item['pay11']+$item['pay12'];
            $red_count=$item['red1']+$item['red2']+$item['red3']+$item['red4']+$item['red5']+$item['red6']+$item['red7']+$item['red8']+$item['red9']+$item['red10']+$item['red11']+$item['red12'];
            $item['allpay']=$pay_count-$red_count/100-$item['consume'];
            if(0==$item['consume']){$item['roi']=0;}else{$item['roi']=number_format(($pay_count)/$item['consume'],4);}
            $newList[] = $item;
        }
        $sumRow = array(); // 用于存储每列的汇总数据

        foreach ($newList as $row) {
            // 在循环中汇总每列的数据
            foreach ($row as $columnName => $columnValue) 
            {
                if($columnName=="reg_day"||
                $columnName=="from_id"||
                $columnName=="from_time")
                {
                    if (!isset($sumRow[$columnName])) 
                    {
                        $sumRow[$columnName] = '';
                    }
                }
                else
                {
                    if (!isset($sumRow[$columnName])) {
                        $sumRow[$columnName] = 0;
                    }
                    $sumRow[$columnName] += $columnValue;
                }
                
            }
        }
        //roi 特殊处理
        if(0==$sumRow['consume'])
        {
            $sumRow['roi']=0;
        }
        else
        {
            $pay_countn=$sumRow['pay1']+$sumRow['pay2']+$sumRow['pay3']+$sumRow['pay4']+$sumRow['pay5']+$sumRow['pay6']+$sumRow['pay7']+$sumRow['pay8']+$sumRow['pay9']+$sumRow['pay10']+$sumRow['pay11']+$sumRow['pay12'];
            $red_countn=$sumRow['red1']+$sumRow['red2']+$sumRow['red3']+$sumRow['red4']+$sumRow['red5']+$sumRow['red6']+$sumRow['red7']+$sumRow['red8']+$sumRow['red9']+$sumRow['red10']+$sumRow['red11']+$sumRow['red12'];
            $sumRow['roi']=number_format(($pay_countn)/$sumRow['consume'],4);
        }
        if('9999'==$from_id)
        {
            $newList = array();
            $newList[]=$sumRow;
        }
        else
        {
            $newList[]=$sumRow;
        }
        
        


        $fromkey = D('DChannels')->order('id asc')->select();
        // 添加全部查询项
        $fromRow = array();
        $fromRow['from_id']='9999';
        $fromRow['from_name']='全部渠道';
        $fromkey[]=$fromRow;
 
        $this->assign('from', $fromkey);
        $this->assign('_list', $newList);
        $this->assign('from_id', $from_id);
        $this->display();
    }

    //信息流
    public function thisdayold(){
        
		$list=D('DUserDayStats')->get_this_day();
        
        $statlist=D('DUserDayConsume')->select();
 
        $sumRow = array(); // 用于存储每列的汇总数据

        foreach ($list as $row) {
            // 在循环中汇总每列的数据
            foreach ($row as $columnName => $columnValue) 
            {
                if($columnName=="from_id"|| $columnName=="from_name")
                {
                    if (!isset($sumRow[$columnName])) 
                    {
                        $sumRow[$columnName] = '';
                    }
                }
                else
                {
                    if (!isset($sumRow[$columnName])) {
                        $sumRow[$columnName] = 0;
                    }
                    $sumRow[$columnName] += $columnValue;
                }
                
            }
        }
        $list[]=$sumRow; 
        

 
        $this->assign('_list', $list);
        $this->display();
    }

    //信息流
    public function thisday()
    {
        $value = I('value');
        $b_time = I('b_time');
        $e_time = I('e_time');

        if (isset($_GET['b_time']) || isset($_GET['e_time'])) {
            if ($b_time == '' || $e_time == '') {
                $this->error('查询时间请填写完整');
            } else {
                $b_time = $b_time.' 00:00:00';
                $e_time = $e_time.' 23:59:59';
            }
        } else {
            $b_time = date('Y-m-d 00:00:00', NOW_TIME);
            $e_time = date('Y-m-d 23:59:59', NOW_TIME);
        }

		$list=D('DUserDayStats')->get_this_day_by_time($value,$b_time,$e_time);
 
        $sumRow = array(); // 用于存储每列的汇总数据

        foreach ($list as $row) {
            // 在循环中汇总每列的数据
            foreach ($row as $columnName => $columnValue) 
            {
                if($columnName=="from_id"|| $columnName=="from_name")
                {
                    if (!isset($sumRow[$columnName])) 
                    {
                        $sumRow[$columnName] = '';
                    }
                }
                else
                {
                    if (!isset($sumRow[$columnName])) {
                        $sumRow[$columnName] = 0;
                    }
                    $sumRow[$columnName] += $columnValue;
                }
                
            }
        }
        $list[]=$sumRow; 
        

 
        $this->assign('_list', $list);
        $this->display();
    }

    public function editindex()
    {
        $from_id = I('from_id');
        $from_time = I('from_time');
        $reg_day = I('reg_day');
        
        $map['from_id'] = $from_id;
        $map['from_time'] = $from_time;
        $map['reg_day'] = $reg_day;
        $keyinfo = D('DUserDayConsume')->where($map)->find();

        if (IS_POST) {
            $from_time = trim(I('post.from_time'));
            $from_id = trim(I('post.from_id'));
            $reg_day = trim(I('post.reg_day'));
            $consume = trim(I('post.consume'));

            if ($from_time == '') {
                $this->error('渠道期数不能为空');
            }

            if ($from_id == '') {
                $this->error("渠道id不能为空");
            }

            if ($reg_day == '') {
                $this->error("注册时间不能为空");
            }

            if ($consume == '') {
                $this->error("渠道消耗不能为空");
            }

            $cmap['from_time'] = $data['from_time'] = $from_time;
            $cmap['from_id'] = $data['from_id'] = $from_id;
            $cmap['reg_day'] = $data['reg_day'] = $reg_day;
            $dataup['consume'] = $data['consume'] = $consume;
            $dataup['updated_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
            $data['created_at'] = date("Y-m-d H:i:s");

            $consume_list= D('DUserDayConsume')->where($cmap)->find();
            if(empty($consume_list))
            {
                D('DUserDayConsume')->add($data);
                $this->success('新增成功', U('index'));
            }
            else{
                D('DUserDayConsume')->where($cmap)->save($dataup);
                $this->success('修改成功', U('index'));
            }
            
  

            
 


            
        }
        else if(empty($keyinfo))
        {
            $this->assign('keyinfo', $map);
            $this->display();
        } 
        else {
            $this->assign('keyinfo', $keyinfo);
            $this->display();
        }
    }

    /**
     * 晶石 获得/消耗日志
     */
    public function exchangeold($p = 1)
    {
        $limit = 20;

        $query = '';
        $type = intval(I('type'));
        $value = I('value');
        //查询条件拼凑
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = D('DUsers')->get_charid('uid', intval($value));
                    break;
                case 1: //角色ID
                    $map['uid'] = intval($value);
                    break;
                case 2: //手机号
                    $map['uid'] = D('DUsers')->get_charid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $map['uid'] = D('DUsers')->get_charid('nickname', trim($value));
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = I('b_time');
        $e_time = I('e_time');
        
        if(empty(I('b_time')) && empty(I('e_time')))
		{
			$b_time = date("Y-m-d H:i:s", strtotime("-1 month"));
			$e_time = date("Y-m-d H:i:s");
            $map['created_at'] = array('between', array($b_time, $e_time));
		}
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['created_at'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }
		
        $model = D('DItemLogs');
        $map['num'] = array('lt', 0);
        $map['item_id'] = 103;
        //默认加载就显示总榜，有输入key 就查询个人榜单 默认当前一个月
        if (isset($_GET['type']) && isset($_GET['value'])) 
        {
            //显示具体信息
            $totals = $model->where($map)->count();
            $list = $model->where($map)->order('created_at desc')->page($p, $limit)->select();
        }
        else
        {
            //显示总榜
            $subQuery = $model->where($map)->group('uid')->select(false);
            $totals= $model->table($subQuery . ' a')->count();
            $list = $model->where($map)->field('uid,SUM(num) as num, MAX(created_at) as created_at')
            ->group('uid')->order('num')->page($p, $limit)->select();
        }
        // 补充account 数据
        foreach ($list as $k=> $v) {
            $list[$k]['account'] = D('DUsers')->get_uid('char_id', intval($v['uid']));
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 兑换排行榜
     */
    public function exchange($p = 1)
    {
        $limit = 20;

        $query = '';
        $type = intval(I('type'));
        $value = I('value');
        //查询条件拼凑
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['account'] = intval($value);
                    break;
                case 1: //角色ID
                    $map['account'] = intval($value);
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = I('b_time');
        $e_time = I('e_time');
        
        if(empty(I('b_time')) && empty(I('e_time')))
		{
			$b_time = date("Y-m-d H:i:s", strtotime("-1 month"));
			$e_time = date("Y-m-d H:i:s");
            $map['created_at'] = array('between', array($b_time, $e_time));
		}
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['created_at'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }
		
        $model = D('DRedPackages');
        //$map['task_id'] = array('in', array(99011, 99012));
        $map['status'] = ['in' , [0,888]];
        //默认加载就显示总榜，有输入key 就查询个人榜单 默认当前一个月
        if (isset($_GET['type']) && isset($_GET['value'])) 
        {
            //显示具体信息
            $totals = $model->where($map)->count();
            $list = $model->where($map)->order('created_at desc')->page($p, $limit)->select();
        }
        else
        {
            //显示总榜
            $subQuery = $model->where($map)->group('account')->select(false);
            $totals= $model->table($subQuery . ' a')->count();
            $list = $model->where($map)->field('account,SUM(amount) as amount, MAX(created_at) as created_at')
            ->group('account')->order('amount desc,created_at desc')->page($p, $limit)->select();
        }
        

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 渠道文件上传
     */
    public function upload()
    {
        
		//文件上传处理  
		$upload = new \Think\Upload();// 实例化上传类
	    $upload->maxSize   =     3145728 ;// 设置附件上传大小
	    $upload->exts      =     array('xlsx');// 设置附件上传类型
	    $upload->rootPath  =     './config/'; // 设置附件上传根目录
        $upload->saveName  = 'channel_plan';
        $upload->replace  = true;
        $upload->autoSub  = false;
	    // 上传文件 
	    $info   =  $upload->upload();
	 	//$data['mimg'] = '/Public/'.$info['mimg']['savepath'].$info['mimg']['savename'];
         if ($info)
         {
            $this->success("添加成功",U("Task/taskConf"));
         }
         else 
         {
             $this->error($upload->getError());
         }
         
    }
    /**
     * 新渠道任务
     **/
    public function fromCount() {
        
        $from_id = I('from_id');
        $from_time = I('from_time');
        if(empty($from_id)){$from_id=1000;}
        if(empty($from_time)){$from_time=1;}

        $value = I('values');
        $b_time = I('b_time');
        $list_con=0;

        if(!empty($from_id) && !empty($from_time)) {

            $taskConfig = D('DChannelTasks');
            $statModel = D('DTaskStat');
            $fromModel = D('DChannels');
            $taskConsumeModel = D('DTaskCost'); //消耗表

            $umap['from_id'] = $map['from_id'] = $from_id;
            $umap['from_time'] = $map['from_time'] = $from_time;

            $flist = $taskConfig->where($map)->order('`task_type` desc, `room_type` desc ,value+0 asc')->select();  //查询渠道配置内容
            
            
            
            $rooms = D('DRooms')->rooms_new();  //房间
            if(!empty($value)){$smap['account']=$value;$umap['account'] =$value;}
            if(!empty($b_time)){$smap['DATE(FROM_UNIXTIME(addtime))']=$b_time;$umap['DATE(FROM_UNIXTIME(addtime))']=$b_time;}
            
            $list = [];
            $userlist = $statModel->where($umap)->field('DISTINCT account')->select();
            foreach($userlist as $useritem)
            {
                $listitem = [];
                $listlist = [];
                $taskConsume=0;
                $leisure=[];
                $listitem['account']=$useritem['account'];
                foreach ($flist as $item) {
                    $smap['task_config_id']=$item['id'];
                    $smap['account']=$useritem['account'];

                    $count = $statModel
                        ->join('game1.d_channel_tasks as c ON c.id = d_task_stat.task_config_id')
                        ->where($smap)->count(); // 查询 Stats

                    $listlist[$item['id']] = ($count * $item['reward']);

                    $typename=$rooms[$item['room_type']]?$rooms[$item['room_type']]:"";
                    if(!empty($typename)){$leisure[$typename]+= ($count * $item['reward']);$taskConsume +=($count * $item['reward']);}
                }
                $listitem['listlist']=$listlist;
                $listitem['leisure']=$leisure;
                $listitem['taskConsume']=$taskConsume;
                $list_con+=$taskConsume;
                $list[]=$listitem;
            }
            

            //var_dump($leisure);exit;


            $from = $fromModel->where(['from_id' => $from_id, 'from_time' => $from_time])->find();
            $from_name = $from['from_name'];
             

        }
        // echo json_encode($flist);exit;

        $fromkey = D('DChannels')->field('from_id as id,from_time as num, from_name')->order('from_id asc')->select();
        $this->assign('from', $fromkey);
       
        $this->assign('list_data', $b_time);
        $this->assign('list_form', $from_id);
        $this->assign('list_con', $list_con);

        $this->assign('all_consume', $taskConsume ? $taskConsume: 0);
        $this->assign('leisure', $leisure);
        $this->assign('from_name', $from_name);
        $this->assign('from_id', $from_id);
        $this->assign('rooms', $rooms);
        $this->assign('from_time', $from_time);
        $this->assign('task_title', $flist);
        $this->assign('_list', $list);
        $this->display();
    }
	/**
     * 渠道兑换统计
     */
    public function tasklog($p = 1)
    {
        $limit = 50;
        $query = ''; //排序条件

        $model = D('DTaskStat');

        $type = intval(I('type'));
        $value = I('values');
        if (isset($_GET['type']) && isset($_GET['values'])) {
            switch ($type) {
                case 0:
                    $map['uid'] = intval($value);
                    break;
                case 1:
                    $map['account'] = intval($value);
                    break;
            }
            $query .= 'type/' . $type . '/';
            $query .= 'values/' . $value . '/';
        }


        if (!isset($_GET['from_id'])) {
            $from_id = -1;
        } else {
            $from_id = intval(I('from_id'));
            $map['d_task_stat.from_id'] = $from_id;
            $query .= 'from_id/' . $from_id . '/';
        }

        

        $b_time = I('b_time');
        $e_time = I('e_time');
        if (isset($_GET['b_time'])) {
            $query .= 'b_time/' . $b_time. '/';
            $b_time = strtotime($b_time);
        }else{$b_time = strtotime(date('Y-m-d 00:00:00', strtotime('-30 days')));}

        if (isset($_GET['e_time'])) {
            $query .= 'e_time/' .  $e_time. '/';
            $e_time = strtotime(I('e_time')) + 86399;
        }else{$e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));}
        $between = [$b_time, $e_time];
        $map['d_task_stat.addtime'] = ['BETWEEN', $between];

        $orders = 'd_task_stat.id desc';
	
        $list_count =$totals = $model->where($map)->join('game1.d_channel_tasks as f ON f.id = d_task_stat.task_config_id', 'left')->count();
        $list   = $model->where($map)->join('game1.d_channel_tasks as f ON f.id = d_task_stat.task_config_id', 'left')->field('d_task_stat.*,f.reward,f.`desc`,f.`value` ')->page($p, $limit)->order($orders)->select();
        $pay_count  = $model->where($map)->join('game1.d_channel_tasks as f ON f.id = d_task_stat.task_config_id', 'left')->sum('reward');
        $user_num=$model->where($map)->join('game1.d_channel_tasks as f ON f.id = d_task_stat.task_config_id', 'left')->count('DISTINCT uid');
         
  
         //充值金额

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
 

        $this->assign('from_id', $from_id);
        $fromkey = D('DChannels')->field('DISTINCT from_id as id,from_name')->order('from_id asc')->select();
        

        $this->assign('query', $query);
        $this->assign('from', $fromkey);
        $this->assign('user_num', $user_num);
        $this->assign('list_count', $list_count);
        $this->assign('pay_count', $pay_count);
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

}

