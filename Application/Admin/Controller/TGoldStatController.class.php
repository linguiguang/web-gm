<?php

/**
 * 默认控制器
 */

namespace Admin\Controller;

use Think\Controller;

class TGoldStatController extends Controller
{

	///opt/aiya/nginx/fastcgi/bin/php admin.php stat/run
	/*
	 * 金币统计  统计任务控制器
	 * user_room
	 */
	public function stat()
	{
		$goldgetuse = D('DUserRooms');
		$stat    = D('DGoldstatday');
		$field   = 'gold';
		$this->run($goldgetuse, $stat, $field);
	}

	/**
	 * @param $get 获得/消耗模型
	 * @param $stat 统计模型
	 * @param $field 字段
	 */
	public function run($get,$stat, $field)
	{

		$stime = microtime(true); #获取程序开始执行的时间
		$d_time = NOW_TIME;

		if ($yes_time = stat_zero_time($d_time)) {
			$b_time = date('Y-m-d 00:00:00',$yes_time['b_time']);
			$e_time = date('Y-m-d 23:59:59',$yes_time['e_time']);
			$t_time = date('Y-m-d',$yes_time['b_time']);
		} else {
			$b_time = date('Y-m-d 00:00:00', $d_time);
			$e_time = date('Y-m-d 23:59:59', $d_time);
			$t_time = date('Y-m-d',$d_time);
		}

		$map['created_at'] = array('between', array($b_time, $e_time));
		// $map['uid'] = array('egt', 150000);
		
		$rooms = D('DRooms')->rooms();  //房间
        $room = array();
        foreach ($rooms as $k => $v) {
            $room[] = intval($k);
        }	

		if ($field == 'gold') {
			$map['room_type'] = array('in', $room); // 房间号1,2,3
		}

		//获得
		$arr_get = $arr_use = array();
		$get_list = $get->where($map)->field("room_type,total_win,total_cost")->group("room_type")->select();
		if ($field == 'gold') {
		
			// goldget
			// room_type,total_win,total_cost		
			$dt = $t_time;  //报表日期
			foreach ($get_list as $k => $val) {			
				//总赢取 消耗
				$arr_get[$val['room_type']]['total_win'] = $val['total_win'];
				$arr_use[$val['room_type']]['total_cost'] = $val['total_cost'];
			
			}
			unset($get_list);
			echo 'getlogs' . PHP_EOL;

			// 金币报表统计
			
			$data['date_at'] = $dt;
			$data['get_gold_'.$room[0]] = intval($arr_get[$room[0]]['total_win']);
			$data['get_gold_'.$room[1]] = intval($arr_get[$room[1]]['total_win']);
			$data['get_gold_'.$room[2]] = intval($arr_get[$room[2]]['total_win']);
			$data['use_gold_'.$room[0]] = intval($arr_get[$room[0]]['total_cost']);
			$data['use_gold_'.$room[1]] = intval($arr_get[$room[1]]['total_cost']);
			$data['use_gold_'.$room[2]] = intval($arr_get[$room[2]]['total_cost']);
			$data['updated_at'] = $d_time;
			$sap['date_at'] = $dt;	
			if ($info = $stat->where($sap)->find()) {
				$stat->where(array('id' => $info['id']))->save($data);
			} else {
				$data['created_at'] = $d_time;
				$stat->add($data);
			}
			echo 'SUCCESS' . PHP_EOL;

			$etime = microtime(true);
			$total = round($etime - $stime);
			echo "Run {$total}s times" . PHP_EOL;
		}
	}

	/**
	 * 金币消耗和统计，修复数据方法
	 */
	protected function operate_data($get, $use)
	{

		if ($get > $use) {
			$data['get'] = $get - $use;
			$data['use'] = 0;
		} else {
			$data['get'] = 0;
			$data['use'] = $use - $get;
		}

		return $data;
	}

	/**
	 * 用户金币统计
	 */
	public function usegoldstat($runtype = 'new')
	{

		$stime = microtime(true); #获取程序开始执行的时间

		$goldget = D('Goldget');
		$golduse = D('Golduse');
		$usergoldstat = D('Usergoldstat');
		$userinfo = D('Userinfo');
		$Caruser = D('Caruser');
		$Brnnuser  = D('Brnnuser');
		$Fruituser = D('Fruituser');

		//总角色
		$user_list = $userinfo->field('uid')->select();
		$uids = array();
		foreach ($user_list as $v) {
			$uids[] = $v['uid'];
		}
		unset($user_list);

		//拉霸数据
		$area_2 = $area_3 = array();
		$fruitusers = $Fruituser->select();
		foreach ($fruitusers as $v) {
			$area_3[$v['uid']] = $v;
		}

		//百人牛牛数据
		$brn['uid'] = array('egt', 150000);
		$brnnusers = $Brnnuser->where($brn)->select();
		foreach ($brnnusers as $v) {
			$area_2[$v['uid']] = $v;
		}

		//豪车数据
		$carusers = $Caruser->select();
		foreach ($carusers as $v) {
			$area_4[$v['uid']] = $v;
		}

		//统计的区域
		$area_arr = array(10, 11);

		if ($runtype == 'all') {
			$d_time = strtotime('2017-06-07');

			$b_time = strtotime(date('Y-m-d 00:00:00', $d_time));
			$e_time = strtotime(date('Y-m-d 19:00:00', NOW_TIME));
			$this->stattime('GOLDSTAT_LAST_TIME', $e_time);
			echo "stop time $e_time \r\n";
		} else {
			$last_time = $this->stattime('GOLDSTAT_LAST_TIME');
			if (empty($last_time)) die('Last time is loss!');

			$b_time = $last_time + 1;
			$e_time = $b_time + 1799;
			$this->stattime('GOLDSTAT_LAST_TIME', $e_time);
		}

		$map['c_time'] = array('between', array($b_time, $e_time));
		$map['uid'] = array('egt', 150000); //排除AI数据
		//5个战斗区域
		$map['room'] = array('in', $area_arr);

		$get_list = $goldget->where($map)->select();
		$uid_get = $userget = array();
		foreach ($get_list as $val) {
			if (in_array($val['room'], $area_arr)) {
				$uid_get[$val['uid']][10] += intval($val['gold'] * 0.95);
				$userget[$val['uid']] += intval($val['gold'] * 0.95);   //每个用户总获得
			}
		}
		unset($get_list);
		echo "getgold_list ok \r\n";

		$use_list = $golduse->where($map)->select();
		$uid_use = $useruse = array();
		foreach ($use_list as $val) {
			if (in_array($val['room'], $area_arr)) {
				$uid_use[$val['uid']][10] += $val['gold'];
				$useruse[$val['uid']] += $val['gold'];    //每个用户总支出
			}
		}
		unset($use_list);
		echo "usegold_list ok \r\n";


		//看牌抢庄、拉霸、百人牛牛数据
		$area_all = array(10, 2, 3);

		foreach ($uids as $k => $v) {

			$uid = intval($v);

			$sap['uid'] = $uid;
			$user_info = $userinfo->where($sap)->find();
			$gold_stat = intval($user_info['gold_stat']); //旧的金币统计数据

			//拉霸
			$uid_get[$uid][3] = $area_3[$uid]['get'] + $area_3[$uid]['pool'];
			$uid_use[$uid][3] = $area_3[$uid]['use'];

			//百人牛牛
			$uid_get[$uid][2] = $area_2[$uid]['get'] + $area_2[$uid]['pool'];
			$uid_use[$uid][2] = $area_2[$uid]['use'];

			//总共输赢
			$userget[$uid] += intval($uid_get[$uid][3] + $uid_get[$uid][2]);
			$useruse[$uid] += intval($uid_use[$uid][3] + $uid_use[$uid][2]);

			foreach ($area_all as $key => $val) {

				if ($uid_get[$uid][$val] || $uid_use[$uid][$val]) {
					$ret['uid'] = $uid;
					$ret['room'] = intval($val);
					$ret['win'] = intval($uid_get[$uid][$val]);
					$ret['lose'] = intval($uid_use[$uid][$val]);
					$ret['stat'] = intval($ret['win'] - $ret['lose']);

					$uap['uid'] = $uid;
					$uap['room'] = $ret['room'];
					if ($log = $usergoldstat->where($uap)->find()) {

						if ($runtype == 'new') {

							if ($val == 3) {
								$ret['win'] = intval($uid_get[$uid][3]);
								$ret['lose'] = intval($uid_use[$uid][3]);
							} elseif ($val == 2) {
								$ret['win'] = intval($uid_get[$uid][2]);
								$ret['lose'] = intval($uid_use[$uid][2]);
							} else {
								$ret['win'] = intval($log['win'] + $uid_get[$uid][$val]);
								$ret['lose'] = intval($log['lose'] + $uid_use[$uid][$val]);
							}
							$ret['stat'] = intval($ret['win'] - $ret['lose']);
						}

						$usergoldstat->where(array('_id' => $log['_id']))->save($ret);
					} else {
						$usergoldstat->add($ret);
					}
				}

				//玩家信息输赢统计排序字段
				if ($runtype == 'new') {

					//拉霸，豪车,百人牛牛单独统计
					if ($val == 3) {
						$stat['area_' . $val] = intval($uid_get[$uid][$val]) - intval($uid_use[$uid][$val]);
					} elseif ($val == 2) {
						$stat['area_' . $val] = intval($uid_get[$uid][$val]) - intval($uid_use[$uid][$val]);
					} else {
						$stat['area_' . $val] = intval($user_info['area_' . $val]) + intval($uid_get[$uid][$val]) - intval($uid_use[$uid][$val]);
					}

					$gold_stats = $gold_stat + intval($userget[$uid]) - intval($useruse[$uid]) - intval($user_info['area_3']) - intval($user_info['area_2']);
				} else {
					$gold_stats = intval($userget[$uid]) - intval($useruse[$uid]);
					$stat['area_' . $val] = intval($uid_get[$uid][$val]) - intval($uid_use[$uid][$val]);
				}
			}
			$stat['gold_stat'] = $gold_stats;
			$userinfo->where(array('_id' => $user_info['_id']))->save($stat);
		}

		echo 'SUCCESS' . PHP_EOL;

		$etime = microtime(true);
		$total = round($etime - $stime);
		echo "Run {$total}s times" . PHP_EOL;
	}

	/**
	 * 存取时间
	 */
	protected function stattime($name, $val)
	{
		$stattime = D('Stattime');
		$map['name'] = trim($name);

		$info = $stattime->where($map)->find();

		$data['time'] = $val;
		if ($val) {
			if ($info) {
				$stattime->where($map)->save($data);
			} else {
				$data['name'] = $name;
				$stattime->add($data);
			}
		} else {
			return $info['time'];
		}
	}

	//旧金币统计，从goldget和golduse表读取数据
	//==================================================================
	//用户金币统计测试
	//总金币统计为 总获得 和总消耗
	//房间统计 只需统计游戏中的输赢
	//==================================================================
	public function use_goldstat_test($runtype = 'new')
	{

		$stime = microtime(true); #获取程序开始执行的时间

		$goldget = D('Goldget');
		$golduse = D('Golduse');
		$usergoldstat = D('Usergoldstat');
		$userinfo = D('Userinfo');
		// $Caruser = D('Caruser');
		$Brnnuser  = D('Brnnuser');
		$Fruituser = D('Fruituser');

		//总角色
		$uls['isrobot'] = 0;
		$user_list = $userinfo->where($uls)->field('uid')->select();
		$uids = array();
		foreach ($user_list as $v) {
			$uids[] = $v['uid'];
		}
		unset($user_list);

		//拉霸数据
		$area_2 = $area_3 = $area_4 = array();
		$fruitusers = $Fruituser->select();
		foreach ($fruitusers as $v) {
			$area_3[$v['uid']] = $v;
		}
		unset($fruitusers);

		//百人牛牛数据
		$brn['uid'] = array('egt', 150000);
		$brnnusers = $Brnnuser->where($brn)->select();
		foreach ($brnnusers as $v) {
			$area_2[$v['uid']] = $v;
		}
		unset($brnnusers);

		//豪车数据
		/* $carusers = $Caruser->select();
		foreach ($carusers as $v){
			$area_3[$v['uid']] = $v;
		}
        unset($carusers); */

		//单独统计的区域(拉霸，百人牛牛，豪车)
		$area_arr = array(10, 11); //牛牛结算，入场费

		$uid_get = $uid_use = $userget = $useruse = array();

		$map['uid'] = array('egt', 150000); //获得，消耗数据排除AI

		if ($runtype == 'all') {
			$d_time = strtotime('2017-06-07');

			$b_time = strtotime(date('Y-m-d 00:00:00', $d_time));
			$e_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
			$this->stattime('GOLDSTAT_LAST_TIME', $e_time);
			echo "stop time $e_time \r\n";

			for ($i = 0; $i < ($e_time - $b_time) / 86400; $i++) {
				$btime = $i * 86400 + $b_time;
				echo date('Y-m-d', $btime) . "\r\n";
				$etime = $btime + 86399;

				$map['c_time'] = array('between', array($btime, $etime));
				$map['room'] = array('in', $area_arr);

				$get_list = $goldget->where($map)->field('uid,gold,room')->select();
				foreach ($get_list as $val) {
					if ($val['room'] == 10) { //看牌抢庄扣税5%
						$val['gold'] = intval($val['gold'] * 0.95);
					}
					$uid_get[$val['uid']][$val['room']] += $val['gold'];
					$userget[$val['uid']] += $val['gold'];   //每个用户总获得
				}

				$use_list = $golduse->where($map)->field('uid,gold,room')->select();
				foreach ($use_list as $val) {
					$uid_use[$val['uid']][$val['room']] += $val['gold'];
					$useruse[$val['uid']] += $val['gold'];    //每个用户总支出
				}
			}

			echo "data ok \r\n";
		} else {

			//用于统计某段时间的金币
			/*$d_time = strtotime('2017-06-27');

			$b_time = strtotime(date('Y-m-d 00:00:00', $d_time));
			$e_time = strtotime(date('Y-m-d 09:30:00', NOW_TIME));
			$this->stattime('GOLDSTAT_LAST_TIME', $e_time);*/

			$last_time = $this->stattime('GOLDSTAT_LAST_TIME');
			if (empty($last_time)) die('Last time is loss!');

			$b_time = $last_time + 1;
			$e_time = $b_time + 1799;
			$this->stattime('GOLDSTAT_LAST_TIME', $e_time);

			$map['c_time'] = array('between', array($b_time, $e_time));
			$map['room'] = array('in', $area_arr);

			$get_list = $goldget->where($map)->select();
			foreach ($get_list as $val) {
				if ($val['room'] == 10) { //看牌抢庄扣税5%
					$val['gold'] = intval($val['gold'] * 0.95);
				}
				$uid_get[$val['uid']][$val['room']] += $val['gold'];
				//                $userget[$val['uid']] += $val['gold'];   //每个用户总获得
			}
			echo "getgold_list ok \r\n";

			$use_list = $golduse->where($map)->select();
			foreach ($use_list as $val) {
				$uid_use[$val['uid']][$val['room']] += $val['gold'];
				//				$useruse[$val['uid']] += $val['gold'];    //每个用户总支出
			}
			echo "usegold_list ok \r\n";
		}

		//遍历的房间(百人牛牛，拉霸，豪车，看牌抢庄, 牛牛入场费)
		$area_all = array(2, 3, 4, 10, 11);

		$no = 0; //用户查看更新用户数量

		foreach ($uids as $k => $v) {

			$uid = intval($v);

			$user_info = $userinfo->where(array('uid' => $uid))->find();
			//			$gold_stat = intval($user_info['gold_stat']);//旧的金币统计数据

			//拉霸
			$uid_get[$uid][3] = $area_3[$uid]['get'] + $area_3[$uid]['pool'];
			$uid_use[$uid][3] = $area_3[$uid]['use'];

			//百人牛牛
			$uid_get[$uid][2] = $area_2[$uid]['get'] + $area_2[$uid]['pool'];
			$uid_use[$uid][2] = $area_2[$uid]['use'];

			//豪车
			$uid_get[$uid][4] = $area_4[$uid]['get'] + $area_4[$uid]['pool'];
			$uid_use[$uid][4] = $area_4[$uid]['use'];

			//总共输赢
			/*$userget[$uid] += intval($uid_get[$uid][3] + $uid_get[$uid][2] + $uid_get[$uid][4]);
			$useruse[$uid] += intval($uid_use[$uid][3] + $uid_use[$uid][2] + $uid_use[$uid][4]);
            */
			foreach ($area_all as $key => $val) {

				$room = intval($val);

				/*if(!in_array($room, array(2, 3, 4, 10))){//房间数据只存这些room值
                    continue;
                }*/

				$ret['uid'] = $uid;
				$ret['room'] = $room;
				$ret['win'] = intval($uid_get[$uid][$room]);
				$ret['lose'] = intval($uid_use[$uid][$room]);
				$ret['stat'] = intval($ret['win'] - $ret['lose']);

				$uap['uid'] = $uid;
				$uap['room'] = $room;
				if ($log = $usergoldstat->where($uap)->find()) {
					if ($runtype == 'new') {

						if ($room == 10) { //看牌抢庄场
							$ret['win'] = intval($log['win'] + $uid_get[$uid][10]);
							$ret['lose'] = intval($log['lose'] + $uid_use[$uid][10]);
						} elseif ($room == 11) { //入场费结算
							$ret['win'] = intval($log['win'] + $uid_get[$uid][11]);
							$ret['lose'] = intval($log['lose'] + $uid_use[$uid][11]);
						} else {
							$ret['win'] = intval($uid_get[$uid][$room]);
							$ret['lose'] = intval($uid_use[$uid][$room]);
						}

						$ret['stat'] = intval($ret['win'] - $ret['lose']);
					}

					$usergoldstat->where(array('_id' => $log['_id']))->save($ret);
				} else {
					$usergoldstat->add($ret);
				}

				//拉霸，豪车,百人牛牛单独统计
				if ($room == 10) {

					if ($runtype == 'new') {
						$stat['area_10'] = intval($user_info['area_10']) + intval($uid_get[$uid][10]) - intval($uid_use[$uid][10]);
					} else {
						$stat['area_10'] = intval($uid_get[$uid][10] - $uid_use[$uid][10]);
					}
				} else {
					$stat['area_' . $room] = intval($uid_get[$uid][$room] - $uid_use[$uid][$room]);
				}

				//玩家信息输赢统计排序字段
				/*if($runtype == 'new'){
					$gold_stats = $gold_stat + intval($userget[$uid]) - intval($useruse[$uid]) - intval($user_info['area_3']) - intval($user_info['area_2']) - intval($user_info['area_4']);
				}else{
					$gold_stats = intval($userget[$uid]) - intval($useruse[$uid]);
				}*/
			}

			//            $stat['gold_stat'] = $gold_stats;
			$userinfo->where(array('_id' => $user_info['_id']))->save($stat);

			echo $uid . '->' . $no++ . "\r\n";
		}

		$etime = microtime(true);
		$total = round($etime - $stime);
		echo "Run {$total}s times" . PHP_EOL;
	}

	//用户金币输赢统计更改（改为身上金币 - 充值获得的金币）
	public function gold_stat()
	{

		/*$gold_pay = array(
            '10001'=>60000,
            '10002'=>300000,
            '10003'=>980000,
            '10004'=>1980000,
            '10005'=>3280000,
            '10006'=>6480000,
            '30003'=>126000,
            '30004'=>210000,
            '30005'=>315000,
            '30006'=>525000,
            '30007'=>840000,
            '30008'=>1050000,
            '30009'=>1575000,
            '30010'=>2100000,
            '30011'=>3150000,
            '30012'=>5250000,
            '4011'=>61000,//60000金币大礼包
            '4012'=>61000,//60000金币大礼包
            '4013'=>61000,//60000金币大礼包
            '4014'=>305000,//300000金币大礼包
            '4016'=>998000,//980000金币大礼包
            '4021'=>305000,//300000金币大礼包
            '4031'=>305000,//300000金币大礼包
            '50001'=>120000,//首充大礼包
            '60001'=>3000000,//至尊月卡
        );

        $map['state'] = 3;
        $paylog = D('Paylog')->where($map)->field('uid,saletype')->select();

        $goldget = array();
        foreach ($paylog as $v){
            $goldget[$v['uid']] += intval($gold_pay[$v['saletype']]);
        }
        echo "calcu golget ok \r\n";*/

		/*$rap['sale'] = 1;
		$redmarket = D('Redmarket')->where($rap)->select();

		$red_get = $red_use = array();
		foreach ($redmarket as $v){
			$red_get[$v['suid']] += $v['price'];
			$red_use[$v['buid']] += $v['price'];
		}
		echo "calcu red ok \r\n";*/

		/*$Userinfo = D('Userinfo');
        $userlist = $Userinfo->where(array('isrobot'=>0))->field('uid,gold')->select();
        $no = 0;
        foreach ($userlist as $v){
            $data['gold_stat'] = intval($v['gold']) - intval($goldget[$v['uid']]) + intval($red_get[$v['uid']]) - intval($red_use[$v['uid']]);
            $Userinfo->where(array('uid'=>$v['uid']))->save($data);
            echo $no++."\r\n";
        }*/ }

	//新金币统计，看牌抢庄数据从kplog读取，其它不变
	//其它数据单独统计
	public function use_goldstat_new()
	{
		$stime = microtime(true); #获取程序开始执行的时间

		$userinfo = D('Userinfo');
		$Arealogin = D('Arealogin');


		//查出当天有登录的玩家信息
		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$b_time = $yes_time['b_time'];
			$e_time = $yes_time['e_time'];
		} else {
			$b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
			$e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
		}

		$map['login_time'] = array('between', array($b_time, $e_time));
		$map['type'] = 1;
		$map['uid'] = array('egt', 150000);
		$list = $Arealogin->where($map)->field('uid')->select();

		echo date('Y-m-d 00:00:00', $dtime) . PHP_EOL;
		echo date('Y-m-d 23:59:59', NOW_TIME) . PHP_EOL;
		echo 'begintime' . time() . PHP_EOL;
		$uidsarr = array_column($list, 'uid');
		$uids = array_unique($uidsarr);
		echo 'endtime' . time() . '   uid nums:' . count($uids) . PHP_EOL;

		$everydaygold  = array();
		$fromstat = D('fromstat')->where(array('uid' => array('in', $uidsarr)))->select();

		foreach ($fromstat as $v) {

			$everydaygold[$v['uid']]['yinli1'] += intval($v['yinli1']);
			$everydaygold[$v['uid']]['yinli2'] += intval($v['yinli2']);
			$everydaygold[$v['uid']]['yinli'] += intval($v['yinli']);

			$everydaygold[$v['uid']]['2yinli1'] += intval($v['2yinli1']);
			$everydaygold[$v['uid']]['2yinli2'] += intval($v['2yinli2']);
			$everydaygold[$v['uid']]['2yinli'] += intval($v['2yinli']);

			// 机战
			$everydaygold[$v['uid']]['airyinli1'] += intval($v['airyinli1']);
			$everydaygold[$v['uid']]['airyinli2'] += intval($v['airyinli2']);
			$everydaygold[$v['uid']]['airyinli'] += intval($v['airyinli']);

			$everydaygold[$v['uid']]['3yinli1'] += intval($v['3yinli1']);
			$everydaygold[$v['uid']]['3yinli2'] += intval($v['3yinli2']);
			$everydaygold[$v['uid']]['3yinli'] += intval($v['3yinli']);

			$everydaygold[$v['uid']]['4yinli1'] += intval($v['4yinli1']);
			$everydaygold[$v['uid']]['4yinli2'] += intval($v['4yinli2']);
			$everydaygold[$v['uid']]['4yinli'] += intval($v['4yinli']);

			$everydaygold[$v['uid']]['6yinli1'] += intval($v['6yinli1']);
			$everydaygold[$v['uid']]['6yinli2'] += intval($v['6yinli2']);
			$everydaygold[$v['uid']]['6yinli'] += intval($v['6yinli']);

			$everydaygold[$v['uid']]['5yinli1'] += intval($v['5yinli1']);
			$everydaygold[$v['uid']]['5yinli2'] += intval($v['5yinli2']);
			$everydaygold[$v['uid']]['5yinli'] += intval($v['5yinli']);

			$everydaygold[$v['uid']]['get1'] += intval($v['get1']);
			$everydaygold[$v['uid']]['get2'] += intval($v['get2']);
			$everydaygold[$v['uid']]['get'] = intval($everydaygold[$v['uid']]['get1']) + intval($everydaygold[$v['uid']]['get2']);

			$everydaygold[$v['uid']]['2get1'] += intval($v['2get1']);
			$everydaygold[$v['uid']]['2get2'] += intval($v['2get2']);
			$everydaygold[$v['uid']]['2get'] = intval($everydaygold[$v['uid']]['2get1']) + intval($everydaygold[$v['uid']]['2get2']);

			// 机战
			$everydaygold[$v['uid']]['airget1'] += intval($v['airget1']);
			$everydaygold[$v['uid']]['airget2'] += intval($v['airget2']);
			$everydaygold[$v['uid']]['airget'] = intval($everydaygold[$v['uid']]['airget1']) + intval($everydaygold[$v['uid']]['airget2']);

			$everydaygold[$v['uid']]['3get1'] += intval($v['3get1']);
			$everydaygold[$v['uid']]['3get2'] += intval($v['3get2']);
			$everydaygold[$v['uid']]['3get'] = intval($everydaygold[$v['uid']]['3get1']) + intval($everydaygold[$v['uid']]['3get2']);

			$everydaygold[$v['uid']]['4get1'] += intval($v['4get1']);
			$everydaygold[$v['uid']]['4get2'] += intval($v['4get2']);
			$everydaygold[$v['uid']]['4get'] = intval($everydaygold[$v['uid']]['4get1']) + intval($everydaygold[$v['uid']]['4get2']);

			$everydaygold[$v['uid']]['6get1'] += intval($v['6get1']);
			$everydaygold[$v['uid']]['6get2'] += intval($v['6get2']);
			$everydaygold[$v['uid']]['6get'] = intval($everydaygold[$v['uid']]['6get1']) + intval($everydaygold[$v['uid']]['6get2']);

			$everydaygold[$v['uid']]['5get1'] += intval($v['5get1']);
			$everydaygold[$v['uid']]['5get2'] += intval($v['5get2']);
			$everydaygold[$v['uid']]['5get'] = intval($everydaygold[$v['uid']]['5get1']) + intval($everydaygold[$v['uid']]['5get2']);

			$everydaygold[$v['uid']]['gold_free'] += intval($v['gold_free']);
			$everydaygold[$v['uid']]['gold_task'] += intval($v['gold_task']);
			$everydaygold[$v['uid']]['red1003'] += intval($v['red1003']);
			$everydaygold[$v['uid']]['redtomoney'] += intval($v['redtomoney']);
			$everydaygold[$v['uid']]['redtogold'] += intval($v['redtogold']);
		}

		unset($fromstat);

		foreach ($uids as $k => $v) {
			$uid = intval($v);

			$everydaygold1['uid'] = $uid;

			$everydaygold1['yinli1'] = $everydaygold[$uid]['yinli1'];
			$everydaygold1['yinli2'] = $everydaygold[$uid]['yinli2'];
			$everydaygold1['yinli'] = $everydaygold[$uid]['yinli'];

			$everydaygold1['2yinli1'] = $everydaygold[$uid]['2yinli1'];
			$everydaygold1['2yinli2'] = $everydaygold[$uid]['2yinli2'];
			$everydaygold1['2yinli'] = $everydaygold[$uid]['2yinli'];

			// 机战
			$everydaygold1['airyinli1'] = $everydaygold[$uid]['airyinli1'];
			$everydaygold1['airyinli2'] = $everydaygold[$uid]['airyinli2'];
			$everydaygold1['airyinli'] = $everydaygold[$uid]['airyinli'];

			$everydaygold1['3yinli1'] = $everydaygold[$uid]['3yinli1'];
			$everydaygold1['3yinli2'] = $everydaygold[$uid]['3yinli2'];
			$everydaygold1['3yinli'] = $everydaygold[$uid]['3yinli'];

			$everydaygold1['4yinli1'] = $everydaygold[$uid]['4yinli1'];
			$everydaygold1['4yinli2'] = $everydaygold[$uid]['4yinli2'];
			$everydaygold1['4yinli'] = $everydaygold[$uid]['4yinli'];

			$everydaygold1['6yinli1'] = $everydaygold[$uid]['6yinli1'];
			$everydaygold1['6yinli2'] = $everydaygold[$uid]['6yinli2'];
			$everydaygold1['6yinli'] = $everydaygold[$uid]['6yinli'];

			$everydaygold1['5yinli1'] = $everydaygold[$uid]['5yinli1'];
			$everydaygold1['5yinli2'] = $everydaygold[$uid]['5yinli2'];
			$everydaygold1['5yinli'] = $everydaygold[$uid]['5yinli'];

			$everydaygold1['get1'] = $everydaygold[$uid]['get1'];
			$everydaygold1['get2'] = $everydaygold[$uid]['get2'];
			$everydaygold1['get'] = $everydaygold[$uid]['get'];

			$everydaygold1['2get1'] = $everydaygold[$uid]['2get1'];
			$everydaygold1['2get2'] = $everydaygold[$uid]['2get2'];
			$everydaygold1['2get'] = $everydaygold[$uid]['2get'];

			// 机战
			$everydaygold1['airget1'] = $everydaygold[$uid]['airget1'];
			$everydaygold1['airget2'] = $everydaygold[$uid]['airget2'];
			$everydaygold1['airget'] = $everydaygold[$uid]['airget'];

			$everydaygold1['3get1'] = $everydaygold[$uid]['3get1'];
			$everydaygold1['3get2'] = $everydaygold[$uid]['3get2'];
			$everydaygold1['3get'] = $everydaygold[$uid]['3get'];

			$everydaygold1['4get1'] = $everydaygold[$uid]['4get1'];
			$everydaygold1['4get2'] = $everydaygold[$uid]['4get2'];
			$everydaygold1['4get'] = $everydaygold[$uid]['4get'];

			$everydaygold1['6get1'] = $everydaygold[$uid]['6get1'];
			$everydaygold1['6get2'] = $everydaygold[$uid]['6get2'];
			$everydaygold1['6get'] = $everydaygold[$uid]['6get'];

			$everydaygold1['5get1'] = $everydaygold[$uid]['5get1'];
			$everydaygold1['5get2'] = $everydaygold[$uid]['5get2'];
			$everydaygold1['5get'] = $everydaygold[$uid]['5get'];

			$everydaygold1['gold_free'] = $everydaygold[$uid]['gold_free'];
			$everydaygold1['gold_task'] = $everydaygold[$uid]['gold_task'];
			$everydaygold1['red1003'] = $everydaygold[$uid]['red1003'];
			$everydaygold1['redtomoney'] = $everydaygold[$uid]['redtomoney'];
			$everydaygold1['redtogold'] = $everydaygold[$uid]['redtogold'];

			$uap['uid'] = $uid;
			if ($log = $userinfo->where($uap)->find()) {
				$userinfo->where(array('_id' => $log['_id']))->save($everydaygold1);
			}
		}

		echo 'run-date' . date('Y-m-d H:i', NOW_TIME) . PHP_EOL;

		$etime = microtime(true);
		$total = round($etime - $stime);
		echo "Run {$total}s times" . PHP_EOL;
	}


	public function use_goldstat_new_bk()
	{
		$stime = microtime(true); #获取程序开始执行的时间

		$userinfo = D('Userinfo');
		$Arealogin = D('Arealogin');


		//查出当天有登录的玩家信息
		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$b_time = $yes_time['b_time'];
			$e_time = $yes_time['e_time'];
		} else {
			$b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
			$e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
		}

		$map['login_time'] = array('between', array($b_time, $e_time));
		$map['type'] = 1;
		$map['uid'] = array('egt', 150000);
		$list = $Arealogin->where($map)->field('uid')->select();

		echo date('Y-m-d 00:00:00', $dtime) . PHP_EOL;
		echo date('Y-m-d 23:59:59', NOW_TIME) . PHP_EOL;
		echo 'begintime' . time() . PHP_EOL;
		$uidsarr = array_column($list, 'uid');
		$uids = array_unique($uidsarr);
		echo 'endtime' . time() . '   uid nums:' . count($uids) . PHP_EOL;

		$everydaygold  = array();
		$fromstat = array();
		$fromstat = D('fromstat')->select();

		foreach ($fromstat as $v) {

			$everydaygold[$v['uid']]['yinli1'] += intval($v['yinli1']);
			$everydaygold[$v['uid']]['yinli2'] += intval($v['yinli2']);
			$everydaygold[$v['uid']]['yinli'] += intval($v['yinli']);

			$everydaygold[$v['uid']]['2yinli1'] += intval($v['2yinli1']);
			$everydaygold[$v['uid']]['2yinli2'] += intval($v['2yinli2']);
			$everydaygold[$v['uid']]['2yinli'] += intval($v['2yinli']);

			$everydaygold[$v['uid']]['3yinli1'] += intval($v['3yinli1']);
			$everydaygold[$v['uid']]['3yinli2'] += intval($v['3yinli2']);
			$everydaygold[$v['uid']]['3yinli'] += intval($v['3yinli']);

			$everydaygold[$v['uid']]['4yinli1'] += intval($v['4yinli1']);
			$everydaygold[$v['uid']]['4yinli2'] += intval($v['4yinli2']);
			$everydaygold[$v['uid']]['4yinli'] += intval($v['4yinli']);

			$everydaygold[$v['uid']]['6yinli1'] += intval($v['6yinli1']);
			$everydaygold[$v['uid']]['6yinli2'] += intval($v['6yinli2']);
			$everydaygold[$v['uid']]['6yinli'] += intval($v['6yinli']);

			$everydaygold[$v['uid']]['5yinli1'] += intval($v['5yinli1']);
			$everydaygold[$v['uid']]['5yinli2'] += intval($v['5yinli2']);
			$everydaygold[$v['uid']]['5yinli'] += intval($v['5yinli']);

			$everydaygold[$v['uid']]['get1'] += intval($v['get1']);
			$everydaygold[$v['uid']]['get2'] += intval($v['get2']);
			$everydaygold[$v['uid']]['get'] = intval($everydaygold[$v['uid']]['get1']) + intval($everydaygold[$v['uid']]['get2']);

			$everydaygold[$v['uid']]['2get1'] += intval($v['2get1']);
			$everydaygold[$v['uid']]['2get2'] += intval($v['2get2']);
			$everydaygold[$v['uid']]['2get'] = intval($everydaygold[$v['uid']]['2get1']) + intval($everydaygold[$v['uid']]['2get2']);

			$everydaygold[$v['uid']]['3get1'] += intval($v['3get1']);
			$everydaygold[$v['uid']]['3get2'] += intval($v['3get2']);
			$everydaygold[$v['uid']]['3get'] = intval($everydaygold[$v['uid']]['3get1']) + intval($everydaygold[$v['uid']]['3get2']);

			$everydaygold[$v['uid']]['4get1'] += intval($v['4get1']);
			$everydaygold[$v['uid']]['4get2'] += intval($v['4get2']);
			$everydaygold[$v['uid']]['4get'] = intval($everydaygold[$v['uid']]['4get1']) + intval($everydaygold[$v['uid']]['4get2']);

			$everydaygold[$v['uid']]['6get1'] += intval($v['6get1']);
			$everydaygold[$v['uid']]['6get2'] += intval($v['6get2']);
			$everydaygold[$v['uid']]['6get'] = intval($everydaygold[$v['uid']]['6get1']) + intval($everydaygold[$v['uid']]['6get2']);

			$everydaygold[$v['uid']]['5get1'] += intval($v['5get1']);
			$everydaygold[$v['uid']]['5get2'] += intval($v['5get2']);
			$everydaygold[$v['uid']]['5get'] = intval($everydaygold[$v['uid']]['5get1']) + intval($everydaygold[$v['uid']]['5get2']);

			$everydaygold[$v['uid']]['gold_free'] += intval($v['gold_free']);
			$everydaygold[$v['uid']]['gold_task'] += intval($v['gold_task']);
			$everydaygold[$v['uid']]['red1003'] += intval($v['red1003']);
			$everydaygold[$v['uid']]['redtomoney'] += intval($v['redtomoney']);
			$everydaygold[$v['uid']]['redtogold'] += intval($v['redtogold']);
		}
		unset($fromstat);

		foreach ($uids as $k => $v) {
			$uid = intval($v);

			$everydaygold1['uid'] = $uid;

			$everydaygold1['yinli1'] = $everydaygold[$uid]['yinli1'];
			$everydaygold1['yinli2'] = $everydaygold[$uid]['yinli2'];
			$everydaygold1['yinli'] = $everydaygold[$uid]['yinli'];

			$everydaygold1['2yinli1'] = $everydaygold[$uid]['2yinli1'];
			$everydaygold1['2yinli2'] = $everydaygold[$uid]['2yinli2'];
			$everydaygold1['2yinli'] = $everydaygold[$uid]['2yinli'];

			$everydaygold1['3yinli1'] = $everydaygold[$uid]['3yinli1'];
			$everydaygold1['3yinli2'] = $everydaygold[$uid]['3yinli2'];
			$everydaygold1['3yinli'] = $everydaygold[$uid]['3yinli'];

			$everydaygold1['4yinli1'] = $everydaygold[$uid]['4yinli1'];
			$everydaygold1['4yinli2'] = $everydaygold[$uid]['4yinli2'];
			$everydaygold1['4yinli'] = $everydaygold[$uid]['4yinli'];

			$everydaygold1['6yinli1'] = $everydaygold[$uid]['6yinli1'];
			$everydaygold1['6yinli2'] = $everydaygold[$uid]['6yinli2'];
			$everydaygold1['6yinli'] = $everydaygold[$uid]['6yinli'];

			$everydaygold1['5yinli1'] = $everydaygold[$uid]['5yinli1'];
			$everydaygold1['5yinli2'] = $everydaygold[$uid]['5yinli2'];
			$everydaygold1['5yinli'] = $everydaygold[$uid]['5yinli'];

			$everydaygold1['get1'] = $everydaygold[$uid]['get1'];
			$everydaygold1['get2'] = $everydaygold[$uid]['get2'];
			$everydaygold1['get'] = $everydaygold[$uid]['get'];

			$everydaygold1['2get1'] = $everydaygold[$uid]['2get1'];
			$everydaygold1['2get2'] = $everydaygold[$uid]['2get2'];
			$everydaygold1['2get'] = $everydaygold[$uid]['2get'];

			$everydaygold1['3get1'] = $everydaygold[$uid]['3get1'];
			$everydaygold1['3get2'] = $everydaygold[$uid]['3get2'];
			$everydaygold1['3get'] = $everydaygold[$uid]['3get'];

			$everydaygold1['4get1'] = $everydaygold[$uid]['4get1'];
			$everydaygold1['4get2'] = $everydaygold[$uid]['4get2'];
			$everydaygold1['4get'] = $everydaygold[$uid]['4get'];

			$everydaygold1['6get1'] = $everydaygold[$uid]['6get1'];
			$everydaygold1['6get2'] = $everydaygold[$uid]['6get2'];
			$everydaygold1['6get'] = $everydaygold[$uid]['6get'];

			$everydaygold1['5get1'] = $everydaygold[$uid]['5get1'];
			$everydaygold1['5get2'] = $everydaygold[$uid]['5get2'];
			$everydaygold1['5get'] = $everydaygold[$uid]['5get'];

			$everydaygold1['gold_free'] = $everydaygold[$uid]['gold_free'];
			$everydaygold1['gold_task'] = $everydaygold[$uid]['gold_task'];
			$everydaygold1['red1003'] = $everydaygold[$uid]['red1003'];
			$everydaygold1['redtomoney'] = $everydaygold[$uid]['redtomoney'];
			$everydaygold1['redtogold'] = $everydaygold[$uid]['redtogold'];

			$uap['uid'] = $uid;
			if ($log = $userinfo->where($uap)->find()) {
				$userinfo->where(array('_id' => $log['_id']))->save($everydaygold1);
			}
		}

		echo 'run-date' . date('Y-m-d H:i', NOW_TIME) . PHP_EOL;

		$etime = microtime(true);
		$total = round($etime - $stime);
		echo "Run {$total}s times" . PHP_EOL;
	}

	//5分钟统计在线用户
	public function use_goldstat_new5()
	{
		$stime = microtime(true); #获取程序开始执行的时间

		$userinfo = D('Userinfo');

		$b_time = NOW_TIME - 180;
		$e_time = NOW_TIME;
		$map['c_time'] = array('between', array($b_time, $e_time));
		$map['uid'] = array('egt', 150000);
		//查出180秒金币变动 玩家信息
		$list = $userinfo->where($map)->field('uid')->select();

		echo 'begintime' . time() . PHP_EOL;
		$uidsarr = array_column($list, 'uid');
		$uids = array_unique($uidsarr);
		echo 'endtime' . time() . '   uid nums:' . count($uids) . PHP_EOL;


		$u_map['uid'] = array('in', $uids);
		$everydaygold = $fromstat = array();
		$fromstat = D('fromstat')->where($u_map)->select();

		foreach ($fromstat as $v) {
			$everydaygold[$v['uid']]['yinli1'] += intval($v['yinli1']);
			$everydaygold[$v['uid']]['yinli2'] += intval($v['yinli2']);
			$everydaygold[$v['uid']]['yinli'] += intval($v['yinli']);

			$everydaygold[$v['uid']]['2yinli1'] += intval($v['2yinli1']);
			$everydaygold[$v['uid']]['2yinli2'] += intval($v['2yinli2']);
			$everydaygold[$v['uid']]['2yinli'] += intval($v['2yinli']);

			$everydaygold[$v['uid']]['3yinli1'] += intval($v['3yinli1']);
			$everydaygold[$v['uid']]['3yinli2'] += intval($v['3yinli2']);
			$everydaygold[$v['uid']]['3yinli'] += intval($v['3yinli']);

			$everydaygold[$v['uid']]['4yinli1'] += intval($v['4yinli1']);
			$everydaygold[$v['uid']]['4yinli2'] += intval($v['4yinli2']);
			$everydaygold[$v['uid']]['4yinli'] += intval($v['4yinli']);

			$everydaygold[$v['uid']]['6yinli1'] += intval($v['6yinli1']);
			$everydaygold[$v['uid']]['6yinli2'] += intval($v['6yinli2']);
			$everydaygold[$v['uid']]['6yinli'] += intval($v['6yinli']);

			$everydaygold[$v['uid']]['5yinli1'] += intval($v['5yinli1']);
			$everydaygold[$v['uid']]['5yinli2'] += intval($v['5yinli2']);
			$everydaygold[$v['uid']]['5yinli'] += intval($v['5yinli']);

			$everydaygold[$v['uid']]['get1'] += intval($v['get1']);
			$everydaygold[$v['uid']]['get2'] += intval($v['get2']);
			$everydaygold[$v['uid']]['get'] = intval($everydaygold[$v['uid']]['get1']) + intval($everydaygold[$v['uid']]['get2']);

			$everydaygold[$v['uid']]['2get1'] += intval($v['2get1']);
			$everydaygold[$v['uid']]['2get2'] += intval($v['2get2']);
			$everydaygold[$v['uid']]['2get'] = intval($everydaygold[$v['uid']]['2get1']) + intval($everydaygold[$v['uid']]['2get2']);

			$everydaygold[$v['uid']]['3get1'] += intval($v['3get1']);
			$everydaygold[$v['uid']]['3get2'] += intval($v['3get2']);
			$everydaygold[$v['uid']]['3get'] = intval($everydaygold[$v['uid']]['3get1']) + intval($everydaygold[$v['uid']]['3get2']);

			$everydaygold[$v['uid']]['4get1'] += intval($v['4get1']);
			$everydaygold[$v['uid']]['4get2'] += intval($v['4get2']);
			$everydaygold[$v['uid']]['4get'] = intval($everydaygold[$v['uid']]['4get1']) + intval($everydaygold[$v['uid']]['4get2']);

			$everydaygold[$v['uid']]['6get1'] += intval($v['6get1']);
			$everydaygold[$v['uid']]['6get2'] += intval($v['6get2']);
			$everydaygold[$v['uid']]['6get'] = intval($everydaygold[$v['uid']]['6get1']) + intval($everydaygold[$v['uid']]['6get2']);

			$everydaygold[$v['uid']]['5get1'] += intval($v['5get1']);
			$everydaygold[$v['uid']]['5get2'] += intval($v['5get2']);
			$everydaygold[$v['uid']]['5get'] = intval($everydaygold[$v['uid']]['5get1']) + intval($everydaygold[$v['uid']]['5get2']);

			$everydaygold[$v['uid']]['gold_free'] += intval($v['gold_free']);
			$everydaygold[$v['uid']]['gold_task'] += intval($v['gold_task']);
			$everydaygold[$v['uid']]['red1003'] += intval($v['red1003']);
			$everydaygold[$v['uid']]['redtomoney'] += intval($v['redtomoney']);
			$everydaygold[$v['uid']]['redtogold'] += intval($v['redtogold']);
		}
		unset($fromstat);

		foreach ($uids as $k => $v) {
			$uid = intval($v);

			$everydaygold1['uid'] = $uid;

			$everydaygold1['yinli1'] = $everydaygold[$uid]['yinli1'];
			$everydaygold1['yinli2'] = $everydaygold[$uid]['yinli2'];
			$everydaygold1['yinli'] = $everydaygold[$uid]['yinli'];

			$everydaygold1['2yinli1'] = $everydaygold[$uid]['2yinli1'];
			$everydaygold1['2yinli2'] = $everydaygold[$uid]['2yinli2'];
			$everydaygold1['2yinli'] = $everydaygold[$uid]['2yinli'];

			$everydaygold1['3yinli1'] = $everydaygold[$uid]['3yinli1'];
			$everydaygold1['3yinli2'] = $everydaygold[$uid]['3yinli2'];
			$everydaygold1['3yinli'] = $everydaygold[$uid]['3yinli'];

			$everydaygold1['4yinli1'] = $everydaygold[$uid]['4yinli1'];
			$everydaygold1['4yinli2'] = $everydaygold[$uid]['4yinli2'];
			$everydaygold1['4yinli'] = $everydaygold[$uid]['4yinli'];

			$everydaygold1['6yinli1'] = $everydaygold[$uid]['6yinli1'];
			$everydaygold1['6yinli2'] = $everydaygold[$uid]['6yinli2'];
			$everydaygold1['6yinli'] = $everydaygold[$uid]['6yinli'];

			$everydaygold1['5yinli1'] = $everydaygold[$uid]['5yinli1'];
			$everydaygold1['5yinli2'] = $everydaygold[$uid]['5yinli2'];
			$everydaygold1['5yinli'] = $everydaygold[$uid]['5yinli'];

			$everydaygold1['get1'] = $everydaygold[$uid]['get1'];
			$everydaygold1['get2'] = $everydaygold[$uid]['get2'];
			$everydaygold1['get'] = $everydaygold[$uid]['get'];

			$everydaygold1['2get1'] = $everydaygold[$uid]['2get1'];
			$everydaygold1['2get2'] = $everydaygold[$uid]['2get2'];
			$everydaygold1['2get'] = $everydaygold[$uid]['2get'];

			$everydaygold1['3get1'] = $everydaygold[$uid]['3get1'];
			$everydaygold1['3get2'] = $everydaygold[$uid]['3get2'];
			$everydaygold1['3get'] = $everydaygold[$uid]['3get'];

			$everydaygold1['4get1'] = $everydaygold[$uid]['4get1'];
			$everydaygold1['4get2'] = $everydaygold[$uid]['4get2'];
			$everydaygold1['4get'] = $everydaygold[$uid]['4get'];

			$everydaygold1['6get1'] = $everydaygold[$uid]['6get1'];
			$everydaygold1['6get2'] = $everydaygold[$uid]['6get2'];
			$everydaygold1['6get'] = $everydaygold[$uid]['6get'];

			$everydaygold1['5get1'] = $everydaygold[$uid]['5get1'];
			$everydaygold1['5get2'] = $everydaygold[$uid]['5get2'];
			$everydaygold1['5get'] = $everydaygold[$uid]['5get'];

			$everydaygold1['gold_free'] = $everydaygold[$uid]['gold_free'];
			$everydaygold1['gold_task'] = $everydaygold[$uid]['gold_task'];
			$everydaygold1['red1003'] = $everydaygold[$uid]['red1003'];
			$everydaygold1['redtomoney'] = $everydaygold[$uid]['redtomoney'];
			$everydaygold1['redtogold'] = $everydaygold[$uid]['redtogold'];

			$uap['uid'] = $uid;
			if ($log = $userinfo->where($uap)->find()) {
				$userinfo->where(array('_id' => $log['_id']))->save($everydaygold1);
			}
		}

		echo 'run-date' . date('Y-m-d H:i', NOW_TIME) . PHP_EOL;

		$etime = microtime(true);
		$total = round($etime - $stime);
		echo "Run {$total}s times" . PHP_EOL;
	}

	public function test()
	{
		$Kplog = D('Kplog');
		$Brnnlog = D('Brnnlog');
		$Fruitlog = D('Fruitlog');
		$Carlog = D('Carlog');
		$Qrredbag = D('Qrredbag');
		$Superfruitlog = D('Superfruitlog');
		$map['uid'] = 204477;
		$getUse = function (array $v) {
			if ($v['get'] > $v['use']) {
				$ret['get'] = $v['get'] - $v['use'];
				$ret['use'] = 0;
			} else {
				$ret['get'] = 0;
				$ret['use'] = $v['use'] - $v['get'];
			}
			return $ret;
		};

		$lists = $Kplog->where($map)->select();
		foreach ($lists as $v) {
			$operate = $getUse($v);
			$wingold  += $operate['get'] + $v['pool_win'] - $v['tax'] - $v['pooladd'];
			$losegold += $operate['use'];
		}
		unset($lists);
		$Kplog_goldstat = $losegold - $wingold;

		$lists = $Brnnlog->where($map)->select();
		foreach ($lists as $v) {
			$operate = $getUse($v);
			$wingold  += $operate['get'] + $v['pool_win'] - $v['tax'] - $v['pooladd'];
			$losegold += $operate['use'];
		}
		unset($lists);
		$Brnnlog_goldstat = $losegold - $wingold;

		$lists = $Fruitlog->where($map)->select();
		foreach ($lists as $v) {
			$operate = $getUse($v);
			$wingold  += $operate['get'] + $v['pool_win'] - $v['tax'] - $v['pooladd'];
			$losegold += $operate['use'];
		}
		unset($lists);
		$Fruitlog_goldstat = $losegold - $wingold;

		$lists = $Carlog->where($map)->select();
		foreach ($lists as $v) {
			$operate = $getUse($v);
			$wingold  += $operate['get'] + $v['pool_win'] - $v['tax'] - $v['pooladd'];
			$losegold += $operate['use'];
		}
		unset($lists);
		$Carlog_goldstat = $losegold - $wingold;

		$lists = $Qrredbag->where($map)->select();
		foreach ($lists as $v) {
			$operate = $getUse($v);
			$wingold  += $operate['get'] + $v['pool_win'] - $v['tax'] - $v['pooladd'];
			$losegold += $operate['use'];
		}
		unset($lists);
		$Qrredbag_goldstat = $losegold - $wingold;

		$lists = $Superfruitlog->where($map)->select();
		foreach ($lists as $v) {
			$operate = $getUse($v);
			$wingold  += $operate['get'] + $v['pool_win'] - $v['tax'] - $v['pooladd'];
			$losegold += $operate['use'];
		}
		unset($lists);
		$Superfruitlog_goldstat = $losegold - $wingold;

		$gold_win = $Kplog_goldstat + $Brnnlog_goldstat + $Fruitlog_goldstat + $Carlog_goldstat + $Qrredbag_goldstat + $Superfruitlog_goldstat;
		echo $gold_win;
	}
}
