<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/30
 * Time: 17:21
 */
namespace Admin\Controller;
use Think\Controller;

class RedtakeController extends Controller{

    public function stat(){
        $stime = microtime(true);

        $config = D('ConfigByshop')->getConfig();
        $red_goods_id = array();
        foreach ($config as $k=>$v){
            if($v['cost_type'] == 1){
                $red_goods_id[] = intval($k);
            }
        }
        //红包id
        $goods_id = array_unique($red_goods_id);

        $dtime = NOW_TIME;
        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', $dtime));
        }
        /*$e_time = strtotime(date('Y-m-d', NOW_TIME))-1;
        $b_time = $e_time - 86399;*/

        /*$bbtime = strtotime('2017-11-01');
        $eetime = strtotime('2017-11-16');
        for($i=0;$i<(($eetime-$bbtime)/86400);$i++) {

            $b_time = $bbtime + $i * 86400;
            $e_time = $b_time + 86399;*/

            $map['c_time'] = array('between', array($b_time, $e_time));
            $map['status'] = 1;
            $list = D('Shops')->where($map)->select();

            $uids = array();
            $money = 0;
            foreach ($list as $v) {
                if (!in_array($v['goods_id'], $goods_id)) continue;
                $uids[] = $v['uid'];
                $money += intval($config[$v['goods_id']]['item_num']);
            }

            $redtakestat = D('Redtakestat');

            $data = array(
                'time' => $b_time,
                'men' => count(array_unique($uids)),
                'money' => $money
            );

            if ($info = $redtakestat->where(array('time' => $b_time))->find()) {
                $redtakestat->where(array('_id' => $info['_id']))->save($data);
            } else {
                $redtakestat->add($data);
            }

//        }
        $etime = microtime(true);
        $totals = round($etime - $stime);
        echo 'RUN totals '.$totals.' S';

    }

}