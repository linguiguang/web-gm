<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 13:56
 */

namespace Admin\Controller;

use Think\Controller;

class AirstatController extends Controller
{

    /**
     * 机战半小时统计一次
     */

    public function airStat()
    {
        $stime = microtime(true);
        $dtime = NOW_TIME;
        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $airlabastat = D('Airlabastat'); // 所有玩家的总数据
        $airlabahourstat = D('Airlabahourstat'); // 每个玩家每个小时的数据
        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        //机战游戏记录
        $map['c_time'] = array('between', array($b_time, $e_time));
        // $map['uid'] = array('egt', 150149); //排除AI,150000--150148不算
        $logs = $airlabahourstat->where($map)->select();
        print_r($map);

        $data = array();
        foreach ($logs as $v) {
            if (in_array($v['uid'], $uids_arr)) {
                continue;
            }

            $date = date('Y-m-d H:00:00', $v['c_time']);
            if (intval($v['test_type']) == 1) {
                // 1: 试玩
                $data[$date]['get1'] += intval($v['get']); // 纯赢
                $data[$date]['use1'] += intval($v['use']); // 纯输

                $data[$date]['pool_win1'] += intval($v['pool_win']); // 彩池产出

                $data[$date]['sui1'] += intval($v['sys_rate']); // 平台纯税收
                $data[$date]['pool1'] += intval($v['pool_rate']); // 彩池新增
                $data[$date]['rank1'] += intval($v['rank_rate']); // 彩池新增

                $data[$date]['sget1'] += intval($v['gold_get']); // 流水get
                $data[$date]['suse1'] += intval($v['gold_use']); // 流水use

                $data[$date]['redpack_get_1_179'] += intval($v['redpack_get']); // 试玩击中红包
            } else if (intval($v['test_type']) == 2) {
                // 2: 娱乐
                $data[$date]['get2'] += intval($v['get']); // 纯赢
                $data[$date]['use2'] += intval($v['use']); // 纯输

                $data[$date]['pool_win2'] += intval($v['pool_win']); // 彩池产出

                $data[$date]['sui2'] += intval($v['sys_rate']); // 平台纯税收
                $data[$date]['pool2'] += intval($v['pool_rate']); // 彩池新增
                $data[$date]['rank2'] += intval($v['rank_rate']); // 彩池新增

                $data[$date]['sget2'] += intval($v['gold_get']); // 流水get
                $data[$date]['suse2'] += intval($v['gold_use']); // 流水use

                $data[$date]['redpack_get_2_180'] += intval($v['redpack_get']); // 娱乐击中红包
            }
        }
        unset($logs);

        // 红包数据 
        $totalMoneyObj =  D('Redbaglog')->mongoCode('db.getCollection("stat_redbaglog").aggregate(
                [
                    { 
                        $match: {
                            c_time: {
                                $gte: ' . $b_time . ',
                                $lte: ' . $e_time . '
                            },
                            room: {
                                $gte: ' . 179 . ',
                                $lte: ' . 180 . '
                            },
                            uid: {
                                $gte: ' . 150150 . '
                            }
                        }
                    },
                    { 
                        $group: {
                            _id: "$room",
                            allmoney: { $sum: "$money" }
                        }
                    } 
                ]);');

        if (!!$totalMoneyObj && !!$totalMoneyObj['_batch']) {
            $resData = $totalMoneyObj['_batch'];
            foreach ($resData as $k => $v) {
                if (intval($v['_id']) == 179) {
                    $data[$date]['redget179'] = intval($v['allmoney']);
                }

                if (intval($v['_id']) == 180) {
                    $data[$date]['redget180'] = intval($v['allmoney']);
                }
            }
        }

        foreach ($data as $k => $v) {
            $ret = array(
                'time' => strtotime($k),
                'get1' => intval($v['get1']),
                'get2' => intval($v['get2']),
                'use1' => intval($v['use1']),
                'use2' => intval($v['use2']),
                'pool_win1' => intval($v['pool_win1']),
                'pool_win2' => intval($v['pool_win2']),
                'sui1' => intval($v['sui1']),
                'sui2' => intval($v['sui2']),
                'pool1' => intval($v['pool1']),
                'pool2' => intval($v['pool2']),
                'rank1' => intval($v['rank1']),
                'rank2' => intval($v['rank2']),
                'sget1' => intval($v['sget1']),
                'suse1' => intval($v['suse1']),
                'sget2' => intval($v['sget2']),
                'suse2' => intval($v['suse2']),
                'allred_1_179' => intval($v['redpack_get_1_179']),
                'allred_2_180' => intval($v['redpack_get_2_180']),
                'allred179' => intval($v['redget179']),
                'allred180' => intval($v['redget180']),
            );

            if ($oldData = $airlabastat->where(array('time' => strtotime($k)))->find()) {
                $airlabastat->where(array('_id' => $oldData['_id']))->save($ret);
            } else {
                $airlabastat->add($ret);
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }
}