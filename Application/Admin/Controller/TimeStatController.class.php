<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/18
 * Time: 10:47
 */
namespace Admin\Controller;
use Think\Controller;

class TimeStatController extends Controller{

    /**
     * 用户在线时长人数统计,平均在线时长
     */
    public function onlinetime(){   
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['login_time'] = array('between', array($b_time, $e_time));
        $map['uid'] = array('egt', 150000);//排除AI数据
        $map['type'] = 1;

        $arealogin = D('Arealogin');
        $list = $arealogin->where($map)->select();

        $users = $total_time = $users_room = $room_time = array();
        foreach ($list as $v){

            $date = date('Y-m-d',$v['login_time']);
            $users[$date][$v['uid']]['times'] += $v['times'];

            //处于游戏房间内和在大厅的时间
            if(in_array($v['room'], array(11, 12, 13, 14, 15, 16, 2, 3, 4, 5))){
                $users_room[$date][$v['uid']]['times'] += $v['times'];
                $total_time[$date]['game_times'] += $v['times'];

                //所有房间的时间数据
                $room_time[$date]['room_'.$v['room']] += $v['times'];

            }elseif($v['room'] == 0){
                $total_time[$date]['dating_times'] += $v['times'];
            }

        }

        $oltimestat = D('Oltimestat');
        foreach ($users as $k=>$v){

            $c_time = strtotime($k);

            $times_all = $times_all_room = $times_new = $times_new_room = array();
            foreach ($v as $key=>$val){
                $times_all[] = $val['times'];   //当日时长数组
                $times_all_room[] = $users_room[$k][$key]['times']; //房间
            }

            $game_times = $total_time[$k]['game_times'];
            $dating_times = $total_time[$k]['dating_times'];

            //所有区域时间
            $roomtimearr = $room_time[$k];

            //总用户在线
            $this->stattime($oltimestat, $times_all, 'all', $game_times, $dating_times, $c_time, $roomtimearr, 'totalroom');
            $this->stattime($oltimestat, $times_all_room, 'all', $game_times, $dating_times, $c_time, $roomtimearr, 'gameroom');

        }

        echo 'SUCCESS'.PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times".PHP_EOL;
    }


    /**
     * @param $oltimestat 模型对象
     * @param $time_arr 每个用户时长数组
     * @param string $type 所有用户还是当日注册用户类型
     * @param $game_times 游戏在线时长
     * @param $dating_times 大厅在线时长
     * @param $c_time 每天的时间
     * @param $roomtimearr 每个房间的时间数组
     * @param string $gameroom 游戏内用户还有包括大厅用户类型
     */
    protected function stattime($oltimestat, $time_arr, $type='all', $game_times, $dating_times, $c_time, $roomtimearr, $gameroom='totalroom'){
        $phase1 = $phase2 = $phase3 = $phase4 = $phase5 = $phase6 = $phase7 = $phase8 = 0;//在线时长8个时段人数

        foreach ($time_arr as $val){
            if($val >= 60 && $val < 360){
                $phase1 += 1;
            }elseif ($val >= 360 && $val <= 600){
                $phase2 += 1;
            }elseif ($val >= 1800 && $val < 3600){
                $phase3 += 1;
            }elseif ($val >= 3600 && $val < 5400){
                $phase4 += 1;
            }elseif ($val >= 5400 && $val < 7200){
                $phase5 += 1;
            }elseif ($val >= 7200 && $val < 14400){
                $phase6 += 1;
            }elseif ($val >= 14400 && $val < 21600){
                $phase7 += 1;
            }elseif ($val >= 21600){ //360分钟以上
                $phase8 += 1;
            }
        }


        $data = array(
            'c_time'  => $c_time,
            'phase1'  => $phase1,
            'phase2'  => $phase2,
            'phase3'  => $phase3,
            'phase4'  => $phase4,
            'phase5'  => $phase5,
            'phase6'  => $phase6,
            'phase7'  => $phase7,
            'phase8'  => $phase8,
            'room_11' => intval($roomtimearr['room_11']),
            'room_12' => intval($roomtimearr['room_12']),
            'room_13' => intval($roomtimearr['room_13']),
            'room_14' => intval($roomtimearr['room_14']),
            'room_15' => intval($roomtimearr['room_15']),
            'room_16' => intval($roomtimearr['room_16']),
            'room_2'  => intval($roomtimearr['room_2']),
            'room_3'  => intval($roomtimearr['room_3']),
            'room_4'  => intval($roomtimearr['room_4']),
            'room_5'  => intval($roomtimearr['room_5']),
            'game_times' => $game_times,
            'dating_times' => intval($dating_times - $game_times),
            'room' => $gameroom,
            'type'   => $type
        );


        $map['c_time'] = $c_time;
        $map['type'] = $type;
        $map['room'] = $gameroom;

        if($info = $oltimestat->where($map)->find()){
            $oltimestat->where(array('_id'=>$info['_id']))->save($data);
        }else{
            $oltimestat->add($data);
        }

    }

}
