<?php

namespace Admin\Controller;

use Think\Controller;

class RedbagStatController extends Controller
{

    /**
     *  累赢红包统计 （自然月）
     */
    public function stat()
    {
        $stime = microtime(true); // 开始运行时间

        $needTime = date('Ym', NOW_TIME);
        $minDay = intval($needTime . "01");
        $maxDay = intval(date('Ymd', NOW_TIME));
        echo "minDay" . $minDay . "\n";
        echo "maxDay" . $maxDay . "\n";

        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uidsArr = array();
        foreach ($uids as $v) {
            $uidsArr[] = intval($v);
        }

        for ($i = $minDay; $i <= $maxDay; $i++) {
            //
            $minDayTime = strtotime($i * 1000000);
            $maxDayTime = $minDayTime + 86399;

            $totalMoneyObj =  D('Redbaglog')->mongoCode('db.getCollection("stat_redbaglog").aggregate(
                [
                    { 
                        $match: {
                            c_time: {
                                $gte: ' . $minDayTime . ',
                                $lte: ' . $maxDayTime . '
                            },
                            uid: {
                                $gte: ' . 150150 . '
                            }
                        }
                    },
                    { 
                        $group: {
                            _id: "$room",
                            allmoney: { $sum: "$money" }
                        }
                    } 
                ]);');

            if (!!$totalMoneyObj) {
                $resData = $totalMoneyObj['_batch'];
                foreach ($resData as $k => $v) {
                    $dbData = array(
                        'c_time' => intval($i),
                        'room' => intval($v['_id']),
                        'money' => intval($v['allmoney']),
                    );

                    if ($oldData = D('Redbagstatistics')->where(array('c_time' => intval($i), 'room' => intval($v['_id'])))->find()) {
                        D('Redbagstatistics')->where(array('_id' => $oldData['_id']))->save($dbData);
                    } else {
                        D('Redbagstatistics')->add($dbData);
                    }
                }
            } else {
                //
                echo "mongodb aggregate error";
            }
        }

        $endTime = microtime(true); // 结束运行时间
        $totalT = intval(($endTime - $stime) * 1000);
        echo "(done)RedbagStat stat run time " . $totalT . " 毫秒";
    }
}
