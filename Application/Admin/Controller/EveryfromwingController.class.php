<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/11
 * Time: 17:57
 */

namespace Admin\Controller;

use Think\Controller;

/**
 * 每日每个渠道的金额消耗统计
 */
class EveryfromwingController extends Controller
{
	public function stat()
	{
		$froms = D('DChannels')->getField('from_id', true);
		$nper_froms = array();

		foreach ($froms as $v) {
			if ($v >= 1000000) {
				$nper_froms[] = $v;
			}
		}
		$Dailywingold = D('Dailywingold');
		$Everyfromwing = D('Everyfromwing');

		//重跑昨天的数据
		if (date('G', NOW_TIME) > 0 && date('G', NOW_TIME) <= 2) {
			$e_time = strtotime(date('Y-m-d', NOW_TIME - 7200));
		} else {
			$e_time = strtotime(date('Y-m-d', NOW_TIME));
		}

		/* $bb_time = strtotime('2017-11-01');
        $ee_time = strtotime('2017-11-18');
        for ($i=0;$i<($ee_time-$bb_time)/86400;$i++) {
            $e_time = $bb_time+$i*86400;*/
		$b_time = $e_time - 86400;
		$map['time'] = array('between', array($b_time, $e_time));
		$list = $Dailywingold->where($map)->select();
		//echo json_encode($list);
		$data = array();
		foreach ($list as $v) {
			$data[$v['from_id']][$v['time']] = $v;
		}
		unset($list);

		$ret = array();
		foreach ($data as $k => $v) {
			if (in_array($k, $nper_froms)) {
				$last_froms = array();
				if (in_array($k, $last_froms)) {
					continue;
				}

				$nper = intval($k / 1000000);
				$functionname = 'plan_one' . $nper;
				$configname = 'PLAN_ONE' . $nper;
				$key1 = $Dailywingold->$functionname($v[$e_time], C($configname));
				$key2 = $Dailywingold->$functionname($v[$b_time], C($configname));

				if ($key1 > $key2) {
					$ret[$nper][$e_time]['f' . $k] = $key1 - $key2;
				} else {
					$ret[$nper][$e_time]['f' . $k] = 0;
				}
				$ret[$nper][$e_time]['total'] += $ret[$nper][$e_time]['f' . $k];
				$ret[$nper][$e_time]['num'] += 1;
				$ret[$nper][$e_time]['nper'] = $nper;
			}
		}

		foreach ($ret as $k => $v) {
			if (in_array(intval($k), C('PLAN_NUMS_END'))) {
				// 方案下线
				continue;
			}

			$eap['nper'] = $k; // 期数
			$eap['time'] = $e_time;
			$needData = $ret[$k];

			if ($info = $Everyfromwing->where($eap)->find()) {
				$Everyfromwing->where(array('_id' => $info['_id']))->save($needData[$e_time]);
			} else {

				$needData[$e_time]['time'] = $e_time;
				$Everyfromwing->add($needData[$e_time]);
			}
		}
	}

	public function stat_bk()
	{
		$froms = D('DChannels')->getField('from_id', true);

		//1期
		$nper_froms = array();

		foreach ($froms as $v) {
			if ($v >= 1000000) {
				$nper_froms[] = $v;
			}
		}

		$Dailywingold = D('Dailywingold');
		$Everyfromwing = D('Everyfromwing');

		//重跑昨天的数据
		if (date('G', NOW_TIME) > 0 && date('G', NOW_TIME) <= 2) {
			$e_time = strtotime(date('Y-m-d', NOW_TIME - 7200));
		} else {
			$e_time = strtotime(date('Y-m-d', NOW_TIME));
		}

		/* $bb_time = strtotime('2017-11-01');
        $ee_time = strtotime('2017-11-18');
        for ($i=0;$i<($ee_time-$bb_time)/86400;$i++) {
            $e_time = $bb_time+$i*86400;*/
		$b_time = $e_time - 86400;
		$map['time'] = array('between', array($b_time, $e_time));
		$list = $Dailywingold->where($map)->select();
		//echo json_encode($list);
		$data = $ret = array();
		foreach ($list as $v) {
			$data[$v['from_id']][$v['time']] = $v;
		}
		unset($list);

		foreach ($data as $k => $v) {
			if (in_array($k, $nper_froms)) {
				if (intval($k) > 1000000 && intval($k) < 2000000) {
					$last_froms = array(1000002, 1000005, 1000006, 1000007, 1000008, 1000009, 1000010, 1000011, 1000012, 1000013, 1000014, 1000015, 1000016, 1000017, 1000018, 1000020, 1000021, 1000022, 1000032, 1000036, 1000041, 1000042, 1000045, 1000047, 1000049);
					if (in_array($k, $last_froms)) {
						continue;
					} else {
						$key1 = $Dailywingold->calcu_forth_money_1($v[$e_time]);
						$key2 = $Dailywingold->calcu_forth_money_1($v[$b_time]);
						if ($key1 > $key2) {
							$ret[$e_time]['f' . $k] = $key1 - $key2;
						} else {
							$ret[$e_time]['f' . $k] = 0;
						}
						$ret[$e_time]['total'] += $ret[$e_time]['f' . $k];
						$ret[$e_time]['num'] += 1;
					}
				}
				if (intval($k) > 2000000 && intval($k) < 3000000) {
					$last_froms = array();
					if (in_array($k, $last_froms)) {
						continue;
					} else {
						$key1 = $Dailywingold->calcu_forth_money_2($v[$e_time]);
						$key2 = $Dailywingold->calcu_forth_money_2($v[$b_time]);
						if ($key1 > $key2) {
							$ret[$e_time]['f' . $k] = $key1 - $key2;
						} else {
							$ret[$e_time]['f' . $k] = 0;
						}
						$ret[$e_time]['total'] += $ret[$e_time]['f' . $k];
						$ret[$e_time]['num'] += 1;
					}
				}
				if (intval($k) > 3000000 && intval($k) < 4000000) {
					$last_froms = array();
					if (in_array($k, $last_froms)) {
						continue;
					} else {
						$key1 = $Dailywingold->calcu_forth_money_3($v[$e_time]);
						$key2 = $Dailywingold->calcu_forth_money_3($v[$b_time]);
						if ($key1 > $key2) {
							$ret[$e_time]['f' . $k] = $key1 - $key2;
						} else {
							$ret[$e_time]['f' . $k] = 0;
						}
						$ret[$e_time]['total'] += $ret[$e_time]['f' . $k];
						$ret[$e_time]['num'] += 1;
					}
				}
				if (intval($k) > 4000000 && intval($k) < 5000000) {
					$last_froms = array();
					if (in_array($k, $last_froms)) {
						continue;
					} else {
						$key1 = $Dailywingold->calcu_forth_money_4($v[$e_time]);
						$key2 = $Dailywingold->calcu_forth_money_4($v[$b_time]);
						if ($key1 > $key2) {
							$ret[$e_time]['f' . $k] = $key1 - $key2;
						} else {
							$ret[$e_time]['f' . $k] = 0;
						}
						$ret[$e_time]['total'] += $ret[$e_time]['f' . $k];
						$ret[$e_time]['num'] += 1;
					}
				}
			}
		}

		//期数
		$ret[$e_time]['nper'] = 1;

		$eap['time'] = $e_time;
		$eap['nper'] = 1;
		if ($info = $Everyfromwing->where($eap)->find()) {
			$Everyfromwing->where(array('_id' => $info['_id']))->save($ret[$e_time]);
		} else {
			$ret[$e_time]['time'] = $e_time;
			$Everyfromwing->add($ret[$e_time]);
		}
		//        }

	}
}
