<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/5
 * Time: 15:21
 */
namespace Admin\Controller;
use Think\Controller;

class EverydayshareusersstatController extends Controller{

    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;
        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['create_time'] = array('between', array($b_time, $e_time));
        $list = D('Mysqlshareusers')->where($map)->select();

        //红包获奖情况，抽奖次数使用情况
        $lap['c_time'] = array('between', array($b_time, $e_time));
        $lottery = D('Sharelottery')->where($lap)->select();
        $lotteryUsers = $lotteryUse = array();
        foreach ($lottery as $v){
            $lotteryUsers[$v['uid']][$v['goods_id']] += $v['num'];
            $lotteryUse[$v['uid']][$v['type']] += 1;
        }
        unset($lottery);

        $shares = array();
        foreach ($list as $v){
            $shares[$v['share_user_id']]['char_id'] = intval($v['share_char_id']);
            $shares[$v['share_user_id']]['total_sharecount'] += 1;
            $shares[$v['share_user_id']]['red'] = intval($lotteryUsers[$v['share_user_id']][109]);//红包
            //抽奖使用次数
            $shares[$v['share_user_id']]['use_day1'] = intval($lotteryUse[$v['share_user_id']][1]);
            $shares[$v['share_user_id']]['use_day3'] = intval($lotteryUse[$v['share_user_id']][3]);
            $shares[$v['share_user_id']]['use_day7'] = intval($lotteryUse[$v['share_user_id']][7]);
        }

        $EverydayShareUsersStat = D('Everydayshareusersstat');
        foreach ($shares as $uid => $info){
            $info['uid'] = $uid;
            $info['time'] = $b_time;

            $eap['uid'] = $uid;
            $eap['time'] = $b_time;
            if($user = $EverydayShareUsersStat->where($eap)->find()){
                $EverydayShareUsersStat->where(array('_id'=>$user['_id']))->save($info);
            }else{
                $EverydayShareUsersStat->add($info);
            }
        }

        $etime = microtime(true);
        $totals = round($etime - $stime, 2);
        echo "Run {$totals} S";
    }

}