<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/22
 * Time: 17:29 
 */

namespace Admin\Controller;

use Think\Controller;

class DailywingoldController extends Controller
{

	// 超级水果 1期
	public function super_fruit_channel_one()
	{
		return;
		$stime = microtime(true);
		//取出渠道数据
		$uids = array();
		$whereFromId = array(
			'from_id' => array(
				'between', array(1000000, 1999999)
			)
		);
		$fromstat = D('fromstat')->where($whereFromId)->field('uid,from_id,money,money_s')->select();
		$fromstatchars = array();
		foreach ($fromstat as $v) {
			$fromstatchars[$v['from_id']][$v['uid']] = $v;
			$uids[$v['uid']] = $v['from_id'];
		}
		unset($fromstat);

		//取出用户数据 1期从渠道id从1000000
		$user_list = D('Users')->where(array(
			'from_id' => array(
				'gt', 1000000
			)
		))->getField('id,from_id,bind_time,adddate', true);

		$data = array();
		foreach ($user_list as $k => $v) {
			$uid = intval($k);
			if (!!$uids[$uid] && $v['from_id'] > 1000000) {
				$from_id = intval($uids[$uid]); // 修正渠道id
				// 红包场累计获取的红包 164
				$where['uid'] = intval($v['id']);
				$where['room'] = 164;
				$where['from_id'] = $from_id;
				$getList = D('Redbaglog')->where($where)->select();
				$totalMoney = 0;
				foreach ($getList as $k => $redbag) {
					if ($redbag['room'] == 164) {
						$totalMoney += $redbag['money'];
					}
				}

				foreach (C('PLAN_ONE1') as $key => $val) {
					if ($totalMoney >= intval($key)) {
						$data[$from_id][$key] += 1;
					}
				}

				$nowpays = intval($fromstatchars[$from_id][$uid]['money']);
				if ($nowpays >= 10) {
					$data[$from_id]['tenRmb'] += 1;
				}
				if ($nowpays >= 200) {
					$data[$from_id]['twoHundredRmb'] += 1;
				}
				if ($nowpays >= 600) {
					$data[$from_id]['sixHundredRmb'] += 1;
				}
				if ($nowpays >= 2000) {
					$data[$from_id]['twoThousandRmb'] += 1;
				}
				if ($nowpays >= 6000) {
					$data[$from_id]['sixThousandRmb'] += 1;
				}

				// 渠道总数
				$data[$from_id]['nowpays'] += $nowpays;
			}
		}

		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$time = $yes_time['b_time'];
		} else {
			$time = strtotime(date('Y-m-d 00:00:00', $dtime));
		}

		foreach ($data as $k => $ele) {
			$ret = $ele;
			$ret['time'] = $time;
			$ret['from_id'] = $k;
			$rap['from_id'] = $k;
			$rap['time'] = $time;
			if ($info = D('Dailywingold')->where($rap)->find()) {
				D('Dailywingold')->where(array('_id' => $info['_id']))->save($ret);
			} else {
				D('Dailywingold')->add($ret);
			}
		}

		$etime = microtime(true);
		$totals = round($etime - $stime);
		echo 'RUN ' . $totals . 'S';
	}

	// 超级水果 2期
	public function super_fruit_channel_two()
	{
		$stime = microtime(true);
		//取出渠道数据
		$uids = array();
		$whereFromId = array(
			'from_id' => array(
				'between', array(2000000, 2999999)
			)
		);
		// 到期的渠道id
		$expire_from_id = array(2000022);

		$fromstat = D('fromstat')->where($whereFromId)->field('uid,from_id,money,money_s')->select();
		$fromstatchars = array();
		foreach ($fromstat as $v) {
			if (in_array($v['from_id'], $expire_from_id)) {
				continue;
			}

			$fromstatchars[$v['from_id']][$v['uid']] = $v;
			$uids[$v['uid']] = $v['from_id'];
		}
		unset($fromstat);

		//取出用户数据 1期从渠道id从2000000
		$user_list = D('Users')->where(array(
			'from_id' => array(
				'gt', 2000000
			)
		))->getField('id,from_id,bind_time,adddate', true);

		$data = array();
		foreach ($user_list as $k => $v) {
			$uid = intval($k);
			if (!!$uids[$uid] && $v['from_id'] > 2000000) {
				$from_id = intval($uids[$uid]); // 修正渠道id
				// 红包场累计获取的红包 164
				$where['uid'] = intval($v['id']);
				$where['room'] = 164;
				$where['from_id'] = $from_id;
				$getList = D('Redbaglog')->where($where)->select();
				$totalMoney = 0;
				foreach ($getList as $k => $redbag) {
					if ($redbag['room'] == 164) {
						$totalMoney += $redbag['money'];
					}
				}

				foreach (C('PLAN_ONE2') as $key => $val) {
					if (intval($v['bind_time']) == 0 && intval($key) == 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}

					if (intval($key) > 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}
				}

				$nowpays = intval($fromstatchars[$from_id][$uid]['money']) - intval($fromstatchars[$from_id][$uid]['money_s']); //充值, 首充不参与
				if ($nowpays >= 10) {
					$data[$from_id]['tenRmb'] += 1;
				}
				if ($nowpays >= 200) {
					$data[$from_id]['twoHundredRmb'] += 1;
				}
				if ($nowpays >= 600) {
					$data[$from_id]['sixHundredRmb'] += 1;
				}
				if ($nowpays >= 2000) {
					$data[$from_id]['twoThousandRmb'] += 1;
				}
				if ($nowpays >= 6000) {
					$data[$from_id]['sixThousandRmb'] += 1;
				}

				// 渠道总数
				$data[$from_id]['nowpays'] += $nowpays;
			}
		}

		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$time = $yes_time['b_time'];
		} else {
			$time = strtotime(date('Y-m-d 00:00:00', $dtime));
		}

		foreach ($data as $k => $ele) {
			$ret = $ele;
			$ret['time'] = $time;
			$ret['from_id'] = $k;
			$rap['from_id'] = $k;
			$rap['time'] = $time;
			if ($info = D('Dailywingold')->where($rap)->find()) {
				D('Dailywingold')->where(array('_id' => $info['_id']))->save($ret);
			} else {
				D('Dailywingold')->add($ret);
			}
		}

		$etime = microtime(true);
		$totals = round($etime - $stime);
		echo 'RUN ' . $totals . 'S';
	}

	// 超级水果 3期
	public function super_fruit_channel_three()
	{
		$stime = microtime(true);
		//取出渠道数据
		$uids = array();
		$whereFromId = array(
			'from_id' => array(
				'between', array(3000000, 3999999)
			)
		);
		// 到期的渠道id
		$expire_from_id = array();

		$fromstat = D('fromstat')->where($whereFromId)->field('uid,from_id,money,money_s')->select();
		$fromstatchars = array();
		foreach ($fromstat as $v) {
			if (in_array($v['from_id'], $expire_from_id)) {
				continue;
			}

			$fromstatchars[$v['from_id']][$v['uid']] = $v;
			$uids[$v['uid']] = $v['from_id'];
		}
		unset($fromstat);

		//取出用户数据 1期从渠道id从2000000
		$user_list = D('Users')->where(array(
			'from_id' => array(
				'gt', 3000000
			)
		))->getField('id,from_id,bind_time,adddate', true);

		$data = array();
		foreach ($user_list as $k => $v) {
			$uid = intval($k);
			if (!!$uids[$uid] && $v['from_id'] > 3000000) {
				$from_id = intval($uids[$uid]); // 修正渠道id
				// 红包场累计获取的红包 164
				$where['uid'] = intval($v['id']);
				$where['room'] = 164;
				$where['from_id'] = $from_id;
				$getList = D('Redbaglog')->where($where)->select();
				$totalMoney = 0;
				foreach ($getList as $k => $redbag) {
					if ($redbag['room'] == 164) {
						$totalMoney += $redbag['money'];
					}
				}

				foreach (C('PLAN_ONE3') as $key => $val) {
					if (intval($v['bind_time']) == 0 && intval($key) == 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}

					if (intval($key) > 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}
				}

				$nowpays = intval($fromstatchars[$from_id][$uid]['money']) - intval($fromstatchars[$from_id][$uid]['money_s']); //充值, 首充不参与
				if ($nowpays >= 10) {
					$data[$from_id]['tenRmb'] += 1;
				}
				if ($nowpays >= 200) {
					$data[$from_id]['twoHundredRmb'] += 1;
				}
				if ($nowpays >= 400) {
					$data[$from_id]['fourHundredRmb'] += 1;
				}
				if ($nowpays >= 800) {
					$data[$from_id]['eightHundredRmb'] += 1;
				}
				if ($nowpays >= 1500) {
					$data[$from_id]['fifteenHundredRmb'] += 1;
				}
				if ($nowpays >= 3000) {
					$data[$from_id]['threeThousandRmb'] += 1;
				}
				if ($nowpays >= 8000) {
					$data[$from_id]['eightThousandRmb'] += 1;
				}
				if ($nowpays >= 16000) {
					$data[$from_id]['sixteenThousandRmb'] += 1;
				}
				if ($nowpays >= 35000) {
					$data[$from_id]['ThirtyfiveThousandRmb'] += 1;
				}

				// 渠道总数
				$data[$from_id]['nowpays'] += $nowpays;
			}
		}

		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$time = $yes_time['b_time'];
		} else {
			$time = strtotime(date('Y-m-d 00:00:00', $dtime));
		}

		foreach ($data as $k => $ele) {
			$ret = $ele;
			$ret['time'] = $time;
			$ret['from_id'] = $k;
			$rap['from_id'] = $k;
			$rap['time'] = $time;
			if ($info = D('Dailywingold')->where($rap)->find()) {
				D('Dailywingold')->where(array('_id' => $info['_id']))->save($ret);
			} else {
				D('Dailywingold')->add($ret);
			}
		}

		$etime = microtime(true);
		$totals = round($etime - $stime);
		echo 'RUN ' . $totals . 'S';
	}

	// 超级水果 4期
	public function super_fruit_channel_four()
	{
		$stime = microtime(true);
		//取出渠道数据
		$uids = array();
		$whereFromId = array(
			'from_id' => array(
				'between', array(4000000, 4999999)
			)
		);
		// 到期的渠道id
		$expire_from_id = array();

		$fromstat = D('fromstat')->where($whereFromId)->field('uid,from_id,money,money_s')->select();
		$fromstatchars = array();
		foreach ($fromstat as $v) {
			if (in_array($v['from_id'], $expire_from_id)) {
				continue;
			}

			$fromstatchars[$v['from_id']][$v['uid']] = $v;
			$uids[$v['uid']] = $v['from_id'];
		}
		unset($fromstat);

		//取出用户数据 1期从渠道id从2000000
		$user_list = D('Users')->where(array(
			'from_id' => array(
				'gt', 4000000
			)
		))->getField('id,from_id,bind_time,adddate', true);

		$data = array();
		foreach ($user_list as $k => $v) {
			$uid = intval($k);
			if (!!$uids[$uid] && $v['from_id'] > 4000000) {
				$from_id = intval($uids[$uid]); // 修正渠道id
				// 红包场累计获取的红包 164
				$where['uid'] = intval($v['id']);
				$where['room'] = 164;
				$where['from_id'] = $from_id;
				$getList = D('Redbaglog')->where($where)->select();
				$totalMoney = 0;
				foreach ($getList as $k => $redbag) {
					if ($redbag['room'] == 164) {
						$totalMoney += $redbag['money'];
					}
				}

				foreach (C('PLAN_ONE4') as $key => $val) {
					if (intval($v['bind_time']) == 0 && intval($key) == 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}

					if (intval($key) > 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}
				}

				$nowpays = intval($fromstatchars[$from_id][$uid]['money']) - intval($fromstatchars[$from_id][$uid]['money_s']); //充值, 首充不参与
				if ($nowpays >= 10) {
					$data[$from_id]['tenRmb'] += 1;
				}
				if ($nowpays >= 200) {
					$data[$from_id]['twoHundredRmb'] += 1;
				}
				if ($nowpays >= 400) {
					$data[$from_id]['fourHundredRmb'] += 1;
				}
				if ($nowpays >= 800) {
					$data[$from_id]['eightHundredRmb'] += 1;
				}
				if ($nowpays >= 1500) {
					$data[$from_id]['fifteenHundredRmb'] += 1;
				}
				if ($nowpays >= 3000) {
					$data[$from_id]['threeThousandRmb'] += 1;
				}
				if ($nowpays >= 8000) {
					$data[$from_id]['eightThousandRmb'] += 1;
				}
				if ($nowpays >= 16000) {
					$data[$from_id]['sixteenThousandRmb'] += 1;
				}
				if ($nowpays >= 35000) {
					$data[$from_id]['ThirtyfiveThousandRmb'] += 1;
				}

				// 渠道总数
				$data[$from_id]['nowpays'] += $nowpays;
			}
		}

		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$time = $yes_time['b_time'];
		} else {
			$time = strtotime(date('Y-m-d 00:00:00', $dtime));
		}

		foreach ($data as $k => $ele) {
			$ret = $ele;
			$ret['time'] = $time;
			$ret['from_id'] = $k;
			$rap['from_id'] = $k;
			$rap['time'] = $time;
			if ($info = D('Dailywingold')->where($rap)->find()) {
				D('Dailywingold')->where(array('_id' => $info['_id']))->save($ret);
			} else {
				D('Dailywingold')->add($ret);
			}
		}

		$etime = microtime(true);
		$totals = round($etime - $stime);
		echo 'RUN ' . $totals . 'S';
	}

	// 超级水果 5期
	public function super_fruit_channel_five()
	{
		$stime = microtime(true);
		//取出渠道数据
		$uids = array();
		$whereFromId = array(
			'from_id' => array(
				'between', array(5000000, 5999999)
			)
		);
		// 到期的渠道id
		$expire_from_id = array();

		$fromstat = D('fromstat')->where($whereFromId)->field('uid,from_id,money,money_s')->select();
		$fromstatchars = array();
		foreach ($fromstat as $v) {
			if (in_array($v['from_id'], $expire_from_id)) {
				continue;
			}

			$fromstatchars[$v['from_id']][$v['uid']] = $v;
			$uids[$v['uid']] = $v['from_id'];
		}
		unset($fromstat);

		//取出用户数据 5期从渠道id从5000000
		$user_list = D('Users')->where(array(
			'from_id' => array(
				'gt', 5000000
			)
		))->getField('id,from_id,bind_time,adddate', true);

		$data = array();
		foreach ($user_list as $k => $v) {
			$uid = intval($k);
			if (!!$uids[$uid] && $v['from_id'] > 5000000) {
				$from_id = intval($uids[$uid]); // 修正渠道id
				// 红包场累计获取的红包 164
				$where['uid'] = intval($v['id']);
				$where['room'] = 164;
				$where['from_id'] = $from_id;
				$getList = D('Redbaglog')->where($where)->select();
				$totalMoney = 0;
				foreach ($getList as $k => $redbag) {
					if ($redbag['room'] == 164) {
						$totalMoney += $redbag['money'];
					}
				}

				foreach (C('PLAN_ONE5') as $key => $val) {
					if (intval($v['bind_time']) == 0 && intval($key) == 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}

					if (intval($key) > 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}
				}

				$nowpays = intval($fromstatchars[$from_id][$uid]['money']) - intval($fromstatchars[$from_id][$uid]['money_s']); //充值, 首充不参与
				if ($nowpays >= 10) {
					$data[$from_id]['tenRmb'] += 1;
				}
				if ($nowpays >= 200) {
					$data[$from_id]['twoHundredRmb'] += 1;
				}
				if ($nowpays >= 400) {
					$data[$from_id]['fourHundredRmb'] += 1;
				}
				if ($nowpays >= 800) {
					$data[$from_id]['eightHundredRmb'] += 1;
				}
				if ($nowpays >= 1500) {
					$data[$from_id]['fifteenHundredRmb'] += 1;
				}
				if ($nowpays >= 3000) {
					$data[$from_id]['threeThousandRmb'] += 1;
				}
				if ($nowpays >= 8000) {
					$data[$from_id]['eightThousandRmb'] += 1;
				}
				if ($nowpays >= 16000) {
					$data[$from_id]['sixteenThousandRmb'] += 1;
				}
				if ($nowpays >= 35000) {
					$data[$from_id]['ThirtyfiveThousandRmb'] += 1;
				}

				// 渠道总数
				$data[$from_id]['nowpays'] += $nowpays;
			}
		}

		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$time = $yes_time['b_time'];
		} else {
			$time = strtotime(date('Y-m-d 00:00:00', $dtime));
		}

		foreach ($data as $k => $ele) {
			$ret = $ele;
			$ret['time'] = $time;
			$ret['from_id'] = $k;
			$rap['from_id'] = $k;
			$rap['time'] = $time;
			if ($info = D('Dailywingold')->where($rap)->find()) {
				D('Dailywingold')->where(array('_id' => $info['_id']))->save($ret);
			} else {
				D('Dailywingold')->add($ret);
			}
		}

		$etime = microtime(true);
		$totals = round($etime - $stime);
		echo 'RUN ' . $totals . 'S';
	}

	// 备份
	public function super_fruit_channel_one_bk()
	{
		$stime = microtime(true);

		//取出渠道数据
		$whereFromId = array(
			'from_id' => array(
				'between', array(1000000, 1999999)
			)
		);
		$fromstat = D('fromstat')->where($whereFromId)->field('uid,from_id,money,money_s')->select();
		$fromstatchars = array();
		foreach ($fromstat as $v) {
			$fromstatchars[$v['from_id']][$v['uid']] = $v;
		}
		unset($fromstat);

		//取出用户数据
		$user_list = D('Users')->where($whereFromId)->getField('id,from_id,wingold,recharge,time_long,bind_time,adddate', true);

		// 最小绑定时间，可能存在0
		$data = array();
		foreach ($user_list as $k => $v) {
			$uid = intval($k);
			$from_id = intval($v['from_id']);


			if ($from_id > 1000000 && $from_id < 1999999) {
				// 红包场累计获取的红包 164
				$where['uid'] = intval($v['id']);
				$where['room'] = 164;

				$c_time = NOW_TIME + 86400; // 默认当前时间加1天
				if ($v['bind_time'] > 0 && $v['bind_time'] >= $v['adddate']) {
					$c_time = $v['bind_time'];
					$where['c_time'] = array('lt', intval($c_time));
				} else if ($v['bind_time'] == 0 &&  $v['adddate'] > 0) {
					$c_time = $v['adddate'];
					$where['c_time'] = array('gte', intval($c_time));
				}

				$getList = D('redbaglog')->where($where)->select();
				$totalMoney = 0;
				foreach ($getList as $k => $redbag) {
					if ($redbag['room'] == 164) {
						$totalMoney += $redbag['money'];
					}
				}

				foreach (C('PLAN_ONE1') as $key => $val) {
					if ($totalMoney >= intval($key)) {
						$data[$from_id][$key] += 1;
					}
				}

				// $nowpays = intval($fromstatchars[$from_id][$uid]['money']) - intval($fromstatchars[$from_id][$uid]['money_s']); //充值, 首充不参与
				$nowpays = intval($fromstatchars[$from_id][$uid]['money']);
				if ($nowpays >= 10) {
					$data[$from_id]['tenRmb'] += 1;
				}
				if ($nowpays >= 200) {
					$data[$from_id]['twoHundredRmb'] += 1;
				}
				if ($nowpays >= 600) {
					$data[$from_id]['sixHundredRmb'] += 1;
				}
				if ($nowpays >= 2000) {
					$data[$from_id]['twoThousandRmb'] += 1;
				}
				if ($nowpays >= 6000) {
					$data[$from_id]['sixThousandRmb'] += 1;
				}

				// 渠道总数
				$data[$from_id]['nowpays'] += $nowpays;
			}
		}

		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$time = $yes_time['b_time'];
		} else {
			$time = strtotime(date('Y-m-d 00:00:00', $dtime));
		}

		foreach ($data as $k => $ele) {
			$ret = $ele;
			$ret['time'] = $time;
			$ret['from_id'] = $k;
			$rap['from_id'] = $k;
			$rap['time'] = $time;
			if ($info = D('Dailywingold')->where($rap)->find()) {
				D('Dailywingold')->where(array('_id' => $info['_id']))->save($ret);
			} else {
				D('Dailywingold')->add($ret);
			}
		}
		$etime = microtime(true);
		$totals = round($etime - $stime);
		echo 'RUN ' . $totals . 'S';
	}

	public function super_fruit_channel_two_bk()
	{
		$stime = microtime(true);

		//取出渠道数据
		$whereFromId = array(
			'from_id' => array(
				'between', array(2000000, 2999999)
			)
		);
		$fromstat = D('fromstat')->where($whereFromId)->field('uid,from_id,money,money_s')->select();
		$fromstatchars = array();
		foreach ($fromstat as $v) {
			$fromstatchars[$v['from_id']][$v['uid']] = $v;
		}
		unset($fromstat);

		//取出用户数据
		$user_list = D('Users')->where($whereFromId)->getField('id,from_id,wingold,recharge,time_long,bind_time,adddate', true);

		// 最小绑定时间，可能存在0
		$data = array();
		$numberUser = 0;
		foreach ($user_list as $k => $v) {
			$uid = intval($k);
			$from_id = intval($v['from_id']);
			if ($from_id > 2000000 && $from_id < 2999999) {

				// 红包场累计获取的红包 164
				$where['uid'] = intval($v['id']);
				$where['room'] = 164;

				$c_time = NOW_TIME + 86400; // 默认当前时间加1天
				if ($v['bind_time'] > 0 && $v['bind_time'] >= $v['adddate']) {
					$c_time = $v['bind_time'];
				} else if ($v['bind_time'] == 0 &&  $v['adddate'] > 0) {
					$c_time = $v['adddate'];
				}

				$where['c_time'] = array('gte', intval($c_time));

				$getList = D('Redbaglog')->where($where)->select();

				$totalMoney = 0;
				foreach ($getList as $k => $redbag) {
					if ($redbag['room'] == 164) {
						$totalMoney += $redbag['money'];
					}
				}

				foreach (C('PLAN_ONE2') as $key => $val) {
					if (intval($v['bind_time']) == 0 && intval($key) == 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}

					if (intval($key) > 1) {
						if ($totalMoney >= intval($key)) {
							$data[$from_id][$key] += 1;
						}
					}
				}

				$nowpays = intval($fromstatchars[$from_id][$uid]['money']) - intval($fromstatchars[$from_id][$uid]['money_s']); //充值, 首充不参与
				// $nowpays = intval($fromstatchars[$from_id][$uid]['money']);
				if ($nowpays >= 10) {
					$data[$from_id]['tenRmb'] += 1;
				}
				if ($nowpays >= 200) {
					$data[$from_id]['twoHundredRmb'] += 1;
				}
				if ($nowpays >= 600) {
					$data[$from_id]['sixHundredRmb'] += 1;
				}
				if ($nowpays >= 2000) {
					$data[$from_id]['twoThousandRmb'] += 1;
				}
				if ($nowpays >= 6000) {
					$data[$from_id]['sixThousandRmb'] += 1;
				}

				// 渠道总数
				$data[$from_id]['nowpays'] += $nowpays;
			}

			$numberUser++;
		}

		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$time = $yes_time['b_time'];
		} else {
			$time = strtotime(date('Y-m-d 00:00:00', $dtime));
		}

		foreach ($data as $k => $ele) {
			$ret = $ele;
			$ret['time'] = $time;
			$ret['from_id'] = $k;

			$rap['from_id'] = $k;
			$rap['time'] = $time;
			if ($info = D('Dailywingold')->where($rap)->find()) {
				D('Dailywingold')->where(array('_id' => $info['_id']))->save($ret);
			} else {
				D('Dailywingold')->add($ret);
			}
		}
		$etime = microtime(true);
		$totals = round($etime - $stime);
		echo 'RUN ' . $totals . 'S';
		echo '人数： ' . $numberUser;
	}

	/**
	 * 累计金币赢取最新统计方法
	 */
	public function new_wingold_stat()
	{
		$stime = microtime(true);
		$Dailywingold = D('Dailywingold');
		$Userinfo = D('Userinfo');
		$Usergoldstat = D('Usergoldstat');
		//配置获取
		$nper1 = $Dailywingold->getconfig(1, true);
		$nper2 = $Dailywingold->getconfig(2, true); //第2期
		$nper3 = $Dailywingold->getconfig(3, true); //第3期
		$nper4 = $Dailywingold->getconfig(4, true); //第4期


		//取出渠道数据
		$fromstat = D('fromstat')->where(array('from_id' => array('egt', 1000000)))->field('uid,from_id,money,money_s,get1,get2,2get2,3get2,4get2,6get2,2get1,4get1,6get1,car_i2,pay_i,3get1')->select();

		$fromstatchars = array();
		foreach ($fromstat as $v) {
			$fromstatchars[$v['from_id']][$v['uid']] = $v;
		}
		unset($fromstat);

		//=================
		//取出用户数据
		$user_list = D('Users')->getField('id,from_id,wingold,recharge,time_long,bind_time', true);
		//以后新一期数据从activate_logs表取出
		$newUserList = D('Activatelogs')->getField('user_id,from_id,wingold,recharge', true);
		$data = array();

		foreach ($user_list as $k => $v) {

			$uid = intval($k);
			$from_id = intval($v['from_id']);
			$bind_time = intval($v['bind_time']);

			if ($from_id > 1000000 && $from_id < 1999999) {
				continue;
			} else {
				if ($from_id > 2000000 && $from_id < 2999999) {
					continue;
				} else {
					if ($from_id > 3000000 && $from_id < 3999999) {
						$nper_froms = array(3000005, 3000006, 3000008, 3000011, 3000010, 3000013, 3000014, 3000015, 3000032, 3000036);
						$nper_froms[] = 3000016;
						$nper_froms[] = 3000002;
						$nper_froms[] = 3000021;
						$nper_froms[] = 3000042;
						$nper_froms[] = 3000017;
						$nper_froms[] = 3000045;
						$nper_froms[] = 3000031;
						$nper_froms[] = 3000007;
						$nper_froms[] = 3000025;
						if (in_array($from_id, $nper_froms)) {
							continue;
						} else {
							$usergetgold = intval($fromstatchars[$from_id][$uid]['get1']); //累赢
							foreach ($nper3[2] as $key => $val) {
								if ($usergetgold >= $val) {
									$data[$from_id][$key] += 1;
								}
							}
							if (!$newUserList[$uid]) {
								if (intval($fromstatchars[$from_id][$uid]['car_i2']) >= 3) { //娱乐场豪车3局
									$data[$from_id]['play1'] += 1;
								}
								if ($fromstatchars[$from_id][$uid]['2get2'] >= 100000) { //红包场赢取0.1元红包
									$data[$from_id]['Redbag1'] += 1;
								} elseif ($fromstatchars[$from_id][$uid]['3get2'] >= 100000) {
									$data[$from_id]['Redbag1'] += 1;
								} elseif ($fromstatchars[$from_id][$uid]['4get2'] >= 100000) {
									$data[$from_id]['Redbag1'] += 1;
								} elseif ($fromstatchars[$from_id][$uid]['6get2'] >= 100000) {
									$data[$from_id]['Redbag1'] += 1;
								}
							}
							$Redbag13 = 0;
							if ($fromstatchars[$from_id][$uid]['2get2'] >= 500000) {
								$Redbag13 = 13;
							} else {
								$Redbag13 = intval($fromstatchars[$from_id][$uid]['2get2'] / 100000) * 3;
								if ($fromstatchars[$from_id][$uid]['3get2'] >= 500000) {
									$Redbag13 = 13;
								} else {
									$Redbag13 = $Redbag13 + intval($fromstatchars[$from_id][$uid]['3get2'] / 100000) * 3;
									if ($fromstatchars[$from_id][$uid]['4get2'] >= 500000) {
										$Redbag13 = 13;
									} else {
										$Redbag13 = $Redbag13 + intval($fromstatchars[$from_id][$uid]['4get2'] / 100000) * 3;
										if ($fromstatchars[$from_id][$uid]['6get2'] >= 500000) {
											$Redbag13 = 13;
										} else {
											$Redbag13 = $Redbag13 + intval($fromstatchars[$from_id][$uid]['6get2'] / 100000) * 3;
										}
									}
								}
							}
							if ($Redbag13 >= 13) { //红包场赢取1.3元红包
								$data[$from_id]['Redbag13'] += 1;
							}

							$nowpays = intval($fromstatchars[$from_id][$uid]['money']) - intval($fromstatchars[$from_id][$uid]['money_s']); //充值
							if ($nowpays >= 10) {
								$data[$from_id]['sev_man'] += 1;
							}
							if ($nowpays >= 389) {
								$data[$from_id]['159_man'] += 1;
							}
							if ($nowpays >= 799) {
								$data[$from_id]['799_man'] += 1;
							}
							if ($nowpays >= 3699) {
								$data[$from_id]['3699_man'] += 1;
							}
							if ($nowpays >= 8899) {
								$data[$from_id]['8899_man'] += 1;
							}
						}
					} else if ($from_id > 4000000 && $from_id < 4999999) {
						$sixGet1 = intval($fromstatchars[$from_id][$uid]['6get1']); //  试玩超级水果纯赢
						$fourGet1 = intval($fromstatchars[$from_id][$uid]['4get1']); //  试玩豪车纯赢
						$twoGet1 = intval($fromstatchars[$from_id][$uid]['2get1']); //  试玩百人纯赢
						$threeGet1 = intval($fromstatchars[$from_id][$uid]['3get1']); //  试玩水果纯赢
						$totalGet = intval($sixGet1 / 50000) + intval($fourGet1 / 50000) + intval($twoGet1 / 50000) + intval($threeGet1 / 50000);
						foreach ($nper4[3] as $key => $val) {
							if ($totalGet * 50000  >= $val) {
								$data[$from_id][$key] += 1;
							}
						}

						$play_num = 0; //任意场3局
						if ($bind_time == 0) { // 判断是否新用户
							$play_num = intval($fromstatchars[$from_id][$uid]['pay_i']); // 任意游戏玩3局（限新用户）
							if ($play_num >= 3) {
								$data[$from_id]['play3'] += 1;
							}
						}

						$nowpays = intval($fromstatchars[$from_id][$uid]['money']) - intval($fromstatchars[$from_id][$uid]['money_s']); //充值
						$data[$from_id]['nowpays'] += $nowpays;
						if ($nowpays >= 10) {
							$data[$from_id]['ten_man'] += 1;
						}
						if ($nowpays >= 389) {
							$data[$from_id]['389_man'] += 1;
						}
						if ($nowpays >= 799) {
							$data[$from_id]['799_man'] += 1;
						}
						if ($nowpays >= 3699) {
							$data[$from_id]['3699_man'] += 1;
						}
						if ($nowpays >= 8899) {
							$data[$from_id]['8899_man'] += 1;
						}
					}
				}
			}
		}
		$dtime = NOW_TIME;
		if ($yes_time = stat_zero_time($dtime)) {
			$time = $yes_time['b_time'];
		} else {
			$time = strtotime(date('Y-m-d 00:00:00', $dtime));
		}
		foreach ($data as $k => $v) {
			$ret = $v;
			$ret['time'] = $time;
			$ret['from_id'] = $k;
			$rap['from_id'] = $k;
			$rap['time'] = $time;
			if ($info = $Dailywingold->where($rap)->find()) {
				$Dailywingold->where(array('_id' => $info['_id']))->save($ret);
			} else {
				$Dailywingold->add($ret);
			}
		}
		$etime = microtime(true);
		$totals = round($etime - $stime);
		echo 'RUN ' . $totals . 'S';
	}
}
