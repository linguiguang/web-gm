<?php

namespace Admin\Controller;

use Think\Controller;

class SharegameController extends Controller
{
    public function sharegame_nums()
    {
        $stime = microtime(true);

        $list = D('User')->query('select id, username, wxnick, from_user_id as uid, COUNT(*) as nums from rui_user GROUP BY from_user_id');
        $list2 = D('ShareGameTask')->query('select from_user_id, SUM(sharegold) as golds from rui_share_game_task GROUP BY from_user_id');

        echo json_encode($list);
        echo json_encode($list2);

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }
}
