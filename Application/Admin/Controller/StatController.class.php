<?php
/**
 * 默认控制器
 */
namespace Admin\Controller;
use Think\Controller;

class StatController extends Controller {

	///opt/aiya/nginx/fastcgi/bin/php admin.php stat/run
	public function run(){

		$stime=microtime(true); #获取程序开始执行的时间

		//$qudao = array(0,10000000,10000001,10000002,10000003,10000004,10000005,20000000,20000001,20000002,20000003,20000004,20000005,20000006,20000007,20000008,20000009,20000010,20000011,20000012,10000007);

		$stat_date = date('Y-m-d');
		//$s_time = strtotime($stat_date.' 00:00:00') - 2592000;
		$s_time = strtotime($stat_date.' 00:00:00') - 86400*7;
		$e_time = strtotime($stat_date.' 23:59:59');
		
		//$s_time=1560268800-86400 * 2;
		//$e_time = 1560355199;
		//账号数据
		$users = D('Users');
		$map['adddate'] = array('between', array($s_time, $e_time));
		$map['state'] = 1;
        $map['id'] = array('egt', 150000);
		$user_list = $users->where($map)->select(); //总注册
		
// 		var_dump($user_list);
		
		//角色数据
		$cha_map['c_time'] = array('between', array($s_time, $e_time));
		$cha_map['uid'] = array('egt', 150000);
		$cha_list = D('userinfo')->where($cha_map)->select();


		$arr_cha = array();
		$arr_pay = array();
		$arr_login = array();
		$arr_cha_uid = array();
		$arr_user = array();
		$arr_all_user = array();


		//支付数据
		$pay_map['paytime'] = array('between', array($s_time, $e_time));
		$pay_map['state'] = 3;
		$pay_list = D('Paylog')->where($pay_map)->select();

// 		$qd_list = D('DChannels')->where(array('from_id'=>array('neq', 999)))->field('from_id')->select();
		
        $qd_list = D('DChannels')->field('from_id')->select();
		$stat_data = array();
		foreach($qd_list as $k=>$val) {
			$qudao[] = $val['from_id'];
			$stat_data['user_count_sum'][$val['from_id']] = $users->where(array('from_id'=>$val['from_id'], 'state'=>1))->count('id');
		}
		
        
		foreach($cha_list as $k=>$val) {
			$arr_cha[$val['uid']] = $val;
			$arr_cha_uid[$val['char_id']] = $val;
		}
        unset($cha_list);

		
		$devid = array();

		foreach($user_list as $k=>$val) {

			$dt = date('Y-m-d',$val['adddate']);
			$stat_data['user_count'][$dt][$val['from_id']] += 1; //新增用户

			if(!$devid[$val['from_id']][$val['devid']]){
				$stat_data['user_count_devid'][$dt][$val['from_id']] += 1; //按唯一设备统计的新增用户
				$devid[$val['from_id']][$val['devid']] = $val['devid'];
			}

//			$stat_data['user_count_sum'][$val['from_id']] += 1; //累积用户

			if($val['siteuser'] == 1){
				$stat_data['user_count_bind'][$dt][$val['from_id']] += 1; //绑定手机的用户
			}

			if($arr_cha[$val['id']]){
				$stat_data['cha_count'][$dt][$val['from_id']] += 1; //创建角色的有效数据
			}


			$arr_all_user[$val['id']] = $val;

			$day_reg_user[date('Y-m-d',$val['adddate'])][$val['id']] = $val;

		}
        unset($user_list);


		foreach($pay_list as $k=>$val) {
			$p_dt = date('Y-m-d', $val['addtime']);
//			$userinfo = $arr_all_user[$arr_cha_uid[$val['uid']]['uid']];

			$pay_count[$p_dt][$val['from_id']][$val['uid']] = 1; //充值的用户
			$stat_data['pay_money'][$p_dt][$val['from_id']] += $val['money']; //充值的费用

			if($p_dt == date('Y-m-d', $arr_all_user[$val['uid']]['adddate'])){
				$first_pay_count[$p_dt][$val['from_id']][$val['uid']] = 1; //首日充值的用户
				$stat_data['first_pay_money'][$p_dt][$val['from_id']] += $val['money']; //首日充值的费用
			}

		}
        unset($pay_list);


		foreach($pay_count as $k=>$val) {
			foreach($val as $ke=>$value) {
				$stat_data['pay_count'][$k][$ke] = count($value); //充值的用户
			}
		}
        unset($pay_count);


		foreach($first_pay_count as $k=>$val) {
			foreach($val as $ke=>$value) {
				$stat_data['first_pay_count'][$k][$ke] = count($value); //首日充值的用户
			}
		}
        unset($first_pay_count);

        //活跃用户
        $actives = D('Activestat')->select();

        $active_arr = array();
        foreach ($actives as $v){
            $active_arr[$v['from_id']][$v['c_time']] = $v['user_nums'];
        }
        // var_dump($active_arr);exit;

		
		$arealogin = D('arealogin');
		$stat = D('Stat');

		for($d = 1; $d <= (strtotime(date('Y-m-d')) - $s_time)/86400; $d++) {
			
			$c_date = date('Y-m-d', $s_time + 86400 * ($d-1));
			
			$day_login = array();
			$login_count = array();

			for($m = 0; $m < (strtotime(date('Y-m-d')) - $s_time)/86400; $m++) {

				if($m > 7 && $m != 13 && $m != 29) continue;

				//登录数据
				$arr_login = array();

				$edt = strtotime($c_date . ' 00:00:00') + 86400*$m;

				$login_map['login_time'] = array('between', array($edt, strtotime(date('Y-m-d 23:59:59', $edt))));
				$login_map['type'] = 1;
                $login_map['uid'] = array('egt', 150000);

				$login_list = $arealogin->where($login_map)->select();

				foreach($login_list as $k=>$val) {
					$arr_login[$val['uid']] = $val;
				}

				foreach($arr_login as $k=>$val) {
					if($user_info = $day_reg_user[$c_date][$val['uid']]){
						//$arr_login[date('Y-m-d', $val['login_time'])] = $val;
						if($val['login_time'] < strtotime(date('Y-m-d 00:00:00', NOW_TIME))){
							$day_login['day_login_count_'.$m][date('Y-m-d', $val['login_time'])][$user_info['from_id']] += 1;
						}
					}

					$login_count['login_count'][date('Y-m-d', $val['login_time'])][$arr_all_user[$val['uid']]['from_id']] += 1; //活跃的用户
				}

			

				$dt = $c_date;

				if($arr_login[date('Y-m-d', strtotime($dt.' 00:00:00') + 86400*$m)][$val['id']]){	//第2天有登录
					$day_login['day_login_count_'.$m][$dt][$val['from_id']] += 1;
				}

				$day_login_user = $arr_login[date('Y-m-d', $s_time+86400*$m)];

				foreach($day_login_user as $key=>$value) {
										
						$userinfo = $arr_all_user[$value['uid']];
						$day_login['day_login_count_'.$m][$dt][$arr_all_user[$value['uid']]['from_id']] += 1;
						$login_count['login_count'][date('Y-m-d', ($s_time+86400*$m))][$userinfo['from_id']] += 1; //活跃的用户
				}

				

			}
			


// 			echo $c_date.PHP_EOL;
			
			
			//插入数据
			$all_data = array();
			foreach($qudao as $k=>$val) {
				
//				$pay_money = 0;

				// $pay_money_list = $stat->where(array('from_id'=>$val))->select();

				$sum_pay_map['state'] = 3;
				$sum_pay_map['from_id'] = $val;
				$pay_money = D('Paylog')->where($sum_pay_map)->sum('money');

				$data['from_id'] = intval($val);
				$data['create_time'] = intval(strtotime($c_date.' 00:00:00'));
				$data['all_reg_count'] = $stat_data['user_count'][$c_date][$val]; //总注册
				$data['all_reg_count_devid'] = $stat_data['user_count_devid'][$c_date][$val]; //按设备排重总注册
				$data['all_reg_bind'] = $stat_data['user_count_bind'][$c_date][$val]; //绑定的用户
				$data['all_cha_count'] = $stat_data['cha_count'][$c_date][$val]; //有效注册
				$data['zh_rate'] = 	round($stat_data['cha_count'][$c_date][$val]/$stat_data['user_count'][$c_date][$val], 2);//首日有效转化率
				$data['user_login'] = $active_arr[$val][strtotime($c_date.' 00:00:00')]; //活跃用户
				$data['pay_money'] = $stat_data['pay_money'][$c_date][$val];
				$data['pay_count'] = $stat_data['pay_count'][$c_date][$val];
				$data['first_pay_money'] = $stat_data['first_pay_money'][$c_date][$val];	//首日充值金额
				$data['first_pay_count'] = $stat_data['first_pay_count'][$c_date][$val];	//首日充值人数
				$data['pay_rate'] = round($data['pay_count']/$data['user_login'], 2);
				$data['first_pay_rate'] = round($stat_data['first_pay_count'][$c_date][$val]/$data['all_reg_count'], 2);	//首日付费率
				$data['pay_arpu'] = round($data['pay_money']/$data['pay_count'], 2);
				$data['fist_pay_arpu'] = round($data['first_pay_money']/$data['first_pay_count'], 2);
				
				
				
				for($i = 1; $i < 31; $i++) {
					if($i > 7 && $i != 13 && $i != 29) continue;

					$dd = date('Y-m-d', strtotime($c_date) + 86400 * $i);
					$data['day_'.$i] = round($day_login['day_login_count_'.$i][$dd][$val]/$stat_data['cha_count'][$c_date][$val], 2);
				}
				
				$data['pay_reg'] = round($data['pay_money']/$data['all_reg_count'], 2);
				$data['pay_login'] = round($data['pay_money']/$data['user_login'], 2);
				$data['first_pay_cha'] = round($data['first_pay_money']/$data['all_cha_count'], 2);
				$data['all_reg'] = $stat_data['user_count_sum'][$val];//累积注册
				$data['all_money'] = intval($pay_money) + $pay_money; //累积收入
				$data['reg_money'] = round($data['all_money']/$data['all_reg'], 2); //累积收入
                
                
				if($info = $stat->where(array('create_time'=>intval($data['create_time']), 'from_id'=>intval($val)))->find()){
					$stat->where(array('_id'=>$info['_id']))->save($data);
				}else{
					$stat->add($data);
				}

				//计算所有值
				$all_data['user_count'] += $stat_data['user_count'][$c_date][$val]; //总注册
				$all_data['user_bind'] += $stat_data['user_count_bind'][$c_date][$val]; //总注册
				$all_data['cha_count'] += $stat_data['cha_count'][$c_date][$val]; //有效注册
				$all_data['login_count'] += $active_arr[$val][strtotime($c_date.' 00:00:00')]; //活跃用户
				$all_data['pay_money'] = array_sum($stat_data['pay_money'][$c_date]); //$stat_data['pay_money'][$c_date][$val];
				$all_data['pay_count'] += $stat_data['pay_count'][$c_date][$val];
				$all_data['first_pay_money'] += $stat_data['first_pay_money'][$c_date][$val];	//首日充值金额
				$all_data['first_pay_count'] += $stat_data['first_pay_count'][$c_date][$val];	//首日充值人数
				
				
				for($i = 1; $i < 31; $i++) {
					if($i > 7 && $i != 13 && $i != 29) continue;

					$dd = date('Y-m-d', strtotime($c_date) + 86400 * $i);
					$all_data['day_'.$i] += $day_login['day_login_count_'.$i][$dd][$val];
				}
		
				$all_data['all_reg'] += $stat_data['user_count_sum'][$val];
			}
			
			
			
			if($c_date == '2021-06-04'){
			    var_dump();
			} 

			//计算最终值
			/*$pay_map['adddate'] = array('between', array($s_time, $e_time));
			$pay_map['state'] = 3;
			$pay_money = D('Paylog')->where($pay_map)->sum('money');
			*/

			$data = array();
			$data['from_id'] = 999; //表示全部的数据
			$data['create_time'] = strtotime($c_date.' 00:00:00');
			$data['all_reg_count'] = $all_data['user_count']; //总注册
			$data['all_reg_bind'] = $all_data['user_bind']; //总绑定
			$data['all_cha_count'] = $all_data['cha_count']; //有效注册
			$data['zh_rate'] = 	round($all_data['cha_count']/$all_data['user_count'], 2);//首日有效转化率
			$data['user_login'] = $all_data['login_count']; //活跃用户
			$data['pay_money'] = $all_data['pay_money'];
			$data['pay_count'] = $all_data['pay_count'];
			$data['first_pay_money'] = $all_data['first_pay_money'];	//首日充值金额
			$data['first_pay_count'] = $all_data['first_pay_count'];	//首日充值人数
			$data['pay_rate'] = round($data['pay_count']/$data['user_login'], 2);
			$data['first_pay_rate'] = round($all_data['first_pay_count']/$data['all_reg_count'], 2);	//首日付费率
			$data['pay_arpu'] = round($data['pay_money']/$data['pay_count'], 2);
			$data['fist_pay_arpu'] = round($data['first_pay_money']/$data['first_pay_count'], 2);
			
			for($i = 1; $i < 31; $i++) {
				if($i > 7 && $i != 13 && $i != 29) continue;

				$data['day_'.$i] = round($all_data['day_'.$i]/$all_data['cha_count'], 2);
			}
			
			$data['pay_reg'] = round($data['pay_money']/$data['all_reg_count'], 2);
			$data['pay_login'] = round($data['pay_money']/$data['user_login'], 2);
			$data['first_pay_cha'] = round($data['first_pay_money']/$data['all_cha_count'], 2);
			$data['all_reg'] = $all_data['all_reg'];//累积注册
			$data['all_money'] = intval($pay_money) + $data['pay_money']; //累积收入
			$data['reg_money'] = round($data['all_money']/$data['all_reg'], 2); //累积收入
            
            
			if($info = $stat->where(array('create_time'=>$data['create_time'], 'from_id'=>$data['from_id']))->find()){
				$stat->where(array('_id'=>$info['_id']))->save($data);
			}else{
				$stat->add($data);
			}

			$all_data = array();
			//结束计算
		}

		/*print_r($stat_data);
		$etime=microtime(true);
		$total=round($etime-$stime);
		echo "Run {$total}s times".PHP_EOL;
		exit();*/
// 		var_dump($stat_data);exit;
		
		$stat = D('Stat');
        
		for($g = 0; $g < (strtotime(date('Y-m-d')) - $s_time)/86400; $g++) {

			$c_date = date('Y-m-d', $s_time + 86400 * $g);
			foreach($qudao as $k=>$val) {
				
				$pay_money = 0;

				$pay_money_list = $stat->where(array('from_id'=>$val))->select();

				foreach($pay_money_list as $k1=>$val1) {
					$pay_money += $val1['money'];
				}

				$data['from_id'] = $val;
				$data['create_time'] = strtotime($c_date.' 00:00:00');
				$data['all_reg_count'] = $stat_data['user_count'][$c_date][$val]; //总注册
				$data['all_cha_count'] = $stat_data['cha_count'][$c_date][$val]; //有效注册
				$data['zh_rate'] = 	round($stat_data['cha_count'][$c_date][$val]/$stat_data['user_count'][$c_date][$val], 2);//首日有效转化率
				$data['user_login'] = $stat_data['login_count'][$c_date][$val]; //活跃用户
				$data['pay_money'] = $stat_data['pay_money'][$c_date][$val];
				$data['pay_count'] = $stat_data['pay_count'][$c_date][$val];
				$data['first_pay_money'] = $stat_data['first_pay_money'][$c_date][$val];	//首日充值金额
				$data['first_pay_count'] = $stat_data['first_pay_count'][$c_date][$val];	//首日充值人数
				$data['pay_rate'] = round($data['pay_count']/$data['user_login'], 2);
				$data['first_pay_rate'] = round($stat_data['first_pay_count'][$c_date][$val]/$data['user_login'], 2);	//首日付费率
				$data['pay_arpu'] = round($data['pay_money']/$data['pay_count'], 2);
				$data['fist_pay_arpu'] = round($data['first_pay_money']/$data['first_pay_count'], 2);
				
				for($i = 1; $i < 7; $i++) {
					$data['day_'.$i] = round($stat_data['day_login_count_'.$i][$c_date][$val]/$stat_data['cha_count'][$c_date][$val], 2);
					//echo $c_date."\t".$stat_data['day_login_count_'.$i][$c_date][$val].'/'.$stat_data['user_count'][$c_date][$val].PHP_EOL;
				}
				// var_dump(date("Y-m-d", $data['create_time']));
				// if(date("Y-m-d", $data['create_time']) == "2021-05-21") {
				//     var_dump($data);
				// }
				
				
				//14日
				$data['day_13'] = round($stat_data['day_login_count_13'][$c_date][$val]/$stat_data['cha_count'][$c_date][$val], 2);

				//30日
				$data['day_29'] = round($stat_data['day_login_count_29'][$c_date][$val]/$stat_data['cha_count'][$c_date][$val], 2);

				$data['pay_reg'] = round($data['pay_money']/$data['all_reg_count'], 2);
				$data['pay_login'] = round($data['pay_money']/$data['user_login'], 2);
				$data['first_pay_cha'] = round($data['first_pay_money']/$data['all_cha_count'], 2);
				$data['all_reg'] = D('Users')->where(array('from_id'=>$val, 'state'=>1))->count();//累积注册
				$data['all_money'] = intval($pay_money) + $data['pay_money']; //累积收入
				$data['reg_money'] = round($data['all_money']/$data['all_reg'], 2); //累积收入

				if($info = $stat->where(array('create_time'=>$data['create_time'], 'from_id'=>$data['from_id']))->find()){
					$stat->where(array('_id'=>$info['_id']))->save($data);
				}else{
					$stat->add($data);
				}
				

			}
		}



		$etime=microtime(true);
		$total=round($etime-$stime);
		echo "Run {$total}s times".PHP_EOL;
    }
}