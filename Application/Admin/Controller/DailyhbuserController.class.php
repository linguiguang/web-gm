<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/21
 * Time: 16:23
 */
namespace Admin\Controller;
use Think\Controller;

//红包场玩家每日输赢
class DailyhbuserController extends Controller{

    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }
        $map['c_time'] = array('between', array($b_time, $e_time));

        $list = D('Qrredbag')->where($map)->select();

        $data = array();
        foreach ($list as $v){
            $stat = $this->operate_data($v['get'], $v['use']);
            $data[$v['uid']]['get'] += intval($stat['get']);
            $data[$v['uid']]['use'] += intval($stat['use']);
            $data[$v['uid']]['tax'] += intval($v['tax']);
            $data[$v['uid']]['char_id'] = $v['char_id'];

            if(in_array($v['redbag'], array(62, 65, 70))){//福袋送的红包
                $data[$v['uid']]['fd_redbag'] += 60;
                $data[$v['uid']]['redbag'] += intval($v['redbag'])-60;
            }else{
                $data[$v['uid']]['redbag'] += intval($v['redbag']);
            }
        }

        $Dailyhbuser = D('Dailyhbuser');
        foreach ($data as $k=>$v){
            $ret['c_time'] = $b_time;
            $ret['uid'] = $k;
            $ret['char_id'] = $v['char_id'];
            $ret['get'] = intval($v['get'] - $v['tax']);
            $ret['use'] = intval($v['use']);
            $ret['tax'] = intval($v['tax']);
            $ret['redbag'] = intval($v['redbag']);
            $ret['fd_redbag'] = intval($v['fd_redbag']);
            $ret['yinli'] = $ret['get'] - $ret['use'];

            $dap['uid'] = $k;
            $dap['c_time'] = $b_time;
            if($info = $Dailyhbuser->where($dap)->find()){
                $Dailyhbuser->where(array('_id'=>$info['_id']))->save($ret);
            }else{
                $Dailyhbuser->add($ret);
            }
        }

        $etime = microtime(true);
        $totals = round($etime-$stime);
        echo 'Run '.$totals.' S';
    }

    //纯输赢
    protected function operate_data($get, $use){
        if($get > $use){
            $data['get'] = $get - $use;
            $data['use'] = 0;
        }else{
            $data['get'] = 0;
            $data['use'] = $use - $get;
        }
        return $data;
    }

}