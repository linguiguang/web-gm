<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Admin\Model\AuthRuleModel;
use Think\Controller;

/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class ChannelApiController extends Controller
{

    /**
     * 后台控制器初始化
     * http://ruiqumanage:8015/index.php/ChannelApi/
     */
    protected function _initialize()
    {

        /* 读取数据库中的配置 */
        $config = S('DB_CONFIG_DATA');
        if (!$config) {
            $config = api('Config/lists');
            S('DB_CONFIG_DATA', $config);
        }
        C($config); //添加配置

    }

    public function index()
    {
		//print(md5(getRandomString(6)));exit;
        print(iconv('UTF-8','GB2312',"欢迎使用渠道API接口"));

    }

    /* 
    json 中文转码函数
    */
    public function arrayrecursive(&$array, $function, $apply_to_keys_also = false) 
    { 
        static $recursive_counter = 0; 
        if (++$recursive_counter > 1000) { 
            die('possible deep recursion attack'); 
        } 
        foreach ($array as $key => $value) { 
            if (is_array($value)) { 
                $this->arrayrecursive($array[$key], $apply_to_keys_also); 
            } else { 
                $array[$key] = $function($value); 
            } 
    
            if ($apply_to_keys_also && is_string($key)) { 
                $new_key = $function($key); 
                if ($new_key != $key) { 
                    $array[$new_key] = $array[$key]; 
                    unset($array[$key]); 
                } 
            } 
        } 
        $recursive_counter--; 
    } 
    public function jsontext($array) { 
        $this->arrayrecursive($array, 'urlencode', true); 
        $json = json_encode($array); 
        return urldecode($json); 
    } 
    /* 
    json 中文转码函数
    */

    /*
    首次根据设备号注册
    url(Get方法)
    http://ruiqumanage:8015/index.php/ChannelApi/regiter?devid=1c4e9a80-1972-46c0-8a2c-395360310254&channel_id=7001&hash=1419484c3b2b23d607767b14f0c57ce3
    参数列表:
    1 devid			//设备ID
    2 channel_id		//渠道ID
    3 hash				//hash计算 
    hash是加密md5规则md5($devid.$secret_key);(字符串无缝拼接)
    status:1 Error:invalid chnId			--无效渠道id
    status:2 Error:hash compare failed		--哈希值不一致
    status:3 Error:account not exist		--账号不存在
    status:4 Error:channel id not peek		--渠道不匹配
    status:5 Error:user not exist			--角色不存在
    {"status":"0"
        ,"result":"{
            "account":"10386",
            "user_name":"荧光粉",
            "is_trigger":"0",
            "is_new_user":"0",
            "chn_id":"7001",
            "device_id":"1c4e9a80-1972-46c0-8a2c-395360310254",
            "klm_win":"0",
            "klm_stage":"0",
            "tly_win":"37394250",
            "tly_stage":"0",
            "recharge":"0"
        }"
    }
    gt 表示 > 大于
    egt 表示 >= 大于等于
    lt 表示 < 小于
    elt 表示 <= 小于等于
    eq 表示 = 等于
    */
    public function regiter()
    {
		
        $data['devid']       =   I('devid');
        $data['channel_id']       =   I('channel_id');
        $data['hash']       =   I('hash');

        if (empty($data['channel_id']) || empty($data['devid']) || empty($data['hash']))
        {
            $ret['status']=1;
            $ret['error']="接口字段缺失"; 
        }
        else
        {
            $umap['d_accounts.devid'] = $data['devid'];         
            $user = D('DAccounts')->join('game1.d_users as u ON u.account = d_accounts.id')->where($umap)->find();
            if (empty($user))
            {
                $ret['status']=1;
                $ret['error']="设备号不一致"; 
                //注册用户
            }        
            else if ($data['channel_id']!=$user['from_id'])
            {
                $ret['status']=2;
                $ret['error']="渠道不匹配"; 
            }
            else
            {
                $stop_arr = array(7010, 7007);
                $cmap['from_id'] = $user['from_id'];
                //$cmap['from_time'] = $user['from_time'];
                $cmap['reg'] = 1;
                $dtime = NOW_TIME;
                $cmap['start_time'] = array('elt', $dtime);
		        $cmap['end_time'] = array('egt', $dtime);
                //print_r();
                $from_time = D('DChannels')->where($cmap)->order('from_time desc')->getField('from_time'); 
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['status'] = 3;
                    $ret['error'] = "渠道不匹配"; 
                }
                else
                {
                    $myhash = md5($data['devid'].$data['channel_id'].$secret_key);        
                    if ($data['hash']!=$myhash)
                    {
                        $ret['status'] = 3;
                        $ret['error'] = "哈希值不一致:".$myhash;
                    
                    }
                    else if($user['reg_from_task']!=$from_time && in_array($data['channel_id'],$stop_arr))
                    {
                        $ret['status'] = 4;
                        $ret['error'] = "老用户不用注册";
                    }
                    else
                    {
                        $cumap['from_id'] = $user['from_id'];
                        $cumap['from_time'] = $from_time;
                        $cumap['uid'] = $user['id']; 
                                          
                        $chatask = D('DChannelUserTasks')->where($cumap)->select(); 
                        $usertask = array();
                        foreach ($chatask as $k => $v) {
                            $usertask[$v['room_type']][$v['task_type']]['value'] = $v['value'];                                                        
                            $usertask[$v['room_type']][$v['task_type']]['stage'] = $v['stage']>0?$v['stage']-1:0;                          
                        }
                   
                        $ret['status']=0;
                        $_list = array(
                            "account"=> $user['account'], 
                            "user_name"=> $user['name'],  	//玩家名称
                            "is_trigger"=> $user['from_time']==$from_time?1:0,  		//是否激活当前渠道 1:激活
                            "is_new_user"=> $user['reg_from_task']==$from_time?1:0, 		//是否新用户 1:是
                            "chn_id"=> $user['from_id'],  	//渠道用户id
                            "device_id"=> $user['devid'],  	//设备号唯一标识
                            "klm_win"=> $usertask[1][1]['value']?intval($usertask[1][1]['value']):0,     	//卡拉姆-赢金
                            "klm_stage"=> $usertask[1][1]['stage']?intval($usertask[1][1]['stage']):0,   		//卡拉姆-闯关数
                            "tly_win"=> $usertask[2][1]['value']?intval($usertask[2][1]['value']):0,     	//特洛伊-赢金
                            "tly_stage"=> $usertask[2][1]['stage']?intval($usertask[2][1]['stage']):0,   		//特洛伊-闯关数
                            "recharge"=> $usertask[0][2]['value']?intval($usertask[0][2]['value']):0,    	//玩家充值总额（元）
                        ); 
                        $ret['result'] = $_list;
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }

    /*
    首次根据设备号注册-IOS专用
    url(Get方法)
    http://ruiqumanage:8015/index.php/ChannelApi/regiter?devid=1c4e9a80-1972-46c0-8a2c-395360310254&channel_id=7001&hash=1419484c3b2b23d607767b14f0c57ce3
    参数列表:
    1 devid			//设备ID
    2 channel_id		//渠道ID
    3 hash				//hash计算 
    hash是加密md5规则md5($devid.$secret_key);(字符串无缝拼接)
    status:1 Error:invalid chnId			--无效渠道id
    status:2 Error:hash compare failed		--哈希值不一致
    status:3 Error:account not exist		--账号不存在
    status:4 Error:channel id not peek		--渠道不匹配
    status:5 Error:user not exist			--角色不存在
    {"status":"0"
        ,"result":"{
            "account":"10386",
            "user_name":"荧光粉",
            "is_trigger":"0",
            "is_new_user":"0",
            "chn_id":"7001",
            "device_id":"1c4e9a80-1972-46c0-8a2c-395360310254",
            "klm_win":"0",
            "klm_stage":"0",
            "tly_win":"37394250",
            "tly_stage":"0",
            "recharge":"0"
        }"
    }
    gt 表示 > 大于
    egt 表示 >= 大于等于
    lt 表示 < 小于
    elt 表示 <= 小于等于
    eq 表示 = 等于
    */
    public function iosregiter()
    {
		
        $data['devid']       =   I('devid');
        $data['channel_id']       =   I('channel_id');
        $data['hash']       =   I('hash');

        if (empty($data['channel_id']) || empty($data['devid']) || empty($data['hash']))
        {
            $ret['status']=1;
            $ret['error']="接口字段缺失"; 
        }
        else
        {
            $umap['d_accounts.devid'] = $data['devid'];         
            $user = D('DAccounts')->join('game1.d_users as u ON u.account = d_accounts.id')->where($umap)->find();
            if (empty($user))
            {
                $ret['status']=1;
                $ret['error']="设备号不一致"; 
                //注册用户
            }        
            else if ($data['channel_id']!=$user['from_id'])
            {
                $ret['status']=2;
                $ret['error']="渠道不匹配"; 
                // 跟服务端打个招呼
                $post_url = C('GM_URL') .'cgi-bin/gm_oprate.modify_ios_chn';
                //$post_url = 'http://47.98.174.237:8180/svjhs/cgi-bin/gm_oprate.modify_ios_chn';
                $post_data['timestamp'] = intval(time());
                $post_data['sign'] = md5(C('SENDGAME_KEY').time());
                $post_data['from_id'] =  intval($data['channel_id']);
                $post_data['account'] = intval($user['account']);
                //$post_data['dev_id'] = $data['devid'];
                $field = json_encode($post_data);
                $rs = PostUrl($post_url, $field,1);
            }
            else
            {
                $stop_arr = array(7010, 7007);
                $cmap['from_id'] = $user['from_id'];
                //$cmap['from_time'] = $user['from_time'];
                $cmap['reg'] = 1;
                $dtime = NOW_TIME;
                $cmap['start_time'] = array('elt', $dtime);
		        $cmap['end_time'] = array('egt', $dtime);
                //print_r();
                $from_time = D('DChannels')->where($cmap)->order('from_time desc')->getField('from_time'); 
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['status'] = 3;
                    $ret['error'] = "渠道不匹配"; 
                }
                else
                {
                    $myhash = md5($data['devid'].$data['channel_id'].$secret_key);        
                    if ($data['hash']!=$myhash)
                    {
                        $ret['status'] = 3;
                        $ret['error'] = "哈希值不一致:".$myhash;
                    
                    }
                    else if($user['reg_from_task']!=$from_time && in_array($data['channel_id'],$stop_arr))
                    {
                        $ret['status'] = 4;
                        $ret['error'] = "老用户不用注册";
                    }
                    else
                    {
                        $cumap['from_id'] = $user['from_id'];
                        $cumap['from_time'] = $from_time;
                        $cumap['uid'] = $user['id']; 
                                          
                        $chatask = D('DChannelUserTasks')->where($cumap)->select(); 
                        $usertask = array();
                        foreach ($chatask as $k => $v) {
                            $usertask[$v['room_type']][$v['task_type']]['value'] = $v['value'];                                                        
                            $usertask[$v['room_type']][$v['task_type']]['stage'] = $v['stage']>0?$v['stage']-1:0;                          
                        }
                   
                        $ret['status']=0;
                        $_list = array(
                            "account"=> $user['account'], 
                            "user_name"=> $user['name'],  	//玩家名称
                            "is_trigger"=> $user['from_time']==$from_time?1:0,  		//是否激活当前渠道 1:激活
                            "is_new_user"=> $user['reg_from_task']==$from_time?1:0, 		//是否新用户 1:是
                            "chn_id"=> $user['from_id'],  	//渠道用户id
                            "device_id"=> $user['devid'],  	//设备号唯一标识
                            "klm_win"=> $usertask[1][1]['value']?intval($usertask[1][1]['value']):0,     	//卡拉姆-赢金
                            "klm_stage"=> $usertask[1][1]['stage']?intval($usertask[1][1]['stage']):0,   		//卡拉姆-闯关数
                            "tly_win"=> $usertask[2][1]['value']?intval($usertask[2][1]['value']):0,     	//特洛伊-赢金
                            "tly_stage"=> $usertask[2][1]['stage']?intval($usertask[2][1]['stage']):0,   		//特洛伊-闯关数
                            "recharge"=> $usertask[0][2]['value']?intval($usertask[0][2]['value']):0,    	//玩家充值总额（元）
                        ); 
                        $ret['result'] = $_list;
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }

    /* 
    url(Get方法)
    http://syhjvip.com:8180/channel.querytask?account=10386&channel_id=7001&hash=71093532633c2a9c95cd18e53853d661
    参数列表:
    1 account			//账号ID
    2 channel_id		//渠道ID
    3 hash				//hash计算
    hash是加密md5 规则:md5($account.$channel_id.$secret_key);    
    错误状态&描述:
    status:1 Error:invalid uid:xxx			--无效角色id
    status:2 Error:hash compare failed		--哈希值不一致
    status:3 Error:user not exist			--角色不存在
    */
    public function querytask()
    {
		
        $data['account']       =   I('account');
        $data['channel_id']       =   I('channel_id');
        $data['hash']       =   I('hash');

        if (empty($data['channel_id']) || empty($data['account']) || empty($data['hash']))
        {
            $ret['status']=1;
            $ret['error']="接口字段缺失"; 
        }
        else
        {
            $umap['d_accounts.id'] = $data['account'];         
            $user = D('DAccounts')->join('game1.d_users as u ON u.account = d_accounts.id')->where($umap)->find();
            if (empty($user))
            {
                $ret['status']=1;
                $ret['error']="账号不存在"; 
                //注册用户
            } 
            else if ($user['attr']>=8)
            {
                $ret['status']=1;
                $ret['error']="未知错误"; 
                //禁止渠道访问
            }        
            else if ($data['channel_id']!=$user['from_id'])
            {
                $ret['status']=2;
                $ret['error']="渠道不匹配"; 
            }
            else
            {
                $cmap['from_id'] = $user['from_id'];
                $cmap['reg'] = 1;
                $dtime = NOW_TIME;
                $cmap['start_time'] = array('elt', $dtime);
		        $cmap['end_time'] = array('egt', $dtime);

                //$cmap['from_time'] = $user['from_time'];

                $from_time = D('DChannels')->where($cmap)->order('from_time desc')->getField('from_time');        
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['status'] = 3;
                    $ret['error'] = "渠道不匹配"; 
                }
                else
                {
                    $myhash = md5($data['account'].$data['channel_id'].$secret_key);        
                    if ($data['hash']!=$myhash)
                    {
                        $ret['status'] = 3;
                        $ret['error'] = "哈希值不一致:".$myhash;
                    
                    }
                    else
                    {
                        
                        $cumap['from_id'] = $user['from_id'];
                        $cumap['from_time'] = $from_time;
                        //$cumap['from_time'] = $user['from_time'];
                        $cumap['uid'] = $user['id']; 
                                          
                        $chatask = D('DChannelUserTasks')->where($cumap)->select(); 
                        $usertask = array();
                        foreach ($chatask as $k => $v) {
                            $usertask[$v['room_type']][$v['task_type']]['value'] = ($usertask[$v['room_type']][$v['task_type']]['value']?$usertask[$v['room_type']][$v['task_type']]['value']:0)+$v['value'];                                                        
                            $usertask[$v['room_type']][$v['task_type']]['stage'] = $v['stage']>0?$v['stage']-1:0;                          
                        }
                        $ret['status']=0;
                        $_list = array(
                            "account"=> $user['account'], 
                            "user_name"=> $user['name'],  	//玩家名称
                            "is_trigger"=> $user['from_time']==$from_time?1:0, 		//是否激活当前渠道 1:激活
                            "is_new_user"=> $user['reg_from_task']==$from_time?1:0, 		//是否新用户 1:是
                            "chn_id"=> $user['from_id'],  	//渠道用户id
                            "device_id"=> $user['devid'],  	//设备号唯一标识
                            "klm_win"=> $usertask[1][1]['value']?intval($usertask[1][1]['value']):0,     	//卡拉姆-赢金
                            "klm_stage"=> $usertask[1][1]['stage']?intval($usertask[1][1]['stage']):0,   		//卡拉姆-闯关数
                            "tly_win"=> $usertask[2][1]['value']?intval($usertask[2][1]['value']):0,     	//特洛伊-赢金
                            "tly_stage"=> $usertask[2][1]['stage']?intval($usertask[2][1]['stage']):0,   		//特洛伊-闯关数
                            "recharge"=> $usertask[0][2]['value']?intval($usertask[0][2]['value']):0,    	//玩家充值总额（元）
                        ); 
                        $ret['result'] = $_list;
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }

    /* 
    用户user_id激活老用户激活:
    参数列表:
    1 account			//账号ID
    2 channel_id		//渠道ID
    3 timestamp		//时间戳
    3 hash				//hash计算
    http://syhjvip.com:8180/channel.activeuser?account=10386&channel_id=7001&timestamp=1626850756&hash=c228555e4e2b4ef1723844f64759f910

    hash 生成方式1:md5($account.$channel_id.$secret_key.$time);   
    错误状态&描述:
    status:1 Error:invalid uid:xxx			--无效角色id
    status:2 Error:hash compare failed		--哈希值不一致
    status:3 Error:user not exist			--角色不存在
    */
    public function activeuser()
    {
		
        $data['account']       =   I('account');
        $data['channel_id']       =   I('channel_id');
        $data['timestamp']       =   I('timestamp');
        $data['hash']       =   I('hash');
        

        if (empty($data['channel_id']) || empty($data['account']) || empty($data['timestamp']) || empty($data['hash']))
        {
            $ret['status']=1;
            $ret['error']="接口字段缺失"; 
        }
        else
        {
            $umap['d_accounts.id'] = $data['account'];         
            $user = D('DAccounts')->join('game1.d_users as u ON u.account = d_accounts.id')->where($umap)->find();
            if (empty($user))
            {
                $ret['status']=1;
                $ret['error']="账号不存在"; 
                //注册用户
            }        
            else if ($data['channel_id']!=$user['from_id'])
            {
                $ret['status']=2;
                $ret['error']="渠道不匹配"; 
            }
            else
            {
                $cmap['from_id'] = $user['from_id'];
                $cmap['reg'] = 1;
                $dtime = NOW_TIME;
                $cmap['start_time'] = array('elt', $dtime);
		        $cmap['end_time'] = array('egt', $dtime);
                $from_time = D('DChannels')->where($cmap)->order('from_time desc')->getField('from_time');           
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['status'] = 3;
                    $ret['error'] = "渠道不匹配"; 
                }
                else
                {
                    $myhash = md5($data['account'].$data['channel_id'].$secret_key.$data['timestamp']);        
                    if ($data['hash']!=$myhash)
                    {
                        $ret['status'] = 3;
                        $ret['error'] = "哈希值不一致:".$myhash;
                    
                    }
                    else
                    {
                        
                        $cumap['from_id'] = $user['from_id'];
                        $cumap['from_time'] = $from_time;
                        $cumap['uid'] = $user['id']; 
                                          
                        $chatask = D('DChannelUserTasks')->where($cumap)->select(); 
                        $usertask = array();
                        foreach ($chatask as $k => $v) {
                            $usertask[$v['room_type']][$v['task_type']]['value'] = $v['value'];                                                        
                            $usertask[$v['room_type']][$v['task_type']]['stage'] = $v['stage']>0?$v['stage']-1:0;                          
                        }
                   
                        $ret['status']=0;
                        $_list = array(
                            "account"=> $user['account'], 
                            "user_name"=> $user['name'],  	//玩家名称
                            "is_trigger"=> $user['from_time']==$from_time?1:0, 		//是否激活当前渠道 1:激活
                            "is_new_user"=> $user['reg_from_task']==$from_time?1:0, 		//是否新用户 1:是
                            "chn_id"=> $user['from_id'],  	//渠道用户id
                            "device_id"=> $user['devid'],  	//设备号唯一标识
                            "klm_win"=> $usertask[1][1]['value']?intval($usertask[1][1]['value']):0,     	//卡拉姆-赢金
                            "klm_stage"=> $usertask[1][1]['stage']?intval($usertask[1][1]['stage']):0,   		//卡拉姆-闯关数
                            "tly_win"=> $usertask[2][1]['value']?intval($usertask[2][1]['value']):0,     	//特洛伊-赢金
                            "tly_stage"=> $usertask[2][1]['stage']?intval($usertask[2][1]['stage']):0,   		//特洛伊-闯关数
                            "recharge"=> $usertask[0][2]['value']?intval($usertask[0][2]['value']):0,    	//玩家充值总额（元）
                        ); 
                        $ret['result'] = $_list;
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }

     
    public function regiter2()
    {
		
        $data['devid']       =   I('devid');
        $data['channel_id']       =   I('channel_id');
        $data['hash']       =   I('hash');

        if (empty($data['channel_id']) || empty($data['devid']) || empty($data['hash']))
        {
            $ret['status']=1;
            $ret['error']="接口字段缺失"; 
        }
        else
        {
            $umap['d_accounts.devid'] = $data['devid'];         
            $user = D('DAccounts')->join('game1.d_users as u ON u.account = d_accounts.id')->where($umap)->find();
            if (empty($user))
            {
                $ret['status']=1;
                $ret['error']="设备号不一致"; 
                //注册用户
            }        
            else if ($data['channel_id']!=$user['from_id'])
            {
                $ret['status']=2;
                $ret['error']="渠道不匹配"; 
            }
            else
            {
                $cmap['from_id'] = $user['from_id'];
                //$cmap['from_time'] = $user['from_time'];
                $cmap['reg'] = 1;
                $dtime = NOW_TIME;
                $cmap['start_time'] = array('elt', $dtime);
		        $cmap['end_time'] = array('egt', $dtime);
                //print_r();
                $from_time = D('DChannels')->where($cmap)->order('from_time desc')->getField('from_time'); 
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['status'] = 3;
                    $ret['error'] = "渠道不匹配"; 
                }
                else
                {
                    $myhash = md5($data['devid'].$data['channel_id'].$secret_key);        
                    if ($data['hash']!=$myhash)
                    {
                        $ret['status'] = 3;
                        $ret['error'] = "哈希值不一致:".$myhash;
                    
                    }
                    else
                    {
                        $cumap['from_id'] = $user['from_id'];
                        $cumap['from_time'] = $from_time;
                        $cumap['uid'] = $user['id']; 
                                          
                        $chatask = D('DChannelUserTasks')->where($cumap)->select(); 
                        $usertask = array();
                        foreach ($chatask as $k => $v) {
                            $usertask[$v['room_type']][$v['task_type']]['value'] = $v['value'];                                                        
                            $usertask[$v['room_type']][$v['task_type']]['stage'] = $v['stage']>0?$v['stage']-1:0;                          
                        }
                   
                        $ret['status']=0;
                        $_list = array(
                            "account"=> $user['account'], 
                            "user_name"=> $user['name'],  	//玩家名称
                            "is_trigger"=> $user['from_time']==$from_time?1:0,  		//是否激活当前渠道 1:激活
                            "is_new_user"=> $user['reg_from_task']==$from_time?1:0, 		//是否新用户 1:是
                            "chn_id"=> $user['from_id'],  	//渠道用户id
                            "device_id"=> $user['devid'],  	//设备号唯一标识
                            "klm_win"=> $usertask[1][1]['value']?intval($usertask[1][1]['value']):0,     	//卡拉姆-赢金
                            "klm_stage"=> $usertask[1][1]['stage']?intval($usertask[1][1]['stage']):0,   		//卡拉姆-闯关数
                            "tly_win"=> $usertask[2][1]['value']?intval($usertask[2][1]['value']):0,     	//特洛伊-赢金
                            "tly_stage"=> $usertask[2][1]['stage']?intval($usertask[2][1]['stage']):0,   		//特洛伊-闯关数
                            "recharge"=> $usertask[0][2]['value']?intval($usertask[0][2]['value']):0,    	//玩家充值总额（元）
                        ); 
                        $ret['result'] = $_list;
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }

    public function querytask2()
    {
		
        $data['devid']       =   I('devid');
        $data['channel_id']       =   I('channel_id');
        $data['hash']       =   I('hash');

        if (empty($data['channel_id']) || empty($data['devid']) || empty($data['hash']))
        {
            $ret['status']=1;
            $ret['error']="接口字段缺失"; 
        }
        else
        {
            $umap['d_accounts.devid'] = $data['devid'];         
            $user = D('DAccounts')->join('game1.d_users as u ON u.account = d_accounts.id')->where($umap)->find();
            if (empty($user))
            {
                $ret['status']=1;
                $ret['error']="设备号不一致"; 
                //注册用户
            }        
            else if ($data['channel_id']!=$user['from_id'])
            {
                $ret['status']=2;
                $ret['error']="渠道不匹配"; 
            }
            else
            {
                $cmap['from_id'] = $user['from_id'];
                $cmap['reg'] = 1;
                $dtime = NOW_TIME;
                $cmap['start_time'] = array('elt', $dtime);
		        $cmap['end_time'] = array('egt', $dtime);

                //$cmap['from_time'] = $user['from_time'];

                $from_time = D('DChannels')->where($cmap)->order('from_time desc')->getField('from_time');        
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['status'] = 3;
                    $ret['error'] = "渠道不匹配"; 
                }
                else
                {
                    $myhash = md5($data['devid'].$data['channel_id'].$secret_key);        
                    if ($data['hash']!=$myhash)
                    {
                        $ret['status'] = 3;
                        $ret['error'] = "哈希值不一致:".$myhash;
                    
                    }
                    else
                    {
                        
                        $cumap['from_id'] = $user['from_id'];
                        $cumap['from_time'] = $from_time;
                        //$cumap['from_time'] = $user['from_time'];
                        $cumap['uid'] = $user['id']; 
                                          
                        $chatask = D('DChannelUserTasks')->where($cumap)->select(); 
                        $usertask = array();
                        foreach ($chatask as $k => $v) {
                            $usertask[$v['room_type']][$v['task_type']]['value'] = ($usertask[$v['room_type']][$v['task_type']]['value']?$usertask[$v['room_type']][$v['task_type']]['value']:0)+$v['value'];                                                       
                            $usertask[$v['room_type']][$v['task_type']]['stage'] = $v['stage']>0?$v['stage']-1:0;                          
                        }

                        $ret['status']=0;
                        $_list = array(
                            "account"=> $user['account'], 
                            "user_name"=> $user['name'],  	//玩家名称
                            "is_trigger"=> $user['from_time']==$from_time?1:0, 		//是否激活当前渠道 1:激活
                            "is_new_user"=> $user['reg_from_task']==$from_time?1:0, 		//是否新用户 1:是
                            "chn_id"=> $user['from_id'],  	//渠道用户id
                            "device_id"=> $user['devid'],  	//设备号唯一标识
                            "klm_win"=> $usertask[1][1]['value']?intval($usertask[1][1]['value']):0,     	//卡拉姆-赢金
                            "klm_stage"=> $usertask[1][1]['stage']?intval($usertask[1][1]['stage']):0,   		//卡拉姆-闯关数
                            "tly_win"=> $usertask[2][1]['value']?intval($usertask[2][1]['value']):0,     	//特洛伊-赢金
                            "tly_stage"=> $usertask[2][1]['stage']?intval($usertask[2][1]['stage']):0,   		//特洛伊-闯关数
                            "recharge"=> $usertask[0][2]['value']?intval($usertask[0][2]['value']):0,    	//玩家充值总额（元）
                        ); 
                        $ret['result'] = $_list;
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }

    public function querycharge()
    {
		//userid channel timestamp sign begintime endtime
        $data['userid']       =   I('userid');
        $data['channel_id']       =   I('channel');
        //$data['timestamp']       =   I('timestamp');
        $data['sign']       =   I('sign');
        $data['begintime']       =   I('begintime');
        $data['endtime']       =   I('endtime');

        if (empty($data['channel_id']) || empty($data['userid'])  
        || empty($data['sign']) || empty($data['begintime']) || empty($data['endtime']))
        {
            $ret['code']=0;
            $ret['message']="接口字段缺失"; 
        }
        else
        {
            $umap['id'] = $data['userid'];         
            $user = D('DAccounts')->where($umap)->find();
            if (empty($user))
            {
                $ret['code']=0;
                $ret['message']="账号不存在"; 
                //注册用户
            }
            else
            {
                $cmap['from_id'] = $data['channel_id'];     
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['code'] = 0;
                    $ret['message'] = "渠道不匹配"; 
                }
                else
                {
                    $myhash = md5($data['userid'].$data['channel_id'].$secret_key);        
                    if ($data['sign']!=$myhash)
                    {
                        $ret['code'] = 0;
                        $ret['message'] = "哈希值不一致:".$myhash;
                    
                    }
                    else
                    {                                          
                        $charge = D('DPays')->where(['UNIX_TIMESTAMP(created_at)' => ['BETWEEN', [$data['begintime'], $data['endtime']]]
                        , 'account' => $data['userid']
                        , 'from_id' => $data['channel_id']
                        , 'status' => 1
                        , 'shop_id' => [array('not between', array(1600001, 1699999)),array('neq', 320001),array('neq', 1200010)]])
                        ->field('SUM(IFNULL(price,0)) as money')->find(); 
                   
                        $ret['code']=200;
                        $ret['message'] = "success"; 
                        $_list = array(
                            "recharge"=> intval($charge['money'])
                        ); 
                        $ret['data'] = $_list;
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }

    /* 
    用户user_id激活老用户激活:
    参数列表:
    1 account			//账号ID
    2 channel_id		//渠道ID
    3 timestamp		//时间戳
    3 hash				//hash计算
    http://syhjvip.com:8180/channel.activeuser?account=10386&channel_id=7001&timestamp=1626850756&hash=c228555e4e2b4ef1723844f64759f910

    hash 生成方式1:md5($account.$channel_id.$secret_key.$time);   
    错误状态&描述:
    status:1 Error:invalid uid:xxx			--无效角色id
    status:2 Error:hash compare failed		--哈希值不一致
    status:3 Error:user not exist			--角色不存在
    */
    public function exchange_chn()
    {
		
        $data['account']       =   I('merid');
        $data['channel_id']       =   I('channel');
        $data['timestamp']       =   I('timestamp');
        $data['hash']       =   I('keycode');
        

        if (empty($data['channel_id']) || empty($data['account']) || empty($data['timestamp']) || empty($data['hash']))
        {
            //请求必要字段校验
            $ret['success']=5;
            $ret['msg']="接口字段缺失"; 
        }
        else
        {
            $umap['d_accounts.id'] = $data['account'];         
            $user = D('DAccounts')->join('game1.d_users as u ON u.account = d_accounts.id')->where($umap)->find();
            if (empty($user))
            {
                //判断账号是否有效
                $ret['success']=6;
                $ret['msg']="账号不存在"; 
                
            }        
            else if ($data['channel_id']!=$user['from_id'])
            {
                //渠道不匹配 修改渠道
                $dmap['from_id'] = $data['channel_id'];
                $dmap['reg'] = 1;
                $dtime = NOW_TIME;
                $dmap['start_time'] = array('elt', $dtime);
		        $dmap['end_time'] = array('egt', $dtime);
                $dfrom_time = D('DChannels')->where($dmap)->order('from_time desc')->getField('from_time');           
                $dsecret_key = D('DChannels')->where($dmap)->order('from_time desc')->getField('secret_key'); 

                $smap['from_id'] = $user['from_id'];
                $smap['reg'] = 1;
                $smap['start_time'] = array('elt', $dtime);
		        $smap['end_time'] = array('egt', $dtime);
                $sfrom_time = D('DChannels')->where($smap)->order('from_time desc')->getField('from_time');  
                if($sfrom_time==$user['from_time'])
                {
                    //原本渠道最新期 不能转 返回失败
                    $ret['success'] = 9;
                    $ret['msg'] = "已在旧渠道注册最新期"; 
                }
                else if(empty($dsecret_key)) 
                {
                    //判断请求的渠道号在系统中是否存在且有效
                    $ret['success'] = 8;
                    $ret['msg'] = "无效的转换渠道"; 
                }
                else
                {
                    $myhash = md5($data['account'].$data['channel_id'].$dsecret_key.$data['timestamp']);        
                    if ($data['hash']!=$myhash)
                    {
                        //api请求 安全校验
                        $ret['success'] = 4;
                        $ret['msg'] = "哈希值不一致:".$data['hash'];
                    
                    }
                    else
                    {
                        //修改用户表的from_id 和from_time 到请求的渠道和该渠道最新期数
                        $sdata['from_id'] = $data['channel_id'];;
                        $sdata['from_time'] = $dfrom_time;
                
                        if (D('DUsers')->where(array('account' => $data['account']))->save($sdata))
                        {
                            $ret['success'] = 1;
                            $ret['msg'] = "已激活";
                            //发送给游戏服务端
                            $post_url = C('GM_URL') .'sys/channel/register_dev_chn2';
                            $post_data['channel_id'] =  intval($data['channel_id']);
                            $post_data['devid'] = $user['devid'];
                            $post_data['flag'] = 1;
                            $post_data['hash'] = md5($post_data['devid'].$post_data['channel_id'].$dsecret_key);
                            $field = http_build_query($post_data);
                            $field = str_replace('%2C', ',', $field);
                            $rets = PostUrl($post_url, $field,0);

                        }
                        else
                        {
                            $ret['success'] = 7;
                            $ret['msg'] = "激活失败";
                        }  
                        
                    }
                }
            }
            else
            {
                $cmap['from_id'] = $user['from_id'];
                $cmap['reg'] = 1;
                $dtime = NOW_TIME;
                $cmap['start_time'] = array('elt', $dtime);
		        $cmap['end_time'] = array('egt', $dtime);
                $from_time = D('DChannels')->where($cmap)->order('from_time desc')->getField('from_time');           
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['success'] = 3;
                    $ret['msg'] = "密钥不匹配"; 
                }
                else
                {
                    $myhash = md5($data['account'].$data['channel_id'].$secret_key.$data['timestamp']);        
                    if ($data['hash']!=$myhash)
                    {
                        $ret['success'] = 4;
                        $ret['msg'] = "哈希值不一致:".$data['hash'];
                    
                    }
                    else
                    {
                        if($user['from_time']==$from_time)
                        {
                            $ret['success'] = 1;
                            $ret['msg'] = "已激活";
                        }
                        else
                        {
                            $ret['success'] = 7;
                            $ret['msg'] = "激活失败";
                        }  
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }

    //查询用户天梯榜
    public function queryrandk()
    {
		//accid channel timestamp sign begintime endtime
        $data['userid']       =   I('accid');
        $data['channel_id']       =   I('channel');
        $data['sign']       =   I('sign');

        if (empty($data['channel_id']) || empty($data['userid']) || empty($data['sign']))
        {
            $ret['code']=0;
            $ret['message']="接口字段缺失"; 
        }
        else
        {
            $umap['id'] = $data['userid'];         
            $user = D('DAccounts')->where($umap)->find();
            $users = D("DUsers")->where(['account' => $data['userid']])->find(); 
            if (empty($user) || empty($users))
            {
                $ret['code']=0;
                $ret['message']="账号不存在"; 
                //注册用户
            }
            else
            {
                $cmap['from_id'] = $data['channel_id'];     
                $secret_key = D('DChannels')->where($cmap)->order('from_time desc')->getField('secret_key'); 
                if (empty($secret_key)) 
                {
                    $ret['code'] = 0;
                    $ret['message'] = "渠道不匹配"; 
                }
                else
                {
                    $myhash = md5($data['userid'].$data['channel_id'].$secret_key);        
                    if ($data['sign']!=$myhash)
                    {
                        $ret['code'] = 0;
                        $ret['message'] = "哈希值不一致:".$myhash;
                    
                    }
                    else
                    {          
                                                       
                        $charge = D("DRanks")->where(['r_type' => 300, 'periods' => 0, 'uid' => $users['id']])
                        ->field('value1')->find(); 
                        $ret['code']=200;
                        $ret['message'] = "success"; 
                        $_list = array(
                            "win_gold"=> intval($charge['value1'])
                        ); 
                        $ret['data'] = $_list;
                        
                    }
                }
            }

        }      
        exit(json_encode($ret));  
    }
 
}