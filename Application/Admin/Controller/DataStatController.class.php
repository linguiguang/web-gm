<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/15
 * Time: 15:18
 */

namespace Admin\Controller;

use Admin\Controller\AdminController;

/**
 * 数据统计
 */
class DataStatController extends AdminController
{

    /**
     * 平台统计
     */
    public function platstat()
    {
        if (!isset($_GET['from_id'])) {
            $from_id = 999;
        } else {
            $from_id = intval(I('from_id'));
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;

        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['create_time'] = array('egt', $b_time);
        }

        $map['from_id'] = $from_id;

        if (!isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['create_time'] = array('egt', strtotime(date('Y-m-d 00:00:00')) - 604800);
            $list = D('Stat')->where($map)->order('create_time desc')->limit(7)->select();
        } else {
            $list = D('Stat')->where($map)->order('create_time desc')->select();
        }
        //var_dump( $map );exit;


        $this->assign('from_id', $from_id);
        $this->assign('_list', $list);

        $fromkey = D('DChannels')->group('from_id')->order('id asc')->select();



        $this->assign('from', $fromkey);
        $this->display();
    }

    /**
     * 渠道管理
     */
    public function fromkey($p = 1)
    {
        $fromobj = D('DChannels');

        $limit = 20;

        $b_time = I('b_time');
        $e_time = I('e_time');

        $from_id = trim(I('from_id'));
        if ($from_id != '') {
            $map['from_id'] = $from_id;
        }

        $num = trim(I('num'));
        if ($num != '') {
            $map['num'] = $num;
        }

        $from_name = trim(I('from_name'));
        if ($from_name != '') {
            $map['from_name'] = array('like', '%' . $from_name . '%');
        }


        $total = $fromobj->where($map)->count();
        $list = $fromobj->where($map)->page($p, $limit)->order('id desc')->select();

        // var_dump($fromobj->getLastSql());exit;

        $from_ids = array();
        // foreach ($list as $k => $val) {
        //     if (intval($val['parent_id']) > 0) {
        //         $list[$k]['parent_name'] = $fromobj->where(array('id' => $val['parent_id']))->getField('from_name');
        //     } else {
        //         $list[$k]['parent_name'] = '无';
        //     }

        //     $recharge = D('Paymoneystat')->fromRecharge(intval($val['from_id']), $b_time, $e_time);
        //     $user = D('Users')->fromUser(intval($val['from_id']), $b_time, $e_time);

        //     $from_ids[] = intval($val['from_id']);
        //     $list[$k]['rechargeman'] = $recharge['man'];
        //     $list[$k]['rechargenum'] = $recharge['money'];
        //     $list[$k]['tenman'] = $recharge['tenman'];
        //     $list[$k]['_159man'] = $recharge['_159man'];
        //     $list[$k]['_799man'] = $recharge['_799man'];
        //     $list[$k]['bind'] = $user['bind'];
        //     $list[$k]['nums'] = $user['num'];
        // }

        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_list', $list);
        $this->assign('fromlist', $fromlist);
        $this->assign('allfromrecharges', D('Paylog')->allFromRecharges($from_ids, $b_time, $e_time));
        $this->assign('_page', $pageNav->show());
        $this->assign();

        $this->display();
    }

    public function addfKey()
    {
        if (IS_POST) {
            $fromobj = D('DChannels');
            $from_name = trim(I('post.from_name'));
            $from_id = trim(I('post.from_id'));
            $start_time = trim(I('post.start_time'));
            $end_time = trim(I('post.end_time'));
            $num = trim(I('post.num'));

            if ($from_name == '') {
                $this->error('渠道名不能为空');
            }

            if ($from_id == '') {
                $this->error("渠道id不能为空");
            }

            if ($start_time == '') {
                $this->error("开始时间不能为空");
            }

            if ($end_time == '') {
                $this->error("结束时间不能为空");
            }

            if ($num == '') {
                $this->error("期数不能为空");
            }

            // $namelength = mb_strlen($username, 'utf-8');
            // $pwdlength = mb_strlen($password, 'utf-8');
            // if ($namelength < 1 || $namelength > 50) {
            //     $this->error('用户名长度不合法');
            // }

            // if ($pwdlength < 6 || $pwdlength > 30) {
            //     $this->error('密码长度不合法');
            // }

            // if ($fromobj->where(array('username' => $username))->find()) {
            //     $this->error('此用户名被占用');
            // }

            //$from_id = intval(D('DChannels')->where(array('parent_id' => 0))->order('id desc,from_id desc')->getField('from_id')) + 1;

            if ($fromobj->where(array('from_id' => $from_id, 'num' => $num))->find()) {
                $this->error('此渠道id与期数已存在');
            }

            $newFrom = D('DChannels')->order('id desc')->find();

            if(isset($newFrom) && $newFrom['end_time'] > strtotime($start_time) && $newFrom['start_time'] > $end_time) {
                $this->error('期数日期段已存在');
            }

            $titleList =  D('ConfigStat')->getConfig();
            $fromkeyStat = D('Task');
            $fromkeyStatData['from_id'] = $from_id;
            $fromkeyStatData['from_time'] = $num; //期数
            // $fromkeyStatData['virtual'] = $num.'_'.$from_id;
            // foreach ($titleList['task'] as $item) {
            //     $fromkeyStatData[$item['task_num']] = 0;
            // }
            // foreach ($titleList['rec'] as $item) {
            //     $fromkeyStatData[$item['task_name'].'/'.$item['task_num']] = 0;
            // }
            $res = $fromkeyStat->add($fromkeyStatData); //添加一条数据

            //$list = $fromkeyStat->select();
            //$password = md5(md5($password));

            $data['from_name'] = $from_name;
            $data['from_id'] = $from_id;
            $data['start_time'] = strtotime($start_time);
            $data['end_time'] = strtotime($end_time) + 86399;
            $data['num'] = $num;
            $data['create_time'] = NOW_TIME;

            //发送给游戏服务端
            $post_url = C('GM_URL') .'cgi-bin/gm_oprate:notice_task_add';
            $sendPost['from_id'] = $from_id;
            $sendPost['from_time'] = $num;
            $sendPost['st_time'] = $data['start_time'];
            $sendPost['ed_time'] = $data['end_time'];
            $field = http_build_query($sendPost);
            $ret = PostUrl($post_url, $field);


            if ($fromobj->add($data)) {

                $this->success('添加成功', U('fromkey'));
            } else {
                echo($fromobj->getLastSql());exit;
                $this->error('添加失败！');
            }
        } else {
            $this->display();
        }
    }

    public function editfKey($id)
    {
        $id = intval($id);
        $map['id'] = $id;
        $keyinfo = D('DChannels')->where($map)->find();

        if (IS_POST) {
            $from_name = trim(I('post.from_name'));
            $from_id = trim(I('post.from_id'));
            $start_time = trim(I('post.start_time'));
            $end_time = trim(I('post.end_time'));
            $num = trim(I('post.num'));

            if ($from_name == '') {
                $this->error('渠道名不能为空');
            }

            if ($from_id == '') {
                $this->error("渠道id不能为空");
            }

            if ($start_time == '') {
                $this->error("开始时间不能为空");
            }

            if ($end_time == '') {
                $this->error("结束时间不能为空");
            }

            if ($num == '') {
                $this->error("期数不能为空");
            }

            $newFrom = D('DChannels')->order('id desc')->find();

            if(isset($newFrom) && $newFrom['end_time'] > strtotime($start_time) && $newFrom['start_time'] > $end_time) {
                $this->error('期数日期段已存在');
            }

            $data['from_name'] = $from_name;
            $data['from_id'] = $from_id;
            $data['start_time'] = strtotime($start_time);
            $data['end_time'] = strtotime($end_time) + 86399;
            $data['from_time'] = $num;

            if (D('DChannels')->where(['id' => $id])->save($data)) {
                $this->success('修改成功', U('fromkey'));
            }
        } else {
            $this->assign('keyinfo', $keyinfo);
            $this->display();
        }
    }

    public function editpwd($id)
    {
        $id = intval($id);
        if ($id == 0) {
            $this->error('参数错误');
        }

        if (IS_POST) {
            $password = trim(I('post.password'));
            $repassword = trim(I('post.repassword'));

            if ($password == '' || $repassword == '') {
                $this->error('密码输入不能为空');
            }

            if ($password != $repassword) {
                $this->error('确认密码输入不一致');
            }

            $password = md5(md5($password));

            $data['id'] = $id;
            $data['password'] = $password;

            if (D('DChannels')->save($data)) {
                $this->success('密码修改成功', U('fromkey'));
            }
        } else {
            $this->display();
        }
    }

    public function delfKey($id)
    {
        $id = intval($id);
        $user = D('DChannels')->where(array('id' => $id))->find();

        // if (intval($user['parent_id']) == 0) {
        //     $this->error('删除失败，只能删除子渠道！');
        // }

        if (D('DChannels')->delete($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
    
    /**
     * 用户金币排行榜
     */
    public function goldranks()
    {
        $room = I('room');
        $rtype = I('rtype');
        $status = I('status');
        /*
        //1 => "卡拉姆沙漠日榜-投注"  排行榜  
         1-3  房间1 -日周月榜
         4-6  房间2 -日周月榜
         7-9  房间3 -日周月榜
         ?room=1&rtype=1

        */
        if (empty($room))
        {
            $room = 0;
        }
        if (empty($status))
        {
            $status = 1;
        }

        $room_list = C('ROOM_LIST');
        $room_name = $room_list[$room];
        if ($room == 1)
        {            
            $ranktype_list = C('RANKTYPE_LIST1');
        }
        else if ($room == 2)
        {
     
            $ranktype_list = C('RANKTYPE_LIST2');
        }
        else if ($room == 3)
        {
       
            $ranktype_list = C('RANKTYPE_LIST3');
        }
        else
        {       
            $ranktype_list = C('RANKTYPE_LIST4');

        }
        $ranks = D("DRanks");
        $users = D("DUsers");

        $ranktypelist = $ranks->get_ranktype();
        $roomday_list = [];
        foreach ($ranktype_list as $k => $val) {   
           
            $redbag_list = []; //1 => "卡拉姆沙漠日榜-投注"
            $mapred['r_type']=$k;
            if(0==$room){if($status==1){$mapred['periods']=0;}else{$mapred['periods']=1;}}else{$mapred['periods']=0;}
            $redbagList = $ranks->where($mapred)
                                ->field('uid,value1')->order("value1 desc")->limit(50)->select();
            
            //var_dump($redbagList);exit;

            foreach ($redbagList as $kk => $vval) {

                $getUser = $users->where(['id'=> $vval['uid']])->field('name,gold')->find();
              
                $redbagList[$kk]['username'] = $getUser['name'];
                $redbagList[$kk]['value1'] = $vval['value1'];
                $redbagList[$kk]['gold'] = $getUser['gold'];
                //print_r($redbagList);
                if(count($redbag_list) < 50 && !empty($redbagList[$kk]['username'])) {
                    $redbag_list[] = $redbagList[$kk];
                }
            }
            //print_r($redbag_list);
            $last_names = array_column($redbag_list,'value1');
            array_multisort($last_names,SORT_DESC,$redbag_list);
            //print_r($redbag_list);
            $roomday_list[$k]=$redbag_list;
            $this->assign("roomday".$k."_list1", $redbag_list); 
        }
        $this->assign('room', $room); 
        $this->assign('status', $status); 
        $this->assign('room_list', $room_list);         
        $this->assign('room_name', $room_name);        
        $this->assign('rtype', $rtype); 
        $this->assign('ranktypelist', $ranktypelist);  
        $this->assign('ranktype_list', $ranktype_list);   
        $this->assign('roomday_list', $roomday_list);   
        $this->display();
    }
    /**
     * 金币统计
     */
    public function goldstat()
    {
        $month = I('month');
        $years = I('years');
        if(empty($month)) {
            $month = date("m");
        }

        if(empty($years)) {
            $years = date('Y');
        }

        for($i = 0; $i < 5; $i++) {
            $years_arr[] = date('Y') - $i;
        }

        if(!empty($month) && !empty($years)) {
            $b_time = I('b_time') ? strtotime(I('b_time')) : strtotime("{$years}-{$month}");
            $get_month = $month == 12 ? 1 :$month + 1;
            $years = $month == 12 ? $years + 1:$years;
            $e_time = I('e_time') ? strtotime(I('e_time')) : strtotime("{$years}-{$get_month}") - 1;
        }


        $show = intval(I('show')); //0:统计，1：产出。2：消耗

        $goldstatday = D('DGoldDayStats');
        //强制当月时间
        $b_time = strtotime(date('Y-m-d 00:00:00', strtotime('-30 days')));
        $e_time = strtotime(date("Y-m-d H:i:s"));

        $where['UNIX_TIMESTAMP(date)'] = array('between', array($b_time, $e_time));
        $limit = 32;
        $page = I('p') ? I('p') : 0;
        $rooms=D('DRooms')->rooms_new_all();
        
        //print_r($where);
        $totals = $goldstatday->where($where)->count();
        $list = $goldstatday->where($where)->order('date desc')->limit($page, $limit)->select();
        foreach ($list as $item) {
            
            foreach($rooms as $rk => $rv)
            {
                //<td>{$vo.room1_win}/{$vo.room1_cost}<br>{:bcdiv($vo[room1_win], $vo[room1_cost], 2)}</td>
                $item['getlist'][$rk] = $item['room'.$rk.'_win'].'/'.$item['room'.$rk.'_cost'].'<br>'.round($item['room'.$rk.'_win']/$item['room'.$rk.'_cost'], 2);
            }
            $lists[$item['date']] = $item;
        }


        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        
		
        $this->assign('month', $month);
        $this->assign('years', $years);
        $this->assign('years_arr', $years_arr);
        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $lists);
        $this->assign('rooms', $rooms);
        $this->display();
    }

    /**
     * 晶石统计
     */
    public function ticketstat()
    {
        $Ticketstat = D('Ticketstat');
        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $show = intval(I('show'));

        $list = $this->_stat_($Ticketstat, $b_time, $e_time, $show, 'ticket');

        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 钥匙统计
     */
    public function diamondstat()
    {
        $Diamondstat = D('Diamodstat');
        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $show = intval(I('show'));

        $list = $this->_stat_($Diamondstat, $b_time, $e_time, $show, 'diamond');

        $this->assign('_list', $list);
        $this->display();
    }

    private function _stat_($obj, $b_time, $e_time, $show, $type = '')
    {

        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = $obj->where($map)->select();

        if ($type == 'diamond') {
            //添加钥匙任务
            $tap['time'] = array('between', array($b_time, $e_time));
            $tap['goods_id'] = 102;
            $tasks = D('Taskstat')->where($tap)->select();
            $diamonds = $redarr = array();
            foreach ($tasks as $v) {
                $diamonds[$v['time']] += $v['gold'];
            }
            //红包场钥匙
            unset($tap['goods_id']);
            $red = D('Qrredstat')->where($tap)->select();
            foreach ($red as $v) {
                $date = strtotime(date('Y-m-d', $v['time']));
                $redarr[$date]['redbag'] += intval($v['redbag']);
                $redarr[$date]['fd_redbag'] += intval($v['fd_redbag']);
            }
        }

        if ($show == 1) {
            $field = 'win'; // 纯赢
        } elseif ($show == 2) {
            $field = 'lose'; // 纯输
        } else {
            $field = 'stat'; // 盈利
        }

        $data = array();
        foreach ($list as $v) {

            $dt = strtotime(date('Y-m-d', $v['c_time']));

            $robot = array(99, 114, 115, 124, 125);

            if ($v['room'] == 10) {
                $data[$dt]['kp'] = $v[$field];
            } elseif ($v['room'] == 11) {
                $data[$dt]['rc'] = $v[$field];
            } elseif ($v['room'] == 20) {
                // 百人牛牛
                $data[$dt]['br'] = $v[$field];
            } elseif ($v['room'] == 21) {
                $data[$dt]['no_use'] = $v['field'];
            } elseif ($v['room'] == 22) {
                $data[$dt]['no_use_1'] = $v['field'];
            } elseif ($v['room'] == 1) {
                $data[$dt]['no_use_2'] = $v['field'];
            } elseif ($v['room'] == 107) {
                // 红包场水果
                $data[$dt]['laba'] = $v[$field];
            } elseif ($v['room'] == 108) {
                $data[$dt]['red'] = $v[$field];
            } elseif ($v['room'] == 112) {
                $data[$dt]['bind'] = $v[$field];
            } elseif ($v['room'] == 113) {
                $data[$dt]['exc'] = $v[$field];
            } elseif ($v['room'] == 104) {
                // 商城购买金币
                $data[$dt]['shop'] = $v[$field];
            } elseif ($v['room'] == 103) {
                $data[$dt]['task'] = $v[$field];
            } elseif ($v['room'] == 102) {
                $data[$dt]['email'] = $v[$field];
            } elseif ($v['room'] == 122) {
                $data[$dt]['oldfh'] = $v[$field];
            } elseif ($v['room'] == 123 || $v['room'] == 30) {
                $data[$dt]['hbjs'] += $v[$field]; //离线房间奖励+红包场结算
            }
            /* elseif($v['room'] == 126){
            $data[$dt]['labatask'] = $v[$field];
            }elseif($v['room'] == 127){
            $data[$dt]['lababx'] = $v[$field];
            }*/
            /*elseif($v['room'] == 128){
            $data[$dt]['brtask'] = $v[$field];
            }elseif($v['room'] == 129){
            $data[$dt]['brbx'] = $v[$field];
            }*/ elseif ($v['room'] == 131) {
                $data[$dt]['reg'] = $v[$field];
            } elseif ($v['room'] == 132) {
                $data[$dt]['fx'] = $v[$field];
            } elseif ($v['room'] == 133) {
                // 豪车统计
                $data[$dt]['car'] = $v[$field];
            } elseif ($v['room'] == 135) {
                $data[$dt]['newfh'] = $v[$field];
            } elseif ($v['room'] == 136) {
                $data[$dt]['djfree'] += $v[$field];
            } elseif ($v['room'] == 142) {
                $data[$dt]['brdjfree'] += $v[$field];
            } elseif ($v['room'] == 145) {
                $data[$dt]['lbdjfree'] += $v[$field];
            } elseif (in_array($v['room'], array(137, 143, 146))) {
                $data[$dt]['djex'] += $v[$field];
            } elseif ($v['room'] == 139) {
                $data[$dt]['7day'] += $v[$field];
            } elseif ($v['room'] == 148) {
                $data[$dt]['ybwl'] += $v[$field];
            } elseif ($v['room'] == 150) {
                $data[$dt]['superlbdjfree'] += $v[$field];
            } elseif ($v['room'] == 152) {
                // 娱乐超级水果=倍率场水果
                $data[$dt]['superlaba'] += $v[$field];
            } elseif ($v['room'] == 1003) {
                $data[$dt]['chairedbag'] = $v[$field];
            } elseif ($v['room'] == 1006) { //红包广场超时
                $data[$dt]['hbchaoshi'] = $v[$field];
            } elseif ($v['room'] == 1011) { //超级拉霸排行
                $data[$dt]['superlabarank'] = $v[$field];
            } elseif ($v['room'] == 1014) { //空战排行
                $data[$dt]['airlabarank'] = $v[$field];
            } elseif ($v['room'] == 1320) { //分享新人奖励
                $data[$dt]['fxnew'] = $v[$field];
            } elseif ($v['room'] == 1321) { //分享好友进阶
                $data[$dt]['fxnext'] = $v[$field];
            } elseif ($v['room'] == 2000) { //第一次领取红包金币
                $data[$dt]['firstgold'] = $v[$field];
            } elseif (in_array($v['room'], $robot)) {
                $data[$dt]['robot'] += $v['field'];
            } elseif ($v['room'] == 'robot') {
                $data[$dt]['no_use_2'] = $v[$field];
            } elseif ($v['room'] == 'redroom') {
                $data[$dt]['redroom'] = $v[$field];
            } elseif ($v['room'] == 'redroomsui') {
                $data[$dt]['redroomsui'] = 0 - $v[$field];
            } elseif ($v['room'] == 174) {
                // 视频
                $data[$dt]['videoreward'] = $v[$field];
            } elseif ($v['room'] == 175) {
                // 随机充值
                $data[$dt]['randomgold'] = $v[$field];
            } elseif ($v['room'] == 176) {
                // 每日工资
                $data[$dt]['dailywages'] = $v[$field];
            } else {
                $data[$dt]['other'] += $v[$field];
                $data[$dt]['other_str'] .= $v['room'] . ',';
            }
            //钥匙
            $data[$dt]['task_dia'] = $diamonds[$dt];
            //红包
            $data[$dt]['redbag'] = $redarr[$dt]['redbag'];
            $data[$dt]['fd_redbag'] = $redarr[$dt]['fd_redbag'];
            if ($field == 'lose') {
                $data[$dt]['task_dia'] = 0;
                $data[$dt]['redbag'] = 0;
                $data[$dt]['fd_redbag'] = 0;
            }
        }
        krsort($data);

        return $data;
    }

    public function goldstatdetail($time, $str)
    {
        $str = trim($str, ',');
        $time = intval($time);
        if (!$str || !$time) {
            $this->error('参数错误');
        }

        $room = explode(',', $str);

        $list = D('Goldstat')->where(array('c_time' => $time))->select();

        $data = array();
        foreach ($list as $v) {
            if (!in_array($v['room'], $room)) {
                continue;
            }

            $data[$v['room']] = $v;
        }
        ksort($data);

        $this->assign('room', $this->path());
        $this->assign('data', $data);
        $this->assign('time', $time);
        $this->display();
    }

    /**
     * 红包广场
     */
    public function redsquare($p = 1)
    {
        $limit = 20;
        $Redmarket = D('Redmarket');

        $b_time = strtotime(I('b_time'));
        $e_time = (strtotime(I('e_time')) + 86399);

        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['stime'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['stime'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['stime'] = array('egt', $b_time);
        }

        $type = intval(I('type'));
        if ($type == 1) {
            $map['sale'] = 1;
        } elseif ($type == 2) {
            $map['sale'] = 0;
        }

        $salbuy = intval(I('salbuy'));
        if ($salbuy == 1) {
            $field = 'bchar_id';
        } else {
            $field = 'schar_id';
        }

        $nametype = I('nametype');
        $value = I('value');
        if (isset($_GET['nametype']) && isset($_GET['value'])) {
            switch ($nametype) {
                case 0: //用户ID
                    $map[$field] = D('Users')->get_charid('uid', intval($value));
                    break;
                case 1: //角色ID
                    $map[$field] = intval($value);
                    break;
                case 2: //手机号
                    $map[$field] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3: { //玩家昵称
                        $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                        $ids = array();
                        foreach ($userinfo as $v) {
                            $ids[] = intval($v['char_id']);
                        }
                        $map[$field] = array('in', $ids);
                        break;
                    }
            }
        }

        $totals = $Redmarket->where($map)->count();
        $list = $Redmarket->where($map)->page($p, $limit)->order('stime desc')->select();

        $ids = array();
        foreach ($list as $k => $v) {
            $ids[] = $v['schar_id'];
            $ids[] = $v['bchar_id'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('chars', D('Userinfo')->getUserInfo($ids, 'char_id'));
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 看牌抢庄大盘
     */
    public function bankergrail($p = 1)
    {
        $limit = 20;
        $Kpgrail = D('Kpgrail');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $Kpgrail->where($map)->count();
        $list = $Kpgrail->where($map)->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli'] = $v['use'] - $v['get'] + $v['sui'];
        }

        $list_data = $Kpgrail->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {
            $data['win'] += $v['get'];
            $data['lose'] += $v['use'];
            $data['yinli'] += ($v['use'] - $v['get'] + $v['sui']);
            $data['sui'] += $v['sui'];
        }
        $data['game'] = $data['yinli'];

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 百人牛牛大盘
     */
    public function brnngrailBk($p = 1)
    {
        if (I('reflesh') == 'yes') {
            echo ("<script>console.log('" . json_encode($p) . "');</script>");
            `/opt/aiya/nginx/fastcgi/bin/php /data/htdocs/RuiquManage/cli.php Brnnstat/stat`;
        }

        $limit = 20;
        $Brnngrail = D('Brnngrail');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $Brnngrail->where($map)->count();
        $list = $Brnngrail->where($map)->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli1'] = $v['use1'] - $v['get1'];
            $list[$k]['yinli2'] = $v['use2'] - $v['get2'];
        }

        $list_data = $Brnngrail->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {

            $data['win1'] += $v['get1'];
            $data['lose1'] += $v['use1'];
            $data['win2'] += $v['get2'];
            $data['lose2'] += $v['use2'];
            $data['yinli1'] += $v['use1'] - $v['get1'] + $v['sui1'];
            $data['yinli2'] += $v['use2'] - $v['get2'] + $v['sui2'];
            $data['sui1'] += $v['sui1'];
            $data['sui2'] += $v['sui2'];
            $data['pooladd1'] += $v['pool1'];
            $data['pooladd2'] += $v['pool2'];
            $data['allred1'] += $v['redget1'] / 10;
            $data['allred2'] += $v['redget2'] / 10;
            $data['poolwin1'] += $v['pool_win1'];
            $data['poolwin2'] += $v['pool_win2'];
            $data['allzhouget'] += $v['zhouget']; //超级水果周榜
            // $data['caichi1'] += $v['pool1'] - $v['pool_win1'];
            // $data['caichi2'] += $v['pool2'] - $v['pool_win2'];
        }
        /*
        //百人水池税收
        $bap['c_time'] = array('between', array($b_time, $e_time));
        $alls = D('Brpool')->where($bap)->select();
        $tax = 0;
        foreach ($alls as $v) {
            $tax += $v['int'];
        }
        $data['yinli'] += $tax;
        $data['sui'] += $tax;
        //未完，彩池税未计算
        // $data['game'] = $data['yinli'] + $data['caichi'];
        $data['game1'] = $data['yinli1'] + $data['caichi1'];
        $data['game2'] = $data['yinli2'] + $data['caichi2'];*/
        $data['ror1'] = sprintf("%.3f", $data['yinli1'] / $data['lose1']) * 100;
        $data['ror2'] = sprintf("%.3f", $data['yinli2'] / $data['lose2']) * 100;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());

        $this->display();
    }

    public function brnngrail($p = 1)
    {
        if (I('reflesh') == 'yes') {
            echo ("<script>console.log('" . json_encode($p) . "');</script>");
            `/opt/aiya/nginx/fastcgi/bin/php /data/htdocs/RuiquManage/cli.php Brnnstat/stat`;
        }

        $limit = 20;
        $Brnngrail = D('Brnngrail');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $Brnngrail->where($map)->count();
        $list = $Brnngrail->where($map)->page($p, $limit)->order('time desc')->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli1'] = intval($v['use1'] - $v['get1']);
            $list[$k]['yinli2'] = intval($v['use2'] - $v['get2']);
        }

        $list_data = $Brnngrail->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {
            $data['win1'] += $v['get1'];
            $data['lose1'] += $v['use1'];
            $data['win2'] += $v['get2'];
            $data['lose2'] += $v['use2'];
            $data['yinli1'] += intval($v['use1'] - $v['get1']);
            $data['yinli2'] += intval($v['use2'] - $v['get2']);
            $data['sui1'] += $v['sui1'] * (5 / 8);
            $data['sui2'] += $v['sui2'] * (5 / 8);
            $data['pooladd1'] += $v['pool1'];
            $data['pooladd2'] += $v['pool2'];
            $data['allred1'] += $v['redget1'] / 10;
            $data['allred2'] += $v['redget2'] / 10;
            $data['poolwin1'] += $v['pool_win1'];
            $data['poolwin2'] += $v['pool_win2'];
        }
        $data['ror1'] = sprintf("%.3f", $data['yinli1'] / $data['lose1']) * 100;
        $data['ror2'] = sprintf("%.3f", $data['yinli2'] / $data['lose2']) * 100;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());

        $this->display();
    }

    /**
     * 水果狂欢大盘
     */
    public function fruitgrailBk($p = 1)
    {
        $limit = 20;
        $show = intval(I('show'));
        $poolsat = $show ? D('Superpoolstat') : D('Poolstat');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $poolsat->where($map)->count();
        $list = $poolsat->where($map)->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli1'] = $v['use1'] - $v['get1'];
            $list[$k]['yinli2'] = $v['use2'] - $v['get2'];
        }

        $list_data = $poolsat->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {

            $data['win1'] += $v['get1'];
            $data['lose1'] += $v['use1'];
            $data['win2'] += $v['get2'];
            $data['lose2'] += $v['use2'];
            $data['yinli1'] += ($v['use1'] - $v['get1'] + $v['sui1']);
            $data['yinli2'] += ($v['use2'] - $v['get2'] + $v['sui2']);

            $data['sui1'] += $v['sui1'];
            $data['sui2'] += $v['sui2'];
            $data['pooladd1'] += $v['pool1'];
            $data['pooladd2'] += $v['pool2'];
            $data['allred1'] += $v['redget1'] / 10;
            $data['allred2'] += $v['redget2'] / 10;
            $data['poolwin1'] += $v['pool_win1'];
            $data['poolwin2'] += $v['pool_win2'];
            $data['allzhouget'] += $v['zhouget']; //日排行奖励
            // $data['caichi1'] += $v['pool1'] - $v['pool_win1'];
            // $data['caichi2'] += $v['pool2'] - $v['pool_win2'];
        }
        $data['game1'] = $data['yinli1'] + $data['caichi1'];
        $data['game2'] = $data['yinli2'] + $data['caichi2'];
        $data['ror1'] = sprintf("%.3f", $data['yinli1'] / $data['lose1']) * 100;
        $data['ror2'] = sprintf("%.3f", $data['yinli2'] / $data['lose2']) * 100;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    public function fruitgrail($p = 1)
    {
        $limit = 20;
        $show = intval(I('show'));
        $poolsat = D('Poolstat');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $poolsat->where($map)->count();
        $list = $poolsat->where($map)->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli1'] = $v['use1'] - $v['get1'];
            $list[$k]['syinli1'] = $v['suse1'] - $v['sget1'];
            $list[$k]['yinli2'] = $v['use2'] - $v['get2'];
            $list[$k]['syinli2'] = $v['suse2'] - $v['sget2'];
        }

        $list_data = $poolsat->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {
            $data['sget1'] += $v['sget1'];
            $data['suse1'] += $v['suse1'];
            $data['sget2'] += $v['sget2'];
            $data['suse2'] += $v['suse2'];
            $data['win1'] += $v['get1'];
            $data['lose1'] += $v['use1'];
            $data['win2'] += $v['get2'];
            $data['lose2'] += $v['use2'];
            $data['yinli1'] += ($v['use1'] - $v['get1']);
            $data['syinli1'] += ($v['suse1'] - $v['sget1']);
            $data['syinli2'] += ($v['suse2'] - $v['sget2']);
            $data['yinli2'] += ($v['use2'] - $v['get2']);
            $data['allzhouget'] += $v['zhouget']; //超级水果周榜
            $data['sui1'] += $v['sui1'];
            $data['sui2'] += $v['sui2'];
            $data['pooladd1'] += $v['pool1'];
            $data['pooladd2'] += $v['pool2'];
            $data['allred1'] += $v['redget1'] / 10;
            $data['allred2'] += $v['redget2'] / 10;
            $data['poolwin1'] += $v['pool_win1'];
            $data['poolwin2'] += $v['pool_win2'];
            // $data['caichi1'] += $v['pool1'] - $v['pool_win1'];
            // $data['caichi2'] += $v['pool2'] - $v['pool_win2'];
        }
        $data['game1'] = $data['yinli1'] + $data['caichi1'];
        $data['game2'] = $data['yinli2'] + $data['caichi2'];
        $data['ror1'] = sprintf("%.3f", $data['yinli1'] / $data['lose1']) * 100;
        $data['sror1'] = sprintf("%.3f", $data['syinli1'] / $data['suse1']) * 100;
        $data['ror2'] = sprintf("%.3f", $data['yinli2'] / $data['lose2']) * 100;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    public function fruitgrail_in($p = 1)
    {
        $this->fruitgrail($p);
    }

    public function fruitsupergrail($p = 1)
    {
        $limit = 20;
        $show = intval(I('show'));
        $poolsat = D('Superpoolstat');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $poolsat->where($map)->count();
        $list = $poolsat->where($map)->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli1'] = $v['use1'] - $v['get1'];
            $list[$k]['yinli2'] = $v['use2'] - $v['get2'];
        }

        $list_data = $poolsat->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {

            $data['win1'] += $v['get1'];
            $data['lose1'] += $v['use1'];
            $data['win2'] += $v['get2'];
            $data['lose2'] += $v['use2'];
            $data['yinli1'] += ($v['use1'] - $v['get1']);
            $data['yinli2'] += ($v['use2'] - $v['get2']);
            $data['allzhouget'] += $v['zhouget']; //超级水果周榜
            $data['sui1'] += $v['sui1'];
            $data['sui2'] += $v['sui2'];
            $data['pooladd1'] += $v['pool1'];
            $data['pooladd2'] += $v['pool2'];
            $data['allred1'] += $v['redget1'] / 10;
            $data['allred2'] += $v['redget2'] / 10;
            $data['poolwin1'] += $v['pool_win1'];
            $data['poolwin2'] += $v['pool_win2'];
            // $data['caichi1'] += $v['pool1'] - $v['pool_win1'];
            // $data['caichi2'] += $v['pool2'] - $v['pool_win2'];
        }
        $data['game1'] = $data['yinli1'] + $data['caichi1'];
        $data['game2'] = $data['yinli2'] + $data['caichi2'];
        $data['ror1'] = sprintf("%.3f", $data['yinli1'] / $data['lose1']) * 100;
        $data['ror2'] = sprintf("%.3f", $data['yinli2'] / $data['lose2']) * 100;

		$data['alldata'] = print_r($data,true);

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    // 机战大盘
    public function airlabagrail($p = 1)
    {
        $limit = 20;
        $poolsat = D('Airlabastat');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $poolsat->where($map)->count();
        $list = $poolsat->where($map)->page($p, $limit)->order('time desc')->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli1'] = $v['use1'] - $v['get1'];
            $list[$k]['syinli1'] = $v['suse1'] - $v['sget1']; // 流水盈利
            $list[$k]['yinli2'] = $v['use2'] - $v['get2'];
            $list[$k]['syinli2'] = $v['suse2'] - $v['sget2']; // 流水盈利
        }

        $list_data = $poolsat->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {
            $data['sget1'] += $v['sget1'];
            $data['suse1'] += $v['suse1'];
            $data['sget2'] += $v['sget2'];
            $data['suse2'] += $v['suse2'];
            $data['win1'] += $v['get1'];
            $data['win2'] += $v['get2'];
            $data['lose1'] += $v['use1'];
            $data['lose2'] += $v['use2'];
            $data['yinli1'] += ($v['use1'] - $v['get1']);
            $data['yinli2'] += ($v['use2'] - $v['get2']);
            $data['syinli1'] += ($v['suse1'] - $v['sget1']);
            $data['syinli2'] += ($v['suse2'] - $v['sget2']);
            $data['poolwin1'] += $v['pool_win1'];
            $data['poolwin2'] += $v['pool_win2'];
            $data['sui1'] += $v['sui1'];
            $data['sui2'] += $v['sui2'];
            $data['pooladd1'] += $v['pooladd1'];
            $data['pooladd2'] += $v['pooladd2'];
            $data['rank1'] += $v['rank1'];
            $data['rank2'] += $v['rank2'];
            $data['allred_1_179'] += $v['allred_1_179'] / 10;
            $data['allred_2_180'] += $v['allred_2_180'] / 10;
            $data['allred179'] += $v['allred179'] / 10;
            $data['allred180'] += $v['allred180'] / 10;
        }

        $data['ror1'] = sprintf("%.3f", $data['yinli1'] / $data['lose1']) * 100;
        $data['ror2'] = sprintf("%.3f", $data['yinli2'] / $data['lose2']) * 100;
        $data['sror1'] = sprintf("%.3f", $data['syinli1'] / $data['suse1']) * 100;
        $data['sror2'] = sprintf("%.3f", $data['syinli2'] / $data['suse2']) * 100;

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    // 超级机战大盘
    public function airlabagrailsuper($p = 1)
    {
        $this->airlabagrail($p);
    }

    private function packageData($list_data, $show) // 处理数据
    {
        $data = array(); // 每天数据
        $data111 = array(); // 月数据
        $data222 = array(); // 月数据
        foreach ($list_data as $v) {
            $dt = strtotime(date('Y-m-d', $v['time']));
            $data[$dt]['time'] = $dt;
            $data[$dt]['win1'] += $v['get1'];
            $data[$dt]['lose1'] += $v['use1'];
            $data[$dt]['suse1'] += $v['suse1'];
            $data[$dt]['sget1'] += $v['sget1'];
            $data[$dt]['win2'] += $v['get2'];
            $data[$dt]['lose2'] += $v['use2'];
            if (intval($show) == 158 || intval($show) == 159) {
                $data[$dt]['yinli1'] += intval($v['use1'] - $v['get1']);
                $data[$dt]['yinli2'] += intval($v['use2'] - $v['get2']);
                $data[$dt]['syinli1'] += intval($v['suse1'] - $v['sget1']); // 流水盈利
                $data[$dt]['sui1'] += $v['sui1'] * (5 / 8);
                $data[$dt]['sui2'] += $v['sui2'] * (5 / 8);
            } else {
                $data[$dt]['yinli1'] += $v['use1'] - $v['get1'];
                $data[$dt]['yinli2'] += $v['use2'] - $v['get2'];
                $data[$dt]['syinli1'] += intval($v['suse1'] - $v['sget1']);
                $data[$dt]['sui1'] += $v['sui1'];
                $data[$dt]['sui2'] += $v['sui2'];
            }

            if ($show == 'air') {
                // air
                $data[$dt]['allred_1_179'] += $v['allred_1_179'];
                $data[$dt]['allred_2_180'] += $v['allred_2_180'];
                $data[$dt]['allred1'] += $v['allred179'];
                $data[$dt]['allred2'] += $v['allred180'];

                $data[$dt]['rank1'] += $v['rank1'];
                $data[$dt]['rank2'] += $v['rank2'];
            } else {
                $data[$dt]['allred1'] += $v['redget1'];
                $data[$dt]['allred2'] += $v['redget2'];
            }

            $data[$dt]['suse2'] += $v['suse2'];
            $data[$dt]['sget2'] += $v['sget2'];
            $data[$dt]['syinli2'] += intval($v['suse2'] - $v['sget2']);

            $data[$dt]['pooladd1'] += $v['pool1'];
            $data[$dt]['pooladd2'] += $v['pool2'];
            $data[$dt]['poolwin1'] += $v['pool_win1'];
            $data[$dt]['poolwin2'] += $v['pool_win2'];
            $data[$dt]['allzhouget'] += $v['zhouget']; //日排行奖励
        }

        foreach ($data as $k => $v) {
            $data111[$k] = $data[$k];
            $data111[$k]['ror1'] = sprintf("%.3f", $data111[$k]['yinli1'] / $data111[$k]['lose1']) * 100;
            $data111[$k]['sror1'] = sprintf("%.3f", $data111[$k]['syinli1'] / $data111[$k]['suse1']) * 100;
            $data111[$k]['ror2'] = sprintf("%.3f", $data111[$k]['yinli2'] / $data111[$k]['lose2']) * 100;
            $data111[$k]['sror2'] = sprintf("%.3f", $data111[$k]['syinli2'] / $data111[$k]['suse2']) * 100;
            $data222['win1'] += $v['win1'];
            $data222['lose1'] += $v['lose1'];
            $data222['suse1'] += $v['suse1'];
            $data222['sget1'] += $v['sget1'];
            $data222['suse2'] += $v['suse2'];
            $data222['sget2'] += $v['sget2'];
            $data222['win2'] += $v['win2'];
            $data222['lose2'] += $v['lose2'];
            $data222['yinli1'] += ($v['yinli1']);
            $data222['syinli1'] += ($v['syinli1']);
            $data222['yinli2'] += ($v['yinli2']);
            $data222['syinli2'] += ($v['syinli2']);
            $data222['sui1'] += $v['sui1'];
            $data222['sui2'] += $v['sui2'];
            $data222['pooladd1'] += $v['pooladd1'];
            $data222['pooladd2'] += $v['pooladd2'];
            $data222['allred1'] += $v['allred1'];
            $data222['allred2'] += $v['allred2'];
            $data222['poolwin1'] += $v['poolwin1'];
            $data222['poolwin2'] += $v['poolwin2'];
            $data222['allzhouget'] += $v['allzhouget']; //日排行奖励

            if ($show == 'air') {
                // air
                $data222['allred_1_179'] += $v['allred_1_179'];
                $data222['allred_2_180'] += $v['allred_2_180'];
                $data222['rank1'] += $v['rank1'];
                $data222['rank2'] += $v['rank2'];
            }
        }
        $data222['ror1'] = sprintf("%.3f", $data222['yinli1'] / $data222['lose1']) * 100;
        $data222['sror1'] = sprintf("%.3f", $data222['syinli1'] / $data222['suse1']) * 100;
        $data222['ror2'] = sprintf("%.3f", $data222['yinli2'] / $data222['lose2']) * 100;
        $data222['sror2'] = sprintf("%.3f", $data222['syinli2'] / $data222['suse2']) * 100;

        return array(
            'once' => $data111,
            'all' => $data222
        );
    }

    // 日统计 (使用)
    public function fruitgrailror()
    {
        $show = intval(I('show'));
        $poolsat = "";
        $sror1 = false;
        if (!!$show) { // 房间编号
            switch ($show) {
                case 120:
                    // $vStat['roomName'] = "红包场";
                    break;
                case 154:
                case 155:
                    $poolsat = D('Poolstat'); // fruitgrail(
                    $sror1 = true;
                    break;
                case 156:
                case 157:
                    $poolsat = D('Superpoolstat'); // fruitgrail(
                    break;
                case 158:
                case 159:
                    $poolsat = D('Brnngrail'); // brnngrail(
                    break;
                case 160:
                case 161:
                    $poolsat = D('Cargrail'); // cargrail(
                    break;
                default:
                    break;
            }
        }

        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'] + 86399;
        $map['time'] = array('between', array($b_time, $e_time));
        if (!!$poolsat) {

            $list_data = $poolsat->where($map)->order('time desc')->select();
            $res = $this->packageData($list_data, $show);
            $oncedata = $res['once']; // 日数据
            $alldata = $res['all']; // 月数据
            $pageNav = new \Think\Page(count($oncedata), 20);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

            if ($sror1) {
                foreach ($oncedata as $k => $v) {
                    // 红包场流水数据
                    $oncedata[$k]['lose1'] = $v['suse1'];
                    $oncedata[$k]['win1'] = $v['sget1'];
                    $oncedata[$k]['yinli1'] = $v['syinli1'];
                    $oncedata[$k]['ror1'] = $v['sror1'];
                }

                $alldata['lose1'] = $alldata['suse1'];
                $alldata['win1'] = $alldata['sget1'];
                $alldata['yinli1'] = $alldata['syinli1'];
                $alldata['ror1'] = $alldata['sror1'];
            }

            $this->assign('_list', $oncedata);
            $this->assign('data', $alldata);
            $this->assign('_page', $pageNav->show());
            $this->display();
        } else {
            // 所有的

            $list_fruit = D('Poolstat')->where($map)->order('time desc')->select();
            $res1 = $this->packageData($list_fruit, 158); // 试玩普通水果

            $list_fruitsuper = D('Superpoolstat')->where($map)->order('time desc')->select();
            $res2 = $this->packageData($list_fruitsuper, 0); // 娱乐超级水果

            $list_brnn = D('Brnngrail')->where($map)->order('time desc')->select();
            $res3 = $this->packageData($list_brnn, 0); // 娱乐牛牛

            $list_car = D('Cargrail')->where($map)->order('time desc')->select();
            $res4 = $this->packageData($list_car, 0); // 娱乐豪车

            $b_time_int = intval(date('Ymd', $b_time));
            $e_time_int = intval(date('Ymd', $e_time));

            $oncedata = array();
            for ($x = $b_time_int; $x <= $e_time_int; $x++) {
                $tt = strtotime(intval($x . "000000"));
                $oncedata[$tt]['time'] = $tt;
                // $oncedata[$tt]['win1'] = $res1['once'][$tt]['win1'] + $res2['once'][$tt]['win1'] + $res3['once'][$tt]['win1'] + $res4['once'][$tt]['win1'];
                // $oncedata[$tt]['lose1'] = $res1['once'][$tt]['lose1'] + $res2['once'][$tt]['lose1'] + $res3['once'][$tt]['lose1'] + $res4['once'][$tt]['lose1'];
                $oncedata[$tt]['win1'] = $res1['once'][$tt]['sget1'] + $res2['once'][$tt]['win1'] + $res3['once'][$tt]['win1'] + $res4['once'][$tt]['win1'];
                $oncedata[$tt]['lose1'] = $res1['once'][$tt]['suse1'] + $res2['once'][$tt]['lose1'] + $res3['once'][$tt]['lose1'] + $res4['once'][$tt]['lose1'];
                $oncedata[$tt]['win2'] = $res1['once'][$tt]['win2'] + $res2['once'][$tt]['win2'] + $res3['once'][$tt]['win2'] + $res4['once'][$tt]['win2'];
                $oncedata[$tt]['lose2'] = $res1['once'][$tt]['lose2'] + $res2['once'][$tt]['lose2'] + $res3['once'][$tt]['lose2'] + $res4['once'][$tt]['lose2'];
                // $oncedata[$tt]['yinli1'] = $res1['once'][$tt]['yinli1'] + $res2['once'][$tt]['yinli1'] + $res3['once'][$tt]['yinli1'] + $res4['once'][$tt]['yinli1'];
                $oncedata[$tt]['yinli1'] = $res1['once'][$tt]['syinli1'] + $res2['once'][$tt]['yinli1'] + $res3['once'][$tt]['yinli1'] + $res4['once'][$tt]['yinli1'];
                $oncedata[$tt]['yinli2'] = $res1['once'][$tt]['yinli2'] + $res2['once'][$tt]['yinli2'] + $res3['once'][$tt]['yinli2'] + $res4['once'][$tt]['yinli2'];
                $oncedata[$tt]['sui1'] = $res1['once'][$tt]['sui1'] + $res2['once'][$tt]['sui1'] + $res3['once'][$tt]['sui1'] + $res4['once'][$tt]['sui1'];
                $oncedata[$tt]['sui2'] = $res1['once'][$tt]['sui2'] + $res2['once'][$tt]['sui2'] + $res3['once'][$tt]['sui2'] + $res4['once'][$tt]['sui2'];
                $oncedata[$tt]['pooladd1'] = $$res1['once'][$tt]['pooladd1'] + $res2['once'][$tt]['pooladd1'] + $res3['once'][$tt]['pooladd1'] + $res4['once'][$tt]['pooladd1'];
                $oncedata[$tt]['pooladd2'] = $res1['once'][$tt]['pooladd2'] + $res2['once'][$tt]['pooladd2'] + $res3['once'][$tt]['pooladd2'] + $res4['once'][$tt]['pooladd2'];
                $oncedata[$tt]['allred1'] = $res1['once'][$tt]['allred1'] + $res2['once'][$tt]['allred1'] + $res3['once'][$tt]['allred1'] + $res4['once'][$tt]['allred1'];
                $oncedata[$tt]['allred2'] = $res1['once'][$tt]['allred2'] + $res2['once'][$tt]['allred2'] + $res3['once'][$tt]['allred2'] + $res4['once'][$tt]['allred2'];
                $oncedata[$tt]['poolwin1'] = $res1['once'][$tt]['poolwin1'] + $res2['once'][$tt]['poolwin1'] + $res3['once'][$tt]['poolwin1'] + $res4['once'][$tt]['poolwin1'];
                $oncedata[$tt]['poolwin2'] = $res1['once'][$tt]['poolwin2'] + $res2['once'][$tt]['poolwin2'] + $res3['once'][$tt]['poolwin2'] + $res4['once'][$tt]['poolwin2'];
                $oncedata[$tt]['allzhouget'] = $res1['once'][$tt]['allzhouget'] + $res2['once'][$tt]['allzhouget'] + $res3['once'][$tt]['allzhouget'] + $res4['once'][$tt]['allzhouget']; //日排行奖励

                $oncedata[$tt]['ror1'] = sprintf("%.3f", $oncedata[$tt]['yinli1'] / $oncedata[$tt]['lose1']) * 100;
                $oncedata[$tt]['ror2'] = sprintf("%.3f", $oncedata[$tt]['yinli2'] / $oncedata[$tt]['lose2']) * 100;

                if ($oncedata[$tt]['win1'] == 0 && $oncedata[$tt]['win2'] == 0 && $oncedata[$tt]['lose1'] == 0 && $oncedata[$tt]['lose2'] == 0) {
                    // 删除无用数据
                    unset($oncedata[$tt]);
                } else {
                    // echo "======". json_encode($oncedata[$tt]);
                }
            }

            $total = array();
            // $total['win1'] = $res1['all']['win1'] + $res2['all']['win1'] + $res3['all']['win1'] + $res4['all']['win1'];
            // $total['lose1'] = $res1['all']['lose1'] + $res2['all']['lose1'] + $res3['all']['lose1'] + $res4['all']['lose1'];
            $total['win1'] = $res1['all']['sget1'] + $res2['all']['win1'] + $res3['all']['win1'] + $res4['all']['win1'];
            $total['lose1'] = $res1['all']['suse1'] + $res2['all']['lose1'] + $res3['all']['lose1'] + $res4['all']['lose1'];
            $total['win2'] = $res1['all']['win2'] + $res2['all']['win2'] + $res3['all']['win2'] + $res4['all']['win2'];
            $total['lose2'] = $res1['all']['lose2'] + $res2['all']['lose2'] + $res3['all']['lose2'] + $res4['all']['lose2'];
            // $total['yinli1'] = $res1['all']['yinli1'] + $res2['all']['yinli1'] + $res3['all']['yinli1'] + $res4['all']['yinli1'];
            $total['yinli1'] = $res1['all']['syinli1'] + $res2['all']['yinli1'] + $res3['all']['yinli1'] + $res4['all']['yinli1'];
            $total['yinli2'] = $res1['all']['yinli2'] + $res2['all']['yinli2'] + $res3['all']['yinli2'] + $res4['all']['yinli2'];
            $total['sui1'] = $res1['all']['sui1'] + $res2['all']['sui1'] + $res3['all']['sui1'] + $res4['all']['sui1'];
            $total['sui2'] = $res1['all']['sui2'] + $res2['all']['sui2'] + $res3['all']['sui2'] + $res4['all']['sui2'];
            $total['pooladd1'] = $res1['all']['pooladd1'] + $res2['all']['pooladd1'] + $res3['all']['pooladd1'] + $res4['all']['pooladd1'];
            $total['pooladd2'] = $res1['all']['pooladd2'] + $res2['all']['pooladd2'] + $res3['all']['pooladd2'] + $res4['all']['pooladd2'];
            $total['allred1'] = ($res1['all']['allred1'] + $res2['all']['allred1'] + $res3['all']['allred1'] + $res4['all']['allred1']) / 10;
            $total['allred2'] = ($res1['all']['allred2'] + $res2['all']['allred2'] + $res3['all']['allred2'] + $res4['all']['allred2']) / 10;
            $total['poolwin1'] = $res1['all']['poolwin1'] + $res2['all']['poolwin1'] + $res3['all']['poolwin1'] + $res4['all']['poolwin1'];
            $total['poolwin2'] = $res1['all']['poolwin2'] + $res2['all']['poolwin2'] + $res3['all']['poolwin2'] + $res4['all']['poolwin2'];
            $total['allzhouget'] = $res1['all']['allzhouget'] + $res2['all']['allzhouget'] + $res3['all']['allzhouget'] + $res4['all']['allzhouget']; //日排行奖励

            $total['ror1'] = sprintf("%.3f", $total['yinli1'] / $total['lose1']) * 100;
            $total['ror2'] = sprintf("%.3f", $total['yinli2'] / $total['lose2']) * 100;

            $pageNav = new \Think\Page(count($oncedata), 20);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
            $this->assign('_list', $oncedata);
            $this->assign('data', $total);
            $this->assign('_page', $pageNav->show());
            $this->display();
        }
    }

    // 每日大盘 (暂未使用)
    public function everydaygrailror()
    {
        $show = intval(I('show'));
        $poolsat = "";
        $sror1 = false; // 是否需要流水数据
        if (!!$show) { // 房间编号
            switch ($show) {
                case 120:
                    // $vStat['roomName'] = "红包场";
                    break;
                case 154:
                case 155:
                    $poolsat = D('Poolstat'); // fruitgrail(
                    $sror1 = true;
                    break;
                case 156:
                case 157:
                    $poolsat = D('Superpoolstat'); // fruitgrail(
                    break;
                case 158:
                case 159:
                    $poolsat = D('Brnngrail'); // brnngrail(
                    break;
                case 160:
                case 161:
                    $poolsat = D('Cargrail'); // cargrail(
                    break;
                case 177:
                    $poolsat = D('Airlabastat'); // Airlabastat(
                    $sror1 = true;
                    break;
                default:
                    break;
            }
        }

        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'] + 86399;
        $map['time'] = array('between', array($b_time, $e_time));
        if (!!$poolsat) {
            $list_data = $poolsat->where($map)->order('time desc')->select();
            $res = $this->packageData($list_data, $show);
            $oncedata = $res['once']; // 日数据
            $alldata = $res['all']; // 月数据
            $pageNav = new \Think\Page(count($oncedata), 20);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

            if ($sror1) {
                foreach ($oncedata as $k => $v) {
                    // 红包场流水数据
                    $oncedata[$k]['lose1'] = $v['suse1'];
                    $oncedata[$k]['win1'] = $v['sget1'];
                    $oncedata[$k]['yinli1'] = $v['syinli1'];
                    $oncedata[$k]['ror1'] = $v['sror1'];
                }

                $alldata['lose1'] = $alldata['suse1'];
                $alldata['win1'] = $alldata['sget1'];
                $alldata['yinli1'] = $alldata['syinli1'];
                $alldata['ror1'] = $alldata['sror1'];
            }

            $this->assign('_list', $oncedata);
            $this->assign('data', $alldata);
            $this->assign('_page', $pageNav->show());
            $this->display();
        } else {
            // 所有的

            $list_fruit = D('Poolstat')->where($map)->order('time desc')->select();
            $res1 = $this->packageData($list_fruit, 158); // 试玩普通水果

            $list_fruitsuper = D('Superpoolstat')->where($map)->order('time desc')->select();
            $res2 = $this->packageData($list_fruitsuper, 0); // 娱乐超级水果

            $list_brnn = D('Brnngrail')->where($map)->order('time desc')->select();
            $res3 = $this->packageData($list_brnn, 0); // 娱乐牛牛

            $list_car = D('Cargrail')->where($map)->order('time desc')->select();
            $res4 = $this->packageData($list_car, 0); // 娱乐豪车

            $list_air = D('Airlabastat')->where($map)->order('time desc')->select();
            $res5 = $this->packageData($list_air, 0); // 机战

            $b_time_int = intval(date('Ymd', $b_time));
            $e_time_int = intval(date('Ymd', $e_time));

            $oncedata = array();
            for ($x = $b_time_int; $x <= $e_time_int; $x++) {
                $tt = strtotime(intval($x . "000000"));
                $oncedata[$tt]['time'] = $tt;
                // $oncedata[$tt]['win1'] = $res1['once'][$tt]['win1'] + $res2['once'][$tt]['win1'] + $res3['once'][$tt]['win1'] + $res4['once'][$tt]['win1'];
                // $oncedata[$tt]['lose1'] = $res1['once'][$tt]['lose1'] + $res2['once'][$tt]['lose1'] + $res3['once'][$tt]['lose1'] + $res4['once'][$tt]['lose1'];
                $oncedata[$tt]['win1'] = $res1['once'][$tt]['sget1'] + $res2['once'][$tt]['win1'] + $res3['once'][$tt]['win1'] + $res4['once'][$tt]['win1'] + $res5['once'][$tt]['sget1'];
                $oncedata[$tt]['lose1'] = $res1['once'][$tt]['suse1'] + $res2['once'][$tt]['lose1'] + $res3['once'][$tt]['lose1'] + $res4['once'][$tt]['lose1'] + $res5['once'][$tt]['suse1'];
                $oncedata[$tt]['win2'] = $res1['once'][$tt]['win2'] + $res2['once'][$tt]['win2'] + $res3['once'][$tt]['win2'] + $res4['once'][$tt]['win2'] + $res5['once'][$tt]['win2'];
                $oncedata[$tt]['lose2'] = $res1['once'][$tt]['lose2'] + $res2['once'][$tt]['lose2'] + $res3['once'][$tt]['lose2'] + $res4['once'][$tt]['lose2'] + $res5['once'][$tt]['lose2'];
                // $oncedata[$tt]['yinli1'] = $res1['once'][$tt]['yinli1'] + $res2['once'][$tt]['yinli1'] + $res3['once'][$tt]['yinli1'] + $res4['once'][$tt]['yinli1'];
                $oncedata[$tt]['yinli1'] = $res1['once'][$tt]['syinli1'] + $res2['once'][$tt]['yinli1'] + $res3['once'][$tt]['yinli1'] + $res4['once'][$tt]['yinli1'] + $res5['once'][$tt]['syinli1'];
                $oncedata[$tt]['yinli2'] = $res1['once'][$tt]['yinli2'] + $res2['once'][$tt]['yinli2'] + $res3['once'][$tt]['yinli2'] + $res4['once'][$tt]['yinli2'] + $res5['once'][$tt]['yinli2'];
                $oncedata[$tt]['sui1'] = $res1['once'][$tt]['sui1'] + $res2['once'][$tt]['sui1'] + $res3['once'][$tt]['sui1'] + $res4['once'][$tt]['sui1'] + $res5['once'][$tt]['sui1'];
                $oncedata[$tt]['sui2'] = $res1['once'][$tt]['sui2'] + $res2['once'][$tt]['sui2'] + $res3['once'][$tt]['sui2'] + $res4['once'][$tt]['sui2'] + $res5['once'][$tt]['sui2'];
                $oncedata[$tt]['pooladd1'] = $res1['once'][$tt]['pooladd1'] + $res2['once'][$tt]['pooladd1'] + $res3['once'][$tt]['pooladd1'] + $res4['once'][$tt]['pooladd1'] + $res5['once'][$tt]['pooladd1'];
                $oncedata[$tt]['pooladd2'] = $res1['once'][$tt]['pooladd2'] + $res2['once'][$tt]['pooladd2'] + $res3['once'][$tt]['pooladd2'] + $res4['once'][$tt]['pooladd2'] + $res5['once'][$tt]['pooladd2'];
                $oncedata[$tt]['allred1'] = $res1['once'][$tt]['allred1'] + $res2['once'][$tt]['allred1'] + $res3['once'][$tt]['allred1'] + $res4['once'][$tt]['allred1'] + $res5['once'][$tt]['allred1'];
                $oncedata[$tt]['allred2'] = $res1['once'][$tt]['allred2'] + $res2['once'][$tt]['allred2'] + $res3['once'][$tt]['allred2'] + $res4['once'][$tt]['allred2'] + $res5['once'][$tt]['allred2'];
                $oncedata[$tt]['poolwin1'] = $res1['once'][$tt]['poolwin1'] + $res2['once'][$tt]['poolwin1'] + $res3['once'][$tt]['poolwin1'] + $res4['once'][$tt]['poolwin1'] + $res5['once'][$tt]['poolwin1'];
                $oncedata[$tt]['poolwin2'] = $res1['once'][$tt]['poolwin2'] + $res2['once'][$tt]['poolwin2'] + $res3['once'][$tt]['poolwin2'] + $res4['once'][$tt]['poolwin2'] + $res5['once'][$tt]['poolwin2'];
                $oncedata[$tt]['allzhouget'] = $res1['once'][$tt]['allzhouget'] + $res2['once'][$tt]['allzhouget'] + $res3['once'][$tt]['allzhouget'] + $res4['once'][$tt]['allzhouget']; //日排行奖励

                $oncedata[$tt]['ror1'] = sprintf("%.3f", $oncedata[$tt]['yinli1'] / $oncedata[$tt]['lose1']) * 100;
                $oncedata[$tt]['ror2'] = sprintf("%.3f", $oncedata[$tt]['yinli2'] / $oncedata[$tt]['lose2']) * 100;

                if ($oncedata[$tt]['win1'] == 0 && $oncedata[$tt]['win2'] == 0 && $oncedata[$tt]['lose1'] == 0 && $oncedata[$tt]['lose2'] == 0) {
                    // 删除无用数据
                    unset($oncedata[$tt]);
                } else {
                    // echo "======". json_encode($oncedata[$tt]);
                }
            }

            $total = array();
            // $total['win1'] = $res1['all']['win1'] + $res2['all']['win1'] + $res3['all']['win1'] + $res4['all']['win1'];
            // $total['lose1'] = $res1['all']['lose1'] + $res2['all']['lose1'] + $res3['all']['lose1'] + $res4['all']['lose1'];
            $total['win1'] = $res1['all']['sget1'] + $res2['all']['win1'] + $res3['all']['win1'] + $res4['all']['win1'] + $res5['all']['sget1'];
            $total['lose1'] = $res1['all']['suse1'] + $res2['all']['lose1'] + $res3['all']['lose1'] + $res4['all']['lose1'] + $res5['all']['suse1'];
            $total['win2'] = $res1['all']['win2'] + $res2['all']['win2'] + $res3['all']['win2'] + $res4['all']['win2'] + $res5['all']['win2'];
            $total['lose2'] = $res1['all']['lose2'] + $res2['all']['lose2'] + $res3['all']['lose2'] + $res4['all']['lose2'] + $res5['all']['lose2'];
            // $total['yinli1'] = $res1['all']['yinli1'] + $res2['all']['yinli1'] + $res3['all']['yinli1'] + $res4['all']['yinli1'];
            $total['yinli1'] = $res1['all']['syinli1'] + $res2['all']['yinli1'] + $res3['all']['yinli1'] + $res4['all']['yinli1'] + $res5['all']['syinli1'];
            $total['yinli2'] = $res1['all']['yinli2'] + $res2['all']['yinli2'] + $res3['all']['yinli2'] + $res4['all']['yinli2'] + $res5['all']['yinli2'];
            $total['sui1'] = $res1['all']['sui1'] + $res2['all']['sui1'] + $res3['all']['sui1'] + $res4['all']['sui1'] + $res5['all']['sui1'];
            $total['sui2'] = $res1['all']['sui2'] + $res2['all']['sui2'] + $res3['all']['sui2'] + $res4['all']['sui2'] + $res5['all']['sui2'];
            $total['pooladd1'] = $res1['all']['pooladd1'] + $res2['all']['pooladd1'] + $res3['all']['pooladd1'] + $res4['all']['pooladd1'] + $res5['all']['pooladd1'];
            $total['pooladd2'] = $res1['all']['pooladd2'] + $res2['all']['pooladd2'] + $res3['all']['pooladd2'] + $res4['all']['pooladd2'] + $res5['all']['pooladd2'];
            $total['allred1'] = ($res1['all']['allred1'] + $res2['all']['allred1'] + $res3['all']['allred1'] + $res4['all']['allred1'] + $res5['all']['allred1']) / 10;
            $total['allred2'] = ($res1['all']['allred2'] + $res2['all']['allred2'] + $res3['all']['allred2'] + $res4['all']['allred2'] + $res5['all']['allred2']) / 10;
            $total['poolwin1'] = $res1['all']['poolwin1'] + $res2['all']['poolwin1'] + $res3['all']['poolwin1'] + $res4['all']['poolwin1'] + $res5['all']['poolwin1'];
            $total['poolwin2'] = $res1['all']['poolwin2'] + $res2['all']['poolwin2'] + $res3['all']['poolwin2'] + $res4['all']['poolwin2'] + $res5['all']['poolwin2'];
            $total['allzhouget'] = $res1['all']['allzhouget'] + $res2['all']['allzhouget'] + $res3['all']['allzhouget'] + $res4['all']['allzhouget']; //日排行奖励

            $total['ror1'] = sprintf("%.3f", $total['yinli1'] / $total['lose1']) * 100;
            $total['ror2'] = sprintf("%.3f", $total['yinli2'] / $total['lose2']) * 100;

            $pageNav = new \Think\Page(count($oncedata), 20);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
            $this->assign('_list', $oncedata);
            $this->assign('data', $total);
            $this->assign('_page', $pageNav->show());
            $this->display();
        }
    }

    // 红包月统计 (使用)
    public function redbagstatistics()
    {
        $show = intval(I('show'));
        $b_time = I('b_time');
        if (!!$b_time) {
            $b_time = str_replace("-", "", $b_time);
        }

        $e_time = I('e_time');
        if (!!$e_time) {
            $e_time = str_replace("-", "", $e_time);
        }

        $where = array();
        $resData = array();
        if (!!$show) {
            // 房间编号
            $where['room'] = $show;
            $resData['show'] = $show;
        } else {
            $where['room'] = array('in', array(
                103,
                164,
                165,
                104,
                153,
                113,
                178,
                179,
                180,
                181,
                182
            ));
            // 任务奖励103， 红包场水果164，红包场【站内】水果165，每日抽奖178）,
            // 试玩机战领取红包179，娱乐机战领取红包180, 商城104,
            // 红包兑换成金币153（负数），红包兑换成人民币113（负数）
            // ['181'] = '试玩机战[击落]红包'; ['182'] = '娱乐机战[击落]红包';
        }

        $needTime = date('Ym', NOW_TIME);
        $minTime = intval($needTime . "01");
        $maxTime = date('Ymd', NOW_TIME);

        if (!!$b_time && $b_time < $minTime) {
            $b_time = $minTime;
        }

        if (!!$e_time && $e_time > $maxTime) {
            $e_time = $maxTime;
        }

        if (!!$b_time && !$e_time) {
            $where['c_time'] = array('egt', intval($b_time));
        } else if (!$b_time && !!$e_time) {
            $where['c_time'] = array('between', array(intval($minTime), intval($e_time)));
        } else if (!!$b_time && !!$e_time) {
            $where['c_time'] = array('between', array(intval($b_time), intval($e_time)));
        }

        $getListStat = D('Redbagstatistics')->where($where)->order('c_time desc')->select();
        $totalMoney = 0;
        $totalMoneyToRMB = 0;
        $totalMoneyToGold = 0;
        $totalMoney = 0;
        $list = array();
        if (!$show) {
            // 显示所有房间
            foreach ($getListStat as $vStat) {
                if ($vStat['money'] > 0) {
                    $totalMoney += $vStat['money'];
                }

                $c_time = strtotime($vStat['c_time'] * 1000000);
                switch ($vStat['room']) {
                    case 113:
                        // 红包兑换成人民币
                        $list[$c_time]['c_time'] = $c_time;
                        if ($vStat['money'] < 0) {
                            $totalMoneyToRMB += $vStat['money'];
                            $list[$c_time]['moneytormb'] += $vStat['money'] / 10;
                        }
                        break;
                    case 153:
                        // 红包兑换成金币
                        $list[$c_time]['c_time'] = $c_time;
                        if ($vStat['money'] < 0) {
                            $totalMoneyToGold += $vStat['money'];
                            $list[$c_time]['moneytogold'] += $vStat['money'] / 10;
                        }
                        break;
                    case 103:
                    case 104:
                    case 164:
                    case 165:
                    case 178:
                    case 179:
                    case 180:
                    case 181:
                    case 182:
                        $list[$c_time]['c_time'] = $c_time;
                        if ($vStat['money'] >= 0) {
                            $list[$c_time]['money'] += $vStat['money'] / 10;
                        }
                        break;
                }
            }
        } else {
            // 显示单个房间
            foreach ($getListStat as $vStat) {
                if ($vStat['money'] > 0) {
                    $totalMoney += $vStat['money'];
                }

                $vStat['c_time'] = strtotime($vStat['c_time'] * 1000000);
                if ($vStat['money'] >= 0) {
                    $vStat['money'] = $vStat['money'] / 10;
                }

                if (!!C('ROOM_NAME')[$vStat['room']]) {
                    // 根据配置表显示红包数据
                    $vStat['roomName'] = C('ROOM_NAME')[$vStat['room']];
                } else {
                    $vStat['roomName'] = $vStat['room'] . " roomName need adding to config.php";
                }

                $list[$vStat['_id']] = $vStat;
            }
        }

        $resData['totalMoney'] = $totalMoney / 10;
        $resData['totalMoneyToRMB'] = $totalMoneyToRMB / 10;
        $resData['totalMoneyToGold'] = $totalMoneyToGold / 10;

        $pageNav = new \Think\Page(count($list), 20);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $this->assign('_list', $list);
        $this->assign('data', $resData);
        $this->assign('_page', $pageNav->show());
        $this->assign('room', $show ? $show : 0);
        $this->display();
    }

    /**
     * 豪车大盘
     */
    public function cargrailBk($p = 1)
    {
        $limit = 20;
        $Cargrail = D('Cargrail');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $Cargrail->where($map)->count();
        $list = $Cargrail->where($map)->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli'] = $v['use'] - $v['get'];
        }

        $list_data = $Cargrail->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {

            $data['win1'] += $v['get1'];
            $data['lose1'] += $v['use1'];
            $data['win2'] += $v['get2'];
            $data['lose2'] += $v['use2'];
            $data['yinli1'] += $v['use1'] - $v['get1'] + $v['sui1'];
            $data['yinli2'] += $v['use2'] - $v['get2'] + $v['sui2'];
            $data['sui1'] += $v['sui1'];
            $data['sui2'] += $v['sui2'];
            $data['pooladd1'] += $v['pooladd1'];
            $data['pooladd2'] += $v['pooladd2'];
            $data['allred1'] += $v['redget1'] / 10;
            $data['allred2'] += $v['redget2'] / 10;
            $data['poolwin1'] += $v['pool_win1'];
            $data['poolwin2'] += $v['pool_win2'];
            $data['allzhouget'] += $v['zhouget']; //日排行奖励
        }
        //$data['swin'] = $data['swin'] - $data['tax'] - $data['pooladd'] + $data['pool'];
        //$data['win'] = $data['win'] - $data['tax'] - $data['pooladd'] + $data['pool'];
        //$data['yinli'] = $data['lose'] - $data['win']; //$data['yinli'] + $data['tax'];
        //$data['sysRealWin'] = $data['lose'] - ($data['win'] - $data['tax']) - $data['pool'];
        $data['ror1'] = sprintf("%.3f", $data['yinli1'] / $data['lose1']) * 100;
        $data['ror2'] = sprintf("%.3f", $data['yinli2'] / $data['lose2']) * 100;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    public function cargrail($p = 1)
    {
        $limit = 20;
        $Cargrail = D('Cargrail');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $Cargrail->where($map)->count();
        $list = $Cargrail->where($map)->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli1'] = $v['use1'] - $v['get1'];
            $list[$k]['yinli2'] = $v['use2'] - $v['get2'];
        }

        $list_data = $Cargrail->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {

            $data['win1'] += $v['get1'];
            $data['lose1'] += $v['use1'];
            $data['win2'] += $v['get2'];
            $data['lose2'] += $v['use2'];
            $data['yinli1'] += ($v['use1'] - $v['get1']);
            $data['yinli2'] += ($v['use2'] - $v['get2']);
            $data['sui1'] += $v['sui1'];
            $data['sui2'] += $v['sui2'];
            $data['pooladd1'] += $v['pooladd1'];
            $data['pooladd2'] += $v['pooladd2'];
            $data['allred1'] += $v['redget1'] / 10;
            $data['allred2'] += $v['redget2'] / 10;
            $data['poolwin1'] += $v['pool_win1'];
            $data['poolwin2'] += $v['pool_win2'];
            $data['allzhouget'] += $v['zhouget']; //日排行奖励
        }
        //$data['swin'] = $data['swin'] - $data['tax'] - $data['pooladd'] + $data['pool'];
        //$data['win'] = $data['win'] - $data['tax'] - $data['pooladd'] + $data['pool'];
        //$data['yinli'] = $data['lose'] - $data['win']; //$data['yinli'] + $data['tax'];
        //$data['sysRealWin'] = $data['lose'] - ($data['win'] - $data['tax']) - $data['pool'];
        $data['ror1'] = sprintf("%.3f", $data['yinli1'] / $data['lose1']) * 100;
        $data['ror2'] = sprintf("%.3f", $data['yinli2'] / $data['lose2']) * 100;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 红包场大盘
     */
    public function redroomgrail($p = 1)
    {
        $limit = 20;
        $Qrredstat = D('Qrredstat');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['time'] = array('between', array($b_time, $e_time));
        $totals = $Qrredstat->where($map)->count();
        $list = $Qrredstat->where($map)->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['yinli'] = $v['use'] - $v['get'] + $v['sui'];
        }

        $list_data = $Qrredstat->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {
            $data['win'] += $v['get'];
            $data['lose'] += $v['use'];
            $data['yinli'] += ($v['use'] - $v['get'] + $v['sui']);
            $data['sui'] += $v['sui'];
            $data['condia'] += $v['condia'];
            $data['redbag'] += $v['redbag'];
            $data['fd_redbag'] += $v['fd_redbag'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    protected function dailyConfig($nper = 0)
    {
        $config = array(
            1 => array(
                'from_id' => array(1000000, 1999999),
                'func_name' => 'calcu_forth_money_1',
                'nper' => 1,
                'display' => 'dailywingoldfirst',
            ),
            2 => array(
                'from_id' => array(2000000, 2999999),
                'func_name' => 'calcu_forth_money_2',
                'nper' => 2,
                'display' => 'dailywingoldtwo',
            ),
            3 => array(
                'from_id' => array(3000000, 3999999),
                'func_name' => 'calcu_forth_money_3',
                'nper' => 3,
                'display' => 'dailywingoldthree',
            ),
            4 => array(
                'from_id' => array(4000000, 4999999),
                'func_name' => 'calcu_forth_money_4',
                'nper' => 4,
                'display' => 'dailywingoldfour',
            ),
        );
        $return = $nper ? $config[$nper] : end($config);
        $return = $return ? $return : end($config);
        $return[0] = range(1, (1 + count($config) - 1)); //期数
        return $return;
    }

    public function dailywingold()
    {
        $nper = intval(I('nper')) ? intval(I('nper')) : 2;
        $Dailywingold = D('Dailywingold');
        if (isset($_GET['b_time'])) {
            $map['time'] = strtotime(I('b_time'));
        } else {
            $map['time'] = strtotime(date('Y-m-d', NOW_TIME));
        }

        $map['from_id'] = array('between', array((0 + 1000000 * $nper), (999999 +  1000000 * $nper)));
        $list = $Dailywingold->where($map)->select();
        $functionname = 'plan_one' . $nper;
        $configname = 'PLAN_ONE' . $nper;
        $all_money = 0;
        $allNowpays = 0;
        $vertical = $from_ids = array();
        foreach ($list as $k => $v) {
            $total_money = $Dailywingold->$functionname($v, C($configname));
            $list[$k]['total_money'] = $total_money;
            $all_money += $total_money;
            $from_ids[] = intval($v['from_id']);
            foreach ($v as $key => $val) {
                $vertical[$key] += $val;
            }

            $allNowpays += $list[$k]["nowpays"];
        }

        $this->assign('nper', C($configname));
        $this->assign('all_money', $all_money);
        $this->assign('allNowpays', $allNowpays);
        $this->assign('vertical', $vertical);
        $this->assign('_list', $list);
        $this->assign('nperlist', C('PLAN_NUMS'));
        $this->assign('nownper', $nper);
        $this->display('dailywingold' . $nper);
    }

    /**
     * 赢取金币总数
     */
    public function dailywingold_bk()
    {
        $nper = intval(I('nper'));
        $config = $this->dailyConfig($nper);

        $Dailywingold = D('Dailywingold');
        if (isset($_GET['b_time'])) {
            $map['time'] = strtotime(I('b_time'));
        } else {
            $map['time'] = strtotime(date('Y-m-d', NOW_TIME));
        }

        $map['from_id'] = array('between', array($config['from_id'][0], $config['from_id'][1]));
        $list = $Dailywingold->where($map)->select();

        $all_money = 0;
        $allNowpays = 0;
        $vertical = $from_ids = array();
        foreach ($list as $k => $v) {
            $total_money = $Dailywingold->$config['func_name']($v);
            $list[$k]['total_money'] = $total_money;
            $all_money += $total_money;
            $from_ids[] = intval($v['from_id']);
            foreach ($v as $key => $val) {
                $vertical[$key] += $val;
            }
            $recharge = D('Paymoneystat')->fromRecharge(intval($v['from_id']), $b_time, $b_time);
            $list[$k]['rechargenum'] = $recharge['money'];
            $allNowpays += $list[$k]["nowpays"];
        }

        $nper = D('Dailywingold')->getconfig($config['nper'], true);

        $fromstat = D('fromstat')->where(array('from_id' => array('in', $from_ids)))->field('from_id,red1003,redtomoney')->select();
        $fromlist = array();
        foreach ($fromstat as $k => $val) {
            $fromlist[$val['from_id']]['red1003'] += $val['red1003'];
            $fromlist[$val['from_id']]['duihuan'] += $val['redtomoney'] / 10;
        }

        $this->assign('nper', $nper[1]);
        $this->assign('nper2', $nper[2]);
        $this->assign('all_money', $all_money);
        $this->assign('allNowpays', $allNowpays);
        $this->assign('vertical', $vertical);
        $this->assign('_list', $list);
        $this->assign('fromlist', $fromlist);
        $this->assign('nperlist', $config[0]);
        $this->assign('nownper', $config['nper']);
        $this->display($config['display']);
    }

    /**
     * 赢取金币总数(五期)
     */
    public function dailywingoldfiveth($p = 1)
    {
        $limit = 20;
        $Dailywingold = D('Dailywingold');

        if (isset($_GET['b_time'])) {
            $map['time'] = strtotime(I('b_time'));
        } else {
            $map['time'] = strtotime(date('Y-m-d', NOW_TIME));
        }

        $map['from_id'] = array('between', array(60000000, 69999999));

        $list = $Dailywingold->where($map)->select();

        $all_money = 0;
        $vertical = array();
        foreach ($list as $k => $v) {
            $total_money = $Dailywingold->calcu_forth_money_fiveth($v);
            $list[$k]['total_money'] = $total_money;
            $all_money += $total_money;

            foreach ($v as $key => $val) {
                $vertical[$key] += $val;
            }
        }

        $nper = D('Dailywingold')->getconfig(5, true);
        $this->assign('nper', $nper[1]);
        $this->assign('all_money', $all_money);
        $this->assign('vertical', $vertical);
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 赢取金币总数(六期)
     */
    public function dailywingoldsixth($p = 1)
    {
        $limit = 20;
        $Dailywingold = D('Dailywingold');

        if (isset($_GET['b_time'])) {
            $map['time'] = strtotime(I('b_time'));
        } else {
            $map['time'] = strtotime(date('Y-m-d', NOW_TIME));
        }

        $map['from_id'] = array('between', array(70000000, 79999999));

        $list = $Dailywingold->where($map)->select();

        $all_money = 0;
        $vertical = array();
        foreach ($list as $k => $v) {
            $total_money = $Dailywingold->calcu_forth_money_sixth($v);
            $list[$k]['total_money'] = $total_money;
            $all_money += $total_money;

            foreach ($v as $key => $val) {
                $vertical[$key] += $val;
            }
        }

        $nper = D('Dailywingold')->getconfig(6, true);
        $this->assign('nper', $nper[1]);
        $this->assign('all_money', $all_money);
        $this->assign('vertical', $vertical);
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 赢取金币总数(七期)
     */
    public function dailywingoldseven($p = 1)
    {
        $limit = 20;
        $Dailywingold = D('Dailywingold');

        if (isset($_GET['b_time'])) {
            $map['time'] = strtotime(I('b_time'));
        } else {
            $map['time'] = strtotime(date('Y-m-d', NOW_TIME));
        }

        $map['from_id'] = array('between', array(80000000, 89999999));

        $list = $Dailywingold->where($map)->select();

        $all_money = 0;
        $vertical = array();
        foreach ($list as $k => $v) {
            $total_money = $Dailywingold->calcu_forth_money_seven($v);
            $list[$k]['total_money'] = $total_money;
            $all_money += $total_money;

            foreach ($v as $key => $val) {
                $vertical[$key] += $val;
            }
        }

        $nper = D('Dailywingold')->getconfig(7, true);
        $this->assign('nper', $nper[1]);
        $this->assign('all_money', $all_money);
        $this->assign('vertical', $vertical);
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 赢取金币总数(八期)
     */
    public function dailywingoldeight($p = 1)
    {
        $limit = 20;
        $Dailywingold = D('Dailywingold');

        if (isset($_GET['b_time'])) {
            $map['time'] = strtotime(I('b_time'));
        } else {
            $map['time'] = strtotime(date('Y-m-d', NOW_TIME));
        }

        $map['from_id'] = array('between', array(90000000, 99999999));

        $list = $Dailywingold->where($map)->select();

        $all_money = 0;
        $vertical = array();
        foreach ($list as $k => $v) {
            $total_money = $Dailywingold->calcu_forth_money_eight($v);
            $list[$k]['total_money'] = $total_money;
            $all_money += $total_money;

            foreach ($v as $key => $val) {
                $vertical[$key] += $val;
            }
        }

        $nper = D('Dailywingold')->getconfig(8, true);
        $this->assign('nper', $nper[1]);
        $this->assign('all_money', $all_money);
        $this->assign('vertical', $vertical);
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 答题渠道完成任务统计
     */
    public function datiplaystat()
    {
        $daTiModel = D('Dati');

        if (isset($_GET['b_time'])) {
            $map['time'] = strtotime(I('b_time'));
        } else {
            $map['time'] = strtotime(date('Y-m-d', NOW_TIME));
        }
        $map['from_id'] = 10000017;

        $list = $daTiModel->where($map)->order('time desc')->select();

        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 任务日志统计
     */
    public function taskstat()
    {

        if (isset($_GET['goods_id'])) {
            $map['goods_id'] = intval(I('goods_id'));
        }

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399;
        } else {
            $b_time = strtotime(date('Y-m-d', NOW_TIME));
            $e_time = $b_time + 86399;
        }
        $map['time'] = array('between', array($b_time, $e_time));

        $list = D('Taskstat')->where($map)->order('goods_id asc')->select();
        $gold = array();
        foreach ($list as $v) {
            $gold[$v['goods_id']] += $v['gold'];
        }

        $this->assign('_list', $list);
        $this->assign('gold', $gold);
        $this->assign('goods', D('Configgoods')->getConfig());
        $this->assign('tasks', D('ConfigTask')->tasks());
        $this->display();
    }

    /**
     * 聊天记录
     */
    public function talklogs($p = 1)
    {
        $limit = 20;

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = intval($value);
                    break;
                    break;
                case 1: //角色ID
                    $map['uid'] = D('Users')->get_uid('char_id', trim($value));
                    break;
                case 2: //手机号
                    $map['uid'] = D('Users')->get_uid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $map['uid'] = D('Users')->get_uid('nickname', trim($value));
                    break;
            }
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time'));
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
        }

        $oneroom = intval(I('oneroom'));
        if ($oneroom) {
            $map['room'] = $oneroom;
        }

        $totals = D('Talklogs')->where($map)->count();
        $list = D('Talklogs')->where($map)->page($p, $limit)->order('c_time desc')->select();
        $uids = array();
        foreach ($list as $v) {
            $uids[] = $v['uid'];
        }
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->assign('onerooms', $this->onerooms());
        $this->assign('userinfo', D('Userinfo')->getUserInfo($uids));
        $this->display();
    }

    public function ybwlstat()
    {
        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $map['time'] = array('between', array($b_time, $e_time));
        $list = D('Ybwlstat')->where($map)->select();

        // $mapred['c_time'] = array('between', array($b_time, $e_time));
        // $mapred['room']=148;
        // $money_list = D('Redbaglog')->where($mapred)->select();
        // foreach($money_list as $k=>$val) {
        //     $moneys += intval($val['money']);
        // }

        $show = $total = array();
        foreach ($list as $v) {

            // echo("<script>console.log(".json_encode($v).");</script>");
            //  echo("<script>console.log(".json_encode('---'.$v['gold']).");</script>");
            if ($v['key'] == 6) //第一档红包
            {
                $moneys = 0;
                $total[$v['key']]['mens'] += $v['mens'];
                $total[$v['key']]['money'] += $v['money'];
                $b_time1 = $v['time'];
                $e_time1 = $v['time'] + 86399;
                $mapred['c_time'] = array('between', array($b_time1, $e_time1));
                $mapred['room'] = 148;
                $money_list = D('Redbaglog')->where($mapred)->select();
                foreach ($money_list as $k => $val) {
                    $moneys += number_format($val['money'], 1); //intval($val['money']);
                }
                $v['gold'] = $moneys / 10;
                $total[$v['key']]['gold'] += $v['gold']; //6元挡位的充值总数
                $show[$v['time']][$v['key']] = $v;

                //$total[$v['key']]['gold'] =$totals;//

            } else {
                $show[$v['time']][$v['key']] = $v;
                $total[$v['key']]['mens'] += $v['mens'];
                $total[$v['key']]['money'] += $v['money'];
                $total[$v['key']]['gold'] += $v['gold'];
            }
        }
        krsort($show);

        $this->assign('recharge', array(6, 38, 98, 328, 648));
        $this->assign('_list', $show);
        $this->assign('total', $total);
        $this->assign('from', D('DChannels')->order('id asc')->select());
        $this->display();
    }
    //七日任务统计
    public function seventaskuser()
    {


        //$data = $this->_calculatetime(); //计算开始结束时间
        // $b_time = $data['b_time'];
        // $e_time = $data['e_time'];
        $map['uid'] = array('egt', 0); //排除AI,150000--150148不算
        // $map['c_time'] = array('between', array($b_time, $e_time));
        $list = D('seventask')->where($map)->select();
        $timeday = $show = $total = array();
        foreach ($list as $k => $v) {
            $datef = date('Y-m-d', $v['c_time']);

            //  echo("<script>console.log(".json_encode('---'.$v['gold']).");</script>");
            if ($v['taskid'] == 560001) //第一档红包
            {
                $show[$datef][1]++;
                $total[1]++;
            } elseif ($v['taskid'] == 560002) //第2档红包
            {
                $show[$datef][2]++;
                $total[2]++;
            } elseif ($v['taskid'] == 560003) //3
            {
                $show[$datef][3]++;
                $total[3]++;
            } elseif ($v['taskid'] == 560004) //第4档红包
            {
                $show[$datef][4]++;
                $total[4]++;
            } elseif ($v['taskid'] == 560005) //第5档红包
            {
                $show[$datef][5]++;
                $total[5]++;
            } elseif ($v['taskid'] == 560006) //第6档红包
            {
                $show[$datef][6]++;
                $total[6]++;
            } elseif ($v['taskid'] == 560007) //第7档红包
            {
                $show[$datef][7]++;
                $total[7]++;
            }
        }
        krsort($show);

        //echo json_encode($show);
        $this->assign('show', $show);
        $this->assign('total', $total);
        $this->display();
    }
    //超级拉霸排行
    public function superlabarank($date)
    {
        $b_time = strtotime($date);
        $e_time = $b_time + 86399;
        $map['room'] = 1011;
        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = D('Goldget')->where($map)->order('gold desc')->select();
        $uids = array();
        foreach ($list as $v) {
            $uids[] = $v['uid'];
        }
        $this->assign('_list', $list);
        $this->assign('userinfo', D('Userinfo')->getUserInfo($uids));
        $this->display();
    }

    //空战拉霸排行
    public function airlabarank($date)
    {
        $b_time = strtotime($date);
        $e_time = $b_time + 86399;
        $map['room'] = 1014;
        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = D('Goldget')->where($map)->order('gold desc')->select();
        $uids = array();
        foreach ($list as $v) {
            $uids[] = $v['uid'];
        }
        $this->assign('_list', $list);
        $this->assign('userinfo', D('Userinfo')->getUserInfo($uids));
        $this->display();
    }

    /**
     * 计算开始结束时间
     */
    protected function _calculatetime()
    {

        if (isset($_GET['type'])) {
            $type = intval(I('type'));
            if ($type == 0) { //按月

                if (isset($_GET['month'])) {
                    $month = intval(I('month'));
                    $monarr = monthday(NOW_TIME, $month);

                    $b_time = strtotime($monarr[0]);
                    $now_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME)); //现在时间
                    $e_time = strtotime($monarr[1]);
                    if ($now_time < $e_time) {
                        $e_time = $now_time;
                    }
                } else {
                    $b_time = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y"))));
                    $e_time = NOW_TIME;
                }
            } elseif ($type == 1) { //按天数
                $b_time = strtotime(I('b_time'));
                $e_time = strtotime(I('e_time'));

                if (!isset($_GET['b_time']) || !isset($_GET['e_time'])) {
                    $this->error('请选择时间');
                }
            }
        } else { //默认当月数据
            $b_time = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y"))));
            $e_time = NOW_TIME;
        }

        $data['b_time'] = $b_time;
        $data['e_time'] = $e_time;

        return $data;
    }

    public function dailylottery($p = 1)
    {

        $limit = 20;
        $lotterylog = D('Lotterylog');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $e_time = $b_time + 86399; // 10 天
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        if (isset($_GET['e_time'])) {
            $e_time = strtotime(I('e_time')) + 86399;
            if ($e_time < $b_time) {
                $e_time = $b_time + 863990; // 10 天
            } else if ($e_time - $b_time > 863990) {
                $e_time = $b_time + 863990; // 10 天
            }
        }

        $map['time'] = array('between', array($b_time, $e_time));

        $totals = $lotterylog->where($map)->count();
        $list = $lotterylog->where($map)->page($p, $limit)->order('time desc')->select();
        // 'uid'
        // 'itmes'
        // 'time'
        // 'getGold'
        // 'useGold'
        // 'getRedbag'
        // 'useRedbag'
        $list_data = $lotterylog->where($map)->select();
        $data = array();
        foreach ($list_data as $v) {
            $data['allGetGold'] += $v['getGold'];
            $data['allUseGold'] += $v['useGold'];
            $data['allGetRedbag'] += $v['getRedbag'];
            $data['allUseRedbag'] += $v['useRedbag'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('data', $data);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }


   //每日统计数据
    public function daily()
    {
        $user = D('DUsers');
        $users = D('DAccounts');
        $pay = D('DPays');
       // $redpacket = D('redpacket_logs');
        $strengthUserList = []; //实力


        // $strengthList = $user->where(['uid' =>array('NEQ' ,'')])->field('uid,get,vip')->order('get desc')->limit(50)->select();
        // foreach ($strengthList as $k => $val) {
        //     $getUser = $users->where(['id'=> $val['uid']])->field('username, wxnick')->find();
        //     $strengthList[$k]['username'] = $getUser['wxnick'] ? $getUser['wxnick']: $getUser['username'];
        //     if(count($strengthUserList) < 20 && !empty($strengthList[$k]['username']) ) {
        //         $strengthUserList[$val['uid']] = $strengthList[$k];
        //     }
        // }


        // $last_names = array_column($strengthUserList,'get');
        // array_multisort($last_names,SORT_DESC,$strengthUserList);

        $contributionUserList = []; //单日贡献 充值
		$d_time = date("Y-m-d");
        $start_time = $d_time." 00:00:00";
        $end_time = $d_time." 23:59:59";
		
        // var_dump($end_time);exit;
        $contributionUserList = $pay->where(['created_at' => ['BETWEEN', [$start_time , $end_time]], 'status' => 1])
                                    ->field('sum(price) as money ,account as uid')->group('uid')->order('money desc')->limit(20)->select();
                                    // var_dump($pay->getLastSql());exit;
        foreach ($contributionUserList as $k => $val) {
            $getvip = $user->where(['account' => intval($val['uid'])])->field('vip_lev as vip,name')->find();
			$contributionUserList[$k]['name'] = $getvip['name'] ? $getvip['name'] :"";
            $contributionUserList[$k]['vip'] = $getvip['vip'] ? $getvip['vip'] :0 ;
        }

        $redbag_list = []; //金币排行
        $redbagList = $user->where(['account' => ['GT',0]])->where(['gold' => ['GT',0]])
                            ->field('account as uid,gold,vip_lev as vip')->order("gold desc")->limit(100)->select();
        
        // var_dump($redbagList);exit;

        foreach ($redbagList as $k => $val) {
            $getUser = $users->where(['id'=> $val['uid']])->field('name')->find();

            $redbagList[$k]['username'] = $getUser['name'];
            $redbagList[$k]['gold'] = $val['gold'];
            if(count($redbag_list) < 50 && !empty($redbagList[$k]['username'])) {
                $redbag_list[] = $redbagList[$k];
            }
        }

        $last_names = array_column($redbag_list,'gold');
        array_multisort($last_names,SORT_DESC,$redbag_list);



        //兑换排行
        // $redpacket_list = $redpacket->join('rui_user ON rui_user.id = rui_redpacket_logs.uid')
        //                             // ->where(['rui_user.id' => ['EGT', 0]])
        //                             ->where(['rui_redpacket_logs.create_time' => ['BETWEEN', [$start_time , $end_time]] , 'status' => 1])
        //                             ->group('rui_redpacket_logs.uid')
        //                             ->field('rui_redpacket_logs.uid, sum(rui_redpacket_logs.amount) as amount')
        //                             ->order('amount desc')
        //                             ->limit(20)->select();



        // foreach ($redpacket_list as $k => $val) {
        //     $getredUser = $users->where(['id'=> $val['uid']])->field('username, wxnick')->find();
        //     $redpacket_list[$k]['username'] = $getredUser['wxnick'] ? $getredUser['wxnick']: $getredUser['username'];
        //     $getvip = $user->where(['uid' => intval($val['uid'])])->field('vip')->find();

        //     $redpacket_list[$k]['vip'] = $getvip['vip'] ? $getvip['vip'] :0 ;
        // }

         //今日新增
       
		$stime = $d_time." 00:00:00";
        $etime = $d_time." 23:59:59";
        $newUserCount = $users->where( ['created_at' => ['BETWEEN' , [$stime,$etime]]])->count();
        $newUserCount = $newUserCount ? $newUserCount : 0;

        //在线人数
        $onlinecount = D('DUsers')->where( ['cur_login' => ['BETWEEN' , [strtotime($stime),strtotime($etime)]],"online"=>1])->count();
      
        $onlie_user = $onlinecount?$onlinecount:0;

        
        $this->assign('onlie_user', $onlie_user);
        $this->assign('new_user', $newUserCount);

        $this->assign('redpacket_list', $redpacket_list);
        $this->assign('redbagList', $redbag_list);
        $this->assign('contribution', $contributionUserList);
        $this->assign('strength', $strengthUserList);
        $this->display();

    }

    //充值总榜
    public function payRanking()
    {
        $limit = 100;
        $pay = D('Paylog');

        $query = ''; //排序条件

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'count_money desc';
        }

        // $totals = $pay->where($map)->join('rui_user ON rui_user.id = rui_pay.uid')->group('rui_pay.uid')->count();

        $list   = $pay->join('rui_user ON rui_user.id = rui_pay.uid')->field('sum(money) as count_money, rui_pay.*,rui_user.from_id,rui_user.from_time,rui_user.wxnick,rui_user.lastlogin')->limit($limit)->group('rui_pay.uid')->order($orders)->select();
        $fromkey = D('DChannels')->field('from_id as id,num, from_name')->order('from_id asc')->select();
        $fromkeyList = [];
        foreach ($fromkey as $item) {
            $fromkeyList[$item['id'].$item['num']] = $item['from_name'];
        }

        $this->assign('query', $query);
        $this->assign('from', $fromkey);
        $this->assign('fromList', $fromkeyList);
        $this->assign('_list', $list);
        $this->display();
    }
    
    // 充值统计
    public function payStat()
    {
        
        $b_time = I('b_time');
        // $b_time = !empty($b_time) ? $b_time :date("Y-m-d");
        $end_time = I("e_time");
        // $end_time = I('e_time') ? I('e_time') : date("Y-m-d");
        $async = 1;

        if ($async) {


            if (trim($b_time) != '' && trim($end_time) != '') {
                $map['created_at'] = array('between', array(strtotime($b_time . ' 00:00:00'), strtotime($end_time . ' 23:59:59')));
            } else if (trim($b_time) != '') {
                $map['created_at'] = array('egt', strtotime($b_time . ' 00:00:00'));
            } else if (trim($end_time) != '') {
                $map['created_at'] = array('elt', strtotime($end_time . ' 23:59:59'));
            }
            $map['status'] = 1;

            $pay = D('DPays');
            $users = $pay->field('sum(price) as price')->where($map)->group('account')->order('price desc')->select();
            // var_dump($pay->getLastSql());exit;
            // var_dump($users);exit;

            $data = array();
            $totals = count($users);
            $item = [];

            foreach ($users as $k => $v) {
                // $totals += $v['num'];
                if( $v['price'] < 100) {
                    $item["100以内"] ++;
                } else if($v['price'] >100 && $v['price'] <= 500 ) {
                    $item["100-500"] ++;
                } else if($v['price'] >500 && $v['price'] <= 1000) {
                    $item['500-1000'] ++;
                } else if($v['price'] > 1000 && $v['price'] <= 3000) {
                    $item['1000-3000'] ++;
                }
                else if($v['price'] > 3000 && $v['price'] <= 5000) {
                    $item['3000-5000'] ++;
                } else if($v['price'] > 5000 && $v['price'] <= 10000) {
                    $item['5000-10000'] ++;
                } else if($v['price'] > 10000) {
                    $item['10000以上'] ++;
                }
            }


            foreach ($item as $k =>$v) {
                $data[] = ['name' => "充值".$k."元:".$v."人", 'y' =>$v/$totals ];
            }



            // foreach ($data as $k => $val) {
            //     $dataStr .= "[ {name: '{$val['name']}',y: {$val['num']}}] ";
            // }

            // return $this->ajaxReturn(['data' => $data, 'totals' => $totals]);
        }
        // echo (json_encode($data));exit;
        $this->assign('dataStr', json_encode($data));
        $this->assign('b_time', $b_time);
        $this->assign('e_time', $end_time);
        $this->assign('totals', $totals);
        $this->display();


    }

    //异常用户列表
    public function abnormalListOld()
    {
        $map = [];
        $timeField = 'mycms_abnormal.addtime';
        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map[$timeField] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map[$timeField] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map[$timeField] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        } else {
            //默认显示今天
            //$map['date_at'] = date("Y-m-d");
        }

        $from_id = I('from_id');
        if($from_id > 0) {
            $map['d_users.from_id'] =  $from_id;
        } else {
            $map['d_users.from_id'] = ['neq', 10000];
        }
        $uid = I('uid');
        if($uid > 0) {
            $map['mycms_abnormal.uid'] = $uid;
        }
        $limit = 20;
        $p = I('p');
        $p = $p ? $p : 1;
        $user = D('DUsers');
        $model = M("Abnormal");
        $sort = I('sort');

        $totals = $model->where($map)
        ->join('gamebox.d_accounts ON d_accounts.id = mycms_abnormal.uid')
        ->join('game1.d_users ON d_users.account = mycms_abnormal.uid')
        ->count();

        $listModel   = $model
        ->join('gamebox.d_accounts ON d_accounts.id = mycms_abnormal.uid')
        ->join('game1.d_users ON d_users.account = mycms_abnormal.uid')
            ->where($map)
            ->field('d_accounts.id as uid,d_users.id as char_id,d_users.from_id,d_users.name as uname,d_users.created_at,mycms_abnormal.addtime, d_accounts.status, d_users.is_brush,mycms_abnormal.num, mycms_abnormal.recharge,mycms_abnormal.consume,d_users.from_time')
            ->page($p, $limit);
        //var_dump($model->getLastSql());
        if($sort) {
            $listModel->order('mycms_abnormal.'.$sort.' desc, mycms_abnormal.num desc');
        } else {
            $listModel->order('mycms_abnormal.num desc, mycms_abnormal.id desc');
        }
        $list = $listModel->select();

        $ids = array();
        foreach ($list as $k => $v) {
            $ids[] = intval($v['uid']);
            $sl_level = D('DChannelUserTasks')->where(['uid' => $v['uid'], 'room_type' => 1,'from_id' => $v['from_id'], 'from_time' => $v['from_time']])->getField('stage');
            $xx_level = D('DChannelUserTasks')->where(['uid' => $v['uid'], 'room_type' => 2,'from_id' => $v['from_id'], 'from_time' => $v['from_time']])->getField('stage');
            $hb_level = D('DChannelUserTasks')->where(['uid' => $v['uid'], 'room_type' => 3,'from_id' => $v['from_id'], 'from_time' => $v['from_time']])->getField('stage');
            $list[$k]['sl_level'] = $sl_level?$sl_level + 1:1;
            $list[$k]['xx_level'] = $xx_level?$xx_level + 1:1;
            $list[$k]['hb_level'] = $hb_level?$hb_level + 1:1;
            //计算消耗
            // $taskConifgId = D('TaskStat')->where(['uid' => $v['uid']])->getField('task_config_id', true);
            // $reward = D('TaskConfig')->where(['id' => ['in', $taskConifgId]])->field("sum(reward) as reward")->find();
            // $list[$k]['reward'] = $reward['reward'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $fromkey = D('DChannels')->order('id asc')->select();
        foreach ($fromkey as $item) {
            $fromkeyList[$item['from_id']]= $item['from_name']; //.$item['num']]
        }
        // var_dump($fromkeyList);exit;
        $user = D('DUsers')->getUserInfo($ids);

        $this->assign('e_time',I('e_time'));
        $this->assign('b_time',I('b_time'));
        $this->assign('from_id', $from_id);
        $this->assign('from', $fromkey);
        $this->assign('_list', $list);
        $this->assign('user',$user );
        $this->assign('fromkeyList', $fromkeyList);
        $this->assign('_page', $pageNav->show());
        $this->assign('rooms', D('DRooms')->rooms());
        $this->display();
    }

    //异常用户列表
    public function abnormalList()
    {
        $map = [];
        $type = I('type');
        $uid = I('uid');
        $from_time = I('from_time');
        if (isset($_GET['type']) && isset($_GET['uid'])) {
            switch ($type) {
                case 1:
                    $map['from_id'] = $uid;
                    $map['type'] = 2;
                    break;
                case 2:
                    $map['account'] = $uid;
                    $map['type'] = 1;
                    break;
                case 3:
                    $map['from_id'] = $uid;
                    $map['type'] = 1;
                    if (isset($_GET['from_time'])){$map['from_time'] = $from_time;}
                    break;
            }
            
        }
        else{$map['type'] = 2;}  
        $limit = 20;
        $p = I('p');
        $p = $p ? $p : 1; 
        $model = D("DUserEarnings"); 

        $totals = $model->where($map)->count();
        $list   = $model->where($map)->order('amount asc')->page($p, $limit)->select();

         

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
 
        $this->assign('type', $type);
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }
    //异常用户列表个人
    public function abnormal()
    {
        $map = [];
        $type = I('type');
        $uid = I('uid');
        $threshold = I('threshold');
        if(empty($threshold)){$threshold=100;}
        $map['amount'] = array('elt', -1*intval($threshold));
        $from_time = I('from_time');
        $map['type'] = 1;
        if (isset($_GET['type']) && isset($_GET['uid'])) {
            switch ($type) {
                case 1:
                    $map['from_id'] = $uid;
                    break;
                case 2:
                    $map['account'] = $uid;
                    break;
                case 3:
                    $map['from_id'] = $uid;
                    if (isset($_GET['from_time'])){$map['from_time'] = $from_time;}
                    break;
            }
            
        }  

        $limit = 20;
        $p = I('p');
        $p = $p ? $p : 1; 
        $model = D("DUserEarnings"); 
        
        $totals = $model->where($map)->count();
        $list   = $model->where($map)->order('amount asc')->page($p, $limit)->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
    
        $this->assign('type', $type);
        $this->assign('threshold', $threshold);
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }
    /**
     * 重新统计
     */
    public function reloadearnings()
    {
        $output = D('DUserEarnings')->chu_shi_hua_stat();
        $this->success($output);
    }
    // 充值档位统计
    public function payStatList()
    {

        $stat_id = I('stat_id');
        $stat_id = $stat_id > 0?$stat_id:1;
        switch ($stat_id) {
            case 1: $having = 'amount > 10000';break;
            case 2: $having = 'amount > 5000 and amount <= 10000';break;
            case 3: $having = 'amount > 3000 and amount <= 5000';break;
            case 4: $having = 'amount > 1000 and amount <= 3000';break;
            case 5: $having = 'amount > 500 and amount <= 1000';break;
            case 6: $having = 'amount >= 100 and amount <= 500';break;
            case 7: $having = 'amount < 100';break;
        }

        $stat = [
            1 => '10000以上',
            2 => '5000-10000',
            3 => '3000-5000',
            4 => '1000-3000',
            5 => '500-1000',
            6 => '100-500',
            7 => '100以下',
        ];
        $pay = D('DPays');
        $map['d_pays.status'] = 1;
        $p = I('p');
        $limit = 20;
        $list = $pay->field('sum(price) as amount,account as uid')
            ->where($map)
            ->group('uid')
            ->order('amount desc')
            ->having($having)
            ->page($p ? $p :0, $limit)
            ->select();
	
			
        $total = $pay
            ->field('sum(price) as amount,account as uid')
            ->where(['status' => 1])
            ->group('uid')
            ->having($having)
            ->select();
        $total = count($total);

        foreach ($list as $k => $v) {
            $mup['account'] = $v['uid'];
            $uinfo = D('DUsers')->where($mup)->find();
         
            $map['id'] = $v['uid'];
            $uacc = D('DAccounts')->where($map)->find();
   
            $list[$k]['last_logout'] = $uinfo['last_logout'];
            $list[$k]['created_at'] = $uinfo['created_at']; 
            $list[$k]['name'] = $uinfo['name'];
            $list[$k]['tel_no'] = $uacc['tel_no'];
        }

        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_page', $pageNav->show());
        $this->assign('stat', $stat);
        $this->assign('stat_id', $stat_id);
        $this->assign('_list', $list);
        $this->display();


    }

    /**
     * 充值档位
     */
    protected function recharge_goods($type=0)
    {
        $rechargeConfig = D('ConfigByshop')->getConfig();

        $goods = array();
        if($type == 1) {
            $goods = $rechargeConfig;
        } else {
            foreach ($rechargeConfig as $k => $v) {
                $goods[$v['id']] = $v['name'];
            }
        }

        return $goods;
    }

    //道具统计
    public function propsStat()
    {
        $stat_id = I('stat_id');
        $stat_id = $stat_id > 0?$stat_id:1;
       //充值物品
        $goods = $this->recharge_goods();
        // var_dump($goods);exit;

        $pay = D('DPays');
        $map['d_pays.status'] = 1;
        $p = I('p');
        $limit = 20;
        $list = $pay
            ->field('count(account) as num,price as money,shop_id as saletype')
            ->where($map)
            ->group('money')
            ->order('money desc')
            ->select();
        $total = count($list);


        $total_array = array_column($list, 'num');
        $total_num = array_sum($total_array);//数组的值作为键名，该值在数组中出现的次数作为值

        foreach ($list as $val) {
            $data[] = ['name' => $goods[$val['saletype']]."：金额【{$val['money']}】元：".$val['num']."人", 'y' =>$val['num']/$total_num ];
        }

        $this->assign('dataStr', json_encode($data));
        $this->assign('goods', $goods);
        $this->assign('stat', $stat);
        $this->assign('stat_id', $stat_id);
        $this->assign('_list', $list);
        $this->assign('totals', $totals);
        $this->display();
    }

    //渠道统计
    public function fromStat()
    {
        $e_date = I('e_time');
        $b_date = I('b_time');

        if(empty($e_date) || empty($b_date)) {
            //默认一个月时间
            $b_time = strtotime('-1 month');
            $e_time = time();
            $where['adddate'] = ['between', [$b_time, $e_time]];
            $e_date = date("Y-m-d", $e_time);
            $b_date = date('Y-m-d', $b_time);
        } else {
            $where['adddate'] = ['between', [strtotime($b_date), strtotime($e_date.' 23:59:59')]];
        }

        $from_id = I('from_id');

        $where['from_id'] = $from_id ? $from_id : 10000; //默认官方渠道
        $list = D('Users')
                    ->field(" FROM_UNIXTIME(adddate,'%Y-%m-%d') as date_at , count(*) as num ")
                    ->where($where)
                    ->group('date_at')
                    ->select();

                    // var_dump(D("Users")->getLastSql());exit;
        $dataStr = '';
        foreach ($list as $val) {
            $dataStr .="['".$val['date_at']."', ".$val['num']."],";
        }
        $dataStr = substr($dataStr, 0, strlen($dataStr)-1);

        //查询所有的渠道
        $fromList = D('DChannels')->field('from_id, from_name')->select();

        $this->assign('b_time', $b_date);
        $this->assign('e_time', $e_date);
        $this->assign('from_list', $fromList);
        $this->assign('from_id', $from_id);
        $this->assign('data_str', $dataStr);
        $this->display();
    }
}
