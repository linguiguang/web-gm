<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/17
 * Time: 14:09
 */

namespace Admin\Controller;

use Admin\Controller\AdminController;

/**
 * 运营操作控制器
 */
class OperateController extends AdminController
{

    /**
     * 物品发放
     */
    public function grant()
    {
        redirect(U('email')); //
        if (IS_POST) {
            $type = intval(I('post.type'));
            $char_id = intval(I('post.char_id'));
            $num = intval(I('post.num'));
            $uid = is_login(); //当前操作者

            if (empty($char_id)) {
                $this->error('角色不能为空');
            }

            if ($num == 0) {
                $this->error('发放数量不能为0');
            }

            //TODO::发送物品

            $data = array(
                'type' => $type,
                'uid' => $uid,
                'char_id' => $char_id,
                'num' => $num,
                'create_time' => NOW_TIME,
            );

            if (D('Grant')->add($data)) {
                $this->success('发放成功！');
            }
        } else {
            $this->display();
        }
    }

    /**
     * 物品发放日志
     */
    public function grantlog($p = 1)
    {
        $limit = 20;
        $grant = D('Grant');

        if (!isset($_GET['type'])) {
            $type = -1;
        } else {
            $type = intval(I('type'));
            $map['type'] = $type;
        }

        $char_id = intval(I('char_id'));
        if (!empty($char_id)) {
            $map['char_id'] = $char_id;
        }

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map['create_time'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['create_time'][] = array('elt', $e_time);
        }

        $totals = $grant->where($map)->count();
        $list = $grant->where($map)->page($p, $limit)->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        foreach ($list as $k => $v) {
            $list[$k]['uid'] = D('Member')->getNickName($v['uid']);
        }

        $this->assign('_list', $list);
        $this->assign('type', $type);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 邮件系统
     */
    public function email()
    {
	
        if (IS_POST) {
            $time = date('YmdHis', NOW_TIME);
            $hour = $time % 1000000 - $time % 10000;
            $hour = $hour / 10000;
            // if ($hour > 21 || ($hour >= 0 && $hour < 9)) {
            //     $this->error('发送失败...');
            //     return;
            // }

            $email = D('DEmail');
            $data = $this->checkData();
			/*
			//后台邮件
			SendMail struct {
				MType   uint32 `json:"type"`    //邮件模板ID
				Title   string `json:"title"`   //标题
				Content string `json:"content"` //内容
				UserId  uint32 `json:"role"`    //角色ID
				Reward  uint32 `json:"reward"`  //物品ID,数量
			}
			*/
            //发送邮件
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate.send_mail';
			//var_dump($post_url);
           // $post_data['time'] = NOW_TIME;
            $post_data['title'] = $data['title'];
            $post_data['content'] = $data['content'];
            $post_data['role'] = intval($data['uid']);
            $post_data['type'] = intval($data['type']);
            $post_data['reward'] = "[{".$data['shop_id'] . ',' . $data['num']."}]"; 

            //生成签名   远程发送邮件先关闭
            ksort($post_data);
            $keys_arr = array_keys($post_data);
            $sign = implode($keys_arr);

            $post_data['sign'] = $sign;

            $field = json_encode($post_data);

            //$field = str_replace('%2C', ',', $field);
            //记录日志
            \Think\Log::write('EMAILI_URL: ' . $post_url . '?' . $field, 'INFO');
            //var_dump($field); 
			//exit;
            $ret = PostUrl($post_url, $field, 1);

            $result = json_decode($ret, true); 

            if ($result['ret'] != 0) {
                $this->error('发送失败，原因：' . $result['msg']);
            }

            if ($email->add($data)) {
                $this->success('发送成功');
            } else {
                $this->error('发送失败');
            }
        } else {
            $type = trim(I('op'));
            if ($type == 'all') {
                $this->assign('type', 'all');
            }
            $goodtype = D('DItemBase')->getgoods();
            $this->assign('goodtype', $goodtype);
            //$this->assign('goods', D('Configgoods')->getConfig());
            $this->display();
        }
    }

    protected function checkData()
    {
        $all = I('post.all'); //全服邮件
        $nametype = intval(I('post.nametype'));
        $name = I('post.name'); //玩家信息

        $data['type'] = 99; //默认为玩家邮件
        $data['adminid'] = is_login();
        $data['create_time'] = date("Y-m-d H:i:s");
        $data['create_ip'] = $_SERVER['REMOTE_ADDR'];

        $data['title'] = trim(I('post.title'));

        $content = trim(I('post.content'));
        if ($content == '') {
            $this->error('内容不能为空');
        }

        $data['content'] = $content;

        $goods_id = intval(I('goods_id'));
        $num = intval(I('num'));

        if ($goods_id == 0 || $num == 0) {
            $this->error('物品选择错误或数量为空');
        }
		
       
		$data['shop_id'] = $goods_id; // 邮件参数
		$data['num'] = $num;
		

        if ($all) { //全服邮件
            $data['type'] = 1;
            return $data;
        }

        //玩家信息类型
        if ($nametype == 0) { //角色ID
            $char_id = intval($name);
        } elseif ($nametype == 1) { //手机号
            $char_id = D('DUsers')->get_charid('mobile', $name);
        } elseif ($nametype == 2) { //昵称
            $char_id = D('DUsers')->get_charid('nickname', trim($name));
        }

        if ($char_id == 0) {
            $this->error('此用户不存在');
        }

        $data['uid'] = $char_id;

        return $data;
    }

    /**
     * 邮件日志
     */
    public function emaillog($p = 1)
    {
        $limit = 20;
        $email = D('DEmail');

        $char_id = intval(I('char_id'));
        if ($char_id != 0) {
            $map['char_id'] = $char_id;
        }

        if (!isset($_GET['type'])) {
            $type = -1;
        } else {
            $type = intval(I('type'));
            $map['type'] = $type;
        }

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map['create_time'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['create_time'][] = array('elt', $e_time);
        }

        $totals = $email->where($map)->count();
        $list = $email->where($map)->page($p, $limit)->order('id desc')->select();

        foreach ($list as $k => $v) {
            $list[$k]['adminname'] = D('Member')->getNickName($v['adminid']);
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('type', $type);
		$goodtype = D('DItemBase')->getgoods();
		
        $this->assign('goodtype', $goodtype);
        $this->assign('config', D('Configgoods')->getConfig());

        $this->assign('_page', $pageNav->show());
        $this->display();
    }


    /**
     * 邮件日志
     */
    public function logemail($p = 1)
    {
        $limit = 20;
        $email = D('DMails');

        $uid = intval(I('char_id'));
        if ($uid != 0) {
            $map['uid'] = $uid;
        }

        if (!empty($_GET['mtypeid'])) {
            $mtypeid = intval(I('mtypeid'));
            // $email->where('LIKE %'.$mtypeid."%");
            $map['m_type'] = array('like', $mtypeid);;
        }
        if (!empty($_GET['shopid'])) {
            $shopid = intval(I('shopid'));
            // $email->where('LIKE %'.$mtypeid."%");
            $map['reward'] = array('like', "%".$shopid.",%");;
        }
        if (isset($_GET['b_time'])) {
            $b_time = I('b_time');
            $map['created_at'] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = date("Y-m-d H:i:s",(strtotime(I('e_time')) + 86399));
            $map['created_at'] = array('elt', $e_time);
        }

        $totals = $email->where($map)->count();
        $list = $email->where($map)->page($p, $limit)->order('created_at desc')->select();
        // var_dump($map);
        $config = D('Configgoods')->getConfig();
		$goodtype = D('DItemBase')->getgoods();
		$mtype = D('DMails')->getmtype();
		//print_r($config);
        foreach ($list as $k => $v) {
            $list[$k]['sender'] = D('Member')->getNickName($v['sender']);
			$list[$k]['mtypename'] = $mtype[$v['m_type']];
			
			$reward = str_replace("[{","",$v['reward']);			
			$reward = str_replace("}]","",$reward);
		
            $goodsArr = explode(',',$reward);
          
            if(count($goodsArr) > 0) {
              
				$goods = $goodtype[$goodsArr[0]]["name"]. ':'.$goodsArr[1]. ";";
               
                $list[$k]['goods'] = $goods;
				
                unset($goods);
            }

        }

        // var_dump($goods_id);exit;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
       
        $this->assign('goodtype', $goodtype);
		$this->assign('_list', $list);
		$this->assign('mtype', $mtype);
        $this->assign('mtypeid', $mtypeid);
        $this->assign('shopid', $shopid);
        $this->assign('config', $config);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 公告
     */
    public function announce()
    {
        if (IS_POST) {
            $nums = intval(I('post.nums'));
            $type = intval(I('post.type'));
            $content = trim(I('post.content'));

            if ($nums < 0) {
                $this->error('轮播次数需为正整数');
            }

            $data['nums'] = $nums;

            if ($content == '') {
                $this->error('公告内容不能为空');
            }

            $data['content'] = $content;          
            $data['type'] = $type;
            $data['create_time'] = NOW_TIME;
            $data['uid'] = is_login();

            //公告
            $post_url = C('POST_URL');
            $post_data['type'] = 'ano';
            $post_data['t'] = $type; //公告类型
            $post_data['count'] = $nums;
            $post_data['content'] = $content;

            $field = http_build_query($post_data);
            $ret = PostUrl($post_url, $field);

            if (D('DAnnounce')->add($data)) {
                $this->success('发送成功');
            } else {
                $this->error('发送失败');
            }
        } else {
            $info = D('DAnnounce')->order('id desc')->find();
            $this->assign('info', $info);
            $this->display();
        }
    }
    /**
     * 公告列表 test
     */
    public function announcelist($p = 1)
    {
        $act = I('act');
        if($act == 'del') {
            $id = I('id');
            D('DAnnounce')->where(['id' => $id])->delete();
            $this->success('删除成功');
            exit();
        } else if($act == 'stop') {
            $post_data = [];
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:cancel_sys_notice';
            $field = http_build_query($post_data);
            $field = str_replace('%2C', ',', $field);
            $ret = PostUrl($post_url, $field, 0);
            $this->success('操作成功');
            exit();
        }
        $limit = 20;

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map['create_time'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['create_time'][] = array('elt', $e_time);
        }

        $totals = D('DAnnounce')->where($map)->count();
        $list = D('DAnnounce')->where($map)->page($p, $limit)->order('id desc')->select();

        foreach ($list as $k => $v) {
            $list[$k]['uid'] = D('Member')->getNickName($v['uid']);
            $list[$k]['typename'] =  $v['type']==1 ? "系统公告-插播" : "系统公告-不插播";
            
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }


    /**
     * 删除公告
     */
    public function delannounce($id)
    {
        $id = intval($id);
        $cyannounce = D('DAnnounce');

        $info = $cyannounce->where(array('id' => $id))->find();
        if (!$info || $info['state'] == 0) {
            $this->error('此公告不存在或已被关闭');
        }

        $data['id'] = $id;
        $data['state'] = 0; //关闭

        //TODO::关闭操作

        if ($cyannounce->save($data)) {
            $this->success('关闭成功');
        } else {
            $this->error('关闭失败');
        }
    }
    /**
     * 循环公告
     */
    public function cycleannouce()
    {
        if (IS_POST) {
            $loop = intval(I('loop'));
            if ($loop == 0) {
                $this->error('周期输入错误');
            }

            $b_time = strtotime(I('b_time'));
            $e_time = strtotime(I('e_time'));
            if (I('b_time') == '' || I('e_time') == '') {
                $this->error('时间不能为空');
            }

            $content = trim(I('content', '', false));
            if ($content == '') {
                $this->error('内容不能为空');
            }

            $data['content'] = $content;
            $data['loop'] = $loop;
            $data['b_time'] = $b_time;
            $data['e_time'] = $e_time;
            $data['uid'] = is_login();
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $data['create_time'] = NOW_TIME;
            

            //公告
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate.horse_lamp';
            $post_data['sign'] = md5(C('SENDGAME_KEY').time());
            $post_data['timestamp'] = intval(time());
            $post_data['count'] = intval($loop );
            $post_data['content'] = $content;
            $field = json_encode($post_data);
            

            //记录日志
            \Think\Log::write('ANNOUNCEMENT: ' . $post_url . '?' . $field, 'INFO');

            $ret = PostUrl($post_url, $field,1);
            //$arrs = json_decode($ret, true);

            if ($ret =='ok') {

                if (D('DCyannounce')->add($data)) {
                    $this->success('发送成功', U('Operate/cycleannounlist'));
                } else {
                    $this->error('发送失败');
                }
            } else {
                $this->error('公告发送失败，原因：' . $arrs['msg']);
            }
        } else {
            $this->display();
        }
    }

    /**
     * 循环公告列表 test
     */
    public function cycleannounlist($p = 1)
    {
        $act = I('act');
        if($act == 'del') {
            $id = I('id');
            D('DCyannounce')->where(['id' => $id])->delete();
            $this->success('删除成功');
            exit();
        } else if($act == 'stop') {
            $post_data = [];
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:cancel_sys_notice';
            $field = http_build_query($post_data);
            $field = str_replace('%2C', ',', $field);
            $ret = PostUrl($post_url, $field, 0);
            $this->success('操作成功');
            exit();
        }
        $limit = 20;

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map['create_time'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['create_time'][] = array('elt', $e_time);
        }

        $totals = D('DCyannounce')->where($map)->count();
        $list = D('DCyannounce')->where($map)->page($p, $limit)->order('id desc')->select();

        foreach ($list as $k => $v) {
            $list[$k]['uid'] = D('Member')->getNickName($v['uid']);
           
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }


    /**
     * 删除循环公告
     */
    public function delcycelannouce($id)
    {
        $id = intval($id);
        $cyannounce = D('DCyannounce');

        $info = $cyannounce->where(array('id' => $id))->find();
        if (!$info || $info['state'] == 0) {
            $this->error('此公告不存在或已被关闭');
        }

        $data['id'] = $id;
        $data['state'] = 0; //关闭

        //TODO::关闭操作

        if ($cyannounce->save($data)) {
            $this->success('关闭成功');
        } else {
            $this->error('关闭失败');
        }
    }

    public function redpackagelist()
    {
        $b_time = I('b_time');
        $e_time = I('e_time');
        $act = I('act');
        if($b_time && $e_time) {
            $limit = I('limit');
            $amount = I('amount');

            if($act == 'update' && isset($_GET['limit']) && isset($_GET['amount'])) 
            {
                //补红包
                $post_url = C('GMBox_URL') . 'cgi-bin/gm_oprate.redpackage_patch';
                $post_data['sign'] = md5(C('SENDGAME_KEY').time());
                $post_data['timestamp'] = intval(time());
                $post_data['limit'] = $limit; //账号ID
                $post_data['amount'] = $amount;
                $post_data['begin_date'] = $b_time;
                $post_data['end_date'] = $e_time;
                $post_data['flag'] = 1;
                $field = json_encode($post_data);
                $ret = PostUrl($post_url, $field,1);
                if ($ret != 'ok')
                {
                 $this->error('补发红包失败');
                }
                else{$this->success('补发红包成功');}
            }
            else
            {
                //查询
                $post_url = C('GMBox_URL') . 'cgi-bin/gm_oprate.redpackage_patch';
                $post_data['sign'] = md5(C('SENDGAME_KEY').time());
                $post_data['timestamp'] = intval(time());
                $post_data['limit'] = 1; //账号ID
                $post_data['amount'] = 20000;
                $post_data['begin_date'] = $b_time;
                $post_data['end_date'] = $e_time;
                $post_data['flag'] = 0;
                $field = json_encode($post_data);
                $list = PostUrl($post_url, $field,1);
                $result = json_decode($ret, true); 

                if ($result['err'] != "") 

                print_r($list);
            }
          
            

        }
        
        $this->assign('_list', $list);
        $this->display();
    }
    /**
     * 启用用户
     */
    public function reforbid()
    {
        $ids = I('post.id');
        $uids = array_unique($ids);
        if (count($uids) == 0) {
            $this->error('请先选择用户');
        }

        $data['status'] = 0;
        foreach ($uids as $v) {
             //T出房间
            $post_url = C('GMBox_URL') . 'cgi-bin/gm_oprate.set_forbid_state';
            $post_data['sign'] = md5(C('SENDGAME_KEY').time());
            $post_data['timestamp'] = intval(time());
            $post_data['roleid'] = intval($v); //账号ID
            $post_data['status'] = 0;
            $field = json_encode($post_data);
            $ret = PostUrl($post_url, $field);
            if ($ret =='ok')
            {
                D('DAccounts')->where(array('id' => $v))->save($data);
            }
            
        }
        $this->success('启用成功');
    }

    /**
     * 封号
     */
    public function forbid()
    {

        if (IS_POST) {
            if (I('post.allid') == 1) {
                //批量封号id传递
                $ids = I('post.id');
                if (empty($ids)) {
                    $this->error('请选择要操作的用户');
                }

                $idstr = implode(',', $ids);
                cookie('forbidurl', $_SERVER['HTTP_REFERER']);

                $this->assign('idstr', $idstr);
                $this->assign('allid', 1);
                $this->display();
            } else {
                $ids = I('post.ids'); //批量封号
                $reason = trim(I('reason'));

                $data['status'] = 1; //封号
                $data['forbid_reason'] = $reason;

                if ($ids) { //批量封号提交 已对接

                    $uids = explode(',', $ids);
                    $uids = array_unique($uids);

                    if (count($uids) == 0) {
                        $this->error('请选择要操作的用户');
                    }

                    if ($reason == '') {
                        $this->error('封号原因不能为空');
                    }

                    foreach ($uids as $v) {
                        //T出房间
                        $post_url = C('GMBox_URL') . 'cgi-bin/gm_oprate.set_forbid_state';
                        $post_data['sign'] = md5(C('SENDGAME_KEY').time());
                        $post_data['timestamp'] = intval(time());
                        $post_data['roleid'] = intval($v); //账号ID
                        $post_data['status'] = 1;                       
                        $field = json_encode($post_data);
                        $ret = PostUrl($post_url, $field,1);                     
                        if ($ret == 'ok')
                        {
                            D('DAccounts')->where(array('id' => intval($v)))->save($data);  
                        }                        

                    }
                    $this->success('封号成功', cookie('forbidurl'));
                } 
                else { 
                        //待对接
                        //单个用户封号提交
                        $nametype = intval(I('nametype'));
                        $name = I('name');

                        switch ($nametype) {
                            case 0: { //角色ID
                                    $char_id = intval($name);
                                    break;
                                }
                            case 1: { //手机号
                                    $char_id = D('Users')->get_charid('mobile', $name);
                                    break;
                                }
                            case 2: { //玩家昵称
                                    $char_id = D('Users')->get_charid('name', $name);
                                    break;
                                }
                            case 3: { //用户id
                                    $char_id = D('Users')->get_charid('uid', $name);
                                    break;
                                }
                            case 4: { //设备号
                                $users = D('Users')->where(array('devid' => intval($name)))->field('id')->find();

                                $char_id = D('Users')->get_charid('uid', $users['id']);
                                break;
                            }
                        }


                        $userinfo = D('DUsers')->where(array('account' => $char_id))->field('account')->find();
                        $uid = intval($userinfo['account']);

                        if (intval($uid) == 0 || intval($char_id) == 0) {
                            $this->error('此玩家不存在');
                        }

                        if ($reason == '') {
                            $this->error('封号原因不能为空');
                        }

                        //T出房间
                        $post_url = C('GMBox_URL') . 'cgi-bin/gm_oprate.set_forbid_state';
                        $post_data['sign'] = md5(C('SENDGAME_KEY').time());
                        $post_data['timestamp'] = intval(time());
                        $post_data['roleid'] = intval($uid); //账号ID
                        $post_data['status'] = 1;
                        $field = json_encode($post_data);
                        $ret = PostUrl($post_url, $field,1);
                       
                        if ($ret == 'ok')
                        {
                            D('DAccounts')->where(array('id' => intval($uid)))->save($data);  
                            $this->success('封号成功'); 
                        }
                        else {
                            $this->error('封号失败'.$ret);
                        }
                } 
                
                
            }
        } else {
            $list = D('DAccounts')->where(['status' => 1])->select();
            $this->assign('_list', $list);
            $this->display();
        }
    }

    /**
     * 游戏倍率控制
     */
    public function ratio($p = 1)
    {
        $limit = 20;
        $Ratio = D('Ratio');

        if (isset($_GET['stype'])) {
            $stype = intval(I('stype'));
        } else {
            $stype = 1;
        }
        $map['type'] = $stype;

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['char_id'] = D('Users')->get_charid('uid', $value);
                    break;
                case 1:
                    $map['char_id'] = intval($value);
                    break;
                case 2:
                    $map['char_id'] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['char_id']);
                    }
                    $map['char_id'] = array('in', $ids);
                    break;
            }
        }

        $totals = $Ratio->where($map)->count();
        $list = $Ratio->where($map)->page($p, $limit)->order('create_time desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('stype', $stype);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 新增倍率调整
     */
    public function addratio()
    {
        if (IS_POST) {

            if (I('ratio') === '') {
                $this->error('倍率不能为空');
            }

            $ratio = intval(I('ratio'));
            $stage = intval(I('stage'));
            $char_id = intval(I('char_id'));

            if ($ratio > 100 || $ratio < 0) {
                $this->error('倍率值设置超出范围');
            }

            if (!D('Userinfo')->where(array('char_id' => $char_id))->find()) {
                $this->error('不存在此游戏角色');
            }

            $map['char_id'] = $char_id;
            $map['type'] = 1;
            $map['stage'] = $stage;
            if (D('Ratio')->where($map)->find()) {
                $this->error('此角色已添加，请到列表页内进行编辑', U('Operate/ratio'));
            }

            $data['ratio'] = $ratio;
            $data['uid'] = is_login();
            $data['char_id'] = $char_id;
            $data['stage'] = $stage;
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $data['create_time'] = NOW_TIME;

            $ret = $this->post_ratio($char_id, 0, 1, $ratio);

            if ($ret == 'SUCCESS') {

                if (D('Ratio')->add($data)) {
                    $this->success('设置成功', U('Operate/ratio'));
                } else {
                    $this->error('设置失败');
                }
            } else {
                $this->error('设置失败，原因' . $ret);
            }
        } else {
            $this->display();
        }
    }

    /**
     * 编辑倍率
     */
    public function editratio($id)
    {

        $map['id'] = intval($id);
        $map['type'] = 1;
        $info = D('Ratio')->where($map)->find();

        if (IS_POST) {

            if (I('ratio') === '') {
                $this->error('倍率不能为空');
            }

            $ratio = intval(I('ratio'));
            $stage = intval(I('stage'));
            $char_id = intval(I('char_id'));

            if ($ratio > 100 || $ratio < 0) {
                $this->error('倍率值设置超出范围');
            }

            if (!D('Userinfo')->where(array('char_id' => $char_id))->find()) {
                $this->error('不存在此游戏角色');
            }

            if ($info['char_id'] != $char_id) {
                $this->error('不存在此此玩家控制记录', U('Operate/ratio'));
            }

            $data['ratio'] = $ratio;
            $data['uid'] = is_login();
            $data['char_id'] = $char_id;
            $data['stage'] = $stage;
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $data['create_time'] = NOW_TIME;

            $ret = $this->post_ratio($char_id, 0, 1, $ratio);

            if ($ret == 'SUCCESS') {

                if (D('Ratio')->where($map)->save($data)) {
                    $this->success('设置成功', U('Operate/ratio'));
                } else {
                    $this->error('设置失败');
                }
            } else {
                $this->error('设置失败，原因' . $ret);
            }
        } else {
            $this->assign('info', $info);
            $this->display();
        }
    }

    /**
     * 删除倍率
     */
    public function delratio($id)
    {

        $Ratio = D('Ratio');

        $map['id'] = intval($id);
        $map['type'] = 1;
        if (!$info = $Ratio->where($map)->find()) {
            $this->error('无此角色的控制记录');
        }

        $ret = $this->post_ratio($info['char_id'], 0, 0, 0);

        if ($ret == 'SUCCESS') {
            if ($Ratio->where($map)->save(array('type' => 0))) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('删除失败,原因' . $ret);
        }
    }

    private function post_ratio($char_id, $type, $flag, $rand)
    {
        //控制倍率
        $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:set_niu_blacklist';
        $post_data['sign'] = '';
        $post_data['roleid'] = $char_id;
        $post_data['type'] = $type;
        $post_data['flag'] = $flag;
        $post_data['rand'] = $rand;

        $field = http_build_query($post_data);
        $field = str_replace('%2C', ',', $field);

        //记录日志
        \Think\Log::write('CONTROLL RATIO: ' . $post_url . '?' . $field, 'INFO');

        $ret = PostUrl($post_url, $field, 0);

        \Think\Log::write('CONTROLL RATIO DETAIL: ' . $ret, 'INFO');

        $arrs = json_decode($ret, true);

        if ($arrs['ret'] == 0) {
            return 'SUCCESS';
        } else {
            return $arrs['msg'];
        }
    }

    public function labarate()
    {
        if (IS_POST) {
            $rate = intval(I('rate'));
            $exponent = floatval(I('exponent'));

            $data['rate'] = $rate;
            $data['exponent'] = $exponent;
            $data['uid'] = is_login();
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $data['create_time'] = NOW_TIME;

            //设置倍率
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:set_game_fax';
            $post_data['sign'] = '';
            $post_data['type'] = 1;
            $post_data['rate'] = $rate;
            $post_data['fax'] = $exponent;

            $field = http_build_query($post_data);
            $field = str_replace('%2C', ',', $field);

            //记录日志
            \Think\Log::write('LABARATE: ' . $post_url . '?' . $field, 'INFO');

            $ret = PostUrl($post_url, $field, 0);
            $arrs = json_decode($ret, true);

            if ($arrs['ret'] == 0) {
                if (D('Labarate')->add($data)) {
                    $this->success('设置成功!');
                } else {
                    $this->error('设置失败');
                }
            } else {
                $this->error('公告发送失败，原因：' . $arrs['msg']);
            }
        } else {
            $this->display();
        }
    }

    public function shop_exchange($p = 1)
    {
        $limit = 20;
        // $where['status'] = intval(I('state'));

        if (isset($_GET['b_time'])) {
            if (isset($_GET['e_time'])) {
                
                if (strtotime(I('e_time')) > strtotime(I('b_time'))) {

                    $this->error('结束时间不能小于开始时间');return;
                } 

                $b_time = I('b_time')." 00:00:00";
                $e_time = I('e_time')." 23:59:59";
                $where['created_at'] = array('between', array($b_time, $e_time));

            } else {
                $where['created_at'] = array('egt', strtotime(I('b_time')));
            }
        } else {
            if (isset($_GET['e_time'])) {
                $where['created_at'] = array('elt', strtotime(I('e_time')));
            } else {
                $where['created_at'] = array('egt', NOW_TIME - 86400 * 7);
            }
        }
        //强制当月时间
        $b_time = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $e_time = date("Y-m-d H:i:s");
        $where['created_at'] = array('between', array($b_time, $e_time));

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:                    
                    $userinfo = D('DUsers')->where(array('account' => intval($value)))->field('id')->find();
                    $where['uid'] = intval($userinfo['id']);
                    break;
                case 1:
                    $where['uid'] = intval($value);
                    break;
                case 2:
                    $uid = D('DUsers')->where(array('name' => trim($value)))->getField('id');
                    $where['uid'] = intval($uid);
                    break;
                case 3:
                    $userinfo = D('DUsers')->where(array('name' => array('like', trim($value))))->field('id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['id']);
                    }
                    $where['uid'] = array('in', $ids);
                    break;
            }
        } else {

            if (isset($_GET['from_id'])) {
                $userinfo = D('DAccounts')
                            ->alias('a')
                            ->join('game1.d_users u ON a.id = u.account')
                            ->where(array('a.from_id' => intval($_GET['from_id'])))
                            ->field('u.id')->select();
                $ids = array();
                foreach ($userinfo as $v) {
                    $ids[] = intval($v['id']);
                }
                $where['uid'] = array('in', $ids);
            } else {
                $from_id = -1;
            }
        }
        $excc = D('DShopExchangesConfig')->getexcconfig();
       // print_r($excc);
        $totals = D('DShopExchanges')->where($where)->count();
        $list = D('DShopExchanges')->where($where)->order('created_at desc')->page($p, $limit)->select();

        $config = D('DTaskBase')->gettaskbase(); //商城兑换数据表
        $goodtype = D('DItemBase')->getgoods();
        foreach ($list as $k => $v) {
            $list[$k]['describe'] = $excc[$v['exchange_id']]['desc']; //商品类型
            $item_id = $excc[$v['exchange_id']]['item_id'];
            $num = $excc[$v['exchange_id']]['num'];
           // print($item_id);
           /*  $reward = $config[$v['shop_id']]['reward']; //奖励
            $reward = str_replace("[{","",$reward);			
            $reward = str_replace("}]","",$reward);            
            $goodsArr = explode(',',$reward);
            if(count($goodsArr) > 0) { */
              
				$goods = $goodtype[$item_id]["name"].":".$num;
               
                $list[$k]['goods'] = $goods;
				
                unset($goods);
           // }

            $userinfo = D('DAccounts')->where(array('id' => $v['uid']))->getField('name');
            $list[$k]['truename'] = $userinfo;
            
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        //$red_top = $this->_getredtop(D('RedpacketLogs')->where($where)->select(), $config); //红包数据顶部显示

        $fromkey = D('DChannels')->order('id asc')->group('from_id')->select();

        // var_dump($config);exit;
        $this->assign('from', $fromkey);
        $this->assign('from_id', $from_id);

        $this->assign('_list', $list);
        $this->assign('goods', $config); //商城数据
        $this->assign('_page', $pageNav->show());
        $this->display();
    }


    public function labalogs($p = 1)
    {
        $limit = 20;

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map['create_time'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['create_time'][] = array('elt', $e_time);
        }

        $totals = D('Labarate')->where($map)->count();
        $list = D('Labarate')->where($map)->page($p, $limit)->order('id desc')->select();

        foreach ($list as $k => $v) {
            $list[$k]['uid'] = D('Member')->getNickName($v['uid']);
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 倍率控制日志
     */
    public function ratiolog($p = 1)
    {
        $limit = 20;
        $ratio = D('Ratio');

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map['create_time'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['create_time'][] = array('elt', $e_time);
        }

        $totals = $ratio->where($map)->count();
        $list = $ratio->where($map)->page($p, $limit)->order('id desc')->select();

        $ids = array();
        foreach ($list as $k => $v) {
            $list[$k]['uid'] = D('Member')->getNickName($v['uid']);
            $ids[] = intval($v['char_id']);
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('char', D('Userinfo')->getUserInfo($ids, 'char_id'));
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 玩家信息查询
     */
    public function userquery($p = 1)
    {
        $limit = 20;
        $user = D('Userinfo');

        $nickname = trim(I('nickname'));

        $query = '';

        if (isset($_GET['islogin'])) {
            $islogin = intval(I('islogin'));
            $map['islogin'] = $islogin;
            $query .= 'islogin/' . $islogin . '/';
        } else {
            $islogin = -1;
        }

        if (isset($_GET['room'])) {
            $room = intval(I('room'));
            $map['area'] = $room;
            $query .= 'room/' . $room . '/';
        } else {
            $room = -1;
        }

        if ($nickname != '') {
            $map['username'] = array('like', $nickname);
            $query .= 'nickname/' . $nickname . '/';
        }

        $uid = intval(I('uid'));
        if ($uid != 0) {
            $map['uid'] = $uid;
            $query .= 'uid/' . $uid . '/';
        }

        $orderby = trim(I('orderby'));
        $order = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'c_time desc';
        }

        $totals = $user->where($map)->count();
        $list = $user->where($map)->page($p, $limit)->order($orders)->select();

        $uids = array();
        $no = 1;
        foreach ($list as $k => $v) {
            $list[$k]['id'] = ($p - 1) * 20 + $no++;
            $uids[] = $v['uid'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $info = $this->gettopinfo($map);

        $this->assign('info', $info);
        $this->assign('islogin', $islogin);
        $this->assign('room', $room);
        $this->assign('query', $query);
        $this->assign('_list', $list);
        $this->assign('users', D('Users')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 用户信息顶部数据显示
     */
    private function gettopinfo($map)
    {
        $list = D('Userinfo')->where($map)->select();

        $gold = $gem = $wing = $bill = $red = 0;
        foreach ($list as $v) {
            $gold += $v['gold'];
            $gem += $v['gem'];
            $wing += $v['wing'];
            $bill += $v['bill'];
            $red += $v['redpacket'];
        }
        $data = array(
            'gold' => $gold,
            'gem' => $gem,
            'wing' => $wing,
            'bill' => $bill,
            'red' => $red,
        );
        return $data;
    }

    /**
     * 商品兑换
     */
    public function exchange_bk($p = 1)
    {
        $limit = 20;
        $shop = D('RedpacketLogs');

        $map = ''; //查询条件

        $state = intval(I('state'));
        $map .= ' a.status = ' . $state;

        if (!isset($_GET['money'])) {
            $money = -1;
        } else {
            $money = intval(I('money'));
        }

        // 红包
        $redba = I('redba');
        if (!$redba && $redba != '0') {
            $redba = 1;
        } else {
            $redba = intval($redba);
        }

        if ($redba == 1) { //红 包

            if ($money == 1) {
                $map .= ' and (a.goods_id = 99050)';
            } elseif ($money == 2) {
                $map .= ' and (a.goods_id = 99053)';
            } elseif ($money == 5) {
                $map .= ' and (a.goods_id = 99018)';
            } elseif ($money == 50) {
                $map .= ' and (a.goods_id = 99020)';
            } elseif ($money == 100) {
                $map .= ' and (a.goods_id = 99051)';
            } else {
                $map .= ' and a.goods_id in (99015,99016,99017,99018,99019,99020,99050,99051,99052,99053,99054)';
            }

            // }elseif($redba == 2){//金币兑换
            //    echo "asdasd";

            //    $this->display();
            //exit;
        } else {
            //非红包物品
            $map .= ' and a.goods_id not in (99015,99016,99017,99018,99019,99020,99050,99051,99052,99053,99054)';
        }

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map .= ' and a.c_time >= ' . $b_time;
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map .= ' and a.c_time <= ' . $e_time;
        }

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map .= ' and a.uid = ' . intval($value);
                    break;
                case 1:
                    $userinfo = D('Userinfo')->where(array('char_id' => intval($value)))->field('uid')->find();
                    $map .= ' and a.uid = ' . $userinfo['uid'];
                    break;
                case 2:
                    $uid = D('Users')->where(array('username' => trim($value)))->getField('id');
                    $map .= ' and a.uid = ' . $uid;
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('uid')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['uid']);
                    }
                    $uids = implode(',', $ids);
                    $map .= ' and a.uid in ( ' . $uids . ' )';
                    break;
            }
        }

        if (isset($_GET['from_id'])) {
            $from_id = intval(I('from_id'));
            $map .= ' and b.from_id = ' . $from_id;
        } else {
            $from_id = -1;
        }

        $total_list = D('RedpacketLogs')->alias('a')->field('a.*')->join('rui_user b ON a.uid = b.id where ' . $map)->select();
        $list = $shop->alias('a')->field('a.*')->join('rui_user b ON a.uid = b.id where ' . $map)->page($p, $limit)->order('a.exchange_id desc')->select();
        $totals = count($total_list);

        $config = D('ConfigByshop')->getConfig(); //商城数据表

        $redLogs = D('RedpacketLogs');

        foreach ($list as $k => $v) {
            $list[$k]['cost_type'] = $config[$v['goods_id']]['cost_type']; //商品类型
            if ($config[$v['goods_id']]['cost_type'] == 1) { //红包
                $log_data = $redLogs->where(array('shop_id' => $v['id']))->field('msg,result,result_code')->order('exchange_id desc')->find();
                $list[$k]['msg'] = $log_data['msg'];
                //                if($log_data['result_code'] == 'SUCCESS'){
                $a_json = json_decode($log_data['result'], true);
                $list[$k]['billno'] = $a_json['mch_billno'];
                //                }
            }
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $red_top = $this->_getredtop($total_list, $config); //红包数据顶部显示

        $this->assign('from', D('DChannels')->select());
        $this->assign('from_id', $from_id);
        $this->assign('red_top', $red_top);
        $this->assign('money', $money);
        $this->assign('_list', $list);
        $this->assign('goods', $config); //商城数据
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    public function exchange($p = 1)
    {
        $limit = 20;
        // $where['status'] = intval(I('state'));

        // 红包条件
        $redba = I('redba');
        if (!$redba && $redba != '0') {
            $redba = 1;
        } else {
            $redba = intval($redba);
        }

        // 红包金额
        if (!isset($_GET['money'])) {
            $money = -1;
        } else {
            $money = intval(I('money'));
        }

        if ($redba == 1) { //红 包
            if ($money == 1) {
                $where['goods_id'] =  99050;
            } else if ($money == 2) {
                $where['goods_id'] =  99053;
            } else if ($money == 5) {
                $where['goods_id'] =  99018;
            } else if ($money == 10) {
                $where['goods_id'] =  99054;
            } else if ($money == 50) {
                $where['goods_id'] =  99020;
            } else if ($money == 300) {
                $where['goods_id'] =  99021;
            } else if ($money == 600) {
                $where['goods_id'] =  99022;
            } else if ($money == 100) {
                $where['goods_id'] =  99051;
            } else if ($money == 150) {
                $where['goods_id'] = 990211;
            } else if ($money == 200) {
                $where['goods_id'] = 990222;
            }
        } else {
            //非红包物品
            $where['goods_id'] = array(
                'not in', array(
                    99015, 99016, 99017, 99018, 99019, 99020, 99021, 99022, 99050, 99051, 99052, 99053, 99054
                )
            );
        }

        if (isset($_GET['b_time'])) {
            if (isset($_GET['e_time'])) {
                if ($_GET['b_time'] == $_GET['e_time']) {
                    $where['create_time'] = array('between', array(strtotime(I('b_time')), strtotime(I('e_time')) + 86399));
                } else {
                    if (strtotime(I('e_time')) > strtotime(I('b_time'))) {
                        $where['create_time'] = array('between', array(strtotime(I('b_time')), strtotime(I('e_time')) + 86399));
                    } else {
                        $where['create_time'] = array('between', array(strtotime(I('b_time')), strtotime(I('e_time')) + 86399));
                    }
                }
            } else {
                $where['create_time'] = array('egt', strtotime(I('b_time')));
            }
        } else {
            if (isset($_GET['e_time'])) {
                $where['create_time'] = array('elt', strtotime(I('e_time')));
            } else {
                $where['create_time'] = array('egt', NOW_TIME - 86400 * 7);
            }
        }

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $where['uid'] = intval($value);
                    break;
                case 1:
                    $userinfo = D('Userinfo')->where(array('char_id' => intval($value)))->field('uid')->find();
                    $where['uid'] = intval($userinfo['uid']);
                    break;
                case 2:
                    $uid = D('Users')->where(array('username' => trim($value)))->getField('id');
                    $where['uid'] = intval($uid);
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('uid')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['uid']);
                    }
                    $where['uid'] = array('in', $ids);
                    break;
            }
        } else {

            if (isset($_GET['from_id'])) {
                $userinfo = D('Users')->where(array('from_id' => intval($_GET['from_id'])))->field('id')->select();
                $ids = array();
                foreach ($userinfo as $v) {
                    $ids[] = intval($v['id']);
                }
                $where['uid'] = array('in', $ids);
            } else {
                $from_id = -1;
            }
        }

        $totals = D('RedpacketLogs')->where($where)->count();
        $list = D('RedpacketLogs')->where($where)->order('create_time desc')->page($p, $limit)->select();

        $config = D('DTaskBase')->gettaskbase(); //商城兑换数据表
       
        foreach ($list as $k => $v) {
            $list[$k]['cost_type'] = $config[$v['goods_id']]['cost_type']; //商品类型
            $list[$k]['num'] = 1;
            $userinfo = D('Users')->where(array('id' => $v['uid']))->getField('wxnick');

            $list[$k]['truename'] = $userinfo;
            $list[$k]['msg'] = $v['msg'];
            if (!$list[$k]['msg'] && $list[$k]['result_code'] == true) {
                $list[$k]['msg'] = '微信返回备注为空';
            }
            $a_json = json_decode($v['result'], true);
            $list[$k]['billno'] = $a_json['mch_billno'];
            unset($list[$k]['result']);
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $red_top = $this->_getredtop(D('RedpacketLogs')->where($where)->select(), $config); //红包数据顶部显示

        $fromkey = D('DChannels')->order('id asc')->group('from_id')->select();

        // var_dump($config);exit;
        $this->assign('from', $fromkey);
        $this->assign('from_id', $from_id);
        $this->assign('red_top', $red_top);
        $this->assign('money', $money);
        $this->assign('_list', $list);
        $this->assign('goods', $config); //商城数据
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    private function _getredtop($list, $config)
    {

        $num = 0;
        $MONEY = C('MONEY');

        $_money = 0;
        $uids = $billuid = array();
        foreach ($list as $v) {
            if ($config[$v['goods_id']]['cost_type'] == 1) { //红包
                $num += 1;
                $uids[] = $v['uid'];
            } elseif ($config[$v['goods_id']]['cost_type'] == 2) { //话费
                $billnum += 1;
                $billuid[] = $v['uid'];
            }

            $_money += $MONEY[$v['goods_id']];
        }

        $data = array(
            'num' => $num,
            'users' => count(array_unique($uids)),
            'money' => $_money,
            'billnum' => $billnum,
            'billusers' => count(array_unique($billuid)),
            'billmoney' => 0
        );
        return $data;
    }

    /**
     * 兑换处理页
     */
    public function dooperate($ordersn = '', $uid = 0)
    {
        $shop = D('Shops');
        $config = D('ConfigByshop')->getConfig(); //商城数据表

        $ordersn = trim($ordersn);
        $uid = intval($uid);

        if ($ordersn == '' || $uid == 0) {
            $this->error('不存在此订单');
        }

        $map['ordersn'] = $ordersn;
        $map['uid'] = $uid;
        $good = $shop->where($map)->find();
        $good['cost_type'] = $config[$good['goods_id']]['cost_type']; //商品类型

        $type = $good['cost_type']; //物品类型

        if (IS_POST) {
            $audit = intval(I('post.audit'));
            if ($audit == 0) {
                $this->error('请审核');
            }

            if ($type == 3) { //实物(iphone7等)

                if ($audit == 1) {
                    $data['province'] = trim(I('province'));
                    $data['city'] = trim(I('city'));
                    $data['county'] = trim(I('county'));
                    $data['detail'] = trim(I('detail'));
                    $data['truename'] = trim(I('truename'));
                    $data['mobile'] = I('mobile');

                    if ($data['detail'] == '' || $data['truename'] == '' || $data['mobile'] == '') {
                        $this->error('详细地址，收件人和手机号不能为空');
                    }

                    if (!preg_match("/^1[34578]{1}\d{9}$/", $data['mobile'])) {
                        $this->error('手机号格式错误');
                    }
                    $content = '您好，您所兑换的奖励已经发货，请注意查收！';
                }
            } elseif ($type == 2) { //话费 (无手机号代表直充)

                if ($good['mobile'] == '') {
                    $card = trim(I('post.card'));
                    $pwd = trim(I('post.pwd'));
                    if ($audit == 1) { //审核通过
                        if ($card == '' || $pwd == '') {
                            $this->error('卡密不能为空');
                        }
                        $data['cardno'] = $card;
                        $data['cardpwd'] = $pwd;
                    }
                    $content = '您好，您所兑换的话费充值卡已经发放，请前往兑换记录查看卡号密码！';
                } else { //直充
                    //TODO:: 话费直充
                    $content = '您好，您所兑换的话费卡已经充入您所输入的账户！';
                }
            }

            $data['sid'] = is_login(); //操作员
            $data['opreate_time'] = NOW_TIME;
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $data['status'] = $audit;

            if ($shop->where($map)->save($data)) {

                //通知玩家发货
                if ($audit == 1) {
                    //角色id
                    $char = D('Userinfo')->getChar(intval($good['uid']));
                    $char_id = intval($char['char_id']);
                    if ($char_id == 0) {
                        $this->error('不存在此角色');
                    }

                    $post_url = C('POST_URL');
                    $post_data['type'] = 'mail';
                    $post_data['uid'] = $char_id; //角色ID
                    $post_data['content'] = $content;

                    //                    $field = http_build_query($post_data);
                    //                    $ret = PostUrl($post_url, $field);
                }

                $this->success('发货成功!', U('Operate/exchange', array('state' => $audit)));
            } else {
                $this->error('发货失败');
            }
        } else {
            $this->assign('good', $good);
            $this->display();
        }
    }

    /**
     * 批量兑换处理
     */
    public function dooperatelist($audit = 'success')
    {
        $ids = I('post.id');
        if (!$ids) {
            $this->error('未选择操作的订单');
        }

        $ids = (array) $ids;

        $audit = $audit == 'success' ? 1 : 2;

        $shop = D('Shops');
        $config = D('ConfigByshop')->getConfig(); //商城数据表
        foreach ($ids as $id) {
            $save = array();
            $goods_id = $shop->where(array('id' => $id))->getField('goods_id');
            $cost_type = $config[$goods_id]['cost_type']; //商品类型
            if ($cost_type == 1 || $cost_type == 3 || !$cost_type) {
                continue;
            }
            //不是虚拟物品跳过处理

            $save['sid'] = is_login(); //操作员
            $save['opreate_time'] = NOW_TIME;
            $save['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $save['status'] = $audit;
            $shop->where(array('id' => $id))->save($save);
        }
        $this->success('操作成功!', U('Operate/exchange', array('state' => $audit)));
    }

    //查询红包发放状态
    public function checkredstatus($ordersn)
    {
        if (!$ordersn) {
            $this->error('订单号错误！');
        }
        //使用红包接口
        require_once dirname(__DIR__) . '/WxPayLib/RedOrderQuery.php';
        $orderQuery = new \RedOrderQuery();
        $postData = array(
            'nonce_str' => $orderQuery->create_nonce_str(32),
            'mch_billno' => $ordersn,
            'mch_id' => $orderQuery::MCHID,
            'appid' => $orderQuery::APPID,
            'bill_type' => 'MCHT',
        );
        $orderQuery->values = $postData;
        $sign = $orderQuery->MakeSign();
        $postData['sign'] = $sign;
        $orderQuery->values = $postData;
        $xmlData = $orderQuery->ToXml();
        $posturl = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/gethbinfo';

        $xml = $orderQuery->curl_post_ssl($posturl, $xmlData);
        if (!$xml) {
            $this->error('请求失败!');
        }

        $retArr = $orderQuery->FromXml($xml);

        if ($retArr['return_code'] == 'SUCCESS' && $retArr['result_code'] == 'SUCCESS') {
            $retArr['status'] = $orderQuery->status($retArr['status']);
        }

        return $this->ajaxReturn($retArr);
    }

    

    /**
     * 红包管理界面
     */
    public function redpacket($p = 1)
    {
        $limit = 20;
        $redpacket = D('Redpacket');

        $closed = intval(I('closed'));
        $map['closed'] = $closed;

        $goods_id = intval(I('goods_id'));
        if (isset($_GET['goods_id'])) {
            $map['goods_id'] = $goods_id;
        }

        $totals = $redpacket->where($map)->count();
        $list = $redpacket->where($map)->page($p, $limit)->order('goods_id desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 添加红包物品
     */
    public function addredpacket()
    {
        if (IS_POST) {
            $data = $this->_checkfield();
            $data['currency'] = 1; //金钱直接兑换
            $data['create_time'] = NOW_TIME;
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];

            if (D('Redpacket')->add($data)) {
                $this->success('添加成功', U('redpacket'));
            } else {
                $this->error('添加失败');
            }
        } else {
            $this->display();
        }
    }

    /**
     * 编辑红包
     */
    public function editredpacket($id)
    {
        $id = intval($id);
        if ($id == 0) {
            $this->error('物品不存在');
        }

        if (IS_POST) {
            $data = $this->_checkfield();
            if (D('Redpacket')->where(array('id' => $id))->save($data)) {
                $this->success('更新成功', U('redpacket'));
            } else {
                $this->error('更新失败');
            }
        } else {
            $goods = D('Redpacket')->where(array('id' => $id))->find();
            $this->assign('goods', $goods);
            $this->display();
        }
    }

    /**
     * 数据检测
     */
    private function _checkfield()
    {
        $data = I('data');
        $list = array();

        $list['title'] = trim($data['title']);
        if ($list['title'] == '') {
            $this->error('标题不能为空');
        }

        $list['goods_id'] = intval($data['goods_id']);
        if ($list['goods_id'] == 0) {
            $this->error('物品ID输入错误');
        }

        $list['limit_num'] = intval($data['limit_num']);
        $list['num'] = intval($data['num']);
        $list['orderby'] = intval($data['orderby']);
        $list['endtime'] = strtotime($data['endtime']);
        $list['details'] = $data['details'];
        return $list;
    }

    /**
     * 上/下架商品
     */
    public function shopoprate($id, $method = 'up')
    {
        $id = intval($id);
        if ($id == 0) {
            $this->error('物品不存在');
        }

        $data['id'] = $id;
        if ($method == 'up') {
            $data['closed'] = 0;
        } elseif ($method == 'down') {
            $data['closed'] = 1;
        }

        if (D('Redpacket')->save($data)) {
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }

    /**
     * 系统盈亏
     */
    public function profitloss()
    {
        $grail = D('Grail');

        if (isset($_GET['type'])) {

            $type = intval(I('type'));
            if ($type == 0) { //按月
                $month = intval(I('month'));
                $monarr = monthday(NOW_TIME, $month);

                $b_time = strtotime($monarr[0]);
                $e_time = strtotime($monarr[1]);
            } elseif ($type == 1) { //按天数
                if (isset($_GET['b_time'])) {
                    $b_time = strtotime(I('b_time'));
                }

                if (isset($_GET['e_time'])) {
                    $e_time = (strtotime(I('e_time')));
                }
            }
        } else { //默认当月数据
            $b_time = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y"))));
            $e_time = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("t"), date("Y"))));
        }

        $list = array();

        for ($i = $b_time; $i <= $e_time; $i += 86400) {
            $date = date('Y-m-d', $i);
            if ($info = $this->_statgrail($i, $grail)) {
                $list[$date] = $info;
            }
        }

        $this->assign('_list', $list);
        $this->display();
    }

    private function _statgrail($time, $obj)
    {
        $b_time = $time;
        $e_time = $time + 86399;

        $grail = $obj;
        $map['c_time'] = array('between', array($b_time, $e_time));

        $ret = $grail->where($map)->field('grail,systempl')->select();
        $data = array();

        foreach ($ret as $v) {
            $data['gra'] += $v['grail'];
            $data['sys'] += $v['systempl'];
        }
        return $data;
    }

    /**
     * 盈亏详情
     */
    public function profitdetail($p = 1)
    {
        $limit = 20;
        $grail = D('Grail');

        $time = I('time');
        $b_time = strtotime($time);
        $e_time = $b_time + 86399;
        $map['c_time'] = array('between', array($b_time, $e_time));

        $totals = $grail->where($map)->count();
        $list = $grail->where($map)->page($p, $limit)->order('c_time asc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 渠道数据
     */
    public function channel()
    {
        if (!isset($_GET['from_id'])) {
            $from_id = 999;
        } else {
            $from_id = intval(I('from_id'));
        }

        $map['from_id'] = $from_id;
        $list = D('Stat')->where($map)->select();

        $this->assign('from_id', $from_id);
        $this->assign('_list', $list);
        $this->assign('from', D('DChannels')->order('id asc')->select());
        $this->display();
    }

    /*来源key*/
    public function fromkey($p = 1)
    {
        $fromobj = D('DChannels');

        $limit = 20;

        $username = trim(I('username'));
        if ($username != '') {
            $map['username'] = array('like', '%' . $username . '%');
        }

        $from_name = trim(I('from_name'));
        if ($from_name != '') {
            $map['from_name'] = array('like', '%' . $from_name . '%');
        }

        $total = $fromobj->where($map)->count();
        $list = $fromobj->where($map)->page($p, $limit)->order('id desc')->select();

        foreach ($list as $k => $val) {
            if (intval($val['parent_id']) > 0) {
                $list[$k]['parent_name'] = $fromobj->where(array('id' => $val['parent_id']))->getField('from_name');
            } else {
                $list[$k]['parent_name'] = '无';
            }

            $list[$k]['nums'] = D('Users')->where(array('from_id' => $val['from_id']))->count('id');

            $recharge = D('Paymoneystat')->fromRecharge(intval($val['from_id']));
            $list[$k]['rechargeman'] = $recharge['man'];
            $list[$k]['rechargenum'] = $recharge['money'];
        }

        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());

        $this->display();
    }

    public function addfKey()
    {
        if (IS_POST) {
            $fromobj = D('DChannels');
            $from_name = trim(I('post.from_name'));
            $username = trim(I('post.username'));
            $password = trim(I('post.password'));
            $repassword = trim(I('post.repassword'));

            if ($from_name == '') {
                $this->error('来源商不能为空');
            }

            if ($username == '') {
                $this->error('用户名不能为空');
            }

            if ($password == '' || $repassword == '') {
                $this->error('密码不能为空');
            }

            if ($repassword != $password) {
                $this->error('确认密码输入不一致');
            }

            $namelength = mb_strlen($username, 'utf-8');
            $pwdlength = mb_strlen($password, 'utf-8');
            if ($namelength < 1 || $namelength > 50) {
                $this->error('用户名长度不合法');
            }

            if ($pwdlength < 6 || $pwdlength > 30) {
                $this->error('密码长度不合法');
            }

            if ($fromobj->where(array('username' => $username))->find()) {
                $this->error('此用户名被占用');
            }

            $from_id = intval(D('DChannels')->where(array('parent_id' => 0))->order('id desc,from_id desc')->getField('from_id')) + 1;

            if ($fromobj->where(array('from_id' => $from_id))->find()) {
                $this->error('此来源id已存在');
            }

            $password = md5(md5($password));
            $data['from_name'] = $from_name;
            $data['from_id'] = $from_id;
            $data['username'] = $username;
            $data['password'] = $password;
            $data['create_time'] = NOW_TIME;
           
            if ($fromobj->add($data)) {
                $this->success('添加成功', U('fromkey'));
            } else {
                $this->error('添加失败！');
            }
        } else {
            $this->display();
        }
    }

    public function editfKey($id)
    {
        $id = intval($id);
        $map['id'] = $id;
        $keyinfo = D('DChannels')->where($map)->find();

        if (IS_POST) {
            $from_name = trim(I('post.from_name'));

            if ($from_name == '') {
                $this->error('来源商不能为空');
            }

            $data['id'] = $id;
            $data['from_name'] = $from_name;

            if (D('DChannels')->save($data)) {
                $this->success('修改成功', U('fromkey'));
            }
        } else {
            $this->assign('keyinfo', $keyinfo);
            $this->display();
        }
    }

    public function editpwd($id)
    {
        $id = intval($id);
        if ($id == 0) {
            $this->error('参数错误');
        }

        if (IS_POST) {
            $password = trim(I('post.password'));
            $repassword = trim(I('post.repassword'));

            if ($password == '' || $repassword == '') {
                $this->error('密码输入不能为空');
            }

            if ($password != $repassword) {
                $this->error('确认密码输入不一致');
            }

            $password = md5(md5($password));

            $data['id'] = $id;
            $data['password'] = $password;

            if (D('DChannels')->save($data)) {
                $this->success('密码修改成功', U('fromkey'));
            }
        } else {
            $this->display();
        }
    }

    public function delfKey($id)
    {
        $id = intval($id);

        $user = D('DChannels')->where(array('id' => $id))->find();

        if (intval($user['parent_id']) == 0) {
            $this->error('删除失败，只能删除子渠道！');
        }

        if (D('DChannels')->delete($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    public function banned()
    {

        if (IS_POST) {

            $nametype = intval(I('post.nametype'));
            $name = I('post.name');
            $flag = intval(I('post.flag'));

            switch ($nametype) {
                case 0: { //角色ID
                        $char_id = intval($name);
                        break;
                    }
                case 1: { //手机号
                        $char_id = D('Users')->get_charid('mobile', $name);
                        break;
                    }
                case 2: { //玩家昵称
                        $char_id = D('Users')->get_charid('nickname', $name);
                        break;
                    }
            }

            $userinfo = D('Userinfo')->where(array('char_id' => $char_id))->field('uid')->find();
            $uid = intval($userinfo['uid']);

            if (intval($uid) == 0 || intval($char_id) == 0) {
                $this->error('此玩家不存在');
            }

            $result = $this->op_banned($char_id, $flag);

            $banned_user = D('Banned')->where(array('uid' => $uid))->find();

            if ($result['ret'] != 0) {
                $this->error('操作失败，原因：' . $result['msg']);
            } else {

                if ($banned_user) {
                    $data['state'] = $flag;
                    $data['create_time'] = NOW_TIME;
                    if (!D('Banned')->where(array('uid' => $uid))->save($data)) {
                        $this->error('操作失败');
                    }
                } else {
                    $data['uid'] = $uid;
                    $data['char_id'] = $char_id;
                    $data['create_time'] = NOW_TIME;
                    if (!D('Banned')->add($data)) {
                        $this->error('操作失败');
                    }
                }

                $this->success('操作成功');
            }
        } else {
            $this->display();
        }
    }

    //禁言列表
    public function bannedlist($p = 1)
    {
        $limit = 20;

        if (isset($_GET['char_id'])) {
            $map['char_id'] = intval(I('char_id'));
        }

        $map['state'] = 1;

        $total = D('Banned')->where($map)->count();
        $list = D('Banned')->where($map)->page($p, $limit)->order('id desc')->select();

        $uids = array();
        foreach ($list as $v) {
            $uids[] = intval($v['uid']);
        }

        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('user', D('Userinfo')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    //解除禁言
    public function refu_banned($char_id)
    {
        $char_id = intval($char_id);
        if ($char_id == 0) {
            $this->error('非法操作');
        }

        $result = $this->op_banned($char_id, 0);

        if ($result['ret'] != 0) {
            $this->error('操作失败，原因：' . $result['msg']);
        } else {
            if (D('Banned')->where(array('char_id' => $char_id))->save(array('state' => 0))) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
    }

    //禁言发送
    protected function op_banned($char_id, $flag)
    {
        $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:set_forbid_state';
        $post_data['sign'] = '';
        $post_data['roleid'] = intval($char_id); //角色ID
        $post_data['type'] = 2;
        $post_data['flag'] = $flag;

        $field = http_build_query($post_data);

        $field = str_replace('%2C', ',', $field);

        $ret = PostUrl($post_url, $field, 0);

        $result = json_decode($ret, true);

        return $result;
    }

    //补游戏局数数据
    public function filldata()
    {
        if (IS_POST) {
            $nametype = intval(I('nametype'));
            $name = I('name');

            if (!$name) {
                $this->error('输入不能为空');
            }

            switch ($nametype) {
                case 0: { //用户ID
                        $char_id = D('Users')->get_charid('uid', $name);
                        break;
                    }
                case 1: { //角色ID
                        $char_id = intval($name);
                        break;
                    }
                case 2: { //手机号
                        $char_id = D('Users')->get_charid('mobile', $name);
                        break;
                    }
            }

            $Userinfo = D('Userinfo');
            $map['char_id'] = $char_id;
            if (!$info = $Userinfo->where($map)->find()) {
                $this->error('玩家不存在');
            } else {
                $Userinfo->where($map)->save(array('paly_three' => 1));
                $data = array(
                    'uid' => $info['uid'],
                    'char_id' => $char_id,
                    'c_time' => NOW_TIME,
                );
                D('Filllogs')->add($data);
            }

            $this->success('操作成功');
        } else {
            $this->display();
        }
    }

    //踢出玩家
    public function tuser()
    {
        if (IS_POST) {
            $nametype = intval(I('nametype'));
            $name = I('name');

            switch ($nametype) {
                case 0: { //用户ID
                        $uid = intval($name);
                        break;
                    }
                case 1: { //角色ID
                        $uid = D('Users')->get_uid('char_id', intval($name));
                        break;
                    }
                case 2: { //手机号
                        $uid = D('Users')->get_uid('mobile', $name);
                        break;
                    }
                case 3: { //玩家昵称
                        $uid = D('Users')->get_uid('nickname', $name);
                        break;
                    }
            }

            $userinfo = D('Userinfo')->where(array('uid' => $uid))->find();

            if (!$userinfo) {
                $this->error('此玩家不存在');
            }

            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:kick_out_player';
            $post_data['sign'] = '';
            $post_data['roleid'] = $uid;

            $field = http_build_query($post_data);
            $ret = PostUrl($post_url, $field);
            $arrs = json_decode($ret, true);

            \Think\Log::write('TUSER_URL: ' . $post_url . '?' . $field, 'INFO');

            if ($arrs['ret'] == 0) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败：' . $arrs['msg']);
            }
        } else {
            $this->display();
        }
    }

    /**
     * 百人牛牛控制概率
     */
    public function broperate()
    {
        if (IS_POST) {
            $data['keyid'] = intval(I('keyid'));
            $data['num1'] = intval(I('num1'));
            $data['num2'] = intval(I('num2'));
            $data['num3'] = intval(I('num3'));
            $data['num4'] = intval(I('num4'));
            $data['num5'] = intval(I('num5'));

            if (!$data['keyid']) {
                $this->error('请选择庄家');
            }

            $data['num1'] = $data['num1'] < 1 ? 1 : $data['num1'];
            $data['num2'] = $data['num2'] < 1 ? 1 : $data['num2'];
            $data['num3'] = $data['num3'] < 1 ? 1 : $data['num3'];
            $data['num4'] = $data['num4'] < 1 ? 1 : $data['num4'];
            $data['num5'] = $data['num5'] < 1 ? 1 : $data['num5'];

            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:update_hundred_change_cards_config';
            $data['sign'] = '';
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field, 0);
            $arrs = json_decode($ret, true);

            \Think\Log::write('BRNN_OPERATE_URL: ' . $post_url . '?' . $field, 'INFO');

            $result['keyid'] = $data['keyid'];
            unset($data['keyid'], $data['sign']);
            $result['content'] = json_encode($data);
            $result['uid'] = is_login();
            $result['create_time'] = NOW_TIME;
            $result['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $result['url'] = $post_url . '?' . $field;

            if ($arrs['ret'] == 0) {
                D('Brnnoperate')->add($result);
                $this->success('操作成功', U('Operate/broperatelogs'));
            } else {
                $this->error('操作失败：' . $arrs['msg']);
            }
        } else {
            $this->display();
        }
    }

    public function broperatelogs($p = 1)
    {
        $limit = 20;

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['create_time'] = array('egt', $b_time);
        }

        $totals = D('Brnnoperate')->where($map)->count();
        $list = D('Brnnoperate')->where($map)->page($p, $limit)->order('create_time desc')->select();

        /*foreach ($list as $k=>$v){
        $list[$k]['content'] = implode(',', json_decode($v['content'], true));
        }*/

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 增加百人水池
     */
    public function changebrpool()
    {
        if (IS_POST) {
            $data['goldnum'] = intval(I('goldnum'));

            if (!$data['goldnum']) {
                $this->error('水池不能为空');
            }

            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:add_hundred_pool';
            $data['sign'] = '';
            $data['roomid'] = '1';
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field, 0);
            $arrs = json_decode($ret, true);

            \Think\Log::write('BRNN_POOL_CHANGE_URL: ' . $post_url . '?' . $field, 'INFO');

            $result['goldnum'] = $data['goldnum'];
            $result['uid'] = is_login();
            $result['create_time'] = NOW_TIME;
            $result['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $result['url'] = $post_url . '?' . $field;

            if ($arrs['ret'] == 0) {
                D('Brpoolchange')->add($result);
                $this->success('操作成功', U('Operate/changebrpoollog'));
            } else {
                $this->error('操作失败：' . $arrs['msg']);
            }
        } else {
            $this->display();
        }
    }

    /**
     * 修改百人特殊水池日志
     */
    public function changebrpoollog($p = 1)
    {
        $limit = 20;

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['create_time'] = array('egt', $b_time);
        }

        $totals = D('Brpoolchange')->where($map)->count();
        $list = D('Brpoolchange')->where($map)->page($p, $limit)->order('create_time desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 增加超级水果水池
     */
    public function changecjsgpool()
    {
        if (IS_POST) {
            $data['num_pool'] = intval(I('goldnum'));
            $data['num_rank'] = intval(I('ranknum'));
            $data['is_super'] = intval(I('test_type'));

            if (!$data['num_pool']) $this->error('水池不能为空');

            $post_url = C('GM1_URL') . 'cgi-bin/gm_oprate:add_super_laba_pool';
            $data['sign'] = '';
            $field = http_build_query($data);

            $ret = PostUrl($post_url, $field, 0);
            $arrs = json_decode($ret, true);

            \Think\Log::write('BRNN_POOL_CHANGE_URL: ' . $post_url . '?' . $field, 'INFO');

            $result['goldnum'] = $data['num'];
            $result['uid'] = is_login();
            $result['create_time'] = NOW_TIME;
            $result['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $result['url'] = $post_url . '?' . $field;

            if ($arrs['ret'] == 0) {
                D('Cjsgpoolchange')->add($result);
                $this->success('操作成功', U('Operate/changecjsgpoollog'));
            } else {
                $this->error('操作失败：' . $arrs['msg']);
            }
        } else {
            $this->display();
        }
    }
    /**
     * 增加水果狂欢水池
     */
    public function changesgkhpool()
    {
        if (IS_POST) {
            $data['num'] = intval(I('goldnum'));

            if (!$data['num']) {
                $this->error('水池不能为空');
            }

            $post_url = C('GM1_URL') . 'cgi-bin/gm_oprate:add_super_laba_pool';
            $data['sign'] = '';
            $data['is_super'] = 0; //20180806普通水果
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field, 0);
            $arrs = json_decode($ret, true);

            \Think\Log::write('BRNN_POOL_CHANGE_URL: ' . $post_url . '?' . $field, 'INFO');

            $result['goldnum'] = $data['num'];
            $result['uid'] = is_login();
            $result['create_time'] = NOW_TIME;
            $result['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $result['url'] = $post_url . '?' . $field;
            $result['issuper'] = '1';
            if ($arrs['ret'] == 0) {
                D('Cjsgpoolchange')->add($result);
                $this->success('操作成功', U('Operate/changesgkhpoollog'));
            } else {
                $this->error('操作失败：' . $arrs['msg']);
            }
        } else {
            $this->display();
        }
    }
    /**
     * 增加超级水果水池日志
     */
    public function changecjsgpoollog($p = 1)
    {
        $limit = 20;

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['create_time'] = array('egt', $b_time);
        }
        //$map['issuper'] = '0';
        $totals = D('Cjsgpoolchange')->where($map)->count();
        $list = D('Cjsgpoolchange')->where($map)->page($p, $limit)->order('create_time desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->display();
    }
    /**
     * 增加超级水果水池日志
     */
    public function changesgkhpoollog($p = 1)
    {
        $limit = 20;

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['create_time'] = array('egt', $b_time);
        }
        $map['issuper'] = '1';
        $totals = D('Cjsgpoolchange')->where($map)->count();
        $list = D('Cjsgpoolchange')->where($map)->page($p, $limit)->order('create_time desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->display();
    }
    /**
     * 修改红包库存
     */
    public function redenvelopes()
    {
        if (IS_POST) {
            $data['id'] = intval(I('id'));
            $data['num'] = intval(I('num'));

            if (!$data['id']) {
                $this->error('请选择红包类型');
            }

            if (!$data['num']) {
                $this->error('库存不能为空');
            }

            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:add_storage';
            $data['sign'] = '';
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field, 0);
            $arrs = json_decode($ret, true);

            \Think\Log::write('RED_ENVELOPES_URL: ' . $post_url . '?' . $field, 'INFO');

            $result['goods_id'] = $data['id'];
            $result['num'] = $data['num'];
            $result['uid'] = is_login();
            $result['create_time'] = NOW_TIME;
            $result['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $result['url'] = $post_url . '?' . $field;

            if ($arrs['ret'] == 0) {
                D('Redenvelopes')->add($result);
                $this->success('操作成功', U('Operate/redenvelopelogs'));
            } else {
                $this->error('操作失败：' . $arrs['msg']);
            }
        } else {
            $config = D('ConfigByshop')->getConfig(); //商城数据表
            $redConfig = array();
            foreach ($config as $k => $v) {
                if ($v['cost_type'] == 1 && $k != '99017') {
                    $redConfig[$k]['name'] = $v['name'];
                }
            }
            $this->assign('redconfig', $redConfig);
            $this->display();
        }
    }

    /**
     * 修改红包库存日志
     */
    public function redenvelopelogs($p = 1)
    {
        $limit = 20;

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['create_time'] = array('egt', $b_time);
        }

        $totals = D('Redenvelopes')->where($map)->count();
        $list = D('Redenvelopes')->where($map)->page($p, $limit)->order('create_time desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('config', D('ConfigByshop')->getConfig());
        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 渠道任务开启
     */
    public function qudaotasks_bak()
    {
        if (IS_POST) {
            $switchs = (array) I('switchs');
            if ($switchs) {
                asort($switchs);
                $switchs = array_values($switchs);
            }
            $taskid = intval(I('taskid'));
            if (!$taskid) {
                $this->error('请选择任务');
            }

            $send = '';
            if ($switchs[0] == '') {
                $send = '';
            } else {
                foreach ($switchs as $k => $val) {
                    if (I('switchsold_' . $val) != '') {
                        $str[intval($val)] = 1;
                        $send .= $val . ',1;';
                    } else {
                        $str[intval($val)] = 0;
                        $send .= $val . ',0;';
                    }
                }
            }

            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:seven_day_carnival';
            $post_data['sign'] = '';
            $post_data['str'] = $send;
            $post_data['id'] = $taskid;
            $field = http_build_query($post_data);
            $ret = PostUrl($post_url, $field, 0);
            $arrs = json_decode($ret, true);

            $data['module'] = $taskid;
            $data['open'] = json_encode($switchs);
            $data['old_reg'] = json_encode($str);
            $data['uid'] = is_login();
            $data['create_time'] = NOW_TIME;
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $data['posturl'] = $post_url . '?' . $field;

            if ($arrs['ret'] == 0) {
                if (D('Qudaotasklogs')->add($data)) {
                    $this->success('操作成功');
                } else {
                    $this->error('记录失败');
                }
            } else {
                $this->error('操作失败：' . $ret['msg']);
            }
        } else {

            if (isset($_GET['task'])) {
                $taskid = intval(I('task'));
                $log = D('Qudaotasklogs')->where(array('module' => $taskid))->order('create_time desc')->find();
                if ($log['open'] != '[""]') {
                    $inTask = json_decode($log['open'], true);
                    $oldReg = json_decode($log['old_reg'], true);
                }
            }

            $tasks = D('Qudaotasks')->getField('taskid,taskname', true);
            $froms = D('DChannels')->where(array('from_id' => array('neq', 999)))->getField('from_id,from_name,create_time', true);
            foreach ($froms as $k => $v) {
                if (in_array($k, $inTask)) {
                    $froms[$k]['select'] = 1;
                    //老注册用户
                    if ($oldReg[$k] === 1) {
                        $froms[$k]['old_reg'] = 1;
                    }
                }
            }
            $this->assign('_list', $froms);
            $this->assign('tasks', $tasks);
            $this->display();
        }
    }

    //服务端红包接口查询
    // public function redpacketcheck()
    // {
    //     if (IS_POST) {
    //         $nametype = intval(I('post.nametype'));
    //         $name = I('post.name');
    //         switch ($nametype) {
    //             case 0: //用户ID
    //                 $char_id = D('Users')->get_charid('uid', $name);
    //                 break;
    //             case 1: //角色ID
    //                 $char_id = intval($name);
    //                 break;
    //             case 2: //手机号
    //                 $char_id = D('Users')->get_charid('mobile', $name);
    //                 break;
    //             case 3: //玩家昵称
    //                 $char_id = D('Users')->get_charid('nickname', trim($name));
    //                 break;
    //         }
    //         if (!$char_id) {
    //             $this->error('玩家不存在！');
    //         }

    //         $btime = I('post.b_time');
    //         if (!$btime) {
    //             $this->error('请选择查询时间！');
    //         }

    //         $time = strtotime($btime);

    //         $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:player_exchange_record';
    //         $postData['sign'] = '';
    //         $postData['id'] = $char_id;
    //         $postData['time'] = $time;
    //         $field = http_build_query($postData);
    //         $ret = PostUrl($post_url, $field, 0);
    //         $arrs = json_decode($ret, true);

    //         if ($arrs['ret'] == 0) {
    //             if (!$arrs['msg']) {
    //                 $this->error('此时间玩家没有兑换记录！');
    //             }

    //             //查询shop表处理丢单日志
    //             $userInfo = D('Userinfo')->where(array('char_id' => intval($char_id)))->find();
    //             if (!$uid = $userInfo['uid']) {
    //                 $this->error('无此对应的用户ID,更新失败!');
    //             }

    //             $shopModel = D('Shops');
    //             $logs = explode(';', rtrim($arrs['msg'], ';'));
    //             $operate = 0;
    //             foreach ($logs as $log) {
    //                 $detail = $add = array();
    //                 $write = '';
    //                 $detail = explode(',', $log);
    //                 $checkLog = $shopModel->where(array('uid' => $uid, 'ordersn' => $detail[0], 'goods_id' => $detail[2]))->find();
    //                 if (!$checkLog) {
    //                     $add['uid'] = $uid;
    //                     $add['truename'] = urlencode($userInfo['nickname']);
    //                     $add['ordersn'] = $detail[0];
    //                     $add['goods_id'] = $detail[2];
    //                     $add['num'] = 1;
    //                     $add['c_time'] = $detail[5];
    //                     $add['type'] = $detail[1];
    //                     if ($insertId = $shopModel->add($add)) {
    //                         $operate++;
    //                         $write = 'UID:' . $uid . ',SHOPID:' . $insertId . ',' . $log . ';';
    //                         writeLog($write, 'INFO', 'redpacketcheck');
    //                     } else {
    //                         $this->error('更新失败！');
    //                     }
    //                 }
    //             }
    //             $this->success('操作成功,更新了 ' . $operate . ' 条记录！');
    //         } else {
    //             $this->error('查询失败！');
    //         }
    //     } else {
    //         $this->display();
    //     }
    // }

    public function redpacketcheck()
    {
        $nametype = intval(I('nametype'));
        $name = I('name');
        $paytype = I('paytype');
        if ($name) {

            switch ($nametype) {
                case 0: //用户ID
                    $char_id = D('Users')->get_charid('uid', intval($name));
                    break;
                case 1: //角色ID
                    $char_id = intval($name);
                    break;
                case 2: //手机号
                    $char_id = D('Users')->get_charid('mobile', $name);
                    break;
                case 3: //玩家昵称
                    $char_id = D('Users')->get_charid('nickname', trim($name));
                    break;
            }
            if (!$char_id) {
                $this->error('玩家不存在！');
            }


            $user = D('Userinfo')->where(['char_id' =>  $char_id]) ->find(); //

            $uid = $user['uid'];
            if(empty($uid)) {
                $this->error('玩家不存在！');
            }
            $btime = I('b_time');

            $redpacket_logs = D('redpacket_logs');
            if($btime) {
                $stime = strtotime($btime);
                $etime = strtotime($btime) + 86399;
                $map['create_time'] = ['BETWEEN', [$stime, $etime]];

            }
            if($paytype) {
                $map['paytype'] = $paytype;
            }
            $map['uid'] = $uid;
            $list = $redpacket_logs->where($map)->select();

            // var_dump($redpacket_logs->getLastSql());

        }
        $this->assign('paytype', $paytype);
        $this->assign('nametype', $nametype);
        $this->assign('_list', $list);
        $this->display();

    }

    protected function send2Server($key, $send, $url, $testtype = 0)
    {
        if ($key >= 100 && $key <= 300) {
            $sendKey = floor($key / 100);
        } else {
            $sendKey = $key;
        }
        $sendIndex = $key % 100;
        $post_data = [];
        $post_data['key'] = intval($sendKey);
        $post_data['index'] = intval($sendIndex);
        $post_data['set'] = json_encode($send);
        $post_data['testtype'] = $testtype;

        $url .= '?';
        foreach ($post_data as $key => $val) {
            $url .= "{$key}={$val}&";
        }
        $url = rtrim($url, '&');
        // var_dump($url);exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $ret = curl_exec($ch);
        curl_close($ch);
        $retArr = json_decode($ret, true);
        $retArr['_url'] = $url;
        return $retArr;
    }

    public function hundredniu_opt($tableid = 0)
    {
        $mapArr = $this->getCommonQuery(
            [],
            [],
            ['tableid' => ['tableid', 'hundredniu_rate', true], 'type' => ['type', 0, true]]
        );

        $tables = C('HUNDREDNIU_OPT_TABLE_FOR_EXCEL');
        $tableid = $mapArr['map']['tableid'];
        $type = intval($mapArr['map']['type']);

        $table = $tables[$tableid];
        $data = excelTable($table[0], $table[1]);

        $uri = $table[3] ?: 'change_hundred_niu_rate';
        $url = C('GM_URL') . 'cgi-bin/gm_oprate:' . $uri;

        if (IS_POST) {
            $list = I('post.list');
            if (!$list) {
                $this->error('配置不能为空！');
            }
            $temp = $list;

            $send = [];
            foreach ($list as $key => $val) {
                array_push($send, (object) $val);
            }

            $ret = $this->send2Server($tableid, $send, $url);
            $retArr = json_decode($ret, true);
            if ((string) $retArr['ret'] != '0') {
                $this->error('修改失败：' . $ret['msg'] . '-' . $ret['ret']);
            }

            $log = [];
            $log['key'] = $tableid;
            $log['set'] = $temp;
            $log['c_time'] = NOW_TIME;
            $log['do_uid'] = intval(is_login());
            D('tablesSettingLogs')->add($log);

            $this->success('修改成功！');
        } else {
            //排序问题
            $log = D('tablesSettingLogs')->where(['key' => $tableid])->order('c_time desc')->select();
            $log = array_values($log)[0];
            if ($type == 0) {
                $data['data'] = $log['set'];
            }

            $this->assign('tables', $tables);
            $this->assign('log', $log);
            $this->assign('tableid', $tableid);
            $this->assign('data', $data);

            $display = '';
            $this->display($display);
        }
    }

    // 2019.12.19
    // 获取机战水池信息
    public function airlabauserpool()
    {
        $url = C('GM_URL') . 'cgi-bin/gm_airlaba_opt:';
        if (IS_POST) {
            //
        } else {
            // 获取玩家水池信息
            if (!I('nickname') && !I('uid')) {
                $this->assign('result_code', 'SUCCESS');
                $this->assign('_list', array());
                $this->display();
                return;
            }

            $user_id = I('uid');
            $user_id = str_replace('.html', '', $user_id);
            $user_id = intval($user_id);
            $info = D('Userinfo')->where(array('uid' => $user_id))->field('char_id,nickname')->find();
            $uid = $info['char_id'];
            if (!$uid) {
                $retArr = array(
                    'ret' => 1,
                    'msg' => '未找到角色id',
                );
                $this->assign('result_code', 'FAIL');
                $this->assign('_list', [$retArr]);
                $this->display();
                return;
            }

            $url = $url . 'get_user_pool?uid=' . $uid;
            $post_data['uid'] = intval($uid);

            //生成签名
            ksort($post_data);
            $keys_arr = array_keys($post_data);
            $sign = implode($keys_arr);
            $post_data['sign'] = $sign;

            $field = http_build_query($post_data);
            $ret = file_get_contents($url);

            $retArr = json_decode($ret, true);
            $retArr['_url'] = $url;
            if ($retArr['ret'] == 2) {
                $this->assign('result_code', 'FAIL');
                $this->assign('_list', [$retArr]);
                $this->display();
            } else if ($retArr['ret'] == 0) {
                //
                $bet_pool = 0;
                $hit_rate_adj = 0;
                $msg = explode('&', $retArr['msg']);
                foreach ($msg as $v) {
                    $arr = (explode('=', $v));
                    if ($arr[0] == 'bet_pool_1') {
                        $bet_pool1 = $arr[1];
                    } else if ($arr[0] == 'bet_pool_2') {
                        $bet_pool2 = $arr[1];
                    } else if ($arr[0] == 'hit_rate_adj') {
                        $hit_rate_adj = $arr[1];
                    }
                }

                $hit_rate_adj = number_format($hit_rate_adj, 4);
                $newArr = array(
                    'uid' => $user_id,
                    'char_id' => $info['char_id'] ? $info['char_id'] : 100110,
                    'nickname' => $info['nickname'] ? $info['nickname'] : '昵称_测试',
                    'bet_pool1' => $bet_pool1 ? $bet_pool1 : 0,
                    'bet_pool2' => $bet_pool2 ? $bet_pool2 : 0,
                    'hit_rate_adj' => $hit_rate_adj,
                );
                $this->assign('uid', $user_id);
                $this->assign('result_code', 'SUCCESS');
                $this->assign('_list', [$newArr]);
                $this->display();
            }
        }
    }

    /**
     * 编辑个人机战水池
     */
    public function editairlabauserpool($uid, $charId, $nickname, $betPool1, $betPool2, $hitRateAdj)
    {
        $uid = intval($uid);
        $char_id = intval($charId);
        $nickname = $nickname;
        $bet_pool1 = intval($betPool1);
        $bet_pool2 = intval($betPool2);
        $hit_rate_adj = $hitRateAdj; //需要是浮点型

        if (!$char_id) {
            $this->error('玩家角色id错误');
        }

        if (IS_POST) {
            $data = I('data');
            $list = array();
            $list['nickname'] = trim($data['nickname']);
            $list['uid'] = intval($data['uid']);
            $list['char_id'] = intval($data['char_id']);

            $data['bet_pool1'] = trim($data['bet_pool1']);
            $data['bet_pool2'] = trim($data['bet_pool2']);
            $data['hit_rate_adj'] = trim($data['hit_rate_adj']);

            if ($list['char_id'] == 0) {
                $this->error('玩家角色id错误');
            }

            if (is_numeric($data['bet_pool1'])) {
                $list['bet_pool1'] = intval($data['bet_pool1']);
            } else {
                $this->error('个人当前试玩水池需要输入数字');
            }

            if (is_numeric($data['bet_pool2'])) {
                $list['bet_pool2'] = intval($data['bet_pool2']);
            } else {
                $this->error('个人当前试玩水池需要输入数字');
            }

            $list['hit_rate_adj'] = $data['hit_rate_adj'];
            if (strpos($list['hit_rate_adj'], '.') == false) {
                $list['hit_rate_adj'] = $list['hit_rate_adj'] . ".0";
            }

            if (is_numeric($list['hit_rate_adj'])) {
                // 发送到游戏服务器
                $url = C('GM_URL') . 'cgi-bin/gm_airlaba_opt:';
                $url = $url . 'update_user_pool?uid=' . $char_id . "&bet_pool_1=" . $list['bet_pool1'] . "&bet_pool_2=" . $list['bet_pool2'] . '&hit_rate_adj=' . $list['hit_rate_adj'];
                $ret = file_get_contents($url);
                $retArr = json_decode($ret, true);
                $retArr['_url'] = $url;
                if ($retArr['ret'] == 0) {
                    $this->success('更新成功', U('airlabauserpool&uid=' . $uid . "&air=html"));
                } else {
                    $this->error('更新失败');
                }
            } else {
                //
                $this->error('击中战机的概率需要输入数字');
            }
        } else {
            $arr = array(
                'uid' => $uid,
                'char_id' => $char_id,
                'nickname' => $nickname,
                'bet_pool1' => $bet_pool1,
                'bet_pool2' => $bet_pool2,
                'hit_rate_adj' => $hit_rate_adj,
            );
            $this->assign('goods', $arr);
            $this->display();
        }
    }

    /**
     * 机战设置奖池信息
     */
    public function updateAirlabapool()
    {
        if (IS_POST) {
            $data = I('data');
            $list = array();

            $data['add_pool_num'] = trim($data['add_pool_num']);
            $data['add_rank_pool_num'] = trim($data['add_rank_pool_num']);

            if (is_numeric($data['add_pool_num'])) {
                $list['add_pool_num'] = intval($data['add_pool_num']);
            } else {
                $this->error('新增奖池数量需要输入数字');
            }

            if (is_numeric($data['add_rank_pool_num'])) {
                $list['add_rank_pool_num'] = intval($data['add_rank_pool_num']);
            } else {
                $this->error('新增排行奖池数量需要输入数字');
            }

            $where = array(
                'test_type' => 1,
                'game_id' => 'air',
            );
            $updateDate = array(
                'add_pool_num' => intval($data['add_pool_num']),
                'add_rank_pool_num' => intval($data['add_rank_pool_num']),
            );
            $result = D('GameConfig')->where($where)->save($updateDate);

            if (!!$result) {
                // 发送到游戏服务器
                $url = C('GM_URL') . 'cgi-bin/gm_airlaba_opt:';
                $url = $url . 'update_airlaba_pool?add_pool_num=' . $list['add_pool_num'] . "&add_rank_pool_num=" . $list['add_rank_pool_num'];
                $ret = file_get_contents($url);
                $retArr = json_decode($ret, true);
                $retArr['_url'] = $url;
                if ($retArr['ret'] == 0) {
                    $this->success('更新成功', U('ManageUsers/airlabapool'));
                } else {
                    $this->error('更新失败');
                }
            } else {
                $this->error('updateAirlabapool mysql gameConfig更新失败');
            }
        } else {
            $testType = I('testType');
            $where = array(
                'test_type' => 1,
                'game_id' => 'air',
            );
            $getData = D('GameConfig')->where($where)->find();
            $arr = array(
                'add_pool_num' => $getData['add_pool_num'] ? $getData['add_pool_num'] : 0,
                'add_rank_pool_num' => $getData['add_rank_pool_num'] ? $getData['add_rank_pool_num'] : 0,
            );
            $this->assign('resData', $arr);
            $this->display();
        }
    }

    /**
     * 水池控制设置
     */
    public function updateAirlabaAbPoolConfig()
    {
        if (IS_POST) {
            $data = I('data');
            $list = array();

            $data['personal_rate'] = trim($data['personal_rate']);
            $data['common_rate'] = trim($data['common_rate']);
            $data['common_rate_share'] = trim($data['common_rate_share']);

            if (is_numeric($data['personal_rate'])) {
                $list['personal_rate'] = floatval($data['personal_rate']);
            } else {
                $this->error('个人水池比例需要输入数字');
            }

            if (is_numeric($data['common_rate'])) {
                $list['common_rate'] = floatval($data['common_rate']);
            } else {
                $this->error('公共水池比例需要输入数字');
            }

            if (is_numeric($data['common_rate_share'])) {
                $list['common_rate_share'] = floatval($data['common_rate_share']);
            } else {
                $this->error('共享水池比例需要输入数字');
            }

            if ($list['personal_rate'] + $list['common_rate'] + $list['common_rate_share'] > 1) {
                $this->error('三个比例的和不能大于 1');
            }

            $test_type = intval(I('test_type'));
            if (!!$test_type) {
                $where = array(
                    'test_type' => $test_type,
                    'game_id' => 'air',
                );
                $updateDate = array(
                    'personal_rate' => floatval($data['personal_rate']),
                    'common_rate' => floatval($data['common_rate']),
                    'common_rate_share' => floatval($data['common_rate_share']),
                );
                $result = D('GameConfig')->where($where)->save($updateDate);
                if (!!$result) {
                    // 发送到游戏服务器
                    $url = C('GM_URL') . 'cgi-bin/gm_airlaba_opt:';
                    $url = $url . 'update_airlaba_ab_pool_config?personal_rate=' . $list['personal_rate'] . "&common_rate=" . $list['common_rate'] . "&common_rate_share=" . $list['common_rate_share'] . "&test_type=" . $test_type;
                    $ret = file_get_contents($url);
                    $retArr = json_decode($ret, true);
                    $retArr['_url'] = $url;
                    if ($retArr['ret'] == 0) {
                        $this->success('更新成功', U('ManageUsers/airlabapool'));
                    } else {
                        $this->error('更新失败');
                    }
                } else {
                    $this->error('updateAirlabaAbPoolConfig mysql gameConfig更新失败');
                }
            } else {
                $this->error('更新失败 403');
            }
        } else {
            $testType = I('testType');
            $where = array(
                'game_id' => 'air',
            );
            $getData = D('GameConfig')->where($where)->select();
            foreach ($getData as $v) {
                if ($v['test_type'] == 1) {
                    $arr_1 = array(
                        'personal_rate' => $v['personal_rate'] ? $v['personal_rate'] : 0,
                        'common_rate' => $v['common_rate'] ? $v['common_rate'] : 0,
                        'common_rate_share' => $v['common_rate_share'] ? $v['common_rate_share'] : 0,
                    );
                } else if ($v['test_type'] == 2) {
                    $arr_2 = array(
                        'personal_rate' => $v['personal_rate'] ? $v['personal_rate'] : 0,
                        'common_rate' => $v['common_rate'] ? $v['common_rate'] : 0,
                        'common_rate_share' => $v['common_rate_share'] ? $v['common_rate_share'] : 0,
                    );
                }
            }

            $this->assign('resData1', $arr_1);
            $this->assign('resData2', $arr_2);
            $this->assign('testType', intval($testType));
            $this->display();
        }
    }
    /**
     * 税收设置
     */
    public function updateAirlabaConstConfig()
    {
        if (IS_POST) {
            $data = I('data');
            $list = array();

            $data['sys_rate'] = trim($data['sys_rate']);
            $data['pool_rate'] = trim($data['pool_rate']);
            $data['rank_rate'] = trim($data['rank_rate']);

            if (is_numeric($data['sys_rate'])) {
                $list['sys_rate'] = floatval($data['sys_rate']);
            } else {
                $this->error('系统税需要输入数字');
            }

            if (is_numeric($data['pool_rate'])) {
                $list['pool_rate'] = floatval($data['pool_rate']);
            } else {
                $this->error('奖池税需要输入数字');
            }

            if (is_numeric($data['rank_rate'])) {
                $list['rank_rate'] = floatval($data['rank_rate']);
            } else {
                $this->error('排行榜税需要输入数字');
            }

            if ($list['sys_rate'] + $list['pool_rate'] + $list['rank_rate'] > 1) {
                $this->error('三个比例的和不能大于 1');
            }

            $test_type = intval(I('test_type'));
            if (!!$test_type) {
                $where = array(
                    'test_type' => $test_type,
                    'game_id' => 'air',
                );
                $updateDate = array(
                    'sys_rate' => floatval($data['sys_rate']),
                    'pool_rate' => floatval($data['pool_rate']),
                    'rank_rate' => floatval($data['rank_rate']),
                );
                $result = D('GameConfig')->where($where)->save($updateDate);
                if (!!$result) {
                    // 发送到游戏服务器
                    $url = C('GM_URL') . 'cgi-bin/gm_airlaba_opt:';
                    $url = $url . 'update_airlaba_const_config?sys_rate=' . $list['sys_rate'] . "&pool_rate=" . $list['pool_rate'] . "&rank_rate=" . $list['rank_rate'] . "&test_type=" . $test_type;
                    $ret = file_get_contents($url);
                    $retArr = json_decode($ret, true);
                    $retArr['_url'] = $url;
                    if ($retArr['ret'] == 0) {
                        $this->success('更新成功', U('ManageUsers/airlabapool'));
                    } else {
                        $this->error('更新失败');
                    }
                } else {
                    $this->error('updateAirlabaConstConfig mysql gameConfig 更新失败');
                }
            } else {
                $this->error('更新失败 403, test_type error');
            }
        } else {
            $testType = I('testType');
            $where = array(
                'game_id' => 'air',
            );
            $getData = D('GameConfig')->where($where)->select();
            foreach ($getData as $v) {
                if ($v['test_type'] == 1) {
                    $arr_1 = array(
                        'sys_rate' => $v['sys_rate'] ? $v['sys_rate'] : 0,
                        'pool_rate' => $v['pool_rate'] ? $v['pool_rate'] : 0,
                        'rank_rate' => $v['rank_rate'] ? $v['rank_rate'] : 0,
                    );
                } else if ($v['test_type'] == 2) {
                    $arr_2 = array(
                        'sys_rate' => $v['sys_rate'] ? $v['sys_rate'] : 0,
                        'pool_rate' => $v['pool_rate'] ? $v['pool_rate'] : 0,
                        'rank_rate' => $v['rank_rate'] ? $v['rank_rate'] : 0,
                    );
                }
            }
            $this->assign('resData1', $arr_1);
            $this->assign('resData2', $arr_2);
            $this->assign('testType', intval($testType));
            $this->display();
        }
    }

    public function userredbaginfo()
    {
        $resData = array();

        $user_id = I('uid');
        $user_id = str_replace('.html', '', $user_id);
        $user_id = intval($user_id);
        $info = D('Userinfo')->where(array('uid' => $user_id))->field('char_id,nickname')->find();
        $fromstat = D('fromstat')->where(array('uid' => $user_id))->field('uid,from_id,money,money_s')->order('from_id asc')->select();
        foreach ($fromstat as $v) {
            $where['uid'] = intval($v['uid']);
            $where['from_id'] = intval($v['from_id']);
            $getList = D('Redbaglog')->where($where)->select();
            $_164 = 0;
            $_165 = 0;
            $_103 = 0;
            $_153 = 0;
            $_113 = 0;
            // 红包兑换成金币153（负数），红包兑换成人民币113（负数）

            foreach ($getList as $k => $redbag) {
                if ($redbag['room'] == 164) {
                    // 红包场
                    $_164 += $redbag['money'];
                } else if ($redbag['room'] == 165) {
                    // 娱乐场
                    $_165 += $redbag['money'];
                } else if ($redbag['room'] == 103) {
                    //任务奖励
                    $_103 += $redbag['money'];
                } else if ($redbag['room'] == 153) {
                    //任务奖励
                    $_153 += $redbag['money'];
                } else if ($redbag['room'] == 113) {
                    //任务奖励
                    $_113 += $redbag['money'];
                }
            }

            $from_name = D('DChannels')->where(array('id' => intval($v['from_id'])))->getField('from_name');

            $resData[] = array(
                'uid' => $user_id,
                'char_id' => $info['char_id'],
                'nickname' => $info['nickname'],
                'from_id' => intval($v['from_id']),
                'from_name' =>  $from_name,
                'redbag_name' => '充值(不包含首充)',
                'redbag_num' => intval($v['money']) - intval($v['money_s']),
            );

            $resData[] = array(
                'uid' => $user_id,
                'char_id' => $info['char_id'],
                'nickname' => $info['nickname'],
                'from_id' => intval($v['from_id']),
                'from_name' =>  $from_name,
                'redbag_name' => '充值(包含首充)',
                'redbag_num' => intval($v['money']),
            );


            $resData[] = array(
                'uid' => $user_id,
                'char_id' => $info['char_id'],
                'nickname' => $info['nickname'],
                'from_id' => intval($v['from_id']),
                'from_name' =>  $from_name,
                'redbag_name' => '红包场',
                'redbag_num' => $_164 / 10
            );

            $resData[] = array(
                'uid' => $user_id,
                'char_id' => $info['char_id'],
                'nickname' => $info['nickname'],
                'from_id' => intval($v['from_id']),
                'from_name' =>  $from_name,
                'redbag_name' => '娱乐场',
                'redbag_num' => $_165 / 10
            );

            $resData[] = array(
                'uid' => $user_id,
                'char_id' => $info['char_id'],
                'nickname' => $info['nickname'],
                'from_id' => intval($v['from_id']),
                'from_name' =>  $from_name,
                'redbag_name' => '任务奖励',
                'redbag_num' => $_103 / 10
            );

            $resData[] = array(
                'uid' => $user_id,
                'char_id' => $info['char_id'],
                'nickname' => $info['nickname'],
                'from_id' => intval($v['from_id']),
                'from_name' =>  $from_name,
                'redbag_name' => '红包兑换成金币',
                'redbag_num' => $_153 / 10
            );

            $resData[] = array(
                'uid' => $user_id,
                'char_id' => $info['char_id'],
                'nickname' => $info['nickname'],
                'from_id' => intval($v['from_id']),
                'from_name' =>  $from_name,
                'redbag_name' => '兑换成现金',
                'redbag_num' => $_113 / 10
            );
        }

        $this->assign('uid', $user_id);
        $this->assign('_list', $resData);
        $this->display();
    }

    public function redpacketexchange()
    {
        $nametype = intval(I('nametype'));
        $name = I('name');
        $paytype = I('paytype');


        // switch ($nametype) {
        //     case 0: //用户ID
        //         $char_id = D('Users')->get_charid('uid', intval($name));
        //         break;
        //     case 1: //角色ID
        //         $char_id = intval($name);
        //         break;
        //     case 2: //手机号
        //         $char_id = D('Users')->get_charid('mobile', $name);
        //         break;
        //     case 3: //玩家昵称
        //         $char_id = D('Users')->get_charid('nickname', trim($name));
        //         break;
        // }
        // if (!$char_id) {
        //     $this->error('玩家不存在！');
        // }


        // $user = D('Userinfo')->where(['char_id' =>  $char_id]) ->find(); //

        // $uid = $user['uid'];
        // if(empty($uid)) {
        //     $this->error('玩家不存在！');
        // }
        $btime = I('b_time');

        $redpacket_error = D('Redpacket_error');

        if($btime) {
            $stime = strtotime($btime);
            $etime = strtotime($btime) + 86399;
            $map['create_time'] = ['BETWEEN', [$stime, $etime]];
        }
        if($paytype) {
            $map['paytype'] = $paytype;
        }
        if ($name) {
            $map['uid'] = $name;
         }
        $list = $redpacket_error->where($map)->order('ctime desc')->limit(30)->select();

        // var_dump($redpacket_logs->getLastSql());

        $this->assign('paytype', $paytype);
        $this->assign('nametype', $nametype);
        $this->assign('_list', $list);
        $this->display();

    }

    //获取列表
    public function wxconfig()
    {
        $model = D('WxConfig');
        $list = $model->select();

        $this->assign('_list', $list);
        $this->display();
    }

    //打开或者关闭设置
    public function setWxConifg()
    {
        $id = I('id');
        $method = I('method');

        $model = D("WxConfig");
        $model->where(['id' => ['neq', $id]])->save(['is_open' => 0]);
        $model->where(['id' => $id])->save(['is_open' => 1]);
        $this->success('操作成功');
    }

    public function editWxConfig()
    {
        $model = D("WxConfig");

        if(IS_POST) {

            $id = I('post.id');
            $data['name'] = I('name');
            $data['key'] = I('key');
            $data['appid'] = I('appid');
            $data['secret'] = I('secret');
            $data['mch_id'] = I('mch_id');

            $model->where(['id' => $id])->save($data);
            $this->success('操作成功');

        } else {
            $id = I('id');

            $list = $model->where(['id' => $id])->find();
            $this->assign('info', $list);
            $this->display();
        }
    }

    //红包补发列表
    public function redReissue()
    {
        $p = I('p');
        $limit = 200;
        $redpackages = D('DRedPackages');
        $b_time = I('b_time');
        $e_time = I('e_time');
        $sctype = I('sctype');
        
        if(2==$sctype )
        {
            //补红包
            $post_url = C('GMBox_URL') . 'cgi-bin/gm_oprate.redpackage_patch';
            $post_data['sign'] = md5(C('SENDGAME_KEY').time());
            $post_data['timestamp'] = intval(time());
            $post_data['limit'] = 1000; //账号ID
            $post_data['amount'] = 0;
            $post_data['account'] = 0;
            $post_data['id'] = 0;
            $post_data['flag'] = 1;
            $field = json_encode($post_data);
            $ret = PostUrl($post_url, $field,1);
            $result = json_decode($ret, true); 

            if ($result['err'] != "") 
            {
                $this->error('补发红包失败:'.$result['err']);
            }
            else{$this->success('补发红包成功');}


        }
        else
        {
            if (isset($_GET['value'])) {
                $value = trim(I('value'));
                $map['account'] = $value;
                $map['trade_no'] = $value;
                $map['from_id'] = $value;
                $map['_logic'] = 'OR';
                $rmap['_complex'] = $map;
            }
            $rmap['status'] = array('not in', array(0, 888));
            $rmap['flag'] = 0;
            $rmap['created_at'] = array('egt', date("Y-m-d H:i:s", strtotime("-30 day")));
            if(!empty(I('b_time')) && !empty(I('e_time')))
            {
                $rmap['created_at'] = array(array('between', array($b_time, $e_time)),array('egt', date("Y-m-d H:i:s", strtotime("-1 month"))));
            }
            
     
            $totals = $redpackages->where($rmap)->count();
            $list   = $redpackages->where($rmap)->page($p, $limit)->select();
    
             
    
            $pageNav = new \Think\Page($totals, $limit);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
    
            $fromkey = D('DChannels')->group('from_id')->order('id asc')->select();
     
            $this->assign('_list', $list);
            $this->assign('_page', $pageNav->show());
            $this->display();

        }

        
        
    }

    public function redReissueList()
    {
        $p = I('p');
        $limit = 50;
        $redpackages = D('DRedPackages');
        $b_time = I('b_time');
        $e_time = I('e_time');
        $sctype = I('sctype');
        $value = I('value');

		if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $rmap['d_red_packages.account'] = intval($value);
                    break;
                case 1:
                    $rmap['d_red_packages.account'] = D('DUsers')->get_uid('char_id', intval($value));
                    break;
                case 2:
                    $rmap['d_red_packages.account'] = D('DUsers')->get_uid('mobile', $value);
                    break;
                case 3:
                    $rmap['d_red_packages.account'] = D('DUsers')->get_uid('nickname', trim($value));
                    break;
            }
            $query .= 'type/' . $type . '/';
            $query .= 'value/' . $value . '/';
        }
		
        //if (isset($_GET['value'])) {
        //   $value = trim(I('value'));
        //    $map['d_red_packages.account'] = $value;
        //    $map['d_red_packages.trade_no'] = $value;
        //    $map['d_red_packages.mch_order_no'] = $value;
        //    $map['d_red_packages.ofi_trade_no'] = $value;
        //    $map['d_red_packages.from_id'] = $value;
        //    $map['_logic'] = 'OR';
        //    $rmap['_complex'] = $map;
        //}
		
		$trade_no  = trim(I('trade_no'));
        if (!isset($_GET['ordertype'])) {
            $ordertype = -1;
        } else {
            $ordertype = intval(I('ordertype'));
            switch ($ordertype) {
                case 1:
                    $rmap['d_red_packages.trade_no'] = $trade_no;
                    break;
                case 2:
                    $rmap['d_red_packages.mch_order_no'] = $trade_no;
                    break;
                case 3:
                    $rmap['d_red_packages.ofi_trade_no'] = $trade_no;
                    break;
            }
            $query .= 'ordertype/' . $ordertype . '/trade_no/' . $trade_no . '/';
        }
        if (!isset($_GET['statustype'])) {
            $statustype = -1;
        } else {
            $statustype = intval(I('statustype'));
            switch ($statustype) {
                case 1:
                    $rmap['d_red_packages.status'] = ['NEQ' , 0];;
                    break;
                case 2:
                    $rmap['d_red_packages.status'] = 0;
                    break;
            }
            $query .= 'statustype/' . $statustype. '/';
        }
        //$rmap['status'] = array('not in', array(0, 888));
        //$rmap['flag'] = 0;
       
        if(!empty(I('b_time')) && !empty(I('e_time')))
        {
            $rmap['d_red_packages.created_at'] = array(array('between', array($b_time, $e_time)),array('egt', date("Y-m-d H:i:s", strtotime("-1 month"))));
        }
        else{ $rmap['d_red_packages.created_at'] = array('egt', date("Y-m-d 00:00:00", strtotime("-30 days")));}
 
        $totals = $redpackages->where($rmap)->join('d_accounts ON d_accounts.id = d_red_packages.account', 'left')->join('game1.d_users as u ON u.account = d_accounts.id', 'left')->count();
        $list   = $redpackages->where($rmap)->join('d_accounts ON d_accounts.id = d_red_packages.account', 'left')->join('game1.d_users as u ON u.account = d_accounts.id', 'left')->field('d_red_packages.*,d_accounts.name,u.sub_from_id ')->page($p, $limit)->order('d_red_packages.created_at desc')->select();
         
		$list_count = $totals; //充值单数
        $uids = $redpackages->where($rmap)->join('d_accounts ON d_accounts.id = d_red_packages.account', 'left')->join('game1.d_users as u ON u.account = d_accounts.id', 'left')->field('count(DISTINCT(d_red_packages.account)) as num')->find();

        $pay_count  = $redpackages->where($rmap)->join('d_accounts ON d_accounts.id = d_red_packages.account', 'left')->join('game1.d_users as u ON u.account = d_accounts.id', 'left')->sum('amount/100'); //充值金额

         

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
 
		$this->assign('user_num', $uids['num']);
		$this->assign('list_count', $list_count);
        $this->assign('pay_count', $pay_count);
		
		$this->assign('ordertype', $ordertype);
		$this->assign('statustype', $statustype);
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();

        
        
    }

    //红包补发
    public function reissueRed()
    {
        $account = I('account');
        $no = I('no');
        if($account && $no) {
          
            $post_url = C('GMBox_URL') . 'cgi-bin/gm_oprate.redpackage_patch';
            $post_data['sign'] = md5(C('SENDGAME_KEY').time());
            $post_data['timestamp'] = intval(time());
            $post_data['account'] = intval($account);
            $post_data['id'] = intval($no);
            $post_data['flag'] = 1;
            $field = json_encode($post_data);
            $ret = PostUrl($post_url, $field,1);
            $result = json_decode($ret, true); 

            if ($result['err'] != "") 
            {
                $this->error('补发红包失败:'.$result['err']);
            }
            else{$this->success('补发红包成功');}

        }
        
        $this->success('redReissue');
    }
    //刷子用户
    public function brush()
    {
        $p = I('p');
        $limit = 20;
        $user = D('DAccounts');
        $onlie = I('onlie');
        $new = I('new');
        if(!empty($onlie)) {
            //实时在线
            $onlieList = D('Online')->order('c_time desc')->find();
        }

        if(!empty($new)) {
            $stime = strtotime(date("Y-m-d"));
            $etime =  strtotime(date("Y-m-d")) + 86399;
            $map['adddate'] = ['BETWEEN' , [$stime,$etime]];
        }

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map['adddate'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['adddate'][] = array('elt', $e_time);
        }

        $bind = intval(I('bind'));
        if ($bind == 1) {
            $map['siteuser'] = 1;
        } elseif ($bind == 2) {
            $map['siteuser'] = 0;
        }

        if (!isset($_GET['from_id'])) {
            $from_id = -1;
        } else {
            $from_id = intval(I('from_id'));
            $map['d_users.from_id'] = $from_id;
        }

        if (!isset($_GET['state'])) {
            $state = 0;
        } else {
            $state = intval(I('state'));
        }
        $map['status'] = $state;


        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['id'] = intval($value);
                    break;
                case 1:
                    $map['id'] = D('Users')->get_uid('char_id', intval($value));
                    break;
                case 2:
                    $map['name'] = trim($value);
                    break;
                case 3:
                    $userinfo = D('DUsers')->where(array('name' => array('like', trim($value))))->field('account')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['account']);
                    }
                    $map['id'] = array('in', $ids);
                    break;
                case 4:
                    $str = explode(',', $value);
                    $uids = array();
                    foreach ($str as $v) {
                        $uids[] = intval($v);
                    }
                    $map['id'] = array('in', $uids);
                    break;
                case 5:
                    $create_ip = trim(I('value'));
                    $map['create_ip'] = $create_ip;
                    break;
            }
        }

        //注册地址和手机码
        $create_ip = trim(I('create_ip'));
        $login_ip = trim(I('login_ip'));
        $devid = trim(I('devid'));

        if ($create_ip != '') {
            $map['create_ip'] = $create_ip;
        }

        if ($login_ip != '') {
            $map['login_ip'] = $login_ip;
        }

        if ($devid != '') {
            $map['devid'] = $devid;
        }

        $map['d_users.is_brush'] = array('in','1,2');

        //var_dump($map);exit;
        $totals = $user->join('game1.d_users on d_users.account = d_accounts.id')
        ->where($map)->count();
        //print($user->getLastSql());
        
       // exit;

        $list   = $user->join('game1.d_users on d_users.account = d_accounts.id')
        ->field("d_accounts.*,d_users.create_ip,d_users.login_count,d_users.last_logout,d_users.login_ip,d_users.id as uid,d_users.is_brush,d_users.from_id,d_users.from_time,d_users.name as uname,d_users.sdk_uid as usdk_uid")->where($map)->page($p, $limit)->order('d_accounts.id desc')->select();

        //print($user->getLastSql());
        $ids = array();
        foreach ($list as $v) {
            $ids[] = intval($v['account']);
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $fromkey = D('DChannels')->group('from_id')->order('id asc')->select();

        $this->assign('from', $fromkey);
        $this->assign('_list', $list);
        $this->assign('state', $state);
        $this->assign('user', D('DUsers')->getUserInfo($ids));
        $this->assign('from_id', $from_id);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    //移除刷子用户标识
    public function cancelbrush()
    {
        $id = I('id');
        if($id) {
          
            //设为刷子用户标识
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate.set_brush_state';
            $post_data['sign'] = md5(C('SENDGAME_KEY').time());
            $post_data['timestamp'] = intval(time());
            $post_data['roleid'] = intval($id); //账号ID
            $post_data['is_brush'] = 0;
            $field = json_encode($post_data);
            $ret = PostUrl($post_url, $field,1);
            if ($ret == 'ok')
            {
                //D('DUsers')->where(['id' => $id])->save(['is_brush' => 0]);
            }

        }
        $brush_log = I('brush_log');
        if($brush_log == 1) {
            D('BrushLog')->where(['uid'=> $id])->save(['state' => 1, 'updatetime' => time()]) ; //易盾刷子标识改为验证成功
        }
        $this->success('brush');
    }

    //白名单配置
    public function whitelist()
    {
        $configModel = D('config');
        $whitelistModel = D('whitelist');

        //配置
        $uphold = $configModel->where(['name' => 'UPHOLD'])->find();
        $uphold_text = $configModel->where(['name' => 'UPHOLD_TEXT'])->find();

        //白名单列表
        $p = I('p');
        $limit = 20;
        $uid = I('uid');
        if(!empty($uid)) {
            $map['uid'] = $uid;
        }
        $totals = $whitelistModel->where($map)->count();
        $list = $whitelistModel->where($map)->page($p, $limit)->order('created_at desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('uphold_text', $uphold_text['value']);
        $this->assign('uphold', $uphold['value']);
        $this->assign('_list', $list);
        $this->display();

    }

    //删除白名单
    public function delWhitelist()
    {
        $whitelistModel = D('whitelist');
        $id = I('id');
        if(!empty($id)) {
            if($whitelistModel->delete($id)) {
                $this->success('删除成功');
            } else {
                echo $whitelistModel->getLastSql();
            }
        } else {
            $this->error('删除失败');
        }

    }

    //新增白名单
    public function addWhitelist()
    {
        $whitelistModel = D('whitelist');

        if(IS_POST) {
            $data['uid'] = I('uid');
            if(empty($data['uid'])) {
                $this->error('用户ID不能为空');
            }
            if($whitelistModel->where($data)->find()) {
                $this->success('添加成功', 'whitelist');
            } else {
                $data['created_at'] = time();
                if($whitelistModel->add($data)) {
                    $this->success('添加成功', 'whitelist');
                } else {
                    $this->error("添加失败，".$whitelistModel->getLastSql());
                }
            }
        } else {
            $this->assign('_list', $list);
            $this->display();
        }

    }

    //修改白名单配置
    public function editWhitelistConf()
    {
        $configModel = D('config');
        $uphold = I('uphold');
        $uphold_text = I('uphold_text');

        if(!isset($uphold) || empty($uphold_text)) {
            $this->error('修改配置失败');
        }

        $configModel->where(['name' => 'UPHOLD'])->save(['value' => $uphold]);
        $configModel->where(['name' => 'UPHOLD_TEXT'])->save(['value' => $uphold_text]);
        $this->success('修改成功');
    }

        //黑白名单配置
    public function wbuserslistolld()
    {
        $wbusersModel = D('DWbUsers');
        $limit = 20;
        
        $p = I('p');
        $status = intval(I('status'));
        if (isset($status)) {
            switch ($status) {
                case 1:
                    $map['d_wb_users.status'] = $status;
                    break;
				case 2:
                    $map['d_wb_users.status'] = $status;
                    break;
                case 7:
                    $map['d_wb_users.status'] = $status;
                    break;
            }
        }

        $id = I('id');
        $sctype = I('sctype');
        if(!empty($id)&&!empty($sctype)) {
            switch ($sctype) {
                case 1:
                    $map['u.id'] = $id;
                    $map['d_wb_users.dev_id'] =array('neq','');
                    $totals = $wbusersModel->join('d_accounts as u ON u.devid = d_wb_users.dev_id', 'right')->where($map)->count();
                    if($totals>0)
                    {
                        $list = $wbusersModel->join('d_accounts as u ON u.devid = d_wb_users.dev_id', 'right')->where($map)->field('d_wb_users.*,u.id as accountid ')->page($p, $limit)->order('d_wb_users.status<>2,d_wb_users.created_at desc')->select();
                    }
                    else
                    {
                        $smap['u.id'] = $id;
                        $smap['d_wb_users.card_no'] = $id;
                        $totals = $wbusersModel->join('d_accounts as u ON u.ic_no = d_wb_users.card_no', 'right')->where($smap)->count();
                        $list = $wbusersModel->join('d_accounts as u ON  u.ic_no = d_wb_users.card_no', 'right')->where($smap)->field('d_wb_users.id,d_wb_users.card_no,d_wb_users.status,d_wb_users.created_at,d_wb_users.updated_at,u.devid as dev_id,u.id as accountid ')->page($p, $limit)->order('d_wb_users.status<>2,d_wb_users.created_at desc')->select();
                        
                    }
                    break;
				case 2:
                    $map['dev_id'] = $id;
                    $totals = $wbusersModel->where($map)->count();
                    $list = $wbusersModel->where($map)->page($p, $limit)->order('created_at desc')->select();
                    break;
                case 3:
                    $map['card_no'] = $id;
                    $totals = $wbusersModel->where($map)->count();
                    $list = $wbusersModel->where($map)->page($p, $limit)->order('created_at desc')->select();
                    break;
            }
        }
        else
        {
            $totals = $wbusersModel->where($map)->count();
            $list = $wbusersModel->where($map)->page($p, $limit)->order('card_no desc,status<>2 desc')->select();

        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('status', $status);
        $this->assign('_list', $list);
        $this->display();

    }

    public function wbuserslist()
    {
        $wbusersModel = D('DWbUserList');
        $limit = 20;
        
        $p = I('p');
        $status = intval(I('status'));
        if (isset($status)) {
            switch ($status) {
                case 1:
                    $map['d_wb_user_list.status'] = $status;
                    break;
				case 2:
                    $map['d_wb_user_list.status'] = $status;
                    break;
                case 7:
                    $map['d_wb_user_list.status'] = $status;
                    break;
            }
        }

        $id = I('id');
        $sctype = I('sctype');
        if(!empty($id)&&!empty($sctype)) {
            switch ($sctype) {
                case 1:
                    $map['u.id'] = $id;
                    $map['d_wb_user_list.dev_id'] =array('neq','');
                    $totals = $wbusersModel->join('d_accounts as u ON u.devid = d_wb_user_list.dev_id', 'right')->where($map)->count();
                    if($totals>0)
                    {
                        $list = $wbusersModel->join('d_accounts as u ON u.devid = d_wb_user_list.dev_id', 'right')->where($map)->field('d_wb_user_list.*,u.id as accountid ')->page($p, $limit)->order('d_wb_user_list.status<>2,d_wb_user_list.created_at desc')->select();
                    }
                    else
                    {
                        $smap['u.id'] = $id;
                        $smap['d_wb_user_list.card_no'] = $id;
                        $totals = $wbusersModel->join('d_accounts as u ON u.ic_no = d_wb_user_list.card_no', 'right')->where($smap)->count();
                        $list = $wbusersModel->join('d_accounts as u ON  u.ic_no = d_wb_user_list.card_no', 'right')->where($smap)->field('d_wb_user_list.id,d_wb_user_list.card_no,d_wb_user_list.status,d_wb_user_list.created_at,d_wb_user_list.updated_at,u.devid as dev_id,u.id as accountid ')->page($p, $limit)->order('d_wb_user_list.status<>2,d_wb_user_list.created_at desc')->select();
                        
                    }
                    break;
				case 2:
                    $map['dev_id'] = $id;
                    $totals = $wbusersModel->where($map)->count();
                    $list = $wbusersModel->where($map)->page($p, $limit)->order('created_at desc')->select();
                    break;
                case 3:
                    $map['card_no'] = $id;
                    $totals = $wbusersModel->where($map)->count();
                    $list = $wbusersModel->where($map)->page($p, $limit)->order('created_at desc')->select();
                    break;
            }
        }
        else
        {
            $totals = $wbusersModel->where($map)->count();
            $list = $wbusersModel->where($map)->page($p, $limit)->order('card_no desc,status<>2 desc')->select();

        }
        // XIN zend_logo_guid
		$data['dev_id']       =   I('dev_id');

        if(!empty($data['dev_id']))
        {        
            $umap['dev_id'] = $data['dev_id'];     
            $user = D('DWbUserList')->where($umap)->find();
            if (!empty($user))
            {
				$this->error('记录已存在', U('Operate/wbuserslist'));
            }
            else
            {
                
                $data['created_at'] = date("Y-m-d H:i:s");
				$data['status'] = 2;
                $res = D('DWbUserList')->add($data);
                if($res) {
                    $this->success('操作成功', U('Operate/wbuserslist'));
                }
                else
                {
					$this->error('新增失败', U('Operate/wbuserslist'));
                }
            }
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('status', $status);
        $this->assign('_list', $list);
        $this->display();

    }
    //编辑名单
    public function editwbuserslist()
    {
        $wbusersModel = D('DWbUserList');
        $id = I('id');
        $status = I('status');
        if(!empty($id)&&!empty($status)) {
            if($wbusersModel->where(['id' => $id])->save(['status' => $status])) {
                //提醒服务端重载
                $post_url = C('GMBox_URL') . 'sys/reload_config'; 
                $data['conf_type'] = intval(102);
                $data['wb_status'] = intval($status);
                $data['account'] = intval($id);
                 
                $data['timestamp'] = intval(time());
                $data['sign'] = md5(C('SENDGAME_KEY').time()); 
                $field = json_encode($data);

                $rets = PostUrl($post_url, $field,1);
                if($rets=="ok")
                {
                    $this->success('编辑成功');
                }
                else{$this->success('服务端提交失败'.$rets);}

                
            } else {
                echo $wbusersModel->getLastSql();
            }
        } else {
            $this->error('编辑失败');
        }

    }

    //易盾刷子管理   待更新   is_brush  放d_users
    public function ydbrush()
    {
       

        $limit = 20;
        $user = D('DUsers');
        $onlie = I('onlie');
        $new = I('new');
        $p = I('p');
        if(!empty($onlie)) {
            //实时在线
            $onlieList = D('Online')->order('c_time desc')->find();
        }

        if(!empty($new)) {
            $stime = date("Y-m-d");
            $etime =  date("Y-m-d H:i:s",strtotime(date("Y-m-d")) + 86399);
            $map['created_at'] = ['BETWEEN' , [$stime,$etime]];
        }

        if (isset($_GET['b_time'])) {
            $b_time = I('b_time');
            $map['created_at'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = date("Y-m-d H:i:s",(strtotime(I('e_time')) + 86399));
            $map['created_at'][] = array('elt', $e_time);
        }

        $bind = intval(I('bind'));
        if ($bind == 1) {
            $map['siteuser'] = 1;
        } elseif ($bind == 2) {
            $map['siteuser'] = 0;
        }

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['d_users.id'] = D('DUsers')->get_charid('uid', intval($value));
                    break;
                case 1:
                    $map['d_users.id'] = intval($value);
                    break;
                case 2:
                    $map['d_users.name'] = trim($value);
                    break;
                case 3:
                    $value = $_GET['value'] =  urldecode($value);
                    $map['d_users.name'] = array('LIKE', "%$value%");
                    break;
                case 4:
                    $str = explode(',', $value);
                    $uids = array();
                    foreach ($str as $v) {
                        $uids[] = intval($v);
                    }
                    $map['d_users.id'] = array('in', $uids);
                    break;
            }
        } else {
            $map['d_users.id'] = array('egt', 1);
        }

        //注册地址和手机码
        $create_ip = trim(I('create_ip'));
        $login_ip = trim(I('login_ip'));
        $devid = trim(I('devid'));

        if ($create_ip != '') {
            $map['create_ip'] = $create_ip;
        }

        if ($login_ip != '') {
            $map['login_ip'] = $login_ip;
        }

        if ($devid != '') {
            $map['devid'] = $devid;
        }

        $state = I('state');
        if(!empty($state)) {
            $map['b.state'] = $state;
        } else {
           $map['d_users.is_brush'] = 1;
        }
       // print_r($user);

        $totals = $user->join('niu_managedb.mycms_brush_log b ON d_accounts.id = b.uid', 'RIGHT')->where($map)->count();

        $list   = $user->join('niu_managedb.mycms_brush_log b ON d_accounts.id = b.uid', 'RIGHT')->where($map)->page($p, $limit)->order('d_users.id desc')->select();
       
	   //echo $user->join('niu_managedb.mycms_brush_log b ON d_accounts.id = b.uid', 'RIGHT')->where($map)->getLastSql();
      
        $ids = array();
        foreach ($list as $v) {
            $ids[] = intval($v['id']);
			
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $fromkey = D('DChannels')->group('from_id')->order('id asc')->select();

        //总校验次数
        $BrushLog = D('BrushLog')->field('sum(brush_times) as brush_times')->find();
        $check_times = $BrushLog['brush_times'];

        $this->assign('from', $fromkey);
        $this->assign('_list', $list);
        $this->assign('state', $state);
        $this->assign('user', D('DUsers')->getUserInfo($ids));
        $this->assign('check_times', $check_times);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    //身份认证
    public function verifiedold()
    {

        $limit = 20;
        $user = D('DRealNames');
        
       // $new = I('new');
        $p = I('p');

       // $status = I('status');

/* 
        if(!empty($new)) {
            $stime = strtotime(date("Y-m-d"));
            $etime =  strtotime(date("Y-m-d")) + 86399;
            $map['adddate'] = ['BETWEEN' , [$stime,$etime]];
        } */

       /*  if($status) {
            $map['status'] = $status;
        } */

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
              /*   case 1:
                    $map['game_data'] = ['like', '%'.$value.'%'];
                    break; */
                case 2:
                    $map['name'] = trim($value);
                    break;
                case 3:
                    $map['card_no'] = trim($value);
                    break;

            }
        }


        $totals = $user->where($map)->count();

        $list   = $user->where($map)->page($p, $limit)->order('id desc')->select();
         // var_dump($user->getLastSql());exit;

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    public function verified()
    {

        $limit = 20;
        $user = D('DRealNameList');
        
        $p = I('p');
 
        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
              /*   case 1:
                    $map['game_data'] = ['like', '%'.$value.'%'];
                    break; */
                case 2:
                    $map['name'] = trim($value);
                    break;
                case 3:
                    $map['card_no'] = trim($value);
                    break;

            }
        }


        $totals = $user->where($map)->count();

        $list   = $user->where($map)->page($p, $limit)->order('id desc')->select();
         // var_dump($user->getLastSql());exit;

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    //手动认证
    public function verifiedAuth()
    {
        $id = I('id');
        $model = D('Verified');
        $info = $model->where(['id' => $id])->find();
        if($info['status'] != 0) {

            //发放奖励
            $data = json_decode($info['game_data'], true);
            foreach ($data['xjl'] as $val) {
                $this->_sendVerifiedService($val);
            }
            $saveData['xjl'] = 1;
            $saveData['operate'] = $_SESSION['username'];
            $saveData['auth_time'] = time();
            $saveData['status'] = 0;

            $model->where(['id' => $id])->save($saveData);
            $this->success("操作成功");
        } else{
            $this->error('已验证过');
        }

    }



    //领取成功发送服务端
    private function _sendVerifiedService($uid)
    {
        $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:real_name_authe';

        $data['uid'] = $uid;
        $field = http_build_query($data);
        $ret = PostUrl($post_url, $field, 0);
        $arrs = json_decode($ret, true);
        if ($arrs['ret'] == 0) {
            return true;
        }

        return false;
    }


    //支付开关
    public function paySwitch()

    {
        $configModel = D('config');
        $paySwitchLog = D('paySwitchLog');

        //配置
        $config = $configModel->where(['name' => 'PAY_SWITCH'])->find();

        if (IS_POST) {
            //提交操作
            $data['username'] = $_SESSION['username'];
            $data['created_at'] = time();
            $data['status'] = I('post.status');

            //修改设置
            $configModel->where(['name' => 'PAY_SWITCH'])->save(['status' => $data['status']]);

            if ($paySwitchLog->add($data)) {
                $this->success('操作成功', 'paySwitch');
            }
        } else {

            //列表
            $p = I('p');
            $limit = 20;
            $uid = I('uid');
            if (!empty($uid)) {
                $map['uid'] = $uid;
            }
            $totals = $paySwitchLog->where($map)->count();
            $list = $paySwitchLog->where($map)->page($p, $limit)->order('created_at desc')->select();

            $pageNav = new \Think\Page($totals, $limit);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

            $this->assign('_page', $pageNav->show());
            $this->assign('pay_config', $config['status']);
            $this->assign('_list', $list);
            $this->display();
        }
    }

    public function bindPhone()
    {
        $model = M('BindPhoneLog');
        $uid = I('uid');
        $p = I('p');
        $limit = 15;

        if(IS_POST && !empty($uid)) {
            //提交绑定
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:bind_player_phone';
            $uid = I('uid');
            $data['phone_num'] = 1;
            $data['role_id'] = $uid;
            $data['sign'] = md5($uid.$data['phone_num']);
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field, 0);
            $arrs = json_decode($ret, true);

            //保存记录
            $saveData['uid'] = $uid;
            $saveData['created_at'] = time();
            $saveData['admin_username'] = $_SESSION['username'];

            if($model->add($saveData)) {
                $this->success('操作成功', 'bindPhone');
            }
        }
        $map = [];
        $totals = $model->where($map)->count();
        $list = $model->where($map)->page($p, $limit)->order('created_at desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->display();
    }


    //支付宝充值配置
    public function aliPaySwitch()
    {
        $model = D('DThirdPays');

        $list = $model->order("th_id asc,status desc,id desc")->select();
        foreach ($list as $k => $v) {
            $list[$k]['get_status'] = $v['status'] == 0 ? "关闭":"开启";
            $list[$k]['get_paytype'] = $v['pay_type'] % 2 == 1 ? "微信":"支付宝";
        }

        $this->assign('_list', $list);
        $this->display();
    }

    //支付宝兑换配置
    public function aliExchangeSwitch()
    {
        $model = D('DThirdPays');
        $list = $model->where(['type' => 2])->select();

        $this->assign('_list', $list);
        $this->display();
    }

    //打开支付设置
    public function setAliConfig()
    {
        $id = I('id');
        $pay_type = I('pay_type');
        
        if(empty($id)) {
            $this->error('参数不正确');
        }

            
        $model = D('DThirdPays');
        //$model->where(['id' => $id])->save(['status' => intval($pay_type)]);
        //print($model->getLastSql());exit;
        if ($model->where(['id' => $id])->save(['status' => intval($pay_type)]))
        {
            //支付开关
            $post_url = C('GMBox_URL') . 'cgi-bin/gm_oprate.notice_3rdpay';
            $post_data['sign'] = md5(C('SENDGAME_KEY').time());
            $post_data['timestamp'] = intval(time()); 
            $field = json_encode($post_data);
            $ret = PostUrl($post_url, $field,1); 
            if ($ret=='ok')
            {
                $this->success('操作成功');
            }
            else
            {
                $this->success('操作失败'.$ret);

            }
            

        }
        else
        {
            $this->success('操作失败');
        }

        

        
    }

    //编辑支付设置
    public function editAliConfig()
    {
        $model = D('DThirdPays');

        if(IS_POST) {

            $id = I('post.id');

            $data['desc'] = I('desc');
            $data['th_id'] = I('th_id');
            $data['pay_type'] = I('pay_type');
            $data['merchant_id'] = I('merchant_id'); 
            $data['private_key'] = I('private_key');
            $data['public_key'] = I('public_key');
            $data['updated_at'] = date("Y-m-d H:i:s");
            if ($id)
            {
                $model->where(['id' => $id])->save($data);
            }
            else
            {
                $data['created_at'] = date("Y-m-d H:i:s");
                $model->add($data);
            }
           
            $this->success('操作成功', U('aliPaySwitch'));

        } else {
            $id = I('id');         
            $paytypes[1] = "微信";
            $paytypes[2] = "支付宝";
            $list = [];
            if ($id)
            {
                $list = $model->where(['id' => $id])->find();
                $pay_type = $list['pay_type'];
            }
            else
            {
                $pay_type = 2;
            }
            
            $this->assign('info', $list);
            $this->assign('pay_type', $pay_type);
            $this->assign('paytypes', $paytypes);
            $this->display();
        }
    }

    public function addAliSwitch()
    {
        $model = D('DThirdPays');
        $type = I('type');
        $list['type'] = $type;

        if(IS_POST) {

            $data['name'] = I('name');
            $data['app_cert_path'] = I('app_cert_path');
            $data['appid'] = I('appid');
            $data['pay_cert_path'] = I('pay_cert_path');
            $data['root_cert_path'] = I('root_cert_path');
            $data['private_key'] = I('private_key');
            $data['public_key'] = I('public_key');
            $data['type'] = I('type');


            $model->add($data);
            $this->success('操作成功');

        } else {
            $this->assign('info', $list);
            $this->display();
        }
    }

    //汇潮支付配置
    public function hcPayConfig()
    {
        $model = D('PayConfig');

        $list = $model->where(['type' => 15])->select();
        // var_dump($model->getLastSql());exit;

        $this->assign('_list', $list);
        $this->display();
    }


    //添加支付
    public function addhcPayConfig()
    {
        $model = D("PayConfig");
        $type = I('type');
        $list['type'] = $type;

        if(IS_POST) {

            $data['name'] = I('name');
            $data['mch_id'] = I('mch_id');
            $data['private_key'] = I('private_key');
            $data['public_key'] = I('public_key');
            $data['type'] = I('type');
            // var_dump($data);exit;

            $model->add($data);
            $this->success('操作成功', U('hcPayConfig'));

        } else {
            $this->assign('info', $list);
            $this->display();
        }
    }

    //编辑汇潮
    public function hcPayedit()
    {
        $model = D('PayConfig');
        if(IS_POST) {

            $id = I('post.id');
            $data['name'] = I('name');
            $data['mch_id'] = I('mch_id');
            $data['private_key'] = I('private_key');
            $data['public_key'] = I('public_key');

            $model->where(['id' => $id])->save($data);
            $this->success('操作成功',U('hcPayConfig'));

        } else {
            $id = I('id');

            $list = $model->where(['id' => $id])->find();
            $this->assign('info', $list);
            $this->display();
        }
    }

    //打开汇潮支付开关
    public function openPayConfig()
    {
        $id = I('id');
        $type = I('type');
        if(empty($type) || empty($id)) {
            $this->error('参数不正确');
        }

        $model = D("PayConfig");
        $model->where(['type' => $type])->save(['is_open' => 0]);
        $model->where(['id' => $id])->save(['is_open' => 1]);
        $this->success('操作成功');
    }

    //兑换配置
    public function exchangeCode()
    {

        $act = I('act');
        $ExchangeLogModel = M('ExchangeLog');
        $ExchangeCodeModel = M('ExchangeCode');
        $fromList = D('DChannels')->select();
        $from_id = I('from_id');

        $countMap['state'] = 1;
        if($from_id) {
            $countMap['from_id'] = $from_id;
        }
        $count = $ExchangeCodeModel->where($countMap)->count();
        $this->assign('count', $count);

        if($act == 'submit') {
            //提交生成

            $data = I();
            unset($data['act']);
            $from_id = $data['from_id'];
            $codeInfo = M('ExchangeCode')->where(['state' =>1, 'from_id' => $from_id])->find();

            if(empty($codeInfo)) {
                $this->error('兑换码库存不足');
            }

            $data['st_time'] = strtotime($data['st_time']);
            $data['ed_time'] = strtotime($data['ed_time']);

            if(!$data['num'] || !$data['st_time'] || !$data['ed_time']) {
                $this->error('开始时间、结束时间、数量不能为空');
            }
            // var_dump($data['st_time'],$data['end_time'], $data['st_time'] >= $data['end_time']);

            if($data['st_time'] >= $data['ed_time']) {
                $this->error('结束时间要大于开始时间');
            }

            if(!$data['from_id']) {
                $this->error('渠道id不能为空');
            }

            $data['ex_code'] = $codeInfo['code'];

            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate:notice_ex_code';
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field, 0);

            //修改state为0
            $ExchangeCodeModel->where(['id' =>$codeInfo['id']])->save(['state' => 0]);
            //生成兑换记录
            M('ExchangeLog')->add([
                    'start_time' => $data['st_time'],
                    'end_time' => $data['ed_time'],
                    'type' => $data['reward'],
                    'code' => $codeInfo['code'],
                    'num' =>$data['num'],
                    'add_time' =>time(),
                    'from_id' => $data['from_id'],
                ]);
            $this->success('生成成功，兑换码：'.$codeInfo['code']);exit;
        }
        else if($act == 'inputCode')   //录入兑换码
        {
            if($from_id) {
                $codeMap['from_id'] = $from_id;
            }
            $codeMap['state'] = 1;
            $list = M('ExchangeCode')->where($codeMap)->order('add_time desc')->select();
            $this->assign('_list', $list);
            $this->assign('from_id', $from_id);
            $this->assign('fromList', $fromList);
            $this->display('inputExchangeCode');
            exit;
        }
        else if( $act == 'submitCode')  //批量录入兑换码
        {
            $code = I('code_str');
            $from_id = I('from_id');
            $codeList = explode("\n", $code);
            // var_dump($codeList);exit;
            $count = 0;
            foreach ($codeList as $val) {
                $code = trim($val);
                $codeCount = $ExchangeCodeModel->where(['code' => $code, 'state' => 1])->count();
                if($codeCount == 0) {
                    if(!empty($code)) {
                        $ExchangeCodeModel->add([
                            'add_time' => time(),
                            'code' => $code,
                            'from_id' => $from_id,
                        ]);
                    }
                    $count ++;
                }
            }
            $this->success('录入成功，共录入 '.$count.' 个');exit;

        }
        else if( $act == 'delCode')
        {
            $id = I('id');
            $ExchangeCodeModel->where(['id' =>$id])->save(['state' =>0]);
            $this->success('删除成功！');
            exit;

        }
        $list = $ExchangeLogModel->order('id desc')->limit(20)->select();

        $this->assign('from_id', $from_id);
        $this->assign('fromList', $fromList);
        $this->assign('_list', $list);
        $this->display();
    }

    //兑换统计
    public function exchangeCodeStat()
    {
        $from_id = I('from_id');
        if($from_id) {
            $mpa['from_id'] = I('from_id');
        }

        $list = M('ExchangeUse')->where($mpa)->field("code,count(*) as num")->group('code')->select();
        $item = '';
        foreach ($list as $val) {
            $item .= "['{$val['code']}', {$val['num']}], ";
        }
        // var_dump($item);exit;
        $this->assign('_list', $item);
        $this->display();
    }

    //奖池配置
    public function pizeConfig()
    {
        $act = I('act');
        $model  = M('PrizeLog');
        $countMap =  [];
        $count = $model
            ->where($countMap)->count();
        $this->assign('count', $count);
        $rooms = D('DRooms')->rooms();
        if($act == 'submit') {
            //提交生成

            $data = I();
            $uid = $data['uid'];
            //判断是正确的uid
            $user = D('DAccounts')->where(['id' =>$uid])->find();

            if(!$user) {
                $this->error('用户ID有误');
                exit;
            }
            //if($user['from_id'] != 10000 && $user['from_id'] != 998) {
            //    $this->error('该用户为非官方渠道用户');
            //    exit();
            //}
            if(empty($data['percent'])) {
                $this->error('请输入百分比');
            }

            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate.diss_pool';            
             $data['account'] =  intval($uid);
             $data['percent'] = intval($data['percent']);
             $data['room_type'] = intval($data['type']);
             $data['timestamp'] = intval(time());
             $data['sign'] = md5(C('SENDGAME_KEY').time()); 
             $field = json_encode($data);

             $rets = PostUrl($post_url, $field,1);


            //生成记录
            $model->add([
                    'uid' => $data['uid'],
                    'operate' => $_SESSION['username'],
                    'percent' => $data['percent'],
                    'add_time' =>time(),
                ]);
            $this->success('添加成功');exit;
        }

        $list = $model
            ->join('gamebox.d_accounts as u ON u.id = mycms_prize_log.uid')
            ->field("mycms_prize_log.id, u.id as uid,operate,percent, mycms_prize_log.add_time, u.name")
            ->order('id desc')->limit(50)->select();
        $this->assign('_list', $list);
        $this->assign('_rooms', $rooms);
        $this->display();
    }

    //重载
    public function reloadConfig()
    {
        $act = I('act');
        $model  = M('PrizeLog');
        $countMap['mycms_prize_log.uid'] = array('exp',' is NULL');
        $count = $model
            ->where($countMap)->count();
        $this->assign('count', $count);
        $rooms = array(
            10001 => 'loadVip',
			10002 => 'loadPlanA',
            10003 => 'loadFruit',
            10004 => 'loadRank',
            10005 => 'loadItem',
            10006 => 'loadMonthCard',
            10007 => 'loadMail',
            10008 => 'loadHorseLamp',
            10009 => 'loadShop',
			10010 => 'loadShopExchange'
			);
        if($act == 'submit') {
            //提交生成

            $data = I();
            
            //$post_url = C('GM_URL') . 'cgi-bin/gm_oprate.diss_pool'; 
            $post_url = C('GM_URL') . 'sys/reload_conf'; 
             $data['tp'] = intval($data['type']);
             $data['timestamp'] = intval(time());
             $data['sign'] = md5(C('SENDGAME_KEY').time()); 
             $field = json_encode($data);

             $rets = PostUrl($post_url, $field,1);
             if($rets=="ok")
             {
                //生成记录
                $model->add([
                    'operate' => $_SESSION['username'],
                    'percent' => $data['type'],
                    'add_time' =>time(),
                ]);
             }


            
            $this->success('添加成功');exit;
        }

        $list = $model
            ->where($countMap)
            ->field("id,operate,percent,add_time")
            ->order('id desc')->limit(50)->select();
        $this->assign('_list', $list);
        $this->assign('_rooms', $rooms);
        $this->display();
    }

    //服务端黑名单
    public function serverBlackList()
    {
        $p = I('p');
        $model = M('serverBlacklist');
        $uidStr = I('uid_str');
        if(IS_POST || !empty($uidStr)) {
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate.black_user';
            $data['uid'] = $uidStr;
            $field = http_build_query($data);
            $ret = PostUrl($post_url, $field, 0);
            //插入记录
            $model->add([
                'uid' => $uidStr,
                'admin' => $_SESSION['username'],
                'add_time' => time(),
            ]);
            $this->success('操作成功');exit;
        }

        $limit = 50;
        // $list = $model->limit(50)->select();

        $totals = $model->count();
        $list = $model->page($p, $limit)->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_page', $pageNav->show());

        $this->assign('_list', $list);
        $this->display();
    }


    public function limit1w()
    {
        $p = I('p');
        $model = D('Limit1w');
        $uidStr = I('uid');
        if(IS_POST || !empty($uidStr)) {

            $model->add([
                'uid' => $uidStr,
                'add_time' => time(),
            ]);
            $this->success('操作成功');exit;
        }

        $limit = 50;
        // $list = $model->limit(50)->select();

        $totals = $model->count();
        $list = $model->page($p, $limit)->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_page', $pageNav->show());

        $this->assign('_list', $list);
        $this->display();
    }


    public function avatarSet()
    {
        $model = M('Avatar');
        $uid = I('uid');
        $avatar = I('avatar');
        $p = I('p');
        $limit = 15;
        $act = I('act');

        if($act == 'del') {
            //删除
            $id = I('id');
            if(!$id) {
                $this->error('用户id有误');
            }
            $model->where(['id' => $id])->delete();
            $this->success('操作成功'); exit;
        }

        if($act == 'edit') {
            $id = I('id');
            if(!$id) {
                $this->error('用户id有误');
            }
            $info = $model->where(['id' => $id])->find();
            $this->assign('info', $info);
            $this->display('avatarEdit');exit;
        }
        // var_dump($act);exit;
        if($act == 'save') {
            //编辑 提交
            $id = I('id');
            if(!$id) {
                $this->error('用户id有误');
            }
            $data['c_time'] = time();
            $data['admin'] = $_SESSION['username'];
            $data['avatar'] = $avatar;
            $info = $model->where(['id' => $id])->save($data);
            if($info) {
                D('Users')->where(['id' => $uid])->save(['wxheadimgurl' => $avatar]);
                // var_dump(D('Users')->getLastSql());exit;
            }
            $this->success('操作成功'); exit;
        }

        if(IS_POST && !empty($uid)) {

            if(!$avatar || !$uid ) {
                $this->error('参数错误');exit;
            }

            $uid = I('uid');
            if($model->where(['uid' => $uid])->find()) {
                $this->error('该用户已存在');exit;
            }

            $data['uid'] = $uid;
            $data['c_time'] = time();
            $data['admin'] = $_SESSION['username'];
            $data['avatar'] = $avatar;

            if($model->add($data)) {
                //修改用户头像
                D('Users')->where(['id' => $uid])->save(['wxheadimgurl' => $avatar]);
                $this->success('操作成功', 'avatarSet'); exit;
            }
        }
        $map = [];
        $totals = $model->where($map)->count();
        $list = $model->where($map)->page($p, $limit)->order('c_time desc')->select();
        // var_dump($model->getLastSql());exit;

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->display();
    }

    //汇天下支付配置
    public function huitianxiaConfig()
    {
        $model = D('Huitianxia');
        $list = $model->select();
        $act = I('act');
        if($act == 'del') {
            $id = I('id');
            $model->where(['id' => $id])->delete();
            $this->success('删除成功！');
            exit;
        }
        $this->assign('_list', $list);
        $this->display();
    }

    //打开或者关闭设置
    public function huitianxiaSet()
    {
        $id = I('id');
        $model = D("Huitianxia");
        $model->where(['id' => ['neq', $id]])->save(['is_open' => 0]);
        $model->where(['id' => $id])->save(['is_open' => 1]);
        $this->success('操作成功');
    }

    //编辑
    public function huitianxiaEdit()
    {
        $model = D("Huitianxia");

        if(IS_POST) {

            $id = I('post.id');
            $data['name'] = I('name');
            $data['app_id'] = I('app_id');
            $data['key_live'] = I('key_live');
            $data['private_key'] = I('private_key');
            $model->where(['id' => $id])->save($data);
            $this->success('操作成功');

        } else {
            $id = I('id');

            $list = $model->where(['id' => $id])->find();
            $this->assign('info', $list);
            $this->display();
        }
    }

    //新增
    public function huitianxiaAdd()
    {
        $model = D("Huitianxia");
        if(IS_POST) {
            $data['name'] = I('name');
            $data['app_id'] = I('app_id');
            $data['key_live'] = I('key_live');
            $data['private_key'] = I('private_key');
            $data['is_open'] = 0;
            $model->add($data);
            $this->success('操作成功', 'huitianxiaConfig');exit;
        } else {
            $this->display();
        }
    }

    public function adduserslist()
	{
		$data['dev_id']       =   I('dev_id');

        if(!empty($data['dev_id']))
        {        
            $umap['dev_id'] = $data['dev_id'];     
            $user = D('DWbUserList')->where($umap)->find();
            if (!empty($user))
            {
				$this->error('记录已存在', U('Operate/wbuserslist'));
            }
            else
            {
                
                $data['created_at'] = date("Y-m-d H:i:s");
                $res = D('DWbUserList')->add($data);
                if($res) {
                    $this->success('操作成功', U('Operate/wbuserslist'));
                }
                else
                {
					$this->error('新增失败', U('Operate/wbuserslist'));
                }
            }
        }
	}
}
