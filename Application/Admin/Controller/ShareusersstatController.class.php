<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/5
 * Time: 15:21
 */
namespace Admin\Controller;
use Think\Controller;

class ShareusersstatController extends Controller{

    public function stat(){
        $stime = microtime(true);

        //分享抽奖玩家
        $listLottery = D('Sharelotteryuser')->select();
        $listLotteryUser = array();
        foreach ($listLottery as $v){
            $listLotteryUser[$v['uid']] = $v;
        }
        unset($listLottery);

        //统计充值金额
        $userinfo = D('Userinfo')->where(array('c_time'=>array('egt', strtotime('2018-03-26'))))->field('char_id,pays')->select();
        $chars = array();
        foreach ($userinfo as $v){
            $chars[$v['char_id']] = $v['pays'];
        }

        $list = D('Mysqlshareusers')->select();
        $shares = array();
        foreach ($list as $v){
            $shares[$v['share_user_id']]['char_id'] = intval($v['share_char_id']);
            $shares[$v['share_user_id']]['total_sharecount'] += 1;
            //补之前邮件发送的次数
            if($v['day_1'] == 1 && $v['is_red'] == 1){
                $shares[$v['share_user_id']]['total_day1'] += 1;
            }if($v['day_3'] == 1 && $v['is_red'] == 1) {
                //有效分享
//                $shares[$v['share_user_id']]['sharecount'] += 1;
                //3日抽奖次数
                $shares[$v['share_user_id']]['total_day3'] += 1;
            }if($v['day_7'] == 1 && $v['is_red'] == 1) {
                $shares[$v['share_user_id']]['total_day7'] += 1;
            }
//            $shares[$v['share_user_id']]['bill'] = intval($listLotteryUser[$v['share_user_id']][107]);//话费
            $shares[$v['share_user_id']]['red'] = intval($listLotteryUser[$v['share_user_id']][109]);//红包
            $shares[$v['share_user_id']]['recharge'] += intval($chars[$v['char_id']]);//充值
            //抽奖使用次数
            $shares[$v['share_user_id']]['use_day1'] = intval($listLotteryUser[$v['share_user_id']]['type1']);
            $shares[$v['share_user_id']]['use_day3'] = intval($listLotteryUser[$v['share_user_id']]['type3']);
            $shares[$v['share_user_id']]['use_day7'] = intval($listLotteryUser[$v['share_user_id']]['type7']);
        }

        $shareUsersStat = D('Shareusersstat');
        foreach ($shares as $uid => $info){
            $info['uid'] = $uid;
            $info['sharecount'] = intval($info['sharecount']);
            $info['total_day1'] = intval($info['total_day1']);
            $info['total_day3'] = intval($info['total_day3']);
            $info['total_day7'] = intval($info['total_day7']);

            if($user = $shareUsersStat->where(array('uid'=>$uid))->find()){
                $shareUsersStat->where(array('_id'=>$user['_id']))->save($info);
            }else{
                $shareUsersStat->add($info);
            }
        }

        $etime = microtime(true);
        $totals = round($etime - $stime, 2);
        echo "Run {$totals} S";
    }

}