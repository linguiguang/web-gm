<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 13:56
 */

namespace Admin\Controller;

use Think\Controller;

class CarstatController extends Controller
{

    /**
     * 豪车大盘半小时统计一次
     */
    public function stat()
    {
        $log = D('tablesSettingLogs')->where(['key' => intval(0)])->order('c_time desc')->select();
        $log = array_values($log)[0];

        // 默认配置的比率
        $platformRatio = 5;
        $poolRatio = 3;
        $rankRatio = 0;
        // 缓存读取的配置比率
        if ($log['set']) {
            $platformRatio = $log['set'][1]['param'];
            $poolRatio = $log['set'][2]['param'];
            $rankRatio  = $log['set'][3]['param'];
        }

        $platformRatio = 5;
        $poolRatio = 3;
        $rankRatio = 0;

        $stime = microtime(true);

        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        $Cargrail = D('Cargrail');
        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['uid'] = array('egt', 0); //排除AI,150000--150148不算0

        $carlogs = D('Carlog')->where($map)->select();
        $data = array();
        foreach ($carlogs as $v) {
            if (in_array($v['uid'], $uids_arr)) {
                continue;
            }

            $date = date('Y-m-d H:00:00', $v['c_time']);
            if (intval($v['test_type']) == 1) {
                // 1: 试玩
                $result = $v['get'] - abs($v['use']); // 获取金币结果
                if ($result > 0) {
                    // 说明： tax = $result * ((platformRatio + poolRatio) / 100); pooladd = $result * (poolRatio/100);
                    $data[$date]['get1'] += $result - abs($v['tax']) + $v['pool_win']; // 有概率从奖池获得部分金币
                    $data[$date]['use1'] += 0;
                } else {
                    // 
                    $data[$date]['get1'] += $v['pool_win'];
                    $data[$date]['use1'] += abs($result);
                }

                $data[$date]['pool_win1'] += intval($v['pool_win']); // 彩池产出
                $data[$date]['sui1'] += intval($v['tax']) * ($platformRatio / ($platformRatio + $poolRatio + $rankRatio)); // 平台纯税收
                $data[$date]['pool1'] += intval($v['pooladd']); // 彩池新增
            } else {
                // 2: 娱乐
                $result = $v['get'] - abs($v['use']); // 获取金币结果
                if ($result > 0) {
                    $data[$date]['get2'] += $result - abs($v['tax'])  + $v['pool_win']; // 有概率从奖池获得部分金币
                    $data[$date]['use2'] += 0;
                } else {
                    // 
                    $data[$date]['get2'] += $v['pool_win'];
                    $data[$date]['use2'] += abs($result);
                }

                $data[$date]['pool_win2'] += intval($v['pool_win']); // 彩池产出
                $data[$date]['sui2'] += intval($v['tax']) * ($platformRatio / ($platformRatio + $poolRatio + $rankRatio)); // 平台纯税收
                $data[$date]['pool2'] += intval($v['pooladd']); // 彩池新增

            }
        }
        //=========================
        $map2['c_time'] = array('between', array($b_time, $e_time));
        $map2['room'] = array('in', array(160, 161)); // 160娱乐，161试玩
        $map2['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $get_list = D('redbaglog')->where($map2)->select();
        $result = array();
        foreach ($get_list as $v) {
            if (in_array($v['uid'], $uids_arr)) {
                continue;
            }

            if ($v['room'] == 160) {
                $date = date('Y-m-d H:00:00', $v['c_time']);
                $data[$date]['redget160'] += intval($v['money']);
            } else if ($v['room'] == 161) {
                $date = date('Y-m-d H:00:00', $v['c_time']);
                $data[$date]['redget161'] += intval($v['money']);
            }
        }
        unset($get_list);

        //日排行
        $mapzhou['room'] = 1012;
        $mapzhou['c_time'] = array('between', array($b_time, $e_time));
        $mapzhou['uid'] = array('egt', 150000); //排除AI,150000--150148不算
        $get_listzhou = D('Goldget')->where($mapzhou)->select();
        $resultzhou = array();
        foreach ($get_listzhou as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            $data[$date]['zhouget'] += intval($v['gold']);
        }
        unset($get_listzhou);

        foreach ($data as $k => $v) {
            $ret = array(
                'time' => strtotime($k),

                'get1' => intval($v['get1']),
                'use1' => intval($v['use1']),
                'pool_win1' => intval($v['pool_win1']),
                'redget1' => intval($v['redget161']),
                'pooladd1' => intval($v['pool1']),
                'sui1' => intval($v['sui1']),
                'zhouget' => intval($v['zhouget']), //排行奖励
                'get2' => intval($v['get2']),
                'use2' => intval($v['use2']),
                'pool_win2' => intval($v['pool_win2']),
                'redget2' => intval($v['redget160']),
                'pooladd2' => intval($v['pool2']),
                'sui2' => intval($v['sui2']),
            );

            if ($info = $Cargrail->where(array('time' => strtotime($k)))->find()) {
                $Cargrail->where(array('_id' => $info['_id']))->save($ret);
            } else {
                $Cargrail->add($ret);
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    public function stat_upgrade()
    {
        $log = D('tablesSettingLogs')->where(['key' => intval(0)])->order('c_time desc')->select();
        $log = array_values($log)[0];

        // 默认配置的比率
        $platformRatio = 5;
        $poolRatio = 3;
        $rankRatio = 0;
        // 缓存读取的配置比率
        if ($log['set']) {
            $platformRatio = $log['set'][1]['param'];
            $poolRatio = $log['set'][2]['param'];
            $rankRatio  = $log['set'][3]['param'];
        }

        $platformRatio = 5;
        $poolRatio = 3;
        $rankRatio = 0;

        $stime = microtime(true);

        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $Cargrail = D('Cargrail');
        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        $bTime = date('YmdHis', $b_time);
        $b_hour = $bTime % 1000000 - $bTime % 10000;
        $b_hour = $b_hour / 10000;

        $eTime = date('YmdHis', $e_time);
        $e_hour = $eTime % 1000000 - $eTime % 10000;
        $e_hour = $e_hour / 10000;

        echo "time1 ：" . $b_hour . "\n";
        echo "time2 : " . $e_hour . "\n";
        for ($i = $b_hour; $i <= $e_hour; $i++) {
            //
            if ($i < 10) {
                $real_b_time = date('Ymd', $b_time) . '0' . $i . "0000";
            } else {
                $real_b_time = date('Ymd', $b_time) . $i . "0000";
            }

            $real_b_time = strtotime($real_b_time);
            $real_e_time = $real_b_time + 3599;

            $map['c_time'] = array('between', array($real_b_time, $real_e_time));
            $map['uid'] = array('egt', 0); //排除AI,150000--150148不算0
            $carlogs = D('Carlog')->where($map)->select();
            $data = array();
            foreach ($carlogs as $v) {
                if (in_array($v['uid'], $uids_arr)) {
                    continue;
                }

                $date = date('Y-m-d H:00:00', $v['c_time']);
                if (intval($v['test_type']) == 1) {
                    // 1: 试玩
                    $result = $v['get'] - abs($v['use']); // 获取金币结果
                    if ($result > 0) {
                        // 说明： tax = $result * ((platformRatio + poolRatio) / 100); pooladd = $result * (poolRatio/100);
                        $data[$date]['get1'] += $result - abs($v['tax']) + $v['pool_win']; // 有概率从奖池获得部分金币
                        $data[$date]['use1'] += 0;
                    } else {
                        // 
                        $data[$date]['get1'] += $v['pool_win'];
                        $data[$date]['use1'] += abs($result);
                    }

                    $data[$date]['pool_win1'] += intval($v['pool_win']); // 彩池产出
                    $data[$date]['sui1'] += intval($v['tax']) * ($platformRatio / ($platformRatio + $poolRatio + $rankRatio)); // 平台纯税收
                    $data[$date]['pool1'] += intval($v['pooladd']); // 彩池新增
                } else {
                    // 2: 娱乐
                    $result = $v['get'] - abs($v['use']); // 获取金币结果
                    if ($result > 0) {
                        $data[$date]['get2'] += $result - abs($v['tax'])  + $v['pool_win']; // 有概率从奖池获得部分金币
                        $data[$date]['use2'] += 0;
                    } else {
                        // 
                        $data[$date]['get2'] += $v['pool_win'];
                        $data[$date]['use2'] += abs($result);
                    }

                    $data[$date]['pool_win2'] += intval($v['pool_win']); // 彩池产出
                    $data[$date]['sui2'] += intval($v['tax']) * ($platformRatio / ($platformRatio + $poolRatio + $rankRatio)); // 平台纯税收
                    $data[$date]['pool2'] += intval($v['pooladd']); // 彩池新增

                }
            }
            //=========================
            $map2['c_time'] = array('between', array($real_b_time, $real_e_time));
            $map2['room'] = array('in', array(160, 161)); // 160娱乐，161试玩
            $map2['uid'] = array('egt', 0); //排除AI,150000--150148不算
            $get_list = D('redbaglog')->where($map2)->select();
            $result = array();
            foreach ($get_list as $v) {
                if (in_array($v['uid'], $uids_arr)) {
                    continue;
                }

                if ($v['room'] == 160) {
                    $date = date('Y-m-d H:00:00', $v['c_time']);
                    $data[$date]['redget160'] += intval($v['money']);
                } else if ($v['room'] == 161) {
                    $date = date('Y-m-d H:00:00', $v['c_time']);
                    $data[$date]['redget161'] += intval($v['money']);
                }
            }
            unset($get_list);

            //日排行
            $mapzhou['room'] = 1012;
            $mapzhou['c_time'] = array('between', array($real_b_time, $real_e_time));
            $mapzhou['uid'] = array('egt', 150000); //排除AI,150000--150148不算
            $get_listzhou = D('Goldget')->where($mapzhou)->select();
            foreach ($get_listzhou as $v) {
                if (in_array($v['uid'], $uids_arr)) {
                    continue;
                }

                $date = date('Y-m-d H:00:00', $v['c_time']);
                $data[$date]['zhouget'] += intval($v['gold']);
            }
            unset($get_listzhou);

            foreach ($data as $k => $v) {
                $ret = array(
                    'time' => strtotime($k),
                    'get1' => intval($v['get1']),
                    'use1' => intval($v['use1']),
                    'pool_win1' => intval($v['pool_win1']),
                    'redget1' => intval($v['redget161']),
                    'pooladd1' => intval($v['pool1']),
                    'sui1' => intval($v['sui1']),
                    'zhouget' => intval($v['zhouget']), //排行奖励
                    'get2' => intval($v['get2']),
                    'use2' => intval($v['use2']),
                    'pool_win2' => intval($v['pool_win2']),
                    'redget2' => intval($v['redget160']),
                    'pooladd2' => intval($v['pool2']),
                    'sui2' => intval($v['sui2']),
                );

                if ($oldData = $Cargrail->where(array('time' => strtotime($k)))->find()) {
                    $Cargrail->where(array('_id' => $oldData['_id']))->save($ret);
                } else {
                    $Cargrail->add($ret);
                }
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    protected function operate_data($get, $use)
    {

        if ($get > $use) {
            $data['get'] = $get - $use;
            $data['use'] = 0;
        } else {
            $data['get'] = 0;
            $data['use'] = $use - $get;
        }

        return $data;
    }
}
