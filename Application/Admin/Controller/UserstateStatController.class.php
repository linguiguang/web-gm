<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/9
 * Time: 13:45
 */
namespace Admin\Controller;
use Think\Controller;

/**
 * 玩家状态写入
 */
class UserstateStatController extends Controller{

    //角色表
    public function state_userinfo(){
        $stime=microtime(true); #获取程序开始执行的时间

        $user = D('Users')->where(array('id'=>array('egt', 150000)))->getField('id,state,from_id,devid,siteuser', true);
        echo "user completed \r\n";

        $Userinfo = D('Userinfo');

        $no = 0;
        foreach ($user as $k=>$v){
            $data = array();

            $data['state'] = intval($v['state']);
            $data['from_id'] = intval($v['from_id']);
            $data['devid'] = $v['devid'];
            $data['bind'] = intval($v['siteuser']);

            $Userinfo->where(array('uid'=>intval($v['id'])))->save($data);
            //echo $v['id'].'->'.$no++.PHP_EOL;
        }

        $etime=microtime(true);
        $total=round($etime-$stime);
        echo "Run {$total}s times".PHP_EOL;
    }

}

