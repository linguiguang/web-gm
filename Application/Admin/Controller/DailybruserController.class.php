<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/21
 * Time: 16:23
 */
namespace Admin\Controller;
use Think\Controller;

//百人玩家每日输赢
class DailybruserController extends Controller{

    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }
        $map['c_time'] = array('between', array($b_time, $e_time));

        $list = D('Brnnlog')->where($map)->select();

        $data = array();
        foreach ($list as $v){
            $stat = $this->operate_data($v['get'], $v['use']);
            $data[$v['uid']]['get'] += intval($stat['get']);
            $data[$v['uid']]['use'] += intval($stat['use']);
            $data[$v['uid']]['tax'] += intval($v['tax']);
            $data[$v['uid']]['pooladd'] += intval($v['pooladd']);
            $data[$v['uid']]['pool'] += intval($v['pool_win']);
            $data[$v['uid']]['char_id'] = $v['char_id'];
        }

        $Dailybruser = D('Dailybruser');
        foreach ($data as $k=>$v){
            $ret['c_time'] = $b_time;
            $ret['uid'] = $k;
            $ret['char_id'] = $v['char_id'];
            $ret['get'] = intval($v['get'] - $v['tax']);
            $ret['use'] = intval($v['use']);
            $ret['tax'] = intval($v['tax']);
            $ret['pooladd'] = intval($v['pooladd']);
            $ret['pool'] = intval($v['pool']);
            $ret['yinli'] = $ret['get'] + $ret['pool'] - $ret['use'];

            $dap['uid'] = $k;
            $dap['c_time'] = $b_time;
            if($info = $Dailybruser->where($dap)->find()){
                $Dailybruser->where(array('_id'=>$info['_id']))->save($ret);
            }else{
                $Dailybruser->add($ret);
            }
        }

        $etime = microtime(true);
        $totals = round($etime-$stime);
        echo 'Run '.$totals.' S';
    }

    //纯输赢
    protected function operate_data($get, $use){
        if($get > $use){
            $data['get'] = $get - $use;
            $data['use'] = 0;
        }else{
            $data['get'] = 0;
            $data['use'] = $use - $get;
        }
        return $data;
    }

}