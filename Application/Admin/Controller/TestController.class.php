<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/21
 * Time: 10:02
 */
namespace Admin\Controller;

use Think\Controller;

class TestController extends Controller
{

    const KEY = 10001121;
    const SECRET = 'VD3BlbQleqmyP8HasYB2iJBcen5cMLvT';

    public function mongo()
    {
        $mongo = D('Test');
        $data1 = $mongo->select();

        foreach ($data1 as $k => $v) {
            $ids[] = $k;
        }

        for ($i = 0; $i <= count($ids); $i++) {
            $data['_id'] = $ids[$i];
            $data['title'] = 111;
            $mongo->save($data);
        }

        $info = array(
            'title' => '666',
            'date' => NOW_TIME,
        );

        $id = $mongo->add($info);
        echo $id;
    }

    public function data()
    {
        $file = 'config/捕鱼商城数据表.xlsx';
        $file = iconv('UTF-8', 'GB2312', $file);

        $info = F('BUYU');
        if (!$info) {
            $data = format_excel2array($file, 3);
            for ($i = 7; $i <= count($data); $i++) {
                $info[$data[$i]['B']] = array(
                    $data[2]['B'] = $data[$i]['B'],
                    $data[2]['C'] = $data[$i]['C'],
                    $data[2]['D'] = $data[$i]['D'],
                    $data[2]['E'] = $data[$i]['E'],
                    $data[2]['F'] = $data[$i]['F'],
                    $data[2]['G'] = $data[$i]['G'],
                    $data[2]['H'] = $data[$i]['H'],
                );
            }
            F('BUYU', $info);
        }
        dump($info);
    }

    /**
     * 导出红包金额领取用户数量
     */
    public function exportred()
    {
        $map['goods_id'] = array('in', array(99015, 99016, 99017));
        $map['status'] = 1;
        $list = D('Shops')->where($map)->select();

        $money_arr = array(
            '99015' => 1,
            '99016' => 5,
            '99017' => 10,
        );

        $users = array();
        foreach ($list as $v) {
            $users[$v['uid']][] = $money_arr[$v['goods_id']];
        }

        $data = array();
        foreach ($users as $k => $v) {
            if (array_sum($v) >= 1) {
                $data['red1'][] = $k;
            }

            if (array_sum($v) >= 6) {
                $data['red6'][] = $k;
            }

            if (array_sum($v) >= 17) {
                $data['red17'][] = $k;
            }

            if (array_sum($v) >= 51) {
                $data['red51'][] = $k;
            }
        }

        $arr = array(
            '1' => count($data['red1']),
            '6' => count($data['red6']),
            '17' => count($data['red17']),
            '51' => count($data['red51']),
        );
        dump($arr);

    }

    //获取code
    public function getcode()
    {
        $code = I('code');
        if (!$code) {
            $url = 'https://oauth.qianmi.com/authorize?';
            $data = array(
                'client_id' => self::KEY, //appkey
                'response_type' => 'code', //获取code类型
                'redirect_uri' => 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], //回调地址
            );
            $request_url = $url . http_build_query($data);
            redirect($request_url);
        } else {
            $this->gettoken($code);
        }
    }

    //获取token
    public function gettoken($val, $type = 'code')
    {
        $url = "https://oauth.qianmi.com/token";
        $client_id = self::KEY;
        $appSecret = self::SECRET;
        $grant_type = "authorization_code";
        $data = array(
            'client_id' => $client_id,
            'grant_type' => $grant_type,
        );

        if ($type == 'code') {
            $data['code'] = $val;
        } elseif ($type == 'refresh_token') {
            $data['refresh_token'] = $val;
        }

        ksort($data);
        $plain_text = "";
        foreach ($data as $key => $value) {
            $plain_text .= $key . $value;
        }
        $plain_text = $appSecret . $plain_text . $appSecret;
        $sign = strtoupper(sha1($plain_text));
        $data['sign'] = $sign;
        ksort($data);
        $url_params = "";
        foreach ($data as $key => $value) {
            $url_params .= "&" . $key . "=" . $value;
        }
        $url_params = ltrim($url_params, "&");

        //curl初始化
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $url_params);
        $return = curl_exec($ch);

        //出错检测
        if (curl_errno($ch)) {
            echo "curl error:" . curl_errno($ch);
        } else {
            $result = json_decode($return, true);

            $info['access_token'] = $result['data']['access_token'];
            $info['expires_in'] = $result['data']['expires_in'];
            $info['refresh_token'] = $result['data']['refresh_token'];
            $info['operate_time'] = NOW_TIME;

            F('ACCESS_TOKEN', $info);

            if ($result['data']['access_token']) {
                $this->chongzhi();
            }

        }
        curl_close($ch);
    }

    //直充入口
    public function chongzhi()
    {
        $accessarr = F('ACCESS_TOKEN');
        if ($accessarr['access_token'] == '') {
            redirect('http://' . $_SERVER['HTTP_HOST'] . '/admin.php?s=/Test/getcode'); //无token数据
        } else {
            $operate_time = $accessarr['operate_time'];
            $expires_in = $accessarr['expires_in'];
            if ((NOW_TIME - $operate_time) > $expires_in) { //token时间过期
                $refresh_token = $accessarr['refresh_token'];
                $this->gettoken($refresh_token, 'refresh_token'); //刷新token
            } else {
                $access_token = $accessarr['access_token']; //获取access_token
            }
        }

        vendor('recharge.OpenClient');
        vendor('recharge.RechargeMobileCreateBillRequest');
        vendor('recharge.RechargeMobileGetItemInfoRequest');

        $client = new \OpenClient();
        $client->appKey = self::KEY;
        $client->appSecret = self::SECRET;

        //查询商品
        $reqitem = new \RechargeMobileGetItemInfoRequest();
        $reqitem->setMobileNo("15960050654");
        $reqitem->setRechargeAmount("10");
        $resitem = $client->execute($reqitem, $access_token);
        dump($resitem);exit;
        \Think\Log::write('ITEM JSON DATA:' . $resitem, 'ITEM DATA'); //商品接口

        //生成订单
        $req = new \RechargeMobileCreateBillRequest();
        $outid = rand(10000000, 99999999);
        $req->setItemId($outid); //商品编号
        $req->setMobileNo("15960050654");
        $req->setRechargeAmount("10");

        $res = $client->execute($req, $access_token);
        dump($res);
        $data = json_decode($res, true);

        \Think\Log::write('BILL JSON DATA:' . $res, 'BILL order'); //订单日志

        if ($data['status'] == 1) { //生成成功
            $billId = $data['data']['billId'];
            $this->recharge($access_token, $billId);
        } else {
            die($res->subErrors);
        }
    }

    //生成话费订单后调用此充值接口
    protected function recharge($access_token, $billId)
    {
        $client = new \OpenClient();
        $client->appKey = self::KEY;
        $client->appSecret = self::SECRET;
        $req = new \RechargeBasePayBillRequest();
        $req->setBillId($billId);
        $res = $client->execute($req, $access_token);
        $data = json_decode($res, true);
        \Think\Log::write('BILL RECHARGE DATA:' . $res, 'BILL RECHARGE'); //订单充值日志

        if ($data['status'] == 1) {
            if ($data['data']['payState'] == 1) {
                $this->success('支付成功');
            }
        } elseif ($data['errorToken']) {
            die($res->subErrors);
        }
    }

    public function zhichong()
    {
//        header("Content-type:text/html; charset=utf-8");
        vendor('qianmi.OpenSdk');

        $loader = new \QmLoader();
        $loader->autoload_path = array(CURRENT_FILE_DIR . DS . "client");
        $loader->init();
        $loader->autoload();

        $client = new \OpenClient();
        $client->appKey = self::KEY;
        $client->appSecret = self::SECRET;
        $accessarr = F('ACCESS_TOKEN');
        $accessToken = $accessarr['access_token'];

        /*$req  = new \RechargeMobileGetItemInfoRequest();
        $req  -> setFields("title,price");
        $req  -> setNumIid("p17080");
        $res  = $client ->  execute($req, $accessToken);*/

        $req = new \RechargeMobileCreateBillRequest();
        $outid = rand(10000000, 99999999);
        $req->setItemId($outid); //商品编号
        $req->setMobileNo("15960050654");
        $req->setRechargeAmount(10);

        $res = $client->execute($req, $accessToken);
        dump($res);

        print_r(json_encode($res));
    }

    //蛋蛋赚pc四期激活玩家
    public function newfrom()
    {
        $list = D('FromLogs')->where(array('from_id' => 50000220))->select();
        file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))) . "/newfrom.csv", '用户ID,赢取金币,时间', FILE_APPEND);
        $str = '';
        $Users = D('Users');
        foreach ($list as $v) {
            $wingold = $Users->where(array('id' => $v['user_id']))->getField('wingold');
            $date = date('Y-m-d H:i:s', $v['create_time']);
            $str .= sprintf('%d,%d,%s' . PHP_EOL, $v['user_id'], intval($wingold), $date);
        }
        file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))) . "/newfrom.csv", $str, FILE_APPEND);
    }

    //导某个渠道数据
    public function fromKey($from_id = 80000057)
    { //电信7期数据
        $map['adddate'] = array('elt', strtotime('2018-05-10 23:59:59'));
        $map['from_id'] = $from_id;
        $users = D('Users')->where($map)->field('id,username')->select();

        //红包id
        $redIds = array(99015, 99016, 99017, 99018, 99019, 99020, 99021, 99022, 99050);
        $redLogs = D('Shops')->select();
        $redUsers = array();
        foreach ($redLogs as $v) {
            if (in_array($v['goods_id'], $redIds)) {
                $redUsers[$v['uid']] = 1;
            }
        }
        file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))) . "/redUsers1.csv", '用户ID,绑定手机号,是否兑换红包', FILE_APPEND);
        $str = '';
        foreach ($users as $v) {
            $mobile = is_numeric($v['username']) ? $v['username'] : '无';
            $exc = $redUsers[$v['id']] ? '是' : '否';
            $str .= sprintf('%s,%s,%s' . PHP_EOL, $v['id'], $mobile, $exc);
        }

        file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))) . "/redUsers1.csv", $str, FILE_APPEND);

    }

    //导出某天领取的红包数据
    public function dayRedpacket()
    {
        //红包id
        $redIds = array(99015, 99016, 99017, 99018, 99019, 99020, 99021, 99022, 99050);
        $redLogs = D('Shops')->where(array('status' => 1))->select();
        //配置
        $config = D('ConfigByshop')->getConfig();
        var_dump($config);
        //用户信息
        $userInfo = D('Userinfo')->field('uid,nickname,c_time,pays,gold_stat')->select();
        $chars = [];
        foreach ($userInfo as $v) {
            $chars[$v['uid']] = $v;
        }
        unset($userInfo);
        echo 'userinfo ok' . PHP_EOL;

        $redUsers = $dayUsers = array();
        foreach ($redLogs as $v) {
            if (!in_array($v['goods_id'], $redIds)) {
                continue;
            }

            $redUsers[$v['uid']] += $config[$v['goods_id']]['item_num'];
            if ($v['c_time'] >= strtotime('2018-04-15') && $v['c_time'] <= strtotime('2018-04-15 23:59:59')) {
                $dayUsers[$v['uid']] += $config[$v['goods_id']]['item_num'];
            }
        }
        echo 'shop data ok' . PHP_EOL;
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/15dayRedpacket.csv", '用户ID,用户昵称,当日兑换红包数量,历史兑换红包数量,金币输赢,充值金额,注册时间' . PHP_EOL, FILE_APPEND);

        $str = '';
        foreach ($dayUsers as $uid => $money) {
            $str .= sprintf('%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $uid, $chars[$uid]['nickname'], $money, $redUsers[$uid], $chars[$uid]['gold_stat'], $chars[$uid]['pays'], date('Y-m-d H:i:s', $chars[$uid]['c_time']));
        }

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/15dayRedpacket.csv", $str, FILE_APPEND);
    }

    public function newrecharge()
    {

        /*$pap['from_id'] = array('gt', 0);
        $pap['state'] = 3;
        $money = D('Paylog')->where($pap)->field('uid,money,addtime')->select();
        F('RECHARGE', $money);*/

        $no = 0;
        $paylogs = F('RECHARGE');
        $pyauidlist = array();
        foreach ($paylogs as $v) {
            $pyauidlist[$v['uid']][$v['addtime']] = $v['money'];
        }
//        dump($pyauidlist);

        $Users = D('Users');
        $uids = $Users->where(array('bind_time' => array('gt', 0)))->getField('id,bind_time', true);

        $pays = $list = array();

        foreach ($uids as $k => $v) {
            $money = 0;
            if ($pyauidlist[$k]) {
                foreach ($pyauidlist[$k] as $key => $val) {
                    if ($key < $v) {
                        $money += $val;
                    }
                }
                echo $k . '=>' . $no++ . PHP_EOL;
                $Users->where(array('id' => $k))->save(array('recharge' => $money));
            }
        }

    }

    //新注册的玩家信息列表
    public function newuser()
    {
        $b_time = strtotime('2018-01-08');
        $e_time = NOW_TIME;

        $uap['adddate'] = array('between', array($b_time, $e_time));
        $uids = D('Users')->where($uap)->getField('id,username,state,ip,lastlogin,lastloginip,from_id,adddate,wingold,login', true);

        $userinfo = D('Userinfo')->where(array('c_time' => array('between', array($b_time, $e_time))))->select();
        foreach ($userinfo as $v) {
            $char[$v['uid']] = $v;
        }

        $pap['state'] = 3;
        $pap['addtime'] = array('between', array($b_time, $e_time));
        $pays = D('Paylog')->where($pap)->field('sum(money) as recharge,uid')->group('uid')->select();

        foreach ($pays as $v) {
            $paylist[$v['uid']] = $v['recharge'];
        }

        $froms = D('DChannels')->getField('from_id, from_name', true);

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/newuser.csv", '用户ID,用户账号,身上金币,角色ID,对应角色,来源,创建时间,创建ip,登录次数,最后登录时间,最后登录ip,状态' . PHP_EOL, FILE_APPEND);

        $str = '';
        foreach ($uids as $k => $v) {
            $str .= sprintf('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $k, $v['username'], intval($char[$k]['gold']), $char[$k]['char_id'], $char[$k]['nickname'], $froms[$v['from_id']], date('Y-m-d H:i:s', $v['adddate']), $v['ip'], $v['login'], date('Y-m-d H:i:s', $v['lastlogin']), $v['lastloginip'], $v['state'] == 1 ? '启用' : '禁用');
        }

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/newuser.csv", $str, FILE_APPEND);
    }

    //
    public function reguser($fromid = 0)
    {
        $b_time = strtotime(date('Y-m-d 00:00:00'));
        //$e_time = strtotime('2018-01-17')+86399;
        $e_time = NOW_TIME;

        $map['adddate'] = array('between', array($b_time, $e_time));
        $map['from_id'] = intval($fromid);
        $users = D('Users')->where($map)->getField('id,username,from_id,ip,lastloginip,login,adddate', true);

        $cmap['c_time'] = array('between', array(intval($b_time), intval($e_time)));
        $userinfo = D('Userinfo')->where($cmap)->select();
        foreach ($userinfo as $v) {
            $chars[$v['uid']] = $v;
        }
        unset($userinfo);

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/reguser_" . $fromid . ".csv", '用户ID,昵称,身上金币,身上钥匙,红包获得,金币输赢统计,钥匙输赢统计,充值金额,注册ip,登录ip,登录次数,注册时间' . PHP_EOL, FILE_APPEND);

        $str = '';
        foreach ($users as $k => $v) {
            $str .= sprintf('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $k, $chars[$k]['nickname'], $chars[$k]['gold'], $chars[$k]['diamond'], $chars[$k]['redbag'] / 10, $chars[$k]['gold_stat'], $chars[$k]['area_5'], $chars[$k]['pays'], $v['ip'], $v['lastloginip'], $v['login'], date('Y-m-d H:i:s', $v['adddate']));
        }

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/reguser_" . $fromid . ".csv", $str . PHP_EOL, FILE_APPEND);
    }

    //最新一期玩家数据
    public function newNperUsers()
    {

        $map['from_id'] = array('between', array(80000000, 89999999));
        /*$b_time = strtotime('2018-01-18');
        $e_time = $b_time + 86399;
        $map['adddate'] = array('between', array($b_time, $e_time));
        $map['state'] = 1;*/
        $uids = D('Users')->where($map)->getField('id,from_id,wingold,recharge,adddate', true);

        $userInfo = D('Userinfo')->field('uid,nickname,wingold,pays,play_num,bet_gold,gold,diamond')->select();
        foreach ($userInfo as $v) {
            $char[$v['uid']] = $v;
        }
        unset($userinfo);

        //19号登录玩家
        /*$login_map['login_time'] = array('between', array($b_time+86400, $e_time+86400));
        $login_map['type'] = 1;
        $login_map['uid'] = array('egt', 150000);
        $login_list = D('arealogin')->where($login_map)->select();
        foreach($login_list as $val) {
        $arr_login[$val['uid']] = $val['uid'];
        }
        unset($login_list);*/

        /*$paylist = array();
        $pap['state'] = 3;
        $pap['addtime'] = array('between', array($b_time, $e_time));
        $pays = D('Paylog')->where($pap)->select();
        foreach ($pays as $v){
        if(in_array($v['saletype'], array(70001, 70002)));
        $paylist[$v['uid']] += 1;
        }*/

        //押注数据
        $newUserList = D('Activatelogs')->getField('user_id,from_id,bet_gold,wingold,recharge,bind_time', true);

        $froms = D('DChannels')->getField('from_id, from_name', true);

//        file_put_contents(dirname(dirname(dirname(__DIR__)))."/newnperuser.csv", '用户ID,累计赢取金币,累计充值金额,所属渠道,注册时间'.PHP_EOL, FILE_APPEND);
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/newnperuser.csv", '用户ID,用户昵称,渠道,累计下注,充值金额,注册时间' . PHP_EOL, FILE_APPEND);

        $str = $liu = '';
        foreach ($uids as $k => $v) {
//            $liu = $arr_login[$k] ? '是' : '否';
            $str .= sprintf('%s,%s,%s,%s,%s,%s' . PHP_EOL, $k, $char[$k]['nickname'], $froms[$v['from_id']], $char[$k]['bet_gold'] - $newUserList[$k]['bet_gold'], $char[$k]['pays'] - $v['recharge'], date('Y-m-d H:i:s', $v['adddate']));
        }
//        $strAfter = iconv('UTF-8','GB2312', $str);

//        file_put_contents(dirname(dirname(dirname(__DIR__)))."/newnperuser.csv", $str, FILE_APPEND);
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/newnperuser.csv", $str, FILE_APPEND);
    }

    //红包兑换记录
    public function redPacketLogs()
    {
        $goodsArr = [
            99015 => 1,
            99016 => 5,
            99017 => 2,
            99018 => 5,
            99019 => 50,
            99020 => 50,
            99021 => 150,
            99022 => 200,
        ]; //红包

        file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))) . "/redbag.csv", '用户ID,玩家名称,红包金额,所属渠道,兑换时间' . PHP_EOL, FILE_APPEND);

        $uids = D('Users')->getField('id, from_id', true);
        $froms = D('DChannels')->getField('from_id, from_name', true);

        $map['status'] = 1;
        $map['c_time'] = array('between', array(strtotime('2017-11-14'), NOW_TIME));
        $list = D('Shops')->where($map)->select();
        $str = '';
        foreach ($list as $v) {
            if (!in_array($v['goods_id'], array_keys($goodsArr))) {
                continue;
            }

            $str .= sprintf('%s,%s,%s,%s,%s' . PHP_EOL, $v['uid'], $v['truename'], $goodsArr[$v['goods_id']], $froms[$uids[$v['uid']]], date('Y-m-d H:i:s', $v['c_time']));
        }

        file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))) . "/redbag.csv", $str, FILE_APPEND);
    }

    //玩家goldGet、goldUse金币输赢
    public function otherRoomGoldStat($uid = 462211)
    {
//        $otherRoom = [10, 11, 20, 21, 22, 107, 133, 108, 1006, 1003];
        $getList = D('Goldget')->where(array('uid' => $uid))->select();
        $data = array();
        foreach ($getList as $v) {
//            if(in_array($v['room'], $otherRoom)) continue;
            $data[$v['room']]['get'] += $v['gold'];
        }

        $useList = D('Golduse')->where(array('uid' => $uid))->select();
        foreach ($useList as $v) {
//            if(in_array($v['room'], $otherRoom)) continue;
            $data[$v['room']]['use'] += $v['gold'];
        }

        $total = 0;
        foreach ($data as $k => $v) {
            $data[$k]['stat'] = $v['get'] - $v['use'];
            $total += $v['get'] - $v['use'];
        }
        ksort($data);
        dump($data);
        var_dump($total);
    }

    public function brnnStat($uid = 462211)
    {
        $list = D('Brnnlog')->where(array('uid' => $uid))->select();
        foreach ($list as $v) {
            $data['get'] += $v['get'] * 0.96;
            $data['use'] += $v['use'];
            $data['pool_win'] += $v['pool_win'];
            $data['stat'] += $v['get'] * 0.96 - $v['use'];
        }
        dump($data);
    }

    public function zzb()
    {
        $uids = D('FromLogs')->where(array('from_id' => 50000008))->getField('user_id', true);
        $list = D('Users')->where(array('id' => array('in', $uids)))->getField('id,wingold', true);
        dump($list);
        foreach ($list as $k => $v) {
            if ($v >= 3000000000) {
                echo $k . '-->' . PHP_EOL;
            }
        }
    }

    public function diamond()
    {
        $b_time = strtotime('2017-12-06');
        $e_time = $b_time + 86399;
        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['isrobot'] = 0;

        $list = D('Qrredbag')->where($map)->select();
        foreach ($list as $v) {
            $ret['get'] += $v['get'];
            $ret['use'] += $v['use'];
            $ret['sui'] += $v['get'] / 3;
        }

        dump($ret);
    }

    public function fruitGoldStat()
    {
        $b_time = strtotime('2017-12-01');
        $e_time = NOW_TIME;
        $roomArr = array(107, 126, 127);

        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['room'] = array('in', $roomArr);

        $list = D('Goldstat')->where($map)->select();
        $data = array();
        $stat = 0;
        foreach ($list as $v) {
            $data[$v['room']]['get'] += $v['win'];
            $data[$v['room']]['use'] += $v['lose'];
            $stat += $v['win'] - $v['lose'];
        }
        dump($data);
        var_dump($stat);
    }

    public function brPoolWin()
    {
        $b_time = strtotime('2017-12-28 17:00:00');
        $e_time = $b_time + 3600;
        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = D('Brnnlog')->where($map)->select();
        foreach ($list as $v) {
            $pool += $v['pool_win'];
        }
        var_dump($pool);
    }

    public function diamonds()
    {
        $b_time = strtotime('2017-12-28');
        $e_time = $b_time + 86399;
        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = D('Qrredbag')->where($map)->select();
        foreach ($list as $v) {
            $info = $this->operate($v['get'], $v['use']);
            $ret['get'] += $info['get'] - $v['tax'];
            $ret['use'] += $info['use'];
        }
        dump($ret);
    }

    protected function operate($get, $use)
    {
        if ($get > $use) {
            $data['get'] = $get - $use;
            $data['use'] = 0;
        } else {
            $data['get'] = 0;
            $data['use'] = $use - $get;
        }
        return $data;
    }

    //看牌抢庄玩家人数和局数
    public function kpUsersPlay()
    {
        $b_time = strtotime('2018-01-01');
        $e_time = $b_time + 86399;
        $map['c_time'] = array('between', array($b_time, $e_time));
        $list = D('Kplog')->where($map)->select();
        $data = array();
        foreach ($list as $v) {
            $data[$v['uid']] += 1;
        }
        arsort($data);

        $str = '';
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/kpuser.csv", '用户ID,局数' . PHP_EOL, FILE_APPEND);

        foreach ($data as $k => $v) {
            $str .= sprintf('%d,%d' . PHP_EOL, $k, $v);
        }

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/kpuser.csv", $str, FILE_APPEND);

    }

    //12月30号 15点后进来的玩家红包场数据
    public function redUsers()
    {
        $b_time = strtotime('2017-12-28 15:00:00');
        $e_time = NOW_TIME;
        $map['first_intime'] = array('between', array($b_time, $e_time));
        $qrUser = D('Qruser')->where($map)->select();

        $uap['c_time'] = array('between', array($b_time, $e_time));
        $userInfo = D('Userinfo')->field('uid,pays,nickname,from_id,c_time')->where($uap)->select();
        $users = array();
        foreach ($userInfo as $v) {
            $users[$v['uid']] = $v;
        }
        unset($userInfo);

        $froms = D('DChannels')->getField('from_id, from_name', true);

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/redbag.csv", '用户ID,玩家名称,钥匙获得,钥匙支出,总盈利,红包获得,渠道,注册时间' . PHP_EOL, FILE_APPEND);

        $str = '';
        foreach ($qrUser as $v) {
            if (!$users[$v['uid']]) {
                continue;
            }

            $str .= sprintf('%s,%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $v['uid'], $users[$v['uid']]['nickname'], $v['get'], $v['use'], $v['stat'], $v['redbag'] / 10, $froms[$users[$v['uid']]['from_id']], date('Y-m-d H:i:s', $users[$v['uid']]['c_time']));
        }

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/redbag.csv", $str, FILE_APPEND);

    }

    //渠道数据核对
    public function fromCheck()
    {
        $file = 'config/nper5.xlsx';
        $_config = F('NPER5');
        if ($_config == null) {
            $data = format_excel2array($file, 0);
            for ($i = 2; $i <= count($data); $i++) {
                if ($data[$i]['B']) {
                    $info[] = $data[$i]['B'];
                }
            }
            $_config = $info;
            F('NPER5', $info);
        }
        echo count($_config) . PHP_EOL;

        $from_id = 60000040; //每天赚点

        //取出角色数据
        //        $cap['from_id'] = $from_id;
        $char_list = D('Userinfo')->where($cap)->field('uid,wingold,bind,paly_three,c_time,pays,from_id')->select();
        $chars = array();
        foreach ($char_list as $v) {
            $chars[$v['uid']] = $v;
        }
        unset($char_list);

        //取出用户数据
        $user_list = D('Users')->where($cap)->getField('id,from_id,wingold,recharge,bind_time', true);
        $data = $data1 = array();
        foreach ($user_list as $k => $v) {

            $uid = intval($k);
            $from_id = intval($v['from_id']);
            $last_wingold = intval($v['wingold']);
            $last_recharge = intval($v['recharge']);

//            $golds = intval($chars[$uid]['wingold']) - $last_wingold;
            $pays = intval($chars[$uid]['pays']) - $last_recharge;

            if ($pays >= 10) {
                if ($from_id == 60000040) {
                    $data1[] = $uid;
                }
                $data[$uid] = $pays;
            }
        }
        dump($data);
        echo count($data1);

        $datas = array_diff($_config, $data1);
        $froms = D('DChannels')->getField('from_id, from_name', true);

        $str = '';
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/qudaodata.csv", '用户ID,渠道,充值' . PHP_EOL, FILE_APPEND);
        foreach ($datas as $v) {
            $str .= sprintf('%s,%s,%s' . PHP_EOL, $v, $froms[$chars[$v]['from_id']], $data[$v]);
        }
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/qudaodata.csv", $str, FILE_APPEND);

    }

    public function num()
    {
        $b_time = strtotime('2018-01-05');
        $e_time = $b_time + 86399;
        $map['c_time'] = array('between', array($b_time, $e_time));
        $logs = D('Fruitlog')->where($map)->count();
        dump($logs);
    }

    public function everyDayUser()
    {
        $b_time = strtotime('2017-01-01');
        $e_time = strtotime('2018-01-07 23:59:59');
        $map['c_time'] = array('between', array($b_time, $e_time));
        $dailyFruit = D('Dailyfruituser')->where($map)->select();
        $froms = D('DChannels')->getField('from_id,from_name', true);
        $from_ids = D('Users')->getField('id,from_id', true);

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/fruit.csv", '用户ID,金币获得,金币消耗,彩池盈利,总盈利,所属渠道,时间' . PHP_EOL, FILE_APPEND);
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/brnn.csv", '用户ID,金币获得,金币消耗,彩池盈利,总盈利,所属渠道,时间' . PHP_EOL, FILE_APPEND);

        $strFruit = $strBrnn = '';
        foreach ($dailyFruit as $k => $v) {
            $strFruit .= sprintf('%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $v['uid'], $v['get'], $v['use'], $v['pool'], $v['yinli'], $froms[$from_ids[$v['uid']]], date('Y-m-d', $v['c_time']));
        }
        unset($dailyFruit);
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/fruit.csv", $strFruit, FILE_APPEND);

        $dailyBrnn = D('Dailybruser')->where($map)->select();
        foreach ($dailyBrnn as $k => $v) {
            $strBrnn .= sprintf('%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $v['uid'], $v['get'], $v['use'], $v['pool'], $v['yinli'], $froms[$from_ids[$v['uid']]], date('Y-m-d', $v['c_time']));
        }
        unset($dailyBrnn);
        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/brnn.csv", $strBrnn, FILE_APPEND);
    }

    //导数据工具
    public function outputdata()
    {
        $dir = dirname(dirname(dirname(dirname(__FILE__))));

        //时间
        $b_time = strtotime('2017-01-01');
        $e_time = strtotime('2017-12-31 23:59:59');

        $str_arr = array(
            'gold_get' => '时间,看牌抢庄,百人牛牛,水果狂欢,豪车,注册,绑定,商城,任务奖励,红包广场,红包广场超时,其它',
            'gold_use' => '时间,看牌抢庄,百人牛牛,水果狂欢,豪车,注册,绑定,商城,任务奖励,红包广场,红包广场超时,其它',
            'stat' => '日期,总注册,有效注册,手机绑定,首日有效转化率,用户登录活跃数,充值金额,充值人数,首日充值金额,首日充值人数,付费率,首日付费日,ARPU值,首日ARPU值,收入/注册,收入/活跃,首日付费/有效注册,次日留存,3日留存,4日留存,5日留存,6日留存,7日留存,14日留存',
            'recharge' => '时间,充值人数,充值笔数,（去除重复）人数,金额',
        );

        foreach ($str_arr as $k => $v) {
            file_put_contents("{$dir}/{$k}.csv", $v . PHP_EOL, FILE_APPEND);
        }

        //金币
        $map['c_time'] = array('between', array($b_time, $e_time));
        $golds = D('Goldstat')->where($map)->order('c_time asc')->select();

//        $task_arr = array(10, 11, 12, 13, 14, 15, 16, 35, 45);
        foreach ($golds as $v) {
            $date = date('Y-m-d', $v['c_time']);
            if ($v['room'] == 10) {
                $data[$date]['kp']['get'] = $v['win'];
                $data[$date]['kp']['use'] = $v['lose'];
            } elseif ($v['room'] == 20) {
                $data[$date]['br']['get'] = $v['win'];
                $data[$date]['br']['use'] = $v['lose'];
            } elseif ($v['room'] == 107) {
                $data[$date]['laba']['get'] = $v['win'];
                $data[$date]['laba']['use'] = $v['lose'];
            } elseif ($v['room'] == 133) {
                $data[$date]['car']['get'] = $v['win'];
                $data[$date]['car']['use'] = $v['lose'];
            } elseif ($v['room'] == 131) {
                $data[$date]['reg']['get'] = $v['win'];
                $data[$date]['reg']['use'] = $v['lose'];
            } elseif ($v['room'] == 112) {
                $data[$date]['bind']['get'] = $v['win'];
                $data[$date]['bind']['use'] = $v['lose'];
            } elseif ($v['room'] == 104) {
                $data[$date]['shop']['get'] = $v['win'];
                $data[$date]['shop']['use'] = $v['lose'];
            } elseif ($v['room'] == 103) {
                $data[$date]['task']['get'] = $v['win'];
                $data[$date]['task']['use'] = $v['lose'];
            } elseif ($v['room'] == 108) { //红包广场
                $data[$date]['red']['get'] = $v['win'];
                $data[$date]['red']['use'] = $v['lose'];
            } elseif ($v['room'] == 1006) {
                $data[$date]['hbchaoshi']['get'] = $v['win'];
                $data[$date]['hbchaoshi']['use'] = $v['lose'];
            } else {
                $data[$date]['other']['get'] += $v['win'];
                $data[$date]['other']['use'] += $v['lose'];
            }
        }

        $strget = $struse = '';
        foreach ($data as $k => $v) {
            $strget .= sprintf('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $k, $v['kp']['get'], $v['br']['get'], $v['laba']['get'], $v['car']['get'], $v['reg']['get'], $v['bind']['get'], $v['shop']['get'], $v['task']['get'], $v['red']['get'], $v['hbchaoshi']['get'], $v['other']['get']);
            $struse .= sprintf('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $k, $v['kp']['use'], $v['br']['use'], $v['laba']['use'], $v['car']['use'], $v['reg']['use'], $v['bind']['use'], $v['shop']['use'], $v['task']['use'], $v['red']['use'], $v['hbchaoshi']['use'], $v['other']['use']);
        }
        file_put_contents("{$dir}/gold_get.csv", $strget, FILE_APPEND);
        file_put_contents("{$dir}/gold_use.csv", $struse, FILE_APPEND);
        echo 'gold ok' . PHP_EOL;

        //留存
        $sap['from_id'] = 999;
        $sap['create_time'] = array('between', array($b_time, $e_time));
        $stats = D('Stat')->where($sap)->order('create_time asc')->select();
        $str = '';
        foreach ($stats as $v) {
            $date = date('Y-m-d', $v['create_time']);
            $str .= sprintf('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' . PHP_EOL, $date, $v['all_reg_count'], $v['all_reg_bind'], $v['all_cha_count'], ($v['zh_rate'] * 100) . '%', $v['user_login'], $v['pay_money'], $v['pay_count'], $v['first_pay_money'], $v['first_pay_count'], ($v['pay_rate'] * 100) . '%', ($v['first_pay_rate'] * 100) . '%', $v['pay_arpu'], $v['fist_pay_arpu'], $v['pay_reg'], $v['pay_login'], $v['first_pay_cha'], ($v['day_1'] * 100) . '%', ($v['day_2'] * 100) . '%', ($v['day_3'] * 100) . '%', ($v['day_4'] * 100) . '%', ($v['day_5'] * 100) . '%', ($v['day_6'] * 100) . '%', ($v['day_13'] * 100) . '%');
        }
        file_put_contents("{$dir}/stat.csv", $str, FILE_APPEND);
        echo 'stat ok' . PHP_EOL;

        //充值统计
        $pap['addtime'] = array('between', array($b_time, $e_time));
        $pap['state'] = 3;
        $paylog = D('Paylog')->where($pap)->order('addtime asc')->select();

        $ret = array();
        $str = '';
        foreach ($paylog as $k => $v) {
            $date = date('Y-m-d', $v['addtime']);
            $ret[$date]['money'] += intval($v['money']);
            $ret[$date]['recharge_num'] += 1;
            $ret[$date]['userarr'][] = $v['uid'];
        }
        foreach ($ret as $k => $v) {
            $str .= sprintf('%s,%d,%d,%d,%d' . PHP_EOL, $k, $v['recharge_num'], $v['recharge_num'], count(array_unique($v['userarr'])), $v['money']);
        }
        file_put_contents("{$dir}/recharge.csv", $str, FILE_APPEND);
        echo 'recharge ok' . PHP_EOL;

    }

    public function arrPost()
    {
        $postUrl = 'http://' . C('GAME_IN_IP') . ':' . C('HTTP_PORT') . '/cgi-bin/gm_oprate:add_storage';

        $postUrl1 = 'http://niuniumanage.com/admin.php?s=/Test/receive';
        $data['num'] = 3;
        $data['list'] = array(1, 2, 3);
        $ret = PostUrl($postUrl, $data, 1);
        var_dump($ret);
    }

    public function receive()
    {
        $data = json_encode(I(''));
//        $log .= 'time->'.time().$data
        file_put_contents(dirname(dirname(dirname(__DIR__))) . '/receive.txt', $data . PHP_EOL, FILE_APPEND);
    }

    public function activatelogs()
    {
        $list = D('Activatelogs')->select();
        var_dump($list);
    }

    public function shareStat()
    {
        $logs = D('Share')->select();
        $shareUsers = D('Shareusers');
        $data = array();
        foreach ($logs as $v) {
            $data[$v['uid']][$v['type']] += 1;
        }
        $no = 0;
        foreach ($data as $char_id => $type) {
            $ret = array();
            $ret['char_id'] = $char_id;
            $ret['type1'] = intval($type[1]);
            $ret['type2'] = intval($type[2]);

            if ($info = $shareUsers->where(array('char_id' => $char_id))->find()) {
//                $shareUsers->where(array('_id'=>$info['_id']))->save($ret);
            } else {
//                $shareUsers->add($ret);
            }
            echo $no++ . PHP_EOL;
        }

    }

    public function delData()
    {
//        D('Qruser')->where(array('isrobot'=>1))->delete();
    }

    public function brnnlog()
    {
        $b_time = strtotime('2018-03-27');
        $e_time = $b_time + 86399;
        $list = D('Brnnlog')->where(array('c_time' => array('between', array($b_time, $e_time))))->select();
        $data = array();
        foreach ($list as $v) {
            $data['get'] += $v['get'];
            $data['use'] += $v['use'];
            $data['pooladd'] += $v['pooladd'];
            $data['tax'] += $v['tax'];
            $data['pool_win'] += $v['pool_win'];
        }
        var_dump($data);
    }

    public function online()
    {
        $online = D('Online');
        $etime = strtotime(date('Y-m-d', NOW_TIME)) + 86400;
        $stime = $etime - 86400 * 3;
        $list = $online->where(array('c_time' => array('between', array($stime, $etime))))->order('c_time asc')->select();
        $data = $return = array();
        foreach ($list as $v) {
            $day = date('Y-m-d', $v['c_time']);
            if ($v['c_time'] >= $stime && $v['c_time'] < ($stime + 1800)) {
                $data[$day][$stime]['totals'] += $v['totals'];
                $data[$day][$stime]['num'] += 1;
            } else {
                $stime += 1800;
            }
        }

        foreach ($data as $day => $time) {
            foreach ($time as $val) {
                $return[$day][] = intval($val['totals'] / $val['num']);
            }

        }

    }

    public function gold($uid = 727602)
    {
        $getlist = D('Goldget')->where(array('uid' => $uid))->select();
        $uselist = D('Golduse')->where(array('uid' => $uid))->select();
        $get = $use = 0;
        $data = array();
        foreach ($getlist as $v) {
            $get += $v['gold'];
            $data['get'][$v['room']] += $v['gold'];
        }
        foreach ($uselist as $v) {
            $use += $v['gold'];
            $data['use'][$v['room']] += $v['gold'];
        }
        var_dump($get, $use, $get - $use);

        $lalb = D('Fruitlog')->where(array('uid' => $uid))->select();
        foreach ($lalb as $v) {
            $data['laba']['get'] += $v['get'];
            $data['laba']['use'] += $v['use'];
            $data['laba']['pooladd'] += $v['pooladd'];
            $data['laba']['tax'] += $v['tax'];
            $data['laba']['pool_win'] += $v['pool_win'];
        }

        $brnn = D('Brnnlog')->where(array('uid' => $uid))->select();
        foreach ($brnn as $v) {
            $data['br']['get'] += $v['get'];
            $data['br']['use'] += $v['use'];
            $data['br']['pooladd'] += $v['pooladd'];
            $data['br']['tax'] += $v['tax'];
            $data['br']['pool_win'] += $v['pool_win'];
        }

        $kp = D('Kplog')->where(array('uid' => $uid))->select();
        foreach ($kp as $v) {
            $data['kp']['get'] += $v['get'];
            $data['kp']['use'] += $v['use'];
            $data['kp']['tax'] += $v['tax'];
            $data['kp']['inmoney'] += $v['inmoney'];
        }
        var_dump($data);
    }

    public function timegold()
    {
        $b_time = strtotime('2018-03-27');
        $e_time = $b_time + 86399;
        $map['c_time'] = array('between', array($b_time, $e_time));
        /*$getlist = D('Goldget')->where($map)->select();
        $uselist = D('Golduse')->where($map)->select();
        $get = $use = 0;
        $data = array();
        foreach ($getlist as $v){
        if(!in_array($v['room'], array(20, 21, 22))) continue;
        //            $get += $v['gold'];
        $data['get'][$v['room']] += $v['gold'];
        }
        foreach ($uselist as $v){
        if(!in_array($v['room'], array(20, 21, 22))) continue;
        //            $use += $v['gold'];
        $data['use'][$v['room']] += $v['gold'];
        }*/

        $brList = D('Brnnlog')->where($map)->select();
        $get = 0;
        foreach ($brList as $v) {
            /*$data['br']['get'] += $v['get'];
            $data['br']['use'] += $v['use'];
            $data['br']['pooladd'] += $v['pooladd'];
            $data['br']['tax'] += $v['tax'];
            $data['br']['pool_win'] += $v['pool_win'];*/
            if ($v['get'] > $v['use']) {
                $get += ($v['get'] - $v['use']);
            }
        }
        var_dump($get * 0.06);
    }

    //红包金额
    public function redUse()
    {
        $b_time = strtotime('2018-01-01');
        $e_time = strtotime('2018-03-31 23:59:59');
        $map['create_time'] = array('between', array($b_time, $e_time));
        $map['status'] = 1;
        echo D('RedpacketLogs')->where($map)->count();
        /*$list = D('RedpacketLogs')->where($map)->select();
    $data = $money = array();
    $money = 0;
    //        $config = D('ConfigByshop')->getConfig();

    //        var_dump($config);exit;
    $cou = count($list);
    $i = 1;
    foreach ($list as $val){
    $ret = json_decode($val['result'], true);
    $money += intval($ret['total_amount']);
    echo $cou."\t".$i++."\t".$money."\t".$ret['total_amount'].PHP_EOL;
    }
    var_dump($money);*/
    }

    public function PalyTime()
    {

        $Arealogin = D('Arealogin');
        $dtime = NOW_TIME;
        $b_time = strtotime(date("Y-m-d H:i:s", strtotime("-1 hour")));
        $e_time = strtotime(date("Y-m-d H:i:s", time()));
        $map['login_time'] = array('between', array($b_time, $e_time));
        $map['type'] = 1;
        $map['uid'] = array('egt', 0); //2019-6-17以后注册的用户ID
        $list = $Arealogin->where($map)->field('uid,times')->select();
        $list1 = $list;
        foreach ($list as $v) {
            $times = 0; //在线时长
            foreach ($list1 as $v1) {
                if ($v['uid'] == $v1['uid']) {$times += $v1['times'];}
            }

            if ($times >= 180) { //180试玩3分钟
                $data1['id'] = $v['uid'];
                $data['time_long'] = 5;
                D('Test1')->where($data1)->save($data);
            }
        }

    }
    //豪车试玩5局 数据库要新增time_long4
    public function PalyTime4()
    {
        /*
    $Arealogin = D('carlog');
    $b_time=1558076198;//2019-05-18 09:0:0
    $e_time=1565956627;//2019-08-17 09:0:0
    $map['c_time'] = array('between', array($b_time, $e_time));
    $map['uid'] = array('egt', 200000);//屏蔽旧人
    //$map['uid'] = 213939;
    $list = $Arealogin->where($map)->field('uid')->select();

    $map1  = array();

    foreach ($list as $v1){
    $val = $map1[$v1['uid']];
    if (!$val && $val != 0 ) {
    $map1[$v1['uid']] = 0;

    } else {
    $map1[$v1['uid']] = $val + 1;

    }
    }

    foreach ($map1 as $k => $v) {
    //echo "\$map1[$k] => $v.\n";
    $data1['id'] =$k;
    $data['time_long4'] = $map1[$k];
    D('Test1')->where($data1)->save($data);
    }

     */
    }

    //导出某天领取的红包数据
    public function test()
    {
        $userinfo = D('Userinfo')->select();
        foreach ($userinfo as $v) {
            $chars[$v['uid']] = $v;
        }
        unset($userinfo);
        $newUserList = D('Activatelogs')->getField('user_id,from_id,bet_gold,wingold,recharge,bind_time', true);

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/user.csv", '用户ID,金币,累计下注,累计赢取,充值金额,渠道ID,' . PHP_EOL, FILE_APPEND);

        $str = '';
        $users = D('Users')->getField('id,username,from_id,ip,lastloginip,login,adddate', true);
        foreach ($users as $k => $v) {
            $uid = intval($k);
            $from_id = intval($v['from_id']);
            if ($from_id == 4000005) {
                $betGolds = intval($chars[$uid]['bet_gold']) - intval($newUserList[$uid]['bet_gold']);
                $winGolds = intval($chars[$uid]['wingold']) - intval($newUserList[$uid]['wingold']);
                $str .= sprintf('%s,%s,%s,%s,%s,%s' . PHP_EOL, $k, $chars[$uid]['gold'], $betGolds, $winGolds, $chars[$uid]['pays'], $chars[$uid]['from_id']);
            }
        }

        file_put_contents(dirname(dirname(dirname(__DIR__))) . "/user.csv", $str . PHP_EOL, FILE_APPEND);
    }

    //核对用户log
    public function testlog()
    {
        $Carlog = D('Carlog');
        $loglist = $Carlog->where(array('uid' => 172582))->select();
        $get = $use = 0;
        foreach ($loglist as $v) {
            if (intval($v['test_type']) == 1) {
                if ($v['get'] > $v['use']) {
                    $get += intval($v['get'] + $v['pool_win'] - $v['use'] - $v['tax']);
                    $use += 0;
                } else {
                    if ($v['get'] == $v['use']) {
                        $get += intval($v['pool_win']);
                        $use += 0;
                    } else {
                        $get += intval($v['pool_win']);
                        $use += intval($v['use'] - $v['get']);
                    }
                }
            }
        }
        echo '赢取:' . $get . PHP_EOL;
        echo '支出:' . $use . PHP_EOL;
        echo '盈利:' . intval($get - $use) . PHP_EOL;
    }

    public function testphpinfo()
    {
        echo phpinfo();
    }

}
