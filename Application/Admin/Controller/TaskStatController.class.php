<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/21
 * Time: 16:07
 */
namespace Admin\Controller;
use Think\Controller;

class TaskStatController extends Controller{


    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;
        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        /*$bb_time = strtotime('2017-10-01');
        $ee_time = strtotime('2017-10-31');

        for($i=0;$i<(($ee_time-$bb_time)/86400);$i++) {
            $b_time = $bb_time + $i * 86400;
            $e_time = $b_time + 86399;*/

            $map['c_time'] = array('between', array($b_time, $e_time));
            $list = D('Tasklogs')->where($map)->select();

            $data = array();
            foreach ($list as $v) {
                $data[$v['taskid']]['goods_id'] = $v['goods_id'];//任务获得金币或钥匙
                $data[$v['taskid']]['gold'] += $v['num'];
                $data[$v['taskid']]['num'] += 1;
                $data[$v['taskid']]['user'][$v['uid']] = $v['uid'];
            }

            $map['room'] = array('in', array(119, 1007, 1008));
            $logs = D('Goldget')->where($map)->select();
            foreach ($logs as $v) {
                $data[$v['room']]['goods_id'] = 101;//金币
                $data[$v['room']]['gold'] += $v['gold'];
                $data[$v['room']]['num'] += 1;
                $data[$v['room']]['user'][$v['uid']] = $v['uid'];
            }


            $Taskstat = D('Taskstat');
            foreach ($data as $k => $v) {
                $ret['time'] = $b_time;
                $ret['taskid'] = $k;
                $ret['user'] = count(array_keys($v['user']));
                $ret['goods_id'] = intval($v['goods_id']);
                $ret['gold'] = intval($v['gold']);
                $ret['num'] = intval($v['num']);

                $tap['time'] = $b_time;
                $tap['taskid'] = $k;
                if ($info = $Taskstat->where($tap)->find()) {
                    $Taskstat->where(array('_id' => $info['_id']))->save($ret);
                } else {
                    $Taskstat->add($ret);
                }
            }
//        }

        echo 'SUCCESS'.PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times".PHP_EOL;
    }


}