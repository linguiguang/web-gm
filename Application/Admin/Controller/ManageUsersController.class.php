<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/15
 * Time: 14:03
 */

namespace Admin\Controller;
use Admin\Controller\AdminController;

/**
 * 用户管理控制器
 */
class ManageUsersController extends AdminController
{
    public function delUser($id)
    {
        $id = I('id');
        if(!empty($id)) {
            //实时在线
            $post_url = C('GMBox_URL') . 'sys/remove_account'; 
            $datas['account'] = intval($id);
            $datas['timestamp'] = intval(time());
            $datas['sign'] = md5(C('SENDGAME_KEY').time()); 
            $fields = json_encode($datas);

            $retss = PostUrl($post_url, $fields,1);
        } 
        $this->success('删除成功！');
    }

    /**
     * 注册信息
     */
    public function index($p = 1)
    {
        $limit = 20;
        $user = D('DAccounts');
		
        $onlie = I('onlie');
        $new = I('new');
        if(!empty($onlie)) {
            //实时在线
            $onlieList = D('DOnlineStat')->order('created_at desc')->find();
        }

        if(!empty($new)) {
            $stime = date("Y-m-d");
            $etime =  date("Y-m-d H:i:s",strtotime(date("Y-m-d")) + 86399);
            $map['d_users.created_at'] = ['BETWEEN' , [$stime,$etime]];
        }

        if (isset($_GET['b_time'])) {
            $b_time = I('b_time');
            $map['d_users.created_at'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = date("Y-m-d H:i:s",strtotime(I('e_time')) + 86399);
            $map['d_users.created_at'][] = array('elt', $e_time);
        }

        $bind = intval(I('bind'));
        if ($bind == 1) {
            $map['siteuser'] = 1;
        } elseif ($bind == 2) {
            $map['siteuser'] = 0;
        }

        if (!isset($_GET['from_id'])) {
            $from_id = -1;
        } else {
            $from_id = intval(I('from_id'));
            $map['from_id'] = $from_id;
        }
        if (!isset($_GET['sub_from_id'])) {
            $sub_from_id = -1;
        } else {
            $sub_from_id = $_GET['sub_from_id'];
            $map['d_users.sub_from_id'] = $sub_from_id;
        }
        
        if (!isset($_GET['status'])) {
            $status = 0;
        } else {
            $status = intval(I('status'));
        }
        $map['d_accounts.status'] = $status;


        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['d_accounts.id'] = intval($value);
                    break;
                case 1:
                    $map['d_accounts.id'] = D('DUsers')->get_uid('char_id', intval($value));
                    break;
				case 2:
                    $map['d_accounts.name'] = array('LIKE', "%$value%");  //角色名称
                    break;
                case 3:
                    $map['d_accounts.id'] = D('DUsers')->get_uid('nickname', $value);  //手机号
                    break;
                
                case 4:
                    // $map['wxnick'] = array('LIKE', "%$value%");
                    break;
                case 5:
                    $map['d_accounts.devid'] = $value;
                    break;
            }
        }
//        else {
//            $map['id'] = array('egt', 6000000);
//        }

        //注册地址和手机码
        $create_ip = trim(I('create_ip'));
        $login_ip = trim(I('login_ip'));
        $devid = trim(I('devid'));
        $fromId = intval(I('fromId'));
        $from_time = intval(I('from_time'));

        if ($create_ip != '') {
            $map['d_users.create_ip'] = $create_ip;
        }

        if ($login_ip != '') {
            $map['d_users.login_ip'] = $login_ip;
        }

        if ($devid != '') {
            $map['d_accounts.devid'] = $devid;
        }

        if ($from_time != '') {
            $map['d_users.from_time'] = $from_time;
        }

        if ($fromId != '') {
            $map['d_users.from_id'] = $fromId;
        }
        //print_r($map);

        $totals = $user->where($map)->join('game1.d_users ON d_accounts.id = d_users.account', 'right')->count();
        $list   = $user->where($map)->join('game1.d_users ON d_accounts.id = d_users.account', 'right')->field("d_users.name as uname,d_users.id as userid,d_accounts.id as uid,d_users.*,d_accounts.name,d_accounts.status,d_users.is_brush,d_accounts.devid")->page($p, $limit)->order('d_accounts.id desc')->select();

        $ids = array();
        foreach ($list as $v) {
            $ids[] = intval($v['id']);		
			//$list[$v['id']]['created_at'] = $this->fomatdate($v['created_at']);	
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $fromkey = D('DChannels')->order('id asc')->select();
        foreach ($fromkey as $item) {
            $fromkeyList[$item['from_id'].$item['from_time']] = $item['from_name'];
        }
       // print_r($fromkeyList);

       $subfromkey=  D('DUsers')->field('DISTINCT sub_from_id')->select();

        $this->assign('from', $fromkey);
        $this->assign('sub_from', $subfromkey);
        $this->assign('_list', $list);
        $this->assign('status', $status);
        //$this->assign('user', D('DUsers')->getUserInfo($ids));
        $this->assign('from_id', $from_id);
        $this->assign('sub_from_id', $sub_from_id);
        $this->assign('fromkeyList', $fromkeyList);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }
    public function forbidinfo($id = 0)
    {
        $id = intval($id);
        if ($id == 0) $this->error('用户不存在');

        if (IS_POST) {

            $data['attr'] = 0;
            $attr_1 = I('attr_1') ? 1 : 0;
            $attr_2 = I('attr_2') ? 2 : 0;
            $attr_3 = I('attr_3') ? 4 : 0;
            $attr_4 = I('attr_4') ? 8 : 0;
            $data['attr']=$attr_1+$attr_2+$attr_3+$attr_4;

            if (D('DAccounts')->where(array('id' => $id))->save($data)) 
            {
                //提醒服务端重载
                $post_url = C('GMBox_URL') . 'sys/reload_account'; 
                $datas['account'] = intval($id);
                $datas['timestamp'] = intval(time());
                $datas['sign'] = md5(C('SENDGAME_KEY').time()); 
                $fields = json_encode($datas);
                $retss = PostUrl($post_url, $fields,1);

                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
    }
    /**
     * 解绑微信
     */
    public function outbindali($uid)
    {
        $uid = intval($uid);

        $user = D('DAccounts')->find($uid);
        if ($uid == 0 || $user == '') {
            $this->error('此用户不存在');
        }

        $openid = $user['ali_user_id'];
        if (trim($openid) == '') $this->error('此用户未绑定阿里');

        $data['ali_user_id'] = '';

        if (D('DAccounts')->where(array('id' => $uid))->save($data)) {
            //提醒服务端重载
            $post_url = C('GMBox_URL') . 'sys/reload_account'; 
            $datas['account'] = intval($uid);
            $datas['timestamp'] = intval(time());
            $datas['sign'] = md5(C('SENDGAME_KEY').time()); 
            $fields = json_encode($datas);
            $retss = PostUrl($post_url, $fields,1);

            $this->success('重置绑成功');
        } else {
            $this->error('重置失败');
        }
    }
    /**
     * 用户详情
     */
    public function info($id = 0)
    {
        $id = intval($id);
        if ($id == 0) $this->error('用户不存在');

        if (IS_POST) {

            $pwd = trim(I('pwd'));
            if ($pwd != '') {
                $data['password'] = md5(md5($pwd));
            }
 

            $status = intval(I('status'));
            $reason = trim(I('reason'));
            $realname = I('realname');
            if ($status == 1 && $reason == '') {
                $this->error('禁用用户，原因不能为空');
            } elseif ($status == 0) {
                $data['forbid_reason'] == '';
            } else {
                $data['forbid_reason'] = $reason;
            }

            //if($realname!= ''){$data['real_name'] = $realname;}

            // 暂时关闭解封功能
            if ($status == 1) {
                $this->error('保存失败');
            }

            $data['status'] = intval($status);

            $data['devid'] = I('devid');
             


            if (D('DAccounts')->where(array('id' => $id))->save($data)) {
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        } else {
            $user     = D('DAccounts')->where(array('id' => $id))->find();
            $userinfo = D('DUsers')->where(array('account' => $id))->find();
			
            $qudaoOldDatas = D('Activatelogs')->where(array('user_id' => $id))->find();

            $b_time = $qudaoOldDatas['bind_time'];
            $e_time = time();
            $map1['uid'] = $id;
            //输赢统计
            $userinfo["gold_stat"]=D("DRooms")->get_user_rooms($userinfo['id'],"","sum(total_win)-sum(total_cost)");

            $list_all = D('Everydaygoldstat')->where($map1)->select();
            $liststat = 0; //当期输赢
            foreach ($list_all as $v) {
                if ($v['time'] > $b_time & $v['time'] < $e_time) {
                    $liststat += $v['stat'];
                }
            }
            unset($list_all);

            //在线时长,登录次数
            // $map['uid'] = $userinfo['id'];
      
            $times = D('DUserRoomIoLogs')->getusertimes($userinfo['id'],'','','');


            //游戏信息，输赢统计
            $data = $this->_gameinfostat($userinfo['id']);

            //充值信息
            $ret['account'] = $id;
            $ret['status'] = 1;

            $recharge = D('DPays')->where($ret)->order('id desc')->select();
            $rech_nums = count($recharge); //充值单数
            $money = 0; //充值金额
            $listmoney = 0; //当期充值金额
            foreach ($recharge as $v) {
                $money += $v['price'];
                if ($v['created_at'] > $b_time & $v['created_at'] < $e_time) {
                    $listmoney += $v['price'];
                }
            }

            //红包广场
            $ids = array();
            $log['buid|suid'] = $id;
            //            $log['_logic'] = 'OR';
            //            $log['buid'] = $id;

            $b_time = strtotime(I('b_time'));
            $e_time = strtotime(I('e_time')) + 86399;

            if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
                $log['stime'] = array('between', array($b_time, $e_time));
            } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
                $log['stime'] = array('elt', $e_time);
            } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
                $log['stime'] = array('egt', $b_time);
            }

            $redlog = D('Redmarket')->where($log)->order('stime desc')->select();

            $total_buy = $total_sale = 0; //总共购买与总共售出
            foreach ($redlog as $v) {
                $ids[] = $v['bchar_id'];
                $ids[] = $v['schar_id'];

                if ($v['sale'] == 1) { //交易成功

                    if ($id == $v['buid']) {
                        $total_buy += $v['price'];
                    } else {
                        $total_sale += $v['price'];
                    }
                }
            }

            //历史红包获得
            $config = D('ConfigByshop')->getConfig();
            foreach ($config as $k => $v) {
                if ($v['cost_type'] == 1) $red_goods_id[] = intval($k);
            }
            $goods_id = array_unique($red_goods_id);
            $sap['uid'] = $id;
            $sap['state'] = 1;
			
			//暂时注释
            /* $shopExchanges = D('RedpacketLogs')->where($sap)->select();

            $redExchange = 0;
            foreach ($shopExchanges as $v) {
                if (!in_array($v['goods_id'], $goods_id)) continue;
                $redExchange += intval($config[$v['goods_id']]['item_num']);
            } */
			//暂时注释
            //1.用户可以登入游戏娱乐2.用户不可以充值3.用户不可以体现兑换4.用户渠道数据不返回
            $attr_1=0;
            $attr_2=0;
            $attr_3=0;
            $attr_4=0;

            $attr_data=$user['attr'];
            $binary = decbin($attr_data); // 转换为二进制字符串
            for ($i = strlen($binary) - 1; $i >= 0; $i--) {
                $char = $binary[$i];

                if(strlen($binary) - 1==$i){$attr_1=intval($char);}
                else if(strlen($binary) - 2==$i){$attr_2=intval($char);}
                else if(strlen($binary) - 3==$i){$attr_3=intval($char);}
                else if(strlen($binary) - 4==$i){$attr_4=intval($char);}
            }
             
            $this->assign('attr_1', $attr_1);
            $this->assign('attr_2', $attr_2);
            $this->assign('attr_3', $attr_3);
            $this->assign('attr_4', $attr_4);

            $back_url = $_SERVER['HTTP_REFERER'];
            $this->assign('total_buy', $total_buy);
            $this->assign('total_sale', $total_sale);
            $this->assign('redlog', $redlog);
            //            $this->assign('fishes', D('Configfishes')->getConfig());
            //$this->assign('redexchange', $redExchange);  //暂时注释
            $this->assign('qudaoolddatas', $qudaoOldDatas);
            $this->assign('liststat', $liststat);
            $this->assign('chars', D('DUsers')->getUserInfo($ids));
            $this->assign('recharge', $recharge);
            $this->assign('pay_types', $this->pay_types());
            $this->assign('goods', $this->recharge_goods());
            $this->assign('rooms', $this->rooms());
            $this->assign('money', $money);
            $this->assign('listmoney', $listmoney);
            $this->assign('rech_nums', $rech_nums);
            $this->assign('data', $data);  // 各个游戏区域输赢统计
            $this->assign('times', $times);
            $this->assign('user', $user);
            $this->assign('userinfo', $userinfo);
            $this->assign('back_url', $back_url);
            $this->display();
        }
    }

    /**
     * 游戏信息统计
     */
    protected function _gameinfostat($uid)
    {
        //游戏时长
        $rooms = D('DRooms')->rooms();  //房间

        $room = array();
        foreach ($rooms as $k => $v) {
            $room[] = intval($k);
        }
        $area_arr = $room;
        
        $ret['room_type'] = array('in', $area_arr);
        $ret['uid'] = $uid;
        $gamelogin = D('DUserRooms')->field('uid,room_type,total_win,total_cost')->where($ret)->select();
        //print(D('DUserRooms')->getLastSql());
        $ares = array();
        foreach ($gamelogin as $v) {
            $ares[$v['room_type']]['times'] =  D('DUserRoomIoLogs')->getusertimes($v['id'],$v['room_type'],'','');;
            $ares[$v['room_type']]['total_earnings'] = $v['total_win']-$v['total_cost'];
            $ares[$v['room_type']]['total_cost'] = $v['total_cost'];


        }

        return $ares;
    }

    /**
     * 解绑微信
     */
    public function outbindwx($uid)
    {
        $uid = intval($uid);

        $user = D('DAccounts')->find($uid);
        if ($uid == 0 || $user == '') {
            $this->error('此用户不存在');
        }

        $openid = $user['sdk_uid'];
        if (trim($openid) == '') $this->error('此用户未绑定微信');

        $data['sdk_uid'] = '';
        $data['name'] = '';

        if (D('DAccounts')->where(array('id' => $uid))->save($data)) {
            $this->success('解绑绑成功');
        } else {
            $this->error('解绑失败');
        }
    }
    /**
     * 解绑微信
     */
    public function outyidun($uid)
    {
        $uid = intval($uid);

        $user = D('DAccounts')->find($uid);
        if ($uid == 0 || $user == '') {
            $this->error('此用户不存在');
        }

        $data['risk_level'] = 0;

        if (D('DAccounts')->where(array('id' => $uid))->save($data)) {
            //提醒服务端重载
            $post_url = C('GMBox_URL') . 'sys/reload_config'; 
            $data['conf_type'] = intval(102);
            $data['yidun_lv'] = 1;
            $data['account'] = intval($id);
             
            $data['timestamp'] = intval(time());
            $data['sign'] = md5(C('SENDGAME_KEY').time()); 
            $field = json_encode($data);

            $rets = PostUrl($post_url, $field,1);
            if($rets=="ok")
            {
                $this->success('解除风险成功');
            }
            else{$this->success('解除风险失败'.$rets);}
        } else {
            $this->error('解除风险失败');
        }
    }
   
    /**
     * 用户信息 
     */
    public function userinfo($p = 1)
    {
        $limit = 20;
        $user = D('DUsers');

        // var_dump($user->where(['uid' => ['GT', 0]])->delete() );  //清空
        $query = '';
        $from_id = intval($_GET['from_id']);
        if ( $from_id > 0 ) {
            $from_id = intval(I('from_id'));
            $map['from_id'] = $from_id;
            $query .= 'from_id/' . $from_id . '/';
        } else {
            $from_id = 999;
        }

        if (isset($_GET['islogin']) && intval($_GET['islogin']) != -1) {
            $islogin = intval(I('islogin'));
            $map['online'] = $islogin;
            $query .= 'islogin/' . $islogin . '/';
        } else {
            $islogin = -1;
        }

        $devid = trim(I('devid'));
        if ($devid) {
            $uac = D('DAccounts')->where(array('devid' => array('like', trim("%".$devid."%"))))->field('id')->select(); //获取角色ID
            
            $uids = array();
            foreach ($uac as $v) {
                $uids[] = intval($v['id']);
            }
            $map['account'] = array('in', $uids);

            $query .= 'devid/' . $devid . '/';
        }

        $timetype = intval(I('timetype'));
        $timeField = $timetype ? 'last_logout' : 'created_at';
        $b_time = $timetype ? strtotime(I('b_time')):I('b_time')." 00:00:00";
        $e_time = $timetype ? (strtotime(I('e_time')) + 86399) :I('e_time')." 23:59:59";
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map[$timeField] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map[$timeField] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map[$timeField] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        if (isset($_GET['testtype'])) {
            $map['testtype'] = intval(I('testtype'));
        }

        $type = I('type');
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
					$map['id'] = D('DUsers')->get_charid('uid', $value);  //获取角色ID
                    break;
                case 1:
                    $map['id'] = intval($value);  //获取角色ID
                    break;
                case 2:
                    $map['id'] = D('DUsers')->get_charid('mobile', trim($value)); //获取角色ID
                    break;
                case 3:
                    $userinfo = D('DUsers')->where(array('name' => array('like', trim("%".$value."%"))))->field('id')->select();
                  
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['id']);
                    }
                    $map['id'] = array('in', $ids);
                    break;
                case 4:
                    $str = explode(',', $value);
                    $uids = array();
                    foreach ($str as $v) {
                        $uids[] = intval($v);
                    }
                    $userinfo = D('DUsers')->where(array('account' => array('in', $uids)))->field('id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['id']);
                    }
                    $map['id'] = array('in', $ids);
                    break;
            }

            $query .= 'type/' . $type . '/value/' . $value . '/';
        } else {
            $map['id'] = array('egt', 150000);
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));

        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'id desc';
        }

        // echo "=====" . json_encode($map['arealog'][1]);

        if (I('async') == 1) { //异步顶部数据
            $sqlstr = 'db.getCollection("stat_userinfo").aggregate([{$group:{
                    _id: 0, 
                    totalRedbag: {
                        $sum: "$redbag"
                    },
                    totalGold: {
                        $sum: "$gold"
                    }
                  }}]);';

            $total =  $user->mongoCode($sqlstr);
            $return = array(
                'gold' => intval($total['_batch'][0]['totalGold']),
                'diamond' => 0,
                'redbag' => intval($total['_batch'][0]['totalRedbag']) / 10
            );

            return $this->ajaxReturn($return);
        }

        if ($_GET['rtn']) {
            unset($map['created_at']);
            unset($map['last_logout']);
        };
       
        $totals = $user->where($map)->count();
        
        if(count($map) > 0) {
            $user->where($map);
        }
        $lists   = $user->page($p, $limit)->order($orders)->select();


        $uids = array();
        $no = 1;
        $caryinli = array();
        $accuser = D('DAccounts');
        //$redpacket = D('RedpacketLogs');   //暂时注释
        // var_dump($lists);
        $rooms = D('DRooms')->rooms_new_all();  //房间
        $this->assign('rooms', $rooms);
        foreach ($lists as $k => $v) {
            $list[$v['id']] = $v;  //$user = D('DUsers');
			$list[$v['id']]['uid'] =$v['account'];
			$list[$v['id']]['user_id'] =$v['id'];
			$list[$v['id']]['name'] =$v['name'];					
            $list[$v['id']]['id'] = ($p - 1) * 20 + $no++;			
            $uids[] = $v['id'];
            if ($resRoom > 0) {
                $list[$v['id']]['room'] = $resRoom;
            } else {
                $list[$v['id']]['room'] = $list[$k]['room'] * 10 + intval($list[$k]['testtype']);
            }
			
            foreach($rooms as $rk => $rv)
            {
                //房间赢取
                //$list[$v['id']]['get'.$rk] = D('DRooms')->get_user_rooms($v['id'],$rk,"total_win");
                $list[$v['id']]['getlist'][$rk] = D('DRooms')->get_user_rooms($v['id'],$rk,"total_win");
                $list[$v['id']]['get'] += $list[$v['id']]['getlist'][$rk];
                //房间消耗 $cost1
                //$list[$v['id']]['cost'.$rk] =  D('DRooms')->get_user_rooms($v['id'],$rk,"total_cost");
                $list[$v['id']]['costlist'][$rk] =  D('DRooms')->get_user_rooms($v['id'],$rk,"total_cost");
                $list[$v['id']]['cost'] += $list[$v['id']]['costlist'][$rk];
            }
			//$get1 = D('DRooms')->get_user_rooms($v['id'],1,"total_win");
			//$get2 = D('DRooms')->get_user_rooms($v['id'],2,"total_win");
			//$get3 = D('DRooms')->get_user_rooms($v['id'],3,"total_win");
            //$get11 = D('DRooms')->get_user_rooms($v['id'],11,"total_win");
            //$get21 = D('DRooms')->get_user_rooms($v['id'],21,"total_win");
			//$get = $get11+$get21+$get1+$get2+$get3;
			//$list[$v['id']]['get'] = $get;
			//$list[$v['id']]['get1'] = $get1;
			//$list[$v['id']]['get2'] = $get2;
			//$list[$v['id']]['get3'] = $get3;
            //$list[$v['id']]['get11'] = $get11;
            //$list[$v['id']]['get21'] = $get21;
			
			//房间消耗 $cost1
			//$cost1 = D('DRooms')->get_user_rooms($v['id'],1,"total_cost");
			//$cost2 = D('DRooms')->get_user_rooms($v['id'],2,"total_cost");
			//$cost3 = D('DRooms')->get_user_rooms($v['id'],3,"total_cost");
            //$cost11 = D('DRooms')->get_user_rooms($v['id'],11,"total_cost");
            //$cost21 = D('DRooms')->get_user_rooms($v['id'],21,"total_cost");
			//$cost = $cost11+$cost21+$cost1+$cost2+$cost3;
			//$list[$v['id']]['cost'] = $cost;
			//$list[$v['id']]['cost1'] = $cost1;
			//$list[$v['id']]['cost2'] = $cost2;
			//$list[$v['id']]['cost3'] = $cost3;
            //$list[$v['id']]['cost11'] = $cost11;
            //$list[$v['id']]['cost21'] = $cost21;
			
            if(empty($v['name'])) {
                $u_info = $accuser->where(['id' => $v['account']])->field('name')->find();
                $list[$v['id']]['name'] = $u_info['name'];
            }
			
			
			 //暂时注释
            //$rep_info = $redpacket->where(['uid' => $v['uid'], 'state' => 1])->field('sum(amount) as amount')->find();
            $list[$v['id']]['redbag'] = $rep_info['amount'];
        }



        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        
        $this->assign('from_id', $from_id);
        $fromkey = D('DChannels')->order('from_id asc')->getField('from_id,from_name');
        $resArr = array();
        foreach ($fromkey as $k => $v) {
            $found = false;
            foreach (C('PLAN_NUMS_END') as $nper) {
                if (strstr($v, $nper . '')) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $resArr[$k] = $v;
            }
        }
        // var_dump(D('Users')->getUserInfo($uids));exit;
        // var_dump(D('Userinfo')->where(['_id' => '60ca081fb448910e318b457c'])->delete());

        $this->assign('froms', $resArr);
        $this->assign('rtn', I('rtn'));
        $this->assign('islogin', $islogin);
        $this->assign('room', $resRoom);
        $this->assign('query', $query);
        $this->assign('_list', $list);
        $this->assign('users', D('DUsers')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 用户信息顶部数据显示
     */
    private function gettopinfo($map)
    {
        $Userinfo = D('Userinfo');
        // echo json_decode($map);
        //身上财富统计
        // $map['uid'] = array('egt', 220149);
        $list = $Userinfo->where($map)->select();

        $gold = $diamond = $ticket = $red = $allol = $aiol = 0;

        foreach ($list as $v) {
            $gold += $v['gold'];
            $diamond  += $v['diamond'];
            $ticket += $v['ticket'];
            $red  += $v['redbag'];

            if ($v['islogin'] == 1) {
                $allol += 1;
            }
        }

        $online = D('Online');
        $info = $online->order('c_time desc')->find();

        $data = array(
            'gold' => number_format($gold),
            'diamond' => number_format($diamond),
            'ticket' => number_format($ticket),
            'red' => $red,
            'allol' => intval($info['totals']),
        );
        return $data;
    }

    /**
     * 在线时长
     */
    public function oltime()
    {

        $room = I('room');
        $oltimestat = D('Oltimestat');

        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        if ($room == 1) {
            $map['room'] = 'gameroom';
        } else {
            $map['room'] = 'totalroom';
        }

        $map['c_time'] = array('between', array($b_time, $e_time));
        $info = $oltimestat->where($map)->select();

        $list = array();
        foreach ($info as $k => $v) {
            $date = strtotime(date('Y-m-d', $v['c_time']));
            $list[$date] = $v;
        }
        krsort($list);

        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 平均时长
     */
    public function avgtime()
    {
        $oltimestat = D('Oltimestat');

        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['room'] = 'totalroom';
        $info = $oltimestat->where($map)->select();

        $actives = D('Activestat')->where(array('c_time' => array('between', array($b_time, $e_time))))->select();
        $active_num = array();
        foreach ($actives as $v) {
            $active_num[date('Y-m-d', $v['c_time'])] += $v['user_nums'];
        }

        $list = array();
        foreach ($info as $k => $v) {
            $datef = date('Y-m-d', $v['c_time']);

            $actives = intval($active_num[$datef]);
            $date = strtotime($datef);
            if ($actives != 0) {
                $list[$date]['avgdatingtime'] = abs(round($v['dating_times'] / $actives));
                $list[$date]['avgkp'] = abs(round(($v['room_11'] + $v['room_12'] + $v['room_13'] + $v['room_14'] + $v['room_15'] + $v['room_16']) / $actives));
                $list[$date]['avgbr'] = abs(round($v['room_2'] / $actives));
                $list[$date]['avgsg'] = abs(round($v['room_3'] / $actives));
                $list[$date]['avghc'] = abs(round($v['room_4'] / $actives));
                $list[$date]['avgqr'] = abs(round($v['room_5'] / $actives));
                $list[$date]['totalavg'] = abs(round(($v['game_times'] + $v['dating_times']) / $actives));
                $list[$date]['nums'] = $actives;
            } else {
                $list[$date]['avgdatingtime'] = 0;
                $list[$date]['avgkp'] = 0;
                $list[$date]['avgbr'] = 0;
                $list[$date]['avgsg'] = 0;
                $list[$date]['avghc'] = 0;
                $list[$date]['avgqr'] = 0;
                $list[$date]['totalavg'] = 0;
                $list[$date]['nums'] = 0;
            }
        }
        krsort($list);

        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * AI管理
     */
    public function robot($p = 1)
    {
        $limit = 20;
        $user = D('Userinfo');

        $nickname = trim(I('nickname'));

        $query = '';

        /*if(isset($_GET['islogin'])){
            $islogin = intval(I('islogin'));
            $map['islogin'] = $islogin;
            $query .= 'islogin/'.$islogin.'/';
        }else{
            $islogin = -1;
        }

        if(isset($_GET['room'])){
            $room = I('room');
            $map['room'] = $room;
            $query .= 'room/'.$room.'/';
        }else{
            $room = -1;
        }*/

        if ($nickname != '') {
            $map['nickname'] = array('like', $nickname);
            $query .= 'nickname/' . $nickname . '/';
        }

        $char_id = intval(I('char_id'));
        if ($char_id != 0) {
            $map['char_id'] = $char_id;
            $query .= 'char_id/' . $char_id . '/';
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'c_time desc';
        }

        //机器人
        $map['isrobot'] = 1;

        if (I('async') == 1) {
            $list = $user->where($map)->field('gold')->select();

            $total_golds = 0;
            foreach ($list as $k => $v) {
                $total_golds += $v['gold'];
            }
            $return = array(
                'total_golds' => number_format($total_golds),
            );
            return $this->ajaxReturn($return);
        }

        $totals = $user->where($map)->count();
        $list   = $user->where($map)->page($p, $limit)->order($orders)->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('rooms', $this->rooms());
        $this->assign('query', $query);
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * AI编辑
     */
    public function editrobot($char_id)
    {
        $char_id = intval($char_id);
        if ($char_id == 0) $this->error('AI不存在');

        $map['char_id'] = $char_id;
        $map['isrobot'] = 1;

        if (IS_POST) {
            $nickname = trim(I('nickname'));
            if ($nickname == '') $this->error('AI昵称不能为空');

            $data['nickname'] = $nickname;
            $data['vip'] = intval(I('vip'));
            $data['gold'] = intval(I('gold'));

            //TODO::发送到服务端处理

            if (D('Userinfo')->where($map)->save($data)) {
                $this->success('更新成功', U('ManageUsers/robot'));
            } else {
                $this->error('更新失败');
            }
        } else {
            $info = D('Userinfo')->where($map)->find();
            $this->assign('info', $info);
            $this->display();
        }
    }

    /**
     * 限制用户
     */
    public function limitusers()
    {
        $this->display();
    }

    /**
     * 限制IP
     */
    public function limitip($p = 1)
    {
        $limit = 20;
        $forbidip = D('Forbidip');
        $user = D('Users');

        $ip = trim(I('ip'));
        if ($ip != '') {
            $map['ip'] = $ip;
        }

        //被封的ip
        $map['state'] = 0;

        $totals = $forbidip->where($map)->count();
        $list   = $forbidip->where($map)->order('id desc')->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['num'] = $user->where(array('ip' => trim($v['ip'])))->count();
            $list[$k]['country'] = getIpInfo($v['ip'], 'country');
            $list[$k]['area'] = getIpInfo($v['ip'], 'area');
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 添加ip
     */
    public function addlimitip()
    {
        if (IS_POST) {
            $ips = trim(I('ip'));
            $iparr = explode("\r\n", $ips);

            if ($iparr == '') $this->error('ip输入不能为空');

            $forbidip = D('Forbidip');
            $data['c_time'] = NOW_TIME;
            $data['uid'] = is_login();
            foreach ($iparr as $k => $v) {
                $data['ip'] = trim($v);
                $forbidip->add($data);
            }

            $this->success('添加成功', U('ManageUsers/limitip'));
        } else {
            $this->display();
        }
    }

    /**
     * 解除限制
     */
    public function unforbid($method)
    {

        $method = trim($method);

        $ids  = I('id');
        if ($ids == '') $this->error('请选择解除限制的ip');

        if ($method == 'ip') {
            $model = D('Forbidip');
        } elseif ($method == 'mac') {
            $model = D('Forbidmac');
        } else {
            $this->error('参数错误');
        }

        $data['state'] = 1;
        foreach ($ids as $k => $v) {
            $data['id'] = intval($v);
            $model->save($data);
        }

        $this->success('解除成功');
    }

    /**
     * 添加机器码
     */
    public function addlimitmaccode()
    {
        if (IS_POST) {
            $maccode = trim(I('maccode'));
            $maccodearr = explode("\r\n", $maccode);

            if ($maccodearr == '') $this->error('ip输入不能为空');

            $forbidmac = D('Forbidmac');
            $data['c_time'] = NOW_TIME;
            $data['uid'] = is_login();
            foreach ($maccodearr as $k => $v) {
                $data['mac'] = trim($v);
                $forbidmac->add($data);
            }

            $this->success('添加成功', U('ManageUsers/limitmaccode'));
        } else {
            $this->display();
        }
    }

    /**
     * 限制机器码
     */
    public function limitmaccode($p = 1)
    {
        $limit = 20;
        $forbidmac = D('Forbidmac');
        $user = D('Users');

        $maccode = trim(I('maccode'));
        if ($maccode != '') {
            $map['mac'] = $maccode;
        }

        //被封的ip
        $map['state'] = 0;

        $totals = $forbidmac->where($map)->count();
        $list   = $forbidmac->where($map)->order('id desc')->page($p, $limit)->select();

        foreach ($list as $k => $v) {
            $list[$k]['num'] = $user->where(array('devid' => trim($v['mac'])))->count();
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 水果狂欢玩家信息
     */
    public function fruitusers($p = 1)
    {
        $limit = 20;
        $show = intval(I('show'));
        $fruituser = $show ? D('Superfruituser') : D('Fruituser');
        $query = '';
        if ($show) $query .= 'show/' . $show . '/';
        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['char_id'] = D('Users')->get_charid('uid', $value);
                    break;
                case 1:
                    $map['char_id'] = intval($value);
                    break;
                case 2:
                    $map['char_id'] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['char_id']);
                    }
                    $map['char_id'] = array('in', $ids);
                    break;
            }

            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['first_intime'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['first_intime'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['first_intime'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        $orderby = trim(I('orderby'));
        $order = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = '';
        }

        $char_ids = $uids = array();
        $totals = $fruituser->where($map)->count();
        $list  = $fruituser->where($map)->page($p, $limit)->order($orders)->select();

        foreach ($list as $k => $v) {
            $char_ids[] = $v['char_id'];
            $uids[] = $v['uid'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('char', D('Userinfo')->getUserInfo($char_ids, 'char_id'));
        $this->assign('user', D('Users')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 百人牛牛玩家信息
     */
    public function brniuniu($p = 1)
    {
        $limit = 20;
        $Brnnuser = D('Brnnuser');
        $query = '';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['char_id'] = D('Users')->get_charid('uid', $value);
                    break;
                case 1:
                    $map['char_id'] = intval($value);
                    break;
                case 2:
                    $map['char_id'] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['char_id']);
                    }
                    $map['char_id'] = array('in', $ids);
                    break;
            }

            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['first_intime'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['first_intime'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['first_intime'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        $orderby = trim(I('orderby'));
        $order = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = '';
        }

        $char_ids = $uids = array();
        $totals = $Brnnuser->where($map)->count();
        $list = $Brnnuser->where($map)->page($p, $limit)->order($orders)->select();

        foreach ($list as $k => $v) {
            $char_ids[] = $v['char_id'];
            $uids[] = $v['uid'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('char', D('Userinfo')->getUserInfo($char_ids, 'char_id'));
        $this->assign('user', D('Users')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 豪车玩家信息
     */
    public function caruser($p = 1)
    {
        $limit = 20;
        $Caruser = D('Caruser');

        $query = '';

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['char_id'] = D('Users')->get_charid('uid', $value);
                    break;
                case 1:
                    $map['char_id'] = intval($value);
                    break;
                case 2:
                    $map['char_id'] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['char_id']);
                    }
                    $map['char_id'] = array('in', $ids);
                    break;
            }

            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;

        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['first_intime'] = array('between', array($b_time, $e_time));
            $map2['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['first_intime'] = array('elt', $e_time);
            $map2['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['first_intime'] = array('egt', $b_time);
            $map2['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }
        //获取每个人的豪车红包数据
        /*	$redlist = array();
			$map2['uid'] = array('egt', 150000);
			$map2['room']=160;
			$redlist = D('redbaglog')->where($map2)->select();
			 foreach ($redlist as $v){
				if(in_array($v['uid'], $uids_arr)) continue;
				$date = date('Y-m-d H:00:00', $v['c_time']);
				$data[$date]['redget160'] += intval($v['money']);
			}
			unset($redlist);
			*/

        //===============


        $orderby = trim(I('orderby'));
        $order = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = '';
        }

        $char_ids = $uids = $list = array();
        //其它排序
        $map['uid'] = array('egt', 0);
        $totals = $Caruser->where($map)->count();
        $list = $Caruser->where($map)->page($p, $limit)->order($orders)->select();
        $redlist = array(); //豪车红包数
        foreach ($list as $k => $v) {
            $char_ids[] = $v['char_id'];
            $uids[] = $v['uid'];
        }

        //查询豪车红包总数
        $map2['uid'] = array('egt', 0);
        $map2['room'] = 160;
        //$recharge=10;
        $recharge = D('redbaglog')->where($map2)->select();

        foreach ($recharge as $kkk) {
            $redlist[$kkk['uid']] += $kkk['money'] / 10;
        }




        // $recharge =D('redbaglog')->where($map2)->sum('money');
        //$recharge =D('redbaglog')->where($map2)->count();
        //$redlist[$v['uid']]=intval($recharge);
        //=====CAR OVER

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $this->assign('redlist', $redlist);
        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('char', D('Userinfo')->getUserInfo($char_ids, 'char_id'));
        $this->assign('user', D('Users')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     *实时在线
     */
    public function onlineusers($p=1)
    {
        $rooms=D('DRooms')->rooms_new_all();
        $this->assign('rooms', $rooms);

        $info['all']=0;$info['totals']=0;
        foreach ($rooms as $key => $value) {
            # code...
            $info[$key]=0;
        };

        //请求总的统计数据
        $post_url = C('GM_URL') . 'cgi-bin/gm_oprate.room_online';
        $post_data['sign'] = md5(C('SENDGAME_KEY').time());
        $post_data['timestamp'] = intval(time());
        $field = json_encode($post_data);
        $rets = PostUrl($post_url, $field,1);
        $ret = json_decode($rets, true);
        if ($ret['err'] == 'ok')
        { 
            foreach ($ret['infos'] as $kkk) 
            {
                $info[$kkk['room_type']]=$kkk['num'];
                $info['totals']+=$info[$kkk['room_type']];
            }
            $info['all']=$ret['all'];
        }
        //var_dump($info);exit;
         
        $this->assign('info', $info);
        

        $limit = 20;
        $user = D('DUsers');
 
        $query = '';
        $map['online'] = 1;

        if (isset($_GET['islogin']) && intval($_GET['islogin']) != -1) {
            $islogin = intval(I('islogin'));
            $map['room_id'] = $islogin;
            $query .= 'islogin/' . $islogin . '/';
        } else {
            $islogin = -1;
        } 

        $totals = $user->where($map)->count();
        
        if(count($map) > 0) {
            $user->where($map);
        }
        $lists   = $user->page($p, $limit)->order($orders)->select();


        $uids = array();
        $no = 1;
        $caryinli = array();
        $accuser = D('DAccounts');
        foreach ($lists as $k => $v) {
            $list[$v['id']] = $v;  //$user = D('DUsers');
			$list[$v['id']]['uid'] =$v['account'];
			$list[$v['id']]['user_id'] =$v['id'];
			$list[$v['id']]['name'] =$v['name'];					
            $list[$v['id']]['id'] = ($p - 1) * 20 + $no++;			
            $uids[] = $v['id'];
            if ($resRoom > 0) {
                $list[$v['id']]['room'] = $resRoom;
            } else {
                $list[$v['id']]['room'] = $list[$k]['room'] * 10 + intval($list[$k]['testtype']);
            }

            foreach($rooms as $rk => $rv)
            {
                //房间赢取
                //$list[$v['id']]['get'.$rk] = D('DRooms')->get_user_rooms($v['id'],$rk,"total_win");
                $list[$v['id']]['getlist'][$rk] = D('DRooms')->get_user_rooms($v['id'],$rk,"total_win");
                $list[$v['id']]['get'] += $list[$v['id']]['getlist'][$rk];
                //房间消耗 $cost1
                //$list[$v['id']]['cost'.$rk] =  D('DRooms')->get_user_rooms($v['id'],$rk,"total_cost");
                $list[$v['id']]['costlist'][$rk] =  D('DRooms')->get_user_rooms($v['id'],$rk,"total_cost");
                $list[$v['id']]['cost'] += $list[$v['id']]['costlist'][$rk];
            }

			//房间赢取
			//$get1 = D('DRooms')->get_user_rooms($v['id'],1,"total_win");
			//$get2 = D('DRooms')->get_user_rooms($v['id'],2,"total_win");
			//$get3 = D('DRooms')->get_user_rooms($v['id'],3,"total_win");
            //$get11 = D('DRooms')->get_user_rooms($v['id'],11,"total_win");
            //$get21 = D('DRooms')->get_user_rooms($v['id'],21,"total_win");
			//$get = $get11+$get21+$get1+$get2+$get3;
			//$list[$v['id']]['get'] = $get;
			//$list[$v['id']]['get1'] = $get1;
			//$list[$v['id']]['get2'] = $get2;
			//$list[$v['id']]['get3'] = $get3;
            //$list[$v['id']]['get11'] = $get11;
            //$list[$v['id']]['get21'] = $get21;
			
			//房间消耗 $cost1
			//$cost1 = D('DRooms')->get_user_rooms($v['id'],1,"total_cost");
			//$cost2 = D('DRooms')->get_user_rooms($v['id'],2,"total_cost");
			//$cost3 = D('DRooms')->get_user_rooms($v['id'],3,"total_cost");
            //$cost11 = D('DRooms')->get_user_rooms($v['id'],11,"total_cost");
            //$cost21 = D('DRooms')->get_user_rooms($v['id'],21,"total_cost");
			//$cost = $cost11+$cost21+$cost1+$cost2+$cost3;
			//$list[$v['id']]['cost'] = $cost;
			//$list[$v['id']]['cost1'] = $cost1;
			//$list[$v['id']]['cost2'] = $cost2;
			//$list[$v['id']]['cost3'] = $cost3;
            //$list[$v['id']]['cost11'] = $cost11;
            //$list[$v['id']]['cost21'] = $cost21;
			
            if(empty($v['name'])) {
                $u_info = $accuser->where(['id' => $v['account']])->field('name')->find();
                $list[$v['id']]['name'] = $u_info['name'];
            }
			
			 
            $list[$v['id']]['redbag'] = $rep_info['amount'];
        }



        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        //$this->assign('rooms', $this->rooms());
        $fromkey = D('DChannels')->order('from_id asc')->getField('from_id,from_name');
        $resArr = array();
        foreach ($fromkey as $k => $v) {
            $found = false;
            foreach (C('PLAN_NUMS_END') as $nper) {
                if (strstr($v, $nper . '')) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $resArr[$k] = $v;
            }
        }

        $this->assign('froms', $resArr);
        $this->assign('rtn', I('rtn'));
        $this->assign('islogin', $islogin);
        $this->assign('room', $resRoom);
        $this->assign('query', $query);
        $this->assign('_list', $list);
        $this->assign('users', D('DUsers')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());

        $this->display();
    }
    
    /**
     * 用户等级
     */
    public function level($async = 0)
    {
        $b_time = I('b_time');
        $b_time = !empty($b_time) ? $b_time :date("Y-m-d");

        $end_time = I('e_time') ? I('e_time') : date("Y-m-d");

        if ($async) {


            if (trim($b_time) != '' && trim($end_time) != '') {
                $map['created_at'] = array('between', array($b_time . ' 00:00:00', $end_time . ' 23:59:59'));
            } else if (trim($b_time) != '') {
                $map['created_at'] = array('egt', $b_time . ' 00:00:00');
            } else if (trim($end_time) != '') {
                $map['created_at'] = array('elt', $end_time . ' 23:59:59');
            }
            //$map['isrobot'] = 0;

            $userinfo = D('DUsers');
            $users = $userinfo->field('vip_lev')->where($map)->select();

            $vip = array();
            $totals = 0;
            foreach ($users as $k => $v) {

                $vip[$v['vip_lev']] += 1;
                $totals += 1;
            }

            $data = array(
                array('name' => "Vip0 ($vip[0]人)", 'y' => empty($totals) ? 0 : $vip[0] / $totals),
                array('name' => "Vip1 ($vip[1]人)", 'y' => empty($totals) ? 0 : $vip[1] / $totals),
                array('name' => "Vip2 ($vip[2]人)", 'y' => empty($totals) ? 0 : $vip[2] / $totals),
                array('name' => "Vip3 ($vip[3]人)", 'y' => empty($totals) ? 0 : $vip[3] / $totals),
                array('name' => "Vip4 ($vip[4]人)", 'y' => empty($totals) ? 0 : $vip[4] / $totals),
                array('name' => "Vip5 ($vip[5]人)", 'y' => empty($totals) ? 0 : $vip[5] / $totals),
                array('name' => "Vip6 ($vip[6]人)", 'y' => empty($totals) ? 0 : $vip[6] / $totals),
                array('name' => "Vip7 ($vip[7]人)", 'y' => empty($totals) ? 0 : $vip[7] / $totals),
                array('name' => "Vip8 ($vip[8]人)", 'y' => empty($totals) ? 0 : $vip[8] / $totals),
                array('name' => "Vip9 ($vip[9]人)", 'y' => empty($totals) ? 0 : $vip[9] / $totals),
                array('name' => "Vip10 ($vip[10]人)", 'y' => empty($totals) ? 0 : $vip[10] / $totals),
            );

            return $this->ajaxReturn($data);
        }

        $this->assign('b_time', $b_time);
        $this->assign('e_time', $end_time);
        $this->display();
    }

    /**
     * 在线统计
     */
    public function onlinestat()
    {
        $b_time = I('b_time');
        $e_time = I('e_time');

        if (isset($_GET['b_time']) || isset($_GET['e_time'])) {
            if ($b_time == '' || $e_time == '') {
                $this->error('查询时间请填写完整');
            } else {
                $st_time = strtotime($b_time);
                $end_time = strtotime($e_time) + 86399;
                $map['c_time'] = array('between', array($st_time, $end_time));
            }
        } else {
            $end_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
            $st_time = $end_time - 30 * 86400 - 86399;
            $map['c_time'] = array('between', array($st_time, $end_time));
        }

        $map['use'] = 1;
        $list = D('DOnlinestat')->where($map)->select();
        $datearr = array();
        foreach ($list as $v) {
            $date = date('y/m/d', $v['c_time']);
            $datearr[$date]['totals'] += $v['total'];
            $datearr[$date][2] += $v['2'];
            $datearr[$date][3] += $v['3'];
            $datearr[$date][5] += $v['5'];
            $datearr[$date][11] += $v['11'];
            $datearr[$date][6] += $v['6'];
            $datearr[$date][7] += $v['7'];
        }

        //日对比曲线图
        $data = $this->dailyonline();

        $this->assign('data', $data);
        $this->assign('st_time', date('y/m/d', $st_time));
        $this->assign('end_time', date('y/m/d', $end_time));
        $this->assign('date', $datearr);
        $this->display();
    }

    //日常对比曲线图
    protected function dailyonline()
    {

        $s_time = I('s_time');
        $end_time = I('end_time');

        if (isset($_GET['s_time']) || isset($_GET['end_time'])) {
            if ($s_time == '' || $end_time == '') {
                $this->error('查询时间请填写完整');
            } else {
                $map['c_time'] = array('between', array(strtotime($s_time), strtotime($end_time) + 86399));
            }
        } else {
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
            $b_time = $e_time - 86400 * 2 - 86399;
            $map['c_time'] = array('between', array($b_time, $e_time));
        }

        $map['use'] = 0;
        $list = D('Onlinestat')->where($map)->select();

        $data = array();
        foreach ($list as $v) {
            $time = date('Y-m-d', $v['c_time']);
            $data[$time][$v['c_time']] = $v['totals'];
        }

        return $data;
    }

    protected function room($game)
    {
        switch ($game) {
            case 0:
                $field = 'room';
                $name = '全部房间';
                break;
            case 11:
                $field = 'xs';
                $name = '新手场';
                break;
            case 12:
                $field = 'cj';
                $name = '初级场';
                break;
            case 13:
                $field = 'zj';
                $name = '中级场';
                break;
            case 14:
                $field = 'gj';
                $name = '高级场';
                break;
            case 15:
                $field = 'gb';
                $name = '贵宾场';
                break;
            case 16:
                $field = 'fh';
                $name = '富豪场';
                break;
            case 2:
                $field = 'brnn';
                $name = '百人牛牛';
                break;
            case 3:
                $field = 'fruit';
                $name = '水果狂欢';
                break;
            case 4:
                $field = 'car';
                $name = '豪车';
                break;
            case 5:
                $field = 'red';
                $name = '红包场';
                break;
        }
        $data = array(
            'field' => $field,
            'name' => $name
        );
        return $data;
    }

    /**
     * 千人抢红包
     */
    public function qrredbag($p = 1)
    {
        $limit = 20;
        $Rruser = D('Qruser');
        $query = '';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['char_id'] = D('Users')->get_charid('uid', $value);
                    break;
                case 1:
                    $map['char_id'] = intval($value);
                    break;
                case 2:
                    $map['char_id'] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['char_id']);
                    }
                    $map['char_id'] = array('in', $ids);
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;

        //今天时间
        $today_b_time = strtotime(date('Y-m-d', NOW_TIME));
        $today_e_time = $today_b_time + 86399;

        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['first_intime'] = array('between', array($b_time, $e_time));
            $log['login_time'] = array('between', array($b_time, $e_time));
            $dia['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['first_intime'] = array('elt', $e_time);
            $log['login_time'] = array('elt', $e_time);
            $dia['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['first_intime'] = array('egt', $b_time);
            $log['login_time'] = array('egt', $b_time);
            $dia['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        } else {
            $log['login_time'] = array('between', array($today_b_time, $today_e_time));
            $dia['c_time'] = array('between', array($today_b_time, $today_e_time));
        }

        $orderby = trim(I('orderby'));
        $order = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'first_intime desc';
        }

        if (I('async') == 1) { //异步顶部数据
            S(array('type' => 'File', 'expire' => 3600));
            $return = S('QRRED_CACHE');
            //缓存数据
            if (!$return) {
                $get = $use = $stat = $condia = $red = $new  = $men = 0;
                $list = $Rruser->where($map)->select();
                foreach ($list as $v) {
                    $get += $v['get'];
                    $use += $v['use'];
                    $stat += $v['stat'];
                    $condia += $v['condia'];
                    $red += $v['redbag'];
                }
                //当天游戏人数
                $log['uid'] = array('egt', 150000);
                $log['room'] = 5;
                $log['type'] = 1;
                $loginlog = D('Arealogin')->where($log)->field('uid')->select();
                foreach ($loginlog as $v) {
                    $arr[] = $v['uid'];
                }
                $men = count(array_unique($arr));

                $return = array(
                    'get' => number_format($get),
                    'use' => number_format($use),
                    'stat' => number_format($stat),
                    'condia' => number_format($condia),
                    'redbag' => $red / 10,
                    'men' => $men
                );
                S('QRRED_CACHE', $return);
            }
            return $this->ajaxReturn($return);
        }

        $totals = $Rruser->where($map)->count();
        $list   = $Rruser->where($map)->page($p, $limit)->order($orders)->select();

        $no = 1;
        foreach ($list as $k => $v) {
            $list[$k]['id'] = ($p - 1) * 20 + $no++;
            $char_ids[] = $v['char_id'];
            $uids[] = $v['uid'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('char', D('Userinfo')->getUserInfo($char_ids, 'char_id'));
        $this->assign('user', D('Users')->getUserInfo($uids));
        $this->display();
    }

    /**
     * 进出记录
     */
    public function inout($p = 1)
    {
        $seldt = intval(I('seldt'));
        switch ($seldt) {
            case 1: {
                $st_time = date('Y-m-d');
                $end_time = date('Y-m-d');
                break;
            }
            case 2: {
                $st_time = date("Y-m-d", strtotime("-1 day"));
                $end_time = date("Y-m-d", strtotime("-1 day"));
                break;
            }
            case 3: {
                $week = lastNWeek(NOW_TIME, 0);
                $st_time = $week[0];
                $end_time = $week[1];
                break;
            }
            case 4: {
                $week = lastNWeek(NOW_TIME, 1);
                $st_time = $week[0];
                $end_time = $week[1];
                break;
            }
            case 5: {
                $st_time = date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y")));
                $end_time = date("Y-m-d", mktime(23, 59, 59, date("m"), date("t"), date("Y")));
                break;
            }
            case 6: {
                $month = lastMonth(NOW_TIME);
                $st_time = $month[0];
                $end_time = $month[1];
                break;
            }
            default:
                $st_time = I('b_time');
                $end_time = I('e_time');
                break;
        }

        $sdate = intval(I('sdate'));
        if ($sdate) {
            $field = 'out_time';
        } else {
            $field = 'in_time';
        }

        if (trim($st_time) != '' && trim($end_time) != '') {
            $map[$field] = array('between', array(strtotime($st_time . ' 00:00:00'), strtotime($end_time . ' 23:59:59')));
        } else if (trim($st_time) != '') {
            $map[$field] = array('egt', strtotime($st_time . ' 00:00:00'));
        } else if (trim($end_time) != '') {
            $map[$field] = array('elt', strtotime($end_time . ' 23:59:59'));
        }

        if (!isset($_GET['room'])) {
            $room = -1;
        } else {
            $room = I('room');
            $map['room_type'] = $room;
        }

        $uid = intval(I('uid'));
        if ($uid) $map['uid'] = D('DUsers')->get_charid("uid",$uid);

        $limit = 20;
        $arealogin = D('DUserRoomIoLogs');
        $totals = $arealogin->where($map)->count();
        $list   = $arealogin->where($map)->page($p, $limit)->order('in_time desc')->select();
        //print($arealogin->getLastSql());

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('rooms', D('DRooms')->rooms());
        $this->assign('_page', $pageNav->show());
        $this->assign('room', $room);
        $this->assign('sdate', $sdate);
        $this->assign('seldt', $seldt);
        $this->assign('b_time', $st_time);
        $this->assign('e_time', $end_time);
        $this->display();
    }

    /**
     * 兑奖记录
     */
    public function exchange($p = 1)
    {
        $limit = 20;
        $Shops = D('RedpacketLogs');

        $uid = intval(I('uid'));
        $config = D('ConfigByshop')->getConfig(); //商城数据表

        $map['uid'] = $uid;

        if (isset($_GET['b_time'])) {
            $map['create_time'][] = array('egt', strtotime(I('b_time')));
            $map2['create_time'] = array('egt', strtotime(I('b_time')));
            $map3['create_time'] = array('egt', strtotime(I('b_time')));
            $map4['create_time'] = array('egt', strtotime(I('b_time')));
        } else {
            $map['create_time'][] = array('egt', 1580832000);
            $map2['create_time'] = array('egt', 1580832000);
            $map3['create_time'] = array('egt', 1580832000);
            $map4['create_time'] = array('egt', 1580832000);
        }

        if (isset($_GET['e_time'])) {
            $map['create_time'][] = array('elt', strtotime(I('e_time')) + 86399);
            $map2['create_time'] = array('elt', strtotime(I('e_time')) + 86399);
            $map3['create_time'] = array('elt', strtotime(I('e_time')) + 86399);
            $map4['create_time'] = array('elt', strtotime(I('e_time')) + 86399);
        }

        $totals = $Shops->where($map)->count();
        $shops  = $Shops->where($map)->page($p, $limit)->order('create_time desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        foreach ($shops as $k => $v) {
            $shops[$k]['cost_type'] = $config[$v['goods_id']]['cost_type']; //商品类型
            if ($config[$v['goods_id']]['cost_type'] == 1) { //红包
                $shops[$k]['msg'] = $v['msg'];
                $a_json = json_decode($v['result'], true);
                $shops[$k]['billno'] = $a_json['mch_billno'];
            }
        }
        //红包兑换金币 20190510
        $map2['uid'] = $uid;
        $map2['room'] = 153;
        $map2['money'] = 10;

        $map3['uid'] = $uid;
        $map3['room'] = 153;
        $map3['money'] = 100;
        $map4['uid'] = $uid;
        $map4['room'] = 153;
        $map4['money'] = 500;

        $get_1 = D('redbaglog')->where($map2)->count();
        $get_2 = D('redbaglog')->where($map3)->count();
        $get_3 = D('redbaglog')->where($map4)->count();
        // echo("<script>console.log('".json_encode($map4)."');</script>");

        $shops2[1]['num'] = $get_1;

        $shops2[10]['num'] = $get_2;
        $shops2[50]['num'] = $get_3;

        $this->assign('shops2', $shops2); //兑换金币个数
        $this->assign('config', $config);
        $this->assign('_list', $shops);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 任务记录
     */
    public function tasks($p = 1)
    {
        $limit = 20;
        $Tasklogs = D('Tasklogs');

        $uid = intval(I('uid'));
        $config = D('Configgoods')->getConfig(); //物品表数据

        $map['uid'] = $uid;

        if (isset($_GET['b_time'])) {
            $map['c_time'][] = array('egt', strtotime(I('b_time')));
        }

        if (isset($_GET['e_time'])) {
            $map['c_time'][] = array('elt', strtotime(I('e_time')) + 86399);
        }

        $totals = $Tasklogs->where($map)->count();
        $list  = $Tasklogs->where($map)->page($p, $limit)->order('c_time desc')->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('config', $config);
        $this->assign('tasks', D('ConfigTask')->tasks());
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 验证码
     */
    public function code()
    {
        $mobile = trim(I('mobile'));

        if ($mobile != '') {
            $info = M('Mobicode')->where(array('mobile' => $mobile))->order('id desc')->find();

            if (strpos($info['return_code'], 'ERROR-CODE') === FALSE) {
                $info['success'] = '发送成功';
            } else {
                $info['success'] = '发送失败，详细信息: ' . $info['return_code'];
            }

            $time = intval(NOW_TIME - $info['addtime']);

            if ($time > 300) {
                $info['expire'] = '已过期';
            } else {
                $info['expire'] = '还有 ' . abs($time - 300) . '秒 过期';
            }
        }

        $this->assign('info', $info);
        $this->display();
    }

    /**
     * 金币 获得/消耗日志
     */
    //http://niuadm.1599game.com/index.php?s=/ManageUsers/goldlog.html&gold=0&type=0&value=241866
    public function goldlog($p = 1)
    {
        $limit = 20;
        $query = '';

        $gold = intval(I('gold'));
        $query .= 'gold/' . $gold . '/';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = intval($value);
                    break;
                    break;
                case 1: //角色ID
                    $map['uid'] = D('Users')->get_uid('char_id', intval($value));
                    break;
                case 2: //手机号
                    $map['uid'] = D('Users')->get_uid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $map['uid'] = D('Users')->get_uid('nickname', trim($value));
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time'));
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        //单独游戏场数据
        $oneroom = intval(I('oneroom'));
        if ($oneroom) {
            switch ($oneroom) {
                case 1:
                    $model = D('Kplog');
                    //变动的金币
                    $diff = function (array $v) {
                        return ($v['get'] - $v['use']);
                    };
                    //TODO::字符串解析
                    $remark = function ($str) {
                    };
                    break;
                case 2:
                    $model = D('Brnnlog'); //总获得，如果盈利的话没有减去use和税，如果输的话 是有减去use的
                    $diff = function (array $v) {
                        //if($v['get'] > $v['use'])
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax']);
                        //else    return ($v['get'] + $v['pool_win']- $v['use']);
                    };
                    break;
                case 3:
                    $model = D('Fruitlog'); //水果get 是存收入的包括本金
                    $diff = function (array $v) {
                        //if($v['get'] > $v['use'])
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                        //else  return ($v['get'] + $v['pool_win']- $v['use']);

                    };
                    $map['test_type'] = 1;
                    break;
                case 4:
                    $model = D('Carlog');
                    $diff = function (array $v) {
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax']);
                    };
                    break;
                case 5:
                    $model = D('Qrredbag');
                    $diff = function () {
                    };
                    break;
                case 6:
                    $model = D('Superfruitlog');
                    $diff = function (array $v) {
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                        //if($v['get'] > $v['use'])
                        //	return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                        //else  return ($v['get'] + $v['pool_win']- $v['use']);
                    };
                    break;
                case 7:
                    $model = D('Fruitlog'); //水果get 是存收入的包括本金
                    $diff = function (array $v) {
                        //if($v['get'] > $v['use'])
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                        //else  return ($v['get'] + $v['pool_win']- $v['use']);

                    };
                    $map['test_type'] = 2;
                    break;
            }
            $query .= 'oneroom/' . I('oneroom') . '/';
        } else {
            if ($gold == 0) {
                $model = D('Goldget');
            } elseif ($gold == 1) {
                $model = D('Golduse');
            }

            if (isset($_GET['room'])) {
                $area = intval(I('room'));
                $map['room'] = $area;
                $query .= 'room/' . $area . '/';
            } else {
                $area = -1;
            }
        }

        //isset($_GET['type']) && isset($_GET['value'])
        if (1) {

            //头部数据
            if (I('async')) {
                $wingold = $losegold = 0;

                //扣除本金的金币算法
                $getUse = function (array $v) {
                    if ($v['get'] > $v['use']) {
                        $ret['get'] = $v['get'] - $v['use'];
                        $ret['use'] = 0;
                    } else {
                        $ret['get'] = 0;
                        $ret['use'] = $v['use'] - $v['get'];
                    }
                    return $ret;
                };

                // echo 1111;
                if ($oneroom) {
                    //echo 2222;
                    print_r($map);
                    $lists = $model->where($map)->select();
                    foreach ($lists as $v) {
                        $operate = $getUse($v);
                        $wingold  += $operate['get'] + $v['pool_win'] - $v['tax'] - $v['pooladd'];
                        $losegold += $operate['use'];
                    }
                } else {
                    //echo 3333;
                    $roomss = array(107, 20, 21, 22, 10, 11, 126, 127, 128, 129, 133);
                    $map['room'] = array('in', $roomss);
                    $winlist = D('Goldget')->where($map)->select();
                    foreach ($winlist as $v) {
                        if (in_array($v['room'], array(107, 20, 21, 22, 10, 11, 126, 127, 128, 129, 133))) continue;
                        $wingold += intval($v['gold']);
                    }

                    $loselist = D('Golduse')->where($map)->select();
                    foreach ($loselist as $v) {
                        if (in_array($v['room'], array(107, 20, 21, 22, 10, 11, 126, 127, 128, 129, 133))) continue;
                        $losegold += intval($v['gold']);
                    }
                }

                $data['wingold'] = number_format($wingold);
                $data['losegold'] = number_format($losegold);
                $data['yinli'] = number_format($losegold - $wingold);
                return $this->ajaxReturn($data);
            }
            $totals = $model->where($map)->count();
            // var_dump($model->where(['uid' => ['GT', 0]])->delete() );
            //echo json_encode($map);
            $list   = $model->where($map)->order('c_time desc')->page($p, $limit)->select();
            //$nick   = D('Userinfo')->where(array('uid'=>$map['uid']))->find();

            if ($oneroom) {
                foreach ($list as $k => $v) {
                    $list[$k]['diff_gold'] = $diff($v);
                    $list[$k]['old_gold'] = $v['new_gold'] - $list[$k]['diff_gold'];
                }
            }
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('nick', $nick);
        $this->assign('room', $area);
        $this->assign('area', $area);
        $this->assign('onerooms', $this->onerooms());
        $this->assign('oneroom', $oneroom);
        $this->assign('rooms', $this->path());
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 新 金币记录
     */
    public function newgoldlog($p = 1)
    {
        $limit = 20;
        $query = '';

        $gold = intval(I('gold'));
        $query .= 'gold/' . $gold . '/';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = D('DUsers')->get_charid('uid', intval($value));
                    break;
                    break;
                case 1: //角色ID
                    $map['uid'] = intval($value);
                    break;
                case 2: //手机号
                    $map['uid'] = D('DUsers')->get_charid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $map['uid'] = D('DUsers')->get_charid('nickname', trim($value));
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }
        
        $b_time = I('b_time');
        $e_time = I('e_time');
        if(empty(I('b_time')) && empty(I('e_time')))
		{
			$b_time = date("Y-m-d H:i:s",time()-600);
			$e_time = date("Y-m-d H:i:s");
            $map['created_at'] = array('between', array($b_time, $e_time));
		}
        
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['created_at'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        //单独游戏场数据
        $oneroom = intval(I('oneroom'));

        if ($oneroom > 0) {
            switch ($oneroom) {
                case 1:
                    $model = D('Kplog');
                    //变动的金币
                    $diff = function (array $v) {
                        return ($v['get'] - $v['use']);
                    };
                    //TODO::字符串解析
                    $remark = function ($str) {
                    };
                    break;
                case 2:
                    $model = D('Brnnlog'); //总获得，如果盈利的话没有减去use和税，如果输的话 是有减去use的
                    $diff = function (array $v) {
                        //if($v['get'] > $v['use'])
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax']);
                        //else    return ($v['get'] + $v['pool_win']- $v['use']);
                    };
                    break;
                case 3:
                    $model = D('Fruitlog'); //水果get 是存收入的包括本金
                    $diff = function (array $v) {
                        //if($v['get'] > $v['use'])
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                        //else  return ($v['get'] + $v['pool_win']- $v['use']);

                    };
                    $map['test_type'] = 1;
                    break;
                case 4:
                    $model = D('Carlog');
                    $diff = function (array $v) {
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax']);
                    };
                    break;
                case 5:
                    $model = D('Qrredbag');
                    $diff = function () {
                    };
                    break;
                case 6:
                    $model = D('Superfruitlog');
                    $diff = function (array $v) {
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                        //if($v['get'] > $v['use'])
                        //	return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                        //else  return ($v['get'] + $v['pool_win']- $v['use']);
                    };
                    break;
                case 7:
                    $model = D('Fruitlog'); //水果get 是存收入的包括本金
                    $diff = function (array $v) {
                        //if($v['get'] > $v['use'])
                        return ($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                        //else  return ($v['get'] + $v['pool_win']- $v['use']);

                    };
                    $map['test_type'] = 2;
                    break;
            }
            $query .= 'oneroom/' . I('oneroom') . '/';
        } else {
            // if ($gold == 0) {
            //     $model = D('Goldget');
            // } elseif ($gold == 1) {
            //     $model = D('Golduse');
            // }

            if (isset($_GET['room'])) {
                $area = intval(I('room'));
                $map['source'] = $area;
                $query .= 'room/' . $area . '/';
            } else {
                $area = -1;
            }
            $model = D('DItemLogs');
			$map['item_id'] = 101;
        }


        //isset($_GET['type']) && isset($_GET['value'])
        if (1) {

            //头部数据
            if (I('async')) {
                $wingold = $losegold = 0;

                //扣除本金的金币算法
                $getUse = function (array $v) {
                    if ($v['get'] > $v['use']) {
                        $ret['get'] = $v['get'] - $v['use'];
                        $ret['use'] = 0;
                    } else {
                        $ret['get'] = 0;
                        $ret['use'] = $v['use'] - $v['get'];
                    }
                    return $ret;
                };

                // echo 1111;
                if ($oneroom) {
                    //echo 2222;
                    print_r($map);
                    $lists = $model->where($map)->select();
                    foreach ($lists as $v) {
                        $operate = $getUse($v);
                        $wingold  += $operate['get'] + $v['pool_win'] - $v['tax'] - $v['pooladd'];
                        $losegold += $operate['use'];
                    }
                } else {
                    //echo 3333;
                    $roomss = array(107, 20, 21, 22, 10, 11, 126, 127, 128, 129, 133);
                    $map['room'] = array('in', $roomss);
                    $winlist = D('Goldget')->where($map)->select();
                    foreach ($winlist as $v) {
                        if (in_array($v['room'], array(107, 20, 21, 22, 10, 11, 126, 127, 128, 129, 133))) continue;
                        $wingold += intval($v['gold']);
                    }

                    $loselist = D('Golduse')->where($map)->select();
                    foreach ($loselist as $v) {
                        if (in_array($v['room'], array(107, 20, 21, 22, 10, 11, 126, 127, 128, 129, 133))) continue;
                        $losegold += intval($v['gold']);
                    }
                }

                $data['wingold'] = number_format($wingold);
                $data['losegold'] = number_format($losegold);
                $data['yinli'] = number_format($losegold - $wingold);
                return $this->ajaxReturn($data);
            }
           
		   $totals = $model->where($map)->count();
            // var_dump($model->where(['uid' => ['GT', 0]])->delete() );
            //echo $totals;
            $list   = $model->where($map)->order('created_at desc')->page($p, $limit)->select();
            foreach ($list as $k=> $v) {
                $list[$k]['account'] = D('DUsers')->get_uid('char_id', intval($v['uid']));
            }
            //print_r($list);exit;
            //$nick   = D('Userinfo')->where(array('uid'=>$map['uid']))->find();

            if ($oneroom) {
                foreach ($list as $k => $v) {
                    $list[$k]['diff_gold'] = $diff($v);
                    $list[$k]['old_gold'] = $v['new_gold'] - $list[$k]['diff_gold'];
                }
            }
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('nick', $nick);
        $this->assign('room', $area);
        $this->assign('area', $area);
        $this->assign('onerooms', $this->onerooms());
        $this->assign('oneroom', $oneroom);
        $this->assign('rooms', D('DItemLogs')->getsource());
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }


    /**
     * 钥匙 获得/消耗日志
     */
    public function diamondlog($p = 1)
    {
        $limit = 20;

        $query = '';

        $gold = intval(I('gold'));
        $query .= 'gold/' . $gold . '/';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = intval($value);
                    break;
                    break;
                case 1: //角色ID
                    $map['uid'] = D('Users')->get_uid('char_id', trim($value));
                    break;
                case 2: //手机号
                    $map['uid'] = D('Users')->get_uid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $map['uid'] = D('Users')->get_uid('nickname', trim($value));
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time'));
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        //单独游戏场数据
        $oneroom = intval(I('oneroom'));
        if ($oneroom) {
            switch ($oneroom) {
                case 5:
                    $model = D('Qrredbag');
                    $diff = function (array $v) {
                        return ($v['get'] - $v['use'] - $v['tax']);
                    };
                    break;
            }
            $query .= 'oneroom/' . I('oneroom') . '/';
        } else {
            if ($gold == 0) {
                $model = D('Diamodget');
            } elseif ($gold == 1) {
                $model = D('Diamoduse');
            }

            if (isset($_GET['room'])) {
                $area = I('room');
                $map['room'] = $area;
                $query .= 'room/' . $area . '/';
            } else {
                $area = -1;
            }
        }

        if (isset($_GET['type']) && isset($_GET['value'])) {
            //头部数据
            if (I('async')) {
                $wingold = $losegold = 0;
                if ($oneroom) {
                    $lists = $model->where($map)->select();
                    foreach ($lists as $v) {
                        $wingold  += $v['get'] - $v['tax'];
                        $losegold += $v['use'];
                    }
                } else {

                    $winlist = D('Diamodget')->where($map)->select();
                    foreach ($winlist as $v) {
                        if (in_array($v['room'], array(30))) continue;
                        $wingold += intval($v['diamond']);
                    }

                    $loselist = D('Diamoduse')->where($map)->select();
                    foreach ($loselist as $v) {
                        if (in_array($v['room'], array(30))) continue;
                        $losegold += intval($v['diamond']);
                    }
                }

                $data['wingold'] = number_format($wingold);
                $data['losegold'] = number_format($losegold);
                $data['yinli'] = number_format($losegold - $wingold);
                return $this->ajaxReturn($data);
            }

            $totals = $model->where($map)->count();
            $list   = $model->where($map)->order('c_time desc')->page($p, $limit)->select();
            $nick   = D('Userinfo')->where(array('uid' => $map['uid']))->find();

            if ($oneroom) {
                foreach ($list as $k => $v) {
                    $list[$k]['diff_dia'] = $diff($v);
                    $list[$k]['old_dia'] = $v['new_dia'] - $list[$k]['diff_dia'];
                }
            }
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('nick', $nick);
        $this->assign('room', $area);
        $this->assign('area', $area);
        $this->assign('rooms', $this->path());
        $this->assign('onerooms', array_slice($this->onerooms(), 4, 1, true));
        $this->assign('oneroom', $oneroom);
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 晶石 获得/消耗日志
     */
    public function ticketlog($p = 1)
    {
        $limit = 20;

        $query = '';

        $gold = intval(I('gold'));
        $query .= 'gold/' . $gold . '/';

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = D('DUsers')->get_charid('uid', intval($value));
                    break;
                    break;
                case 1: //角色ID
                    $map['uid'] = intval($value);
                    break;
                case 2: //手机号
                    $map['uid'] = D('DUsers')->get_charid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $map['uid'] = D('DUsers')->get_charid('nickname', trim($value));
                    break;
            }

            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = I('b_time');
        $e_time = I('e_time');
        if(empty(I('b_time')) && empty(I('e_time')))
		{
			$b_time = date("Y-m-d")." 00:00:00";
			$e_time = date("Y-m-d H:i:s");
            $map['created_at'] = array('between', array($b_time, $e_time));
		}
        
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['created_at'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }
		
        //强制当月时间
        $b_time = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $e_time = date("Y-m-d H:i:s");
        $map['created_at'] = array('between', array($b_time, $e_time));

        $model = D('DItemLogs');
		
        if ($gold == 1) {
           $map['num'] = array('gt', 0);
        } elseif ($gold == 2) {
           $map['num'] = array('lt', 0);
        }

        if (isset($_GET['room'])) {
            $area = I('room');
            $map['source'] = $area;
            $query .= 'room/' . $area . '/';
        } else {
            $area = -1;
        }

        $map['item_id'] = 103;

        //isset($_GET['type']) && isset($_GET['value'])



        //顶部数据
        if (I('async')) {
            $total_ticket = 0;
            $lists = $model->where($map)->select();

            foreach ($lists as $v) {
                $total_ticket += $v['ticket'];
            }

            $data['totalgold'] = number_format($total_ticket);
            return $this->ajaxReturn($data);
        }

        $totals = $model->where($map)->count();
        $list = $model->where($map)->order('created_at desc')->page($p, $limit)->select();
        foreach ($list as $k=> $v) {
            $list[$k]['account'] = D('DUsers')->get_uid('char_id', intval($v['uid']));
        }
        //$nick = D('Userinfo')->where(array('uid' => $map['uid']))->find();
        // var_dump($list);exit;
        // var_dump($model->where(['uid' => ['GT', 0]])->delete());

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_list', $list);
       // $this->assign('nick', $nick);
        $this->assign('room', $area);
        $this->assign('area', $area);
        $this->assign('rooms', D('DItemLogs')->getsource());
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }
    /**
     * 宝石 获得/消耗日志
     */
    public function gemstonelog($p = 1)
    {
        $limit = 35; 

        $query = '';

        $b_time = I('b_time');
        $e_time = I('e_time');
        if(empty(I('b_time')) && empty(I('e_time')))
		{
            $b_time = date("Y-m-d",strtotime('-30 days')); //一个月
            $e_time = date('Y-m-d', strtotime("1 week"));
            $map['date'] = array('between', array($b_time, $e_time));
		}
        
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['date'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['date'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['date'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }
		
        $model = D('DGoldDayStats');
		 
        $totals = $model->where($map)->count();
        $list = $model->where($map)->order('date desc')->field('*,0 as cash_ex')->page($p, $limit)->select();
        $newList = array(); //构建新返回数据 
        $totalList = array(); 
        foreach ($list as $item) {
            $totalList['date']='总：'; 
            $totalList['cash_add']+=$item['cash_add']; 
            $totalList['cash_ex_gold']+=$item['cash_ex_gold']; 
            $totalList['cash_ex_redpackage']+=$item['cash_ex_redpackage'];
            $totalList['cash_ex']+=$item['cash_add']-$item['cash_ex_gold']-$item['cash_ex_redpackage']; 
            $item['cash_ex']=$item['cash_add']-$item['cash_ex_gold']-$item['cash_ex_redpackage']; 
            $newList[]=$item;
        }
        $newList[]=$totalList;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_list', $newList);
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }
    /**
     * 充值档位
     */
    protected function recharge_goods()
    {
        $rechargeConfig = D('ConfigByshop')->getRecharge();
        $goods = array();
        foreach ($rechargeConfig as $k => $v) {
            $str = '';
            if (in_array($k, range(401, 405))) {
                $str = '(一本万利)';
            }
            $goods[$v['id']] = $str . $v['name'];
        }
	
        return $goods;
    }

    /**
     * 计算开始结束时间
     */
    protected function _calculatetime()
    {

        if (isset($_GET['type'])) {
            $type = intval(I('type'));
            if ($type == 0) { //按月

                if (isset($_GET['month'])) {
                    $month = intval(I('month'));
                    $monarr = monthday(NOW_TIME, $month);

                    $b_time = strtotime($monarr[0]);
                    $now_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME)); //现在时间
                    $e_time = strtotime($monarr[1]);
                    if ($now_time < $e_time) {
                        $e_time = $now_time;
                    }
                } else {
                    $b_time = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y"))));
                    $e_time = NOW_TIME;
                }
            } elseif ($type == 1) { //按天数
                $b_time = strtotime(I('b_time'));
                $e_time = strtotime(I('e_time'));

                if (!isset($_GET['b_time']) || !isset($_GET['e_time'])) {
                    $this->error('请选择时间');
                }
            }
        } else { //默认当月数据
            //$b_time = strtotime(date("Y-m-d",mktime(0, 0, 0, date("m"),1,date("Y")))); //一个月
            //date('Y-m-d H:i:s', strtotime("-2 week"));
            $b_time = strtotime(date('Y-m-d H:i:s', strtotime("-1 week")));
            $e_time = NOW_TIME;
        }

        $data['b_time'] = $b_time;
        $data['e_time'] = $e_time;

        return $data;
    }

    /**
     * 多种查询条件
     */
    protected function _query()
    {
        $type = intval(I('type'));
        $value = I('value');
        $query = '';

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['char_id'] = D('Users')->get_charid('uid', $value);
                    break;
                case 1:
                    $map['char_id'] = intval($value);
                    break;
                case 2:
                    $map['char_id'] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['char_id']);
                    }
                    $map['char_id'] = array('in', $ids);
                    break;
            }

            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $ret = array(
            'map' => $map,
            'query' => $query
        );

        return $ret;
    }

    //计算每天红包领取金额
    public function redbagstat()
    {
        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'] + 86399;

        //红包
        $config = D('ConfigByshop')->getConfig();
        $red_goods_id = array();
        foreach ($config as $k => $v) {
            if ($v['cost_type'] == 1) {
                $red_goods_id[] = intval($k);
            }
        }
        //红包id
        $goods_id = array_unique($red_goods_id);

        $map['create_time'] = array('between', array($b_time, $e_time));
        $map['state'] = 1;
        // $list = D('Shops')->where($map)->select();
        $list = D('RedpacketLogs')->where($map)->select();

        $data = array();
        foreach ($list as $v) {
            $date = strtotime(date('Y-m-d', $v['create_time']));
            if (!in_array($v['goods_id'], $goods_id)) continue;
            $data[$date]['uids'][] = $v['uid'];
            $data[$date]['money'] += intval($config[$v['goods_id']]['item_num']);
        }

        $no = $total = 0;
        foreach ($data as $k => $v) {
            $data[$k]['men'] = count(array_unique($v['uids']));
            $total += $v['money'];
            $no++;
        }
        krsort($data);

        $this->assign('avg', $total / $no);
        $this->assign('_list', $data);
        $this->display();
    }

    //每日消耗统计
    public function everyfromwing_bk()
    {
        $nper = intval(I('nper')) ? intval(I('nper')) : 1;
        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'] + 86399; // 当前时间+24小时-1秒

        $map['time'] = array('between', array($b_time, $e_time));
        $map['nper'] = $nper;
        $list = D('Everyfromwing')->where($map)->order('time desc')->select();

        //echo("<script>console.log(".json_encode($map).");</script>");
        //充值数据
        $pap['addtime'] = array('between', array($b_time, $e_time));
        $pap['state'] = 3;
        $pays = D('Paylog')->where($pap)->select();
        $paylist = $exchanges = array();
        foreach ($pays as $v) {
            $date = strtotime(date('Y-m-d', $v['addtime']));
            $paylist[$date] += $v['money'];
        }

        //红包消耗
        $config = D('ConfigByshop')->getConfig();
        foreach ($config as $k => $v) {
            if ($v['cost_type'] == 1) $red_goods_id[] = intval($k);
        }
        $goods_id = array_unique($red_goods_id);
        $sap['c_time'] = array('between', array($b_time, $e_time));
        $sap['state'] = 1;
        $shopExchanges = D('Shops')->where($sap)->select();
        foreach ($shopExchanges as $v) {
            if (!in_array($v['goods_id'], $goods_id)) continue;
            $date = strtotime(date('Y-m-d', $v['c_time']));
            $exchanges[$date] += intval($config[$v['goods_id']]['item_num']);
        }

        $data = array();
        foreach ($list as $k => $v) {
            $list[$k]['money'] = intval($paylist[$v['time']]);
            $data['allmoney'] += intval($paylist[$v['time']]);
            $list[$k]['red'] = intval($exchanges[$v['time']]);
            $data['allred'] += intval($exchanges[$v['time']]);
            $data['total'] += intval($v['total']);
            $data['no']++;
        }

        $this->assign('nperlist', C('PLAN_NUMS'));
        $this->assign('nownper', $nper);
        $this->assign('data', $data);
        $this->assign('nper', $map['nper']);
        $this->assign('_list', $list);
        $this->display();
    }

    public function everyfromwing()
    {
        $nper = intval(I('nper')) ? intval(I('nper')) : 3;
        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'] + 86399; // 当前时间+24小时-1秒

        $resList = array();
        $data = array();
        $temp = 0;

        for ($i = intval(date('Ymd', $e_time)); $i >= intval(date('Ymd', $b_time)); $i = intval(date('Ymd', $e_time - $temp))) {
            $real_b_time = strtotime($i . "000000");
            $real_e_time = strtotime($i . "235959");

            $map['time'] = array('between', array($real_b_time, $real_e_time));
            $listAll = D('Everyfromwing')->where($map)->field('time,total,nper')->order('time desc')->select();

            $arr = array();
            foreach ($listAll as $ele) {
                if ($ele['nper'] == 0 || $ele['nper'] == 111) {
                    continue;
                }

                $arr[$ele['time']]['total'] += $ele['total'];
                $arr[$ele['time']]['time'] = $ele['time'];
            }

            //充值数据
            $pap['addtime'] = array('between', array($real_b_time, $real_e_time));
            $pap['state'] = 3;
            $pays = D('Paylog')->where($pap)->select();
            $paylist = $exchanges = array();
            foreach ($pays as $v) {
                // if (intval($v['from_id'] / 1000000) != $nper) {
                //     continue;
                // }

                $date = strtotime(date('Y-m-d', $v['addtime']));
                $paylist[$date] += $v['money'];
            }

            //红包消耗
            $config = D('ConfigByshop')->getConfig();
            foreach ($config as $k => $v) {
                if ($v['cost_type'] == 1) $red_goods_id[] = intval($k);
            }

            $goods_id = array_unique($red_goods_id);
            $sap['create_time'] = array('between', array($real_b_time, $real_e_time));
            $sap['state'] = 1;
            $shopExchanges = D('RedpacketLogs')->where($sap)->select();
            foreach ($shopExchanges as $v) {
                if (!in_array($v['goods_id'], $goods_id)) continue;
                // $userInfo = D('Users')->where(array('id' => intval($v['uid'])))->field('from_id')->find();
                // $userNper =  intval($userInfo['from_id'] / 1000000);
                // if ($userNper != $nper) {
                //     continue;
                // }

                $date = strtotime(date('Y-m-d', $v['create_time']));
                $exchanges[$date] += intval($config[$v['goods_id']]['item_num']);
            }

            foreach ($arr as $k => $v) {
                $arr[$k]['money'] = intval($paylist[$v['time']]);
                $data['allmoney'] += intval($paylist[$v['time']]);
                $arr[$k]['red'] = intval($exchanges[$v['time']]);
                $data['allred'] += intval($exchanges[$v['time']]);
                $data['total'] += intval($v['total']);
                $data['no']++;

                $resList[$k] = $arr[$k];
            }

            $temp = $temp + 86400;
        }

        $this->assign('nperlist', C('PLAN_NUMS'));
        $this->assign('nownper', $nper);
        $this->assign('data', $data);
        $this->assign('nper', $nper);
        $this->assign('_list', $resList);
        $this->display();
    }

    //百人水池记录
    public function brpool($p = 1)
    {
        $limit = 20;
        $query = '';

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time'));
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'c_time desc';
        }

        $totals = D('Brpool')->where($map)->count();
        $list   = D('Brpool')->where($map)->page($p, $limit)->order($orders)->select();

        //当天税统计
        if (!isset($_GET['b_time'])) {
            $b_time = strtotime(date('Y-m-d'));
        }
        $e_time = $b_time + 86399;
        $tap['c_time'] = array('between', array($b_time, $e_time));
        $alls = D('Brpool')->where($tap)->select();
        $taxArr = array();
        $taxArr['time'] = $b_time;
        foreach ($alls as $v) {
            $taxArr['tax'] += $v['int'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('taxarr', $taxArr);
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    public function carpool($p = 1)
    {
        $limit = 20;

        $query = '';

        $b_time = strtotime(I('b_time'));

        $e_time = strtotime(I('e_time'));

        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {

            $map['c_time'] = array('between', array($b_time, $e_time));

            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {

            $map['c_time'] = array('elt', $e_time);

            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {

            $map['c_time'] = array('egt', $b_time);

            $query .= 'b_time/' . I('b_time') . '/';
        }


        $orderby = trim(I('orderby'));

        $order   = trim(I('order'));

        if ($orderby != '' && $order != '') {

            $orders = $orderby . ' ' . $order;
        } else {

            $orders = 'c_time desc';
        }



        $totals = D('Carpool')->where($map)->count();

        $list   = D('Carpool')->where($map)->page($p, $limit)->order($orders)->select();

        $taxArr = array();

        if (!isset($_GET['b_time'])) {

            $b_time = strtotime(date('Y-m-d'));
        }

        $taxArr['time'] = $b_time;


        $pageNav = new \Think\Page($totals, $limit);

        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_list', $list);

        $this->assign('taxarr', $taxArr);

        $this->assign('query', $query);

        $this->assign('_page', $pageNav->show());

        $this->display();
    }

    // 普通水果水池
    public function fruitpool($p = 1)
    {
        if (IS_POST) {
            // %% 增减水果水池
            // %%http://127.0.0.1:9991/cgi-bin/gm_oprate:add_fruit_special_pool?
            // %%参数：sign=Sign&addNum=1000&addIndex=1&testType=1
            // %%
            // addNum = 当前水池数值 * -1 + 200000000

            $addIndex = intval(I('addIndex'));
            $map['testType'] = 1;
            $list = D('Fruitpool')->where($map)->order('c_time desc')->find();
            if (!!$list) {
                // // 发送到游戏服务器
                if ($addIndex < 4) {
                    $addNum = intval($list[$addIndex]) * -1 + C('FRUIT_POOL_OBJECT_OF_REFERENCE');
                } else {
                    $addNum = intval($list[$addIndex]) * -1 + C('FRUIT_POOL_OBJECT_OF_REFERENCE_HIGH');
                }

                $url = C('GM_URL') . 'cgi-bin/gm_oprate:';
                $url = $url . 'add_fruit_special_pool?sign=Sign&testType=1' . "&addIndex=" . $addIndex . '&addNum=' . $addNum;
                $ret = file_get_contents($url);
                $retArr = json_decode($ret, true);
                $retArr['_url'] = $url;
                if ($retArr['ret'] == 0) {
                    $array = array(
                        'state' => 1,
                        'addNum' => $addNum,
                        'addIndex' => $addIndex,
                        'data' => $retArr
                    );
                    $this->ajaxReturn($array);
                } else {
                    $array = array(
                        'state' => 0,
                        'error' => '更新到游戏服失败'
                    );
                    $this->ajaxReturn($array);
                }
            } else {
                $array = array(
                    'state' => 0,
                    'error' => '没有记录'
                );
                $this->ajaxReturn($array);
            }
        } else {
            $time = date('YmdHis', NOW_TIME);
            $nowTime =  $time - $time % 1000000;
            $nowTime = strtotime($nowTime);
            $needTime = $nowTime - 86400 * 3; // 七天前的时间戳
            $deleteWhere['c_time'] = array('LT', $needTime);
            $res = D('Fruitpool')->where($deleteWhere)->delete(); // 删除七天前的记录

            $limit = 20;
            $query = '';
            $b_time = strtotime(I('b_time'));
            $e_time = strtotime(I('e_time'));

            if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
                $map['c_time'] = array('between', array($b_time, $e_time));
                $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
            } else if (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
                $map['c_time'] = array('elt', $e_time);
                $query .= 'e_time/' . I('e_time') . '/';
            } else if (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
                $map['c_time'] = array('egt', $b_time);
                $query .= 'b_time/' . I('b_time') . '/';
            }

            $orderby = trim(I('orderby'));
            $order   = trim(I('order'));
            if ($orderby != '' && $order != '') {
                $orders = $orderby . ' ' . $order;
            } else {
                $orders = 'c_time desc';
            }

            $testType = 1;
            if (intval(I('show')) == 4) {
                // 站内场
                $testType = 2;
            }

            // if (intval(I('show')) == 2) {
            //     // 小注
            //     $map['betIndex'] = array('between', array(1, 3));
            // } else if (intval(I('show')) == 3) {
            //     // 大注
            //     $map['betIndex'] = array('between', array(4, 10));
            // }

            $map['testType'] = intval($testType);
            $totals = D('Fruitpool')->where($map)->count();
            $list   = D('Fruitpool')->where($map)->page($p, $limit)->order($orders)->select();
            $taxArr = array();
            // totalNum 当前水池总数, diffNum 变化量, betRet 下注回收, earnRet 系统赢取回收, testType试玩类型, time 时间
            if (!isset($_GET['b_time'])) {
                $b_time = strtotime(date('Y-m-d'));
            }

            $taxArr['time'] = $b_time;
            $pageNav = new \Think\Page($totals, $limit);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

            $this->assign('_list', $list);
            $this->assign('taxarr', $taxArr);
            $this->assign('query', $query);
            $this->assign('_page', $pageNav->show());
            $this->display();
        }
    }

    // 普通水果水池站内
    public function fruitpool2($p = 1)
    {
        if (IS_POST) {
            // %% 增减水果水池
            // %%http://127.0.0.1:9991/cgi-bin/gm_oprate:add_fruit_special_pool?
            // %%参数：sign=Sign&addNum=1000&addIndex=1&testType=1
            // %%
            // addNum = 当前水池数值 * -1 + 200000000

            $addIndex = intval(I('addIndex'));
            $map['testType'] = 2;
            $list = D('Fruitpool')->where($map)->order('c_time desc')->find();
            if (!!$list) {
                // // 发送到游戏服务器
                if ($addIndex < 4) {
                    $addNum = intval($list[$addIndex]) * -1 + C('FRUIT_POOL_OBJECT_OF_REFERENCE');
                } else {
                    $addNum = intval($list[$addIndex]) * -1 + C('FRUIT_POOL_OBJECT_OF_REFERENCE_HIGH');
                }

                $url = C('GM_URL') . 'cgi-bin/gm_oprate:';
                $url = $url . 'add_fruit_special_pool?sign=Sign&testType=2' . "&addIndex=" . $addIndex . '&addNum=' . $addNum;
                $ret = file_get_contents($url);
                $retArr = json_decode($ret, true);
                $retArr['_url'] = $url;
                if ($retArr['ret'] == 0) {
                    $array = array(
                        'state' => 1,
                        'addNum' => $addNum,
                        'addIndex' => $addIndex,
                        'data' => $retArr
                    );
                    $this->ajaxReturn($array);
                } else {
                    $array = array(
                        'state' => 0,
                        'error' => '更新到游戏服失败'
                    );
                    $this->ajaxReturn($array);
                }
            } else {
                $array = array(
                    'state' => 0,
                    'error' => '没有记录'
                );
                $this->ajaxReturn($array);
            }
        } else {
            $time = date('YmdHis', NOW_TIME);
            $nowTime =  $time - $time % 1000000;
            $nowTime = strtotime($nowTime);
            $needTime = $nowTime - 86400 * 7; // 七天前的时间戳
            $deleteWhere['c_time'] = array('LT', $needTime);
            $res = D('Fruitpool')->where($deleteWhere)->delete(); // 删除七天前的记录

            $limit = 20;
            $query = '';
            $b_time = strtotime(I('b_time'));
            $e_time = strtotime(I('e_time'));

            if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
                $map['c_time'] = array('between', array($b_time, $e_time));
                $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
            } else if (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
                $map['c_time'] = array('elt', $e_time);
                $query .= 'e_time/' . I('e_time') . '/';
            } else if (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
                $map['c_time'] = array('egt', $b_time);
                $query .= 'b_time/' . I('b_time') . '/';
            }

            $orderby = trim(I('orderby'));
            $order   = trim(I('order'));
            if ($orderby != '' && $order != '') {
                $orders = $orderby . ' ' . $order;
            } else {
                $orders = 'c_time desc';
            }

            $testType = 2;
            if (intval(I('show')) == 1) {
                // 站内场
                $testType = 2;
            }

            // if (intval(I('show')) == 2) {
            //     // 小注
            //     $map['betIndex'] = array('between', array(1, 3));
            // } else if (intval(I('show')) == 3) {
            //     // 大注
            //     $map['betIndex'] = array('between', array(4, 10));
            // }

            $map['testType'] = intval($testType);
            $totals = D('Fruitpool')->where($map)->count();
            $list   = D('Fruitpool')->where($map)->page($p, $limit)->order($orders)->select();
            $taxArr = array();
            // totalNum 当前水池总数, diffNum 变化量, betRet 下注回收, earnRet 系统赢取回收, testType试玩类型, time 时间
            if (!isset($_GET['b_time'])) {
                $b_time = strtotime(date('Y-m-d'));
            }

            $taxArr['time'] = $b_time;
            $pageNav = new \Think\Page($totals, $limit);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

            $this->assign('_list', $list);
            $this->assign('taxarr', $taxArr);
            $this->assign('query', $query);
            $this->assign('_page', $pageNav->show());
            $this->display();
        }
    }

    // 超级水果水池
    public function fruitsuperpool($p = 1)
    {
        if (IS_POST) {
            // %% 增减超级水果水池
            // %%http://127.0.0.1:9991/cgi-bin/gm_oprate:add_super_fruit_special_pool?
            // %%参数：sign=Sign&addNum=1000&addIndex=1&testType=1
            // %%
            // addNum = 当前水池数值 * -1 + 200000000
            $addIndex = intval(I('addIndex'));
            $map['testType'] = 2;
            $list = D('Fruitsuperpool')->where($map)->order('c_time desc')->find();
            if (!!$list) {
                // // 发送到游戏服务器
                if ($addIndex < 4) {
                    $addNum = intval($list[$addIndex]) * -1 + C('SUPER_FRUIT_POOL_OBJECT_OF_REFERENCE');
                } else {
                    $addNum = intval($list[$addIndex]) * -1 + C('SUPER_FRUIT_POOL_OBJECT_OF_REFERENCE_HIGH');
                }

                $url = C('GM_URL') . 'cgi-bin/gm_oprate:';
                $url = $url . 'add_super_fruit_special_pool?sign=Sign&testType=2' . "&addIndex=" . $addIndex . '&addNum=' . $addNum;
                $ret = file_get_contents($url);
                $retArr = json_decode($ret, true);
                $retArr['_url'] = $url;
                if ($retArr['ret'] == 0) {
                    $array = array(
                        'state' => 1,
                        'addNum' => $addNum,
                        'addIndex' => $addIndex,
                        'data' => $retArr
                    );
                    $this->ajaxReturn($array);
                } else {
                    $array = array(
                        'state' => 0,
                        'error' => '更新到游戏服失败'
                    );
                    $this->ajaxReturn($array);
                }
            } else {
                $array = array(
                    'state' => 0,
                    'error' => '没有记录'
                );
                $this->ajaxReturn($array);
            }
        } else {
            $time = date('YmdHis', NOW_TIME);
            $nowTime =  $time - $time % 1000000;
            $nowTime = strtotime($nowTime);
            $needTime = $nowTime - 86400 * 1; // 七天前的时间戳
            $deleteWhere['c_time'] = array('LT', $needTime);
            $res = D('Fruitsuperpool')->where($deleteWhere)->delete(); // 删除七天前的记录

            $limit = 20;
            $query = '';
            $b_time = strtotime(I('b_time'));
            $e_time = strtotime(I('e_time'));
            if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
                $map['c_time'] = array('between', array($b_time, $e_time));
                $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
            } else if (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
                $map['c_time'] = array('elt', $e_time);
                $query .= 'e_time/' . I('e_time') . '/';
            } else if (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
                $map['c_time'] = array('egt', $b_time);
                $query .= 'b_time/' . I('b_time') . '/';
            }

            $orderby = trim(I('orderby'));
            $order   = trim(I('order'));
            if ($orderby != '' && $order != '') {
                $orders = $orderby . ' ' . $order;
            } else {
                $orders = 'c_time desc';
            }

            $testType = 2;
            if (intval(I('show')) == 1) {
                // 红包场水果
                $testType = 1;
            }

            if (intval(I('show')) == 2) {
                // 小注
                $map['betIndex'] = array('between', array(1, 3));
            } else if (intval(I('show')) == 3) {
                // 大注
                $map['betIndex'] = array('between', array(4, 10));
            }

            $map['testType'] = intval($testType);
            $totals = D('Fruitsuperpool')->where($map)->count();
            $list   = D('Fruitsuperpool')->where($map)->page($p, $limit)->order($orders)->select();
            $taxArr = array();
            // totalNum 当前水池总数, diffNum 变化量, betRet 下注回收, earnRet 系统赢取回收, testType试玩类型, time 时间
            if (!isset($_GET['b_time'])) {
                $b_time = strtotime(date('Y-m-d'));
            }

            $taxArr['time'] = $b_time;
            $pageNav = new \Think\Page($totals, $limit);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

            $this->assign('_list', $list);
            $this->assign('taxarr', $taxArr);
            $this->assign('query', $query);
            $this->assign('_page', $pageNav->show());
            $this->display();
        }
    }


    private function airlabapoolFun($p = 1, $testType)
    {
        $time = date('YmdHis', NOW_TIME);
        $nowTime =  $time - $time % 1000000;
        $nowTime = strtotime($nowTime);
        $needTime = $nowTime - 86400 * 1; // 七天前的时间戳
        $deleteWhere['c_time'] = array('LT', $needTime);
        $res = D('Airlabapool')->where($deleteWhere)->delete(); // 删除七天前的记录

        $limit = 20;
        $query = '';
        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time'));
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } else if (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } else if (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'c_time desc';
        }

        $map['testType'] = $testType;
        $totals = D('Airlabapool')->where($map)->count();
        $list   = D('Airlabapool')->where($map)->page($p, $limit)->order($orders)->select();
        $taxArr = array();
        // totalNum 当前水池总数, diffNum 变化量, betRet 下注回收, earnRet 系统赢取回收, testType试玩类型, time 时间
        if (!isset($_GET['b_time'])) {
            $b_time = strtotime(date('Y-m-d'));
        }

        $taxArr['time'] = $b_time;
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $this->assign('_list', $list);
        $this->assign('taxarr', $taxArr);
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    // 机战水池
    public function airlabapool()
    {
        $this->airlabapoolFun($p = 1, 1);
    }

    // 超级机战水池
    public function airlabasuperpool()
    {
        $this->airlabapoolFun($p = 1, 2);
    }

    //红包场水池记录
    public function redpool($p = 1)
    {
        $limit = 20;
        $query = '';

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time'));
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'c_time desc';
        }

        $totals = D('Redpool')->where($map)->count();
        $list   = D('Redpool')->where($map)->page($p, $limit)->order($orders)->select();

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 分享的玩家信息
     */
    public function shareusers($p = 1)
    {
        $limit = 20;
        $query = '';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:
                    $map['char_id'] = D('Users')->get_charid('uid', $value);
                    break;
                case 1:
                    $map['char_id'] = intval($value);
                    break;
                case 2:
                    $map['char_id'] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3:
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['char_id']);
                    }
                    $map['char_id'] = array('in', $ids);
                    break;
            }

            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        if (isset($_GET['b_time'])) {
            $map['time'] = $b_time;
            $query .= 'b_time/' . I('b_time') . '/';
            $model = D('Everydayshareusers');
        } else {
            $model = D('Shareusers');
        }

        $orderby = trim(I('orderby'));
        $order = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'type1 desc';
        }

        if (I('async') == 1) {
            $listAll = $model->where($map)->select();
            $asyncData['totals'] = count($listAll);
            foreach ($listAll as $v) {
                $asyncData['type1'] += intval($v['type1']);
                $asyncData['type2'] += intval($v['type2']);
            }
            $asyncData['diamonds'] = $asyncData['type2'] * 40;
            return $this->ajaxReturn($asyncData);
        }

        $char_ids = $list = array();
        $totals = $model->where($map)->count();
        $list   = $model->where($map)->page($p, $limit)->order($orders)->select();

        foreach ($list as $k => $v) {
            $char_ids[] = $v['char_id'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('char', D('Userinfo')->getUserInfo($char_ids, 'char_id'));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    public function sharedetail($p = 1)
    {
        $limit = 20;

        $map['uid'] = intval(I('char_id'));
        $map['type'] = intval(I('type'));

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
        }

        $totals = D('Share')->where($map)->count();
        $list   = D('Share')->where($map)->page($p, $limit)->order('c_time desc')->select();
        $char_ids = array();
        foreach ($list as $v) {
            $char_ids[] = $v['uid'];
            $char_ids[] = $v['re_uid'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('type', intval(I('type')));
        $this->assign('userstate', D('Share')->getState($map['uid'], $char_ids));
        $this->assign('char', D('Userinfo')->getUserInfo($char_ids, 'char_id'));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 玩家每日输赢统计
     */
    public function everydaygoldstat($p = 1)
    {
        $limit = 20;
        $query = '';
        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = intval($value);
                    break;
                case 1: //角色ID
                    $map['uid'] = D('Users')->get_uid('char_id', intval($value));
                    break;
                case 2: //手机号
                    $map['uid'] = D('Users')->get_uid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('uid')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['uid']);
                    }
                    $map['uid'] = array('in', $ids);
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        } else {
            $map['uid'] = array('egt', 0);
        }

        //排序
        $room = I('room');
        if ($room) {
            $query .= 'room/' . I('room') . '/';
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
            $orderby_column = $orderby;
            $map[$orderby] = array('ne', null);
        } else {
            $orders = $room . 'stat desc';
            $orderby_column = 'stat';
        }

        //$map['uid'] = array('egt', 0);
        $b_time = I('b_time');
        $e_time = I('e_time');
        if ((!$b_time && !$e_time) || ($b_time == $e_time)) {
            if ($b_time) {
                $b_time = strtotime($b_time);
                $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
            } else {
                $b_time = strtotime(date('Y-m-d', NOW_TIME));
            }
            //一天的数据
            $map['time'] = array('between', array($b_time, $b_time + 86399));

            $totals = D('Everydaygoldstat')->where($map)->count();
            $list   = D('Everydaygoldstat')->where($map)->page($p, $limit)->order($orders)->select();
        } else {
            //多天的数据

            if (!$b_time || !$e_time) $this->error('开始，结束日期不能为空');
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';

            $b_time = strtotime($b_time);
            $e_time = strtotime($e_time);
            if (($e_time - $b_time) / 86400 > 15) {
                $this->error('查询日期不能超过15天');
            }

            $map['time'] = array('between', array($b_time, $e_time + 1));
            $list_all = D('Everydaygoldstat')->where($map)->select();

            $listuid = $listorder = array();
            foreach ($list_all as $v) {

                $listuid[$v['uid']][$room . 'get1'] += $v[$room . 'get1'];
                $listuid[$v['uid']][$room . 'use1'] += $v[$room . 'use1'];
                $listuid[$v['uid']][$room . 'pool_win1'] += $v[$room . 'pool_win1'];
                $listuid[$v['uid']][$room . 'yinli1'] += $v[$room . 'yinli1'];

                $listuid[$v['uid']]['get1'] += $v['get1'];
                $listuid[$v['uid']]['use1'] += $v['use1'];
                $listuid[$v['uid']]['pool_win1'] += $v['pool_win1'];
                $listuid[$v['uid']]['yinli1'] += $v['yinli1'];

                $listuid[$v['uid']][$room . 'get2'] += $v[$room . 'get2'];
                $listuid[$v['uid']][$room . 'use2'] += $v[$room . 'use2'];
                $listuid[$v['uid']][$room . 'pool_win2'] += $v[$room . 'pool_win2'];
                $listuid[$v['uid']][$room . 'yinli2'] += $v[$room . 'yinli2'];

                $listuid[$v['uid']]['get2'] += $v['get2'];
                $listuid[$v['uid']]['use2'] += $v['use2'];
                $listuid[$v['uid']]['pool_win2'] += $v['pool_win2'];
                $listuid[$v['uid']]['yinli2'] += $v['yinli2'];

                $listuid[$v['uid']]['money'] += $v['money'];
                $listuid[$v['uid']]['fd_redbag'] += $v['fd_redbag'];
                $listuid[$v['uid']]['gold_task'] += $v['gold_task'];
                $listuid[$v['uid']]['redtogold'] += $v['redtogold'];
                $listuid[$v['uid']]['redtomoney'] += $v['redtomoney'];
                $listuid[$v['uid']]['gold_free'] += $v['gold_free'];
                $listuid[$v['uid']]['red1003'] += $v['red1003'];
                $listuid[$v['uid']]['yinli'] += $v['yinli'];

                $listuid[$v['uid']]['sget1'] += $v['sget1'];
                $listuid[$v['uid']]['suse1'] += $v['suse1'];

                $listorder[$v['uid']] += $v[$orderby_column];
            }
            echo ("<script>console.log(" . json_encode($list_all) . ");</script>");
            unset($list_all);

            if ($order == 'asc') {
                asort($listorder);
            } else {
                arsort($listorder);
            }

            foreach ($listorder as $k => $v) {
                $list[$k] = $listuid[$k];
                $list[$k]['uid'] = $k;
            }

            $totals = count($listuid);
            $list = array_slice($list, 20 * ($p - 1), 20);
        }

        $uids = array();
        foreach ($list as $v) {
            $uids[] = $v['uid'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $statAreas = array(
            4 => '激情豪车',
            2 => '百人拼牛',
            3 => '红包场水果',
            6 => '倍率场水果',
            'air' => '机战'
        );

        if (I('output') == 1) {
            // 导出数据
            $listAlls   = D('Everydaygoldstat')->where($map)->order($orders)->select();
            //用户信息
            $userInfos = D('Userinfo')->field('uid,char_id,from_id,state,nickname')->select();
            foreach ($userInfos as $v) {
                $chars[$v['uid']] = $v;
            }
            //渠道信息
            $froms = D('DChannels')->getField('from_id,from_name', true);

            $expTitle = $statAreas[$room] . '_玩家每日输赢统计_' . I('b_time');
            $cellName = range('a', 'z');
            $sheets = D('Everydaygoldstat')->setSheets($room);

            $expCellName = array(
                array('a', '用户ID'),
                array('b', '角色ID'),
                array('c', '玩家名称'),
            );
            $step = 3;
            foreach ($sheets as $k => $v) {
                // array_push($expCellName, array($cellName[$step], $v));
                $expCellName[] = array($cellName[$step], $v);
                $step++;
            }
            // array_push($expCellName, array($cellName[$step], '来源'));
            // array_push($expCellName, array($cellName[$step++], '账号状态'));
            $expCellName[] = array($cellName[$step], '来源');
            // $expCellName[] = array($cellName[$step++], '账号状态');

            $expTableData = array();
            $i = 0;
            foreach ($listAlls as $v) {
                $expTableData[$i]['a'] = " " . $v['uid'];
                $expTableData[$i]['b'] = " " . $chars[$v['uid']]['char_id'];
                $expTableData[$i]['c'] = " " . filterUtf8($chars[$v['uid']]['nickname']);
                $step = 3;
                foreach ($sheets as $key => $val) {
                    $expTableData[$i][$cellName[$step]] = " " . number_format($v[$key]);
                    $step++;
                }
                $expTableData[$i][$cellName[$step]] = " " . $froms[$chars[$v['uid']]['from_id']];
                // $expTableData[$i][$cellName[$step++]] = " " . $chars[$v['uid']]['state'] == 1 ? '启用' : '禁用';
                $i++;
            }
            exportExcel($expTitle, $expCellName, $expTableData);
        }

        $roomnum = $room ? $room : 0;
        $usernums[$roomnum] = $totals;

        $this->assign('_page', $pageNav->show());
        $this->assign('usernums', $usernums);
        $this->assign('room', $room);
        $this->assign('query', $query);
        $this->assign('statarea', $statAreas);
        $this->assign('userinfo', D('Userinfo')->getUserInfo($uids));
        $this->assign('users', D('Users')->getUserInfo($uids));
        $this->assign('_list', $list);
        $this->display();
    }

    /**
     * 渠道玩家输赢统计
     */
    public function fromusergoldstat($p = 1)
    {
        $limit = 20;
        $query = '';
        if (isset($_GET['from_id'])) {
            $from_id = intval(I('from_id'));
            $map['from_id'] = $from_id;
            $query .= 'from_id/' . $from_id . '/';
        } else {
            $from_id = 999;
            //$map['from_id'] = $from_id;

        }

        //	echo("<script>console.log(".json_encode($map1).");</script>");

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = intval($value);
                    break;
                case 1: //角色ID
                    $map['uid'] = D('Users')->get_uid('char_id', intval($value));
                    break;
                case 2: //手机号
                    $map['uid'] = D('Users')->get_uid('mobile', trim($value));
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        } else {
            $map['uid'] = array('egt', 150000);
        }

        //排序
        $room = I('room');
        if ($room) {
            $query .= 'room/' . I('room') . '/';
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
            $orderby_column = $orderby;
            $map[$orderby] = array('ne', null);
        } else {
            $orders = $room . 'yinli desc';
            $orderby_column = 'yinli';
        }

        //  $map['from_id'] = $from_id;

        $totals = D('Fromstat')->where($map)->count();
        $list   = D('Fromstat')->where($map)->page($p, $limit)->order($orders)->select();


        $uids = array();
        foreach ($list as $v) {
            $uids[] = $v['uid'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $statAreas = array(
            4 => '激情豪车',
            2 => '百人拼牛',
            3 => '红包场水果',
            6 => '倍率场水果',
        );

        /*  if(I('output') == 1){//导出数据
            $listAlls   = D('Everydaygoldstat')->where($map)->order($orders)->select();
            //用户信息
            $userInfos = D('Userinfo')->field('uid,char_id,from_id,state,nickname')->select();
            foreach ($userInfos as $v){
                $chars[$v['uid']] = $v;
            }
            //渠道信息
            $froms = D('DChannels')->getField('from_id,from_name', true);

            $expTitle = $statAreas[$room].'_玩家每日输赢统计_'.I('b_time');
            $cellName = range('a', 'z');
            $sheets = D('Everydaygoldstat')->setSheets($room);

            $expCellName = array(
                array('a', '用户ID'),
                array('b', '角色ID'),
                array('c', '玩家名称'),
            );
            $step = 3;
            foreach ($sheets as $k=>$v){

                $expCellName[] = array($cellName[$step], $v);
                $step++;
            }

            $expCellName[] = array($cellName[$step], '来源');


            $expTableData = array();
            $i = 0;
            foreach ($listAlls as $v){
                $expTableData[$i]['a'] = " " . $v['uid'];
                $expTableData[$i]['b'] = " " . $chars[$v['uid']]['char_id'];
                $expTableData[$i]['c'] = " " . filterUtf8($chars[$v['uid']]['nickname']);
                $step = 3;
                foreach ($sheets as $key=>$val){
                    $expTableData[$i][$cellName[$step]] = " " . number_format($v[$key]);
                    $step ++;
                }
                $expTableData[$i][$cellName[$step]] = " " . $froms[$chars[$v['uid']]['from_id']];

                $i++;
            }
            exportExcel($expTitle, $expCellName, $expTableData);

        } */


        // $usernums = D('Fromstat')->where($map1)->find();

        //  echo("<script>console.log(".json_encode('---'. $from_id).");</script>");
        $usernums[0] = $totals;
        $this->assign('from_id', $from_id);
        $this->assign('froms', D('DChannels')->getField('from_id,from_name'));
        $this->assign('_page', $pageNav->show());
        $this->assign('usernums', $usernums);
        $this->assign('room', $room);
        $this->assign('query', $query);
        $this->assign('statarea', $statAreas);
        $this->assign('userinfo', D('Userinfo')->getUserInfo($uids));
        $this->assign('users', D('Users')->getUserInfo($uids));
        $this->assign('_list', $list);
        $this->display();
    }

    /* 登录信息统计 */
    public function useriplog($p = 1)
    {
        $limit = 20;
        if (!isset($_GET['state'])) {
            $state = 0;
            $map['state'] = 0;
        } else {
            $state = intval(I('state'));
            $map['state'] = intval(I('state'));
        }

        $room = I('room');
        if ($room == 1) {
            $map['info2'] = array('ne', null);
        } else {
            $map['info1'] = array('ne', null);
        }

        $totals = D('Useriplog')->where($map)->count();

        $list = D('Useriplog')->where($map)->page($p, $limit)->order('num desc')->select();
        //echo json_encode($list);
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('state', $state);
        $this->assign('room', $room);
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    public function forbiduseriplog()
    {
        if (IS_POST) {
            $Useriplog = D('Useriplog');
            $type = intval($_GET['type']);
            if ($type == 1) {
                $map['info1'] = $_GET['value'];;
            } else {
                $map['info2'] = $_GET['value'];;
            }
            if ($info = $Useriplog->where($map)->find()) {
                if (intval($_GET['state']) == 1) {
                    $Useriplog->where(array('_id' => $info['_id']))->save(array('state' => 0));
                    $this->success('启用成功');
                } else {
                    $Useriplog->where(array('_id' => $info['_id']))->save(array('state' => 1));
                    $this->success('禁用成功');
                }
            } else {
                $this->success(json_encode($map));
            }
        } else {
            $this->display();
        }
    }

    public function getCode($mobile = '', $stype = 'sms')
    {

        if ($mobile == '') {
            return $this->error('请输入手机号！');
        } elseif (!preg_match("/^1[345678]{1}\d{9}$/", $mobile)) {
            return $this->error('手机号格式不正确！');
        }

        $mobiobj  = M('Mobicode');

        //发送
        $code = rand(1000, 9999);
        if ($stype == 'sms') {
            $result = $this->sendSMS($mobile, $code);
            $type = 98;
        } else {
            $result = $this->sendVoicecode($mobile, $code);
            $type = 99;
        }

        $data = array(
            'mobile'  => $mobile,
            'code'    => $code,
            'return_code' => $result,
            'addtime' => NOW_TIME,
            'type'    => $type
        );
        $logid = $mobiobj->add($data);
        $logid += 1000;

        return $this->success('发送成功！');
    }

    public function redbaglogs($p = 1)
    {
        $limit = 20;
        $query = '';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = intval($value);
                    break;
                case 1: //角色ID
                    $map['uid'] = D('Users')->get_uid('char_id', intval($value));
                    break;
                case 2: //手机号
                    $map['uid'] = D('Users')->get_uid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('uid')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['uid']);
                    }
                    $map['uid'] = array('in', $ids);
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'c_time desc';
        }

        $room = I('room');
        if ($room) {
            $map['room'] = $room;
            $query .= 'room/' . I('room') . '/';
        } else {
            // $map['room'] = array('neq', 113);//实物兑换消耗红包
            $map['room'] = array('not in', array(153, 113));
        }

        //$map['room'] = array('neq', 153);
        $m_redbaglog = D('Redbaglog');
        $totals = $m_redbaglog->where($map)->count();
        $list   = $m_redbaglog->where($map)->order($orders)->page($p, $limit)->select();

        $uids = array();
        foreach ($list as $v) {
            $uids[] = $v['uid'];
        }

        if (I('async') == 1) {
            $b_time = 0;
            $e_time = 0;
            if (!!$map['c_time']) {
                $b_time = $map['c_time'][1][0];
                $e_time = $map['c_time'][1][1];
            }

            $moneys = 0;
            if ($map['room'][0] == 'not in' && !$map['uid']) {
                $b_time = 0;
                $e_time = 0;
                if (!!$map['c_time']) {
                    $b_time = $map['c_time'][1][0];
                    $e_time = $map['c_time'][1][1];
                }

                if (!!$b_time && !!$e_time) {
                    $totalMoneyObj =  D('Redbaglog')->mongoCode('db.getCollection("stat_redbaglog").aggregate(
                        [
                            { 
                                $match: {
                                    room: {$nin: [153, 113]}, 
                                    c_time: {
                                        $gte: ' . $b_time . ',
                                        $lte: ' . $e_time . ',
                                    }
                                }
                            },
                            { 
                                $group: {
                                    _id: null,
                                    allmoney: { $sum: "$money" }
                                }
                            } 
                        ]);');
                } else if (!!$b_time && !$e_time) {
                    $totalMoneyObj =  D('Redbaglog')->mongoCode('db.getCollection("stat_redbaglog").aggregate(
                        [
                            { 
                                $match: {
                                    room: {$nin: [153, 113]},
                                    c_time: {
                                        $gte: ' . $b_time . '
                                    }
                                }
                            },
                            { 
                                $group: {
                                    _id: null,
                                    allmoney: { $sum: "$money" }
                                }
                            } 
                        ]);');
                } else if (!$b_time && !!$e_time) {
                    $totalMoneyObj =  D('Redbaglog')->mongoCode('db.getCollection("stat_redbaglog").aggregate(
                        [
                            { 
                                $match: {
                                    room: {$nin: [153, 113]}, 
                                    c_time: {
                                        $gte: ' . $e_time . '
                                    }
                                }
                            },
                            { 
                                $group: {
                                    _id: null,
                                    allmoney: { $sum: "$money" }
                                }
                            } 
                        ]);');
                } else {
                    $totalMoneyObj =  D('Redbaglog')->mongoCode('db.getCollection("stat_redbaglog").aggregate(
                        [
                            { 
                                $match: {room: {$nin: [153, 113]}}
                            },
                            { 
                                $group: {
                                    _id: null,
                                    allmoney: { $sum: "$money" }
                                }
                            } 
                        ]);');
                }

                $moneys =  $totalMoneyObj['_batch'][0]['allmoney'] / 10;
            } else {
                $list_c_time = D('Redbaglog')->where($map)->select();
                foreach ($list_c_time as $v) {
                    $moneys += $v['money'] / 10;
                }
            }

            return $this->ajaxReturn(array('moneys' => intval($moneys)));
        }


        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('redrooms', $this->path());
        $this->assign('goods', D('Configgoods')->getConfig());
        $this->assign('userinfo', D('Userinfo')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    //分享玩家统计
    public function shareuserstat($p = 1)
    {
        $limit = 20;
        $query = '';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = intval($value);
                    break;
                case 1: //角色ID
                    $map['uid'] = D('Users')->get_uid('char_id', intval($value));
                    break;
                case 2: //手机号
                    $map['uid'] = D('Users')->get_uid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('uid')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['uid']);
                    }
                    $map['uid'] = array('in', $ids);
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        if (isset($_GET['b_time'])) {
            $map['time'] = strtotime(I('b_time'));
            $query .= 'b_time/' . I('b_time') . '/';
            $model = D('Everydayshareusersstat');
        } else {
            $model = D('Shareusersstat');
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'total_sharecount desc';
        }

        $totals = $model->where($map)->count();
        $list   = $model->where($map)->order($orders)->page($p, $limit)->select();

        $uids = array();
        foreach ($list as $v) {
            $uids[] = $v['uid'];
        }

        $topShow = array();
        $listall = $model->where($map)->select();
        foreach ($listall as $v) {
            $topShow['red'] += $v['red'];
            $topShow['total_sharecount'] += $v['total_sharecount'];
            $topShow['recharge'] += $v['recharge'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('topshow', $topShow);
        $this->assign('userinfo', D('Userinfo')->getUserInfo($uids));
        $this->assign('users', D('Users')->getUserInfo($uids));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    //被分享者
    public function shared($p = 1)
    {
        $limit = 20;
        $map['share_char_id'] = intval(I('char_id'));
        if (!$map['share_char_id']) $this->error('无效分享者！');

        $model = D('Mysqlshareusers');
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['create_time'] = array('between', array(strtotime(I('b_time')), strtotime(I('e_time')) + 86399));
            $this->assign('b_time', I('b_time'));
        }

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['char_id'] = D('Users')->get_charid('uid', trim($value));
                    break;
                case 1: //角色ID
                    $map['char_id'] = intval($value);
                    break;
                case 2: //手机号
                    $map['char_id'] = D('Users')->get_charid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('char_id')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['char_id']);
                    }
                    $map['char_id'] = array('in', $ids);
                    break;
            }
        }

        $totals = $model->where($map)->count();
        $list   = $model->where($map)->order('create_time desc')->page($p, $limit)->select();

        $char_ids = array();
        foreach ($list as $k => $v) {
            $char_ids[] = intval($v['char_id']);
            $char_ids[] = intval(I('char_id'));
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('userinfo', D('Userinfo')->getUserInfo($char_ids, 'char_id'));
        $this->assign('shar_char_id', intval(I('char_id')));
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    //分享抽奖记录
    public function sharelottery($p = 1)
    {
        $limit = 20;
        $query = '';

        $type = intval(I('type'));
        $value = I('value');
        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = intval($value);
                    break;
                case 1: //角色ID
                    $map['uid'] = D('Users')->get_uid('char_id', intval($value));
                    break;
                case 2: //手机号
                    $map['uid'] = D('Users')->get_uid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $userinfo = D('Userinfo')->where(array('nickname' => array('like', trim($value))))->field('uid')->select();
                    $ids = array();
                    foreach ($userinfo as $v) {
                        $ids[] = intval($v['uid']);
                    }
                    $map['uid'] = array('in', $ids);
                    break;
            }
            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time')) + 86399;
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['c_time'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['c_time'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }

        $taskday = intval(I('taskday'));
        if ($taskday) {
            $map['type'] = $taskday;
            $query .= 'taskday/' . I('taskday') . '/';
        }

        if (I('async') == 1) {
            $listall = D('Sharelottery')->where($map)->select();
            $data = array();
            foreach ($listall as $v) {
                $data[$v['goods_id']] += $v['num'] / 10;
            }
            $ret[109] = number_format($data[109], 2);
            return $this->ajaxReturn($ret);
        }

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'c_time desc';
        }

        $totals = D('Sharelottery')->where($map)->count();
        $list   = D('Sharelottery')->where($map)->order($orders)->page($p, $limit)->select();

        $uids = array();
        foreach ($list as $v) {
            $uids[] = $v['uid'];
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('userinfo', D('Userinfo')->getUserInfo($uids));
        $this->assign('goods', D('Configgoods')->getConfig());
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 赢取数据排序
     */
    public function orderwingold($p = 1)
    {
        $orderby = trim(I('orderby')); //字段
        $order   = trim(I('order'));
        $limit = 5;
        $sqllimit = 200;
        $totals = 200;
        $user = D('Userinfo');
        if ($orderby == '' || $order == '') {
            //continue;
        }

        if ($orderby == 'gold' || $orderby == 'wingold' ||  $orderby == 'redbag'  ||  $orderby == 'bet_gold' || $orderby == 'pays' || $orderby == 'gold_stat') { //不排序
            $orders = $orderby . ' ' . $order;
            $map['uid'] = array('egt', 0);
            $map['gold'] = array('egt', 0);
            $map['wingold'] = array('egt', 0);


            $list   = $user->where($map)->page($p, $sqllimit)->order($orders)->field('uid,nickname,gold,wingold,redbag,bet_gold,pays,gold_stat')->select();
            $uids = array();

            $lists = array();
            foreach ($list as $v) {

                $uids[] = $v['uid'];
                $lists[$v['uid']] = $v;
            }
            unset($list);
            $u_map['uid'] = array('in', $uids);
            $yincar = D('caruser')->where($u_map)->field('uid,yinli,get')->select();
            $Superinfo = D('Superfruituser')->where($u_map)->field('uid,get1,get2')->select();
            $Fruitinfo = D('Fruituser')->where($u_map)->field('uid,get1,get2')->select();
            $Brnninfo = D('Brnnuser')->where($u_map)->field('uid,get1,get2')->select();


            foreach ($yincar as $v) {
                $lists[$v['uid']]['caryinli'] = $v['yinli'];
                $lists[$v['uid']]['carget'] = $v['get'];
            }
            unset($yincar);
            //echo json_encode($lists);

            //echo json_encode($lists);

            foreach ($Superinfo as $v) {
                $lists[$v['uid']]['sufru1'] = $v['get1'];
                $lists[$v['uid']]['sufru2'] = $v['get2'];
            }
            unset($Superinfo);


            foreach ($Fruitinfo as $v) {
                $lists[$v['uid']]['fru1'] = $v['get1'];
                $lists[$v['uid']]['fru2'] = $v['get2'];
            }
            unset($Fruitinfo);


            foreach ($Brnninfo as $v) {
                $lists[$v['uid']]['brn1'] = $v['get1'];
                $lists[$v['uid']]['brn2'] = $v['get2'];
            }
            unset($Brnninfo);

            //获取存储的历史数据

            $l_map['user_id'] = array('in', $uids);
            $log_info2 = D('Activatelogs')->where($l_map)->field('user_id,wingold')->select();

            foreach ($log_info2 as $v) {
                $lists[$v['uid']]['logwin'] = intval($lists[$v['uid']]['sufru1']) + intval($lists[$v['uid']]['fru1']) + intval($lists[$v['uid']]['brn1']) - intval($v['wingold']);
            }
            unset($log_info2);
        } elseif ($orderby == 'caryinli' || $orderby == 'carget') { //豪车

            if ($orderby == 'caryinli') $orderby = "yinli";
            else   $orderby = "get";
            $orders = $orderby . ' ' . $order;

            $map['uid'] = array('egt', 0);
            $yincar = D('caruser')->where($map)->page($p, $sqllimit)->order($orders)->field('uid,yinli,get')->select();
            $uids = array();
            $lists = array();
            foreach ($yincar as $v) {
                $uids[] = $v['uid'];
                $lists[$v['uid']]['caryinli'] = $v['yinli'];
                $lists[$v['uid']]['carget'] = $v['get'];
                $lists[$v['uid']]['uid'] = $v['uid'];
            }
            unset($yincar);

            $u_map['uid'] = array('in', $uids);
            $Superinfo = D('Superfruituser')->where($u_map)->field('uid,get1,get2')->select();
            $Fruitinfo = D('Fruituser')->where($u_map)->field('uid,get1,get2')->select();
            $Brnninfo = D('Brnnuser')->where($u_map)->field('uid,get1,get2')->select();


            foreach ($Superinfo as $v) {
                $lists[$v['uid']]['sufru1'] = $v['get1'];
                $lists[$v['uid']]['sufru2'] = $v['get2'];
            }
            unset($Superinfo);


            foreach ($Fruitinfo as $v) {
                $lists[$v['uid']]['fru1'] = $v['get1'];
                $lists[$v['uid']]['fru2'] = $v['get2'];
            }
            unset($Fruitinfo);


            foreach ($Brnninfo as $v) {
                $lists[$v['uid']]['brn1'] = $v['get1'];
                $lists[$v['uid']]['brn2'] = $v['get2'];
            }
            unset($Brnninfo);

            //获取存储的历史数据

            $l_map['user_id'] = array('in', $uids);
            $log_info2 = D('Activatelogs')->where($l_map)->field('user_id,wingold')->select();

            foreach ($log_info2 as $v) {
                $lists[$v['uid']]['logwin'] = intval($lists[$v['uid']]['sufru1']) + intval($lists[$v['uid']]['fru1']) + intval($lists[$v['uid']]['brn1']) - intval($v['wingold']);
            }
            unset($log_info2);

            $list   = $user->where($u_map)->field('uid,nickname,gold,wingold,redbag,bet_gold,pays,gold_stat')->select();


            foreach ($list as $v) {

                $lists[$v['uid']]['nickname'] = $v['nickname'];
                $lists[$v['uid']]['gold'] = $v['gold'];
                $lists[$v['uid']]['wingold'] = $v['wingold'];
                $lists[$v['uid']]['redbag'] = $v['redbag'];
                $lists[$v['uid']]['bet_gold'] = $v['bet_gold'];
                $lists[$v['uid']]['pays'] = $v['pays'];
                $lists[$v['uid']]['gold_stat'] = $v['gold_stat'];
            }
            unset($list);
            // echo  json_encode($lists);

        } elseif ($orderby == 'sufru1' || $orderby == 'sufru2') { //超级水果

            if ($orderby == 'sufru1') $orderby = "get1";
            else   $orderby = "get2";
            $orders = $orderby . ' ' . $order;

            $map['uid'] = array('egt', 0);
            $Superinfo = D('Superfruituser')->where($map)->page($p, $sqllimit)->order($orders)->field('uid,get1,get2')->select();
            $uids = array();
            $lists = array();
            foreach ($Superinfo as $v) {
                $uids[] = $v['uid'];
                $lists[$v['uid']]['sufru1'] = $v['get1'];
                $lists[$v['uid']]['sufru2'] = $v['get2'];
                $lists[$v['uid']]['uid'] = $v['uid'];
            }
            unset($Superinfo);
            $u_map['uid'] = array('in', $uids);
            $yincar = D('caruser')->where($u_map)->field('uid,yinli,get')->select();
            $Fruitinfo = D('Fruituser')->where($u_map)->field('uid,get1,get2')->select();
            $Brnninfo = D('Brnnuser')->where($u_map)->field('uid,get1,get2')->select();
            foreach ($yincar as $v) {
                $lists[$v['uid']]['caryinli'] = $v['yinli'];
                $lists[$v['uid']]['carget'] = $v['get'];
            }
            unset($yincar);
            foreach ($Fruitinfo as $v) {
                $lists[$v['uid']]['fru1'] = $v['get1'];
                $lists[$v['uid']]['fru2'] = $v['get2'];
            }
            unset($Fruitinfo);
            foreach ($Brnninfo as $v) {
                $lists[$v['uid']]['brn1'] = $v['get1'];
                $lists[$v['uid']]['brn2'] = $v['get2'];
            }
            unset($Brnninfo);
            //获取存储的历史数据

            $l_map['user_id'] = array('in', $uids);
            $log_info2 = D('Activatelogs')->where($l_map)->field('user_id,wingold')->select();

            foreach ($log_info2 as $v) {
                $lists[$v['uid']]['logwin'] = intval($lists[$v['uid']]['sufru1']) + intval($lists[$v['uid']]['fru1']) + intval($lists[$v['uid']]['brn1']) - intval($v['wingold']);
            }
            unset($log_info2);

            $list   = $user->where($u_map)->field('uid,nickname,gold,wingold,redbag,bet_gold,pays,gold_stat')->select();


            foreach ($list as $v) {

                $lists[$v['uid']]['nickname'] = $v['nickname'];
                $lists[$v['uid']]['gold'] = $v['gold'];
                $lists[$v['uid']]['wingold'] = $v['wingold'];
                $lists[$v['uid']]['redbag'] = $v['redbag'];
                $lists[$v['uid']]['bet_gold'] = $v['bet_gold'];
                $lists[$v['uid']]['pays'] = $v['pays'];
                $lists[$v['uid']]['gold_stat'] = $v['gold_stat'];
            }
            unset($list);
        } elseif ($orderby == 'fru1' || $orderby == 'fru2') { //普通水果
            if ($orderby == 'fru1') $orderby = "get1";
            else   $orderby = "get2";
            $orders = $orderby . ' ' . $order;
            $map['uid'] = array('egt', 0);
            $Fruitinfo = D('Fruituser')->where($map)->page($p, $sqllimit)->order($orders)->field('uid,get1,get2')->select();
            $uids = array();
            $lists = array();
            foreach ($Fruitinfo as $v) {
                $uids[] = $v['uid'];
                $lists[$v['uid']]['fru1'] = $v['get1'];
                $lists[$v['uid']]['fru2'] = $v['get2'];
                $lists[$v['uid']]['uid'] = $v['uid'];
            }
            unset($Fruitinfo);
            $u_map['uid'] = array('in', $uids);
            $yincar = D('caruser')->where($u_map)->field('uid,yinli,get')->select();
            $Superinfo = D('Superfruituser')->where($u_map)->field('uid,get1,get2')->select();
            $Brnninfo = D('Brnnuser')->where($u_map)->field('uid,get1,get2')->select();
            foreach ($yincar as $v) {
                $lists[$v['uid']]['caryinli'] = $v['yinli'];
                $lists[$v['uid']]['carget'] = $v['get'];
            }
            unset($yincar);
            foreach ($Superinfo as $v) {
                $lists[$v['uid']]['sufru1'] = $v['get1'];
                $lists[$v['uid']]['sufru2'] = $v['get2'];
            }
            unset($Superinfo);
            foreach ($Brnninfo as $v) {
                $lists[$v['uid']]['brn1'] = $v['get1'];
                $lists[$v['uid']]['brn2'] = $v['get2'];
            }
            unset($Brnninfo);
            //获取存储的历史数据

            $l_map['user_id'] = array('in', $uids);
            $log_info2 = D('Activatelogs')->where($l_map)->field('user_id,wingold')->select();

            foreach ($log_info2 as $v) {
                $lists[$v['uid']]['logwin'] = intval($lists[$v['uid']]['sufru1']) + intval($lists[$v['uid']]['fru1']) + intval($lists[$v['uid']]['brn1']) - intval($v['wingold']);
            }
            unset($log_info2);

            $list   = $user->where($u_map)->field('uid,nickname,gold,wingold,redbag,bet_gold,pays,gold_stat')->select();


            foreach ($list as $v) {

                $lists[$v['uid']]['nickname'] = $v['nickname'];
                $lists[$v['uid']]['gold'] = $v['gold'];
                $lists[$v['uid']]['wingold'] = $v['wingold'];
                $lists[$v['uid']]['redbag'] = $v['redbag'];
                $lists[$v['uid']]['bet_gold'] = $v['bet_gold'];
                $lists[$v['uid']]['pays'] = $v['pays'];
                $lists[$v['uid']]['gold_stat'] = $v['gold_stat'];
            }
            unset($list);
        } elseif ($orderby == 'brn1' || $orderby == 'brn2') { //百人牛牛排行
            if ($orderby == 'brn1') $orderby = "get1";
            else   $orderby = "get2";
            $orders = $orderby . ' ' . $order;
            $map['uid'] = array('egt', 0);
            $Brnninfo = D('Brnnuser')->where($map)->page($p, $sqllimit)->order($orders)->field('uid,get1,get2')->select();
            $uids = array();
            $lists = array();
            foreach ($Brnninfo as $v) {
                $uids[] = $v['uid'];
                $lists[$v['uid']]['brn1'] = $v['get1'];
                $lists[$v['uid']]['brn2'] = $v['get2'];
                $lists[$v['uid']]['uid'] = $v['uid'];
            }
            unset($Brnninfo);
            $u_map['uid'] = array('in', $uids);
            $yincar = D('caruser')->where($u_map)->field('uid,yinli,get')->select();
            $Superinfo = D('Superfruituser')->where($u_map)->field('uid,get1,get2')->select();
            $Fruitinfo = D('Fruituser')->where($u_map)->field('uid,get1,get2')->select();
            foreach ($yincar as $v) {
                $lists[$v['uid']]['caryinli'] = $v['yinli'];
                $lists[$v['uid']]['carget'] = $v['get'];
            }
            unset($yincar);
            foreach ($Superinfo as $v) {
                $lists[$v['uid']]['sufru1'] = $v['get1'];
                $lists[$v['uid']]['sufru2'] = $v['get2'];
            }
            unset($Superinfo);
            foreach ($Fruitinfo as $v) {
                $lists[$v['uid']]['fru1'] = $v['get1'];
                $lists[$v['uid']]['fru2'] = $v['get2'];
            }
            unset($Fruitinfo);
            //获取存储的历史数据

            $l_map['user_id'] = array('in', $uids);
            $log_info2 = D('Activatelogs')->where($l_map)->field('user_id,wingold')->select();

            foreach ($log_info2 as $v) {
                $lists[$v['uid']]['logwin'] = intval($lists[$v['uid']]['sufru1']) + intval($lists[$v['uid']]['fru1']) + intval($lists[$v['uid']]['brn1']) - intval($v['wingold']);
            }
            unset($log_info2);

            $list   = $user->where($u_map)->field('uid,nickname,gold,wingold,redbag,bet_gold,pays,gold_stat')->select();


            foreach ($list as $v) {

                $lists[$v['uid']]['nickname'] = $v['nickname'];
                $lists[$v['uid']]['gold'] = $v['gold'];
                $lists[$v['uid']]['wingold'] = $v['wingold'];
                $lists[$v['uid']]['redbag'] = $v['redbag'];
                $lists[$v['uid']]['bet_gold'] = $v['bet_gold'];
                $lists[$v['uid']]['pays'] = $v['pays'];
                $lists[$v['uid']]['gold_stat'] = $v['gold_stat'];
            }
            unset($list);
        }

        $this->assign('_list', $lists);
        $this->display();
    }

    public function gethbinfo()
    {
        if (!I('mch_billno') && !I('bill_type')) {
            //
            $this->assign('result_code', 'SUCCESS');
            $this->assign('_list', array());
            $this->display();
            return;
        }

        $mch_billno = I('mch_billno');
        $bill_type = I('bill_type');
        if (!$bill_type) {
            $bill_type = 'MCHT';
        }

        vendor('Gethbinfo');
        $gethbinfoObj = new \Gethbinfo();
        $info = $gethbinfoObj->gethbinfo($mch_billno,  $bill_type);

        echo json_encode($info);

        $array[] = $info;
        $this->assign('result_code', $info['result_code']);
        $this->assign('_list', $array);
        $this->display();
    }


    // 水果流水曲线图
    public function fruitlogdiagram()
    {
        $b_time = strtotime(date('Y-m-d') . '-1 month');
        for ($i = $b_time; $i <= NOW_TIME; $i += 86400) {
            $date = date('y/m/d', $i);
            $data[$date] = $date;
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function fruitlogdiagram_data()
    {
        $test_type = I('test_type') ? I('test_type') : 1;
        $b_time = strtotime(date('Y-m-d') . '-1 month');
        $Fruitlogdiagram = D('Fruitlogdiagram');
        $data = array();
        $data['_500']       = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 500, $test_type);
        $data['_1000']      = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 1000, $test_type);
        $data['_2500']      = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 2500, $test_type);
        $data['_5000']      = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 5000, $test_type);
        $data['_10000']     = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 10000, $test_type);
        $data['_25000']     = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 25000, $test_type);
        $data['_50000']     = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 50000, $test_type);
        $data['_100000']    = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 100000, $test_type);
        $data['_250000']    = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 250000, $test_type);
        $data['_500000']    = $this->_fruitlogdiagram($b_time, $Fruitlogdiagram, 500000, $test_type);
        return $this->ajaxReturn($data);
    }

    private function _fruitlogdiagram($b_time, $obj, $number, $test_type)
    {
        $b_time = strtotime(date('Y-m-d 00:00:00', $b_time));
        $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME)) - 86400;

        $map['time'] = array('between', array($b_time, $e_time));
        $map['test_type'] = intval($test_type);
        $logs = $obj->where($map)->field($number . ',time')->order('time asc')->select();

        $res = $return = array();
        for ($i = $b_time; $i < $e_time + 1; $i = $i + 86400) {
            $dt = date('Y-m-d', $i);
            $res[$dt] = 0;
        }

        foreach ($logs as $v) {
            $dt = date('Y-m-d', $v['time']);
            $res[$dt] = $v[$number];
        }

        foreach ($res as $v) {
            $return[] = $v;
        }

        return $return;
    }

    //设为刷子用户标识
    public function setbrush()
    {
        $id = I('id');
        $type = I('type');
        if($id) {
          
            //设为刷子用户标识
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate.set_brush_state';
            $post_data['sign'] = md5(C('SENDGAME_KEY').time());
            $post_data['timestamp'] = intval(time());
            $post_data['roleid'] = intval($id); //账号ID
            $post_data['is_brush'] = $type?intval($type):1;
            $field = json_encode($post_data);
            $ret = PostUrl($post_url, $field,1);
            if ($ret == 'ok')
            {
                //D('DUsers')->where(['id' => $id])->save(['is_brush' => 1]);
                $this->success('操作成功');
            }
            else
            {
                $this->success('操作失败'.$ret);  
            }

        }
        
    }

    /**
     * 红包扣取记录
     */
    public function redlog($p = 1)
    {
        $limit = 20;

        $query = '';

        $b_time = strtotime(I('b_time'));
        $e_time = strtotime(I('e_time'));

        // if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
        //     $map['c_time'] = array('between', array($b_time, $e_time));
        //     $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        // } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
        //     $map['c_time'] = array('elt', $e_time);
        //     $query .= 'e_time/' . I('e_time') . '/';
        // } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
        //     $map['c_time'] = array('egt', $b_time);
        //     $query .= 'b_time/' . I('b_time') . '/';
        // }

        $uid = I('uid');
        if($uid) {
            $map['uid'] = $uid;
        }
        $model = D('buckleRedLog');
        // var_dump($model);exit;

        $totals = $model->where($map)->count();
        $list = $model->where($map)->order('time desc')->page($p, $limit)->select();
        // var_dump($list);exit;
        // var_dump($model->where(['uid' => ['GT', 0]])->delete());

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_list', $list);
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 异常登入
     *    GErr_Login_SdkUidNil        = 10001 //微信openid为空
     *    GErr_Login_AccountNoExist   = 10002 //账号不存在
     *    GErr_Login_SdkUidNoPeek     = 10003 //微信openid不匹配
     *    GErr_Login_TokenNoPeek      = 10004 //token不匹配
     *    GErr_Login_AccountNoCreate  = 10005 //账号未创建
     *    GErr_Login_AccountForbidden = 10006 //封号
     *    GErr_Login_BlackUser        = 10007 //黑名单
     *    GErr_Login_IpConstraint     = 10008 //ip限制
     *    GErr_Login_FromNoPeek = 10101 //渠道号不匹配
     */
    public function usersloginex()
    {
        $usersModel = D('DUserLoginErrors');
        $room_list = array(
            10001 => '微信openid为空',
            10002 => '账号不存在',
            10003 => '微信openid不匹配',
            10004 => 'token不匹配',
            10005 => '账号未创建',
            10006 => '封号',
            10007 => '黑名单',
            10008 => 'ip限制',
            10009 => '易盾失败(一般为高风险)',
            10101 => '渠道号不匹配'
        );

        //白名单列表
        $p = I('p');
        $limit = 20;
        $id = I('id');
        if(!empty($id)) {
            $rmap['account'] = $id;
            $rmap['dev_id'] = $id;
            $rmap['_logic'] = 'OR';
            $map['_complex'] = $rmap;
        }
        
        $status = intval(I('status'));

        if (isset($_GET['status'])) {
            if(0!=$status){$map['login_code'] = $status;}
        }
 
        $totals = $usersModel->where($map)->count();
        $list = $usersModel->where($map)->field('d_user_login_errors.*,"" as login_name ')->page($p, $limit)->order('id desc')->select();
        $listnew = array();
        foreach ($list as $v) {
            $v['login_name'] = $room_list[$v['login_code']];
            $listnew[]=$v;
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_page', $pageNav->show());
        $this->assign('status', $status);
        $this->assign('room', $room_list);
        $this->assign('_list', $listnew);
        $this->display();

    }
    /**
     * 晶石 获得/消耗日志
     */
    public function ticketduihuanglog($p = 1)
    {
        $limit = 20;

        $query = '';

        $gold = intval(I('gold'));
        $query .= 'gold/' . $gold . '/';

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0: //用户ID
                    $map['uid'] = D('DUsers')->get_charid('uid', intval($value));
                    break;
                    break;
                case 1: //角色ID
                    $map['uid'] = intval($value);
                    break;
                case 2: //手机号
                    $map['uid'] = D('DUsers')->get_charid('mobile', trim($value));
                    break;
                case 3: //玩家昵称
                    $map['uid'] = D('DUsers')->get_charid('nickname', trim($value));
                    break;
            }

            $query .= 'type/' . $type . '/value/' . $value . '/';
        }

        $b_time = I('b_time');
        $e_time = I('e_time');
        if(empty(I('b_time')) && empty(I('e_time')))
		{
			$b_time = date("Y-m-d")." 00:00:00";
			$e_time = date("Y-m-d H:i:s");
            $map['created_at'] = array('between', array($b_time, $e_time));
		}
        //强制当月时间
        $b_time = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $e_time = date("Y-m-d H:i:s");
        $map['created_at'] = array('between', array($b_time, $e_time));

        if (isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('between', array($b_time, $e_time));
            $query .= 'b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
        } elseif (!isset($_GET['b_time']) && isset($_GET['e_time'])) {
            $map['created_at'] = array('elt', $e_time);
            $query .= 'e_time/' . I('e_time') . '/';
        } elseif (isset($_GET['b_time']) && !isset($_GET['e_time'])) {
            $map['created_at'] = array('egt', $b_time);
            $query .= 'b_time/' . I('b_time') . '/';
        }
		

        $model = D('DItemLogs');
		
        if ($gold == 1) {
           $map['num'] = array('gt', 0);
        } elseif ($gold == 2) {
           $map['num'] = array('lt', 0);
        }

        // 只显示兑换的
        $area = 7;
        $map['item_id'] = 103;



        //顶部数据
        if (I('async')) {
            $total_ticket = 0;
            $lists = $model->where($map)->select();

            foreach ($lists as $v) {
                $total_ticket += $v['ticket'];
            }

            $data['totalgold'] = number_format($total_ticket);
            return $this->ajaxReturn($data);
        }

        $totals = $model->where($map)->count();
        $list = $model->where($map)->order('created_at desc')->page($p, $limit)->select();
        foreach ($list as $k=> $v) {
            $list[$k]['account'] = D('DUsers')->get_uid('char_id', intval($v['uid']));
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');


        $this->assign('_list', $list);
       // $this->assign('nick', $nick);
        $this->assign('room', $area);
        $this->assign('area', $area);
        $this->assign('rooms', D('DItemLogs')->getsource());
        $this->assign('query', $query);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }
    
    /**
     *天梯排行榜
     */
    public function tiantirank($p=1)
    {
        $ranks = D("DRanks");
        $allvalue=0;
        $limit = 20;
        $p = I('p');
        $p = $p ? $p : 1;

        $info = [
            ['kk' => '1档','vv' => 1,'fv' => 1,'cv' => 0,'av' => 0],
            ['kk' => '2档','vv' => 3,'fv' => 2,'cv' => 0,'av' => 0],
            ['kk' => '3档','vv' => 5,'fv' => 3,'cv' => 0,'av' => 0],
            ['kk' => '4档','vv' => 7,'fv' => 4,'cv' => 0,'av' => 0],
            ['kk' => '5档','vv' => 9,'fv' => 5,'cv' => 0,'av' => 0]
        ]; 
        $newinfo = array();
        foreach ($info as $v) {
            $map['r_type'] = 300;
            $map['periods'] = 0;
            $map['getLimitRankLev(flag)'] = $v['fv'];

            $getranks = $ranks->where($map)->field('COUNT(id) as cv')->find();
            if($getranks)
            {
                $v['cv']=$getranks['cv'];
                $v['av']=$v['cv']*$v['vv'];
                $allvalue=$allvalue+$v['av'];
            }
            $newinfo[]=$v;
        }

        $lmap['r_type'] = 300;
        $lmap['periods'] = 0;
        $totals = $ranks->where($lmap)->join('game1.d_users ON d_users.id = d_ranks.uid')->count();
        $list   = $ranks->where($lmap)->join('game1.d_users ON d_users.id = d_ranks.uid')
                        ->field('d_users.id, d_users.name, d_ranks.value1, getLimitRankLev(d_ranks.flag) as flag, d_ranks.updated_at,0 as jv')
                        ->order('value1 desc')
                        ->page($p, $limit)->select();
        //奖励值数据补充
        $newList = array();
        foreach ($list as $v) 
        {
            foreach ($info as $f) 
            {
                if($f['fv']==$v['flag'])
                {
                    $v['jv']=$f['vv'];

                }
            }
            $newList[]=$v;  
        }


        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $this->assign('info', $newinfo);
        $this->assign('allvalue', $allvalue);
        $this->assign('_list', $newList);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }
    //重载
    public function zhifuConfig()
    {
        $act = I('act');
        $model  = D('DSysData');
        $zhifus = array(
            101 => '微信',
			102 => '支付宝'
			);
        $caozuos = array(
            '1' => '开启',
            '0' => '关闭'
            
            );
        $this->assign('_zhifus', $zhifus);
        $this->assign('_caozuos', $caozuos);

        if($act == 'submit') {
            //提交生成
            $data = I();
            $post_url = C('GM_URL') . 'cgi-bin/gm_oprate.sysevent'; 
             $data['id'] = intval($data['m_type']);
             $data['data1'] = intval($data['m_value']);
             $data['timestamp'] = intval(time());
             $data['sign'] = md5(C('SENDGAME_KEY').time()); 
             $field = json_encode($data);

             $rets = PostUrl($post_url, $field,1);
             if($rets=="ok")
             {
                $this->success('添加成功');exit;
             }
             else{$this->error('添加失败');exit;}
        }
        
        
        $list = $model->order('id desc')->limit(50)->field("*,'' as m_type_name,'' as m_value_name ")->select();
        $listnew = array();
        foreach ($list as $v) 
        {
            if('101'==$v['m_type'])
            {$v['m_type_name']="微信";}
            else{$v['m_type_name']="支付宝";}

            if('0'==$v['m_value'])
            {$v['m_value_name']="关闭";}
            else{$v['m_value_name']="开启";}
            $listnew[]=$v;
        }
        $this->assign('_list', $listnew);
        $this->display();
    }

    //那就拉黑解黑  易盾解除
    public function setwbusers()
    {
        $devid = I('devid');
        $type = I('type');
        $id = I('id');

        if($devid && $type && $id) 
        {
            $issuccess=1;
            if(1==$type)
            {
                //解黑
                $umap['dev_id'] = $devid; 
                $user = D('DWbUserList')->where($umap)->find();
                if(!empty($user))
                {
                    if(1==$user['status']){$issuccess=1;}
                    else
                    {
                        $dataup['status'] = 1;
                        $res = D('DWbUserList')->where($umap)->save($dataup);
                        if($res){$issuccess=1;}else{$issuccess=2;}
                        
                    }
                }

                /*
                else
                {
                    $data['dev_id']=$devid;
                    $data['status']=1;
                    $data['created_at'] = date("Y-m-d H:i:s");
                    $res = D('DWbUserList')->add($data);
                    if($res){$issuccess=1;}else{$issuccess=2;}

                }
                */
            }
            else if(2==$type)
            {
                //拉黑
                $umap['dev_id'] = $devid; 
                $user = D('DWbUserList')->where($umap)->find();
                if(!empty($user))
                {
                    if(2==$user['status']){$issuccess=1;}
                    else
                    {
                        $dataup['status'] = 2;
                        $res = D('DWbUserList')->where($umap)->save($dataup);
                        if($res){$issuccess=1;}else{$issuccess=2;}
                    }
                }
                /*else
                {
                    $data['dev_id']=$devid;
                    $data['status']=2;
                    $data['created_at'] = date("Y-m-d H:i:s");
                    $res = D('DWbUserList')->add($data);
                    if($res){$issuccess=1;}else{$issuccess=2;}

                }
                */
            }

            if(1!=$issuccess)
            {
                $this->error('操作失败');
            }
            else
            {
                if(3==$type)
                {
                    //易盾解除
                    $post_url = C('GMBox_URL') . 'sys/reload_account'; 
                    $data['yidun_lv'] = 1;
                    $data['account'] = intval($id);
                    $data['timestamp'] = intval(time());
                    $data['sign'] = md5(C('SENDGAME_KEY').time()); 
                    $field = json_encode($data);

                    $rets = PostUrl($post_url, $field,1);
                    if($rets=="ok")
                    {
                        $this->success('操作成功');
                    }
                    else{$this->success('服务端提交失败'.$rets);}

                }
                else
                {
                    //提醒服务端重载
                    $post_url = C('GMBox_URL') . 'sys/reload_account'; 
                    $data['wb_status'] = intval($type);
                    $data['account'] = intval($id);
                    $data['timestamp'] = intval(time());
                    $data['sign'] = md5(C('SENDGAME_KEY').time()); 
                    $field = json_encode($data);

                    $rets = PostUrl($post_url, $field,1);
                    if($rets=="ok")
                    {
                        $this->success('操作成功');
                    }
                    else{$this->success('服务端提交失败'.$rets);}

                }
                
            }
            
        }
        
    }
}
