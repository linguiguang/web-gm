<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/28
 * Time: 13:39
 */
namespace Admin\Controller;
use Admin\Controller\AdminController;
/**
 * 用户模块
 */
class MemberController extends AdminController{

    /**
     * 用户管理
     */
    public function users($p=1){
        $limit = 20;
        $user = D('Users');//或M('Users', 'stat_', 'DB_CONFIG2');

        if(isset($_GET['b_time'])){
            $b_time = strtotime(I('b_time'));
            $map['adddate'][] = array('egt', $b_time);
        }

        if(isset($_GET['e_time'])){
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['adddate'][] = array('elt', $e_time);
        }

        $bind = intval(I('bind'));
        if($bind == 1){
            $map['siteuser'] = 1;
        }elseif($bind == 2){
            $map['siteuser'] = 0;
        }

        if(!isset($_GET['from_id'])){
            $from_id = -1;
        }else{
            $from_id = intval(I('from_id'));
            $map['from_id'] = $from_id;
        }

        if(!isset($_GET['state'])){
            $state = 1;
        }else{
            $state = intval(I('state'));
        }
        $map['state'] = $state;


        $type = intval(I('type'));
        $value = I('value');

        if(isset($_GET['type']) && isset($_GET['value'])){
            switch ($type){
                case 0 : {//用户ID
                    $map['id'] = intval($value);
                    break;
                }

                case 1 : {//角色ID
                    $userinfo = D('Userinfo')->where(array('char_id'=>intval($value)))->field('uid')->find();
                    $map['id'] = $userinfo['uid'];
                    break;
                }

                case 2 : {//手机号
                    $map['username'] = trim($value);
                    break;
                }

                case 3 : {//玩家昵称
                    $userinfo = D('Userinfo')->where(array('username'=>trim($value)))->field('uid')->find();
                    $map['id'] = $userinfo['uid'];
                    break;
                }
            }
        }

        //注册地址和手机码
        $ip = trim(I('ip'));
        $lastloginip = trim(I('lastloginip'));
        $devid = trim(I('devid'));

        if($ip != ''){
            $map['ip'] = $ip;
        }

        if($lastloginip != ''){
            $map['lastloginip'] = $lastloginip;
        }

        if($devid != ''){
            $map['devid'] = $devid;
        }

        $totals = $user->where($map)->count();
        $list   = $user->where($map)->page($p, $limit)->order('id desc')->select();

        $ids = array();
        foreach ($list as $v){
            $ids[] = intval($v['id']);
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('from', D('DChannels')->select());
        $this->assign('_list', $list);
        $this->assign('state', $state);
        $this->assign('user', D('Userinfo')->getUserInfo($ids));
        $this->assign('from_id', $from_id);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 用户详情
     */
    public function info($id=0){
        $id = intval($id);
        if($id == 0) $this->error('用户不存在');

        if(IS_POST){

            $pwd = trim(I('pwd'));
            if($pwd != ''){
                $data['password'] = md5(md5($pwd));
            }

            $state = intval(I('state'));
            $reason = trim(I('reason'));
            if($state == 0 && $reason == ''){
                $this->error('禁用用户，原因不能为空');
            }elseif($state == 1){
                $data['forbid_reason'] == '';
            }else{
                $data['forbid_reason'] = $reason;
            }
            $data['state'] = intval($state);

            if(D('Users')->where(array('id'=>$id))->save($data)){
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }

        }else{
            $user     = D('Users')->where(array('id'=>$id))->find();
            $userinfo = D('Userinfo')->where(array('uid'=>$id))->find();
            
            //在线时长,登录次数
            $map['uid'] = $id;
            $map['type'] = 1;
            $time_logs = D('Arealogin')->where($map)->select();

            $times = 0;//在线时长
            foreach ($time_logs as $v){
                $times += $v['times'];
            }

            //游戏信息，输赢统计
            $data = $this->_gameinfostat($id);

            //充值信息
            $char_id = $userinfo['char_id'];
            $ret['uid'] = $char_id;
            $ret['state'] = 3;

            $recharge = D('Paylog')->where($ret)->order('id desc')->select();
            $rech_nums = count($recharge);//充值单数
            $money = 0;//充值金额
            foreach ($recharge as $v){
                $money += $v['money'];
            }

            $config = D('ConfigByshop')->getConfig();//商城数据表
            //兑换记录
            $shops = D('Shops')->where(array('uid'=>$id))->select();
            foreach ($shops as $k=>$v){
                $shops[$k]['cost_type'] = $config[$v['goods_id']]['cost_type'];//商品类型
                if($config[$v['goods_id']]['cost_type'] == 3){//红包
                    $shops[$k]['msg'] =  D('RedpacketLogs')->where(array('shop_id'=>$v['id']))->getField('msg');
                }
            }

            //鱼市记录
            $ids = array();
            $log['suid'] = $id;
            $log['buid'] = $id;
            $log['_logic'] = 'OR';

            $fishlog = D('Fishmarket')->where($log)->select();
            foreach ($fishlog as $v){
                $ids[] = $v['bchar_id'];
                $ids[] = $v['schar_id'];
            }

            $back_url = $_SERVER['HTTP_REFERER'];
            $this->assign('fishlog', $fishlog);
            $this->assign('fishes', D('Configfishes')->getConfig());
            $this->assign('chars', D('Userinfo')->getUserInfo($ids, 'char_id'));
            $this->assign('recharge', $recharge);
            $this->assign('config', $config);
            $this->assign('shops', $shops);
            $this->assign('goods', $this->goods());
            $this->assign('money', $money);
            $this->assign('rech_nums', $rech_nums);
            $this->assign('data', $data['area_winlose']);  // 各个游戏区域输赢统计
            $this->assign('winlose', $data['winlose']);
            $this->assign('times', $times);
            $this->assign('user', $user);
            $this->assign('userinfo', $userinfo);
            $this->assign('back_url', $back_url);
            $this->display();
        }
    }

    /**
     * 游戏信息统计
     */
    protected function _gameinfostat($uid){
        //游戏时长
        $area_arr = array(201, 1, 2, 3, 4, 5);//5个游戏区域
        $ret['type'] = 1;
        $ret['area'] = array('in', $area_arr);
        $ret['uid'] = $uid;
        $gamelogin = D('Arealogin')->field('area,times,logout_time')->where($ret)->select();

        $ares = array();
        foreach ($gamelogin as $v){
            $ares[$v['area']]['times'] += $v['times'];
        }

        $area_arrs = array(36, 101, 102, 103, 104, 105);
        $map['area'] = array('in', $area_arrs);
        $map['uid'] = $uid;
        $gamelogs = D('Usergoldstat')->where($map)->select();

        $area_winlose = array();
        $winlose = 0;
        foreach ($gamelogs as $v){
            if($v['area'] == 36){
                $v['area'] = 301;//拉霸区域
            }

            $area_winlose[$v['area']]['times'] = $ares[$v['area']-100]['times'];
            $area_winlose[$v['area']]['win'] += $v['win'];
            $area_winlose[$v['area']]['stat'] += $v['stat'];
            $winlose += $v['stat'];
        }

        $data = array(
            'winlose' => $winlose,
            'area_winlose' => $area_winlose
        );

        return $data;
    }

    /**
     * 进出记录
     */
    public function inout($p=1){
        $seldt = intval(I('seldt'));
        switch($seldt){
            case 1: {
                $st_time = date('Y-m-d');
                $end_time = date('Y-m-d');
                break;
            }
            case 2: {
                $st_time = date("Y-m-d",strtotime("-1 day"));
                $end_time = date("Y-m-d",strtotime("-1 day"));
                break;
            }
            case 3: {
                $week = lastNWeek(NOW_TIME, 0);
                $st_time = $week[0];
                $end_time = $week[1];
                break;
            }
            case 4:{
                $week = lastNWeek(NOW_TIME, 1);
                $st_time = $week[0];
                $end_time = $week[1];
                break;
            }
            case 5: {
                $st_time = date("Y-m-d",mktime(0, 0 , 0,date("m"),1,date("Y")));
                $end_time = date("Y-m-d",mktime(23,59,59,date("m"),date("t"),date("Y")));
                break;
            }
            case 6: {
                $month = lastMonth(NOW_TIME);
                $st_time = $month[0];
                $end_time = $month[1];
                break;
            }
            default:
                $st_time = I('b_time');
                $end_time = I('e_time');
                break;
        }

        $sdate = intval(I('sdate'));
        if($sdate){
            $field = 'logout_time';
        }else{
            $field = 'login_time';
        }

        if(trim($st_time) != '' && trim($end_time) != ''){
            $map[$field] = array('between', array(strtotime($st_time.' 00:00:00'), strtotime($end_time.' 23:59:59')));
        }else if(trim($st_time) != ''){
            $map[$field] = array('egt', strtotime($st_time.' 00:00:00'));
        }else if(trim($end_time) != ''){
            $map[$field] = array('elt', strtotime($end_time.' 23:59:59'));
        }

        if(!isset($_GET['area'])){
            $area = -1;
        }else{
            $area = intval(I('area'));
            $map['area'] = $area;
        }

        $uid = intval(I('uid'));
        if($uid) $map['uid'] = $uid;

        $map['type'] = 1;//登录数据

        $limit = 20;
        $arealogin = D('Arealogin');
        $totals = $arealogin->where($map)->count();
        $list   = $arealogin->where($map)->page($p, $limit)->order('login_time desc')->select();

        foreach ($list as $k=>$v){
            $ret['login_time'] = $v['login_time'];
            $ret['uid'] = intval($v['uid']);
            $ret['type'] = 0;//登出数据
            $out = $arealogin->where($ret)->find();
            if(!empty($out)){
                $list[$k]['cgold'] = $out['gold'];
                $list[$k]['cgem'] = $out['gem'];
                $list[$k]['cwing'] = $out['wing'];
                $list[$k]['cbill'] = $out['bill'];
            }
        }

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->assign('area', $area);
        $this->assign('sdate', $sdate);
        $this->assign('seldt', $seldt);
        $this->assign('b_time', $st_time);
        $this->assign('e_time', $end_time);
        $this->display();
    }

    /**
     * 充值记录
     */
    public function paylog($p=1){
        $limit = 20;
        $pay = D('Paylog');

        if(isset($_GET['char_id'])){
            $map['uid'] = intval(I('char_id'));
        }

        if(!isset($_GET['state'])){
            $state = 3;
        }else{
            $state = intval(I('state'));
        }
        $map['state'] = $state;

        if(!isset($_GET['devi'])){
            $devi = -1;
        }else{
            $devi = intval(I('devi'));
            $map['device'] = $devi;
        }

        if(!isset($_GET['goods'])){
            $goods_id = -1;
        }else{
            $goods_id = intval(I('goods'));
            $map['saletype'] = $goods_id;
        }

        if(!isset($_GET['paytype'])){
            $paytype = -1;
        }else{
            $paytype = intval(I('paytype'));
            $map['paytype'] = $paytype;
        }

        $ordertype = intval(I('ordertype'));
        $ordersn  = trim(I('ordersn'));
        if($ordersn && $ordertype){
            if($ordertype == 1){
                $map['ordersn'] = $ordersn;
            }elseif($ordertype == 2){
                $map['returnsn'] = $ordersn;
            }
        }

        if(isset($_GET['b_time'])){
            $b_time = strtotime(I('b_time'));
            $map['addtime'][] = array('egt', $b_time);
        }

        if(isset($_GET['e_time'])){
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['addtime'][] = array('elt', $e_time);
        }

        $totals = $pay->where($map)->count();
        $list   = $pay->where($map)->page($p, $limit)->order('id desc')->select();
        $charids = array();
        foreach ($list as $v){
            $charids[] = intval($v['uid']);
        }

        $users = D('Userinfo')->getusers($charids);
        $userinfo = D('Userinfo')->getUserInfo($charids, 'char_id');
        $list_count = $pay->where($map)->count(); //充值单数
        $pay_count  = $pay->where($map)->sum('money');//充值金额
        
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        //充值物品
        $goods = $this->goods();

        $this->assign('goods_id', $goods_id);
        $this->assign('userinfo', $userinfo);
        $this->assign('list_count', $list_count);
        $this->assign('pay_count' ,$pay_count);
        $this->assign('goods', $goods);
        $this->assign('_list', $list);
        $this->assign('device', C('DEVICE'));
        $this->assign('state', $state);
        $this->assign('users', $users);
        $this->assign('devi', $devi);
        $this->assign('paytype', $paytype);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 解绑微信
     */
    public function outbindwx($uid){
        $uid = intval($uid);

        $user = D('Users')->find($uid);
        if($uid == 0 || $user == ''){
            $this->error('此用户不存在');
        }

        $openid = $user['wxopenid'];
        if(trim($openid) == '') $this->error('此用户未绑定微信');

        $data['wxopenid'] = '';
        $data['wxnick'] = '';

        if(D('Users')->where(array('id'=>$uid))->save($data)){
            $this->success('解绑绑成功');
        }else{
            $this->error('解绑失败');
        }

    }

    /**
     * 充值统计
     */
    public function pay(){
        $pay = D('Paylog');

        $data = $this->_calculatetime();//计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];

        $list = array();
        for($i=$b_time;$i<=$e_time;$i+=86400){
            $date = date('Y-m-d', $i);
            $list[$date] = $this->_paycount($i, $pay);
        }

        $this->assign('_list', $list);
        $this->display();
    }

    private function _paycount($time, $obj){
        $b_time = $time;
        $e_time = $time + 86399;

        $pay = $obj;
        $map['addtime'] = array('between', array($b_time, $e_time));
        $map['state'] = 3;

        $paylog = $pay->where($map)->select();

        $money = $recharge_num = $users = $uique_users = 0;
        $userarr = array();
        foreach ($paylog as $k=>$v){
            $money += $v['money'];
            $recharge_num += 1;
            $userarr[] = $v['uid'];
        }

        $data['money'] = $money;
        $data['recharge_num'] = $recharge_num;
        $data['users'] = count($userarr);
        $data['uique_users'] = count(array_unique($userarr));

        return $data;
    }

    /**
     * 计算开始结束时间
     */
    protected function _calculatetime(){

        if(isset($_GET['type']) && isset($_GET['month'])){
            $type = intval(I('type'));
            if($type == 0){//按月
                $month = intval(I('month'));
                $monarr = monthday(NOW_TIME,$month);

                $b_time = strtotime($monarr[0]);
                $now_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));//现在时间
                $e_time = strtotime($monarr[1]);
                if($now_time < $e_time){
                    $e_time = $now_time;
                }

            }elseif($type == 1){//按天数
                $b_time = strtotime(I('b_time'));
                $e_time = strtotime(I('e_time'));

                if($b_time == '' || $e_time == ''){
                    $this->error('请选择时间');
                }
            }

        }else{//默认当月数据
            $b_time = strtotime(date("Y-m-d",mktime(0, 0, 0, date("m"),1,date("Y"))));
            $e_time = NOW_TIME;
        }

        $data['b_time'] = $b_time;
        $data['e_time'] = $e_time;

        return $data;
    }

    /**
     * 充值物品
     */
    protected function goods(){
        $goods = array(
            // '1'=>'60钥匙',
            // '2'=>'300钥匙',
            // '3'=>'980钥匙',
            // '4'=>'1980钥匙',
            // '5'=>'3280钥匙',
            // '6'=>'6480钥匙',
            '7'=>'6000金币',
            '8'=>'30000金币',
            '9'=>'98000金币',
            '10'=>'198000金币',
            '11'=>'328000金币',
            '12'=>'648000金币',
            '101'=>'6元礼包',
            '201'=>'月卡礼包',
            '301'=>'特惠礼包',
			'60002'=>'黄金周卡',
			'60001'=>'至尊月卡',
        );
        return $goods;
    }

}