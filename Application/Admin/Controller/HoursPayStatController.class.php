<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/16
 * Time: 17:35
 */
namespace Admin\Controller;
use Think\Controller;

/**
 * 每小时充值统计
 */
class HoursPayStatController extends Controller{


    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $map['created_at'] = array('between', array($b_time, $e_time));
        $map['status'] = 1;

        $list = D('DPays')->where($map)->field('created_at,price')->select();

        $ret = array();

        $ret['time'] = $b_time;

        foreach ($list as $v){
            $hour =  date('H', $v['addtime']);
            $ret['h_'.$hour] += intval($v['money']);
        }

        if($info = D('Hourspaystat')->where(array('time'=>$b_time))->find()){
            D('Hourspaystat')->where(array('_id'=>$info['_id']))->save($ret);
        }else{
            D('Hourspaystat')->add($ret);
        }

        $etime = microtime(true);
        $totals = round($etime - $stime);
        echo 'RUN '.$totals.'s .';
    }


}