<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 后台配置控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class ConfigController extends AdminController {

    /**
     * 配置管理
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function index(){
        /* 查询条件初始化 */
        $map = array();
        $map  = array('status' => 1);
        if(isset($_GET['group'])){
            $map['group']   =   I('group',0);
        }
        if(isset($_GET['name'])){
            $map['name']    =   array('like', '%'.(string)I('name').'%');
        }

        $list = $this->lists('Config', $map,'sort,id');
        // 记录当前列表页的cookie
        Cookie('__forward__',$_SERVER['REQUEST_URI']);
        $this->assign('group',C('CONFIG_GROUP_LIST'));
        $this->assign('group_id',I('get.group',0));
        $this->assign('list', $list);
        $this->meta_title = '配置管理';
        $this->display();
    }

    /**
     * 新增配置
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function add(){
        if(IS_POST){
            $Config = D('Config');
            $data = $Config->create();
            if($data){
                if($Config->add()){
                    S('DB_CONFIG_DATA',null);
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($Config->getError());
            }
        } else {
            $this->meta_title = '新增配置';
            $this->assign('info',null);
            $this->display('edit');
        }
    }

    /**
     * 编辑配置
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function edit($id = 0){
        if(IS_POST){
            $Config = D('Config');
            $data = $Config->create();
            if($data){
                if($Config->save()){
                    S('DB_CONFIG_DATA',null);
                    //记录行为
                    action_log('update_config','config',$data['id'],UID);
                    $this->success('更新成功', Cookie('__forward__'));
                } else {
                    $this->error('更新失败');
                }
            } else {
                $this->error($Config->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('Config')->field(true)->find($id);

            if(false === $info){
                $this->error('获取配置信息错误');
            }
            $this->assign('info', $info);
            $this->meta_title = '编辑配置';
            $this->display();
        }
    }

    /**
     * 批量保存配置
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function save($config){
        if($config && is_array($config)){
            $Config = M('Config');
            foreach ($config as $name => $value) {
                $map = array('name' => $name);
                $Config->where($map)->setField('value', $value);
            }
        }
        S('DB_CONFIG_DATA',null);
        $this->success('保存成功！');
    }

    /**
     * 删除配置
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function del(){
        $id = array_unique((array)I('id',0));

        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }

        $map = array('id' => array('in', $id) );
        if(M('Config')->where($map)->delete()){
            S('DB_CONFIG_DATA',null);
            //记录行为
            action_log('update_config','config',$id,UID);
            $this->success('删除成功');
        } else {
            $this->error('删除失败！');
        }
    }

    // 获取某个标签的配置参数
    public function group() {
        $id     =   I('get.id',1);
        $type   =   C('CONFIG_GROUP_LIST');
        $list   =   M("Config")->where(array('status'=>1,'group'=>$id))->field('id,name,title,extra,value,remark,type')->order('sort')->select();
        if($list) {
            $this->assign('list',$list);
        }
        $this->assign('id',$id);
        $this->meta_title = $type[$id].'设置';
        $this->display();
    }

    /**
     * 配置排序
     * @author huajie <banhuajie@163.com>
     */
    public function sort(){
        if(IS_GET){
            $ids = I('get.ids');

            //获取排序的数据
            $map = array('status'=>array('gt',-1));
            if(!empty($ids)){
                $map['id'] = array('in',$ids);
            }elseif(I('group')){
                $map['group']	=	I('group');
            }
            $list = M('Config')->where($map)->field('id,title')->order('sort asc,id asc')->select();

            $this->assign('list', $list);
            $this->meta_title = '配置排序';
            $this->display();
        }elseif (IS_POST){
            $ids = I('post.ids');
            $ids = explode(',', $ids);
            foreach ($ids as $key=>$value){
                $res = M('Config')->where(array('id'=>$value))->setField('sort', $key+1);
            }
            if($res !== false){
                $this->success('排序成功！',Cookie('__forward__'));
            }else{
                $this->error('排序失败！');
            }
        }else{
            $this->error('非法请求！');
        }
    }
    
    //热更新页面
    public function hotUpdate()
    {
        
        
        $file_list = D('ConfigHotUpdate')->getHotconfigPath();
        // var_dump($file_list);exit;
        $this->assign('_list', $file_list);
        $this->display();
    }
    
    //第一步 生成文件
    //读取目录下的所有erl文件生成为一个文件
    public function generateConfig()
    {
         //转化Execl表格为erl文件
        // $res = exec("sudo python3 test_config.py 2>&1" , $array, $error);
        $res = D('ConfigHotUpdate')->generateConfig();
        if($res != 0){
            $this->error('生成文件失败');
        }else {
            $this->success('生成文件成功');
        }

        
    }
    
    //组合文件
    public function buildConfig()
    {
        // echo phpinfo();exit;
        $fileConfig = D('ConfigHotUpdate')->buildConfig();
        if($fileConfig) {
            $this->success('生成文件成功');
        } else {
            $this->error('生成文件失败');
        }
    }
    
    
    //下载目标文件
    //上传文件到服务器
    public function exportConfig()
    {
        //http://47.118.43.119:8099/Public/config.erl
        // $file = 'http://'.$_SERVER['HTTP_HOST'].'/Public/game.config';
        // redirect($file);
        
        header("Content-type: text/html; charset=utf-8");
        include_once('./Application/Admin/Lib/Sftp.class.php');
        // include_once('./Application/Admin/Lib/UploadFtp.class.php');test
        
       
        
        $local_file = '/www/wwwroot/RuiquManage/config/game.config';
        $remote_file = C('REMOTE_FILE');
//        $remote_file = '/root/GameServerDev/script/script_server/game.config'; ///www/wwwroot/www/
        
        $sftp_config = C('SFTP_CONF');
        
        $connection = ssh2_connect( $sftp_config['host'] , 22 );  //host
        ssh2_auth_password($connection , $sftp_config['name'] , $sftp_config['pwd'] );  //账号密码
        $res = ssh2_scp_send($connection, $local_file, $remote_file, 0644);
        if($res) {
            $this->success('成功');
        } else {
            $this->error('失败');
        }
        
    }
    
    //通知服务端更新
    public function postConfig()
    {
        $key = I('key');
        // var_dump($key);exit;
        //发送给游戏服务端
        $post_url = C('GM_URL') .'cgi-bin/gm_oprate:hot_update';
        // var_dump($post_url);exit;
        $sendPost['key'] = implode(';', $key);
        $field = http_build_query($sendPost);
        $ret = PostUrl($post_url, $field);
        
        $this->success('成功');
        
    }


    //兑换生成列表
    public function exchange()
    {
        $model = M('ExchangeDf');
        $act = I('act');
        if($act == 'generate') {
            //生成链接

            //查询兑换记录
            $exchangeList = D('RedpacketLogs')->where(['paytype' => 5, 'status' => 0])
                ->field('mobile as alipayphone, result as alipayname, amount, exchange_id')
                ->limit(100)
                ->select();
            // echo json_encode($exchangeList);exit;

            if(count($exchangeList) == 0) {
                $this->error('暂无兑换记录');
            }
            $amount = 0;
            foreach ($exchangeList as $val) {
                $orderno_dteail = 'DF_DETAIL_' .$val['exchange_id'].'_'. rand(10000,99999).time();
                $jsonData[] = [
                	    'aliaccount' => $val['alipayphone'],  //支付宝账号
                	    'money' => $val['amount'],
                	    'aliname' => $val['alipayname'],  //支付宝姓名
                	    'out_trade_no' => $orderno_dteail
                	 ];
                $exchange_id[] = $val['exchange_id'];
                $amount = bcadd($val['amount'], $amount, 2);
            }

            ksort($jsonData);
            // echo json_encode($exchange_id); exit();
            $jsonData = json_encode($jsonData);
            $json = base64_encode($jsonData);


            $orderno = 'DF_'. rand(1000000,9999999).time();
            $data = array(
            	'mchid' => C('HUANXIJIAYI_MCH_ID'),//商户编号
            // 	'pay_applydate' => date('Y-m-d H:i:s'),//时间戳
            	'mer_order_no' => $orderno,//订单号
            	'detail' => $json,
            );
            $key  = C('HUANXIJIAYI_KEY');

            ksort($data);

            $stringSignTemp="detail={$data['detail']}&mchid={$data['mchid']}&mer_order_no={$data['mer_order_no']}&key=".$key;

            $sign = strtoupper(md5($stringSignTemp));
            $data['df_md5sign'] = $sign;
            // echo json_encode($data);exit;
            // var_dump($data);exit;

            $url = 'http://api.huanxijiayi.cn/Payment_AliDf_alidf.html';

            $ch = curl_init($url);
            $timeout = 6000;
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HEADER,0 );
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            $ret = curl_exec($ch);
            curl_close($ch);
            // echo 'return:'.$ret;
            $retjson = json_decode($ret,true);

            if($retjson['status'] == 'success') {
                echo "<a href='".$retjson['data']['dfok_url']."' target='_blank'  >".$retjson['data']['dfok_url']."</a>";
                $url = $retjson['data']['dfok_url'];
                if (get_magic_quotes_gpc()){
                    $url = stripslashes($retjson['data']['dfok_url']);
                }
                //保存记录
                $dfData = [
                    'order_no' => $orderno,
                    'detail' => $jsonData,
                    'status'=> 0,  //默认未代付
                    'url' => $url,
                    'ctime' => time(),
                    'return' => $ret,
                    'amount' => $amount,
                    'exchange_id' => json_encode($exchange_id),
                ];
                $model->add($dfData);

                //修改兑换记录表的状态为已兑换
                $logMap['exchange_id'] = ['in', $exchange_id];
                D('RedpacketLogs')->where($logMap)->save(['status' => 1]);
                // redirect(urldecode($retjson['data']['dfok_url']));
                exit;
            } else {
                // echo json_encode($exchangeList); exit;
                // echo $ret;
                $this->error('生成代付链接出错');
                // pay_Logs('huanxijiayiAliDf_ERROR======>'. $ret);
                exit;
            }

        } else if($act == 'query') {
            $orderno = I('order_no');
            $res = $this->huanxijiayiAliDfQuery($orderno);

//            echo($res); exit;

        } else if( $act == 'info') {
            $id = I('id');
            $info = $model->where(['id' => $id ])->find();
            echo $info['detail']; exit;
        }

        $p = I('p') ? I('p') : 1;
        $limit = 20;

        $list = $model->page($p, $limit)->order('ctime desc')->select();
        $totals = $model->count();
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $count = D('RedpacketLogs')->where(['paytype' => 5, 'status' => 0])->count();
        $money = D('RedpacketLogs')->where(['paytype' => 5, 'status' => 0])->field('sum(amount) as money')->find();

        $this->assign('count', $count ? $count : 0); // 待代付条数
        $this->assign('money', $money['money'] ? $money['money'] : 0); // 待代付金额
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }


    //代付查询接口
    public function huanxijiayiAliDfQuery($order_no)
    {
        header("Content-Type: text/html;charset=utf-8");
        $info = M('ExchangeDf')->where(['order_no'=>$order_no])->field('id, return, amount')->find();

        $order_return  = json_decode($info['return'], true);
        $orderno = $order_return['data']['orderno'];

        // $orderno = I('ordersn');
        $mchid = C('HUANXIJIAYI_MCH_ID');
        $key = C('HUANXIJIAYI_KEY');

        $data = [
            'orderno' => $orderno,
            'mchid' => $mchid,
        ];
        $stringSignTemp="mchid={$mchid}&orderno={$orderno}&key=".$key;

        $sign = strtoupper(md5($stringSignTemp));
        $data['df_md5sign'] = $sign;
        // var_dump($data);exit;

        $url = 'http://api.huanxijiayi.cn/Payment_AliDf_query.html';
        $ch = curl_init($url);
        $timeout = 6000;
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER,0 );
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $ret = curl_exec($ch);
        curl_close($ch);
        // echo 'return:'.$ret;
        $retjson = json_decode($ret,true);

        if($retjson['status'] == 'success' && $retjson['data']['success_amount'] == $info['amount']) {
            $status = 1;
        } else {
            $status = 0;
        }
        M('ExchangeDf')->where(['order_no' => $order_no])->save(['status' => $status]);
        $this->success('查询成功：'.$status == 1?'已代付':'未代付'); exit;
        return $ret;


    }

    //机器人配置
    public function robotsConf()
    {
        $fromobj = D('DRobots'); 
        $p = I('p');
        $limit = 20;

        $dev_id = trim(I('dev_id'));
        if ($dev_id != '') {
            $map['dev_id'] = $dev_id;
            $map['name'] = $dev_id;
            $map['_logic'] = 'OR';
        } 
        //$map['u.id'] =array('neq','');

        $total = $fromobj->join('game1.d_users as u ON u.account = d_robots.id')->where($map)->count();
        $list = $fromobj->join('game1.d_users as u ON u.account = d_robots.id')->where($map)->field('d_robots.*,u.gold,u.vip_lev ')->page($p, $limit)->order('id desc')->select();
        
        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $from_ids = array();

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->assign();

        $this->display();
    }
    //编辑机器人
    public function editRobots()
    {
        $fromobj = D('DRobots');  
        if (IS_POST) {

            $name = trim(I('post.name'));
            $bet = trim(I('post.bet'));
            $line = trim(I('post.line'));
            $status = trim(I('post.status'));
            $room_type = trim(I('post.room_type'));
            $run_time = trim(I('post.run_time'));
            $rw_cond = trim(I('post.rw_cond'));


            if ($bet == '') {
                $this->error("投注挡位不能为空");
            }

            if ($line == '') {
                $this->error("线数不能为空");
            }

            if ($status == '') {
                $this->error("状态不能为空");
            }

            if ($room_type == '') {
                $this->error("房间类型不能为空");
            }

            if ($run_time == '') {
                $this->error("运行时长不能为空");
            }
 

            $id = I('id');

            $data = [
                'bet' => $bet,
                'line' => $line,
                'status' => $status,
                'run_time' =>$run_time,
            ];

            if ($fromobj->where(['id' => $id])->save($data)) {
                $this->success('编辑成功', U('robotsConf'));
            } else {
                
                $this->error('编辑失败！');
            }

        } else {

            $id = I('id');
            $info = $fromobj->where(['id' => $id])->find();
            $this->assign('info', $info);
            $this->display();

        }
    }

    //开启配置
    public function openrobots()
    {
        $robot_config = C('ROBOT_CONF');
        $connection = ssh2_connect( $robot_config['host'] , 22 );  //host
        ssh2_auth_password($connection , $robot_config['name'] , $robot_config['pwd'] );  //账号密码

        $command1 = 'echo `date +%Y%m%d_%H%M` start robot > /home/svjh/1.txt';
        $command2 = 'sudo sh /home/svjh/startRobot.sh';

        ssh2_exec($connection, $command1);

        $stream = ssh2_exec($connection, $command2);
        stream_set_blocking($stream, true);
        $output = stream_get_contents($stream);        
        $this->success($output);
    }

    //关闭配置
    public function closerobots()
    {
        $robot_config = C('ROBOT_CONF');
        $connection = ssh2_connect( $robot_config['host'] , 22 );  //host
        ssh2_auth_password($connection , $robot_config['name'] , $robot_config['pwd'] );  //账号密码

        $command1 = 'echo `date +%Y%m%d_%H%M` stop robot > /home/svjh/2.txt';
        $command2 = 'sudo sh /home/svjh/stopRobot.sh';

        ssh2_exec($connection, $command1);

        $stream = ssh2_exec($connection, $command2);
        stream_set_blocking($stream, true);
        $output = stream_get_contents($stream);        
        $this->success($output);


    }

    public function testrobot()
    {
        header("Content-type: text/html; charset=utf-8");
        $robot_config = C('ROBOT_CONF');
        $connection = ssh2_connect( $robot_config['host'] , 22 );  //host
        ssh2_auth_password($connection , $robot_config['name'] , $robot_config['pwd']  );  //账号密码
        $stream = ssh2_exec($connection, 'sudo sh /home/svjh/startRobot.sh');
        stream_set_blocking($stream, true);
        $output = stream_get_contents($stream);
        
        echo $output; 
    }
    
    public function reloadnginx()
    {
        $robot_config = C('ROBOT_CONF');
        $connection = ssh2_connect( $robot_config['host'] , 22 );  //host
        ssh2_auth_password($connection , $robot_config['name'] , $robot_config['pwd'] );  //账号密码

        $command = 'nginx -s reload';

        $stream = ssh2_exec($connection, $command);
        stream_set_blocking($stream, true);
        $output = stream_get_contents($stream);        
        $this->success($output);
    }

    public function reloadkefu()
    {
        $robot_config = C('ROBOT_CONF');
        $connection = ssh2_connect( $robot_config['host'] , 22 );  //host
        ssh2_auth_password($connection , $robot_config['name'] , $robot_config['pwd'] );  //账号密码

        $command = 'php /www/wwwroot/kefu.syhjvip.com/service/start.php restart -d';

        $stream = ssh2_exec($connection, $command);
        stream_set_blocking($stream, true);
        $output = stream_get_contents($stream);        
        $this->success($output);
    }
}