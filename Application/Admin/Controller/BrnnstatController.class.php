<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 13:56
 */

namespace Admin\Controller;

use Think\Controller;

class BrnnstatController extends Controller
{

    /**
     * 百人牛牛半小时统计一次
     */
    public function stat2()
    {
        $stime = microtime(true);
        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        $Brnngrail = D('Brnngrail');

        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['uid'] = array('egt', 0); //排除AI,150000--150148不算

        $logs = D('Brnnlog')->where($map)->select();

        $data = array();
        foreach ($logs as $v) {

            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            if (intval($v['test_type']) == 1) //1:试玩
            {
                if ($v['get'] > $v['use']) //如果赢取大于下注
                {
                    $data[$date]['get1'] += intval($v['get'] + $v['pool_win'] - $v['use'] - $v['tax']);
                    $data[$date]['use1'] += 0; //intval($v['use']);
                } else if ($v['get'] == $v['use']) {
                    $data[$date]['get1'] += intval($v['pool_win']);
                    $data[$date]['use1'] += 0; //intval($v['use']);
                } else {
                    $data[$date]['get1'] += intval($v['pool_win']);
                    // $data[$date]['use1'] += intval($v['use'] + abs($v['get']));
                    $data[$date]['use1'] += intval($v['use'] - $v['get']);
                }
                $data[$date]['pool_win1'] += intval($v['pool_win']);
                $data[$date]['sui1'] += intval($v['tax'] * 5 / 8);
                $data[$date]['pool1'] += intval($v['tax'] * 3 / 8);
                /*             
                 if($v['get'] > 0) {
                $data[$date]['get1'] += intval($v['get'] + $v['pool_win']);
                $data[$date]['use1'] += intval($v['use']);
                 }else{
                $data[$date]['get1'] += intval(0 + $v['pool_win']);
                $data[$date]['use1'] += intval($v['use'] + abs($v['get']));
                 }             
            $data[$date]['pool_win1'] += intval($v['pool_win']);
            $data[$date]['sui1'] += intval($v['tax'] * 4 / 9);
            $data[$date]['pool1'] += intval($v['tax'] * 3 / 9);*/
            } else {
                if ($v['get'] > $v['use']) {
                    $data[$date]['get2'] += intval($v['get'] + $v['pool_win'] - $v['use'] - $v['tax']);
                    $data[$date]['use2'] += 0; //intval($v['use']);
                } else if ($v['get'] == $v['use']) {
                    $data[$date]['get2'] += intval($v['pool_win']);
                    $data[$date]['use2'] += 0; //intval($v['use']);
                } else {
                    $data[$date]['get2'] += intval($v['pool_win']);
                    // $data[$date]['use2'] += intval($v['use'] + abs($v['get']));
                    $data[$date]['use2'] += intval($v['use'] - $v['get']);
                }
                $data[$date]['pool_win2'] += intval($v['pool_win']);
                $data[$date]['sui2'] += intval($v['tax'] * 5 / 8);
                $data[$date]['pool2'] += intval($v['tax'] * 3 / 8);
            }
            // $data[$date]['pool_win'] += intval($v['pool_win']);
            //$data[$date]['sui'] += intval($v['tax'] * 4 / 9);
            // $data[$date]['pool'] += intval($v['tax'] * 3 / 9);
        }
        unset($logs);

        //=========================
        $map2['c_time'] = array('between', array($b_time, $e_time));
        $map2['room'] = array('in', array(158, 159)); //156=试玩超级水果，157=娱乐超级水果
        $map2['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $get_list = D('redbaglog')->where($map2)->select();
        $result = array();
        foreach ($get_list as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            if ($v['room'] == 158)
                $data[$date]['redget156'] += intval($v['money']);
            else
                $data[$date]['redget157'] += intval($v['money']);
        }
        unset($get_list);

        foreach ($data as $k => $v) {
            $ret = array(
                'time' => strtotime($k),
                'get1' => intval($v['get1']),
                'use1' => intval($v['use1']),
                'get2' => intval($v['get2']),
                'use2' => intval($v['use2']),
                'redget1' => intval($v['redget156']),
                'redget2' => intval($v['redget157']),
                'sui1' => intval($v['sui1']),
                'pool_win1' => intval($v['pool_win1']),
                'pool1' => intval($v['pool1']), //进入彩池的金额
                'sui2' => intval($v['sui2']),
                'pool_win2' => intval($v['pool_win2']),
                'pool2' => intval($v['pool2']), //进入彩池的金额
                'stat' => intval($v['get'] - $v['use'])
            );

            if ($info = $Brnngrail->where(array('time' => strtotime($k)))->find()) {
                $Brnngrail->where(array('_id' => $info['_id']))->save($ret);
            } else {
                $Brnngrail->add($ret);
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    public function stat()
    {
        $stime = microtime(true); // 函数返回当前 Unix 时间戳的微秒数。
        $dtime = NOW_TIME;
        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        // 查找内部账号，统计的时候排除
        $DBUsers = D('Users');
        $where['inside'] = 1;
        $uids = $DBUsers->where($where)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $logs = D('Brnnlog')->where($map)->select();
        $data = array();
        foreach ($logs as $key => $v) {
            // 排除内部账号
            if (in_array($v['uid'], $uids_arr))
                continue;

            $date = date('Y-m-d H:00:00', $v['c_time']); // 创建时间转换为小时制
            if (intval($v['test_type']) == 1) {
                // 1: 试玩
                $result = $v['get'] - abs($v['use']); // 获取金币结果
                if ($result > 0) {
                    $data[$date]['get1'] += $result - abs($v['tax']) + $v['pool_win']; // 有概率从奖池获得部分金币
                    $data[$date]['use1'] += 0;
                } else {
                    // 
                    $data[$date]['get1'] += $v['pool_win'];
                    $data[$date]['use1'] += abs($result);
                }

                $data[$date]['pool_win1'] += intval($v['pool_win']); // 彩池产出
                $data[$date]['sui1'] += intval($v['tax']); // 税收
                $data[$date]['pool1'] += intval($v['tax'] * (3 / 8)); // 彩池新增
            } else {
                // 2: 娱乐
                $result = $v['get'] - abs($v['use']); // 获取金币结果
                if ($result > 0) {
                    $data[$date]['get2'] += $result - abs($v['tax']) + $v['pool_win']; // 有概率从奖池获得部分金币
                    $data[$date]['use2'] += 0;
                } else {
                    // 
                    $data[$date]['get2'] += $v['pool_win'];
                    $data[$date]['use2'] += abs($result);
                }

                $data[$date]['pool_win2'] += intval($v['pool_win']); // 彩池产出
                $data[$date]['sui2'] += intval($v['tax']); // 税收
                $data[$date]['pool2'] += intval($v['tax'] * (3 / 8)); // 彩池新增
            }
        }
        unset($logs); // 释放内存

        // 获取红包数据
        $map2['c_time'] = array('between', array($b_time, $e_time));
        $map2['room'] = array('in', array(158, 159)); //156=试玩超级水果，157=娱乐超级水果
        $map2['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $get_list = D('redbaglog')->where($map2)->select();
        $result = array();
        foreach ($get_list as $key => $v) {
            // 排除内部账号
            if (in_array($v['uid'], $uids_arr))
                continue;

            $date = date('Y-m-d H:00:00', $v['c_time']);
            if ($v['room'] == 158)
                $data[$date]['redget156'] += intval($v['money']);
            else
                $data[$date]['redget157'] += intval($v['money']);
        }
        unset($get_list); // 释放内存

        foreach ($data as $k => $v) {
            $ret = array(
                'time' => strtotime($k),
                'get1' => intval($v['get1']),
                'use1' => intval($v['use1']),
                'get2' => intval($v['get2']),
                'use2' => intval($v['use2']),
                'redget1' => intval($v['redget156']),
                'redget2' => intval($v['redget157']),
                'sui1' => intval($v['sui1']),
                'pool_win1' => intval($v['pool_win1']),
                'pool1' => intval($v['pool1']), //进入彩池的金额
                'sui2' => intval($v['sui2']),
                'pool_win2' => intval($v['pool_win2']),
                'pool2' => intval($v['pool2']), //进入彩池的金额
                'stat' => intval($v['get'] - $v['use'])
            );
            $Brnngrail = D('Brnngrail');
            if ($info = $Brnngrail->where(array('time' => strtotime($k)))->find()) {
                $Brnngrail->where(array('_id' => $info['_id']))->save($ret);
            } else {
                $Brnngrail->add($ret);
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    public function stat_upgrade()
    {
        $stime = microtime(true); // 函数返回当前 Unix 时间戳的微秒数。
        $dtime = NOW_TIME;
        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        // 查找内部账号，统计的时候排除
        $Brnngrail = D('Brnngrail');
        $DBUsers = D('Users');
        $where['inside'] = 1;
        $uids = $DBUsers->where($where)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        $bTime = date('YmdHis', $b_time);
        $b_hour = $bTime % 1000000 - $bTime % 10000;
        $b_hour = $b_hour / 10000;

        $eTime = date('YmdHis', $e_time);
        $e_hour = $eTime % 1000000 - $eTime % 10000;
        $e_hour = $e_hour / 10000;

        echo "time1 ：" . $b_hour . "\n";
        echo "time2 : " . $e_hour . "\n";
        for ($i = $b_hour; $i <= $e_hour; $i++) {
            //
            if ($i < 10) {
                $real_b_time = date('Ymd', $b_time) . '0' . $i . "0000";
            } else {
                $real_b_time = date('Ymd', $b_time) . $i . "0000";
            }

            $real_b_time = strtotime($real_b_time);
            $real_e_time = $real_b_time + 3599;

            $map['c_time'] = array('between', array($real_b_time, $real_e_time));
            $map['uid'] = array('egt', 0); //排除AI,150000--150148不算
            $logs = D('Brnnlog')->where($map)->select();
            $data = array();
            foreach ($logs as $key => $v) {
                // 排除内部账号
                if (in_array($v['uid'], $uids_arr))
                    continue;

                $date = date('Y-m-d H:00:00', $v['c_time']); // 创建时间转换为小时制
                if (intval($v['test_type']) == 1) {
                    // 1: 试玩
                    $result = $v['get'] - abs($v['use']); // 获取金币结果
                    if ($result > 0) {
                        $data[$date]['get1'] += $result - abs($v['tax']) + $v['pool_win']; // 有概率从奖池获得部分金币
                        $data[$date]['use1'] += 0;
                    } else {
                        // 
                        $data[$date]['get1'] += $v['pool_win'];
                        $data[$date]['use1'] += abs($result);
                    }

                    $data[$date]['pool_win1'] += intval($v['pool_win']); // 彩池产出
                    $data[$date]['sui1'] += intval($v['tax']); // 税收
                    $data[$date]['pool1'] += intval($v['tax'] * (3 / 8)); // 彩池新增
                } else {
                    // 2: 娱乐
                    $result = $v['get'] - abs($v['use']); // 获取金币结果
                    if ($result > 0) {
                        $data[$date]['get2'] += $result - abs($v['tax']) + $v['pool_win']; // 有概率从奖池获得部分金币
                        $data[$date]['use2'] += 0;
                    } else {
                        // 
                        $data[$date]['get2'] += $v['pool_win'];
                        $data[$date]['use2'] += abs($result);
                    }

                    $data[$date]['pool_win2'] += intval($v['pool_win']); // 彩池产出
                    $data[$date]['sui2'] += intval($v['tax']); // 税收
                    $data[$date]['pool2'] += intval($v['tax'] * (3 / 8)); // 彩池新增
                }
            }
            unset($logs); // 释放内存

            // 获取红包数据
            $map2['c_time'] = array('between', array($real_b_time, $real_e_time));
            $map2['room'] = array('in', array(158, 159)); //156=试玩超级水果，157=娱乐超级水果
            $map2['uid'] = array('egt', 0); //排除AI,150000--150148不算
            $get_list = D('redbaglog')->where($map2)->select();
            $result = array();
            foreach ($get_list as $key => $v) {
                // 排除内部账号
                if (in_array($v['uid'], $uids_arr))
                    continue;

                $date = date('Y-m-d H:00:00', $v['c_time']);
                if ($v['room'] == 158)
                    $data[$date]['redget156'] += intval($v['money']);
                else
                    $data[$date]['redget157'] += intval($v['money']);
            }
            unset($get_list); // 释放内存

            foreach ($data as $k => $v) {
                $ret = array(
                    'time' => strtotime($k),
                    'get1' => intval($v['get1']),
                    'use1' => intval($v['use1']),
                    'get2' => intval($v['get2']),
                    'use2' => intval($v['use2']),
                    'redget1' => intval($v['redget156']),
                    'redget2' => intval($v['redget157']),
                    'sui1' => intval($v['sui1']),
                    'pool_win1' => intval($v['pool_win1']),
                    'pool1' => intval($v['pool1']), //进入彩池的金额
                    'sui2' => intval($v['sui2']),
                    'pool_win2' => intval($v['pool_win2']),
                    'pool2' => intval($v['pool2']), //进入彩池的金额
                    'stat' => intval($v['get'] - $v['use'])
                );
                if ($oldData = $Brnngrail->where(array('time' => strtotime($k)))->find()) {
                    $Brnngrail->where(array('_id' => $oldData['_id']))->save($ret);
                } else {
                    $Brnngrail->add($ret);
                }
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }


    public function setWin()
    {

        $map['uid'] = array('egt', 0); //排除AI
        $map['c_time'] = array('egt', 1500480000);
        $logs = D('Brnnlog');


        $map['win'] = 0;
        $map['lose'] = 0;

        $count = $logs->where($map)->count();

        $slip = ceil($count / 100);

        for ($i = 0; $i < $slip; $i++) {
            $logs_list = $logs->where($map)->limit(($i * 100) . ',100')->select();

            foreach ($logs_list as $k => $v) {
                $operate = $this->operate_data($v['get'], $v['use']);
                //if(intval($operate['get']) ==0 && intval($operate['use']) == 0) continue;
                $new_get = intval($operate['win'] * 0.96);

                echo $slip . '|' . $i . '|' . $v['get'] . '|' . $v['use'] . '|' . $operate['win'] . '|' . $operate['lose'] . '|' . $new_get . PHP_EOL;

                $new['win'] = intval($operate['win']);
                $new['lose'] = intval($operate['lose']);
                $new['new_get'] = $new_get;

                $logs->where(array('_id' => $v['_id']))->save($new);
            }

            unset($new);
        }

        echo 'SUCCESS' . PHP_EOL;
    }

    protected function operate_data($get, $use)
    {

        if ($get > $use) {
            $data['get'] = $get - $use;
            $data['use'] = 0;
        } else {
            $data['get'] = 0;
            $data['use'] = $use - $get;
        }

        return $data;
    }
}
