<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use User\Api\UserApi;
/**
 * 后台用户控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class WechatPayController extends \Think\Controller
{

    /**
     * 用户管理首页 
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function index()
    {
		
        $jsApi =   I('jsApi');
        $data['orderid'] = I('orderid');
        $data['pricetotal'] = I('pricetotal'); 
        $this->assign('jsApi', $jsApi);
        $this->assign('info', $data);
        $this->display();
    }

    public function pay()
    {
		
        
        $data['appId'] = I('appId');
        $data['timeStamp'] = I('timeStamp');
        $data['nonceStr'] = I('nonceStr');
        $data['package'] = I('package');
        $data['signType'] = I('signType');
        $data['paySign'] = I('paySign');
        $data['orderid'] = I('orderid');
        $data['pricetotal'] = I('pricetotal'); 
        
        $this->assign('info', $data);
        $this->display();
        // http://paopao.youyiwl.cn/index.php/WechatPay/index.html?appId=wx2421b1c4370ec43b&orderid=666&pricetotal=666&timeStamp=1395712654&nonceStr=e61463f8efa94090b1f366cccfbbb444&package=wx21201855730335ac86f8c43d1889123400&signType=RSA&paySign=oR9d8PuhnIc+YZ8cBHFCwfgpaK9gd7vaRvkYD7rthRAZ\/X+QBhcCYL21N7cHCTUxbQ+EAt6Uy+lwSN22f5YZvI45MLko8Pfso0jm46v5hqcVwrk6uddkGuT+Cdvu4WBqDzaDjnNa5UK3GfE1Wfl2gHxIIY5lLdUgWFts17D4WuolLLkiFZV+JSHMvH7eaLdT9N5GBovBwu5yYKUR7skR8Fu+LozcSqQixnlEZUfyE55feLOQTUYzLmR9pNtPbPsu6WVhbNHMS3Ss2+AehHvz+n64GDmXxbX++IOBvm2olHu3PsOUGRwhudhVf7UcGcunXt8cqNjKNqZLhLw4jq\/xDg==
    }

    public function weipay()
    {
		
        
        $data['weiurl'] = I('weiurl');
        if(empty($data['weiurl'])){$data['weiurl']='weixin://dl/business/?appid=wx2a99fbd4706e4c30&path=pages/pay/index&query=mchId=2404250640&mchName=湖南营博网络科技有限公司&money=0.02&thirdOrderNo=hzzh-trade1714484420&env_version=release';}
        $this->assign('info', $data);
        $this->display();
        // http://paopao.youyiwl.cn/index.php/WechatPay/weipay.html
    }
       
}
