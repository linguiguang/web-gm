<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Admin\Model\AuthRuleModel;
use Think\Controller; 
/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class CenterDataController extends Controller
{

    /**
     * 后台控制器初始化
     * http://ruiqumanage:8015/index.php/ChannelApi/
     */
    protected function _initialize()
    {

        /* 读取数据库中的配置 */
        $config = S('DB_CONFIG_DATA');
        if (!$config) {
            $config = api('Config/lists');
            S('DB_CONFIG_DATA', $config);
        }
        C($config); //添加配置

    }

    public function index()
    {
		//print(md5(getRandomString(6)));exit;
        print(iconv('UTF-8','GB2312',"欢迎使用数据中心API接口"));

    }

    /* 
    json 中文转码函数
    */
    public function arrayrecursive(&$array, $function, $apply_to_keys_also = false) 
    { 
        static $recursive_counter = 0; 
        if (++$recursive_counter > 1000) { 
            die('possible deep recursion attack'); 
        } 
        foreach ($array as $key => $value) { 
            if (is_array($value)) { 
                $this->arrayrecursive($array[$key], $apply_to_keys_also); 
            } else { 
                $array[$key] = $function($value); 
            } 
    
            if ($apply_to_keys_also && is_string($key)) { 
                $new_key = $function($key); 
                if ($new_key != $key) { 
                    $array[$new_key] = $array[$key]; 
                    unset($array[$key]); 
                } 
            } 
        } 
        $recursive_counter--; 
    } 
    public function jsontext($array) { 
        $this->arrayrecursive($array, 'urlencode', true); 
        $json = json_encode($array); 
        return urldecode($json); 
    } 
    /* 
    json 中文转码函数
    */

    
    //实名制    黑白名单    
    /*
     黑白名单
    */
    public function blackwhitelist()
    {
		
        $data['dev_id']       =   I('get.devid');
        $data['card_no']       =   I('get.cardno');
        $data['ip']       =   I('get.ip');
        //$data['hash']       =   I('hash');

        if (empty($data['ip']) && empty($data['dev_id']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
            $ret['data']=""; 
        }
        else
        {
            //if(!empty($data['card_no'])){$umap['card_no'] = $data['card_no'];  }
            //if(!empty($data['dev_id'])){$umap['dev_id'] = $data['dev_id'];  }
            if(!empty($data['card_no'])&&!empty($data['dev_id'])&&!empty($data['ip']))
            {
                $map['card_no'] = $data['card_no'];
                $map['dev_id'] = $data['dev_id']; 
                $map['ip'] = $data['ip']; 
                $map['_logic'] = 'OR';
                $umap['_complex'] = $map;

            }
            else if(!empty($data['card_no'])&&!empty($data['dev_id']))
            {
                $map['card_no'] = $data['card_no'];
                $map['dev_id'] = $data['dev_id']; 
                $map['_logic'] = 'OR';
                $umap['_complex'] = $map;

            }else if(!empty($data['card_no'])&&!empty($data['ip']))
            {
                $map['card_no'] = $data['card_no'];
                $map['ip'] = $data['ip']; 
                $map['_logic'] = 'OR';
                $umap['_complex'] = $map;

            }else if(!empty($data['dev_id'])&&!empty($data['ip']))
            {
                $map['dev_id'] = $data['dev_id']; 
                $map['ip'] = $data['ip']; 
                $map['_logic'] = 'OR';
                $umap['_complex'] = $map;

            }else if(!empty($data['card_no']))
            {
                $umap['card_no'] = $data['card_no']; 
            }else if(!empty($data['ip']))
            {
                $umap['ip'] = $data['ip']; 
            }else if(!empty($data['dev_id']))
            {
                $umap['dev_id'] = $data['dev_id']; 
            }     
            $user = D('DWbUserList')->where($umap)->find();
            if (empty($user))
            {
                $ret['code']="102";
                $ret['msg']="记录不存在"; 
                $ret['data']= ""; 
            }
            else
            {
                $ret['code']="200";
                $ret['msg']="查询成功"; 
                $ret['data']= array(
                    "status"=> $user['status'],
                    "attr"=> $user['attr']
                ); 
            }

        }
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }

    public function blackwhiteadd()
    {
		
        $data['dev_id']       =   I('post.devid');
        $data['card_no']       =   I('post.cardno');
        $data['status']       =   I('post.status');
        
        //$data['hash']       =   I('hash');

        if ((empty($data['card_no']) && empty($data['dev_id']))||empty($data['status']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
        }
        else
        {          
            if(!empty($data['card_no'])){$umap['card_no'] = $data['card_no'];  }
            if(!empty($data['dev_id'])){$umap['dev_id'] = $data['dev_id'];  }       
            $user = D('DWbUserList')->where($umap)->find();
            if (!empty($user))
            {
                $ret['code']="102";
                $ret['msg']="记录已存在"; 
            }
            else
            {
                
                $data['created_at'] = date("Y-m-d H:i:s");
                $res = D('DWbUserList')->add($data);
                if($res) {
                    $ret['code']="200";
                    $ret['msg']="新增成功";
                }
                else
                {
                    $ret['code']="103";
                    $ret['msg']="新增失败"; 
                }

                
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }

    public function blackwhiteupdate()
    {
		
        $data['dev_id']       =   I('post.devid');
        $data['card_no']       =   I('post.cardno');
        $data['status']       =   I('post.status');
        
        //$data['hash']       =   I('hash');

        if ((empty($data['card_no']) && empty($data['dev_id']))||empty($data['status']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
        }
        else
        {          
            if(!empty($data['card_no'])){$umap['card_no'] = $data['card_no'];  }
            if(!empty($data['dev_id'])){$umap['dev_id'] = $data['dev_id'];  }       
            $user = D('DWbUserList')->where($umap)->find();
            if (empty($user))
            {
                $ret['code']="102";
                $ret['msg']="记录不存在"; 
            }
            else
            {
                $dataup['status'] = $data['status'];
                $res = D('DWbUserList')->where($umap)->save($dataup);
                if($res) {
                    $ret['code']="200";
                    $ret['msg']="修改成功";
                }
                else
                {
                    $ret['code']="103";
                    $ret['msg']="修改失败"; 
                }

                
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }
     
    public function redlnamelist()
    {
		
        $data['name']       =   I('get.name');
        $data['card_no']       =   I('get.cardno');
        //$data['hash']       =   I('hash');

        if (empty($data['card_no']) || empty($data['name']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
            $ret['data']=""; 
        }
        else
        {
            if(!empty($data['card_no'])){$umap['card_no'] = $data['card_no'];  }
            if(!empty($data['name'])){$umap['name'] = $data['name'];  }       
            $user = D('DRealNameList')->where($umap)->find();
            if (empty($user))
            {
                $ret['code']="102";
                $ret['msg']="记录不存在"; 
                $ret['data']= ""; 
            }
            else
            {
                $ret['code']="200";
                $ret['msg']="查询成功"; 
                $ret['data']= array(
                    "card_no"=> $user['card_no']
                ); 
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }

    public function redlnameadd()
    {
		
        $data['name']       =   I('post.name');
        $data['card_no']       =   I('post.cardno');
        
        //$data['hash']       =   I('hash');

        if (empty($data['card_no'])||empty($data['name']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
        }
        else
        {          
            if(!empty($data['card_no'])){$umap['card_no'] = $data['card_no'];  }
            if(!empty($data['name'])){$umap['name'] = $data['name'];  }       
            $user = D('DRealNameList')->where($umap)->find();
            if (!empty($user))
            {
                $ret['code']="102";
                $ret['msg']="记录已存在"; 
            }
            else
            {
                
                $data['created_at'] = date("Y-m-d H:i:s");
                $data['updated_at'] = date("Y-m-d H:i:s");
                $res = D('DRealNameList')->add($data);
                if($res) {
                    $ret['code']="200";
                    $ret['msg']="新增成功";
                }
                else
                {
                    $ret['code']="103";
                    $ret['msg']="新增失败"; 
                }

                
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }

    
    public function curl_post($params, $url, $timout){
        $ch = curl_init();
        $json = json_encode($params);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 设置超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, $timout);
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:'.'application/json; charset=UTF-8'));
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /**
     * 生成签名方法
     * 不同openapi的签名参数不同
     * @param $secretKey
     * @param $params
     * @return string
     */
    public function gen_signature($secretKey, $params){
        ksort($params);
        $buff="";
        foreach($params as $key=>$value){
            if($value !== null) {
                $buff .=$key;
                $buff .=$value;
            }
        }
        $buff .= $secretKey;
        return md5($buff);
    }

    /**
     * 将输入数据的编码统一转换成utf8
     * @params 输入的参数
     */
    public function toUtf8($params){
        $utf8s = array();
        foreach ($params as $key => $value) {
            $utf8s[$key] = is_string($value) ? mb_convert_encoding($value, "utf8", INTERNAL_STRING_CHARSET) : $value;
        }
        return $utf8s;
    }
    public function generateRandomString($length = 32) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
      
    public function yidunlisttest()
    {
		
        $data['dev_id']       =   I('get.devid');
        $data['token']       =   I('get.token');
        $data['account']       =   I('get.account');
        $data['ip']       =   I('get.ip');
        $data['registerIp']       =   I('get.registerIp');
        
        //$data['hash']       =   I('hash');

        if (empty($data['dev_id']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
            $ret['data']=""; 
        }
        else
        {
            // 1,根据devid查询数据库数据，有则返回记录
            if(!empty($data['dev_id'])){$umap['dev_id'] = $data['dev_id'];  }     
            $user = D('DYidunList')->where($umap)->find();
            if (!empty($user))
            {
                $ret['code']="200";
                $ret['msg']="查询成功"; 
                $ret['data']= array(
                    "dev_id"=> $user['dev_id'],
                    "level"=> $user['level'],
                    "created_at"=> $user['created_at']
                ); 
                
            }
            else
            {
                //2,数据库没有记录，请求易盾获取结果
                $params["secretId"] = "8b47b7baa579cd4f1b2c343928708c4b";
                $params["businessId"] = "b93ef6645658ccba9336ec851a73171c";
                $params["version"] = "603";
                $params["timestamp"] = time() * 1000;
                $params["nonce"] = $this->generateRandomString(32);
                //$params["token"] = $data['token'];
                $params["token"] = "AS9kGIEgmJERfZiNEW1INxM3qPhp9Ot6VD/Z2Q==";
                $params["ip"] = $data['ip'];
                $params["roleId"] = $data['account'];
                $params["account"] = $data['account'];
                $params["deviceId"] = "";
                $params["registerIp"] = $data['registerIp'];
                //$params["sceneData"] = "{\"appChannel\":\"app_store\",\"callbackUrl\":\"your_call_back_url\",\"createTime\":1688106777462,\"currency\":\"USD\",\"money\":1.22,\"orderCreateTime\":1688106777462,\"orderFinishTime\":1688106777462,\"orderId\":\"GPA.3329-6166-4658-28227-01\",\"orderPayTime\":1688106777462,\"payChannel\":\"如app_store\",\"payCurrency\":\"USD\",\"payMoney\":1.11,\"payTime\":1688106777462,\"sceneType\":\"APP_OR_GAME_RECHARGE\"}";
                $params["sceneData"] = "{\"loginType\":\"weixin\",\"appChannel\":\"hzxb\",\"identity\":\"commonUser\"}";
                $params["signature"] = $this->gen_signature("cfa6163fd30aa959c209f3eef58ac43e", $params);
                $params = $this->toUtf8($params);


                $result = $this->curl_post($params, "http://ir-open.dun.163.com/v6/risk/check", 20);
                if ($result === FALSE) {
                    return array("code" => 500, "msg" => "file_get_contents failed.");
                } else {
                    return json_decode($result, true);
                }
                
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }

    public function yidunlistold()
    {
		
        $data['dev_id']       =   I('post.devid');
        $data['token']       =   I('post.token');
        $data['account']       =   I('post.account');
        $data['ip']       =   I('post.ip');
        $data['registerIp']       =   I('post.registerIp');
        $data['devtype']       =   I('post.devtype');
        
        //$data['hash']       =   I('hash');

        if (empty($data['dev_id'])||empty($data['token'])||empty($data['account'])||empty($data['ip'])||empty($data['devtype']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
            $ret['data']=""; 
        }
        else
        {
            // 1,根据devid查询数据库数据，有则返回记录
            if(!empty($data['dev_id'])){$umap['dev_id'] = $data['dev_id'];  }     
            $user = D('DYidunList')->where($umap)->find();
            if (!empty($user))
            {
                $ret['code']="200";
                $ret['msg']="查询成功"; 
                $ret['data']= array(
                    "dev_id"=> $user['dev_id'],
                    "level"=> $user['level'],
                    "task_id"=> $user['task_id']
                ); 
            }
            else
            {
                //2,数据库没有记录，请求易盾获取结果
                $params["secretId"] = "8b47b7baa579cd4f1b2c343928708c4b";
                if(2==$data['devtype'])
                {
                    $params["businessId"] = "b93ef6645658ccba9336ec851a73171c";
                }
                else
                {
                    $params["businessId"] = "8e901285d7a407cc992d01b4c9f0fdb8";
                }
                
                $params["version"] = "603";
                $params["timestamp"] = time() * 1000;
                $params["nonce"] = $this->generateRandomString(32);
                $params["token"] = $data['token'];
                //$params["token"] = "AS9kGIEgmJERfZiNEW1INxM3qPhp9Ot6VD/Z2Q==";
                $params["ip"] = $data['ip'];
                $params["roleId"] = $data['account'];
                $params["account"] = $data['account'];
                $params["deviceId"] = "";
                $params["registerIp"] = $data['registerIp'];
                //$params["sceneData"] = "{\"appChannel\":\"app_store\",\"callbackUrl\":\"your_call_back_url\",\"createTime\":1688106777462,\"currency\":\"USD\",\"money\":1.22,\"orderCreateTime\":1688106777462,\"orderFinishTime\":1688106777462,\"orderId\":\"GPA.3329-6166-4658-28227-01\",\"orderPayTime\":1688106777462,\"payChannel\":\"如app_store\",\"payCurrency\":\"USD\",\"payMoney\":1.11,\"payTime\":1688106777462,\"sceneType\":\"APP_OR_GAME_RECHARGE\"}";
                $params["sceneData"] = "{\"loginType\":\"weixin\",\"appChannel\":\"hzxb\",\"identity\":\"commonUser\"}";
                $params["signature"] = $this->gen_signature("cfa6163fd30aa959c209f3eef58ac43e", $params);
                $params = $this->toUtf8($params);


                $result = $this->curl_post($params, "http://ir-open.dun.163.com/v6/risk/check", 20);
                if ($result === FALSE) {
                    $ret=array("code" => 500, "msg" => "file_get_contents failed.");
                } else {
                    $rets=json_decode($result, true);
                    if(200!=$rets["code"])
                    {
                        exit($rets);
                    }
                    else
                    {
                        $riskLevel=$rets["data"]["riskLevel"];
                        $taskId=$rets["data"]["taskId"];
                        $ret['code']="200";
                        $ret['msg']="查询成功"; 
                        $ret['data']= array(
                            "dev_id"=> $data['dev_id'],
                            "level"=> $riskLevel,
                            "task_id"=> $taskId
                        ); 

                        $udata['dev_id'] = $data['dev_id'];
                        $udata['level'] = $riskLevel;
                        $udata['task_id'] = $taskId;
                        $udata['created_at'] = date("Y-m-d H:i:s");
                        $udata['updated_at'] = date("Y-m-d H:i:s");
                        D('DYidunList')->add($udata);
                    }
                }
                
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }
    public function yidunlist()
    {
		
        $data['dev_id']       =   I('get.devid');

        if (empty($data['dev_id']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
            $ret['data']=""; 
        }
        else
        {
            $umap['dev_id'] = $data['dev_id'];  
            $user = D('DYidunList')->where($umap)->find();
            if (!empty($user))
            {
                $ret['code']="200";
                $ret['msg']="查询成功"; 
                $ret['data']= array(
                    "dev_id"=> $user['dev_id'],
                    "level"=> $user['level'],
                    "task_id"=> $user['task_id']
                ); 
            }
            else
            {
                $ret['code']="102";
                $ret['msg']="记录不存在"; 
                $ret['data']= ""; 
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }

    public function yidunadd()
    {
		
        $data['dev_id']       =   I('post.devid');
        $data['level']       =   I('post.level');
        $data['task_id']       =   I('post.taskid');
        if(empty($data['task_id'])){$data['task_id']="";}

        if ((''==$data['level'])||empty($data['dev_id']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
        }
        else
        {          
            $umap['dev_id'] = $data['dev_id'];  
            $user = D('DYidunList')->where($umap)->find();
            if (!empty($user))
            {
                $ret['code']="102";
                $ret['msg']="记录已存在"; 
            }
            else
            {
                
                $udata['dev_id'] = $data['dev_id'];
                $udata['level'] = $data['level'];
                $udata['task_id'] = $data['task_id'];
                $udata['created_at'] = date("Y-m-d H:i:s");
                $udata['updated_at'] = date("Y-m-d H:i:s");
                $res =D('DYidunList')->add($udata);
                if($res) {
                    $ret['code']="200";
                    $ret['msg']="新增成功";
                }
                else
                {
                    $ret['code']="103";
                    $ret['msg']="新增失败"; 
                }

                
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }

    public function yidunupdate()
    {
		
        $data['dev_id']       =   I('post.devid');
        $data['level']       =   I('post.level');
        $data['task_id']       =   I('post.taskid');
        if(empty($data['task_id'])){$data['task_id']="";}

        if ((''==$data['level'])||empty($data['dev_id']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段缺失"; 
        }
        else
        {          
            $umap['dev_id'] = $data['dev_id'];  
            $user = D('DYidunList')->where($umap)->find();
            if (empty($user))
            {
                $ret['code']="102";
                $ret['msg']="记录不存在"; 
            }
            else
            {
                
                $dataup['level'] = $data['level'];
                $res = D('DYidunList')->where($umap)->save($dataup);
                if($res) {
                    $ret['code']="200";
                    $ret['msg']="修改成功";
                }
                else
                {
                    $ret['code']="103";
                    $ret['msg']="修改失败"; 
                }
            }

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }
    
    public function complaintadd()
    {
		$data['server_name']       =   I('server_name');
        $data['role_info']       =   I('role_info');
        $data['role_id']       =   I('role_id');
        $data['problem_type']       =   I('problem_type');
        $data['order_number']       =   I('order_number');
        $data['problem_detail']       =   I('problem_detail');
        
          


        if (empty($data['server_name']))
        {
            $ret['code']="101";
            $ret['msg']="接口字段server_name缺失"; 
        }
        else if (empty($data['role_info']))
        {
            $ret['code']="102";
            $ret['msg']="接口字段role_info缺失"; 
        }
        else if (empty($data['role_id']))
        {
            $ret['code']="103";
            $ret['msg']="接口字段role_id缺失"; 
        }
        else if (empty($data['problem_type']))
        {
            $ret['code']="104";
            $ret['msg']="接口字段problem_type缺失"; 
        }
        else if (empty($data['problem_detail']))
        {
            $ret['code']="105";
            $ret['msg']="接口字段problem_detail缺失"; 
        }
        else
        {          
            $complaint = D('DComplaint')->where($data)->find();
            if (!empty($complaint))
            {
                $ret['code']="106";
                $ret['msg']="已提交勿重复提交";
            }
            else
            {
                $res = D('DComplaint')->add($data);
                if($res) {
                    $ret['code']="200";
                    $ret['msg']="新增成功";
                }
                else
                {
                    $ret['code']="103";
                    $ret['msg']="新增失败"; 
                }
            }
            

        }      
        exit(json_encode($ret, JSON_UNESCAPED_UNICODE));  
    }
}