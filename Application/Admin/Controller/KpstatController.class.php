<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 13:56
 */
namespace Admin\Controller;
use Think\Controller;

class KpstatController extends Controller{

    /**
     * 看牌抢庄大盘半小时统计一次
     */
    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v){
            $uids_arr[] = intval($v);
        }

        $Kpgrail = D('Kpgrail');

        $map['c_time'] = array('between', array($b_time, $e_time));
        $kplogs = D('Kplog')->where($map)->select();

        $data = array();
        foreach ($kplogs as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            $data[$date]['get'] += $v['get'] + $v['tax'];
            $data[$date]['use'] += $v['use'] + $v['inmoney'];
            $data[$date]['sui'] += $v['tax'];
        }
        unset($kplogs);

        foreach ($data as $k=>$v){
            $ret['time'] = strtotime($k);
            $ret['get'] = intval($v['get']);
            $ret['use'] = intval($v['use']);
            $ret['sui'] = intval($v['sui']);
            $ret['stat'] = intval($v['get'] - $v['use']);

            if($info = $Kpgrail->where(array('time'=>strtotime($k)))->find()){
                $Kpgrail->where(array('_id'=>$info['_id']))->save($ret);
            }else{
                $Kpgrail->add($ret);
            }
        }

        echo 'SUCCESS'.PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times".PHP_EOL;
    }
}