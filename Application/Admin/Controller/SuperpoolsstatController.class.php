<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 13:56
 */

namespace Admin\Controller;

use Think\Controller;

class SuperpoolsstatController extends Controller
{

    /**
     * 超级水果狂欢半小时统计一次
     */
    public function poolstatBk()
    {
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        $poolstat = D('Superpoolstat');

        //水果游戏记录
        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $logs = D('Superfruitlog')->where($map)->select();
        $data = array();
        foreach ($logs as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);

            // $data[$date]['get'] += intval($v['get'] + $v['pool_win']);
            // $data[$date]['use'] += intval($v['use']);
            if (intval($v['test_type']) == 1) //1:试玩
            {
                /*
				 if($v['get'] > 0) {
                $data[$date]['get1'] += intval($v['get'] + $v['pool_win']);
                $data[$date]['use1'] += intval($v['use']);
                 }else{
                $data[$date]['get1'] += intval(0 + $v['pool_win']);
                $data[$date]['use1'] += intval($v['use'] + abs($v['get']));
                 } 
				 */
                if ($v['get'] > $v['use']) //如果赢取大于下注
                {
                    $data[$date]['get1'] += intval($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                    $data[$date]['use1'] += 0;
                } else if ($v['get'] == $v['use']) {
                    $data[$date]['get1'] += intval($v['pool_win']);
                    $data[$date]['use1'] += 0;
                } else {
                    $data[$date]['get1'] += intval($v['pool_win']);
                    $data[$date]['use1'] += intval($v['use'] - $v['get']);
                }
                $data[$date]['pool_win1'] += intval($v['pool_win']);
                $data[$date]['sui1'] += intval($v['tax']);
                $data[$date]['pool1'] += intval($v['pooladd']);
            } else {     /*          
                 if($v['get'] > 0) {
                $data[$date]['get2'] += intval($v['get'] + $v['pool_win']);
                $data[$date]['use2'] += intval($v['use']);
                 }else{
                $data[$date]['get2'] += intval(0 + $v['pool_win']);
                $data[$date]['use2'] += intval($v['use'] + abs($v['get']));
                 }  */
                if ($v['get'] > $v['use']) {
                    $data[$date]['get2'] += intval($v['get'] + $v['pool_win'] - $v['use'] - $v['tax'] - $v['pooladd']);
                    $data[$date]['use2'] += 0;
                } else if ($v['get'] == $v['use']) {
                    $data[$date]['get1'] += intval($v['pool_win']);
                    $data[$date]['use1'] += 0;
                } else {
                    $data[$date]['get2'] += intval($v['pool_win']);
                    $data[$date]['use2'] += intval($v['use'] - $v['get']);
                }
                $data[$date]['pool_win2'] += intval($v['pool_win']);
                $data[$date]['sui2'] += intval($v['tax']);
                $data[$date]['pool2'] += intval($v['pooladd']);
            }
        }
        unset($logs);
        //=========================================获取红包数据
        $map2['c_time'] = array('between', array($b_time, $e_time));
        $map2['room'] = array('in', array(156, 157)); //156=试玩超级水果，157=娱乐超级水果
        $map2['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $get_list = D('redbaglog')->where($map2)->select();
        $result = array();
        foreach ($get_list as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            if ($v['room'] == 156)
                $data[$date]['redget156'] += intval($v['money']);
            else
                $data[$date]['redget157'] += intval($v['money']);
        }
        unset($get_list);

        //拉霸任务，宝箱获得数据
        /*$map['room'] = array('in', array(126, 127));
        $get_list = D('Goldget')->where($map)->select();
        $result = array();
        foreach ($get_list as $v){
            if(in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            $result[$date]['goldget'] += intval($v['gold']);
        }
        unset($get_list);*/
        //拉霸任务，周排行
        $mapzhou['room'] = 1011;
        $mapzhou['c_time'] = array('between', array($b_time, $e_time));
        $mapzhou['uid'] = array('egt', 150000); //排除AI,150000--150148不算
        $get_listzhou = D('Goldget')->where($mapzhou)->select();
        $resultzhou = array();
        foreach ($get_listzhou as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            $data[$date]['zhouget'] += intval($v['gold']);
        }
        unset($get_listzhou);

        foreach ($data as $k => $v) {
            $ret = array(
                'time' => strtotime($k),
                'zhouget' => intval($v['zhouget']), //周排行奖励
                'get1' => intval($v['get1']),
                'use1' => intval($v['use1']),
                'get2' => intval($v['get2']),
                'use2' => intval($v['use2']),
                'sui1' => intval($v['sui1']),
                'sui2' => intval($v['sui2']),
                'redget1' => intval($v['redget156']),
                'redget2' => intval($v['redget157']),
                'pool_win2' => intval($v['pool_win2']),
                'pool2' => intval($v['pool2']), //进入彩池的金额
                'pool_win1' => intval($v['pool_win1']),
                'pool1' => intval($v['pool1']), //进入彩池的金额
                'goldget' => intval($result[$k]['goldget']),
                'stat' => intval($v['get1'] + $v['get2'] - $v['use1'] - $v['use2'])
            );

            if ($info = $poolstat->where(array('time' => strtotime($k)))->find()) {
                $poolstat->where(array('_id' => $info['_id']))->save($ret);
            } else {
                $poolstat->add($ret);
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    public function poolstat()
    {
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        $poolstat = D('Superpoolstat');

        //水果游戏记录
        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $logs = D('Superfruitlog')->where($map)->select();
        $data = array();
        foreach ($logs as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);

            if (intval($v['test_type']) == 1) {
                // 1: 试玩
                $result = $v['get'] - abs($v['use']); // 获取金币结果
                if ($result > 0) {
                    // 说明： tax = $result * (platformRatio/100); pooladd = $result * (poolRatio/100);
                    $data[$date]['get1'] += $result - abs($v['tax']) - abs($v['pooladd'])  + $v['pool_win']; // 有概率从奖池获得部分金币
                    $data[$date]['use1'] += 0;
                } else {
                    // 
                    $data[$date]['get1'] += $v['pool_win'];
                    $data[$date]['use1'] += abs($result);
                }

                $data[$date]['pool_win1'] += intval($v['pool_win']); // 彩池产出
                $data[$date]['sui1'] += intval($v['tax']); // 平台纯税收
                $data[$date]['pool1'] += intval($v['pooladd']); // 彩池新增
            } else {
                // 2: 娱乐
                $result = $v['get'] - abs($v['use']); // 获取金币结果
                if ($result > 0) {
                    $data[$date]['get2'] += $result - abs($v['tax']) - abs($v['pooladd'])  + $v['pool_win']; // 有概率从奖池获得部分金币
                    $data[$date]['use2'] += 0;
                } else {
                    // 
                    $data[$date]['get2'] += $v['pool_win'];
                    $data[$date]['use2'] += abs($result);
                }

                $data[$date]['pool_win2'] += intval($v['pool_win']); // 彩池产出
                $data[$date]['sui2'] += intval($v['tax']); // 平台纯税收
                $data[$date]['pool2'] += intval($v['pooladd']); // 彩池新增
            }
        }
        unset($logs);
        //=========================================获取红包数据
        $map2['c_time'] = array('between', array($b_time, $e_time));
        $map2['room'] = array('in', array(156, 157)); //156=试玩超级水果，157=娱乐超级水果
        $map2['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $get_list = D('redbaglog')->where($map2)->select();
        $result = array();
        foreach ($get_list as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            if ($v['room'] == 156)
                $data[$date]['redget156'] += intval($v['money']);
            else
                $data[$date]['redget157'] += intval($v['money']);
        }
        unset($get_list);

        //拉霸任务，宝箱获得数据
        /*$map['room'] = array('in', array(126, 127));
        $get_list = D('Goldget')->where($map)->select();
        $result = array();
        foreach ($get_list as $v){
            if(in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            $result[$date]['goldget'] += intval($v['gold']);
        }
        unset($get_list);*/
        //拉霸任务，周排行
        $mapzhou['room'] = 1011;
        $mapzhou['c_time'] = array('between', array($b_time, $e_time));
        $mapzhou['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $get_listzhou = D('Goldget')->where($mapzhou)->select();
        $resultzhou = array();
        foreach ($get_listzhou as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);
            $data[$date]['zhouget'] += intval($v['gold']);
        }
        unset($get_listzhou);

        foreach ($data as $k => $v) {
            $ret = array(
                'time' => strtotime($k),

                'get1' => intval($v['get1']),
                'use1' => intval($v['use1']),
                'get2' => intval($v['get2']),
                'use2' => intval($v['use2']),
                'sui1' => intval($v['sui1']),
                'sui2' => intval($v['sui2']),
                'redget1' => intval($v['redget156']),
                'redget2' => intval($v['redget157']),
                'pool_win2' => intval($v['pool_win2']),
                'pool2' => intval($v['pool2']), //进入彩池的金额
                'pool_win1' => intval($v['pool_win1']),
                'pool1' => intval($v['pool1']), //进入彩池的金额
                'zhouget' => intval($v['zhouget']), //周排行奖励
                'goldget' => intval($result[$k]['goldget']),
                'stat' => intval($v['get1'] + $v['get2'] - $v['use1'] - $v['use2']),
            );

            if ($info = $poolstat->where(array('time' => strtotime($k)))->find()) {
                $poolstat->where(array('_id' => $info['_id']))->save($ret);
            } else {
                $poolstat->add($ret);
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    public function poolstat_upgrade()
    {
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $poolstat = D('Superpoolstat');
        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        $bTime = date('YmdHis', $b_time);
        $b_hour = $bTime % 1000000 - $bTime % 10000;
        $b_hour = $b_hour / 10000;

        $eTime = date('YmdHis', $e_time);
        $e_hour = $eTime % 1000000 - $eTime % 10000;
        $e_hour = $e_hour / 10000;

        echo "time1 ：" . $b_hour . "\n";
        echo "time2 : " . $e_hour . "\n";
        for ($i = $b_hour; $i <= $e_hour; $i++) {
            //
            if ($i < 10) {
                $real_b_time = date('Ymd', $b_time) . '0' . $i . "0000";
            } else {
                $real_b_time = date('Ymd', $b_time) . $i . "0000";
            }

            $real_b_time = strtotime($real_b_time);
            $real_e_time = $real_b_time + 3599;

            //水果游戏记录
            $map['c_time'] = array('between', array($real_b_time, $real_e_time));
            $map['uid'] = array('egt', 0); //排除AI,150000--150148不算
            $logs = D('Superfruitlog')->where($map)->select();
            $data = array();
            foreach ($logs as $v) {
                if (in_array($v['uid'], $uids_arr)) continue;
                $date = date('Y-m-d H:00:00', $v['c_time']);
                if (intval($v['test_type']) == 1) {
                    // 1: 试玩
                    $result = $v['get'] - abs($v['use']); // 获取金币结果
                    if ($result > 0) {
                        // 说明： tax = $result * (platformRatio/100); pooladd = $result * (poolRatio/100);
                        $data[$date]['get1'] += $result - abs($v['tax']) - abs($v['pooladd'])  + $v['pool_win']; // 有概率从奖池获得部分金币
                        $data[$date]['use1'] += 0;
                    } else {
                        // 
                        $data[$date]['get1'] += $v['pool_win'];
                        $data[$date]['use1'] += abs($result);
                    }

                    $data[$date]['pool_win1'] += intval($v['pool_win']); // 彩池产出
                    $data[$date]['sui1'] += intval($v['tax']); // 平台纯税收
                    $data[$date]['pool1'] += intval($v['pooladd']); // 彩池新增
                } else {
                    // 2: 娱乐
                    $result = $v['get'] - abs($v['use']); // 获取金币结果
                    if ($result > 0) {
                        $data[$date]['get2'] += $result - abs($v['tax']) - abs($v['pooladd'])  + $v['pool_win']; // 有概率从奖池获得部分金币
                        $data[$date]['use2'] += 0;
                    } else {
                        // 
                        $data[$date]['get2'] += $v['pool_win'];
                        $data[$date]['use2'] += abs($result);
                    }

                    $data[$date]['pool_win2'] += intval($v['pool_win']); // 彩池产出
                    $data[$date]['sui2'] += intval($v['tax']); // 平台纯税收
                    $data[$date]['pool2'] += intval($v['pooladd']); // 彩池新增
                }
            }
            unset($logs);
            //=========================================获取红包数据
            $map2['c_time'] = array('between', array($real_b_time, $real_e_time));
            $map2['room'] = array('in', array(156, 157)); //156=试玩超级水果，157=娱乐超级水果
            $map2['uid'] = array('egt', 0); //排除AI,150000--150148不算
            $get_list = D('redbaglog')->where($map2)->select();
            $result = array();
            foreach ($get_list as $v) {
                if (in_array($v['uid'], $uids_arr)) continue;
                $date = date('Y-m-d H:00:00', $v['c_time']);
                if ($v['room'] == 156)
                    $data[$date]['redget156'] += intval($v['money']);
                else
                    $data[$date]['redget157'] += intval($v['money']);
            }
            unset($get_list);

            //拉霸任务，周排行
            $mapzhou['room'] = 1011;
            $mapzhou['c_time'] = array('between', array($real_b_time, $real_e_time));
            $mapzhou['uid'] = array('egt', 0); //排除AI,150000--150148不算
            $get_listzhou = D('Goldget')->where($mapzhou)->select();
            foreach ($get_listzhou as $v) {
                if (in_array($v['uid'], $uids_arr)) continue;
                $date = date('Y-m-d H:00:00', $v['c_time']);
                $data[$date]['zhouget'] += intval($v['gold']);
            }
            unset($get_listzhou);


            foreach ($data as $k => $v) {
                $ret = array(
                    'time' => strtotime($k),
                    'get1' => intval($v['get1']),
                    'use1' => intval($v['use1']),
                    'get2' => intval($v['get2']),
                    'use2' => intval($v['use2']),
                    'sui1' => intval($v['sui1']),
                    'sui2' => intval($v['sui2']),
                    'redget1' => intval($v['redget156']),
                    'redget2' => intval($v['redget157']),
                    'pool_win2' => intval($v['pool_win2']),
                    'pool2' => intval($v['pool2']), //进入彩池的金额
                    'pool_win1' => intval($v['pool_win1']),
                    'pool1' => intval($v['pool1']), //进入彩池的金额
                    'zhouget' => intval($v['zhouget']), //周排行奖励
                    'goldget' => intval($result[$k]['goldget']),
                    'stat' => intval($v['get1'] + $v['get2'] - $v['use1'] - $v['use2']),
                );

                if ($oldData = $poolstat->where(array('time' => strtotime($k)))->find()) {
                    $poolstat->where(array('_id' => $oldData['_id']))->save($ret);
                } else {
                    $poolstat->add($ret);
                }
            }
        }

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }

    public function poolstat_upgrade_check()
    {
        $stime = microtime(true);

        $dtime = NOW_TIME;

        if ($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        } else {
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v) {
            $uids_arr[] = intval($v);
        }

        //水果游戏记录
        $map['c_time'] = array('between', array($b_time, $e_time));
        $map['uid'] = array('egt', 0); //排除AI,150000--150148不算
        $logs = D('Superfruitlog')->where($map)->select();
        $data = array();
        foreach ($logs as $v) {
            if (in_array($v['uid'], $uids_arr)) continue;
            $date = date('Y-m-d H:00:00', $v['c_time']);

            if (intval($v['test_type']) == 1) {
                // 1: 试玩
                $result = $v['get'] - abs($v['use']); // 获取金币结果
                if ($result > 0) {
                    // 说明： tax = $result * (platformRatio/100); pooladd = $result * (poolRatio/100);
                    $data[$date]['get1'] += $result - abs($v['tax']) - abs($v['pooladd'])  + $v['pool_win']; // 有概率从奖池获得部分金币
                    $data[$date]['use1'] += 0;
                } else {
                    // 
                    $data[$date]['get1'] += $v['pool_win'];
                    $data[$date]['use1'] += abs($result);
                }

                $data[$date]['pool_win1'] += intval($v['pool_win']); // 彩池产出
                $data[$date]['sui1'] += intval($v['tax']); // 平台纯税收
                $data[$date]['pool1'] += intval($v['pooladd']); // 彩池新增
            } else {
                // 2: 娱乐
                $result = $v['get'] - abs($v['use']); // 获取金币结果
                if ($result > 0) {
                    $data[$date]['get2'] += $result - abs($v['tax']) - abs($v['pooladd'])  + $v['pool_win']; // 有概率从奖池获得部分金币
                    $data[$date]['use2'] += 0;
                } else {
                    // 
                    $data[$date]['get2'] += $v['pool_win'];
                    $data[$date]['use2'] += abs($result);
                }

                $data[$date]['pool_win2'] += intval($v['pool_win']); // 彩池产出
                $data[$date]['sui2'] += intval($v['tax']); // 平台纯税收
                $data[$date]['pool2'] += intval($v['pooladd']); // 彩池新增
            }

        }
        unset($logs);

        echo json_encode($data) . "\n";

        echo 'SUCCESS' . PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times" . PHP_EOL;
    }
}
