<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/15
 * Time: 17:12
 */

namespace Admin\Controller;

use Admin\Controller\AdminController;
use Think\Log;

/**
 * 充值
 */
class PayController extends AdminController
{
    /**
     * 支付统计
     */
    public function payment()
    {
        $month = I('month');
        $years = I('years');
        if(!empty($month) && !empty($years)) {
            $b_time = I('b_time') ? strtotime(I('b_time')) : strtotime("{$years}-{$month}");
            $month = $month + 1;
            $e_time = I('e_time') ? strtotime(I('b_time')) : strtotime("{$years}-{$month}") - 1;
        } else {

            $b_time = I('b_time') ? strtotime(I('b_time')) : strtotime(date("Y-m-d", strtotime('-30 day')));
            $e_time = I('e_time') ? strtotime('+1 day' , strtotime(I('e_time'))) -1 : strtotime('+1 day') -1;
        }
        $b_time = date("Y-m-d",$b_time)." 00:00:00";
        $e_time = date("Y-m-d",$e_time)." 23:59:59";  
        //强制当月时间
        $b_time = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $e_time = date("Y-m-d H:i:s");
        $map['d_pays.created_at'] = array('between', array($b_time, $e_time));
        $map['d_pays.status'] = 1;
        //$map['d_pays.from_id'] = ['NEQ' , 1000]; //去掉官方渠道的 充值数据
        //$map['d_pays.pay_type'] = ['NEQ' , 9]; //去掉模拟充值
        //$map['d_pays.thn_id'] = ['NEQ' , 0]; //去掉 


        $paylog = D('DPays')->join('d_third_pays as B ON d_pays.pay_type=B.pay_type AND d_pays.thn_id=B.th_id ', 'left')
            ->where($map)
            ->field('d_pays.pay_type,thn_id,SUM(price) AS price,MAX(`desc`) AS sc,DATE(d_pays.created_at) as created_at ')
            ->group('d_pays.pay_type,thn_id,DATE(d_pays.created_at)')
            ->order('DATE(d_pays.created_at) desc')
            ->select();


        $ret =  array();
        foreach ($paylog as $k => $v) 
        {
            if(0==$v['thn_id']){$v['sc']="官方原生";} 
            $ret[] = $v;
        }
            
 
        $this->assign('_list', $ret);
        $this->display();
    }

    /**
     * 充值记录
     */
    public function paylog($p = 1)
    {
        $limit = 20;
        $pay = D('DPays');

        $type = intval(I('type'));
        $value = I('values');

        $query = ''; //排序条件

        if (isset($_GET['type']) && isset($_GET['values'])) {
            switch ($type) {
                case 0:
                    $map['d_pays.account'] = intval($value);
                    break;
                case 1:
                    $map['d_pays.account'] = D('DUsers')->get_uid('char_id', intval($value));
                    break;
                case 2:
                    $map['d_pays.account'] = D('DUsers')->get_uid('mobile', $value);
                    break;
                case 3:
                    $map['d_pays.account'] = D('DUsers')->get_uid('nickname', trim($value));
                    break;
            }
            $query .= 'type/' . $type . '/';
            $query .= 'values/' . $value . '/';
        }

        if (!isset($_GET['status'])) {
            $status = -1;
        } else {
            $status = intval(I('status'));
            if(1==$status){$map['d_pays.status'] = $status;}
            else{$map['d_pays.status'] =array('neq',1);}
            $query .= 'status/' . $status . '/';
        }
        $totalmap['d_pays.status'] = 1;
        $totalmap['d_pays.pay_type'] = ['NEQ' , 9]; //去掉模拟充值
      /*   if (!isset($_GET['devi'])) {
            $devi = -1;
        } else {
            $devi = intval(I('devi'));
            if ($devi == 0)
                $devi = "android";
            else  $devi = "ios";
            $map['devid'] = $devi;
            $query .= 'devi/' . $devi . '/';
        } */

        if (!isset($_GET['from_id'])) {
            $from_id = -1;
        } else {
            $from_id = intval(I('from_id'));
            $map['d_users.from_id'] = $from_id;
            $query .= 'from_id/' . $from_id . '/';
        }

        if (!isset($_GET['goods'])) {
            $goods_id = -1;
        } else {
            $goods_id = intval(I('goods'));
            $map['d_pays.saletype'] = $goods_id;
            $query .= 'goods/' . $goods_id . '/';
        }

        if (!isset($_GET['pay_type'])) {
            $pay_type = -1;
        } else {
            $pay_type = intval(I('pay_type'));
            $map['d_pays.pay_type'] = $pay_type;
            $query .= 'pay_type/' . $pay_type . '/';
        }

        $trade_no  = trim(I('trade_no'));
        if (!isset($_GET['ordertype'])) {
            $ordertype = -1;
        } else {
            $ordertype = intval(I('ordertype'));
            switch ($ordertype) {
                case 1:
                    $map['d_pays.trade_no'] = $trade_no;
                    break;
                case 2:
                    $map['d_pays.mch_trade_no'] = $trade_no;
                    break;
                case 3:
                    $map['d_pays.mch_order_no'] = $trade_no;
                    break;
            }
            $query .= 'ordertype/' . $ordertype . '/trade_no/' . $trade_no . '/';
        }

        if (isset($_GET['b_time'])) {
            $b_time = I('b_time');
            $map['d_pays.created_at'][] = array('egt', $b_time);
            $query .= 'b_time/' . date('Y-m-d', $b_time) . '/';
        }

        if (isset($_GET['e_time'])) {
            $e_time = date("Y-m-d H:i:s",(strtotime(I('e_time')) + 86399));
            $map['d_pays.created_at'][] = array('elt', $e_time);
            $query .= 'e_time/' . date('Y-m-d', $e_time) . '/';
        }

        //强制当月时间
        $b_time = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $e_time = date("Y-m-d H:i:s");
        $map['d_pays.created_at'] = array('between', array($b_time, $e_time));

        $orderby = trim(I('orderby'));
        $order   = trim(I('order'));
        if ($orderby != '' && $order != '') {
            $orders = $orderby . ' ' . $order;
        } else {
            $orders = 'd_pays.id desc';
        }
	
        $totals = $pay->where($map)->join('game1.d_users ON d_users.account = d_pays.account', 'left')->count();
        $list   = $pay->where($map)->join('game1.d_users ON d_users.account = d_pays.account', 'left')->field('d_pays.*,d_users.sub_from_id ')->page($p, $limit)->order($orders)->select();
    
        $uids = array();
        foreach ($list as $k => $v) {
            $uids[] = intval($v['account']);
			$list[$k]['uid']=$v['account'];
        }

        $users = D('DUsers')->getusers($uids);
        $userinfo = D('DUsers')->getUserInfo($uids);
        $list_count = $pay->where($map)->where($totalmap)->join('game1.d_users ON d_users.account = d_pays.account', 'left')->count(); //充值单数
        $uids = $pay->where($map)->where($totalmap)->join('game1.d_users ON d_users.account = d_pays.account', 'left')->field('count(DISTINCT(d_pays.account)) as num')->find();

        $pay_count  = $pay->where($map)->where($totalmap)->join('game1.d_users ON d_users.account = d_pays.account' , 'left')->sum('price'); //充值金额

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        //充值物品
        $goods = $this->recharge_goods();

        $this->assign('user_num', $uids['num']); //count(array_unique($uids))
        $this->assign('from_id', $from_id);
        $fromkey = D('DChannels')->field('from_id as id,from_time as num, from_name')->order('from_id asc')->select();
        $fromkeyList = [];

        foreach ($fromkey as $item) {
            $fromkeyList[$item['id'].$item['num']] = $item['from_name'];
        }
        //$list_count|number_format} &nbsp;&nbsp;&nbsp;人民币：{$pay_count|number_format} &nbsp;&nbsp;&nbsp;充值人数：{$user_num
        $this->assign('query', $query);
        $this->assign('from', $fromkey);
        $this->assign('fromList', $fromkeyList);
        $this->assign('goods_id', $goods_id);
        $this->assign('userinfo', $userinfo);
        $this->assign('list_count', $list_count);
        $this->assign('pay_count', $pay_count);
        $this->assign('goods', $goods);
        $this->assign('_list', $list);
        $this->assign('device', C('DEVICE'));
        $this->assign('status', $status);
        $this->assign('users', $users);
        $this->assign('devi', $devi);
        $this->assign('ordertype', $ordertype);
        $this->assign('pay_type', $pay_type);
        $this->assign('pay_types', $this->pay_types());
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 充值统计
     */
    public function pays()
    {
        $pay = D('DPays');

        $data = $this->_calculatetime(); //计算开始结束时间
        $b_time = $data['b_time'];
        $e_time = $data['e_time'];
        
        //强制当月时间
        $b_time = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $e_time = date("Y-m-d H:i:s");
        $map['created_at'] = array('between', array($b_time, $e_time));
        $map['status'] = 1;
        $map['from_id'] = ['NEQ' , 1000]; //去掉官方渠道的 充值数据
        $map['pay_type'] = ['NEQ' , 9]; //去掉模拟充值
        $paylog = $pay->where($map)->select();
        //var_dump($pay->getLastSql());
        $ret = $uids = array();
        $pay_count = $list_count = 0;

        foreach ($paylog as $k => $v) {
            $date = strtotime(date('Y-m-d', strtotime($v['created_at'])));
            $ret[$date]['money'] += intval($v['price']);
            $ret[$date]['recharge_num'] += 1;
            $ret[$date]['userarr'][] = $v['account'];
            $uids[] = $v['account'];
            $pay_count += intval($v['price']);
            $list_count += 1;
        }

        foreach ($ret as $k => $v) {
            $ret[$k]['users'] = count($v['userarr']);
            $ret[$k]['uique_users'] = count(array_unique($v['userarr']));
        }
        krsort($ret);

        $this->assign('user_num', count(array_unique($uids)));
        $this->assign('list_count', $list_count);
        $this->assign('pay_count', $pay_count);
        $this->assign('_list', $ret);
        $this->display();
    }

    /*private function _paycount($time, $obj){
        $b_time = $time;
        $e_time = $time + 86399;

        $pay = $obj;
        $map['addtime'] = array('between', array($b_time, $e_time));
        $map['state'] = 3;

        $paylog = $pay->where($map)->select();

        $money = $recharge_num = $users = $uique_users = 0;
        $userarr = array();
        foreach ($paylog as $k=>$v){
            $money += $v['money'];
            $recharge_num += 1;
            $userarr[] = $v['uid'];
        }

        $data['money'] = $money;
        $data['recharge_num'] = $recharge_num;
        $data['users'] = count($userarr);
        $data['uique_users'] = count(array_unique($userarr));

        return $data;
    }*/

    /**
     * 充值统计详细
     */
    public function paycount_detail($date)
    {
        $b_time = $date;
        $e_time = $b_time + 86399;

        $Paylog = D('Paylog');

        $map['addtime'] = array('between', array($b_time, $e_time));
        $map['state'] = 3;
        $paylogs = $Paylog->where($map)->field('uid,money,addtime')->select();

        $data = array();
        foreach ($paylogs as $v) {
            $dt = date('Y-m-d H:00:00', $v['addtime']);
            $data[$dt]['money'] += $v['money'];
            $data[$dt]['num'] += 1;
            $data[$dt]['men'][] = $v['uid'];
        }

        foreach ($data as $k => $v) {
            $data[$k]['men'] = count(array_unique($v['men']));
        }

        $this->assign('_list', $data);
        $this->display();
    }

    /**
     * 计算开始结束时间
     */
    protected function _calculatetime()
    {

        if (isset($_GET['type']) && isset($_GET['month'])) {
            $type = intval(I('type'));
            if ($type == 0) { //按月
                $month = intval(I('month'));
                $monarr = monthday(NOW_TIME, $month);

                $b_time = strtotime($monarr[0]);
                $now_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME)); //现在时间
                $e_time = strtotime($monarr[1]);
                if ($now_time < $e_time) {
                    $e_time = $now_time;
                }
            } elseif ($type == 1) { //按天数
                $b_time = strtotime(I('b_time'));
                $e_time = strtotime(I('e_time'));

                if ($b_time == '' || $e_time == '') {
                    $this->error('请选择时间');
                }
            }
        } else { //默认当月数据
            $b_time = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y"))));
            $e_time = NOW_TIME;
        }
        
        $data['b_time'] = date("Y-m-d",$b_time)." 00:00:00";
        $data['e_time'] = date("Y-m-d",$e_time)." 23:59:59";

        return $data;
    }

    /**
     * 计算开始结束时间
     */
    protected function _calculatetimemore()
    {

        if (isset($_GET['type'])) {
            $type = intval(I('type'));
            if ($type == 0) { //按月
                $month = intval(I('month'));
                $monarr = monthday(NOW_TIME, $month);

                $b_time = strtotime($monarr[0]);
                $now_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME)); //现在时间
                $e_time = strtotime($monarr[1]);
                if ($now_time < $e_time) {
                    $e_time = $now_time;
                }
            } elseif ($type == 1) { //按天数
                $b_time = strtotime(I('b_time'));
                $e_time = strtotime(I('e_time'));

                if ($b_time == '' || $e_time == '') {
                    $this->error('请选择时间');
                }
            }
        } else { //默认当天数据
            $b_time = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y"))));
            $e_time = NOW_TIME;
        }
        
        $data['b_time'] = date("Y-m-d",$b_time)." 00:00:00";
        $data['e_time'] = date("Y-m-d",$e_time)." 23:59:59";

        return $data;
    }

    /**
     * 模拟充值
     */
    public function monipay()
    {
        //充值物品
        $goods = $this->recharge_goods(1);
		
		$goodsList = [];		
        foreach ($goods as $item) {
            $goodsList[$item['id']]['money'] = $item['money'];
        }
		
        //print($goods[100001]['money']);
        if (IS_POST) {
            $nametype = intval(I('post.nametype'));
            $name = I('post.name');
            $shop_id = intval(I('post.shop_id'));
            $uquid = uniqid(md5(microtime(true)), true);
            $ordersn = 'moni' . date('YmdHis') . substr(md5($uquid), 8, 16);

            // var_dump(I(''));exit;
            switch ($nametype) {
                case 0:
                { //角色ID
                    $uid = D('DUsers')->get_uid('char_id', intval($name));
					$userid = intval($name);
                    break;
                }
                case 1:
                { //用户ID
                    $uid = intval($name);
					$userid = D('DUsers')->get_charid('uid', intval($name));
                    break;
                }
                case 2:
                { //手机号
                    $uid = D('DUsers')->get_uid('mobile', $name);
					$userid = D('DUsers')->get_charid('mobile', $name);
                    break;
                }
                case 3:
                { //玩家昵称
                    $uid = D('DUsers')->get_uid('nickname', trim($name));
					$userid = D('DUsers')->get_charid('nickname', $name);
                    break;
                }
            }

            if (intval($uid) == 0) $this->error('用户不存在');
            /*
            $arr['order_id'] = $ordersn;
            $arr['money'] = 'rmb';
            $arr['server_id'] = 1;
            $arr['role_id'] = $uid;
            $arr['call_back'] = "$shop_id,niuniu";
            $arr['openid'] = 1;
            $arr['order_status'] = 1;
            $arr['pay_type'] = '';
            $arr['pay_time'] = '';
            $arr['chn_id'] = '';
            $arr['sub_chn_id'] = '';
            $arr['remark'] = '';
            // pay_Logs('===========   $shop_id:'.$shop_id);

            $field = http_build_query($arr);
            $url = C('GM_URL') . 'cgi-bin/pay_handle:delivery';
            $field = str_replace('%2C', ',', $field);

            //记录日志
            \Think\Log::write('MONI_PAY_URL: ' . $url . '?' . $field, 'INFO');
            // var_dump($url . '?' . $field);exit;

            $res = PostUrl($url, $field, 0);
            $arrs = json_decode($res, true);


            if (true) {   
                //post 支付信息到状态服务器
                $urlStatus = 'http://' . C('WEB_SERVER_IP') . ':' . C('WEB_SERVER_PORT') . '/index.php?s=/api/paylog';
                $receiptStatus['uid'] = $uid;
                $receiptStatus['money'] = intval($goodsList[$shop_id]['money']);
                $receiptStatus['saletype'] = 'monipay';
                $receiptStatus['time'] = NOW_TIME;
                $fieldStatus = http_build_query($receiptStatus);
                PostUrl2Status($urlStatus, $fieldStatus);
                //
            }
            */
            $data['account'] = $uid;
			$data['uid'] = $userid;
            $data['ordersn'] = $ordersn;
            $data['money'] = intval($goodsList[$shop_id]['money']);
            $data['shop_id'] = $shop_id;
            $data['user_id'] = is_login();
            $data['create_time'] = NOW_TIME;
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $ret = D('DMonipay')->add($data);

            if ($ret) {
                
                //发送给游戏服务端
                $post_url = C('GMBox_URL') .'cgi-bin/gm_oprate.recharge';
                $post_data['sign'] = md5(C('SENDGAME_KEY').time());
                $post_data['timestamp'] = intval(time());
                $post_data['account'] =  intval($uid);
                $post_data['shop_id'] = intval($shop_id);
                $field = json_encode($post_data);

                $rets = PostUrl($post_url, $field,1);

                $this->success('充值成功');
            } else {
                $this->error('充值失败');
            }

        } else {
            $this->assign('goods', $goods);
            $this->display();
        }
    }

    /**
     * 模拟充值日志
     */
    public function monipaylog($p = 1)
    {
        $limit = 20;
        $monipay = D('DMonipay');

        if (isset($_GET['goods_id'])) {
            $goods_id = intval(I('goods_id'));
            $map['shop_id'] = $goods_id;
        } else {
            $goods_id = -1;
        }

        $type = intval(I('type'));
        $value = I('value');

        if (isset($_GET['type']) && isset($_GET['value'])) {
            switch ($type) {
                case 0:   //角色ID
                    $map['account'] = intval($value);
                    break;
                case 1:  //用户ID
                    $map['account'] = D('DUsers')->get_uid('char_id', $value);
                    break;
                case 2:
                    $map['account'] = D('DUsers')->get_uid('mobile', $value);
                    break;
                case 3:
                    $map['account'] = D('DUsers')->get_uid('nickname', trim($value));
                    break;
            }
        }
        //print_r($map);
        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $map['create_time'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = (strtotime(I('e_time')) + 86399);
            $map['create_time'][] = array('elt', $e_time);
        }

        $totals = $monipay->where($map)->count();
        $list   = $monipay->where($map)->page($p, $limit)->order('id desc')->select();


        $ids = array();
        foreach ($list as $k => $v) {
            $list[$k]['user_id'] = D('Member')->getNickName($v['user_id']); //z操作员
			
            $ids[] = intval($v['account']);
        }
		//print_r(D('DUsers')->getUserInfo($ids));

        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $goods = $this->recharge_goods();
		
        $this->assign('user', D('DUsers')->getUserInfo($ids));
        $this->assign('goods_id', $goods_id);
        $this->assign('goods', $goods);
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    /**
     * 充值档位
     */
    protected function recharge_goods($type=0)
    {
        $rechargeConfig = D('ConfigByshop')->getConfig();

        $goods = array();
        if($type == 1) {
            $goods = $rechargeConfig;
        } else {
            foreach ($rechargeConfig as $k => $v) {
                $goods[$v['id']] = $v['name'];
            }
        }

        return $goods;
    }

    /*每小时充值统计*/
    public function hourspay()
    {

        if (isset($_GET['b_time'])) {
            $b_time = strtotime(I('b_time'));
            $today = $b_time + 6 * 86400;
        } else {
            $today = strtotime(date('Y-m-d', NOW_TIME));
            $b_time = $today - 6 * 86400;
        }

        $map['time'] = array('between', array($b_time, $today));
        $list = D('Hourspaystat')->where($map)->order('time desc')->select();

        $time = array();
        for ($i = 0; $i <= 23; $i++) {
            if ($i < 10) {
                $time[] = "0$i:00";
            } else {
                $time[] = "$i:00";
            }
        }

        $ret = array();
        foreach ($list as $v) {
            $date = date('Y-m-d', $v['time']);
            unset($v['time'], $v['_id']);
            $ret[$date] = $v;
        }

        $this->assign('ret', $ret);
        $this->assign('time', $time);
        $this->display();
    }

    /**
     * 充值统计
     */
    public function income()
    {
        $pay = D('DPays');
        $redpackages = D('DRedPackages');
       
        $month = I('month');
        $years = I('years');
        if(!empty($month) && !empty($years)) {
            $b_time = I('b_time') ? strtotime(I('b_time')) : strtotime("{$years}-{$month}");
            $month = $month + 1;
            $e_time = I('e_time') ? strtotime(I('b_time')) : strtotime("{$years}-{$month}") - 1;
        } else {

            $b_time = I('b_time') ? strtotime(I('b_time')) : strtotime(date("Y-m-d", strtotime('-30 day')));
            $e_time = I('e_time') ? strtotime('+1 day' , strtotime(I('e_time'))) -1 : strtotime('+1 day') -1;
        }
        $b_time = date("Y-m-d",$b_time)." 00:00:00";
        $e_time = date("Y-m-d",$e_time)." 23:59:59";
        //var_dump($b_time, $e_time);exit;

        $map['d_pays.created_at'] = array('between', array($b_time, $e_time));
        $redmap['created_at'] = array('between', array($b_time, $e_time));
        $redmap['status'] = ['in' , [0,888]];
        $map['d_pays.status'] = ['in' , [1,2,32]];
        $limit = 10;
        $page = I('page');
        $page = !empty($page) ? $page : 1;
        $paylog = [];
        //$map['from_id'] = ['not in' , [10000, 998]]; //去掉官方渠道的 充值数据
        $map['d_pays.from_id'] = ['NEQ' , 1000]; //去掉官方渠道的 充值数据
        $map['d_pays.pay_type'] = ['NEQ' , 9]; //去掉模拟充值
        // $paylog = $pay->where($map)->join("game1.d_users on d_users.account = d_pays.account")->field('d_pays.pay_type,d_pays.from_id,d_pays.from_time, d_pays.price as money, d_pays.created_at')->order('d_pays.created_at desc')->select();
        // var_dump($pay->getLastSql());
        $paylog = $pay->where($map)->join("game1.d_users on d_users.account = d_pays.account")
        ->field('d_pays.pay_type,SUM(d_pays.price) as money, DATE_FORMAT(d_pays.created_at, "%Y-%m-%d") as created_at')
        ->group('d_pays.pay_type,created_at')->order('d_pays.created_at desc')->select();

        $redpackageslog =$redpackages->where($redmap)
        ->field('ROUND(SUM(amount)/100,2) as money, DATE_FORMAT(created_at, "%Y-%m-%d") as created_at')
        ->group('DATE_FORMAT(created_at, "%Y-%m-%d")')->select();

        $redpackageslogData = [];
        foreach ($redpackageslog as $val) {
            $redpackageslogData[$val['created_at']] = $val['money'] ;
        }

        $ret = [];
        $tasks = D('DTaskCost');

        //print_r($paylog);
        //foreach ($paylog as $k => $v) {
        //    $date = date('Y-m-d', strtotime($v['created_at']));
        //    $ret[$date]['money_'.$v['pay_type']] += $v['money']; //10微信 2支付宝充值 ,13米花充值
        //    $ret[$date]['total'] += intval($v['money']);  //充值总额
        //    $ret[$date]['all_consume'] = $tasks->getchannelconsumenew($date);
        //    $ret[$date]['red_consume'] = $redpackageslogData[$date]?$redpackageslogData[$date]:0; //红包兑换
        //    $ret[$date]['all_consume'] = $ret[$date]['all_consume']; //渠道消耗
        //}

        $b_time_n = strtotime($b_time); // 假设开始时间戳
        $e_time_n = strtotime('-1 day' ,strtotime($e_time)); // 假设结束时间戳
        $current_time = $e_time_n; // 设置当前时间为开始时间

        while ($current_time >= $b_time_n) 
        {
            //判断当前日期是否有充值记录
            $date = date('Y-m-d', $current_time);
            $riqibaohan=1;
            foreach ($paylog as $k => $v) {
                $pdate = date('Y-m-d', strtotime($v['created_at']));
                if($date==$pdate)
                {
                    $riqibaohan=2;
                    $newpaytype=9;
                    if($v['pay_type']!=9){if($v['pay_type']%2==1){$newpaytype=1;}else{$newpaytype=2;}}
                    $ret[$date]['money_'.$newpaytype] += $v['money']; //10微信 2支付宝充值 ,13米花充值
                    $ret[$date]['total'] += intval($v['money']);  //充值总额
                    $ret[$date]['all_consume'] = $tasks->getchannelconsumenew($date);//渠道消耗
                    $ret[$date]['all_chongfan'] = D('DUserDayStats')->getchongfan($date);//渠道消耗
                    $ret[$date]['red_consume'] = $redpackageslogData[$date]?$redpackageslogData[$date]:0; //红包兑换
                }
            }
            // 当天没充值处理
            if($riqibaohan==1)
            {
                $ret[$date]['money_1'] += 0; //10微信 2支付宝充值 ,13米花充值
                $ret[$date]['money_2'] += 0; //10微信 2支付宝充值 ,13米花充值
                $ret[$date]['total'] += intval(0);  //充值总额
                $ret[$date]['all_consume'] = $tasks->getchannelconsumenew($date);//渠道消耗
                $ret[$date]['all_chongfan'] = D('DUserDayStats')->getchongfan($date);//渠道消耗
                $ret[$date]['red_consume'] = $redpackageslogData[$date]?$redpackageslogData[$date]:0; //红包兑换
            }

            $current_time = strtotime("-1 day", $current_time); // 增加一天
        }




        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $ret);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }

    //批量充值界面
    public function batchPay()
    {
        if (isset($_GET['b_time'])) {
            $b_time = I('b_time');
            $map['d_users.created_at'][] = array('egt', $b_time);
            $query .= 'b_time/' . date('Y-m-d', $b_time) . '/';
        }

        if (isset($_GET['e_time'])) {
            $e_time = I('e_time');
            $map['d_users.created_at'][] = array('elt', $e_time);
            $query .= 'e_time/' . date('Y-m-d', $e_time) . '/';
        }


        $limit = 20;
        $pay = D('DPays');

        $type = intval(I('type'));
        $value = I('value');

        $query = ''; //排序条件

        $state = 1;
        $map['d_pays.status'] = $state;
        $query .= 'state/' . $state . '/';




        if(!empty($_GET['b_time']) && !empty(isset($_GET['e_time']))) {
            $orderby = trim(I('orderby'));
            $order   = trim(I('order'));
            if ($orderby != '' && $order != '') {
                $orders = $orderby . ' ' . $order;
            } else {
                $orders = 'id desc';
            }

            $totals = $pay->where($map)
            ->join('game1.d_users ON d_users.account = d_pays.account')
            ->count();
            // var_dump($pay->getLastSql());exit;

            $list   = $pay->where($map)
            ->join('game1.d_users ON d_users.account = d_pays.account')
            ->field('d_pays.*,d_users.from_id,d_users.from_time,d_users.name as uname')
            ->page($p, $limit)
            ->order($orders)->select();
            //var_dump($pay->getLastSql());

            $uids = array();
            foreach ($list as $v) {
                $uids[] = intval($v['uid']);
            }

            $users = D('DUsers')->getusers($uids);
            $userinfo = D('DUsers')->getUserInfo($uids, 'uid');
            $list_count = $totals; //充值单数
            $uids = $pay->where($map)
            ->join('game1.d_users ON d_users.account = d_pays.account')
            ->getField('account', true);
            $pay_count  = $pay->where($map)
            ->join('game1.d_users ON d_users.account = d_pays.account')
            ->sum('price'); //充值金额

            $pageNav = new \Think\Page($totals, $limit);
            $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

            //充值物品
            $goods = $this->recharge_goods();

            $this->assign('user_num', count(array_unique($uids)));
            $this->assign('from_id', $from_id);
            $fromkey = D('DChannels')->field('from_id as id,from_time as num, from_name')->order('from_id asc')->select();
            $fromkeyList = [];
            foreach ($fromkey as $item) {
                $fromkeyList[$item['id'].$item['num']] = $item['from_name'];
            }
            // var_dump($fromkeyList);

            $this->assign('query', $query);
            $this->assign('query', $query);
            $this->assign('from', $fromkey);
            $this->assign('fromList', $fromkeyList);
            $this->assign('goods_id', $goods_id);
            $this->assign('userinfo', $userinfo);
            $this->assign('list_count', $list_count);
            $this->assign('pay_count', $pay_count);
            $this->assign('goods', $goods);
            $this->assign('_list', $list);
            $this->assign('device', C('DEVICE'));
            $this->assign('state', $state);
            $this->assign('users', $users);
            $this->assign('devi', $devi);
            $this->assign('paytype', $paytype);
            $this->assign('pay_types', $this->pay_types());
            $this->assign('_page', $pageNav->show());
        }



        $this->display();
    }

    //批量充值处理
    public function batchPayHandle()
    {

        if (isset($_GET['b_time'])) {
            $b_time = I('b_time');
            $map['created_at'][] = array('egt', $b_time);
        }

        if (isset($_GET['e_time'])) {
            $e_time = I('e_time');
            $map['created_at'][] = array('elt', $e_time);
        }
        if(empty($b_time) || empty($e_time)) {
            $this->error('开始时间和结束时间必填');
        }

        $page = I('page') ? I('page') : 1;
        $limt = 100;
        $map = ['created_at' => ['between' ,[$b_time,$e_time]], 'status' => 1 ];
        $total = D('DPays')->where($map)->count();
        $list = D('DPays')->where($map)->page($page, $limt)->order('created_at')->select();
        // var_dump($list);exit;


        if(count($list) == 0) {
            $this->success('成功', U('pay/batchPay'));
        }

        foreach ($list as $val) {

            $uid = $val['uid'];
            $saletype = $val['saletype'];
            $ordersn = $val['ordersn'];

            $arr['order_id']     = $ordersn;
            $arr['money']        = 'rmb';
            $arr['server_id']    = 1;
            $arr['role_id']      = $uid;
            $arr['call_back']    = "$saletype,niuniu";
            $arr['openid']       = 1;
            $arr['order_status'] = 1;
            $arr['pay_type']     = '';
            $arr['pay_time']     = '';
            $arr['chn_id']       = '';
            $arr['sub_chn_id']   = '';
            $arr['remark']      = '';
            $field = http_build_query($arr);
            $url = C('GM_URL') . 'cgi-bin/pay_handle:delivery';
            $field = str_replace('%2C', ',', $field);



            $res = PostUrl($url, $field, 0);
            $arrs = json_decode($res, true);
        }
        $page = $page + 1;

        $this->success('成功', U('pay/batchPayHandle?page='.$page));

    }

    //零钱到账
    public function changeToAccount()
    {
        /* $res = include_once( dirname(__FILE__).'/../Lib/WxChatPay.php' );
        $app = new \WxChatPay();
        $res = $app->paytest();
        print_r($res);
        exit;
        */
        $model = M('DibLog');
        if($_POST) {
            //接口
            $uid = I('uid');
            $user = D('DUsers')->where(['id' => $uid])->find();
            if(!$user) {
                $this->error("用户ID不存在");exit;
            }

            $opne_id = $user['sdk_uid'];//$user['openid'];
            $amount = I('amount');


            $res = include_once( dirname(__FILE__).'/../Lib/WxWithdraw.php' );
            $app = new \Withdraw();
            $res = $app->transfer($opne_id, date("YmdHis").$app->createNonceStr(), $amount);
          

            //记录日志
            $model->add([
                'uid' => $uid,
                'user_name' => $user['name'],
                'openid' => $opne_id,
                'amount' => $amount,
                'return' => json_encode($res),
                'addtime' => time(),
                'admin' => $_SESSION['username'],
            ]);
            $this->success('成功');exit;
        }
        $page = I('p') ? I('p') : 1;
        $limit = 10;

        $total = $model->count();
        $list = $model->where($map)->page($page, $limit)->order('addtime desc')->select();

        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');

        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->display();
    }
    /**
     * 充值返利
     */
    public function paysreward()
    {
        $pay = D('DUserDayStats');
        
        if (isset($_GET['b_time']) && isset($_GET['e_time'])) 
        {
            $b_time = I('b_time');
            $e_time = I('e_time');
            $map['updated_at'] = array('between', array($b_time, $e_time));
        }
		else
		{
			$b_time = date("Y-m-d", strtotime('-30 day'));
            $e_time = date("Y-m-d", strtotime('+1 day'));
			$map['updated_at'] = array('between', array($b_time, $e_time));
		}
		$map['d_user_day_stats.from_id'] = ['NEQ' , 1000]; 
        $list   = $pay->where($map)->join('game1.d_channels as B ON B.from_id = d_user_day_stats.from_id AND B.from_time = d_user_day_stats.from_time', 'left')
        ->group('d_user_day_stats.from_time,d_user_day_stats.from_id')
        ->field('d_user_day_stats.from_id ,d_user_day_stats.from_time, MAX(B.from_name) as from_name, SUM(d_user_day_stats.pays) as pays,SUM(d_user_day_stats.red_packages) as red_package,SUM(d_user_day_stats.pays_reward) as pays_reward')
        ->order('d_user_day_stats.from_id,d_user_day_stats.from_time')
        ->select();
        
        $pays_sum = 0;
        $red_packages_sum = 0;
        $pays_reward_sum = 0;
        
        foreach ($list as $k => $v) { 
            $pays_sum += doubleval($v['pays']);
            $red_packages_sum += doubleval($v['red_packages']);
            $pays_reward_sum += doubleval($v['pays_reward']);
        }
 
        $this->assign('pays_sum', $pays_sum);
        $this->assign('red_packages_sum', $red_packages_sum);
        $this->assign('pays_reward_sum', $pays_reward_sum);
        $this->assign('_list', $list);
        $this->display();
    }
}
