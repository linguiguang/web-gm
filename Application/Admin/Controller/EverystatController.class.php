<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/5
 * Time: 15:51
 */
namespace Admin\Controller;
use Think\Controller;

//记录玩家每天的输赢统计
class EverystatController extends Controller{

    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;
        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $data = $userNums = array();
        $map['c_time'] = array('between', array($b_time, $e_time));
		//统计今日充值记录
		/*
		 $changemap['paytime'] = array('between', array($b_time, $e_time));
		 $changemap['state'] = 3;
		 $changeGet = D('Paylog')->where($changemap)->select();
        foreach ($changeGet as $v){
			
			  $data[$v['uid']]['money'] += $v['money']; 
			    $data[$v['uid']]['from_id'] = $v['from_id']; 
        }
		//echo json_encode($changeGet).PHP_EOL;
        unset($changeGet);
        echo 'money ok'.PHP_EOL;		
        //从gold表获取数据
		*/
/*
        //1、拉霸宝箱，任务金币,看牌抢庄免费领取金币
        $map['room'] = array('in', array(103,109,105,110,111,112,116,117,118,126, 127, 141, 144, 148,149,153,1007,1008,1020));
        $goldGet = D('Goldget')->where($map)->select();
        foreach ($goldGet as $v){
			if($v['room']==153)
			{
			 $data[$v['uid']]['redtogold'] += $v['gold']; //红包兑换金币
			}
			  $data[$v['uid']]['gold_free'] += $v['gold']; 
        }
        unset($goldGet);
        echo 'get ok'.PHP_EOL;
*/
        //从diamond表获取数据
/*
        //1、免费获得钥匙，和兑换钥匙
        $map['room'] = array('in', array(136, 137, 142, 143, 145, 146, 150, 151));
        $diamonds = D('Diamodget')->where($map)->select();
        foreach ($diamonds as $v){
          
			  $data[$v['uid']]['dj_free'] += $v['diamond'];
        }
		unset($diamonds);*/
        //1、兑换实物.
		/*
        $map['room'] = array('in', array(113));
        $redmoney = D('Redbaglog')->where($map)->select();
        foreach ($redmoney as $v){
          
			  $data[$v['uid']]['redtomoney'] += $v['money'];
        }
		unset($redmoney);
        //存储
		*/
		/*
        $Everydaygoldstat = D('Everydaygoldstat');
        foreach ($data as $k=>$v) { 
           $fromret=$ret = $eap = array();
            $eap['time'] = $b_time;
            $eap['uid'] = $k;
            $info = $Everydaygoldstat->where($eap)->find();
            $ret[' $fromret['from_id']'] = $b_time;
            $ret['uid'] = $k;
			//echo json_encode($k).PHP_EOL;
			
			//echo json_encode($info).PHP_EOL;
			//更新每日输赢统计表 开始
				$ret['redtomoney'] = $data[$k]['redtomoney'];//红包兑换实物
				$ret['yinli'] = intval( $info['yinli1']+$info['yinli2']+  $data[$k]['gold_free']);
				$ret['gold_free'] = $data[$k]['gold_free'];
				$ret['dj_free'] = $data[$k]['dj_free'];
				$ret['money'] = $data[$k]['money'];
				$ret['redtogold'] = $data[$k]['redtogold'];//红包兑换金币
				$ret['from_id'] =$data[$k]['from_id'];//渠道ID
				
            if ($info) {
                $Everydaygoldstat->where(array('_id' => $info['_id']))->save($ret);
            } else {
                $Everydaygoldstat->add($ret);
            }
			//更新每日输赢统计表 结束
			
			 $fromret['uid'] = $k;
			 $fromret['from_id']= $data[$k]['from_id'];//渠道ID
			 $fromret['time'] = strtotime('now'));
			  $fromret['yinli1']
			 
			
			

        }
*/
        //统计每天每个游戏人数
        $everydayUserNum = D('Everydayusernum');
        $roomUserNum['time'] = $b_time;
        foreach ($statAreas as $room){
            $roomUserNum[$room] = count($userNums[$room]);
        }
        if($userNumInfo = $everydayUserNum->where(array('time'=>$b_time))->find()){
            $everydayUserNum->where(array('_id'=>$userNumInfo['_id']))->save($roomUserNum);
        }else{
            $everydayUserNum->add($roomUserNum);
        }

        $etime = microtime(true);
        $totals = round($etime - $stime);
        echo "Run $totals S\n";
		
		
		
		
    }

    //纯输赢
    protected function operate_data($get, $use){
        if($get > $use){
            $data['get'] = $get - $use;
            $data['use'] = 0;
        }else{
            $data['get'] = 0;
            $data['use'] = $use - $get;
        }
        return $data;
    }

}