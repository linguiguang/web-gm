<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 13:56
 */
namespace Admin\Controller;
use Think\Controller;

class RedroomstatController extends Controller{

    /**
     * 红包场钥匙半小时统计一次
     */
    public function stat(){
        $stime = microtime(true);

        $dtime = NOW_TIME;
        if($yes_time = stat_zero_time($dtime)) {
            $b_time = $yes_time['b_time'];
            $e_time = $yes_time['e_time'];
        }else{
            $b_time = strtotime(date('Y-m-d 00:00:00', $dtime));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));
        }

        $Users = D('Users');
        $uap['inside'] = 1;
        $uids = $Users->where($uap)->getField('id', true);
        $uids_arr = array();
        foreach ($uids as $v){
            $uids_arr[] = intval($v);
        }

        $Qrredstat = D('Qrredstat');
        /*$bb_time = strtotime('2017-11-01');
        $ee_time = strtotime('2017-12-09');
        for ($i=0;$i<(($ee_time-$bb_time)/86400);$i++) {

            $b_time = $bb_time + $i*86400;
            $e_time = $b_time + 86399;
            echo date('Y-m-d', $e_time).PHP_EOL;*/

            $map['c_time'] = array('between', array($b_time, $e_time));
            $map['isrobot'] = 0;
            $logs = D('Qrredbag')->where($map)->select();

            $data = array();
            foreach ($logs as $v) {
                if (in_array($v['uid'], $uids_arr)) continue;
                $date = date('Y-m-d H:00:00', $v['c_time']);
                $data[$date]['get'] += $v['get'];
                $data[$date]['use'] += $v['use'];
                $data[$date]['sui'] += intval($v['tax']); 
				$data[$date]['condia'] += intval($v['condia']);
                if(in_array($v['redbag'], array(61, 64))){//福袋送的红包
                    $data[$date]['fd_redbag'] += 60;
                    $data[$date]['redbag'] += intval($v['redbag'])-60;
                }else{
                    $data[$date]['redbag'] += intval($v['redbag']);
                }
            }

            foreach ($data as $k => $v) {
                $ret['time'] = strtotime($k);
                $ret['get'] = intval($v['get']);
                $ret['use'] = intval($v['use']);
                $ret['sui'] = intval($v['sui']);
				$ret['redbag'] = intval($v['redbag']);
                $ret['condia'] = intval($v['condia']);
                $ret['fd_redbag'] = intval($v['fd_redbag']);
                $ret['stat'] = intval($v['get'] - $v['use']);

                if ($info = $Qrredstat->where(array('time' => strtotime($k)))->find()) {
                    $Qrredstat->where(array('_id' => $info['_id']))->save($ret);
                } else {
                    $Qrredstat->add($ret);
                }
                unset($ret);
            }
//        }

        echo 'SUCCESS'.PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times".PHP_EOL;
    }

    /**
     * 金币消耗和统计，修复数据方法
     */
    protected function operate_data($get, $use){

        if($get > $use){
            $data['get'] = $get - $use;
            $data['use'] = 0;
        }else{
            $data['get'] = 0;
            $data['use'] = $use - $get;
        }

        return $data;
    }

}