<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/15
 * Time: 15:18
 */

namespace Admin\Controller;

use Admin\Controller\AdminController;


/**
 * 渠道配置
 */
class ReportController extends AdminController
{



    /**
     * 渠道活动
     */
    public function fromkey($p = 1)
    {


        $limit = 30;
        $p = I('p');
        $model = D('DChannelActivities');

        $total = $model->count();
        $list = $model->page($p, $limit)->order('id desc')->select();
         
        foreach ($list as $k => $v) {
            $list[$k]['get_status'] = $v['status'] == 0 ? "关闭":"开启";
        }


        $pageNav = new \Think\Page($total, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
          
        $this->assign('_list', $list);
        $this->assign('_page', $pageNav->show());
        $this->assign();

        $this->display();
    }

    /**
     * 新渠道任务
     **/
    public function fromStat() {
        
        $from_id = I('from_id');
        $from_time = I('from_time');

        if(!empty($from_id) && !empty($from_time)) {

            $taskConfig = D('DChannelTasks');
            $statModel = D('DTaskStat');
            $fromModel = D('DChannels');
            $taskConsumeModel = D('DTaskCost'); //消耗表

            $map['from_id'] = $from_id;
            $map['from_time'] = $from_time;

            $flist = $taskConfig->where($map)->order('`task_type` desc, `room_type` desc ,value+0 asc')->select();  //查询渠道配置内容
            
            $list = [];
            $b_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME));
            $e_time = strtotime(date('Y-m-d 23:59:59', NOW_TIME));

            $cmap['addtime'] = array('between', array($b_time, $e_time));

            foreach ($flist as $item) {
                $count = $statModel
                    ->join('game1.d_channel_tasks as c ON c.id = d_task_stat.task_config_id')
                    ->where(['task_config_id' => $item['id']])
                    ->where($cmap)
                    ->count(); // 查询 Stats

                $list[$item['id']] = ($count * $item['reward']);
                if($item['room_type'] == 0) {
                    $leisure0 += ($count * $item['reward']);
                }
                if($item['room_type'] == 1) {
                    $leisure1 += ($count * $item['reward']);
                }
                if($item['room_type'] == 2) {
                    $leisure2 += ($count * $item['reward']);
                }
                if($item['room_type'] == 3) {
                    $leisure3 += ($count * $item['reward']);
                }
                if($item['room_type'] == 11) {
                    $leisure11 += ($count * $item['reward']);
                }
                if($item['room_type'] == 21) {
                    $leisure21 += ($count * $item['reward']);
                }
                $taskConsume = $leisure0+$leisure1+$leisure2+$leisure3+$leisure11+$leisure21;
            }
            $from = $fromModel->where(['from_id' => $from_id, 'from_time' => $from_time])->find();
            $from_name = $from['from_name'];

        }
        $rooms = D('DRooms')->rooms();  //房间
       
        $this->assign('all_consume', $taskConsume ? $taskConsume: 0);
        $this->assign('leisure1', $leisure1 ? $leisure1 : 0);
        $this->assign('leisure2', $leisure2 ? $leisure2 : 0);
        $this->assign('leisure3', $leisure3 ? $leisure3 : 0);
        $this->assign('leisure11', $leisure11 ? $leisure11 : 0);
        $this->assign('leisure21', $leisure21 ? $leisure21 : 0);
        $this->assign('from_name', $from_name);
        $this->assign('from_id', $from_id);
        $this->assign('rooms', $rooms);
        $this->assign('from_time', $from_time);
        $this->assign('task_title', $flist);
        $this->assign('_list', $list);
        $this->display();
    }

    public function setActivityStatus()
    {
        $id = I('id');
        $pay_type = I('status');
        
        if(empty($id)) {
            $this->error('参数不正确');
        }

        $model = D('DChannelActivities');
        if ($model->where(['id' => $id])->save(['status' => intval($pay_type)]))
        {
            //提醒服务端重载
            $post_url = C('GM_URL') . 'sys/reload_conf'; 
            $pdata['tp'] = intval(201);              
            $pdata['timestamp'] = intval(time());
            $pdata['sign'] = md5(C('SENDGAME_KEY').time()); 
            $field = json_encode($pdata);
            $rets = PostUrl($post_url, $field,1);

            $this->success('操作成功');
        }
        else
        {
            $this->success('操作失败');
        }
    }

    public function editActivity()
    {
        $model = D('DChannelActivities');

        if(IS_POST) {

            $id = I('post.id');

            $data['from_id'] = I('from_id');
            $data['from_time'] = I('from_time');
            $updatedata['pay_reward'] =$data['pay_reward'] = I('pay_reward');
            $updatedata['pay'] =$data['pay'] = I('pay'); 
            $updatedata['status'] =$data['status'] = I('status');
            $updatedata['begin_time'] =$data['begin_time'] = I('begin_time');
            $updatedata['end_time'] =$data['end_time'] = I('end_time');
            if ($id)
            {
                $r=$model->where(['id' => $id])->save($updatedata);
            }
            else
            {
                $r=$model->add($data);
            }

             //提醒服务端重载
             $post_url = C('GM_URL') . 'sys/reload_conf'; 
             $pdata['tp'] = intval(201);              
             $pdata['timestamp'] = intval(time());
             $pdata['sign'] = md5(C('SENDGAME_KEY').time()); 
             $field = json_encode($pdata);
             $rets = PostUrl($post_url, $field,1);
             

            if($r===false){
                $this->error('操作失败', U('fromkey'));
            } else{
                $this->success('操作成功!',U('fromkey'));
            }

        } else {
            $id = I('id');
            
            $list = [];
            if ($id)
            {
                $list = $model->where(['id' => $id])->find();
                $status = $list['status'];
            }
            else
            {
                $status = 1;
            }
            $statuss[0] = "关闭";
            $statuss[1] = "开启";
            $this->assign('statuss', $statuss);

            $this->assign('info', $list);
            $this->assign('status', $status);
            $this->display();
        }
    }

    //后台登入记录
    public function userLoginlog()
    {
        
        $loginlog = M('Loginlog');
        $uid = I('uid');
        
        if(!empty($uid))
        {
            $rmap['mobile'] = $uid;
            $rmap['ip'] = $uid;
            $rmap['_logic'] = 'OR';
            $map['_complex'] = $rmap;
        }

        $p = I('p');
        $limit = 30;
       
        $totals = $loginlog->where($map)->count();
        $list = $loginlog->where($map)->page($p, $limit)->order('addtime desc')->select();
        
        $pageNav = new \Think\Page($totals, $limit);
        $pageNav->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        
        $this->assign('_page', $pageNav->show());
      
        $this->assign('_list', $list);
        $this->display();
    }
 
 

}

