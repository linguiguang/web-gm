<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/16
 * Time: 17:35
 */
namespace Admin\Controller;
use Think\Controller;

class TChannelTaskStatTestController extends Controller{
	
///opt/aiya/nginx/fastcgi/bin/php admin.php stat
/*
 * 
 DChannels   DChannelTasks   DAccounts->from_time */ 
 public function stat(){ 
    $stime = microtime(true);
    $etime = microtime(true);     
    $totals = round($etime - $stime);
    $content ='RUN '.$totals.'s .';
  
    echo $content;
 }
    
    public function channelstattest(){        
        //10s
        $stime = microtime(true);
		$zt = 1;
       
       D('DTaskStat')->channelstat_test();       
		
        $etime = microtime(true);     
        $totals = round($etime - $stime);
        $content ='RUN '.$totals.'s .';
      
        echo $content;
    }
    public function dotaskstattest(){        
        //30s
        $stime = microtime(true);
		$zt = 1;
      
        D('DTaskStat')->do_task_stat_test();      
		
        $etime = microtime(true);     
        $totals = round($etime - $stime);
        $content ='RUN '.$totals.'s .';
      
        echo $content;
    }
    public function payhourstat(){        
        //60s
        $stime = microtime(true);
		$zt = 1;
       
       //每小时充值计划执行  10分钟1次      
       D('DPayhourStat')->payhourstat();       
		
        $etime = microtime(true);     
        $totals = round($etime - $stime);
        $content ='RUN '.$totals.'s .';
      
        echo $content;
    }
    
    public function goldstat(){        
        //50s
        $stime = microtime(true);
		$zt = 1;
       
     
       D('DGoldstatday')->dgold(); 
       
		
        $etime = microtime(true);     
        $totals = round($etime - $stime);
        $content ='RUN '.$totals.'s .';
      
        echo $content;
    }

    public function onlinehourstat(){        
        //70s
        $stime = microtime(true);
		$zt = 1;
       
         //每小时充值计划执行  10分钟1次  
        D('DOnlineStat')->onlinehourstat();
       
		
        $etime = microtime(true);     
        $totals = round($etime - $stime);
        $content ='RUN '.$totals.'s .';
      
        echo $content;
    }
    
}