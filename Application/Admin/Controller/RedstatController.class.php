<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/15
 * Time: 16:37
 */
namespace Admin\Controller;
use Think\Controller;

/**
 * 红包场钥匙统计
 */

class RedstatController extends Controller{

    public function stat(){

        $stime = microtime(true);

        $Qruser = D('Qruser');
        $usergoldstat = D('Usergoldstat');

        $map['uid'] = array('gt', 0);
        $users = $Qruser->where($map)->field('uid,get,use,stat')->select();
        $room = 5;//红包场

        foreach ($users as $v){
            $operate = $this->operate_data($v['get'], $v['use']);
            $data = array(
                'uid' => $v['uid'],
                'room' => $room,
                'win_dia' => intval($operate['get'] - $v['tax']),
                'lose_dia' => intval($operate['use']),
                'stat' => $v['stat'],
            );

            $uap['uid'] = $v['uid'];
            $uap['room'] = 5;

            if($info = $usergoldstat->where($uap)->find()){
                $usergoldstat->where(array('_id'=>$info['_id']))->save($data);
            }else{
                $usergoldstat->add($data);
            }
        }

        $endtime = microtime(true);
        $totals = round($endtime - $stime);
        echo 'RUN '.$totals.'S';


    }

    protected function operate_data($get, $use){

        if($get > $use){
            $data['get'] = $get - $use;
            $data['use'] = 0;
        }else{
            $data['get'] = 0;
            $data['use'] = $use - $get;
        }

        return $data;
    }

}