<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/21
 * Time: 16:07
 */
namespace Admin\Controller;
use Think\Controller;

class LossStatController extends Controller{

    /**
     * 计算次日，3日，7日流失用户身上金币、钥匙、元宝、话费、红包统计
     */
    public function loss($time = ''){
        $stime = microtime(true);

//        $d_time = strtotime(date('Y-m-d 00:00:00', NOW_TIME - 86400));

        $b_time = strtotime($time);//注册日期
//        $e_time = strtotime('2017-03-16');

        $red['goods_id'] = array('in', array(99015, 99016, 99017));
        $reds = D('Shops')->where($red)->select();

        $redlogs = array();
        foreach ($reds as $k=>$v){
            $redlogs[$v['uid']][$v['c_time']] = $v['goods_id'];
        }
        unset($reds);

        $arealogin = D('Arealogin');
        $users = D('Users');
        $lossstat = D('Lossstat');

        for($i=1; $i<=1; $i++){
            $s_time = $b_time + ($i-1)*86400;
            $end_time = $s_time + 86399;

            echo 'day for'.date('Y-m-d', $s_time);echo PHP_EOL;

            //用户数据
            $us['adddate'] = array('between', array($s_time, $end_time));
            $us['state'] = 1;
            $list = $users->where($us)->select();

            $userlist = array();
            foreach ($list as $k=>$v){
                $date = date('Y-m-d', $v['adddate']);
                $userlist[$date][$v['id']] = $v;
            }
            unset($list);
            echo 'userlist success'.PHP_EOL;

            for($m=1; $m<8; $m++) {

                if ($m == 2 || $m == 3 || $m == 7) {

                    //当天数据和前一天登录数据
                    $s_time_log = $s_time + ($m-2)*86400;
                    $end_time_log = $s_time + ($m-1)*86400 + 86399;
                    echo 'login_b_time for '.date('Y-m-d', $s_time_log).PHP_EOL;
                    echo 'login_e_time for '.date('Y-m-d', $end_time_log).PHP_EOL;

                    //登出记录
                    $map['login_time'] = array('between', array($s_time_log, $end_time_log));
                    $map['type'] = 0;
                    $loginlog = $arealogin->where($map)->select();

                    $logins = array();
                    foreach ($loginlog as $v){
                        $logins[date('Y-m-d', $v['login_time'])][$v['uid']] = $v;
                    }

                    echo 'arrlogin_data sucess'.PHP_EOL;

                    foreach ($userlist[date('Y-m-d', $s_time)] as $k=>$v) {

                        if (!$logins[date('Y-m-d', ($v['adddate'] + 86400 * ($m - 1)))][$v['id']]) {
                            echo 'data_show date'.date('Y-m-d', ($v['adddate'] + 86400 * ($m - 1))).PHP_EOL;
                            if ($list_info = $logins[date('Y-m-d', ($v['adddate'] + 86400 * ($m - 2)))][$v['id']]) {

                                $data['c_time'] = $s_time;
                                $data['type'] = $m;
                                $data['uid'] = intval($list_info['uid']);
                                $data['gold'] = intval($list_info['gold']);
                                $data['gem'] = intval($list_info['gem']);
                                $data['wing'] = intval($list_info['wing']);
                                $data['bill'] = intval($list_info['bill']);
                                $data['red'] = $this->getred($redlogs[$data['uid']], intval($list_info['logout_time']));//红包

                                $uap['c_time'] = $data['c_time'];
                                $uap['uid'] = $data['uid'];
                                $uap['type'] = $m;
                                if ($info = $lossstat->where($uap)->find()) {
                                    $lossstat->where(array('_id' => $info['_id']))->save($data);
                                } else {
                                    $lossstat->add($data);
                                }

                                echo 'first record'.PHP_EOL;

                            }

                        }

                    }

                }

            }

        }

        echo 'SUCCESS'.PHP_EOL;
        $etime = microtime(true);
        $total = round($etime - $stime);
        echo "Run {$total}s times".PHP_EOL;
    }


    /**
     * 统计红包金额
     */
    protected function getred($data, $time){

        if(empty($data)) return 0;

        $goods_ids = array();
        foreach ($data as $k=>$v){

            if($time >= $k){//流失前共兑换的红包
                $goods_ids[$v] += 1;
            }
        }

        $red1 = intval($goods_ids[99015]);
        $red5 = intval($goods_ids[99016] * 5);
        $red10 = intval($goods_ids[99017] * 10);

        $total = $red1 + $red5 + $red10;
        return $total;
    }

}