<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------
namespace Admin\Api;
use Admin\Model\AuthRuleModel;
use Think\Controller;

/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class ChannelApiController extends Controller
{

    /**
     * 后台控制器初始化
     */
    protected function _initialize()
    {
        // 获取当前用户ID
        if (defined('UID')) {
            return;
        }

        define('UID', is_login());
        if (!UID) { // 还没登录 跳转到登录页面
            $this->redirect('Public/login');
        }
        /* 读取数据库中的配置 */
        $config = S('DB_CONFIG_DATA');
        if (!$config) {
            $config = api('Config/lists');
            S('DB_CONFIG_DATA', $config);
        }
        C($config); //添加配置

        // 是否是超级管理员
        define('IS_ROOT', is_administrator());
        if (!IS_ROOT && C('ADMIN_ALLOW_IP')) {
            // 检查IP地址访问
            if (!in_array(get_client_ip(), explode(',', C('ADMIN_ALLOW_IP')))) {
                $this->error('403:禁止访问');
            }
        }
        // 检测系统权限
        if (!IS_ROOT) {
            $access = $this->accessControl();
            if (false === $access) {
                $this->error('403:禁止访问');
            } elseif (null === $access) {
                //检测访问权限
                $rule = strtolower(MODULE_NAME . '/' . CONTROLLER_NAME . '/' . ACTION_NAME);
                if (!$this->checkRule($rule, array('in', '1,2'))) {
                    $this->error('未授权访问!');
                } else {
                    // 检测分类及内容有关的各项动态权限
                    $dynamic = $this->checkDynamic();
                    if (false === $dynamic) {
                        $this->error('未授权访问!');
                    }
                }
            }
        }

        $this->assign('__MENU__', $this->getMenus());
    }

    /**
     * 权限检测
     * @param string  $rule    检测的规则
     * @param string  $mode    check模式
     * @return boolean
     * @author 朱亚杰  <xcoolcc@gmail.com>
     */
    final protected function checkRule($rule, $type = AuthRuleModel::RULE_URL, $mode = 'url')
    {
        static $Auth = null;
        if (!$Auth) {
            $Auth = new \Think\Auth();
        }
        if (!$Auth->check($rule, UID, $type, $mode)) {
            return false;
        }
        return true;
    }

    /**
     * 检测是否是需要动态判断的权限
     * @return boolean|null
     *      返回true则表示当前访问有权限
     *      返回false则表示当前访问无权限
     *      返回null，则表示权限不明
     *
     * @author 朱亚杰  <xcoolcc@gmail.com>
     */
    protected function checkDynamic()
    {
    }

    /**
     * action访问控制,在 **登陆成功** 后执行的第一项权限检测任务
     *
     * @return boolean|null  返回值必须使用 `===` 进行判断
     *
     *   返回 **false**, 不允许任何人访问(超管除外)
     *   返回 **true**, 允许任何管理员访问,无需执行节点权限检测
     *   返回 **null**, 需要继续执行节点权限检测决定是否允许访问
     * @author 朱亚杰  <xcoolcc@gmail.com>
     */
    final protected function accessControl()
    {
        $allow = C('ALLOW_VISIT');
        $deny = C('DENY_VISIT');
        $check = strtolower(CONTROLLER_NAME . '/' . ACTION_NAME);
        if (!empty($deny) && in_array_case($check, $deny)) {
            return false; //非超管禁止访问deny中的方法
        }
        if (!empty($allow) && in_array_case($check, $allow)) {
            return true;
        }
        return null; //需要检测节点权限
    }

    /**
     * 对数据表中的单行或多行记录执行修改 GET参数id为数字或逗号分隔的数字
     *
     * @param string $model 模型名称,供M函数使用的参数
     * @param array  $data  修改的数据
     * @param array  $where 查询时的where()方法的参数
     * @param array  $msg   执行正确和错误的消息 array('success'=>'','error'=>'', 'url'=>'','ajax'=>false)
     *                     url为跳转页面,ajax是否ajax方式(数字则为倒数计时秒数)
     *
     * @author 朱亚杰  <zhuyajie@topthink.net>
     */
    final protected function editRow($model, $data, $where, $msg)
    {
        $id = array_unique((array) I('id', 0));
        $id = is_array($id) ? implode(',', $id) : $id;
        //如存在id字段，则加入该条件
        $fields = M($model)->getDbFields();
        if (in_array('id', $fields) && !empty($id)) {
            $where = array_merge(array('id' => array('in', $id)), (array) $where);
        }

        $msg = array_merge(array('success' => '操作成功！', 'error' => '操作失败！', 'url' => '', 'ajax' => IS_AJAX), (array) $msg);
        if (M($model)->where($where)->save($data) !== false) {
            $this->success($msg['success'], $msg['url'], $msg['ajax']);
        } else {
            $this->error($msg['error'], $msg['url'], $msg['ajax']);
        }
    }

    /**
     * 禁用条目
     * @param string $model 模型名称,供D函数使用的参数
     * @param array  $where 查询时的 where()方法的参数
     * @param array  $msg   执行正确和错误的消息,可以设置四个元素 array('success'=>'','error'=>'', 'url'=>'','ajax'=>false)
     *                     url为跳转页面,ajax是否ajax方式(数字则为倒数计时秒数)
     *
     * @author 朱亚杰  <zhuyajie@topthink.net>
     */
    protected function forbid($model, $where = array(), $msg = array('success' => '状态禁用成功！', 'error' => '状态禁用失败！'))
    {
        $data = array('status' => 0);
        $this->editRow($model, $data, $where, $msg);
    }

    /**
     * 恢复条目
     * @param string $model 模型名称,供D函数使用的参数
     * @param array  $where 查询时的where()方法的参数
     * @param array  $msg   执行正确和错误的消息 array('success'=>'','error'=>'', 'url'=>'','ajax'=>false)
     *                     url为跳转页面,ajax是否ajax方式(数字则为倒数计时秒数)
     *
     * @author 朱亚杰  <zhuyajie@topthink.net>
     */
    protected function resume($model, $where = array(), $msg = array('success' => '状态恢复成功！', 'error' => '状态恢复失败！'))
    {
        $data = array('status' => 1);
        $this->editRow($model, $data, $where, $msg);
    }

    /**
     * 还原条目
     * @param string $model 模型名称,供D函数使用的参数
     * @param array  $where 查询时的where()方法的参数
     * @param array  $msg   执行正确和错误的消息 array('success'=>'','error'=>'', 'url'=>'','ajax'=>false)
     *                     url为跳转页面,ajax是否ajax方式(数字则为倒数计时秒数)
     * @author huajie  <banhuajie@163.com>
     */
    protected function restore($model, $where = array(), $msg = array('success' => '状态还原成功！', 'error' => '状态还原失败！'))
    {
        $data = array('status' => 1);
        $where = array_merge(array('status' => -1), $where);
        $this->editRow($model, $data, $where, $msg);
    }

    /**
     * 条目假删除
     * @param string $model 模型名称,供D函数使用的参数
     * @param array  $where 查询时的where()方法的参数
     * @param array  $msg   执行正确和错误的消息 array('success'=>'','error'=>'', 'url'=>'','ajax'=>false)
     *                     url为跳转页面,ajax是否ajax方式(数字则为倒数计时秒数)
     *
     * @author 朱亚杰  <zhuyajie@topthink.net>
     */
    protected function delete($model, $where = array(), $msg = array('success' => '删除成功！', 'error' => '删除失败！'))
    {
        $data['status'] = -1;
        $this->editRow($model, $data, $where, $msg);
    }

    /**
     * 设置一条或者多条数据的状态
     */
    public function setStatus($Model = CONTROLLER_NAME)
    {

        $ids = I('request.ids');
        $status = I('request.status');
        if (empty($ids)) {
            $this->error('请选择要操作的数据');
        }

        $map['id'] = array('in', $ids);
        switch ($status) {
            case -1:
                $this->delete($Model, $map, array('success' => '删除成功', 'error' => '删除失败'));
                break;
            case 0:
                $this->forbid($Model, $map, array('success' => '禁用成功', 'error' => '禁用失败'));
                break;
            case 1:
                $this->resume($Model, $map, array('success' => '启用成功', 'error' => '启用失败'));
                break;
            default:
                $this->error('参数错误');
                break;
        }
    }

    /**
     * 获取控制器菜单数组,二级菜单元素位于一级菜单的'_child'元素中
     * @author 朱亚杰  <xcoolcc@gmail.com>
     */
    final public function getMenus($controller = CONTROLLER_NAME)
    {
        $menus = session('ADMIN_MENU_LIST.' . $controller);
        if (empty($menus)) {

            // 获取主菜单
            $where['pid'] = 0;
            $where['hide'] = 0;
            if (!C('DEVELOP_MODE')) { // 是否开发者模式
                $where['is_dev'] = 0;
            }
            $menus['main'] = M('Menu')->where($where)->order('sort asc')->field('id,title,url')->select();
            $menus['child'] = array(); //设置子节点
            foreach ($menus['main'] as $key => $item) {
                // 判断主菜单权限
                if (!IS_ROOT && !$this->checkRule(strtolower(MODULE_NAME . '/' . $item['url']), AuthRuleModel::RULE_MAIN, null)) {
                    unset($menus['main'][$key]);
                    continue; //继续循环
                }
                if (strtolower(CONTROLLER_NAME . '/' . ACTION_NAME) == strtolower($item['url'])) {
                    $menus['main'][$key]['class'] = 'current';
                }
            }

            // 查找当前子菜单
            $pid = M('Menu')->where("pid !=0 AND url like '%{$controller}/" . ACTION_NAME . "%'")->getField('pid');
            if ($pid) {
                // 查找当前主菜单
                $nav = M('Menu')->find($pid);
                if ($nav['pid']) {
                    $nav = M('Menu')->find($nav['pid']);
                }
                foreach ($menus['main'] as $key => $item) {
                    // 获取当前主菜单的子菜单项
                    if ($item['id'] == $nav['id']) {
                        $menus['main'][$key]['class'] = 'current';
                        //生成child树
                        $groups = M('Menu')->where(array('group' => array('neq', ''), 'pid' => $item['id']))->distinct(true)->getField("group", true);
                        //获取二级分类的合法url
                        $where = array();
                        $where['pid'] = $item['id'];
                        $where['hide'] = 0;
                        if (!C('DEVELOP_MODE')) { // 是否开发者模式
                            $where['is_dev'] = 0;
                        }
                        $second_urls = M('Menu')->where($where)->getField('id,url');

                        if (!IS_ROOT) {
                            // 检测菜单权限
                            $to_check_urls = array();
                            foreach ($second_urls as $key => $to_check_url) {
                                if (stripos($to_check_url, MODULE_NAME) !== 0) {
                                    $rule = MODULE_NAME . '/' . $to_check_url;
                                } else {
                                    $rule = $to_check_url;
                                }
                                if ($this->checkRule($rule, AuthRuleModel::RULE_URL, null)) {
                                    $to_check_urls[] = $to_check_url;
                                }
                            }
                        }
                        // 按照分组生成子菜单树
                        foreach ($groups as $g) {
                            $map = array('group' => $g);
                            if (isset($to_check_urls)) {
                                if (empty($to_check_urls)) {
                                    // 没有任何权限
                                    continue;
                                } else {
                                    $map['url'] = array('in', $to_check_urls);
                                }
                            }
                            $map['pid'] = $item['id'];
                            $map['hide'] = 0;
                            if (!C('DEVELOP_MODE')) { // 是否开发者模式
                                $map['is_dev'] = 0;
                            }
                            $menuList = M('Menu')->where($map)->field('id,pid,title,url,tip')->order('sort asc')->select();
                            $menus['child'][$g] = list_to_tree($menuList, 'id', 'pid', 'operater', $item['id']);
                        }
                    }
                }
            }
            session('ADMIN_MENU_LIST.' . $controller, $menus);
        }

        return $menus;
    }

    /**
     * 返回后台节点数据
     * @param boolean $tree    是否返回多维数组结构(生成菜单时用到),为false返回一维数组(生成权限节点时用到)
     * @retrun array
     *
     * 注意,返回的主菜单节点数组中有'controller'元素,以供区分子节点和主节点
     *
     * @author 朱亚杰 <xcoolcc@gmail.com>
     */
    final protected function returnNodes($tree = true)
    {
        static $tree_nodes = array();
        if ($tree && !empty($tree_nodes[(int) $tree])) {
            return $tree_nodes[$tree];
        }
        if ((int) $tree) {
            $list = M('Menu')->where(['status' => 1])->field('id,pid,title,url,tip,hide')->order('sort asc')->select();
            foreach ($list as $key => $value) {
                if (stripos($value['url'], MODULE_NAME) !== 0) {
                    $list[$key]['url'] = MODULE_NAME . '/' . $value['url'];
                }
            }
            $nodes = list_to_tree($list, $pk = 'id', $pid = 'pid', $child = 'operator', $root = 0);
            foreach ($nodes as $key => $value) {
                if (!empty($value['operator'])) {
                    $nodes[$key]['child'] = $value['operator'];
                    unset($nodes[$key]['operator']);
                }
            }
        } else {
            $nodes = M('Menu')->field('title,url,tip,pid')->order('sort asc')->select();
            foreach ($nodes as $key => $value) {
                if (stripos($value['url'], MODULE_NAME) !== 0) {
                    $nodes[$key]['url'] = MODULE_NAME . '/' . $value['url'];
                }
            }
        }
        $tree_nodes[(int) $tree] = $nodes;
        return $nodes;
    }

    /**
     * 通用分页列表数据集获取方法
     *
     *  可以通过url参数传递where条件,例如:  index.html?name=asdfasdfasdfddds
     *  可以通过url空值排序字段和方式,例如: index.html?_field=id&_order=asc
     *  可以通过url参数r指定每页数据条数,例如: index.html?r=5
     *
     * @param sting|Model  $model   模型名或模型实例
     * @param array        $where   where查询条件(优先级: $where>$_REQUEST>模型设定)
     * @param array|string $order   排序条件,传入null时使用sql默认排序或模型属性(优先级最高);
     *                              请求参数中如果指定了_order和_field则据此排序(优先级第二);
     *                              否则使用$order参数(如果$order参数,且模型也没有设定过order,则取主键降序);
     *
     * @param boolean      $field   单表模型用不到该参数,要用在多表join时为field()方法指定参数
     * @author 朱亚杰 <xcoolcc@gmail.com>
     *
     * @return array|false
     * 返回数据集
     */
    protected function lists($model, $where = array(), $order = '', $field = true)
    {
        $options = array();
        $REQUEST = (array) I('request.');
        if (is_string($model)) {
            $model = M($model);
        }

        $OPT = new \ReflectionProperty($model, 'options');
        $OPT->setAccessible(true);

        $pk = $model->getPk();
        if ($order === null) {
            //order置空
        } else if (isset($REQUEST['_order']) && isset($REQUEST['_field']) && in_array(strtolower($REQUEST['_order']), array('desc', 'asc'))) {
            $options['order'] = '`' . $REQUEST['_field'] . '` ' . $REQUEST['_order'];
        } elseif ($order === '' && empty($options['order']) && !empty($pk)) {
            $options['order'] = $pk . ' desc';
        } elseif ($order) {
            $options['order'] = $order;
        }
        unset($REQUEST['_order'], $REQUEST['_field']);

        if (empty($where)) {
            $where = array('status' => array('egt', 0));
        }
        if (!empty($where)) {
            $options['where'] = $where;
        }
        $options = array_merge((array) $OPT->getValue($model), $options);
        $total = $model->where($options['where'])->count();

        if (isset($REQUEST['r'])) {
            $listRows = (int) $REQUEST['r'];
        } else {
            $listRows = C('LIST_ROWS') > 0 ? C('LIST_ROWS') : 10;
        }
        $page = new \Think\Page($total, $listRows, $REQUEST);
        if ($total > $listRows) {
            $page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        }
        $p = $page->show();
        $this->assign('_page', $p ? $p : '');
        $this->assign('_total', $total);
        $options['limit'] = $page->firstRow . ',' . $page->listRows;

        $model->setProperty('options', $options);

        return $model->field($field)->select();
    }

    /**
     * 处理文档列表显示
     * @param array $list 列表数据
     * @param integer $model_id 模型id
     */
    protected function parseDocumentList($list, $model_id = null)
    {
        $model_id = $model_id ? $model_id : 1;
        $attrList = get_model_attribute($model_id, false, 'id,name,type,extra');
        // 对列表数据进行显示处理
        if (is_array($list)) {
            foreach ($list as $k => $data) {
                foreach ($data as $key => $val) {
                    if (isset($attrList[$key])) {
                        $extra = $attrList[$key]['extra'];
                        $type = $attrList[$key]['type'];
                        if ('select' == $type || 'checkbox' == $type || 'radio' == $type || 'bool' == $type) {
                            // 枚举/多选/单选/布尔型
                            $options = parse_field_attr($extra);
                            if ($options && array_key_exists($val, $options)) {
                                $data[$key] = $options[$val];
                            }
                        } elseif ('date' == $type) { // 日期型
                            $data[$key] = date('Y-m-d', $val);
                        } elseif ('datetime' == $type) { // 时间型
                            $data[$key] = date('Y-m-d H:i', $val);
                        }
                    }
                }
                $data['model_id'] = $model_id;
                $list[$k] = $data;
            }
        }
        return $list;
    }
	
    /* 游戏房间 */
    protected function rooms()
    {
		return C('ROOMS');
    }

    /**
     * 途径
     */
    protected function path()
    {
        $path = array(
            1 => '不处理',
            // 10 => '牛牛结算', //95%
            // 11 => '牛牛入场费',
            // 20 => '百人结算', //96%
            // 21 => '百人开奖池奖励', //2%
            // 22 => '百人下注',
            // // 30 => '红包场结算',
            // 99 => '机器人相关',
            // 100 => '管理人员发邮件',
            // 101 => '魔法表情消耗',
            // 102 => '邮件奖励',
            // 103 => '任务奖励',
            // 104 => '商城',
            105 => '救济金',
            // 106 => '充值钥匙',
            // 107 => '拉霸',
            // 108 => '红包广场',
            // 109 => '签到',
            // 110 => 'vip特别奖励',
            // 111 => '改名',
            112 => '绑定手机',
            113 => '兑换',
            // 114 => '机器人金币补充',
            // 115 => '机器人金币减少',
            // 116 => '对局宝箱',
            // 117 => '新手引导领奖',
            200 => '任务',
            // 119 => '拉霸(百人)大厅活动', //现在改为大厅活动(拉霸/百人累计金币赢取)领取的金币,,前为红包任务
            // 120 => '红包场领红包',
            // 121 => '红包场奖励钥匙',
            // 122 => '重置钥匙数', //复活
            // 123 => '离线房间奖励',
            // 124 => '机器人超出上限扣钱',
            // 125 => '机器人 钥匙补充',
            // 126 => '拉霸任务',
            // 127 => '拉霸宝箱',
            // 128 => '牛牛任务',
            // 129 => '牛牛宝箱',
            // 130 => '月卡领取的金币',
            // 131 => '注册',
            // 132 => '分享',
            // 1320 => '新人奖励(分享)',
            // 1321 => '好友进阶(分享)',
            // 133 => '豪车',
            // 134 => '金牛',
            // 135 => '红包场复活',
            // 136 => '对局免费产出',
            // 137 => '对局兑换产出',
            // 138 => '晶石兑换成金币',
            // 139 => '7日狂欢',
            // 140 => '分享转盘',
            // 141 => '百人对局金币产出',
            // 142 => '百人对局钥匙产出',
            // 143 => '百人对局兑换钥匙',
            // 144 => '拉霸对局金币产出',
            // 145 => '拉霸对局钥匙产出',
            // 146 => '拉霸对局兑换钥匙',
            // 147 => '实名认证',
            // 148 => '一本万利',
            // 149 => '超级拉霸对局金币产出',
            // 150 => '超级拉霸对局钥匙产出',
            // 151 => '超级拉霸对局兑换钥匙',
            // 152 => '超级拉霸',
            // 153 => '红包兑换金币',
            // 154 => '粘性红包试玩场拉霸',
            // 155 => '粘性红包娱乐场拉霸',
            // 156 => '粘性红包试玩场超级拉霸',
            // 157 => '粘性红包娱乐场超级拉霸',
            // 158 => '粘性红包试玩场百人牛牛',
            // 159 => '粘性红包娱乐场百人牛牛',
            // 160 => '粘性红包豪车',
            // 161 => '粘性红包渠道拉霸',
            // 162 => '粘性红包渠道超级拉霸',
            // 1001 => '群发邮件',
            // 1002 => '发单人邮件',
            // 1003 => '通知拆红包邮件',
            // 1004 => '福袋',
            // 1005 => '取消红包邮件',
            // 1006 => '红包广场超时',
            // 1007 => '赚金榜单局榜', // 百人牛牛赚金榜
            // 1008 => '赚金榜累计榜', // 百人牛牛赚金榜
            // 1009 => '每周充值奖励',
            // 1010 => '兑换',
            // 1011 => '超级拉霸周榜',
            // 1014 => '空战排行榜奖励',
            // 1020 => '赚金榜排行奖励',
            // 2000 => '第一次领取红包金币',
            // 164 => '红包场水果',
            // 777 => '每日工资(旧)'
        );
        //$path = array();
        // $path['152'] = '超级拉霸';
        $path['104'] = '商城';
        // $path['178'] = '每日抽奖';
        // $path['176'] = '每日工资';
        $path['107'] = '拉霸';
        // $path['20'] = '百人结算';
        // $path['131'] = '注册';
        // $path['1002'] = '发单人邮件';
        // $path['109'] = '签到';
        $path['147'] = '实名认证';
        $path['221'] = '招财猫';

        // $path['174'] = '看视频领取';
        // $path['175'] = '商城随机获取金币';
        // $path['153'] = '红包兑换金币';

        // $path['164'] = '红包场水果';
        // $path['165'] = '娱乐场水果';
        // $path['103'] = '任务奖励';
        // $path['104'] = '商城';

        // $path['101'] = '魔法表情消耗';
        $path['130'] = '月卡领取的金币';
        $path['163'] = '周卡领取';
        
        $path['201'] = '新手狂欢';
        $path['202'] = '红包场大满贯';
        $path['203'] = '吃鸡狂欢场宝箱';
        
        $path['204'] = '娱乐场';
        $path['205'] = '休闲场';
        $path['206'] = '吃鸡狂欢场';
        $path['207'] = '超级狂欢场';
        
        $path['208'] = '升级招财魂';
        $path['209'] = '狂欢场日榜';
        
        $path['210'] = '狂欢场周榜';
        $path['211'] = '狂欢场月榜';
        $path['212'] = '红包场日榜';
        
        $path['213'] = '红包场周榜';
        $path['214'] = '红包场月榜';
        $path['215'] = '比武场日榜';
        $path['216'] = '比武场周榜';
        $path['217'] = '比武场月榜';
        $path['218'] = '休闲场吃鸡榜';
        $path['219'] = '基金瓜分榜';
        $path['220'] = '狂欢场吃鸡榜';

        


        return $path;
    }

    /**
     * 单独游戏房间
     * @return array
     */
    protected function onerooms()
    {
        $config = array(
            // 1 => '看牌抢庄',
            2 => '百人拼牛',
            3 => '红包场',
            4 => '激情豪车',
            // 5 => '红包场',
            6 => '倍率场',
            7 => '红包场[站内]',
            0 => '大厅'
        );
        return $config;
    }


    protected function pay_types()
    {
        $config = array(          
            1 => '支付宝',
            2 => '微信支付',
            /* 13 => '米花支付',
            14 => '支付宝',
            15 => '汇潮支付宝',
            16 => '汇天下支付宝',
            17 => '汇天下小程序',
            18 => '合利宝支付宝',
            19 => '合利宝小程序',
            20 => '酷点支付宝',
            21 => '酷点微信',
            22 => '连连支付',
            24 => '现在支付alipay',
            25 => '喜钛游支付宝', */
        );
        return $config;
    }

    /**
     * 语音验证
     */
    protected function sendVoicecode($mobile, $code)
    {
        if (true) {
            $username = 'C20';
            $password = 'EC09F26AC8F4C3748D764E7DECF02C05E225B5C7';
            $timestamp = NOW_TIME;
            $seq = rand(1000000, 9999999);

            $digest = $username . '@' . $timestamp . '@' . $seq . '@' . $password;
            $param = array();

            $param['authentication'] = array(
                'customer' => $username,
                'digest' => md5($digest),
                'seq' => $seq,
                'timestamp' => $timestamp,
            );

            $param['param'] = array('debug' => false, 'lang' => 'en_US');

            $param['request'] = array(
                'callee' => $mobile,
                'playTimes' => 2,
                'seq' => $seq,
                'verifyCode' => $code,
                'userData' => ''
            );

            $post_field = json_encode($param);

            $url = 'http://223.111.192.94/openapi/V2.0.6/VoiceVerify';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field);
            curl_setopt($ch, CURLOPT_TIMEOUT, 25);
            $result = curl_exec($ch);

            //$result = post_json($url, $post_field);

            $ret = json_decode($result, true);

            if ($ret['result']['error'] == 0) {
                \Think\Log::write("Send voice verify to mobile " + $mobile + " success");
                return $ret['data']['seq'];
            } else {
                \Think\Log::write('SEND MOBILE CODE RETURN_ERROR_NO: ' . $ret['error'] . 'JSON DATA ' . $result, 'ERR');
                return 'ERROR-CODE : ' . $ret['result']['error'];
            }
        } else {
            $username = 'api';
            $password = 'key-5f4ef864bf1ecf132acfa4d38f0bca2b';

            $field = array(
                'mobile' => $mobile,
                'code' => $code,
            );
            $post_field = http_build_query($field);

            $url = 'http://voice-api.luosimao.com/v1/verify.json';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field);
            curl_setopt($ch, CURLOPT_TIMEOUT, 25);
            $result = curl_exec($ch);

            $ret = json_decode($result, true);

            if ($ret['error'] == 0) {
                return $ret['batch_id'];
            } else {
                \Think\Log::write('SEND MOBILE CODE RETURN_ERROR_NO: ' . $ret['error'] . 'JSON DATA ' . $result, 'ERR');
                return 'ERROR-CODE : ' . $ret['error'];
            }
        }
    }

    /**
     * 不专业的短信接口
     */
    protected function sendSMS($mobile, $code)
    {

        $tm = date('YmdHis', NOW_TIME);
        $md5pwd = md5('153410' . $tm);
        $content = '【航海夺宝】您的验证码为' . $code . '请于5分钟内正确输入，如非本人操作，请忽略此短信。';
        $data_arr = array(
            'uid' => 630,
            'pw' => $md5pwd,
            'mb' => $mobile,
            'ms' => $content,
            'ex' => 01,
            'tm' => $tm,

        );
        $get_field = http_build_query($data_arr);

        $url = 'http://139.196.144.25:18002/send.do?' . $get_field;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);

        $str = curl_exec($curl);

        if (!curl_errno($curl)) {
            $data = explode(',', $str);
            if ($data[0] != 0) {
                \Think\Log::write('SEND MOBILE CODE RETURN_ERROR_NO: ' . $data[0], 'ERR');
            } else {
                return $data[1];
            }
        } else {
            \Think\Log::write('SEND MOBILE CODE CURL_ERROR: ' . curl_error($curl), 'ERR');
        }

        return '';
    }


    //查询通用条件
    protected function getCommonQuery($param, $selecteq0, $selectneq0)
    {
        $map = array();
        $query = $orders = '';
        //通过玩家id查询
        if (array_key_exists('user_id', $param)) {
            $type = intval(I('type'));
            $value = I('value');
            if (isset($_GET['type']) && isset($_GET['value'])) {

                if (is_array($param['user_id'])) {
                    $keyField = $param['user_id'][0];
                    $queryField = $param['user_id'][1];
                } else {
                    $keyField = $param['user_id'];
                    $queryField = $param['user_id'];
                }

                if ($keyField == 'uid') {
                    switch ($type) {
                        case 0: //用户ID
                            $map[$queryField] = intval($value);
                            break;
                        case 1: //角色ID
                            $map[$queryField] = D('Users')->get_uid('char_id', intval($value));
                            break;
                        case 2: //手机号
                            $map[$queryField] = D('Users')->get_uid('mobile', trim($value));
                            break;
                        case 3: //玩家昵称
                            $userinfo = D('Userinfo')->where(array('username' => array('like', trim($value))))->field('uid')->select();
                            $ids = array();
                            foreach ($userinfo as $v) {
                                $ids[] = intval($v['uid']);
                            }
                            $map[$queryField] = array('in', $ids);
                            break;
                    }
                } elseif ($keyField == 'char_id') {
                    switch ($type) {
                        case 0: //用户ID
                            $map[$queryField] = D('Users')->get_charid('uid', trim($value));
                            break;
                        case 1: //角色ID
                            $map[$queryField] = intval($value);
                            break;
                        case 2: //手机号
                            $map[$queryField] = D('Users')->get_charid('mobile', trim($value));
                            break;
                        case 3: //玩家昵称
                            $userinfo = D('Userinfo')->where(array('username' => array('like', trim($value))))->field('char_id')->select();
                            $ids = array();
                            foreach ($userinfo as $v) {
                                $ids[] = intval($v['char_id']);
                            }
                            $map[$queryField] = array('in', $ids);
                            break;
                    }
                }
                $query .= '/type/' . I('type') . '/value/' . I('value') . '/';
            }
        }
        //时间查询
        if (array_key_exists('time', $param)) {
            //点击获取时间（今天，昨天，本周，上周，本月，上月）
            $clickTime = intval(I('clicktime'));
            $timeArr = clickDate($clickTime);
            if ($clickTime)
                $query .= '/clicktime/' . I('clicktime') . '/';

            if (strpos(I('b_time'), ':') || strpos(I('e_time'), ':')) {
                $b_time = strtotime($timeArr['b_time']);
                $e_time = strtotime($timeArr['e_time']);
            } else {
                $b_time = strtotime($timeArr['b_time']);
                $e_time = strtotime($timeArr['e_time']) + 86399;
            }

            if ($timeArr['b_time'] && $timeArr['e_time']) {
                $map[$param['time']] = array('between', array($b_time, $e_time));
                $query .= '/b_time/' . I('b_time') . '/e_time/' . I('e_time') . '/';
            } elseif (!$timeArr['b_time'] && $timeArr['e_time']) {
                $map[$param['time']] = array('elt', $e_time);
                $query .= '/e_time/' . I('e_time') . '/';
            } elseif ($timeArr['b_time'] && !$timeArr['e_time']) {
                $map[$param['time']] = array('egt', $b_time);
                $query .= '/b_time/' . I('b_time') . '/';
            }
        }
        //按月份时间查询
        if (array_key_exists('month', $param)) {
            $daytype = intval(I('daytype'));
            $b_time = I('b_time');
            $e_time = I('e_time');
            if ($daytype == 1) { //按天数查询
                if (!isset($_GET['b_time']) || !isset($_GET['e_time']))
                    $this->error('请选择时间区间');
                $b_time = strtotime($b_time);
                $e_time = strtotime($e_time) + 86399;
            } else { //默认按月查询
                if (isset($_GET['month'])) {
                    if (!$b_time && !$e_time) {
                        $monarr = monthday(NOW_TIME, intval(I('month')));
                        $b_time = strtotime($monarr[0]);
                        $e_time = strtotime($monarr[1]) + 86399;
                    } elseif (date('y-m', strtotime($b_time)) == date('y-m', strtotime($e_time))) {
                        $b_time = strtotime($b_time);
                        $e_time = strtotime($e_time) + 86399;
                    } else {
                        $year_time = $b_time ? $b_time : $e_time;
                        $monarr = monthday(strtotime($year_time), intval(I('month'))); //取输入时间区间的年份
                        $b_time = strtotime($monarr[0]);
                        $e_time = strtotime($monarr[1]) + 86399;
                    }
                    $query .= '/month/' . I('month') . '/';
                } else {
                    $b_time = strtotime(date('Y-m-1', NOW_TIME));
                    $e_time = strtotime(date('Y-m-t', NOW_TIME)) + 86399;
                }
                $this->assign('month', date('n', $b_time));
            }
            $map[$param['month']] = array('between', array($b_time, $e_time));
            $query .= '/daytype/' . $daytype . '/';
            $query .= I('b_time') ? '/b_time/' . I('b_time') . '/' : '';
            $query .= I('e_time') ? '/e_time/' . I('e_time') . '/' : '';
        }
        //充值区间查询
        if (array_key_exists('pays', $param)) {
            $payArr['b_pays'] = I('b_pays');
            $payArr['e_pays'] = I('e_pays');
            $b_pays = intval(I('b_pays'));
            $e_pays = intval(I('e_pays'));

            if ($payArr['b_pays'] && $payArr['e_pays']) {
                $map[$param['pays']] = array('between', array($b_pays, $e_pays));
                $query .= '/b_pays/' . I('b_pays') . '/e_pays/' . I('e_pays') . '/';
            } elseif (!$payArr['b_pays'] && $payArr['e_pays']) {
                $map[$param['pays']] = array('elt', $e_pays);
                $query .= '/e_pays/' . I('e_pays') . '/';
            } elseif ($payArr['b_pays'] && !$payArr['e_pays']) {
                $map[$param['pays']] = array('egt', $b_pays);
                $query .= '/b_pays/' . I('b_pays') . '/';
            }
        }
        //模糊查询 mysql示例 ['like'=>'%field%'], mongo示例 ['like'=>'field']
        if (array_key_exists('like', $param)) {
            $field = trim($param['like'], '%');
            $val = trim(I($field));
            if ($val) {
                $map[$field] = strpos($param['like'], '%') !== false ? array('like', '%' . $val . '%') : array('like', $val);
                $query .= '/' . $field . '/' . I($field) . '/';
            }
        }
        //排序
        if (array_key_exists('orderby', $param)) {
            $orderby = trim(I('orderby'));
            $order   = trim(I('order'));
            if ($orderby != '' && $order != '') {
                $orders = $orderby . ' ' . $order;
            } else {
                $orders = $param['orderby'] . ' desc';
            }
        }
        //select 默认值为空或0 或 input 输入
        //格式如['type' => 'usetype'] type为数据库字段，usetype为html的name
        if ($selecteq0) {
            foreach ($selecteq0 as $queryField => $htmlField) {
                $htmlVal = I($htmlField);
                if ($htmlVal) {
                    $htmlVal = is_numeric($htmlVal) && $htmlVal < 2147483647 ? intval($htmlVal) : trim($htmlVal);
                    $map[$queryField] = $htmlVal;
                    $query .= '/' . $htmlField . '/' . $htmlVal . '/';
                }
            }
        }

        //select 默认值不为空
        //格式如['type' => ['usetype', 1],] type为数据库字段，usetype为html的name，1为默认值，，
        //如果在为默认值情况下需要进行查询格式为['type' => ['usetype', 1, true],]
        if ($selectneq0) {
            foreach ($selectneq0 as $queryField => $htmlField) {
                $htmlVal = I($htmlField[0]);
                if ((string) $htmlVal != '' && $htmlVal != $htmlField[1]) {
                    $htmlVal = is_numeric($htmlVal) ? intval($htmlVal) : trim($htmlVal);
                    $map[$queryField] = $htmlVal;
                    $query .= '/' . $htmlField[0] . '/' . $htmlVal . '/';
                } else {
                    $htmlVal = $htmlField[1];
                    //默认值情况下也执行查询条件
                    if ($htmlField[2] === true) $map[$queryField] = $htmlField[1];
                }
                $this->assign($htmlField[0], $htmlVal);
            }
        }

        $this->assign('query', $query);
        return array('map' => $map, 'query' => $query, 'order' => $orders);
    }
}
