<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/15
 * Time: 11:03
 */
namespace Admin\Model;
use Think\Model;

class LevelModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'mg_level';

}