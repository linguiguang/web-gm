<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DComplaintModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG4';

    protected $tableName = 'complaint';
}