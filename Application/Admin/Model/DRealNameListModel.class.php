<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/7
 * Time: 18:53
 */
namespace  Admin\Model;
use Think\Model;

class DRealNameListModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG4';

    protected $tableName = 'real_name_list';

}