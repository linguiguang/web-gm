<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/12
 * Time: 18:06
 */
namespace Admin\Model;
use Think\Model;

class PaymoneystatModel extends Model{
    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';


    public function fromRecharge($from_id, $b_time = '', $e_time = ''){

        if($b_time != '' && $e_time != ''){
            $map['date'] = array('between', array(strtotime($b_time), strtotime($e_time)+86399));
        }elseif($b_time == '' && $e_time != ''){
            $map['date'] = array('elt', strtotime($e_time)+86399);
        }elseif($b_time != '' && $e_time == ''){
            $map['date'] = array('egt', strtotime($b_time));
        }

        $map['from_id'] = $from_id;

        $list = $this->where($map)->order('date asc')->select();

        $money = $man = $tenman = $_159men = $_799men = 0;
        foreach ($list as $v){
            $money = $v['money'];
            $man = $v['mans'];
            $tenman = $v['ten_man'];
            $_159men = $v['159_man'];
            $_799men = $v['799_man'];
        }
        $data = array(
            'money' => $money,
            'man' => $man,
            'tenman' => $tenman,
            '_159man' => $_159men,
            '_799man' => $_799men,
        );
        return $data;
    }
}