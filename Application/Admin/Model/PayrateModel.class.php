<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/15
 * Time: 10:22
 */
namespace Admin\Model;
use Think\Model;

class PayrateModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'mg_payrate';

}