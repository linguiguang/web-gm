<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/7
 * Time: 18:53
 */
namespace  Admin\Model;
use Think\Model;

class YidunBrushModel extends Model{

/** 本接口用于智能风控嫌疑数据在线检测，并且接口会同步返回检测结果。 */
    /** 产品ID，从【易盾官网-服务管理-已开通业务】页面获取 */
    const SECRET_ID = "f6190426214cdb23b8e8da76047f4195";
    /** 密钥，从【易盾官网-服务管理-已开通业务】页面获取 */
    const SECRET_KEY = "3e7557699996830234e762946ce0ff8e";
    const BUSINESS_ID = "YD00800071985417";
    const VERSION = "500";
    /** 接口URL */
    const API_URL = "http://ir-open.dun.163.com/v5/risk/check";//http://ir-open.dun.163.com/v6/risk/check
    /** API timeout*/
    const API_TIMEOUT = 5;
    

    /**
     * curl post请求
     * @params 输入的参数
     */
    public function curl_post($params, $url, $timout){
        $ch = curl_init();
        $json = json_encode($params);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 设置超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, $timout);
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:'.'application/json; charset=UTF-8'));
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /**
     * 生成签名方法
     * 不同openapi的签名参数不同
     * @param $secretKey
     * @param $params
     * @return string
     */
    public function gen_signature($secretKey, $params){
        ksort($params);
        $buff="";
        foreach($params as $key=>$value){
            if($value !== null) {
                $buff .=$key;
                $buff .=$value;
            }
        }
        $buff .= $secretKey;
        return md5($buff);
    }

    /**
     * 将输入数据的编码统一转换成utf8
     * @params 输入的参数
     */
    public function toUtf8($params){
        $utf8s = array();
        foreach ($params as $key => $value) {
            $utf8s[$key] = is_string($value) ? mb_convert_encoding($value, "utf8", INTERNAL_STRING_CHARSET) : $value;
        }
        return $utf8s;
    }
    /**
     * 智能风控请求接口简单封装
     * $params 请求参数
     */
    public function ydcheck($params)
    {
        $params["secretId"] = SECRET_ID;
        $params["businessId"] = BUSINESS_ID;
        $params["version"] = VERSION;
        $params["timestamp"] = time() * 1000;
        // 随机码 32位
        $params["nonce"] = "mmm888f73yyy59440583zzz9bfcc79de";
        // 风控SDK上报的数据后回参获取到的 token，应用方需要从智能风控客户端SDK获取该数据。详情请查看客户端接入文档。
        $params["token"] = "your_token";
        // 用户/ 玩家的IP，或当前客户端业务事件发生时的公网IP地址（ipv4）
        $params["ip"] = "183.136.182.141";
        // 用户/玩家的角色 ID，非游戏类型应用，roleId 可以与 roleAccount 相同
        $params["roleId"] = "yyyyyyy";
        // 用户/玩家的角色名称，非游戏类型应用，roleName 可以是当前用户昵称相同
        //$params["nickname"] = "yyyyyyy";
        // 用户/玩家的角色的服务器名称
        //$params["server"] = "yyyyyyy";
        // 用户/玩家的账号
        //$params["account"] = "zzzzzzz";
        // 用户/玩家的等级
       // $params["level"] = 150;
        // 游戏类型应用的版本号
        $params["gameVersion"] = "1.0.2";
        // 游戏类型应用的资源版本号
        $params["assetVersion"] = "assetVersion";
        // 额外/拓展的信息，应用 / 游戏方可以自己构建json结构，最大长度：2048。不同场景构建信息见分场景接入说明
        $params["extData"] = "";
        // 活动操作的目标，比如：A给B点赞，则target为B。如果target是手机号或邮箱，请提供hash值，hash算法：md5(target)。如没有，可传空
        $params["target"] = "";
        // 活动的唯一标识，用于标记场景下的细分类别，如：注册-自主注册、注册-受邀请注册；再如：登录- app登录、登录-web登录等
        $params["activityId"] = "";
        // 用户用于登录的手机号码或者关联的手机号码，默认国内手机号。如有海外手机，需包含国家地区代码，
        // 格式为“+447410xxx186（+44即为国家码）”。如果需要加密，支持传入hash值，hash算法：md5(phone)
        //$params["phone"] = "";
        // 用户的邮箱，如果需要加密，支持传入hash值，hash算法：md5(email)
        //$params["email"] = "";
        // 用户的注册IP
        $params["registerIp"] = "";
        // 用户的注册时间，单位：毫秒
        $params["registerTime"] = time() * 1000;
        print_r($params);
        $params["signature"] = $this->gen_signature(SECRET_KEY, $params);
        $params = $this->toUtf8($params);


        $result = $this->curl_post($params, API_URL, API_TIMEOUT);
        if ($result === FALSE) {
            return array("code" => 500, "msg" => "file_get_contents failed.");
        } else {
            return json_decode($result, true);
        }


    }


    // 简单测试
    public function ydmain()
    {
        echo "mb_internal_encoding=" . mb_internal_encoding() . "\n";

        $params = array(
            "nonce"=> "c0bcmo3aa617x1n62tlhtragdd009wy3",
            "version"=> "500",
            "signature"=> "d0bcmo3aa617x1n62tlhtragdd009wy4",
            "token"=> "${riskDataToken}",
            "ip"=>  "1.1.1.1",
            "roleId"=>  "xxxxxxx",
            "account"=>  "yyyyyyy",
            "nickname"=>  "zzzzzzz",
            "server"=>  "水之道",
            "level"=>  "150",
            "gameVersion"=> "1.0.2",
            "assetVersion"=>  "0.2.1",
            "deviceId"=>  "381xfu028f2083u230fuc2",
            "sceneData"=> "",
            "extData"=> ""
        );

        $ret = $this->ydcheck($params);
        var_dump($params);
        var_dump($ret);
        if ($ret["code"] == 200) {
            $data = $ret["data"];
            echo "请求成功，data: {$data}";
            var_dump($ret);
        } else {
            $msg = $ret["msg"];
            echo "请求失败，msg: {$msg}";
            var_dump($ret);
        }
    }

	

}