<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/14
 * Time: 20:12
 */
namespace Admin\Model;
use Think\Model;

class ConfiggoodsModel extends Model{

    private $_config;

    public function _initialize(){
        $file = 'config/夺宝物品数据表.xlsx';
//        $file = iconv('UTF-8','GB2312',$file);
        $this->_config = F('NIUNIUGOODS');

        if($this->_config == null) {
            $data = format_excel2array($file, 1);
            for ($i = 7; $i <= count($data); $i++) {
                if($data[$i]['B'] != ''){
                    $info[$data[$i]['B']] = array(
                        $data[2]['B'] => $data[$i]['B'],
                        $data[2]['C'] => $data[$i]['C'],
                        $data[2]['G'] => $data[$i]['G'],
                    );
                }
            }
            $this->_config = $info;
            F('NIUNIUGOODS', $info);
        }
    }

    public function getConfig() {
        return $this->_config;
    }
	
	//物品名
	public function getGoods() {
		
		$goods = array(
			101 => '金币',
			401 => '钥匙',
			103 => '晶石'
			);

        return $goods;
    }

}