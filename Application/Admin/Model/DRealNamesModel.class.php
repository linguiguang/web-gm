<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/7
 * Time: 18:53
 */
namespace  Admin\Model;
use Think\Model;

class DRealNamesModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG1';

    protected $tableName = 'real_names';

}