<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/19
 * Time: 18:46
 */
namespace Admin\Model;
use Think\Model;

class EverydaygoldstatModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    public function setSheets($room){
        switch ($room){
            case 2 :
                $return = array(
                    '2get' => '金币获得',
                    '2use' => '金币支出',
                    '2pool_win' => '彩池盈利',
                    '2stat' => '总盈利',
                );
            break;
            case 3 :
                $return = array(
                    '3get' => '金币获得',
                    '3use' => '金币支出',
                    '3pool_win' => '彩池盈利',
                    '3task' => '任务',
                    '3box' => '宝箱',
                    '3stat' => '总盈利',
                );
            break;
			 case 4 :
                $return = array(
                    '4get' => '金币获得',
                    '4use' => '金币支出',
                    '4pool_win' => '彩池盈利',
                    '4task' => '任务',
                    '4box' => '宝箱',
                    '4stat' => '总盈利',
                );
            break;
            case 5 :
                $return = array(
                    '5get' => '钥匙获得',
                    '5use' => '钥匙支出',
                    '5stat' => '总盈利',
                    '5fd_redbag' => '福袋红包',
                    '5redbag' => '普通红包',
                );
            break;
            case 10 :
                $return = array(
                    '10get' => '金币获得',
                    '10use' => '金币支出',
                    '10stat' => '总盈利',
                    '10play_num' => '游戏局数',
                    '10gold_free' => '免费产出金币',
                    '10dj_free' => '免费产出钥匙',
                    '10dj_ex' => '兑换产出钥匙',
                );
            break;
            default :
                $return = array(
                'get' => '金币获得',
                'use' => '金币支出',
                'pool_win' => '彩池盈利',
                'stat' => '总盈利',
            );
        }
        return $return;
    }
}