<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DGoldstatdayModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG3';

    protected $tableName = 'goldstatday';   
   
    ///opt/aiya/nginx/fastcgi/bin/php admin.php stat/run
	/**
	 * 金币统计  统计任务控制器   测试金币统计	
	 */
	public function dgold()
	{
		$goldgetuse = D('DResultLogs');
		$stat    = D('DGoldstatday');
		$field   = 'gold';
		$this->drun($goldgetuse, $stat, $field);
	}
    
	/**
	 * @param $get 获得/消耗模型

	 * @param $stat 统计模型
	 * @param $field 字段
	 */
	public function drun($get,$stat, $field)
	{
        

		$stime = microtime(true); #获取程序开始执行的时间
		$content = "---------------------------------金币统计执行开始时间：".date("H:i:s",$stime)."------------------------------------\n";
		
		$d_time = NOW_TIME;

		if ($yes_time = stat_zero_time($d_time)) {
			$b_time = date('Y-m-d 00:00:00',$yes_time);
			$e_time = date('Y-m-d 23:59:59',$yes_time);
			$t_time = date('Y-m-d',$yes_time);
		} else {
			$b_time = date('Y-m-d 00:00:00', $d_time);
			$e_time = date('Y-m-d 23:59:59', $d_time);
			$t_time = date('Y-m-d',$d_time);
		} 

		$map['created_at'] = array('between', array($b_time, $e_time));
		// $map['uid'] = array('egt', 150000);
		
		$rooms = D('DRooms')->rooms();  //房间
        $room = array();
        foreach ($rooms as $k => $v) {
            $room[] = intval($k);
        }

		if ($field == 'gold') {
			$map['room_type'] = array('in', $room); // 房间号1,2,3
		}
        //print_r($map);
		//获得
		$arr_get = $arr_use = array();
		$get_list = $get->where($map)->field("room_type,sum(win) as total_win,sum(bet*attack) as total_cost")->group("room_type")->select();
		
		if ($field == 'gold') {
			
			// goldget
			// room_type,total_win,total_cost		
			$dt = $t_time;  //报表日期
			$jl=0;
			foreach ($get_list as $k => $val) {			
				//总赢取 消耗
				$arr_get[$val['room_type']]['total_win'] = $val['total_win'];
				$arr_use[$val['room_type']]['total_cost'] = $val['total_cost'];
			    if ($arr_get[$val['room_type']]['total_win']>0 || $arr_use[$val['room_type']]['total_cost']>0)
				{
					$jl=1;
				}
			}
			//print_r($arr_get);
			//print_r($arr_use);
			unset($get_list);
			//echo 'getlogs' . PHP_EOL;

			// 金币报表统计
			if ($jl==1)
			{
				$data['date_at'] = $dt;
				$data['get_gold_'.$room[0]] = intval($arr_get[$room[0]]['total_win']);
				$data['get_gold_'.$room[1]] = intval($arr_get[$room[1]]['total_win']);
				$data['get_gold_'.$room[2]] = intval($arr_get[$room[2]]['total_win']);
				$data['use_gold_'.$room[0]] = intval($arr_use[$room[0]]['total_cost']);
				$data['use_gold_'.$room[1]] = intval($arr_use[$room[1]]['total_cost']);
				$data['use_gold_'.$room[2]] = intval($arr_use[$room[2]]['total_cost']);
				$data['updated_at'] = $d_time;
				//print_r($data);
				$sap['date_at'] = $dt;	
				if ($info = $stat->where($sap)->find()) {
					$stat->where(array('id' => $info['id']))->save($data);
					
				} else {
					$data['created_at'] = $d_time;
					//print_r($data);
					$stat->add($data);
				}
			}
			//echo 'SUCCESS' . PHP_EOL;

			$etime = microtime(true);
			$totals = round($etime - $stime);
			$content .= "---------------------------------共计执行：".$totals."s------------------------------------\n";        
            $content .= "---------------------------------金币统计执行结束时间：".date("H:i:s",$etime)."------------------------------------\n";
        
            tasklogger("goldstat",$content);
		}
	}

}