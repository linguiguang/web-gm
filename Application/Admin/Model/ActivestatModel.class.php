<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/17
 * Time: 17:20
 */
namespace Admin\Model;
use Think\Model;
/**
 * 活跃用户统计
 */
class ActivestatModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

}
