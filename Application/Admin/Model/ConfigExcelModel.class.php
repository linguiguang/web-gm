<?php
namespace Admin\Model;
use Think\Model;
//读取excel表配置
class ConfigExcelModel extends Model{

    //读取的字段
    protected $config;

    private $cache = '';

    public function __construct(){
        $constant = EXCEL_COLUMN;
        $this->config = json_decode($constant, true);
    }

    /**
     * 获取缓存数据
     * @param  [type]  $file   [常量#文件]
     * @param  integer $index  [excel索引]
     * @param  boolean $modify [是否修改缓存数据]
     * @return [type]          [array]
     */
    public function get($file, $index = 0, $modify = false){
        list($constant, $fileName) = explode(',', $file);
        $cacheName = $constant.'_'.$index;
        if($cache = $this->getCache($cacheName, $modify)){
            return $cache;
        }
        $filePath = 'config/'.$fileName;
        $data = format_excel2array($filePath, $index);
        //读取配置
        $config = $this->config[$cacheName];
        if($config){
            $keys = explode(',', trim($config, ','));
        }else{
            $getOne = $data[1];
            $keys = array_keys($getOne);
        }

        $cache = array();
        for($i=7; $i<=count($data); $i++){
            foreach ($keys as $key) {
                if(!$data[$i]['B']) continue;
                $cache[$data[$i]['B']][$data[2][$key]] = $data[$i][$key];
            }
        }
        $this->setCache($cacheName, $cache);
        return $cache;
    }

    /**
     * 获取缓存数据
     * @param  [type]  $file   [常量#文件]
     * @param  integer $index  [excel索引]
     * @param  boolean $modify [是否修改缓存数据]
     * @return [type]          [array]
     */
    public function getTable($file, $index = 0){
        list($constant, $fileName) = explode(',', $file);
        $cacheName = sprintf('%s_%s_TABLE', $constant, $index);
        if($cache = $this->getCache($cacheName, $modify)){
            return $cache;
        }
        $filePath = 'config/'.$fileName;
        $data = format_excel2array($filePath, $index);
        //读取配置
        $getOne = $data[1];
        $keys = array_keys($getOne);

        $cache = array();
        //头部内容
        foreach ($keys as $field) {
            if($data[2][$field] == '' || $data[2][$field] == '字段'){
                continue;
            }
            $cache['header'][$data[2][$field]]['key'] = $data[2][$field];
            $cache['header'][$data[2][$field]]['type'] = $data[3][$field];
            $cache['header'][$data[2][$field]]['name'] = $data[4][$field];
        }

        for($i=7; $i<=count($data); $i++){
            foreach ($keys as $key) {
                if(strlen((string)$data[$i]['B']) < 1) continue;
                $cache['data'][$data[$i]['B']][$data[2][$key]] = $data[$i][$key];
            }
        }
        $this->setCache($cacheName, $cache);
        return $cache;
    }

    protected function getCache($cacheName, $modify){
        $cache = F($cacheName);
        if($cache){
            if($modify === true){
                $cache = $this->modifyCache($cacheName, $cache);
            }
            return $cache;
        }
        return false;
    }

    protected function setCache($cacheName, $cacheValue){
        F($cacheName, $cacheValue);
    }

    protected function delCache($cacheName){
        F($cacheName, null);
    }

    //修改缓存并返回
    protected function modifyCache($cacheName, $cacheValue){
        if($cacheName == 'EXCEL_SHOP_DATA_3'){
        }elseif($cacheName == 'EXCEL_GOODS_DATA_1'){
        }
        return $cacheValue;
    }


}
