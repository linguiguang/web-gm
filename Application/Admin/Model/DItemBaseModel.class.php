<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DItemBaseModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG3';

    protected $tableName = 'item_config';
    
	/**
     * 获取物品名
     * 
     */
 
	public function getgoods() {
		$goods = $this->field('id,name,cls')->select();
		$list=[];
	    foreach ($goods as $k => $v) {
			$list[$v['id']]['id'] = $v['id'];
            $list[$v['id']]['name'] = $v['name'];
            $list[$v['id']]['cls'] = $v['cls'];
        }
        return $list;
    }
	

}