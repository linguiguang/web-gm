<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/14
 * Time: 20:12
 */
namespace Admin\Model;
use Think\Model;

class ConfigStatModel extends Model{

    private $_config;

    public function _initialize(){
        $file = 'config/渠道方案1期.xlsx';
        // $file = iconv('UTF-8','GB2312',$file);
        $this->_config = F('NIUNIUSTAT');
        
        if(1) { //!$this->_config
            $data = format_excel2array($file, 0);
            for ($i = 4; $i <= count($data); $i++) {
                if(!empty($data[$i]["C"]) && !empty($data[$i]['D'])) {
                    $info['task']["task_".$i] = array(
                        'task_name' => $data[$i]['C'],
                        'task_num' => $data[$i]['D'],
                        'task_reward' => $data[$i]['E']
                    );
                }
                
                if($data[$i]['G'] > 0 ){
                    $info['rec']['rec_'.$i] = [
                        'task_name' => $data[$i]['G'],
                        'task_num' => intval($data[$i]['H'])
                    ];
                }
                
            }
            $this->_config = $info;
            F('NIUNIUSTAT', $info);
        }
    }

    public function getConfig() {
        return $this->_config;
    }

}