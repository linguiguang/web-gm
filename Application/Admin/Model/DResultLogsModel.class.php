<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DResultLogsModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'result_logs';

    
    public function getusertimesold($uid,$room_type){  

        $cumap['uid'] = $uid;
        $cumap['room_type'] = $room_type; //1,2        
        $uinfo = $this->where($cumap)->select();  //用户当前赢金
        $times = count($uinfo);
        
        return $times;

    }
    public function getusertimes($uid,$room_type){  

        $cumap['uid'] = $uid;
        $cumap['room_type'] = $room_type; //1,2        
        $times = $this->where($cumap)->count();  //用户当前赢金        
        return $times;

    }
    /**
     * 获取用户信息
     */
    public function getUserInfo($ids = array()){
        if(empty($ids)) return array();
        $data = $this->where(array('id'=>array('in', $ids)))->select();
        $list = array();
        foreach ($data as $v){
            $list[$v['id']] = $v;
        }
        return $list;
    }

    

}