<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DRanksModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'ranks';

    /**
     * 获取排行榜类型
   
     */
    public function get_ranktype(){       
        return C('RANKTYPE');
    }
    /**
     * 获取排行榜类型
   
     */
    public function get_ranklst($r_type,$limit){  

        $ranks = D("DRanks");
        $users = D("DUsers");
        $redbag_list = []; //1 => "卡拉姆沙漠日榜-投注"
        $redbagList = $ranks->where(['r_type' => $r_type])
                            ->field('uid,value1')->order("value1 desc")->limit($limit)->select();
        
        // var_dump($redbagList);exit;

        foreach ($redbagList as $k => $val) {

            $getUser = $users->where(['id'=> $val['uid']])->field('name,tel')->find();
            $redbagList[$k]['username'] = $getUser['name'] ? $getUser['name']: $getUser['tel'];
            $redbagList[$k]['value1'] = $val['value1'];
            if(count($redbag_list) < 50 && !empty($redbagList[$k]['username'])) {
                $redbag_list[] = $redbagList[$k];
            }
        }

        $last_names = array_column($redbag_list,'value1');
        array_multisort($last_names,SORT_DESC,$redbag_list);

        return $redbag_list;
    }

}