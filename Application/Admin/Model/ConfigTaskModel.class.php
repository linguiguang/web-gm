<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/2
 * Time: 11:28
 */
namespace Admin\Model;
use Think\Model;

class ConfigTaskModel extends Model{

    private $_config;

    public function _initialize(){
        $file = 'config/牛牛任务表.xlsx';
//        $file = iconv('UTF-8','GB2312',$file);
        $this->_config = F('NIUNIUTASKS');

        $info = array();
        if($this->_config == null) {
            for ($j=1; $j<5; $j++) {
                $data = format_excel2array($file, $j);
                for ($i = 7; $i <= count($data); $i++) {
                    if ($data[$i]['B'] != '') {
                        $info[$data[$i]['B']] = array(
                            'taskid' => $data[$i]['B'],
                            'name' => $data[$i]['E'],
                            'goods_id' => $data[$i]['K'],
                            'num' => $data[$i]['L'],
                        );
                    }
                }
            }

            $activitys = format_excel2array('config/牛牛活动数据表.xlsx', 3);
            for ($i=7; $i<count($activitys); $i++){
                if($activitys[$i]['B'] != ''){
                    $info[$activitys[$i]['B']] = array(
                        'taskid' => $activitys[$i]['B'],
                        'name' => $activitys[$i]['F'],
                        'goods_id' => $activitys[$i]['G'],
                        'num' => $activitys[$i]['D'],
                    );
                }
            }

            $this->_config = $info;
            F('NIUNIUTASKS', $info);
        }
    }

    public function tasks(){
        $config = $this->_config;
        $array_other = array(
            119 => array(
                'taskid' => 119,
                'name' => '拉霸(百人)大厅活动',
                'goods_id' => 101
            ),
            1007 => array(
                'taskid' => 1007,
                'name' => '赚金榜日榜',
                'goods_id' => 101
            ),
            1008 => array(
                'taskid' => 1008,
                'name' => '赚金榜周榜',
                'goods_id' => 101
            ),
        );
        $config += $array_other;
        return $config;
    }

}