<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DWbUserListModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG4';

    protected $tableName = 'wb_user_list';
}