<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DMonipayModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG3';

    protected $tableName = 'monipay';

    //单个渠道充值人数和金额
    public function fromRecharge($from_id){
        $recharge = $this->field('d_pays.account,d_pays.price')->join('d_accounts as u on d_pays.account = u.id where u.from_id = '.$from_id.' and d_pays.status = 1 ')->select();

        $man = array();
        $money = 0;
        for($i=0;$i<count($recharge);$i++){
            $man[] = $recharge[$i]['account'];
            $money += $recharge[$i]['price'];
        }

        $data['man'] = count(array_unique($man));
        $data['price'] = $money;

        return $data;
    }

    //渠道充值人数和金额
    public function allFromRecharges($from_ids, $b_time = '', $e_time = ''){
        if($b_time != '' && $e_time != ''){
			$e_time = date("Y-m-d H:i:s",strtotime($e_time)+86399);
            $map['created_at'] = array('between', array($b_time, $e_time));
        }elseif($b_time == '' && $e_time != ''){
			$e_time = date("Y-m-d H:i:s",strtotime($e_time)+86399);
            $map['created_at'] = array('elt', $e_time);
        }elseif($b_time != '' && $e_time == ''){			
            $map['created_at'] = array('egt', $b_time);
        }
        $map['status'] = 1;
        $map['from_id'] = array('in', $from_ids);

        $list = $this->where($map)->field('account,price,from_id')->select();
        $ret = $data = array();
        foreach ($list as $v){
            $ret[$v['from_id']]['money'] += $v['price'];
            $ret[$v['from_id']]['uid'][] = $v['account'];
        }
        foreach ($ret as $k=>$v){
            $data[$k]['money'] = intval($v['price']);
            $data[$k]['man'] = count(array_unique($v['account']));
        }
        return $data;
    }

}