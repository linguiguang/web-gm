<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/7
 * Time: 18:53
 */
namespace  Admin\Model;
use Think\Model;

class DChannelUserTasksModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'channel_user_tasks';

    /**
     * 获取关卡
     * $type string  $uid,$from_id,$from_time,$room_type,$task_type
     */
    public function getuserstage($uid,$from_id,$from_time,$room_type,$task_type){
       
        $ctask = D('DChannelTasks');

        $cumap['uid'] = $uid;  
        $cumap['from_id'] = $ctmap['from_id'] = $from_id;
        $cumap['from_time'] = $ctmap['from_time'] = $from_time;
        $cumap['room_type'] = $ctmap['room_type'] = $room_type; //1,2
        $cumap['task_type'] = $ctmap['task_type'] = $task_type; //1
       
        $userwin = $this->where($cumap)->field('value,created_at')->find();  //用户当前赢金
        $ctmap['value'] = array("elt",intval($userwin['value']));
        $stage['value'] = $userwin['value'];
		$stage['created_at'] = $userwin['created_at'];
        $stage['level'] = $ctask->where($ctmap)->order("level desc")->getField('level');
        return $stage;
    }

    public function gettaskconfigid($uid,$from_id,$from_time,$room_type,$task_type){
        $content ='该文件位于 " '  . __FILE__ . ' " ---\n';
        $content .= '函数名为：' . __FUNCTION__. ' " ---\n';
        $ctask = D('DChannelTasks');

        $cumap['uid'] = $uid;  
        $cumap['from_id'] = $ctmap['from_id'] = $from_id;
        $cumap['from_time'] = $ctmap['from_time'] = $from_time;
        $cumap['room_type'] = $ctmap['room_type'] = $room_type; //1,2
        $cumap['task_type'] = $ctmap['task_type'] = $task_type; //1
        
        $userwin = $this->where($cumap)->getField('value');  //用户当前赢金
        $ctmap['value'] = array("elt",intval($userwin));

        $stage = $ctask->where($ctmap)->field("id,level,value,reward")->select();

        $content .= "这是第 ". __LINE__ ." 行---------gettaskconfig：".$ctask->getLastSql()."------------stage：".var_export($stage,true);
  
		//tasklogger("taskstat",$content);
        return $stage;
    }

}