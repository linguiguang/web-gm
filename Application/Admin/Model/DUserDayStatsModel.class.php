<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/7
 * Time: 18:53
 */
namespace  Admin\Model;
use Think\Model;

class DUserDayStatsModel extends Model{

    protected $tablePrefix = 'd_';
    protected $connection = 'DB_CONFIG3';
    protected $tableName = 'user_day_stats';

    /**
	 * 获取信息流统计数据
     * 
	 */
	
	public function get_xin_xi_liu_stat($from_id,$b_time,$e_time)
	{
        // 执行原生 SQL 查询
        $time="reg_day BETWEEN ''".$b_time."'' AND ''".$e_time."''";
        $where="";
        if (!empty($from_id))
        {  
            $where=" WHERE from_id = ".$from_id." AND ".$time;
        }
        else{ $where=" WHERE  ".$time;}

        $sql="CALL GetStatsByDateRange('".$time."', '".$where."');";
        $result = $this->query($sql);
        
        // 返回查询结果
        return $result;
	}

    /**
	 * 获取信息流统计数据-渠道消耗
     * 
	 */
	
	public function get_d_task_stat($from_id,$b_time,$e_time)
	{
        // 执行原生 SQL 查询
        $time="reg_day BETWEEN ''".$b_time."'' AND ''".$e_time."''";
        $where="";
        if (!empty($from_id))
        {  
            $where=" WHERE from_id = ".$from_id." AND ".$time;
        }
        else{ $where=" WHERE  ".$time;}

        $sql="CALL GettasksByDateRange('".$time."', '".$where."');";
        $result = $this->query($sql);
        
        // 返回查询结果
        return $result;
	}

        /**
	 * 渠道当日数据
     * 
	 */
	
	public function get_this_day()
	{
        // 执行原生 SQL 查询
         

        $sql="CALL GetQuDaoFenXi();";
        $result = $this->query($sql);
        
        // 返回查询结果
        return $result;
	}

     /**
	 * 注册用户，游戏用户，充值人数
     * 
	 */
	
	public function get_user_new()
	{
        // 执行原生 SQL 查询
        $sql="CALL GetUserNew();";
        $result = $this->query($sql);
        
        // 返回查询结果
        return $result;
	}

        function getchongfan($date_at)
        {
                
                $consumeMap['day'] = $date_at;
                  
                $consumeList = D('DUserDayStats')->where($consumeMap)
                ->field('SUM(pays_reward) AS reward')
                ->select();
                $all_consume=0;
                foreach ($consumeList as $val) {
                $all_consume += $val['reward'];
                }
                return $all_consume;
        }
        public function get_this_day_by_time($account,$b_time,$e_time)
	{
                // 执行原生 SQL 查询
                $time=" BETWEEN ''".$b_time."'' AND ''".$e_time."''";
                $where="";
                if (!empty($account))
                {  
                    $where=" and account = ".$account." ";
                }

                $sql="CALL GetQuDaoFenXiByTime('".$time."', '".$where."');";
                $result = $this->query($sql);
                
                // 返回查询结果
                return $result;
	}
        public function get_xin_xi_liu_all($b_time)
	{
                // 执行原生 SQL 查询
                $time="''".$b_time."''";

                $sql="CALL GetStatsByDateAll('".$time."');";
                $result = $this->query($sql);
                
                // 返回查询结果
                return $result;
	}
 


}