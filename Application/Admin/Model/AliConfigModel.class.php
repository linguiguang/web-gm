<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class AliConfigModel extends Model
{
    protected $tablePrefix = 'rui_';
    protected $tableName = 'ali_config';
}