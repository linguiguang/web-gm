<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DUsersModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';  //gamebox

    protected $tableName = 'users';
	
    /**
     * 获取刷子记录
     * 
     */
    public function get_brushlog($uid,$mapbr){
		$brush = D('BrushLog')->where($mapbr)->field('brush_times,state,return')->find();
        return $brush;
    }
	
    /**
     * 获取角色ID
     * $type string  nickname=>昵称, mobile=>手机号
     */
    public function get_charid($type= 'nickname', $val){
		$user = D('DAccounts');
        $userinfo = D('DUsers');
        if($type == 'nickname'){  //d_users->name
            $char = $userinfo->where(array('name'=>$val))->field('id')->find();
        }elseif($type == 'mobile') {  //d_accounts->name
            $uid = $user->where(array('tel_no' => $val))->getField('id');
            if(!$uid) return 0;
            $char = $userinfo->where(array('account' => intval($uid)))->field('id')->find();
        }elseif($type == 'uid'){
            $char = $userinfo->where(array('account' => intval($val)))->field('id')->find();
        }elseif($type == 'devid'){
            $char = $user->where(array('devid' => intval($val)))->field('id')->find();
        }        
        $char_id = $char['id'];
        return intval($char_id);
    }

    /**
     * 获取账号ID
     * $type string  nickname=>昵称, mobile=>手机号
     */
    public function get_uid($type= 'nickname', $val){
        $account = D('DAccounts');
        $userinfo = D('DUsers');
		
        if($type == 'nickname'){
            $user = $userinfo->where(array('name'=>$val))->field('account')->find();
			$user['uid'] = $user['account'];
        }elseif($type == 'mobile') {
            $user = $account->where(array('name'=>$val))->find();
            $user['uid'] = $user['id'];
        }elseif($type == 'char_id'){
            $user = $userinfo->where(array('id'=>$val))->field('account')->find();
			$user['uid'] = $user['account'];
        }
        $uid = $user['uid'];
        return intval($uid);
    }

    /**
     * 获取用户信息
     */
    public function getUserInfo($ids = array()){
		
        if(empty($ids)) return array();
        $data = $this->where(array('account'=>array('in', $ids)))->select();
        $list = array();
        foreach ($data as $v){
            $list[$v['account']] = $v;
        }
        return $list;
    }
	
	/**
     * 角色ID读取用户数据
     */
    public function getusers($charids = array()){
        if(empty($charids)) return array();
        $data = $this->where(array('id'=>array('in', $charids)))->select();
        $list = array();
        foreach ($data as $v){
            $list[$v['account']] = D('DAccounts')->where(array('id'=>$v['account']))->find();
        }
        return $list;
    }

    /**
     * 用户数  改时间类型datetime查询
     */
    public function fromUser($from_id, $b_time = '', $e_time = ''){

        if($b_time != ''){
            $map['created_at'][] = array('egt', $b_time);
        }

        if($e_time != ''){
			$e_time = date("Y-m-d H:i:s",strtotime($e_time)+86399);
            $map['created_at'][] = array('elt', $e_time);
        }

        $map['id'] = array('egt', 150000);
        $map['from_id'] = $from_id;
        $num = $this->where($map)->count();

        $map['siteuser'] = 1;
        $bind = $this->where($map)->count();

        $data = array(
            'num' => $num,
            'bind' => $bind
        );

        return $data;
    }

}