<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/29
 * Time: 15:50
 */
namespace Admin\Model;
use Think\Model;

class ConfigfishesModel extends Model{

    private $_config;

    public function _initialize(){
        $file = 'config/捕鱼鱼类NPC数据表.xlsx';
//        $file = iconv('UTF-8','GB2312',$file);
        $this->_config = F('FISHES');

        if($this->_config == null) {
            $data = format_excel2array($file, 1);
            for ($i = 7; $i <= count($data); $i++) {
                $info[$data[$i]['AV']] = array(
                    'id'   => $data[$i]['AV'],
                    'name' => $data[$i]['AU'],
                );
            }
            $this->_config = $info;
            F('FISHES', $info);
        }
    }

    public function getConfig() {
        return $this->_config;
    }

}