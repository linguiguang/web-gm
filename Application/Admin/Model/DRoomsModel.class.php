<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DRoomsModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'rooms';
	
	/**
     * 获取游戏房间名称
     * 
     */
    public function rooms()
    {		
        return C('ROOMS');
    }
	
	/**
     * 
     * rooms 关联 user_rooms 获取游戏统计值
     */
    public function get_user_rooms($uid,$room,$files)
    {
		$map=[];
		if(!empty($uid)){
			$map['uid']=$uid;
		}
		if(!empty($room)){
			$map['room_type']=$room;
		}
	
		$userrooms = D('DUserRooms')->where($map)->field($files)->find();
		
        return $userrooms[$files];
    }





    /**
     * 获取角色ID
     * $type string  nickname=>昵称, mobile=>手机号
     */
    public function get_charid($type= 'nickname', $val){
        $userinfo = D('Userinfo');

        if($type == 'nickname'){
            $char = $userinfo->where(array('nickname'=>$val))->field('char_id')->find();
        }elseif($type == 'mobile') {
            $uid = $this->where(array('username' => $val))->getField('id');
            if(!$uid) return 0;
            $char = $userinfo->where(array('uid' => intval($uid)))->field('char_id')->find();
        }elseif($type == 'uid'){
            $char = $userinfo->where(array('uid' => intval($val)))->field('char_id')->find();
        }elseif($type == 'devid'){
            $char = $userinfo->where(array('devid' => intval($val)))->field('char_id')->find();
        }
        
        $char_id = $char['char_id'];
        return intval($char_id);
    }

    /**
     * 获取账号ID
     * $type string  nickname=>昵称, mobile=>手机号
     */
    public function get_uid($type= 'nickname', $val){
        $userinfo = D('Userinfo');

        if($type == 'nickname'){
            $user = $userinfo->where(array('nickname'=>$val))->field('uid')->find();
        }elseif($type == 'mobile') {
            $user = $this->where(array('username'=>$val))->find();
            $user['uid'] = $user['id'];
        }elseif($type == 'char_id'){
            $user = $userinfo->where(array('char_id'=>$val))->field('uid')->find();
        }
        $uid = $user['uid'];
        return intval($uid);
    }

    /**
     * 获取用户信息
     */
    public function getUserInfo($ids = array()){
        if(empty($ids)) return array();
        $data = $this->where(array('id'=>array('in', $ids)))->select();
        $list = array();
        foreach ($data as $v){
            $list[$v['id']] = $v;
        }
        return $list;
    }

    /**
     * 用户数
     */
    public function fromUser($from_id, $b_time = '', $e_time = ''){

        if($b_time != ''){
            $map['adddate'][] = array('egt', strtotime($b_time));
        }

        if($e_time != ''){
            $map['adddate'][] = array('elt', strtotime($e_time)+86399);
        }

        $map['id'] = array('egt', 150000);
        $map['from_id'] = $from_id;
        $num = $this->where($map)->count();

        $map['siteuser'] = 1;
        $bind = $this->where($map)->count();

        $data = array(
            'num' => $num,
            'bind' => $bind
        );

        return $data;
    }
    
    /**
     * 获取游戏房间名称-总
     * 
     */
    public function rooms_new()
    {		
        $data=C('ROOMS_NEW');
        $newData=array();
        foreach ($data as $roomId => $room) {
            $newData[$roomId] = $room['name'];
        }
        return $newData;
    }

    /**
     * 获取游戏房间名称-总
     * 
     */
    public function rooms_new_all()
    {		
        $data=C('ROOMS_NEW');
        $newData=[];
        foreach ($data as $roomId => $room) {
            if(empty($room['sub_rooms']))
            {
                $newData[$roomId] = $room['name'];
            }else{
                foreach ($room['sub_rooms'] as $subroomId => $subroom) 
                {
                    $newData[$subroomId] = $subroom;
                }
            }
            
        }
        return $newData;
    }

}