<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/19
 * Time: 18:46
 */
namespace Admin\Model;
use Think\Model;

class ShareModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    //获取状态
    public function getState($char_id, $re_char_ids){
        $map['uid'] = $char_id;
        $map['re_uid'] = array('in', $re_char_ids);
        $map['type'] = 2;
        $list = $this->where($map)->select();
        $data = array();
        foreach ($list as $v){
            $data[$v['re_uid']] = 1;
        }
        return $data;
    }
}