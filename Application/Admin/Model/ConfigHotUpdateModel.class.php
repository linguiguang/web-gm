<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/2
 * Time: 11:28
 */
namespace Admin\Model;
use Think\Model;

class ConfigHotUpdateModel extends Model{

    private $_config;

    public function _initialize(){
        header("Content-type: text/html; charset=utf-8");
    }
    
    //读取目录生成erl文件
    public function generateConfig()
    {
        $dir = 'config/hotUpdate/';  //Excel文件
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){
                    $file_list[] = $file;
                }
                closedir($dh);
            }   
        }
        
        unset($file_list[0], $file_list[1]);
        sort($file_list);
        // var_dump($file_list);exit;
        
        //先删除 config/hotconfig 目录文件下的文件
        $hotconfigFileName = 'config/hotconfig/';
        if (is_dir($hotconfigFileName)){
            if ($dh = opendir($hotconfigFileName)){
                while (($file = readdir($dh)) !== false){
                    $res = unlink($hotconfigFileName.$file);
                }
                closedir($dh);
            }   
        }
      
       
        
        foreach ($file_list as $k => $item) {
            // var_dump($item);exit;
            if(1) { //$k >= 2  //$item == 'base_vip.xlsx'
                $file = $dir.$item;
                $data = format_excel2array($file, 0);
                $count = count(array_filter($data[1]));  //列数量长度
                
                $sName = explode('.', $item);
                $fileName = $sName[0].'.erl';  //文件名
                
                $str = '';
                
                for ($i = 4; $i <= count($data); $i++) {
                    
                    if(isset($data[$i]["A"])) {
                        
                        $itemData = array_slice($data[$i], 0 ,$count);
                        
                        $str .= '{'. $sName[0].', ';
                        foreach ($itemData as $k => $v) {
                            if($k != "A" ) {
                                $str .= ', ';
                            }
                            if(isset($v)) {
                                $str .= $v;
                            } else {
                                $str .= '""';
                            }
                        }
                        $str .= "}.\n";
                    }
                    
                    // if(!empty($data[$i]["A"])) {
                    //     $str .='{'.$sName[0]." ,". implode(' ,', $itemData)."}.\n";
                    // }
                    unset($itemData);
                }
                
                //写入文件 覆盖
                $f_open = fopen('config/hotconfig/'.$fileName, 'w+');
                $res = fwrite($f_open, $str);
                fclose($f_open);
                unset($data);
                
               
            }
        }
    }

    public function buildConfig(){
        // $dir = 'config/hotconfig/';
        $dir = 'config/hotconfig/';
        // $dir = 'config/hotUpdate/';
        $file_content = null;

        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){
                    $file_list[] = $file;
                }
                closedir($dh);
            }   
        }
        foreach ($file_list as $k => $item) {
            if($k >= 2) {
                $file = $dir.$item;
                
                $file_content .= file_get_contents($file);
               
            }
        }

        $file_content .= $this->getConstConfig();

        if(!file_exists('config/game.config')) {
            $f_open = fopen('config/game.config', 'w');
        } else {
            $f_open = fopen('config/game.config', 'w+');
        }

        $res = fwrite($f_open, $file_content);
        fclose($f_open);
        
        return $res;
    }
    
    public function getHotconfigPath()
    {
        $dir = 'config/hotUpdate/';  //Excel文件
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){
                    $sName = explode('.', $file);
                    $file_list[] = $sName[0];
                }
                closedir($dh);
            }   
        }
        sort($file_list);
        unset($file_list[0], $file_list[1]);
        return $file_list;
    }
    
    //获取常量文件配置
    public function getConstConfig()
    {
        $file = 'config/const_config';
        return file_get_contents($file);
    }

}