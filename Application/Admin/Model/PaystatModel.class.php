<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/17
 * Time: 10:59
 */
namespace Admin\Model;
use Think\Model;

class PaystatModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'mg_paystat';

}