<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/15
 * Time: 11:20
 */
namespace Admin\Model;
use Think\Model;

class VipModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'mg_vip';

}