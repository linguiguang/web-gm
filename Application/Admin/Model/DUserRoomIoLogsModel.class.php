<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DUserRoomIoLogsModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'user_room_io_logs';
    /* 
    房间游戏时长
    $b_time,$e_time
    查询时间段游戏时长
    */
    public function getusertimes($uid,$room_type,$b_time,$e_time)
    {        
        if (isset($b_time) && isset($e_time)) {
            $b_time = strtotime($b_time);
            $e_time = strtotime($e_time);
            $map['in_time'] = array('between', array($b_time, $e_time));
     
        } elseif (!isset($b_time) && isset($e_time)) {           
            $e_time = strtotime($e_time);
            $map['in_time'] = array('elt', $e_time);
        } elseif (isset($b_time) && !isset($e_time)) {
            $b_time = strtotime($b_time);
            $map['in_time'] = array('egt', $b_time);
        }
        if (!isset($uid))
        {  
            $map['uid']=$uid;
        }
        if (!isset($room_type))
        {       
            $map['room_type']=$room_type;
        }
        $times = $this->where($map)->field("sum(out_time-in_time) as times");
        return $times;
    }

   

}