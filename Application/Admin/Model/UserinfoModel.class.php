<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/18
 * Time: 16:23
 */
namespace Admin\Model;
use Think\Model;

class UserinfoModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    /**
     * id获取角色信息(默认通过用户id获取角色信息)
     */
    public function getUserInfo($ids = array(), $type ='uid'){
        if(empty($ids)) return array();

        $data = $this->where(array($type=>array('in', $ids)))->select();
        $list = array();
        foreach ($data as $v){
            $list[$v[$type]] = $v;
        }
        return $list;
    }

    /**
     * 获取单个角色
     */
    public function getChar($id){
        if(empty($id)) return '';
        $char = $this->where(array('uid'=>$id))->find();
        return $char;
    }

    /**
     * 角色ID读取用户数据
     */
    public function getusers($charids = array()){
        if(empty($charids)) return array();
        $data = $this->where(array('char_id'=>array('in', $charids)))->select();
        $list = array();
        foreach ($data as $v){
            $list[$v['char_id']] = D('Users')->where(array('id'=>$v['uid']))->find();
        }
        return $list;
    }

}