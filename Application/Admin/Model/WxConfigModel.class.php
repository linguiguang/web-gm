<?php
// +----------------------------------------------------------------------
// | XiaoQiGameServer
// +----------------------------------------------------------------------
// | Copyright (c) 2016
// +----------------------------------------------------------------------
// | Author: 汪伟麟 <coolker111@gmail.com> <http://www.coolker.com>
// +----------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

/**
 * 获取微信配置
 */
class WxConfigModel extends Model{

    /**
     * 返回配置
     * @param null $key
     * @return array|bool|mixed|null
     */
     
    protected $tablePrefix = 'rui_';

    protected $tableName = 'wx_config';
    
	public function getWxConfig($key= null) {

        $map['is_open'] = 1;
        $config = $this->where($map)->find();
        if(!empty($key)) {
            return $config[$key];
        }
		return $config;
 	}


}
