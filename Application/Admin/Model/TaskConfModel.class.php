<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/14
 * Time: 20:12
 */
namespace Admin\Model;
use Think\Model;

class TaskConfModel extends Model{

    private $_config;

    public function _initialize()
    {
        
        $file = 'config/channel_plan.xlsx';
        // $file = iconv('UTF-8','GB2312',$file);
        // $this->_config = F('NIUNIUSTAT');
        $data = format_excel2array($file, 0);
        
        $info = [];
        for ($i = 4; $i <= count($data); $i++) {
            if(!empty($data[$i]['B'])) {
                $info[$i]['from_id'] = $data[$i]['B'];
                $info[$i]['task_type'] = $data[$i]['C'];
                $info[$i]['desc'] = $data[$i]['D'];
                $info[$i]['value'] = $data[$i]['E'];
                $info[$i]['reward'] = $data[$i]['F'];
                $info[$i]['rw_cond'] = $data[$i]['G'];
                $info[$i]['from_time'] = $data[$i]['H'];
                $info[$i]['room_type'] = $data[$i]['I'];
                $info[$i]['level'] = $data[$i]['J'];
                $info[$i]['limit'] = $data[$i]['K'];
            }
        }
        
        $this->_config = $info;
    }

    public function getConfig() {
        return $this->_config;
    }

}