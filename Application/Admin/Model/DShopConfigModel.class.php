<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DShopConfigModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG1';

    protected $tableName = 'shop_config';
	
}