<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Model;

use Think\Model;

/**
 * 用户模型
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */

class MemberModel extends Model
{

    protected $_validate = array(
        array('nickname', '1,16', '昵称长度为1-16个字符', self::EXISTS_VALIDATE, 'length'),
        array('nickname', '', '昵称被占用', self::EXISTS_VALIDATE, 'unique'), //用户名被占用
    );

    public function lists($status = 1, $order = 'uid DESC', $field = true)
    {
        $map = array('status' => $status);
        return $this->field($field)->where($map)->order($order)->select();
    }

    /**
     * 登录指定用户
     * @param  integer $uid 用户ID
     * @return boolean      ture-登录成功，false-登录失败
     */
    public function login($uid, $keyData)
    {
        /* 检测是否在当前应用注册 */
        $user = $this->field(true)->find($uid);
        if (!$user || 1 != $user['status']) {
            $this->error = '用户不存在或已被禁用！'; //应用级别禁用
            return false;
        }

        //记录行为
        action_log('user_login', 'member', $uid, $uid);

        /* 登录用户 */
        $this->autoLogin($user, $keyData);
        return true;
    }

    /**
     * 注销当前用户
     * @return void
     */
    public function logout()
    {
        cookie('user_auth', null);
        cookie('user_auth_sign', null);
    }

    /**
     * 加密函数
     * @param string $txt 需要加密的字符串
     * @param string $key 加密key
     * @param string $localIV 偏移向量
     * @return string 返回加密结果
     */
    function encrypt($txt, $localIV = "726df1ae-0462-40ee-b449-690467adbf75", $key = "885467d8-9d87-4a18-b952-d11e4503f0ef")
    {
        $input = $txt;
        // 加密
        $module = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, $localIV);
        mcrypt_generic_init($module, $key, $localIV);
        $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = $size - (strlen($input) % $size);
        $input = $input . str_repeat(chr($pad), $pad);
        $encrypted = mcrypt_generic($module, $input);
        mcrypt_generic_deinit($module);
        mcrypt_module_close($module);
        $encrypted = base64_encode($encrypted);
        return $encrypted;
    }

    /**
     * 解密函数
     * @param string $txt 需要解密的字符串
     * @param string $key 密匙
     * @return string 字符串类型的返回结果
     */

    function decrypt($txt, $localIV = "726df1ae-0462-40ee-b449-690467adbf75", $key = "885467d8-9d87-4a18-b952-d11e4503f0ef")
    {
        // 解密
        $input = $txt;
        $module2 = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, $localIV);
        mcrypt_generic_init($module2, $key, $localIV);
        $encryptedData = base64_decode($input);
        $encryptedData = mdecrypt_generic($module2, $encryptedData);
        $dec_s = strlen($encryptedData);
        $padding = ord($encryptedData[$dec_s - 1]);
        $decrypted = substr($encryptedData, 0, -$padding);
        mcrypt_generic_deinit($module2);
        mcrypt_module_close($module2);
        return $decrypted;
    }

    /**
     * 自动登录用户
     * @param  integer $user 用户信息数组
     */
    private function autoLogin($user, $keyData)
    {
        /* 更新登录信息 */

        $data = array(
            'uid'             => $user['uid'],
            'login'           => array('exp', '`login`+1'),
            'last_login_time' => NOW_TIME,
            'last_login_ip'   => get_client_ip(1),
        );
        $this->save($data);

        /* 记录登录SESSION和COOKIES */
        $auth = array(
            'uid'             => encrypt($user['uid'], $keyData['iv'], $keyData['key']),
            'username'        => encrypt($user['nickname'], $keyData['iv'], $keyData['key']),
            'last_login_time' => encrypt($user['last_login_time'], $keyData['iv'], $keyData['key']),
            'id'              => $keyData['id'],
            'login_time'      => encrypt($user['last_login_time'], $keyData['iv'], $keyData['key']),
        );

        cookie('user_auth', $auth);
        cookie('user_auth_sign', encrypt(data_auth_sign($auth), $keyData['iv'], $keyData['key']));
    }

    public function getNickName($uid)
    {
        return $this->where(array('uid' => (int) $uid))->getField('nickname');
    }
}
