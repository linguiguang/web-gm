<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/15
 * Time: 15:05
 */
namespace Admin\Model;
use Think\Model;

class PaylogModel extends Model{

    protected $tablePrefix = 'rui_';

    //protected $connection = 'DB_CONFIG3';

    protected $tableName = 'pay';

    //单个渠道充值人数和金额
    public function fromRecharge($from_id){
        $recharge = $this->field('rui_pay.uid,rui_pay.money')->join('rui_user as u on rui_pay.uid = u.id where u.from_id = '.$from_id.' and rui_pay.state = 3 ')->select();

        $man = array();
        $money = 0;
        for($i=0;$i<count($recharge);$i++){
            $man[] = $recharge[$i]['uid'];
            $money += $recharge[$i]['money'];
        }

        $data['man'] = count(array_unique($man));
        $data['money'] = $money;

        return $data;
    }

    //渠道充值人数和金额
    public function allFromRecharges($from_ids, $b_time = '', $e_time = ''){
        if($b_time != '' && $e_time != ''){
            $map['addtime'] = array('between', array(strtotime($b_time), strtotime($e_time)+86399));
        }elseif($b_time == '' && $e_time != ''){
            $map['addtime'] = array('elt', strtotime($e_time)+86399);
        }elseif($b_time != '' && $e_time == ''){
            $map['addtime'] = array('egt', strtotime($b_time));
        }
        $map['state'] = 3;
        $map['from_id'] = array('in', $from_ids);

        $list = $this->where($map)->field('uid,money,from_id')->select();
        $ret = $data = array();
        foreach ($list as $v){
            $ret[$v['from_id']]['money'] += $v['money'];
            $ret[$v['from_id']]['uid'][] = $v['uid'];
        }
        foreach ($ret as $k=>$v){
            $data[$k]['money'] = intval($v['money']);
            $data[$k]['man'] = count(array_unique($v['uid']));
        }
        return $data;
    }

}