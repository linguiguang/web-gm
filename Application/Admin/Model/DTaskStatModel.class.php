<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/7
 * Time: 18:53
 */
namespace  Admin\Model;
use Think\Model;

class DTaskStatModel extends Model{

    protected $tablePrefix = 'd_';
    protected $connection = 'DB_CONFIG3';
    protected $tableName = 'task_stat';

    /**
	 * 渠道期数统计  统计任务控制器  每日执行00:05:00
     * 10分钟执行一次	
	 */
	
	public function do_task_stat()
	{        
        
		$stime = microtime(true); //获取程序开始执行的时间
        $content ='该文件位于 " '  . __FILE__ . ' " ---\n';
        $content .= '函数名为：' . __FUNCTION__. ' " ---\n';
        $content = "---------------------------------渠道统计执行开始时间：".date("H:i:s",$stime)."------------------------------------\n";
       
        $d_time = NOW_TIME;

		if ($stime == stat_zero_time($d_time)) {
			$b_time = date('Y-m-d H:i:s',$stime-300);
			$e_time = date('Y-m-d H:i:s',$stime);
			$t_time = date('Y-m-d',$stime);
		} else {
			$b_time = date('Y-m-d H:i:s', $d_time-100);
			$e_time = date('Y-m-d H:i:s', $d_time);
			$t_time = date('Y-m-d',$d_time);
		} 
        $content.="-------统计开始时间：".$b_time."------\n";
        $content.="-------统计结束时间：".$e_time."------\n";

		$map['updated_at'] = array('between', array($b_time, $e_time));
        //更新用戶的期數
        $rooms = D('DRooms')->rooms();  //房间
        $room = array();
        $room[] = 0;
        foreach ($rooms as $k => $v) {
            $room[] = intval($k);
        }
		
		$map['room_type'] = array('in', $room); // 房间号1,2,3
	
        $get = D('DChannelUserTasks');
     
        $users = D('DUsers');
        $tcost = D('DTaskCost');
        $tstat = D('DTaskStat');
        $tlevel = D('DTaskLevel');
        $trecord = D('DTaskRecord');
        // $fromkey = D('DChannels');
        
		//获得用户房间输赢日统计
		$get_list = $get->where($map)                      
                        ->field("uid,from_id,from_time,room_type,task_type,value as total_win,value2 as total_cost")                     
                        ->select();		
            //$content .= "这是第 ". __LINE__ ." 行-----------------查询sql：".$get->getLastSql()."---------\n"; 
 
            $u_ids = $arr_stat = $arr_cost = array();	
			$dt = $t_time;  //报表日期
			$jl=0;
			if ($get_list)
			{
				foreach ($get_list as $k => $val) {
					//消耗                
					$user_id = $users->get_uid("char_id",$val["uid"]);
					//print($users->getLastSql());    
				   
					//存入task_cost
					$arr_cost[$val["from_id"]][$val["from_time"]][$val["room_type"]][$dt]['date_at'] = $dt;
					$arr_cost[$val["from_id"]][$val["from_time"]][$val["room_type"]][$dt]['from_time'] = $val["from_time"];
					$arr_cost[$val["from_id"]][$val["from_time"]][$val["room_type"]][$dt]['from_id'] = $val["from_id"];            
					$arr_cost[$val["from_id"]][$val["from_time"]][$val["room_type"]][$dt]['room_type'] = $val["room_type"];
					$arr_cost[$val["from_id"]][$val["from_time"]][$val["room_type"]][$dt]['all_consume'] += $val['total_cost'];            
				   // print_r($arr_cost);
					$u_ids[] = $val['uid'];  //d_users->id;
					//存入task_stat
					$arr_stat[$val["from_id"]][$val["from_time"]][$val["room_type"]][$val['uid']]['from_time'] = $val["from_time"];
					$arr_stat[$val["from_id"]][$val["from_time"]][$val["room_type"]][$val['uid']]['from_id'] = $val["from_id"];
					$arr_stat[$val["from_id"]][$val["from_time"]][$val["room_type"]][$val['uid']]['room_type'] = $val["room_type"];
					$arr_stat[$val["from_id"]][$val["from_time"]][$val["room_type"]][$val['uid']]['account'] = $user_id;
					$arr_stat[$val["from_id"]][$val["from_time"]][$val["room_type"]][$val['uid']]['uid'] = $val["uid"];
					$arr_stat[$val["from_id"]][$val["from_time"]][$val["room_type"]][$val['uid']]['total_win'] += $val['total_win'];
				 
					if ($val['total_cost']>0 || $val['total_win']>0)
					{
						$jl=1;
					}
				}  
			}			
       
			// 消耗统计
			if ($jl==1)
			{   
                foreach ($get_list as $k => $val) { 
                    //房间消耗统计           
                    foreach ($room as $kk => $v) {
                        if ($arr_cost[$val["from_id"]][$val["from_time"]][$v][$dt]['from_time']>0)
                        {
                            $data[$kk]['date_at'] = $dt;		
                            $data[$kk]['all_consume'] = $arr_cost[$val["from_id"]][$val["from_time"]][$v][$dt]['all_consume'];
                            $data[$kk]['room_type'] = $v;
                            $data[$kk]['from_id'] = $arr_cost[$val["from_id"]][$val["from_time"]][$v][$dt]['from_id'];
                            $data[$kk]['from_time'] = $arr_cost[$val["from_id"]][$val["from_time"]][$v][$dt]['from_time'];
                        
                           
                            $sap1['date_at'] = $dt;
                            $sap1['room_type'] = $v;
                            $sap1['from_id'] = $arr_cost[$val["from_id"]][$val["from_time"]][$v][$dt]['from_id'];	
                            $sap1['from_time'] = $arr_cost[$val["from_id"]][$val["from_time"]][$v][$dt]['from_time'];  
                          
                            //$info = $tcost->where($sap1)->find();  
                                     
                            if ($info = $tcost->where($sap1)->find()) {     
                                                               
                                $tcost->where(["id"=>$info['id']])->save($data[$kk]);                       
                            } else {
                            				
                                $tcost->add($data[$kk]); 
                                              
                            }
                            unset($sap1);

                        }
                       
                    } 
                }
           
                foreach ($get_list as $kk => $val) { 
                    //$content .= "这是第 ". __LINE__ ." 行---------------:".var_export($val,true);
                    //房间消耗统计           
                    foreach ($room as $rk => $rv) {
                        //用户消耗统计           
                        foreach ($u_ids as $k => $v) {
                            //用户task_stat

                        
                            $data2[$k]['account'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['account'];
                            $data2[$k]['uid'] = $v;
                            $data2[$k]['from_id'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_id'];
                            $data2[$k]['from_time'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_time'];
                            //定位任务 task_config_id
                            // $total_win = $arr_stat[$v]['total_win'];
                            $from_id = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_id'];
                            $from_time = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_time'];
                            $room_type = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['room_type'];
                            //1:赢金 2充值
                            $task_type = $room_type == 0 ? 2 : 1;
                            
                            //用户task_level                      
                            $data3[$k]['updatetime'] = $d_time;
                            $data3[$k]['account'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['account'];                    
                            $data3[$k]['from_id'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_id'];
                            $data3[$k]['from_time'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_time'];
                            //定位任务 task_config_id
                            $data3[$k]['value'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['total_win'];
                            $data3[$k]['room_type'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['room_type'];

                            if ($from_id && $from_time)
                            {
                            
                                $task_configs = D('DChannelUserTasks')->gettaskconfigid($v,$from_id,$from_time,$rv,$task_type);
                            
                                foreach ($task_configs as $t => $tv) {
                                        //$content .= "这是第 ". __LINE__ ." 行---------------:".$tv['id'];
                                        $data2[$k]['task_config_id'] = $tv['id'];

                                        if ($data2[$k]['account'] && $v)
                                        {
                                            $tap2['uid'] = $v;
                                            $tap2['from_id'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_id'];	
                                            $tap2['from_time'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_time']; 
                                            $tap2['task_config_id'] = $tv['id'];
                                        }
                                        
                                        $data3[$k]['task_config_id'] = $tv['id'];

                                        $data3[$k]['level'] = $tv['level']? $tv['level'] : 0;
                                        
                                        if ($data3[$k]['account'] && $v)
                                        {
                                        
                                            $tap3['account'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['account'];
                                            $tap3['from_id'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_id'];	
                                            $tap3['from_time'] = $arr_stat[$val["from_id"]][$val["from_time"]][$rv][$v]['from_time'];   
                                            $tap3['task_config_id'] = $tv['id'];							
                                            if ($info = $tlevel->where($tap3)->find()) {   
                                                //$content .= "这是第 ". __LINE__ ." 行----------------更新:".$v."-".$from_id."-".$from_time."-".$tv['id']."---------\n";
                                                $tlevel->where(["id"=>$info['id']])->save($data3[$k]);                       
                                            } else {	
                                                $data3[$k]['addtime'] = strtotime($e_time);
                                                //$content .= "这是第 ". __LINE__ ." 行-----------------添加:".$v."-".$from_id."-".$from_time."-".$tv['id']."---------\n"; 										
                                                $tlevel->add($data3[$k]);                   
                                            }
                                        }
                                    
                                }
                            
                            }
                            //用户task_record


                        }
                    }
                   
                }
				
				
			}
			

			$etime = microtime(true);
			$totals = round($etime - $stime);
			$content .= "这是第 ". __LINE__ ." 行----------------------------------------共计执行：".$totals."s------------------------------------\n";        
            $content .= "这是第 ". __LINE__ ." 行-------------------------------------渠道统计执行结束时间：".date("H:i:s",$etime)."------------------------------------\n";
            //$contentlog.="---------------------------------渠道统计执行结束时间：".date("H:i:s",$etime)."---共计执行：".$totals."s---------------------------------\n";
            //tasklogger("taskstatlog",$contentlog);
            tasklogger("taskstat",$content);
	
	}

     //自动变期 
    public function channelstat(){

        $stime = microtime(true);
        $content = "--------------------------------变期执行开始时间：".date("H:i:s",$stime)."------------------------------------\n";
        

        $dtime = NOW_TIME;
     
        $map['start_time'] = array('elt', $dtime); //elt 表示 <= 小于等于
		$map['end_time'] = array('egt', $dtime);   //egt 表示 >= 大于等于
		//$map['reg'] = 0;
        $list = D('DChannels')->where($map)->field('start_time,end_time,from_id,from_time,id,reg')->select();
        //$content .= "---------查询sql：".D('DChannels')->getLastSql()."---------\n"; 
		//符合条件激活渠道ID，期数
        $channal ="";
        foreach ($list as $k => $v){  
           
            if ($v['reg']==0)
            {
                //DChannelTasks 有数据了，才执行变期及通知
                $smaps['from_id']=$v['from_id'];
                $smaps['from_time']=$v['from_time'];
                $stotal = D('DChannelTasks')->where($smaps)->count();
                if($stotal>20)
                {
                    $datac['reg'] = 1;  
                    if (D('DChannels')->where(array('id'=>$v['id']))->save($datac))
                    {
                        //发送给游戏服务端
                        $post_url = C('GM_URL') .'cgi-bin/gm_oprate.notice_channel_update';
                        $sendPost['from_id'] = $v['from_id'];
                        $sendPost['from_time'] = $v['from_time'];
                        $sendPost['st_time'] = $v['start_time'];
                        $sendPost['ed_time'] = $v['end_time'];
                        $field = http_build_query($sendPost);
                        $ret = PostUrl($post_url, $field);
                        $channal .= ",远程URL：".$ret."\n";
                        $jhnum = 0;
                        if ($ret=="ok")
                        {
                            /*
                            $umap['online'] =  1;				
                            $umap['from_id'] =  $v['from_id'];
                            $ulist = D('DUsers')->where($umap)->field('id,created_at,from_time,reg_from_task')->select();
                            
                            foreach ($ulist as $kk => $vv){					
                            
                                $fdata['uid'] = $ckfdata['uid'] = $vv['id'];
                                $fdata['from_id'] =$ckfdata['from_id'] =  $v['from_id'];
                                $fdata['task_time'] = $ckfdata['task_time'] = $v['from_time'];
                                
                                if ($vv['from_time']>$vv['reg_from_task'])
                                {
                                    $fdata['task_time_back'] = $v['from_time']-1;
                                }
                                else if ($vv['from_time']==$vv['reg_from_task'])
                                {
                                    $fdata['task_time_back'] = $vv['reg_from_task'];
                                }
                                $fdata['date_at'] = date("Y-m-d");
                                $fdata['addtime'] = time();
                                //$channal .= "这是第 ". __LINE__ ." 行-----------------------fdata：".var_export($fdata,true)."---------\n";
                                $faid = D('fromActivation')->where($ckfdata)->getField('id');
                                //$channal .= "这是第 ". __LINE__ ." 行----------".$faid."-------------查询sql：".D('fromActivation')->getLastSql()."---------\n";
                                if(empty($faid))
                                {
                                    $jhnum++;
                                    D('fromActivation')->add($fdata); //存入激活记录
                                    //$channal .= "这是第 ". __LINE__ ." 行-----------------------查询sql：".D('fromActivation')->getLastSql()."---------\n";
                                }
                                

                            } 
                            $channal .= ",1-执行人数：".$jhnum."\n";
                            */
                            
                        }
                        
                    }

                }

				
            
            }
            /*
            else
            {
                $channal .= "本次执行渠道ID：".$v['from_id'].",期数：".$v['from_time'];
				
                $umap['online'] =  1;				
                $umap['from_id'] =  $v['from_id'];
                $ulist = D('DUsers')->where($umap)->field('id,created_at,from_time,reg_from_task')->select();
                $jhnum = 0;
                foreach ($ulist as $kk => $vv){					
                   
					$fdata['uid'] = $ckfdata['uid'] = $vv['id'];
					$fdata['from_id'] =$ckfdata['from_id'] =  $v['from_id'];
					$fdata['task_time'] = $ckfdata['task_time'] = $v['from_time'];
					 
					if ($vv['from_time']>$vv['reg_from_task'])
					{
						$fdata['task_time_back'] = $v['from_time']-1;
					}
					else if ($vv['from_time']==$vv['reg_from_task'])
					{
						$fdata['task_time_back'] = $vv['reg_from_task'];
					}
					$fdata['date_at'] = date("Y-m-d");
					$fdata['addtime'] = time();
					//$channal .= "这是第 ". __LINE__ ." 行-----------------------fdata：".var_export($fdata,true)."---------\n";
					$faid = D('fromActivation')->where($ckfdata)->getField('id');
					//$channal .= "这是第 ". __LINE__ ." 行----------".$faid."-------------查询sql：".D('fromActivation')->getLastSql()."---------\n";
					if(empty($faid))
					{
						$jhnum++;
						D('fromActivation')->add($fdata); //存入激活记录
						//$channal .= "这是第 ". __LINE__ ." 行-----------------------查询sql：".D('fromActivation')->getLastSql()."---------\n";
					}
					

				} 
                $channal .= ",2-执行人数：".$jhnum."\n"; 

            }
            */

          
               
        }

        $etime = microtime(true);
        $totals = round($etime - $stime);
        $content .= $channal?$channal:"";
        $content .= "---------------------------------共计执行：".$totals."s------------------------------------\n";        
        $content .= "---------------------------------变期执行结束时间：".date("H:i:s",$etime)."------------------------------------\n";
        
        tasklogger("channel",$content);
    
    }


}