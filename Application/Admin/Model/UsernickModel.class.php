<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/2
 * Time: 9:36
 */
namespace Admin\Model;
use Think\Model;

class UsernickModel extends Model{

    protected $tablePrefix = 'rui_';

    //protected $connection = 'DB_CONFIG3';

    protected $tableName = 'usernick';

    /**
     * 获取玩家昵称数据
     */
    public function getUserNick($ids, $type='uid'){
        $data = array();
        foreach ($ids as $v){
            $data[$v] = $this->where(array($type=>$v))->find();
        }
        return $data;
    }


}