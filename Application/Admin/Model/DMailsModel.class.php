<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DMailsModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'mails';
    
	/**
     * 获取邮件类型 配置
     * 
     */	 
	public function getmtype() {
		return C('MTYPE');
    }
	

}