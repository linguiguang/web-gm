<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/7
 * Time: 18:53
 */
namespace  Admin\Model;
use Think\Model;

class DPayhourStatModel extends Model{

    protected $tablePrefix = 'd_';
    protected $connection = 'DB_CONFIG3';
    protected $tableName = 'payhour_stat';
    //�Զ����� 
    public function payhourstat(){
        $stime = microtime(true);
        $content = "---------------------------------ÿСʱ��ִֵ�п�ʼʱ�䣺".date("H:i:s",$stime)."------------------------------------\n";      

        $d_time = NOW_TIME;
        if ($yes_time = stat_zero_time($d_time)) {
			$b_time = date('Y-m-d H:00:00', $yes_time);
			$e_time = date('Y-m-d H:59:59',$yes_time);
			$t_time = date('Y-m-d',$yes_time);
		} else {
			$b_time = date('Y-m-d H:00:00', $d_time);
			$e_time = date('Y-m-d H:59:59', $d_time);
			$t_time = date('Y-m-d',$d_time);
		} 
        $map['created_at'] = array('between', array($b_time, $e_time));
        $map['status'] = 1;
		$map['flag'] = 0;
        $list = D('DPays')->where($map)->field('created_at, price,id')->order('created_at')->select();
        $dt = $t_time;        
        $ret = $datar = array();
        foreach ($list as $k=>$v){            
            $hour =  date('H', strtotime($v['created_at']));
            $ret[$hour]['paydate'] = $dt;
            $ret[$hour]['hour'] =  $hour;
            $ret[$hour]['gold'] += intval($v['price']);
			D('DPays')->where(["id"=>$v['id']])->save(["flag"=>1]); 
        }
        $datar = array();
        foreach ($ret as $k=>$v){ 
            $datar = $v;            
            $datar['u_time']=$d_time;            
            $sap1['paydate']=$v['paydate'];
            $sap1['hour']=$v['hour'];
            $info = $this->where($sap1)->find();
            if ($info) {                        
                $this->where(["id"=>$info['id']])->save($datar);                       
            } else {
                $datar['c_time']=$d_time;            
                $this->add($datar); 
                                
            }
        }
        $etime = microtime(true);
        $totals = round($etime - $stime);
        $content .= "---------------------------------����ִ�У�".$totals."s------------------------------------\n";        
        $content .= "---------------------------------ÿСʱ��ִֵ�н���ʱ�䣺".date("H:i:s",$etime)."------------------------------------\n";
    
        tasklogger("payhour",$content);

    }


}