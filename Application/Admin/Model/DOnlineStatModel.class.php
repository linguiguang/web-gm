<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DOnlineStatModel extends Model{

    protected $tablePrefix = 'd_';
    protected $connection = 'DB_CONFIG3';
    protected $tableName = 'onlinehour_stat';
    
    //批量处理3天记录
    public function onlinedayhour(){
        return;
        $stime = microtime(true); 
        $content = "---------------------------------每小时在线人数批量执行开始时间：".date("H:i:s",$stime)."------------------------------------\n"; 
        for($s=0; $s<3; $s++){
            $stime = date('Y-m-d', strtotime('-'.$s. ' day')); 
            $b_time = strtotime($stime." 00:00:00");
            $e_time = strtotime($stime." 23:59:59");
            for($i=$b_time;$i<=$e_time;$i+=3600){
                $date[] = date('H', $i);
                $inoutsql = "in_time between ".$b_time." and ".$e_time;
                $inoutsql .= " or out_time between ".$b_time." and ".$e_time;
                $list = D('DUserRoomIoLogs')->where($inoutsql)->field('DISTINCT uid')->order('uid asc')->select();
                $content .= "---------------------------------sql：".D('DUserRoomIoLogs')->getLastSql()."------------------------------------\n";  
                $oninfo = $return = array();
                foreach ($list as $v){
                    $dt = date('Y-m-d', $b_time);
                    $hour = date('H', $b_time);
                    $oninfo[$dt][$hour]['paydate'] = $dt;
                    $oninfo[$dt][$hour]['hour']=$hour;
                    $oninfo[$dt][$hour]['play_num'] += 1;
                }
                
                $datar = array();
                foreach ($oninfo as $k=>$v){ 
                    $datar = $v;            
                    $datar['u_time']=time();
                     
                    $sap1['paydate']=$v['paydate'];
                    $sap1['hour']=$v['hour'];
                    $info = $this->where($sap1)->find();
                    if ($info) {                        
                        $this->where(["id"=>$info['id']])->save($datar);  
                        $content .= "---------------------------------sql：".$this->getLastSql()."------------------------------------\n";        
                                    
                    } else {
                        $datar['c_time']=time();            
                        $this->add($datar); 
                        $content .= "---------------------------------sql：".$this->getLastSql()."------------------------------------\n"; 
                                      
                    }
                }
            }
                        
        }
        
        //tasklogger("onlinehour",$content);
    }
    //自动变期 
    public function onlinehourstat(){
        $stime = microtime(true);
        $content = "---------------------------------每小时在线人数执行开始时间：".date("H:i:s",$stime)."------------------------------------\n";      


        $d_time = NOW_TIME;
        if ($yes_time = stat_zero_time($d_time)) {
			$b_time = strtotime(date('Y-m-d H:00:00', $yes_time));
			$e_time = strtotime(date('Y-m-d H:59:59',$yes_time));
		
		} else {
			$b_time = strtotime(date('Y-m-d H:00:00'));
			$e_time = strtotime(date('Y-m-d H:59:59'));
		
		}  

        $content .= "---------------------------------sql：".date('Y-m-d H:i:00')."---".date('Y-m-d H:i:59')."s------------------------------------\n";        
        
        $map['in_time'] = array('between', array($b_time, $e_time));
        $map['out_time'] = array('between', array($b_time, $e_time));
        $inoutsql = "in_time between ".$b_time." and ".$e_time;
        $inoutsql .= " or out_time between ".$b_time." and ".$e_time;
        $list = D('DUserRoomIoLogs')->where($inoutsql)->field('DISTINCT uid')->order('uid asc')->select();
        $content .= "---------------------------------sql：".D('DUserRoomIoLogs')->getLastSql()."------------------------------------\n";     
       // tasklogger("onlinehour",$content);
        $oninfo = $return = array();
        foreach ($list as $v){
            $dt = date('Y-m-d', $b_time);
            $hour = date('H', $b_time);
            $oninfo[$hour]['paydate'] = $dt;
            $oninfo[$hour]['hour']=$hour;

            $oninfo[$hour]['play_num'] += 1;
        }
        
        $datar = array();
        foreach ($oninfo as $k=>$v){ 
            $datar = $v;            
            $datar['u_time']=$d_time;
             
            $sap1['paydate']=$v['paydate'];
            $sap1['hour']=$v['hour'];
            $info = $this->where($sap1)->find();
            if ($info) {                        
                $this->where(["id"=>$info['id']])->save($datar);  
                $content .= "---------------------------------sqladd：".$this->getLastSql()."s------------------------------------\n";        
                            
            } else {
                $datar['c_time']=$d_time;            
                $this->add($datar); 
                $content .= "---------------------------------sqladd：".$this->getLastSql()."s------------------------------------\n";        
       
                                
            }
        }
        $etime = microtime(true);
        $totals = round($etime - $stime);
        $content .= "---------------------------------共计执行：".$totals."s------------------------------------\n";        
        $content .= "---------------------------------每小时在线人数执行结束时间：".date("H:i:s",$etime)."------------------------------------\n";
    
        tasklogger("onlinehour",$content);

    }

   

}