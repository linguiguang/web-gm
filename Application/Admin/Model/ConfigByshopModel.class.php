<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/9
 * Time: 11:56
 */

namespace Admin\Model;

use Think\Model;

class ConfigByshopModel extends Model
{

    private $_config;
    private $_recharge;

    public function _initialize(){
        $file = 'config/shop_recharge.xlsx';
        $file = iconv('UTF-8','GB2312',$file);
        $this->_config = F('SHOP');
        $info = array();
        if(1) { //!$this->_config
            $data = format_excel2array($file, 0);

            for ($i = 4; $i <= count($data); $i++) {
                if(!empty($data[$i]["A"])) {
                    $info[$data[$i]['A']] = array(
                        'id' => $data[$i]['A'],
                        'name' => $data[$i]['B'],
                        'need_vip' => $data[$i]['O'],
                        'day_buying' => $data[$i]['P'],
                        'time' => $data[$i]['Q'],
                        'money' => $data[$i]['F'],
                    );
                }
            }
            $this->_config = $info;
            F('SHOP', $info);
        }
        //var_dump($this->_config);exit;
    }

    public function getConfig() {
        return $this->_config;
    }


    public function getRecharge()
    {
        return $this->_config; //$this->_recharge;
    }

    //充值获得金币
    public function getGoldRecharge()
    {
        $recharges = $this->_recharge;
        $ret = array();
        foreach ($recharges as $v) {
            if ($v['item_id'] != 101) {
                continue;
            }

            $ret[$v['key']] = $v['item_num'] + $v['item_extra_num'];
        }
        return $ret;
    }

    public function getExchange()
    {
        $file = 'config/牛牛商城数据表.xlsx';
        //        $file = iconv('UTF-8','GB2312',$file);
        if (1) { //$this->_config == null
            $info = [];
            $data = format_excel2array($file, 2);
            for ($i = 7; $i <= count($data); $i++) {
                $info[$data[$i]['B']] = array(
                    $data[2]['B'] => $data[$i]['B'],
                    $data[2]['C'] => $data[$i]['C'],
                    $data[2]['D'] => $data[$i]['D'],
                    $data[2]['E'] => $data[$i]['E'],
                    $data[2]['F'] => $data[$i]['F'],
                    $data[2]['G'] => $data[$i]['G'],
                    $data[2]['K'] => $data[$i]['K'],
                );
            }

        }
        return $info;
    }

   
}
