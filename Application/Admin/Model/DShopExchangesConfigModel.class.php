<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DShopExchangesConfigModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG3';

    protected $tableName = 'shop_exchanges_config';

	/**
     * 获取兑换商品描述
     * 
     */
    public function getexcconfig()
    {
        $excconfigs = $this->field("ID,desc,item_id,num")->select();
        $congfigs = [];
        foreach ($excconfigs as $k => $v)
        {
            $congfigs[$v['ID']]['desc'] = $v['desc'];
            $congfigs[$v['ID']]['item_id'] = $v['item_id'];            
            $congfigs[$v['ID']]['num'] = $v['num'];
        }
	    
        return $congfigs;
    }

  
   

}