<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DShopExchangesModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'shop_exchanges';


	/**
     * 获取兑换商品名称
     * 
     */
    public function shopexc()
    {
		$rooms = array(
            99001 => '2000金币',
			99002 => '10000金币',
			99003 => '20000金币',
            99004 => '200000金币',
			99005 => '1000000金币',
			99006 => '2000000金币'
			);
		
        return $rooms;
    }


   

}