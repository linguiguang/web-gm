<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/7
 * Time: 18:53
 */
namespace  Admin\Model;
use Think\Model;

class DTaskCostModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG3';

    protected $tableName = 'task_cost';

    function getchannelconsume($from_id,$from_time,$date_at)
    {
        if ($from_id)
        {
            $consumeMap["d_task_stat.from_id"] = $from_id;
        }
        if ($from_time)
        {
            $consumeMap["d_task_stat.from_time"] = $from_time;
        }       
        $b_time = $date_at;
        if($b_time) {
            $b_time = strtotime($date_at.' 00:00:00');
            $e_time = strtotime($date_at.' 23:59:59');
            $between = [$b_time, $e_time];
        }
		
		//tasklogger("cost",$b_time."-----".$e_time."-----".$date_at."\n");
        if($between) {
            $consumeMap['d_task_stat.addtime'] = ['BETWEEN', $between];
        }
        $consumeMap['d_task_stat.task_config_id'] = ['gt', 0];
		$consumeMap['d_task_stat.from_id'] = ['NEQ' , 1000];
        $consumeList = D('DTaskStat')->join('game1.d_channel_tasks as f ON f.id = d_task_stat.task_config_id', 'left')->where($consumeMap)
            ->field('f.reward,f.from_id,f.from_time,f.task_type,f.room_type')
            ->select();
	    
		//tasklogger("cost",D('DTaskStat')->getLastsql()."----------array".var_export($consumeList,true)."\n");
        $all_consume=0;
        foreach ($consumeList as $val) {
            $all_consume += $val['reward'];
        }
        return $all_consume;
    }

    function getchannelconsumenew($date_at)
    {
           
        $b_time = $date_at;
        if($b_time) {
            $b_time = strtotime($date_at.' 00:00:00');
            $e_time = strtotime($date_at.' 23:59:59');
            $between = [$b_time, $e_time];
        }
		
		//tasklogger("cost",$b_time."-----".$e_time."-----".$date_at."\n");
        if($between) {
            $consumeMap['d_task_stat.addtime'] = ['BETWEEN', $between];
        }
        $consumeMap['d_task_stat.task_config_id'] = ['gt', 0];
		$consumeMap['d_task_stat.from_id'] = ['NEQ' , 1000];
        $consumeList = D('DTaskStat')->join('game1.d_channel_tasks as f ON f.id = d_task_stat.task_config_id', 'left')->where($consumeMap)
            ->field('SUM(f.reward) AS reward')
            ->select();
	    
		tasklogger("cost",D('DTaskStat')->getLastsql()."----------array".var_export($consumeList,true)."\n");
        $all_consume=0;
        foreach ($consumeList as $val) {
            $all_consume += $val['reward'];
        }
        return $all_consume;
    }


}