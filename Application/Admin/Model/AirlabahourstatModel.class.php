<?php

namespace Admin\Model;

use Think\Model;

/**
 * 机战玩家游戏日志
 */
class AirlabahourstatModel extends Model
{
    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';
}
