<?php

namespace Admin\Model;

use Think\Model;

/**
 * 每日抽奖
 */
class LotterylogModel extends Model
{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';
}
