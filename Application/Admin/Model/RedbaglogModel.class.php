<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/11
 * Time: 17:50
 */
namespace Admin\Model;
use Think\Model;

class RedbaglogModel extends Model{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    public function areaarr(){
        $room = array(
//            113 => '实物兑换',
            120 => '红包场领红包',
            139 => '7日狂欢',
            140 => '分享转盘',
            148 => '一本万利',
        );
        return $room;
    }
}