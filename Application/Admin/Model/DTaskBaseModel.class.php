<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DTaskBaseModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG2';

    protected $tableName = 'task_base';
    
	/**
     * 获取物品名
     * 
     */
 
	public function gettaskbase() {
		$goods = $this->order("ID asc")->select();
        //print_r($goods);
		$list=[];
	    foreach ($goods as $k => $v) {
			$list[$v['ID']]['id'] = $v['ID'];
            if ($v['ID']<3014)
            {
                $describe = iconv('UTF-8','GB2312',$v['describe']);
            }
            else
            {
                $describe = $v['describe'];
            }
            $list[$v['ID']]['describe'] = $describe;
            $list[$v['ID']]['task_type'] = $v['task_type'];
        }
        
        return $list;
    }
	

}