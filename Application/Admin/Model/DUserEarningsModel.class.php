<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/10
 * Time: 18:04
 */
namespace Admin\Model;
use Think\Model;

class DUserEarningsModel extends Model{

    protected $tablePrefix = 'd_';

    protected $connection = 'DB_CONFIG1';

    protected $tableName = 'user_earnings';

    public function chu_shi_hua_stat()
	{
        // 执行原生 SQL 查询

        $sql="CALL RenewDUserEarnings();";
        $result = $this->query($sql);
        
        // 返回查询结果
        return $result;
	}
  

}