<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/22
 * Time: 17:41
 */

namespace Admin\Model;

use Think\Model;

class DailywingoldModel extends Model
{

    protected $dbName = 'niuniulogs';

    protected $tablePrefix = 'stat_';

    protected $connection = 'DB_CONFIG2';

    /**
     * 获取配置
     * @param $nper
     * @param int $formatting 0 不进行操作，1 键值->键的格式化， 2 键键->键的格式化操作
     * @return mixed
     */
    public function getconfig($nper, $formatting = false)
    {
        $nper = intval($nper);
        $config = $this->configarrs($nper);
        if (!$config) $this->error('不存在此期数：' . $nper);

        if ($formatting) {
            if ($nper !== 4) {
                $config = $this->doconfig($config);
            } else if ($nper === 4) {
                $config = $this->doconfig4($config);
            }
        }

        return $config;
    }

    //配置
    private function configarrs($nper)
    {
        $config[1] = array( //第1期
            '50000' => 1,
            '150000' => 1,
            '300000' => 1.5,
            '600000' => 1.5,
            '1200000' => 4,
            '2500000' => 8,
            '5000000' => 12,
            '7500000' => 15,
            '15000000' => 35,
            '30000000' => 65,
            '60000000' => 120,
            '100000000' => 200,
            '300000000' => 400,
            '600000000' => 600,
            '1000000000' => 800,
            '2000000000' => 2000,
            '4000000000' => 4000,
            '9000000000' => 10000,
            '18000000000' => 18000,
        );
        $config[2] = array( //第2期
            '50000' => 1,
            '150000' => 1,
            '300000' => 1.5,
            '600000' => 1.5,
            '1200000' => 4,
            '2500000' => 8,
            '5000000' => 12,
            '7500000' => 15,
            '15000000' => 35,
            '30000000' => 65,
            '60000000' => 120,
            '100000000' => 200,
            '300000000' => 400,
            '600000000' => 600,
            '1000000000' => 800,
            '2000000000' => 2000,
            '4000000000' => 4000,
            '9000000000' => 10000,
            '18000000000' => 18000,
        );
        $config[3] = array( //第3期
            '20000' => 1,
            '60000' => 1,
            '150000' => 1,
            '300000' => 1.5,
            '600000' => 1.5,
            '1200000' => 4,
            '2500000' => 8,
            '5000000' => 12,
            '7500000' => 15,
            '15000000' => 35,
            '30000000' => 65,
            '60000000' => 120,
            '100000000' => 200,
            '300000000' => 400,
            '600000000' => 600,
            '1000000000' => 800,
            '2000000000' => 2000,
            '4000000000' => 4000,
            '9000000000' => 10000,
            '18000000000' => 18000,
        );
        $config[4] = array( //第四期
            '50000' =>    1,
            '150000'    => 1,
            '500000'    => 1,
            '1000000'    => 1,
            '2000000'    => 2,
            '4000000'    => 4,
            '7500000'    => 6,
            '15000000'    => 10,
            '30000000'    => 15,
            '50000000'    => 20,
            '100000000'    => 45,
            '200000000'    => 90,
            '400000000'    => 180,
            '750000000'    => 300,
            '1500000000'    => 600,
            '3000000000'    => 1200,
            '5000000000'    => 2000,
            '10000000000'    => 4000,
            '20000000000'    => 9000,
            '50000000000'    => 20000
        );
        return $config[$nper];
    }

    //单位换算处理
    protected function doconfig($config)
    {
        foreach ($config as $k => $v) {
            $unit = '';
            if ($k >= 100000000) {
                $unit = ($k / 100000000) . 'e';
            } else {
                $unit = ($k / 10000) . 'w';
            }
            $unit = str_replace('.', '_', $unit);

            $ret[1][$unit] = $v;
            $ret[2][$unit] = $k;
        }
        return $ret;
    }

    //单位换算处理
    protected function doconfig4($config)
    {
        foreach ($config as $k => $v) {
            $unit = '';
            $unit = ($k / 500000) . '元';
            $unit2 = ($k / 50000);
            $ret[1][$unit] = $v;
            $ret[2][$unit2] = $v;
            $ret[3][$unit2] = $k;
        }
        return $ret;
    }

    /**
     * 1期数据
     * @param $param
     * @return int|mixed
     */
    public function calcu_forth_money_1($param)
    {
        if (!is_array($param)) return 0;

        $money = 0;
        foreach ($param as $k => $v) {
            $nper = $this->getconfig(1, true);
            foreach ($nper[1] as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v;
                }
            }
            if ($k == 'sev_man') {
                $money += 12 * $v;
            } elseif ($k == '159_man') {
                $money += 25 * $v;
            } elseif ($k == '799_man') {
                $money += 40 * $v;
            } elseif ($k == '3699_man') {
                $money += 300 * $v;
            } elseif ($k == '8899_man') {
                $money += 500 * $v;
            } elseif ($k == 'play1') {
                $money += $v;
            }
        }
        return $money;
    }
    /**
     * 2期数据
     * @param $param
     * @return int|mixed
     */
    public function calcu_forth_money_2($param)
    {
        if (!is_array($param)) return 0;

        $money = 0;
        foreach ($param as $k => $v) {
            $nper = $this->getconfig(2, true);
            foreach ($nper[1] as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v;
                }
            }
            if ($k == 'sev_man') {
                $money += 12 * $v;
            } elseif ($k == '159_man') {
                $money += 25 * $v;
            } elseif ($k == '799_man') {
                $money += 40 * $v;
            } elseif ($k == '3699_man') {
                $money += 300 * $v;
            } elseif ($k == '8899_man') {
                $money += 500 * $v;
            } elseif ($k == 'play1') {
                $money += $v;
            }
        }
        return $money;
    }
    /**
     * 3期数据
     * @param $param
     * @return int|mixed
     */
    public function calcu_forth_money_3($param)
    {
        if (!is_array($param)) return 0;

        $money = 0;
        foreach ($param as $k => $v) {
            $nper = $this->getconfig(3, true);
            foreach ($nper[1] as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v / 10;
                }
            }
            if ($k == 'sev_man') {
                $money += 12 * $v;
            } elseif ($k == '159_man') {
                $money += 25 * $v;
            } elseif ($k == '799_man') {
                $money += 40 * $v;
            } elseif ($k == '3699_man') {
                $money += 300 * $v;
            } elseif ($k == '8899_man') {
                $money += 500 * $v;
            } elseif ($k == 'play1') {
                $money += $v;
            } elseif ($k == 'Redbag1') {
                $money += $v;
            } elseif ($k == 'Redbag13') {
                $money += $v;
            }
        }
        return $money;
    }
    /**
     * 4期数据
     * @param $param
     * @return int|mixed
     */
    public function calcu_forth_money_4($param)
    {
        if (!is_array($param)) return 0;

        $money = 0;
        foreach ($param as $k => $v) {
            $nper = $this->getconfig(4, true);
            foreach ($nper[2] as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v;
                }
            }

            if ($k == 'ten_man') {
                $money += 12 * $v;
            } elseif ($k == '389_man') {
                $money += 25 * $v;
            } elseif ($k == '799_man') {
                $money += 40 * $v;
            } elseif ($k == '3699_man') {
                $money += 300 * $v;
            } elseif ($k == '8899_man') {
                $money += 500 * $v;
            } elseif ($k == 'play3') {
                $money += $v;
            }
        }

        return $money;
    }

    public function plan_one1($param, $config)
    {
        if (!is_array($param)) return 0;
        $money = 0;
        foreach ($param as $k => $v) {
            foreach ($config as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v;
                }
            }

            if ($k == 'tenRmb') {
                $money += 10 * $v;
            } elseif ($k == 'twoHundredRmb') {
                $money += 10 * $v;
            } elseif ($k == 'sixHundredRmb') {
                $money += 40 * $v;
            } elseif ($k == 'twoThousandRmb') {
                $money += 120 * $v;
            } elseif ($k == 'sixThousandRmb') {
                $money += 400 * $v;
            }
        }

        return $money;
    }

    public function plan_one2($param, $config)
    {
        if (!is_array($param)) return 0;
        $money = 0;
        foreach ($param as $k => $v) {
            foreach ($config as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v;
                }
            }

            if ($k == 'tenRmb') {
                $money += 8 * $v;
            } elseif ($k == 'twoHundredRmb') {
                $money += 10 * $v;
            } elseif ($k == 'sixHundredRmb') {
                $money += 40 * $v;
            } elseif ($k == 'twoThousandRmb') {
                $money += 120 * $v;
            } elseif ($k == 'sixThousandRmb') {
                $money += 400 * $v;
            }
        }

        return $money;
    }

    public function plan_one3($param, $config)
    {
        if (!is_array($param)) return 0;
        $money = 0;
        foreach ($param as $k => $v) {
            foreach ($config as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v;
                }
            }

            if ($k == 'tenRmb') {
                $money += 8 * $v;
            } elseif ($k == 'twoHundredRmb') {
                $money += 10 * $v;
            } elseif ($k == 'fourHundredRmb') {
                $money += 16 * $v;
            } elseif ($k == 'eightHundredRmb') {
                $money += 40 * $v;
            } elseif ($k == 'fifteenHundredRmb') {
                $money += 70 * $v;
            } elseif ($k == 'threeThousandRmb') {
                $money += 140 * $v;
            } elseif ($k == 'eightThousandRmb') {
                $money += 500 * $v;
            } elseif ($k == 'sixteenThousandRmb') {
                $money += 800 * $v;
            } elseif ($k == 'ThirtyfiveThousandRmb') {
                $money += 1800 * $v;
            }
        }

        return $money;
    }

    public function plan_one4($param, $config)
    {
        if (!is_array($param)) return 0;
        $money = 0;
        foreach ($param as $k => $v) {
            foreach ($config as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v;
                }
            }

            if ($k == 'tenRmb') {
                $money += 13 * $v;
            } elseif ($k == 'twoHundredRmb') {
                $money += 10 * $v;
            } elseif ($k == 'fourHundredRmb') {
                $money += 16 * $v;
            } elseif ($k == 'eightHundredRmb') {
                $money += 40 * $v;
            } elseif ($k == 'fifteenHundredRmb') {
                $money += 70 * $v;
            } elseif ($k == 'threeThousandRmb') {
                $money += 140 * $v;
            } elseif ($k == 'eightThousandRmb') {
                $money += 500 * $v;
            } elseif ($k == 'sixteenThousandRmb') {
                $money += 800 * $v;
            } elseif ($k == 'ThirtyfiveThousandRmb') {
                $money += 1800 * $v;
            }
        }

        return $money;
    }

    public function plan_one5($param, $config)
    {
        if (!is_array($param)) return 0;
        $money = 0;
        foreach ($param as $k => $v) {
            foreach ($config as $key => $val) {
                if ($k == $key) {
                    $money += $val * $v;
                }
            }

            if ($k == 'tenRmb') {
                $money += 13 * $v;
            } elseif ($k == 'twoHundredRmb') {
                $money += 10 * $v;
            } elseif ($k == 'fourHundredRmb') {
                $money += 16 * $v;
            } elseif ($k == 'eightHundredRmb') {
                $money += 40 * $v;
            } elseif ($k == 'fifteenHundredRmb') {
                $money += 70 * $v;
            } elseif ($k == 'threeThousandRmb') {
                $money += 140 * $v;
            } elseif ($k == 'eightThousandRmb') {
                $money += 500 * $v;
            } elseif ($k == 'sixteenThousandRmb') {
                $money += 800 * $v;
            } elseif ($k == 'ThirtyfiveThousandRmb') {
                $money += 1800 * $v;
            }
        }

        return $money;
    }
}
