<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.thinkphp.cn>
// +----------------------------------------------------------------------

/**
 * 前台配置文件
 * 所有除开系统级别的前台配置
 */
return array(
    // 微信支付结果查询
    'WX_APPID' => 'wxd722bedb59bbd7e9',
    'WX_SECRET' => 'e13d74bfec0288824530480e8b302174',
    'APP_WX_APPID' => 'wxda57526bcc163327',
    'APP_WX_SECRET' => '33f7015192408bee3e66c0d6f6702a3e',
    'WX_KEY' => 'HNRqoK0ZfbeNCPSR3OYeMWrum5L3N9lr',
    'WX_MCHID' => '1560120881',
    'ACCESS_URL' => "http://niuwx.jisudw.com/index.php?s=redbag/binduser",
    'WX_DOMAIN' => 'niuwx.jisudw.com',
    'WX_API_URL' => 'api.weixin.qq.com',
    'JINFU_CALLBACK_URL' => 'http://niuwx.jisudw.com/index.php?s=pay/jinfucallback', //支付成功回调
    'WX_PAY_APPID' => 'wx6a7507a20d428141',
    'WX_EXT_MINPROG_USERNAME' => 'gh_316fea1f9a5b',

    //微信支付证书地址
    'PUBLIC_KEY' => './certs/apiclient_cert.pem',
    'PRIVATE_KEY' => './certs/apiclient_key.pem',
    // 'ROOTCA'     => './rootca.pem',

    /* 数据缓存设置 */
    'DATA_CACHE_PREFIX' => 'gamesystem_', // 缓存前缀
    'DATA_CACHE_TYPE' => 'File', // 数据缓存类型
    'URL_MODEL' => 1, //URL模式

    /* 文件上传相关配置 */
    'DOWNLOAD_UPLOAD' => array(
        'mimes' => '', //允许上传的文件MiMe类型
        'maxSize' => 5 * 1024 * 1024, //上传的文件大小限制 (0-不做限制)
        'exts' => 'jpg,gif,png,jpeg,zip,rar,tar,gz,7z,doc,docx,txt,xml', //允许上传的文件后缀
        'autoSub' => true, //自动子目录保存文件
        'subName' => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
        'rootPath' => './Uploads/Download/', //保存根路径
        'savePath' => '', //保存路径
        'saveName' => array('uniqid', ''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
        'saveExt' => '', //文件保存后缀，空则使用原后缀
        'replace' => false, //存在同名是否覆盖
        'hash' => true, //是否生成hash编码
        'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
    ), //下载模型上传配置（文件上传类配置）

    /* 图片上传相关配置 */
    'PICTURE_UPLOAD' => array(
        'mimes' => '', //允许上传的文件MiMe类型
        'maxSize' => 2 * 1024 * 1024, //上传的文件大小限制 (0-不做限制)
        'exts' => 'jpg,gif,png,jpeg', //允许上传的文件后缀
        'autoSub' => true, //自动子目录保存文件
        'subName' => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
        'rootPath' => './Uploads/Picture/', //保存根路径
        'savePath' => '', //保存路径
        'saveName' => array('uniqid', ''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
        'saveExt' => '', //文件保存后缀，空则使用原后缀
        'replace' => false, //存在同名是否覆盖
        'hash' => true, //是否生成hash编码
        'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
    ), //图片上传相关配置（文件上传类配置）

    'PICTURE_UPLOAD_DRIVER' => 'local',
    //本地上传文件驱动配置
    'UPLOAD_LOCAL_CONFIG' => array(),
    'UPLOAD_BCS_CONFIG' => array(
        'AccessKey' => '',
        'SecretKey' => '',
        'bucket' => '',
        'rename' => false,
    ),
    'UPLOAD_QINIU_CONFIG' => array(
        'accessKey' => '__ODsglZwwjRJNZHAu7vtcEf-zgIxdQAY-QqVrZD',
        'secrectKey' => 'Z9-RahGtXhKeTUYy9WCnLbQ98ZuZ_paiaoBjByKv',
        'bucket' => 'onethinktest',
        'domain' => 'onethinktest.u.qiniudn.com',
        'timeout' => 3600,
    ),

    /* 编辑器图片上传相关配置 */
    'EDITOR_UPLOAD' => array(
        'mimes' => '', //允许上传的文件MiMe类型
        'maxSize' => 2 * 1024 * 1024, //上传的文件大小限制 (0-不做限制)
        'exts' => 'jpg,gif,png,jpeg', //允许上传的文件后缀
        'autoSub' => true, //自动子目录保存文件
        'subName' => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
        'rootPath' => './Uploads/Editor/', //保存根路径
        'savePath' => '', //保存路径
        'saveName' => array('uniqid', ''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
        'saveExt' => '', //文件保存后缀，空则使用原后缀
        'replace' => false, //存在同名是否覆盖
        'hash' => true, //是否生成hash编码
        'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
    ),

    /* 模板相关配置 */
    'TMPL_PARSE_STRING' => array(
        '__STATIC__' => __ROOT__ . '/Public/static',
        '__ADDONS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/Addons',
        '__IMG__' => __ROOT__ . '/Public/' . MODULE_NAME . '/images',
        '__CSS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/css',
        '__JS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/js',
    ),

    /* SESSION 和 COOKIE 配置 */
    'SESSION_PREFIX' => 'onethink_admin', //session前缀
    'COOKIE_PREFIX' => 'onethink_admin_', // Cookie前缀 避免冲突
    'VAR_SESSION_ID' => 'session_id', //修复uploadify插件无法传递session_id的bug

    /* 后台错误页面模板 */
    'TMPL_ACTION_ERROR' => MODULE_PATH . 'View/Public/error.html', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS' => MODULE_PATH . 'View/Public/success.html', // 默认成功跳转对应的模板文件
    'TMPL_EXCEPTION_FILE' => MODULE_PATH . 'View/Public/exception.html', // 异常页面的模板文件

    //'DB_CONFIG1' => 'mysqli://mhy:123456@192.168.2.17:3306/testgg',
    'DB_CONFIG1' => 'mysqli://root:123456@localhost:3306/gamebox',
    //'DB_CONFIG2' => 'mongo://niu:nV9l$y0#g9cbJZ^QJ315C96aY8%6twcS@StatusServerMongo:27017/niuniulogs',
    //'DB_CONFIG2' => 'mongo://StatusServerMongo:27017/niuniulogs',
    //'DB_CONFIG2' => 'mongo://root:magic#77880@dds-uf6b0827703dca441174-pub.mongodb.rds.aliyuncs.com:3717/admin',
    // 'DB_CONFIG2' => 'mongo://localhost:27017/niuniulogs',
    //'DB_CONFIG2' => 'mysqli://mhy:123456@192.168.2.17:3306/game1',
    'DB_CONFIG2' => 'mysqli://root:123456@localhost:3306/game1',
    'DB_CONFIG3' => 'mysqli://root:123456@localhost:3306/gamedatalogs',
    'DB_CONFIG4' => 'mysqli://root:syhjvip-work616-dijfuhGro90F@47.96.22.83:3306/centerd',
	//'DB_CONFIG4' => 'mysqli://root:magic#77880@localhost:3306/game1',
    //    'DB_CONFIG3' => 'mysql://root:root@127.0.0.1:3306/ruiqusystemdb',
    //    'EMAIL_SEND_URL' => 'http://106.15.52.115:9001/cgi-bin/gm_oprate:send_mail',//邮件地址
    //    'MONIPAY_SEND_URL' => 'http://106.15.52.115:9001/cgi-bin/pay_handle:delivery',//模拟充值地址
    'GM_URL' => 'http://139.224.217.108:8180/svjhs/',
    'GMBox_URL' => 'http://139.224.217.108:8180/svjh/',
    'GM1_URL' => 'http://192.168.2.17:9991/',
    'SENDGAME_KEY' =>"2FrWpAiCykeFCWoyezRovt6o56miitqc",

    'DEVICE' => array(
        '0' => '安卓',
        '1' => 'IOS',
    ),

    'ROOM_NAME' => array(
        103 => '任务奖励',
        164 => '红包场水果',
        165 => '红包场[站内]水果',
        153 => '红包直接兑换成⾦币',
        113 => '实物兑换',
        1002 => '发单人邮件',
        104 => '商城',
        100 => '后台命令',
        178 => '每日抽奖',
        179 => '试玩机战领取红包',
        180 => '娱乐机战领取红包',
        181 => '试玩机战[击落]红包',
        182 => '娱乐机战[击落]红包'
    ),

    // 运行中的方案编号, 数组最后一个为当前期
    'PLAN_NUMS' => array(1, 2, 3, 4, 5),
    // 全部到期的方案编号,
    'PLAN_NUMS_END' => array(1, 2, 3),

    // 超级水果 1期 key的单位为角, value的单位为元
    'PLAN_ONE1' => array(
        '1' => 1, '5' => 1, '9' => 1, '20' => 1, '40' => 2, '80' => 4,
        '160' => 6, '300' => 10, '600' => 18,
        '1000' => 30, '2000' => 60, '4000' => 120, '8000' => 240,
        '15000' => 400, '30000' => 800, '60000' => 1600,
        '100000' => 3000, '300000' => 8000,
        '1000000' => 20000,
    ),

    // 超级水果 2期 key的单位为角, value的单位为元
    'PLAN_ONE2' => array(
        '1' => 1, '5' => 1, '9' => 1, '20' => 1, '40' => 2, '80' => 4,
        '160' => 6, '300' => 10, '600' => 18,
        '1000' => 30, '2000' => 60, '4000' => 120, '8000' => 240,
        '15000' => 400, '30000' => 800, '60000' => 1600,
        '100000' => 3000, '300000' => 8000,
        '1000000' => 20000,
    ),

    // 超级水果 3期 key的单位为角, value的单位为元
    'PLAN_ONE3' => array(
        '1' => 1, '3' => 1, '8' => 1, '15' => 1, '30' => 1, '50' => 1, '70' => 1, '90' => 1,
        '120' => 1.5, '150' => 2, '180' => 2.5, '250' => 5, '400' => 10, '600' => 13,
        '1000' => 20, '1500' => 30, '3000' => 85, '6000' => 170,
        '10000' => 250, '20000' => 550, '40000' => 1000, '60000' => 1200,
        '100000' => 2000, '150000' => 2500, '300000' => 7000, '500000' => 10000,
        '1000000' => 20000,
    ),

    // 超级水果 4期 key的单位为角, value的单位为元
    'PLAN_ONE4' => array(
        '1' => 1, '3' => 1.5, '8' => 1, '15' => 1, '30' => 1, '50' => 1, '70' => 1, '90' => 1,
        '120' => 1.5, '150' => 2, '180' => 2.5, '250' => 5, '400' => 10, '600' => 13,
        '1000' => 20, '1500' => 30, '3000' => 85, '6000' => 170,
        '10000' => 250, '20000' => 550, '40000' => 1000, '60000' => 1200,
        '100000' => 2000, '150000' => 2500, '300000' => 7000, '500000' => 10000,
        '1000000' => 20000,
    ),

    // 超级水果 5期 key的单位为角, value的单位为元
    'PLAN_ONE5' => array(
        '1' => 1, '3' => 1.5, '8' => 1, '15' => 1, '30' => 1, '50' => 1, '70' => 1, '90' => 1,
        '120' => 1.5, '150' => 2, '180' => 2.5, '250' => 5, '400' => 10, '600' => 13,
        '1000' => 20, '1500' => 30, '3000' => 85, '6000' => 170,
        '10000' => 250, '20000' => 550, '40000' => 1000, '60000' => 1200,
        '100000' => 2000, '150000' => 2500, '300000' => 7000, '500000' => 10000,
        '1000000' => 20000,
    ),


    // 模拟充值使用
    'WEB_SERVER_IP' => 'StatusServerWeb',
    'WEB_SERVER_PORT' => '9809',

    // 自动填平功能
    'FRUIT_POOL_OBJECT_OF_REFERENCE' => 200000000,
    'SUPER_FRUIT_POOL_OBJECT_OF_REFERENCE' => 200000000,
    // 大注
    'FRUIT_POOL_OBJECT_OF_REFERENCE_HIGH' => 30000000,
    'SUPER_FRUIT_POOL_OBJECT_OF_REFERENCE_HIGH' => 30000000,

    // 兑换红包配置
    //红包金额
    'MONEY' => array(
        '99018' => 5,
        '99054' => 10,
        '99020' => 50,
        '99021' => 300,
        '99022' => 600,
        '99050' => 1,
        '99053' => 2,
        '99051' => 100,
        '990211' => 150,
        '990222' => 200,
    ),

    'SFTP_CONF' => [
        'host' => '47.100.114.158',
        'name' => 'root',
        'pwd' => 'Shaigong@123',
    ],
    'ROBOT_CONF' => [
        'host' => '39.105.180.203',
        'name' => 'root',
        'pwd' => 'syhjmKfo27wd0Uerruijwegwye43g',
    ],

    'PRODUCT_NAME' => 'magic',  //配置产品名
    'PRODUCT' => '航海夺宝',
    'COLOR' => '#fa8c35',  //后台导航颜色color

    //aliyun短信keyID
    'ALI_SMS_KEYID' => 'LTAI5tAxEGzXwe8CkqdjtWdj',
    //aliyun短信keySecret
    'ALI_SMS_SECRET' => '9SpNC8j9tnVsk84jKpJbU8L0Ae53Fy',
    //aliyun短信名称
    'ALI_SMS_NAME' =>'大连勇鸿峰有限公司',
    //aliyun短信setTemplateCode
    'ALI_SMS_TEMPLATE_CODE' => 'SMS_277415442',

    //渠道用户管理 游戏名称分类
    "TASK_GAME_TYPE" => [
        1 => '卡拉姆',
        2 => '特洛伊',
        3 => '星舰争霸',
        // 4 => '超级狂欢'
    ],

    //休闲场与红包场配置名，后台显示用
    "HB_CONSUME" => '岛歌少女消耗',
    "LEISURE_CONSUME" => '疾风夜刃消耗',


    //SFTP更新文件 目录
    'REMOTE_FILE' => '/root/GameServerDev/script/script_server/game.config', ///wangzhejuji/GameServerDev/script/script_server

    //huanxijiayi
    'HUANXIJIAYI_MCH_ID' => '211309659',
    'HUANXIJIAYI_KEY' => 'jl835ikj288i5tbm67tycxmgq4276ic6',
    'ROOMS' => array(
        1 => '卡拉姆试玩场',
        2 => '特洛伊休闲场',
        3 => '索罗之海权益场'
    ),
    'ROOMS_NEW' => array(
        11 => array(
        'name' => '大圣归来',
        'sub_rooms' => array()),
		10 => array(
			'name' => '鱼跃龙门',
			'sub_rooms' => array(
				1 => '鱼跃九排',
				21 => '鱼跃十五排',
				12 => '鱼跃消消乐'
			))
    ),
    'ROOM_LIST' => array(
        1 => "卡拉姆沙漠排行榜",
        2 => "特洛伊之城排行榜",
        3 => "索罗之海排行榜",
        0 => "用户排行榜"
    ),
    'RANKTYPE_LIST1' => array(
        1 => "卡拉姆沙漠日榜",
        2 => "卡拉姆沙漠周榜",
        3 => "卡拉姆沙漠月榜"
    ),
    'RANKTYPE_LIST2' => array(
        4 => "特洛伊之城日榜",
        5 => "特洛伊之城周榜",
        6 => "特洛伊之城月榜"
    ),
    'RANKTYPE_LIST3' => array(
        7 => "索罗之海日榜",
        8 => "索罗之海周榜",
        9 => "索罗之海月榜"
    ),
    'RANKTYPE_LIST4' => array(                
        31 => "英雄争霸排行榜"
    ),
    'RANKTYPE' => array(
        //0 => "未知",
        1 => "卡拉姆沙漠日榜-投注",
        2 => "卡拉姆沙漠周榜-投注",
        3 => "卡拉姆沙漠月榜-投注",
        4 => "特洛伊之城日榜-投注",
        5 => "特洛伊之城周榜-投注",
        6 => "特洛伊之城月榜-投注",
        7 => "索罗之海日榜-投注",
        8 => "索罗之海周榜-投注",
        9 => "索罗之海月榜-投注",            
        33 => "红包瓜分-投注",
        31 => "英雄争霸排行榜-赢金",
        32 => "领主争霸-赢金"
        //91 => "其它"
    ),
    'MTYPE' => array(
        101 => '卡拉姆沙漠 周榜',
        102 => '卡拉姆沙漠 日榜',
        201 => '特洛伊之城 日榜',
        202 => '特洛伊之城 周榜',
        203 => '特洛伊之城 月榜',
        14 => '领主争霸榜 排名',
        18 => '风暴之榜 排行',
        19 => '每周瓜分',
        1 => '系统邮件',
        99 => '用户邮件'
    ),
    'SOURCE' => array(
        999=>"测试",
        996=>"GM",
        0 =>"未知",
        1 =>"任务",
        2 =>"红包",
        3 =>"排行榜",
        4 =>"laba", 
        5 =>"邮件", 
        6 =>"商店", 
        7 =>"兑换",
        8 =>"laba祈愿坛", 
        9 =>"破产补助", 
        10 =>"月卡", 
        11 =>"红包场宝箱", 
        12 =>"实名认证", 
        13 =>"手机绑定", 
        14 =>"VIP领取", 
        15 =>"laba大奖777", 
        16 =>"皇家宝藏", 
        17 =>"渠道升级晶石为宝石", 
        18 =>"奖池瓜分"
    )
);
